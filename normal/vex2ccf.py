#! /usr/bin/python
"""
vex2ccf v1.5 dated 20150824

Usage: vex2ccf.py [-1|--single] vexfile.vex [templatecontrolfile.ctrl]

Generates new SFXC control file(s) for each scan found in the VEX file.

If -1 or --single is specified, one control file (<experimentName.ctrl>)
is created that contains all the scans of the VEX file. The default is 
to create separate control files (<experimentName_scanNr>.ctrl) for each 
scan and also use seprate visibility output files (<...scanNr.cor>).
In the latter case you might want to run 'adust_start.py' on the generated 
control files to fix their start times to integration time boundaries.

Custom correlator settings may be loaded from a template control file. 
These user settings are merged into the auto-generated control file(s). 
"""

Tint_recommended = [0.25, 0.5, 1.0, 2.0, 4.0]  # https://svn.astron.nl/sfxc/trunk/sfxc/docs/Control_file_description.txt
nchan_recommended = [32, 64, 128, 256, 512, 1024, 2048, 4096]

# Standard Python modules.
import os, re, sys, time

# The json module is new in Python 2.6; fall back on simplejson if it
# isn't available.
try:
    import json
except:
    import simplejson as json

# Choose one of the VEX parsers
vex_parser_version = -1
try:
    # Check for ./sfxc/lib/vex_parser/vex_parser/python/ library
    from vex_parser import Vex as vexparser
    vex_parser_version = 1
except:
    pass

try:
    # Check for ./vex/ simple library
    import vex as vexparser
    vex_parser_version = 2
except:
    pass

if (vex_parser_version < 1):
    print ('Error: could not load any VEX parser module.')
    sys.exit(-1)


# Proper time.
os.environ['TZ'] = "UTC"

def vex2time(str):
    tupletime = time.strptime(str, "%Yy%jd%Hh%Mm%Ss");
    return time.mktime(tupletime)

def time2vex(secs):
    tupletime = time.gmtime(secs)
    return time.strftime("%Yy%jd%Hh%Mm%Ss", tupletime)

def get_mode(vex):
    sched = vex['SCHED']
    for scan in sched:
        break
    return sched[scan]['mode']

def get_scan_stop(scan):
    time = vex2time(scan['start'])
    time += max(float(station[2].split()[0]) \
                    for station in scan.getall('station'))
    return time2vex(time)

def get_start(vex):
    sched = vex['SCHED']
    for scan in sched:
        return sched[scan]['start']
    return ""

def get_stop(vex):
    sched = vex['SCHED']
    for scan in sched:
        continue
    return get_scan_stop(sched[scan])

def get_scans(vex):
    sched = vex['SCHED']
    return [(sched[scan]['start'], get_scan_stop(sched[scan])) \
                for scan in sched]

def get_subband(vex, chan_def, IF, BBC):
    result = {}
    result["frequency"] = int(float(chan_def[1].split()[0]) * 1e6)
    result["bandwidth"] = int(float(chan_def[3].split()[0]) * 1e6)
    result["sideband"] = chan_def[2]

    # In order to figure out the polarisation we need the IF this
    # subband is connected to.  The connection between IF and subband
    # is provided by the BBC.
    for BBC_assign in vex["BBC"][BBC].getall("BBC_assign"):
        if BBC_assign[0] == chan_def[5]:
            for if_def in vex["IF"][IF].getall("if_def"):
                if if_def[0] == BBC_assign[2]:
                    result["polarisation"] = if_def[2]
                    break
                continue
            pass
        continue
    return result

def get_subbands(vex):
    mode = get_mode(vex)

    # Pick the first $FREQ block for the mode and find the matching
    # $IF and $BBC blocks.
    freq = vex['MODE'][mode]['FREQ'][0]
    station = vex['MODE'][mode]['FREQ'][1]
    for IF in vex['MODE'][mode].getall('IF'):
        if station in IF[1:]:
            break
        continue
    for BBC in vex['MODE'][mode].getall('BBC'):
        if station in BBC[1:]:
            break
        continue

    result = []
    for chan_def in vex['FREQ'][freq].getall('chan_def'):
        result.append(get_subband(vex, chan_def, IF[0], BBC[0]))
        continue
    return result

def get_channels(vex):
    mode = get_mode(vex)
    freq = vex['MODE'][mode]['FREQ'][0]
    channels = []
    for chan_def in vex['FREQ'][freq].getall('chan_def'):
        channels.append(chan_def[4])
        continue
    return channels

def make_default_ccf(vex):
    ccf = {}
    ccf["exper_name"] = vex["GLOBAL"]["EXPER"]
    ccf["start"] = get_start(vex)
    ccf["stop"] = get_stop(vex)
    ccf["scans"] = get_scans(vex)
    ccf["reference_station"] = ""
    ccf["cross_polarize"] = False
    ccf["number_channels"] = 32
    ccf["channels"] = get_channels(vex)
    ccf["subbands"] = get_subbands(vex)
    ccf["integr_time"] = 1.0
    ccf["message_level"] = 1
    ccf["delay_directory"]  = ""
    ccf["output_file"] = ""
    ccf["data_sources"] = {}
    ccf["site_position"] = {}

    ccf["stations"] = []
    for station in vex["STATION"]:
        ccf["stations"].append(station)
        ccf["data_sources"][station] = []
        site = vex["STATION"][station]["SITE"]
        ccf["site_position"][station] = \
            [ float(pos.split()[0]) for pos in vex["SITE"][site]["site_position"] ]
        continue

    return ccf

def merge_ccf(ccf,refccf):

    # By default we only add new keys into 'ccf' that are in 'refccf' and missing from 'ccf'.
    # However, certain keys already present in 'ccf' can be overwritten by or merged from 'refccf'.

    # Special list of keys to always merge, even if already present in 'ccf':
    always_merge  = ['stations','cross_polarize','integr_time','sub_integr_time','number_channels','fft_size_delaycor']
    always_merge += ['output_file','delay_directory']
    always_merge += ['site_position', 'data_sources']
    always_merge += ['message_level']

    # More keys for Phased Array SFXC:
    always_merge += ['phased_array','pulsars','coherent_dedispersion']
    always_merge += ['reference_station','cl_table','bp_table', 'window_function']

    # Note some misleading SFXC key names:
    #   'channels' = a string array of IF names
    #   'number_channels' = number of correlator lags/bins/channels, not the length of 'channels'

    # Merge from reference settings
    for key in refccf:

        if (key in ccf) and not (key in always_merge):
            continue

        if key in always_merge:
            # Merge
            if isinstance(refccf[key], dict):
                # Dictionary: look at all key-value pairs, retain "longest" values
                lhs = ccf[key].copy()
                rhs = refccf[key].copy()
                tmp = lhs.copy() 
                tmp.update(rhs)
                for subkey in tmp:
                    if (subkey in lhs) and (subkey in rhs) and (lhs[subkey] != rhs[subkey]):
                        if (len(lhs[subkey]) > len(rhs[subkey])):
                            tmp[subkey] = lhs[subkey]
                        else:
                            tmp[subkey] = rhs[subkey]
                ccf[key] = tmp
            elif isinstance(refccf[key], list):
                # Lists: make union, discard duplicate entries
                ccf[key] = ccf[key] + list(set(refccf[key]) - set(ccf[key]))
            else:
                # Other: overwrite with copy from reference
                ccf[key] = refccf[key]
        else:
            # Key still missing, copy it from reference
            ccf[key] = refccf[key]

    return ccf

def check_ccf(ccf):
    warnings = []

    if not (ccf['integr_time'] in Tint_recommended):
        warnings.append('Integration time %f not in recommended list of %s' % (ccf['integr_time'],str(Tint_recommended)))

    if not (ccf['number_channels'] in nchan_recommended):
        warnings.append('Number of correlator lags %d not in recommended list of %s' % (ccf['number_channels'],str(nchan_recommended)))

    for station in ccf['data_sources']:
        for ds in ccf['data_sources'][station]:
            if len(ds) < 1:
                continue
            if (ds.find('file://') != 0) and (ds.find('mark5://') != 0) and (ds.find('mk5://') != 0) :
                warnings.append('Station %s data source string %s might be illegit' % (station,ds))

    return warnings

def write_ccf(vex,ccf,single_ccf_mode):

    # Write all scans into one control file
    if single_ccf_mode:

        formatted = json.dumps(ccf, indent=4, sort_keys=True, separators=(',',':'))

        fname = vex["GLOBAL"]["EXPER"]+'.ctrl'
        f = open(fname, 'w')
        f.write(formatted)
        f.close()

        # Original vex2ccf.py prints the control file to standard out, do the same here.
        print (formatted)

    # Each scans into separate control file, assign own .cor output file to each
    else:

        json_copy = ccf.copy()
        for i in range(len(ccf["scans"])):

            fout = ccf["output_file"]
            if len(fout) <= 4:
                 fout = 'file:///%s_%04d.cor' % (vex["GLOBAL"]["EXPER"],i+1)
            elif fout[-4:] == '.cor':
                 fout = '%s_%04d.cor' % (fout[:-4],i+1)

            json_copy["output_file"] = fout
            json_copy["scans"] = [ccf["scans"][i]]

            formatted = json.dumps(json_copy, indent=4, sort_keys=True, separators=(',',':'))

            fname = '%s_%04d.ctrl' % (vex["GLOBAL"]["EXPER"],i+1)
            f = open(fname, 'w')
            f.write(formatted)
            f.close()
            print ('Wrote %s' % (fname))


if __name__ == "__main__":

    singleMode = False
    args = sys.argv[1:]

    if (len(args) >= 1) and (args[0] in ['--singlemode','-1']):
        singleMode = True
        args = args[1:]

    if (len(args) not in [1,2]):
        print (__doc__)
        sys.exit(-1)

    # Parse the VEX file
    if (vex_parser_version == 1):
        vex = vexparser(args[0])
    else:
        vex = vexparser.Vex(args[0])

    # Derive a default control file
    ccf = make_default_ccf(vex)

    # Merge default with template control file
    try:
        if (len(args) >= 2):
           user_ccf = json.load(open(args[1],'r'))
           ccf = merge_ccf(ccf,user_ccf)
    except:
        print ('Error loading reference control file : ' + str(err))

    # Auto-generate control file(s)
    write_ccf(vex,ccf,singleMode)

    warnings = check_ccf(ccf)
    if len(warnings) > 0:
        print ('\n\nThe resulting control file may have some issues:')
        for warning in warnings:
            print ('   ' + warning)
