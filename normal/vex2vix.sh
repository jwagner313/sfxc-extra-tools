#!/bin/bash

if [ "$1" == "" ] || [ ! -f $1 ]; then
	echo "Usage: vex2vix.sh <vexfile>"
	exit
fi

fvix="${1/%vex/vix}"
cp $1 $fvix

# Remove some ref's and sections
DELKEYS=('$THREADS' '$BITSTREAMS' '$EOP' '$CLOCK')
for key in "${DELKEYS[@]}"; do

	# echo $key

	# Testing:
	# echo "/[ \t]*ref[ \t]*$key/d"
	# echo "/[ \t]*\\$key;/,/[ \t]*\\\$/{//!d}"
	# echo "/[ \t]*\\$key;/d"
	# '/[ \t]*ref[ \t]*$THREADS/d'
	# '/[ \t]*\$THREADS;/,/[ \t]*\$/{//!d}'
	# '/[ \t]*\$THREADS;/d'

	# Actual commands:
	sed -i "/[ \t]*ref[ \t]*$key/d" $fvix
	sed -i "/[ \t]*\\$key;/,/[ \t]*\\\$/{//!d}" $fvix
	sed -i "/[ \t]*\\$key;/d" $fvix
done

echo "Created $fvix for SFXC utility 'j2ms2'. Diff to $1:"
diff -u $1 $fvix
