#!/usr/bin/python
"""
Adjust SFXC ctl files to have start times at an integration time granularity.

Usage: adust_start.py <vex file> <ctrl_file 1> ...  <ctrl_file N>

If your observation spans multiple scans then we assume that you correlate 
each scan individually. You should make sure that the start times in the 
control files are an integer multiple of 'integr_time' apart. This
script (adust_start.py) will make sure of this.
"""
import sys, json
from math import ceil
from vex import Vex

def vex_time(vstring):
  vs = vstring.split('y')
  vs = vs[1].split('d')
  day = int(vs[0])
  vs = vs[1].split('h')
  hour = int(vs[0])
  vs = vs[1].split('m')
  minute = int(vs[0])
  vs = vs[1].split('s')
  second = int(vs[0])
  return day * 86400 + (hour * 60 + minute) * 60 + second

if len(sys.argv) < 3:
  i = max(0, sys.argv[0].rfind('/'))
  print __doc__
  sys.exit(1)

vex = Vex(sys.argv[1])
start_experiment = None
for ctrl_filename in sys.argv[2:]:
  f = open(ctrl_filename, 'r')
  ctrl = json.load(f)
  correlation_start = vex_time(ctrl['start'])
  # find the correct scan
  for scan in vex['SCHED']:
    scan_start = vex_time(vex['SCHED'][scan]['start'])
    scan_offset = 0
    scan_length = 0
    for station in vex['SCHED'][scan].getall('station'):
      if station[0] in ctrl["stations"]:
        scan_offset = max(scan_offset, int(station[1].rstrip(' sec')))
        scan_length = max(scan_length, int(station[2].rstrip(' sec')))
    if (correlation_start >= scan_start) and (correlation_start < scan_start + scan_length):
      break
  # A bit of a hack : We assume that we always find the scan
  new_start = scan_start + scan_offset + 1
  if start_experiment == None:
    start_experiment = new_start
  integr_time = ctrl['integr_time']
  new_start = start_experiment + ceil((new_start-start_experiment) / integr_time) * integr_time

  if correlation_start < new_start:
    year = int(ctrl['start'].split('y')[0])
    day = new_start / 86400
    hour = (new_start % 86400) / 3600;
    minute = (new_start % 86400) % 3600 / 60
    second = new_start%60
    print 'old start =', ctrl['start'],
    ctrl['start'] ="%dy%03dd%02dh%02dm%02ds"%(year,day,hour,minute,second)
    print ', new start = ', ctrl['start']
    f.close()
    f = open(ctrl_filename, 'w')
    json.dump(ctrl, f,indent=2)
  f.close()

