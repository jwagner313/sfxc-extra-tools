#!/usr/bin/python

import sys, os

stationmap = {'KVNTAMNA':'KTN', 'KVNULSAN':'KUS', 'KVNYONSEI':'KYS', 'ISHIGAKI':'ISG', 'IRIKI':'IRK',
  'OGASAWARA':'OGA', 'MIZUSAKI':'MIZ', 'MIZNAO20':'MIZ', 'OGASA20':'OGA'}

max_Nif = 1

HOME = os.path.dirname(os.path.realpath(sys.argv[0]))+'/'

if len(sys.argv) != 2:
	print ('log2antabVERA.py <station log file>')
	print ('The output in ANTAB format is to STDOUT.')
	sys.exit(-1)

f = open(sys.argv[1])

station = ''
key = '/STATION/'
for line in f:
	i = line.find(key)
	if (i > 0):
		# 017234501/STATION/ I, II, ISHIGAKI, AZEL, -3263994.8821, 4808056.3608, 2619949.1356, 0.0
		data = line[(i+len(key)):]
		data = data.replace(' ','')
		data = data.split(',')
		station = data[2]
station_orig = station
if station in stationmap:
	station = stationmap[station]
f.close()

f = open(sys.argv[1])
key  = '/TSYS/'
tsys = []
Nif  = 0
for line in f:
	i = line.find(key)
	if (i > 0):
		# 018000025/TSYS/  01,  30.0653, , 179.78
		timecode = line[:line.find('/')]
		timecode = timecode.strip()
		data = line[(line.find(key)+len(key)):]
		data = data.strip()
		data = data.replace(' ','')

		tt = (timecode[:3], timecode[3:5], timecode[5:7], timecode[7:9]) # doy hh mm ss
		antab_T = '%s %s:%s:%s ' % tt

		antab_D = ''
		dd  = data.split(',')
		Nif = len(dd)/2
		Nif = min([max_Nif, Nif])
		for jj in range(Nif):
			ifnr = dd[2*jj]
			iftsys = dd[2*jj+1]
			antab_D = antab_D + ('%s ' % iftsys)
		tsys.append(antab_T + ' ' + antab_D)
f.close()

gain = []
f = open(HOME+'/JNET_gain.txt')
for line in f:
	i = line.find(station_orig)
	if (i > 0):
		line = line.strip()
		line = line.replace(station_orig, station)
		gain.append(line)
f.close()


print ('!--- Tsys* Data ---------')
print ('TSYS %s FT=1.0 TIMEOFF=0.0 RANGE=10,800 INDEX=\'L1:%d\' /' % (station,Nif)) 
print ('!DOY UT TSYS*')
for line in tsys:
	print (line)
print ('/')

if (len(gain) > 0):
	print ('!--- Gain Curve Data ---------')
	for g in gain:
		print (g)

