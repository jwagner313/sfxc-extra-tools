
rm -f r14018a.antab

cat r14018aKTN.ANTAB.1 > r14018a.antab
cat r14018aKUS.ANTAB.1 >> r14018a.antab
cat r14018aKYS.ANTAB.1 >> r14018a.antab

~/code/sfxc-extra-tools/KaVA/log2antabVERA.py ../log/r14018aIRK.log > irk.antab
~/code/sfxc-extra-tools/KaVA/log2antabVERA.py ../log/r14018aISG.log > isg.antab
~/code/sfxc-extra-tools/KaVA/log2antabVERA.py ../log/r14018aOGA.log > oga.antab
~/code/sfxc-extra-tools/KaVA/log2antabVERA.py ../log/r14018aMIZ.log > miz.antab

cat irk.antab >> r14018a.antab
cat isg.antab >> r14018a.antab
cat oga.antab >> r14018a.antab
cat miz.antab >> r14018a.antab

echo "GAIN MIZ ALTAZ DPFU=0.057455 POLY=1.00 FREQ=21530,22630 / " >> r14018a.antab
echo "GAIN IRK ALTAZ DPFU=0.061437 POLY=1.00 FREQ=21530,22630 / " >> r14018a.antab
echo "GAIN OGA ALTAZ DPFU=0.050970 POLY=1.00 FREQ=21530,22630 / " >> r14018a.antab
echo "GAIN ISG ALTAZ DPFU=0.048353 POLY=1.00 FREQ=21530,22630 / " >> r14018a.antab

#echo "GAIN MIZ ALTAZ DPFU=0.053700 POLY=1.00 FREQ=42450,43200 / " >> r14018a.antab
#echo "GAIN IRK ALTAZ DPFU=0.052107 POLY=1.00 FREQ=42450,43200 / " >> r14018a.antab
#echo "GAIN OGA ALTAZ DPFU=0.048922 POLY=1.00 FREQ=42450,43200 / " >> r14018a.antab
#echo "GAIN ISG ALTAZ DPFU=0.045964 POLY=1.00 FREQ=42450,43200 / " >> r14018a.antab

