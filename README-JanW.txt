Contents

1. Building SFXC itself
2. Building the extra utilities


== 1. Building SFXC itself ===

When compiling on a 64-bit system, need additional packages for a full SFXC compile:

  libgcc.i686
  libgfortran.i686
  libstdc++.i686

The trunk SFXC has a ./sfxc/compile.sh with preparations

   aclocal -I m4 && autoheader && autoconf && automake --add-missing

For a full SFXC compile can use

  export IPP_ROOT=/share/apps/intel/ipp/
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$IPP_ROOT/lib/intel64/:$IPP_ROOT/../compiler/lib/intel64/

  ./configure --enable-debug --enable-progress \
              --enable-calc10 --enable-gendelay \
              --enable-ipp --with-ipp-path=/share/apps/intel/ipp/ \
              CXXFLAGS="-DPRINT_PID=true -DPRINT_HOST=true -DSFXC_DETERMINISTIC" \
              CXX=mpicxx \
              MPICXX=/share/apps/openmpi/bin/mpicxx \
              --prefix=/home/jwagner/code/sfxc-trunk/install/

  Defining SFXC_DETERMINISTIC forces all processing to be serialized, and run
  on the "current correlator node" only (see manager_node.cc). Good for debugging.
  For actual parallel processing, leave out the SFXC_DETERMINISTIC define:

  export IPP_ROOT=/share/apps/intel/ipp/
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$IPP_ROOT/lib/intel64/:$IPP_ROOT/../compiler/lib/intel64/

  ./configure --enable-debug --enable-progress \
              --enable-calc10 --enable-gendelay \
              --enable-ipp --with-ipp-path=/share/apps/intel/ipp/ \
              CXXFLAGS="-DPRINT_PID=true -DPRINT_HOST=true" \
              CXX=mpicxx \
              MPICXX=/share/apps/openmpi/bin/mpicxx \
              --prefix=/home/jwagner/code/sfxc-trunk/install/


If sfxc/lib/vex_parser/vex_parser/python/ should be built, it needs

   vex_parser$ ./configure --enable-32-bit --enable-pythonlib=yes \
                           --prefix=/home/jwagner/code/sfxc-trunk/sfxc/lib/vex_parser/install

   and requires packages

     boost
     boost-python



== 2. Building the extra utilities ===

Building the extra utilities (j2ms2, tConvert) to get FITS files from SFXC output
is a terrible mess... Need Casa Core (see ./README.txt here). Also need to edit
some "hard-coded" paths pointing to CFITSIO and CASA binary (not code) install
locations. Also needs the package

    flex   or   flex-devel    (whichever also provides /usr/lib64/libfl.a)

Some steps are described in the ./README.txt which at some point refers
you onwards to code/jive/README.

The following must be updated to reflect your CASA Core installation:

   ./code/jive/system/locations.mak
   ./code/jive/system/compiler.mak

   (many pieces like Albert Bos' 'albert' code packages are mentioned, but
    do not exist, but these are not necessary for the compile either!)

The extra utilities are quite... old... and apparently are not refreshed
to include newest SFXC headers. In particular output_header.h should be
replaced with the newest one:

   cp ~/sfxc-trunk/sfxc/include/output_header.h ./code/jive/implement/j2ms/sfxc/output_header.h

   and in case there were renamings or dramatic changes in the new output_header.h
   compared to the old one, you might also have to fix SFXCVisBuffer.cc,

   nano ./code/jive/implement/j2ms/SFXCVisBuffer.cc

I've tried to put compile related things into this script:

   ./code/jive/compile_JanW.sh
