
This is a set of scripts and tools for SFXC, largely identical
to their original versions provided by Mark Kettenis, Aard Keimpema
and other JIVE folks.

The tools here include the SFXC .cor to FITS-IDI converter that
allows SFXC output to be post-processed in AIPS.

They also include ParselTongue scripts of the SFXC phased array
mode with some adaptions for the KaVA Korea-Japan VLBI network.

================================================================

Notes on SFXC .cor to FITS conversion:

1) $ cd  <SFXC .cor output directory>
2) $ cp ../experiment.vex experiment.vix
3) $ edit experiment.vix  
     remove $THREADS; section and 
     remove $THREADS references
3) $ j2ms2 experiment_file1.cor experiment_file2.cor 
     this creates a new subdirectory "experiment.ms"
4) $ tConvert experiment.ms experiment.fits

Correlation must apparently be done on a per-scan basis,
one SFXC control file per scan, since otherwise the
AIPS 'SU' table has only the 1st source and 'NX' has
a single scan that starts/ends at whatever time was specified
in the .ctl file used during correlation.

Could use generate_jobs.py, but with KJCC OCTADISK-extracted data 
this is problematic as OCTADISK produces a single huge VDIF file 
per station with data from experiment start time until stop time.

The $CLOCK; section in a VEX file prepared for SFXC correlation
requires clock offsets in microseconds, and clock rates in
microseconds per second rather than the usual s/s.

================================================================

Below is a copy of the documentation from:
http://www.jive.nl/~kettenis/sfxc/tools/

Prerequisites
-------------

On Ubuntu 12.04 LTS the following packages are needed on top of a
default install:

bison
cmake
flex
g++
gfortran
libatlas-dev
libblas-dev
libbsd-dev
libcfitsio3-dev
libfftw3-dev
liblapack-dev
libncurses5-dev
libreadline-dev
tcl
wcslib-dev

Packages might have slightly different names on other Linux
distributions.  Let me know if you have trouble locating a particular
package.


Step 1: Build casacore
----------------------

See <http://code.google.com/p/casacore/>.  I recommend building and
installing version 1.5.0.  Follow the build instructions on
<http://code.google.com/p/casacore/wiki/BuildInstructions>.  Don't
forget install a copy of the measures data as the conversion tools
depend on them.


Step 2: Build myvex
-------------------

Download <http://www.jive.nl/~kettenis/sfxc/tools/myvex.tar.gz>.
Unpack the source code in a appropriate place and type "make".


Step 3: Build j2ms2 & tConvert
------------------------------

Download <http://www.jive.nl/~kettenis/sfxc/tools/tools.tar.gz>.
Unpack the source code in an appropriate place and follow the build
instructions in code/jive/README.  Make sure you follow the
instructions labelled "Casacore mode" in the second part of the
README.  It tells you to edit ${ROOT}/system/locations.mak.  If you
managed to install all the packages mention in the "Prerequisites"
section above, you will probably only need to change CASAROOT and
VEXROOT.  Set CASAROOT to the location where you installed casacore,
and VEXROOR to the location where you built myvex.  Assuming a 32-bit
Linux system, the tools end up in ${ROOT}/bin/i686-32/.


Using the tools
---------------

Converting SFXC correlator output into FITS-IDI is a two-step process.
You first need to run j2ms2 to convert the data to an aips++/CASA
MeasurementSet, which then can be converted into FITS-IDI by running
tConvert.

To run j2ms2, create a directory with the name of the experiment.
Copy the VEX file for the experiment into that directory and rename it
EXPER.vix, where EXPER is the name of the experiment.  Also copy the
correlator output files into that directory.  Assuming yur experiment
name is N12L1, you should have something like the following:

N12L1/N12L1.vix
N12L1/N12L1_No0001.cor
N12L1/N12L1_No0002.cor

To run j2ms2, you now do

$ cd N12L1
$ j2ms2 N12L1_No0001.cor N12L1_No0002.cor

this will produce a directory N12L2.ms with lots of stuff in it.  To
convert it into FITS-IDI you run tConvert:

$ tConvert N12L1.ms N12L1.IDI

You now have a FITS file that can be loaded into AIPS!
