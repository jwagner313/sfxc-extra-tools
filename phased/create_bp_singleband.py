#!/usr/bin/env ParselTongue

# JanW: issues 
#  1) original create_bp.py did not cope with the case of just 1 IF
#  2) SFXC wants BP data points in upper sideband mode (band top edge with
#     a negative frequency increment per channel does not work),
#     this is even when the VEX file and FITS data are lower sideband...
#  3) antenna names! Hardware correlator outputs MIZ, IRK, etc that
#     thus will also be the AIPS station names, whereas the VEX file
#     and the SFXC station namers are Vm, Vr, etc!

# Hardware correlator to software correlator station name mapping for KaVA stations
KJCC_3to2letter_names = { 'MIZ': 'Vm', 'IRK': 'Vr', 'OGA': 'Vo', 'ISG': 'Vs', 'KYS': 'Ky', 'KUS': 'Ku', 'KTN': 'Kt' }

import AIPS, sys, datetime, struct, pdb
from Wizardry.AIPSData import AIPSUVData
from Wizardry.AIPSData import AIPSTableRow
from numpy import *
from scipy import interpolate
from optparse import OptionParser

try:
  import json
except:
  import simplejson as json

def parse_cli_args():
  parser = OptionParser('%prog <config_file>')
  parser.add_option('-k',  '--kjcc', action='store_true', help = 'Assume AIPS uses KJCC hardware correlator station names (3-letter names)')
  (options, args) = parser.parse_args()
  if len(args) != 1:
    parser.error('invalid number of arguments')

  try:
    cfg = json.load(open(args[0], 'r'))
  except StandardError, err:
    print "Error loading config file : " + str(err)
    sys.exit(1);

  a = cfg['aips']
  AIPS.userno = a['user_nr']
  uvdata = AIPSUVData(a['name'].encode('ascii'), a['class'].encode('ascii'), a['disk'], a['seq'])
  if cfg['outname'].startswith('file://'):
    outname = cfg['outname'][7:]
  else:
    outname = cfg['outname']
  return outname, uvdata, options.kjcc

def get_bp_old(uvdata,outname):
  bp = uvdata.table('BP', 0)
  nstation = bp.keywords['NO_ANT']
  nif = bp.keywords['NO_IF']
  npol = bp.keywords['NO_POL']
  nchan = bp.keywords['NO_CHAN']
  outfile = open(outname+'.bp', 'w')
  bp_table = [[[] for j in range(4+(npol-1)*2)] for i in range(nstation)] 
  #pdb.set_trace()
  date_obs = [int(t) for t in uvdata.header.date_obs.split('-')] # format : year-month-day
  mjd_obs = mjd(date_obs[2], date_obs[1], date_obs[0])
  # Read the bp table into memory. 
  for row in bp:
   ant_no = row.antenna-1
   bp_table[ant_no][0].append(row.time)
   bp_table[ant_no][1].append(row.interval)
   bp_table[ant_no][2].append(row.real_1)
   bp_table[ant_no][3].append(row.imag_1)
   if(npol == 2):
     bp_table[ant_no][4].append(row.real_2)
     bp_table[ant_no][5].append(row.imag_2)
  # Convert to numpy arrays
  for ant in bp_table:
    for i in range(4+(npol-1)*2):
      ant[i] = array(ant[i], dtype='float32')

  #A table to convert the AIPS station numbers to SFXC station number
  aips2sfxc = []
  sfxc_antennas = sorted(uvdata.antennas)
  for ant in uvdata.antennas:
    aips2sfxc.append(sfxc_antennas.index(ant))
  
  #write the BP to disk
  global_header = array([nstation, nif, npol, nchan, mjd_obs], dtype='int32')
  global_header.tofile(outfile)
  for ant_no, ant in enumerate(bp_table):
    header = array([aips2sfxc[ant_no],len(ant[0])], dtype='int32')
    header.tofile(outfile)
    for i in xrange(len(ant[0])):
      # The complex bandpass
      bp_1 = (ant[2][i] + 1j * ant[3][i])
      ant[0][i:i+1].tofile(outfile) # time
      ant[1][i:i+1].tofile(outfile) # interval
      bp_1.tofile(outfile)
      if npol == 2:
        bp_2 = (ant[4][i] + 1j * ant[5][i])
        bp_2.tofile(outfile)

def get_fg(uvdata,outname):
  fg = uvdata.table('FG', 0)
  nstation = bp.keywords['NO_ANT']
  nif = bp.keywords['NO_IF']
  npol = bp.keywords['NO_POL']
  nchan = bp.keywords['NO_CHAN']
  outfile = open(outname+'.bp', 'w')
  bp_table = zeros([nstation, npol, nchan*nif], dtype=complex64)
  weights = zeros([nstation, npol, nchan*nif], dtype=float32)
  #pdb.set_trace()
  # Read the bp table and do weighted sum of all bandpasses
  for row in bp:
    ant_no = row.antenna-1
    for i in range(nif):
      for c in range(nchan):
        j = i*nchan + c
        if not(type(row.weight_1) is list):
          if row.weight_1 > 0.01:
            if row.real_1[j] < 20.:
              weights[ant_no][0][j] += row.weight_1
              bp_table[ant_no][0][j] += (row.real_1[j] + 1j*row.imag_1[j])*row.weight_1
          if (npol ==2) and (row.weight_2 > 0.01):
            if row.real_2[j] < 20.:
              weights[ant_no][1][j] += row.weight_2
              bp_table[ant_no][1][j] += (row.real_2[j] + 1j*row.imag_2[j])*row.weight_2
        else:
          if row.weight_1[i] > 0.01:
            if row.real_1[j] < 20.:
              weights[ant_no][0][j] += row.weight_1[i]
              bp_table[ant_no][0][j] += (row.real_1[j] + 1j*row.imag_1[j])*row.weight_1[i]
          if (npol ==2) and (row.weight_2[i] > 0.01):
            if row.real_2[j] < 20.:
              weights[ant_no][1][j] += row.weight_2[i]
              bp_table[ant_no][1][j] += (row.real_2[j] + 1j*row.imag_2[j])*row.weight_2[i]
    if (ant_no == 3):
      #print row.time,' : ',weights[4][0][0:32]#,'\n', bp_table[4][0][0:32],'\n',row.weight_1
      print row.time,' : ', row.weight_1
      #pdb.set_trace()
  # normalize bandpass
  bp_table /= (weights + 1e-8)

  #A table to convert the AIPS station numbers to SFXC station number
  aips2sfxc = []
  sfxc_antennas = sorted(uvdata.antennas)
  for ant in uvdata.antennas:
    aips2sfxc.append(sfxc_antennas.index(ant))
    print 'ant = ', ant, ', idx = ', aips2sfxc[-1]
  
  #write the BP to disk
  global_header = array([nstation, nif, npol, nchan], dtype='int32')
  global_header.tofile(outfile)
  for ant_no, ant in enumerate(bp_table):
    header = array([aips2sfxc[ant_no]], dtype='int32')
    header.tofile(outfile)
    ant[0].tofile(outfile)
    if npol == 2:
      ant[1].tofile(outfile)

def get_bp(uvdata,outname,isKJCC):
  bp = uvdata.table('BP', 0)
  print 'Getting BP table version %02d' % (bp.version)
  nstation = bp.keywords['NO_ANT']
  nif = bp.keywords['NO_IF']
  npol = bp.keywords['NO_POL']
  nchan = bp.keywords['NO_CHAN']
  outfile = open(outname+'.bp', 'w')
  bp_table = zeros([nstation, npol, nchan*nif], dtype=complex64)
  weights = zeros([nstation, npol, nchan*nif], dtype=float32)
  #pdb.set_trace()
  # Read the bp table and do weighted sum of all bandpasses
  for row in bp:
    ant_no = row.antenna-1
    for i in range(nif):
      for c in range(nchan):
        j = i*nchan + c
        if not(type(row.weight_1) is list):
          if row.weight_1 > 0.01:
            if row.real_1[j] < 20.:
              weights[ant_no][0][j] += row.weight_1
              bp_table[ant_no][0][j] += (row.real_1[j] + 1j*row.imag_1[j])*row.weight_1
          if (npol ==2) and (row.weight_2 > 0.01):
            if row.real_2[j] < 20.:
              weights[ant_no][1][j] += row.weight_2
              bp_table[ant_no][1][j] += (row.real_2[j] + 1j*row.imag_2[j])*row.weight_2
        else:
          if row.weight_1[i] > 0.01:
            if row.real_1[j] < 20.:
              weights[ant_no][0][j] += row.weight_1[i]
              bp_table[ant_no][0][j] += (row.real_1[j] + 1j*row.imag_1[j])*row.weight_1[i]
          if (npol ==2) and (row.weight_2[i] > 0.01):
            if row.real_2[j] < 20.:
              weights[ant_no][1][j] += row.weight_2[i]
              bp_table[ant_no][1][j] += (row.real_2[j] + 1j*row.imag_2[j])*row.weight_2[i]
    if (ant_no == 3):
      #print row.time,' : ',weights[4][0][0:32]#,'\n', bp_table[4][0][0:32],'\n',row.weight_1
      print row.time,' : ', row.weight_1
  #pdb.set_trace()
  # normalize bandpass
  bp_table /= (weights + 1e-8)

  #A table to convert the AIPS station numbers to SFXC station number
  aips2sfxc = []
  aips_antennas = uvdata.antennas
  if isKJCC:
    aips_antennas = [KJCC_3to2letter_names[sID] for sID in aips_antennas]
    print ' KJCC correlator data : changed AIPS names from %s to %s' % (uvdata.antennas,aips_antennas)
  sfxc_antennas = sorted(aips_antennas)
  print 'Antennas:\n  AIPS=%s\n  SFXC=%s' % (aips_antennas, sfxc_antennas)
  for ant in aips_antennas:
    aips2sfxc.append(sfxc_antennas.index(ant))
    print 'ant = ', ant, ', idx = ', aips2sfxc[-1]
  for ii in range(len(aips2sfxc)):
    jj = aips2sfxc[ii]
    print 'AIPS ant#%d (%s) == SFXC ant#%d (%s)' % (ii+1, aips_antennas[ii], jj, sfxc_antennas[jj])
  
  #write headers to disk
  global_header = array([nstation, nif, npol, nchan], dtype='int32')
  global_header.tofile(outfile)
  # Write the IF's to disk
  fq = uvdata.table('FQ', 0)
  f0 = uvdata.header.crval[2] # The reference frequency

  if not(type(fq[0]) is list):
    frequencies = around(array([f0 + fq[0].if_freq], dtype ='float64'))
  else:
    frequencies = around(array([f + f0 for f in fq[0].if_freq], dtype ='float64'))
  frequencies.tofile(outfile)

  # Store the bandwidths
  bandwidths = array(fq[0].total_bandwidth, dtype='float64')
  bandwidths.tofile(outfile)
  print frequencies
  print bandwidths
  #write the bandpasses to disk
  for ant_no, ant in enumerate(bp_table):
    #pdb.set_trace()
    header = array([aips2sfxc[ant_no]], dtype='int32')
    header.tofile(outfile)
    ant[0].tofile(outfile)
    if npol == 2:
      ant[1].tofile(outfile)

def mjd2date(mjd):
  # algorithm taken from wikipedia
  J = int(mjd + 2400001)
  j = J + 32044
  g = j / 146097; dg = j % 146097
  c = (dg / 36524 + 1) * 3 / 4; dc = dg - c*36524
  b = dc/1461 ;  db = dc % 1461;
  a = (db / 365 + 1) * 3 / 4 ; da = db-a*365
  y = g*400 + c*100 + b*4 + a
  m = (da*5 + 308)/153 - 2
  d = da-(m + 4)*153/5 + 122
  Y = y-4800 + (m + 2) / 12
  M = (m + 2) % 12 + 1
  D = d + 1
  return Y,M,D

def mjd(day, month, year):
  a = (14-month)/12;
  y = year + 4800 - a;
  m = month + 12*a - 3;
  jdn = day + ((153*m+2)/5) + 365*y + (y/4) - (y/100) + (y/400) - 32045
  return int(jdn - 2400000.5)

###############################################3
######
###### 

outname, uvdata, isKJCC = parse_cli_args()
#get_cl(uvdata)
get_bp(uvdata, outname, isKJCC)
