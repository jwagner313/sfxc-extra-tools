{
    "exper_name": "r14018a", 
    "cross_polarize": false, 
    "message_level": 10,
    "number_channels": 1024,
    "fft_size_delaycor": 1024,
    "window_function": "NONE",
    "subbands": [{"bandwidth": 256000000.0, "frequency": 22056000000.0, "sideband": "LSB", "polarisation": "L"}],
    "channels": ["CH01"],
    "integr_time": 1.0, 
    "output_file": "file:///home/jwagner/correlation/sfxc-experiments/r14018a-pulsar/r14018a/r14018a_trunk.cor",
    "stations": [ "Kt", "Ku", "Ky", "Vm", "Vr", "Vo", "Vs" ],
    "data_sources": {
        "Kt": [ "file:///scratch0/oper/jw2/r14018a/TN/r14018a_KTN_No0001_No0010" ],
        "Ku": [ "file:///scratch0/oper/jw2/r14018a/US/r14018a_KUS_No0001_No0010" ],
        "Ky": [ "file:///scratch0/oper/jw2/r14018a/YS/r14018a_KYS_No0001_No0010" ],
        "Vm": [ "file:///scratch0/oper/jw2/r14018a/MIZ/r14018a_MIZ_No0001_No0010" ],
        "Vr": [ "file:///scratch0/oper/jw2/r14018a/IRK/r14018a_IRK_No0001_No0010" ],
        "Vo": [ "file:///scratch0/oper/jw2/r14018a/OGA/r14018a_OGA_No0001_No0010" ],
        "Vs": [ "file:///scratch0/oper/jw2/r14018a/ISG/r14018a_ISG_No0001_No0010" ]
    },
    "site_position": {
        "Vm": [-3857241.8226999999, 3108784.9037000001, 4003900.5591000002],
        "Vo": [-4491068.8940000003, 3481544.8295, 2887399.6227000002],
        "Vr": [-3521719.5687000002, 4132174.7527999999, 3336994.3254999998],
        "Vs": [-3263994.8821, 4808056.3607999999, 2619949.1356000002],
        "Ku": [-3287268.6186000002, 4023450.1798999999, 3687380.0197999999],
        "Kt": [-3171731.5580000002, 4292678.4878000002, 3481038.7252000002],
        "Ky": [-3042280.9136999999, 4045902.7163999998, 3867374.3544000001]
    },
    "start": "2014y017d23h59m55s", 
    "stop": "2014y018d00h13m56s", 
    "delay_directory": "file:///home/jwagner/correlation/sfxc-experiments/r14018a-pulsar/delay_data_trunk/"
}
