{
    "exper_name": "r14018a",
    "cross_polarize": false,
    "message_level": 10,
    "number_channels": 1024,
    "fft_size_delaycor": 1024,
    "window_function": "NONE",
    "subbands": [{"bandwidth": 256000000.0, "frequency": 22056000000.0, "sideband": "LSB", "polarisation": "L"}],
    "channels": ["CH01"],
    "integr_time": 0.001,
    "phased_array": true,
    "pulsars": "B2021+51",
    "coherent_dedispersion": false,
    "output_file": "file:///home/jwagner/correlation/sfxc-experiments/r14018a-pulsar/r14018a/r14018a.phased_1ms.cor",
    "stations": [ "Kt", "Vm", "Vs" ],
    "data_sources": {
        "Kt": [ "file:///scratch0/oper/jw2/r14018a/TN/r14018a_KTN_No0001_No0010" ],
        "Vm": [ "file:///scratch0/oper/jw2/r14018a/MIZ/r14018a_MIZ_No0001_No0010" ],
        "Vs": [ "file:///scratch0/oper/jw2/r14018a/ISG/r14018a_ISG_No0001_No0010" ]
    },
    "site_position": {
        "Vm": [-3857241.8226999999, 3108784.9037000001, 4003900.5591000002], 
        "Vs": [-3263994.8821, 4808056.3607999999, 2619949.1356000002], 
        "Kt": [-3171731.5580000002, 4292678.4878000002, 3481038.7252000002]
    },
    "reference_station": "Vm",
    "start": "2014y018d00h05m40s",
    "stop": "2014y018d00h13m14s",
    "delay_directory": "file:///home/jwagner/correlation/sfxc-experiments/r14018a-pulsar/delay_data_phased_1ms/",
    "cl_table": "file:///home/jwagner/correlation/sfxc-experiments/r14018a-mat/r14018a.full.cl",
    "bp_table": "file:///home/jwagner/correlation/sfxc-experiments/r14018a-mat/r14018a.full.bp"
}
