#!/bin/bash
#
# Usage:    SFXC-env.sh <trunk|3.2|phased>
#
# Sets up environment variables required for SFXC.
# Derived from information at http://www.jive.nl/~kettenis/sfxc.html.
# Assumes several SFXC variants are available on your system,
#   SFXC version 3.2          under  $HOME/code/sfxc-3.2/install/ 
#   SFXC trunk                under  $HOME/code/sfxc-trunk/install/ 
#   SFXC phased array branch  under  $HOME/code/sfxc-phased/install/ 
#
# You may add this to your ~/.bashrc file:
#   alias dstrunk='source ~/SFXC-env.sh trunk'
#   alias dsphased='source ~/SFXC-env.sh phased'
# Then you can just call for example 'dstrunk' from the command prompt to
# set up paths required for using the trunk version of SFXC.
#

if [ "$1" != "trunk" ] && [ "$1" != "3.2" ] && [ "$1" != "phased" ] ; then
   echo "Usage: SFXC-env.sh <trunk|3.2|phased>"
   return
fi

# Home directory
export SFXC_DIR=$HOME/code/sfxc-$1/install/
export SFXC_SRC=$HOME/code/sfxc-$1/

# CALC10 directory
# Location of the JPL Solar System Ephemeris (DE405_le.jpl), ocean loading
# information (ocean.dat) and antenna tilt (tilt.dat) files.
#
# The 3 files are identical between SFXC 3.2 and SFXC Trunk of Aug2014
export CALC_DIR=$HOME/data/calc10/

# Derived Env vars
export PATH_SFXC_OLD=$PATH
export PATH=$PATH:$SFXC_DIR/bin/
export PYTHONPATH=$PYTHONPATH:$SFXC_SRC/sfxc/lib/vex_parser/install/lib64/python2.6/site-packages/
export PYTHONPATH=$PYTHONPATH:$SFXC_SRC/vex/
export PYTHONPATH=$PYTHONPATH:$HOME/install//lib/python2.6/site-packages/  # contains module ply-3.4 and perhaps others

echo "The SFXC env ($SFXC_DIR) has been set up for '$1'."

