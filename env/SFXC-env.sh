#!/bin/bash

# derived from information on http://www.jive.nl/~kettenis/sfxc.html


if [ "$1" != "trunk" ] && [ "$1" != "3.2" ] && [ "$1" != "phased" ] ; then
   echo "Usage: SFXC-env.sh <trunk|3.2|phased>"
   return
fi

# Home directory
export SFXC_DIR=/home/jwagner/code/sfxc-$1/install/
#export SFXC_DIR=/home/jwagner/code/sfxc-trunk/install/

# CALC10 directory
# Location of the JPL Solar System Ephemeris (DE405_le.jpl), ocean loading
# information (ocean.dat) and antenna tilt (tilt.dat) files.
#
# The 3 files are identical between SFXC 3.2 and SFXC Trunk of Aug2014
export CALC_DIR=/home/jwagner/data/calc10/

# Derived Env vars
export PATH_SFXC_OLD=$PATH
export PATH=$PATH:$SFXC_DIR/bin/
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/oper/jwagner/sfxc-$1/sfxc/lib/
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/rdodson/Libraries/
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/share/apps/fftw/lib/


echo "The SFXC env ($SFXC_DIR) has been set up for '$1'."

