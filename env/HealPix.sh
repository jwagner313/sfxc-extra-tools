
export HEALPIX=/home/jwagner/code/Healpix_3.11/
export HEALPIX_TARGET=basic_gcc

# and also see 
#  .profile
# where config is loaded via
#  .  /home/jwagner/.healpix/3_11_Linux/config
# Well, can re-do that load here as well:

. /home/jwagner/.healpix/3_11_Linux/config

export PATH=$PATH:/home/jwagner/code/Healpix_3.11/bin/

