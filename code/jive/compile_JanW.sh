
# Compile settings and environment used in my own (JanW) compile 
# of all of this JIVE JCCS / Casa Core 1.5.0 / SFXC extra tools mess

export ROOT=/home/jwagner/code/sfxc-extra-tools/code/jive/

ln -sfn ${ROOT}/implement ${ROOT}/jive

cp ${ROOT}/system/locations.mak.JanW ${ROOT}/system/locations.mak
cp ${ROOT}/system/compiler.mak.JanW ${ROOT}/system/compiler.mak

cd ${ROOT}/implement
make -j8 -f Makefile clean
# make -j8 -f Makefile # parallel build often fails!
make -f Makefile

cd ${ROOT}/apps/
make -j8 -f Makefile clean
make -j8 -f Makefile

# or manual:
# cd /home/jwagner/code/sfxc-extra-tools/code/jive/apps/j2ms2/
# /usr/bin/g++ -m64 -L/home/jwagner/install/lib  -o /home/jwagner/code/sfxc-extra-tools/code/jive//bin/x86_64-64/j2ms2 -L/home/jwagner/install/ -L/lib -L/home/jwagner/code/sfxc-extra-tools/myvex -L/usr/lib -L/home/jwagner/code/sfxc-extra-tools/code/jive//lib/x86_64-64 -L/usr/lib -Xlinker -rpath -Xlinker /home/jwagner/install/ -Xlinker -rpath -Xlinker /lib -Xlinker -rpath -Xlinker /home/jwagner/code/sfxc-extra-tools/myvex -Xlinker -rpath -Xlinker /usr/lib -Xlinker -rpath -Xlinker /home/jwagner/code/sfxc-extra-tools/code/jive//lib/x86_64-64 -Xlinker -rpath -Xlinker /home/jwagner/install/lib -Xlinker -rpath -Xlinker /usr/lib /home/jwagner/code/sfxc-extra-tools/code/jive/apps/j2ms2/Repos/x86_64-64/./j2ms2.o -lj2ms -llabels -lUtilities -lcasa_measures -lcasa_msfits -lcasa_tables -lcasa_coordinates -lcasa_fits -lcasa_images -lcasa_lattices -lcasa_mirlib -lcasa_ms -lcasa_scimath -lcasa_scimath_f -lcasa_components -lcasa_casa -lfftw3 -lfftw3f -lvex -lfl -ldl -lbsd 


# the above Makefiles lack a 'make install', 
# do it manually:

cp ./bin/x86_64-64/j2ms2 ~/bin/
cp ./bin/x86_64-64/tConvert ~/bin/

