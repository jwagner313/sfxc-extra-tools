//
//  AmppTimePlotter - Specialization of the PloterIF-class:
//
//  This one makes plots of amp/time... (sheeeee)
//
//
#ifndef AMPTIMEPLOTTER_H
#define AMPTIMEPLOTTER_H

#include <jive/Plotters/PlotterIF.h>
#include <casa/Containers/RecordDesc.h>


class AmpTimePlotter :
	public PlotterIF
{
public:

	//
	//  Construct an AmpTimePlotter object...
	//	
	AmpTimePlotter();


	virtual casa::Record plotResult( const casa::Table& tab, casa::Float wt, 
						       const casa::String& wtcolName,
						       const casa::String& datacolName,
							   const casa::Vector<casa::Bool>& newplot,
							   const casa::Vector<casa::Int>& polSel,
							   const casa::Vector<casa::Int>& chanSel,
							   PlotterIF::AvgMethod timeAvg,
							   PlotterIF::AvgMethod chanAvg ) const;


	virtual casa::Block<PlotterIF::Axis>   bypassAxes( void ) const;

	virtual ~AmpTimePlotter();

private:
	//
	//  Nothing (yet)
	//
	casa::RecordDesc      myRetvalLayout;

	//
	//  Prohibit these:
	//
	AmpTimePlotter( const AmpTimePlotter& );
	AmpTimePlotter& operator=( const AmpTimePlotter& );
};



#endif
