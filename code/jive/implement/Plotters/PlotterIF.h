//
//  PlotterIF - Define the (virtual) base class (i.e. interface) for plotter
//  classes....
//
//  Author:  Harro Verkouter, 7-12-2000
//
//  $Id: PlotterIF.h,v 1.4 2006/02/10 08:53:42 verkout Exp $
//
//  $Log: PlotterIF.h,v $
//  Revision 1.4  2006/02/10 08:53:42  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.3  2006/01/13 11:35:35  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.2  2003/09/12 07:19:56  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.1  2001/05/30 11:40:51  verkout
//  HV: First iteration of the standardplots in C++. Ported glish stuff to C++.
//
//
#ifndef PLOTTERIF_H
#define PLOTTERIF_H

#include <casa/BasicSL/String.h>
#include <casa/Arrays/Array.h>
#include <casa/Containers/Record.h>

#include <iostream>



casa::Bool shapeDataEtcEqual( casa::IPosition& ipos,
							  casa::Array<casa::Complex>& data,
							  casa::Array<casa::Float>& weight,
						      casa::Array<casa::Bool>& flags,
							  casa::Float wt, const casa::Table& tab, 
						      const casa::String& wtcolName,
							  const casa::String& datacolName );

casa::RecordDesc makeDataIndexLayout( void );
casa::RecordDesc makePlotResultLayout( void );


casa::String dataColumnName( const casa::Table& tab );



class PlotterIF
{
public:
	//
	//  Defines the structure for a plotResult subrecord
	//  Plotters could return multiple results. In that case,
	//  the returned record contains multiple sub-records with the 
	//  PlotResult-structure.
	//
	//  If a plotter returns just one result, they should create a record
	//  with one sub-record with the PlotResult structure..
	//
	//  Tip: the fieldname(s) of the 'plotResult' subrecords could be used to 
	//  identify the quantity that that dataset represents... eg 'amplitude'
	//  'phase' or 'StokesI' or whatever!
	//
	static casa::RecordDesc  PlotResult;

	//
	//
	//  Available standard axes, which can be used to iterate over
	//  (or not)
	//
	//  Note: if you add/remove entries or change the order, the only
	//        thing(s) you'll need to do are:
	//
	//        1) Verify that the labDataType for the axis is ok (see
	//           PlotterIF.cc). In case of addition, add an entry for
	//           this new axis, in case of reordering: make sure the 
	//           labDataType is reordered as well
	//
	//        2) Check the axisToString and stringToAxis funcions in
	//           case of addition/removal. In case of reordering nothing
	//           needs to be done in these functions.
	//
	//
	typedef enum _Axis {
		P=0, CH=1, SB=2, FQ=3, BL=4, SRC=5, TIME=6,
		DATAAXES=7, TYPE=7, NUMAXES=8, NONE
	} Axis;	


	//
	//  Available averaging methods for the data (note: for 'real' data there is
	//  no difference between scalar/vector but for complex data there is.
	//
	typedef enum _AvgMethod {
		avgNone, avgScalar, avgVector
	} AvgMethod;

	//
	// Keep track of what datatype the indices are. Use PlotterIF::Axis as index.
	//
	static const casa::DataType    labDataType[];


	//
	//  Define translation to/from string and enums in this class
	//
	static casa::String               axisToString( PlotterIF::Axis axis );
	static PlotterIF::Axis      stringToAxis( const casa::String& str );

	static casa::String               avgToString( PlotterIF::AvgMethod method );
	static PlotterIF::AvgMethod stringToAvg( const casa::String& str );



	//
	//  Plotter machine -> from a name, return the correct plotter
	//  
	//  Returns a null-pointer is not recognized..
	//
	//  Caller should *not* delete the storage...
	//
	static const PlotterIF*      makePlotter( const casa::String& plotterId );


	

	//
	//  C'tor.. takes as argument the 'name' of the plotter...
	//  Is used for identification...
	//	
	PlotterIF( const casa::String& plotterid );


	//
	//  Return the ID
	//
	const casa::String&  getID( void ) const;


	//
	//  This will be *the* function! 
	//
	//  The plotter is requested to turn the data+flags+weight+index stuff
	//  into a plotresult!
	//
	//  The plotresult being two vectors:
	//    1) an array representing th x-loci of the result
	//    2) an array representing the corresponding y-values
	//
	virtual casa::Record     plotResult( const casa::Table& tab, casa::Float wt, 
								   const casa::String& wtcolName,
								   const casa::String& datacolName,
								   const casa::Vector<casa::Bool>& newplot,
								   const casa::Vector<casa::Int>& polSel,
								   const casa::Vector<casa::Int>& chanSel,
								   PlotterIF::AvgMethod timeAvg,
								   PlotterIF::AvgMethod chanAvg ) const = 0;

	//
	//  List which axes should be bypassed (if any). E.g. If you want to 
	//  plot stokes I you need both parrallel polarizations. With this
	//  method you can tell the system to bypass the polarization axis 
	//  for iteration -> you get a chunk of data with all polarizations....
	//  Or, when wanting to plot the delay, you want all the channels and
	//  you do'nt want to iterate over them...
	//
	virtual casa::Block<PlotterIF::Axis> bypassAxes( void ) const = 0;

	//
	//  Virtual destructor
	//
	virtual ~PlotterIF();

private:

	//
	//  Remeber the plotter ID...
	//
	casa::String    myID;
};


//
// Global functions for generation of plot- and dataset name generation.
//
casa::String makePlotName( const casa::Record& cur,
						   const casa::Vector<casa::Bool>& newplot,
					 	   PlotterIF::Axis xaxis );
casa::String makeDatasetName( const casa::Record& cur,
							  const casa::Vector<casa::Bool>& newplot,
							  PlotterIF::Axis xaxis );

//
// Handy global functions to insert stuff enumerated values in HRF (Human Readable
// Format) into a stream. The attentive reader may realise that these are just 
// convenience wrappers around the static functions defined in the interface of
// PlotterIF... :)
//
std::ostream& operator<<( std::ostream& os, PlotterIF::Axis ax );
std::ostream& operator<<( std::ostream& os, PlotterIF::AvgMethod am );

#endif
