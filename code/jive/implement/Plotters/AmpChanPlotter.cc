//
//  Implementation of the AmpChanPlotter....
//
// 
//
#include <jive/Plotters/AmpChanPlotter.h>

#include <casa/Arrays/Vector.h>
#include <casa/Arrays/ArrayUtil.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/Slicer.h>
#include <tables/Tables.h>

#include <casa/Containers/RecordField.h>
#include <tables/Tables/ScalarColumn.h>

#include <jive/ms2uvfitsimpl/DumpRecord.h>

#include <casa/BasicSL/Constants.h>

#include <iomanip>
#include <sstream>

using namespace std;
using namespace casa;

AmpChanPlotter::AmpChanPlotter() :
	PlotterIF( "ampchan" )
{
	//
	//  Define the retval-layout
	//
	myRetvalLayout.addField( "amplitude", PlotterIF::PlotResult );
}

Block<PlotterIF::Axis>   AmpChanPlotter::bypassAxes( void ) const
{
	//return Block<PlotterIF::Axis>();
	Block<PlotterIF::Axis>     retval(1);
		
	retval[0] = PlotterIF::TIME;
	return retval;
}


Record AmpChanPlotter::plotResult( const Table& tab, Float wt,
								   const String& wtcolName,
								   const String& datacolName,
								   const Vector<Bool>& newplot,
								   const Vector<Int>& polSel,
								   const Vector<Int>& chanSel,
								   PlotterIF::AvgMethod timeAvg,
								   PlotterIF::AvgMethod) const
{
	//
	//  Some static stuff
	//
	static String                 plotName;
	static String                 datasetName;
	static Record                 emptyRec;
	static Record                 dataIndex( ::makeDataIndexLayout() );
	static IPosition              theShape;
	static Array<Bool>            theFlags;
	static Array<Float>           theWeight;
	static Array<Complex>         theData;
	static RecordFieldPtr<Int>    p( dataIndex, "P" );
	static RecordFieldPtr<Int>    ch( dataIndex, "CH" );
	static RecordFieldPtr<Int>    sb( dataIndex, "SB" );
	static RecordFieldPtr<Int>    fq( dataIndex, "FQ" );
	static RecordFieldPtr<Int>    src( dataIndex, "SRC" );
	static RecordFieldPtr<Int>    bl( dataIndex, "BL" );
	static RecordFieldPtr<Double> time( dataIndex, "TIME" );
	static RecordFieldPtr<String> type( dataIndex, "TYPE" );


	Record                            pr;
	IPosition                         shp;
	IPosition                         resshp;
	Array<Double>                     avgarg;
	Array<Complex>                    avgcmpl;
	Vector<Double>                    phases;
	Vector<Double>                    channels;
	ROScalarColumn<Int>               ant1( tab, "ANTENNA1" );
	ROScalarColumn<Int>               ant2( tab, "ANTENNA2" );
	ROScalarColumn<Int>               dd( tab, "DATA_DESC_ID" );
	ROScalarColumn<Int>               fld( tab, "FIELD_ID" );
	ROScalarColumn<Double>            timecol( tab, "TIME" );
	RecordFieldPtr<String>            comment;
	RecordFieldPtr<Array<Double> >    xptr;
	RecordFieldPtr<Array<Double> >    yptr;


	// 
	// Fill in stuff that will not change 
	// 
	*type = "amplitude";
	*ch   = 0;

	Int           npol;
	Int           nchan;
	Int           curpol;
	Int           curchan;
	Bool          doAvg( timeAvg!=PlotterIF::avgNone );
	Double        firstTime = 0.0;
	Double        lastTime = 0.0;
	IPosition     idx(1,0);
	Vector<uLong> nrInt;

	polSel.shape( npol );
	chanSel.shape( nchan );

	nrInt.resize( IPosition(1,npol) );
	phases.resize( IPosition(1,nchan) );
	channels.resize( IPosition(1,nchan) );

	//
	// already convert the (Int) channels to (Double)
	//
	for( Int tt=0; tt<nchan; tt++ )
	{
		channels( tt ) = (Double)(chanSel(tt));
	}

	//
	// Initialize integrationcounters to 0... (guess how I found out I had those
	//   forgotten...)
	// 
	nrInt = 0L;

	//
	//  Resize averaging tmp-storage (if necessary)
	//
	if( doAvg )
	{
		//
		//  Dang! We need to do stuff...
		//
		//  Lets allocate buffers. If we're doing vector average we 
		//  need a tmp complex array 'cuz we just add up all the complex
		//  numbers. If we're doing scalar average we only need an array
		//  of doubles since we'll be averaging the args straightaway.
		//
		//  Hence: in the case of Vector averaging we will allocate an array
		//  of doubles as well: this array will contain all the values we want
		//  to plot (in the end).
		//
		switch( timeAvg )
		{
			case PlotterIF::avgVector:
				avgcmpl.resize( IPosition(2,npol,nchan) );
				avgcmpl = Complex(0.0, 0.0);

			case PlotterIF::avgScalar:
				avgarg.resize( IPosition(2,npol,nchan) );
				avgarg = (Double)0.0;
				break;

			default:
				break;
		}
		
		//
		//  We know we have to time-average. Let's save the
		//  first time in the data for future reference.
		//
		firstTime = timecol(0);
	}

	//
	//  Get the data/weight etc. equal and get the flags as well...
	//
	if( !::shapeDataEtcEqual(theShape, theData, theWeight, theFlags,
							 wt, tab, wtcolName, datacolName) )
	{
		return emptyRec;
	}

	if( theShape.nelements()==0 )
	{
		return emptyRec;
	}

	//
	//  Now loop over the rows in the table
	//
	for( uInt row=0; row<tab.nrow(); row++ )
	{
		for( Int cp=0; cp<npol; cp++ )
		{
			Int     cc;

			curpol  = polSel(cp);

			for( cc=0; cc<nchan; cc++ )
			{	
				curchan = chanSel(cc);
			
				if( theFlags(IPosition(3,curpol,curchan,row)) )
				{
					break;
				}
				//
				//  save some CPUcycles if we're averaging
				//   (in that case we do not have to calc the phase yet)
				//
				if( doAvg )
				{
					continue;
				}
				idx(0) = cc;
				phases( idx )   = abs( theData(IPosition(3,curpol,curchan,row)) );
			}
		
			if( cc<nchan )
			{
				// at least one of the selected channels was flagged as having
				// a weight below the limit
				continue;
			}

			//
			//  We have verified that all channels satisfy the weight-criterium
			//
			//  If we're integrating we first add up all the data in the
			//  tmp array...
			//
			if( doAvg )
			{
				switch( timeAvg )
				{
					case PlotterIF::avgVector:
						for( Int cnt=0; cnt<nchan; cnt++ )
						{
							avgcmpl( IPosition(2,cp,cnt) ) =
								theData( IPosition(3,curpol,chanSel(cnt),row) );
						}
						nrInt(cp)++;
						break;

					case PlotterIF::avgScalar:
						for( Int cnt=0; cnt<nchan; cnt++ )
						{
							Complex&    data = 
										 theData(IPosition(3,curpol,chanSel(cnt),row)); 
							IPosition   dst(2,cp,cnt);
								
							avgarg( dst ) += abs(data);
						}
						nrInt(cp)++;
						break;

					default:
						break;
				}
				//
				// We have processed yet another row. Let's save the timevalue
				// for future reference.
				//
				lastTime = timecol(row);
				continue;
			}

			//
			//  *if* we end up here we may be sure that the following
			//  is true:
			//
			//  1) doAvg is False, in which case we find the desired plotdata
			//     in the 'phases' array and no further processing is required
			//
			*p    = curpol;
			*sb   = dd(row);
			*fq   = 0;
			*src  = fld(row);
			*bl   = 1000*ant1(row)+ant2(row);
			*time = timecol(row);

			plotName    = ::makePlotName( dataIndex, newplot, PlotterIF::CH );
			datasetName = ::makeDatasetName( dataIndex, newplot, PlotterIF::CH );

			if( !pr.isDefined(plotName) )
			{
				pr.defineRecord(plotName, Record() );
			}
			Record&   recref( pr.rwSubRecord(plotName) );
			
			if( !recref.isDefined(datasetName) )
			{
				recref.defineRecord(datasetName, Record(::makePlotResultLayout()) );
			}
		
			Record&   dsref( recref.rwSubRecord(datasetName) );

			xptr.attachToRecord( dsref, "xval" );
			yptr.attachToRecord( dsref, "yval" ); 

			Array<Double>    resx( ::concatenateArray(*xptr, channels) );
			Array<Double>    resy( ::concatenateArray(*yptr, phases) );

			(*xptr).reference( resx );
			(*yptr).reference( resy );
		}
	}

	//
	//  Finished processing all rows in the subtable
	//  Now we find out if we need to do some processing and plotting
	//  (as is the case when we are timeaveraging). 
	//
	if( !doAvg )
	{
		//
		// Nothing particular to do but to return the results we have
		// produced so far.....
		// 
		return pr;
	}

	//
	//  Depending on the type of averaging we need to produce stuff...
	//
	//  First let's loop over the polarizations to produce a final 
	//  array of complex values. After that we will determine the 
	//  args of those numbers.
	//
	switch( timeAvg )
	{
		case PlotterIF::avgVector:
			//
  			//  Loop over the pols and determine the average
			//
			for( Int cp=0; cp<npol; cp++ )
			{
				if( !nrInt(cp) )
				{
					continue;
				}
		
				IPosition      begin( 2, cp, 0 );
				IPosition      end( 2, cp, nchan-1 );
				IPosition      inc( 2, 1, 1 );
				Array<Complex> thispol( avgcmpl(begin, end, inc) );

				thispol /= Complex( (Float)(nrInt(cp)), 0.0 );

				//
				//  Ok. The average complex numbers for this pol
				//  have been determined. Now calc the args!
				//
				IPosition      indx(2, cp, 0);

				for( indx(1)=0; indx(1)<nchan; indx(1)++ )
				{
					avgarg( indx ) = abs( avgcmpl(indx) );
				}
			}
			break;

		case PlotterIF::avgScalar:
			for( Int cp=0; cp<npol; cp++ )
			{
				if( !nrInt(cp) )
				{
					continue;
				}
		
				IPosition      begin( 2, cp, 0 );
				IPosition      end( 2, cp, nchan-1 );
				IPosition      inc( 2, 1, 1 );
				Array<Double>  thispol( avgarg(begin, end, inc) );

				//
				// Deal with the real
				//
				thispol /= ((Double)nrInt(cp));
				// done...
			}
			break;

		default:
			//
			//  Error?
			//
			return pr;
	}

	//
	//  *phew* now we can loop over the pols and 
	//  make the plots!
	//
	String        commentstr;
	ostringstream strm;

	for( Int cp=0; cp<npol; cp++ )
	{
		//
		//  If no integrations, nothing to do....
		//
		if( !nrInt(cp) )
		{
			continue;
		}

		strm << timeAvg << "averaged " << nrInt(cp) << " integrations from " 
			 << setw(15) << setprecision(15) << firstTime 
			 << " to " 
			 << setw(15) << setprecision(15) << lastTime << "\n";


		curpol = polSel(cp);

		*p    = curpol;
		*sb   = dd(0);
		*fq   = 0;
		*src  = fld(0);
		*bl   = 1000*ant1(0)+ant2(0);
		*time = (firstTime+lastTime)/2.0;

		plotName    = ::makePlotName( dataIndex, newplot, PlotterIF::CH );
		datasetName = ::makeDatasetName( dataIndex, newplot, PlotterIF::CH );

		if( !pr.isDefined(plotName) )
		{
			pr.defineRecord(plotName, Record() );
		}
		Record&   recref( pr.rwSubRecord(plotName) );
			
		if( !recref.isDefined(datasetName) )
		{
			recref.defineRecord(datasetName, Record(::makePlotResultLayout()) );
		}
		
		Record&   dsref( recref.rwSubRecord(datasetName) );

		xptr.attachToRecord( dsref, "xval" );
		yptr.attachToRecord( dsref, "yval" ); 
		comment.attachToRecord( dsref, "comment" );

		//
		//  Retrieve the right piece(slice) of data
		//	
		IPosition      begin( 2, cp, 0 );
		IPosition      end( 2, cp, nchan-1 );
		IPosition      inc( 2, 1, 1 );
		Array<Double>  tmp;
	
		tmp = avgarg(begin, end, inc);

		Array<Double>    resx( ::concatenateArray(*xptr, channels) );
		Array<Double>    resy( ::concatenateArray(*yptr, tmp.reform(IPosition(1,nchan))) );

		//
		//  Update contents of the returnvalue
		//
		(*xptr).reference( resx );
		(*yptr).reference( resy );

		commentstr = strm.str();
		if( (*comment)!=commentstr )
		{
			(*comment) = (*comment)+commentstr;
		}
	}
	return pr;	
}


AmpChanPlotter::~AmpChanPlotter()
{
}
