//
//  AmppTimePlotter - Specialization of the PloterIF-class:
//
//  This one makes plots of amp/time... (sheeeee)
//
//
#ifndef PHACHANPLOTTER_H
#define PHACHANPLOTTER_H

#include <jive/Plotters/PlotterIF.h>
#include <casa/Containers/RecordDesc.h>


class PhaChanPlotter :
	public PlotterIF
{
public:

	//
	//  Construct an PhaChanPlotter object...
	//	
	PhaChanPlotter();


	virtual casa::Record plotResult( const casa::Table& tab, casa::Float wt, 
						       const casa::String& wtcolName,
						       const casa::String& datacolName,
							   const casa::Vector<casa::Bool>& newplot,
							   const casa::Vector<casa::Int>& polSel,
							   const casa::Vector<casa::Int>& chanSel,
							   PlotterIF::AvgMethod timeAvg,
							   PlotterIF::AvgMethod chanAvg ) const;

	virtual casa::Block<PlotterIF::Axis>   bypassAxes( void ) const;

	virtual ~PhaChanPlotter();

private:
	//
	//  Nothing (yet)
	//
	casa::RecordDesc      myRetvalLayout;

	//
	//  Prohibit these:
	//
	PhaChanPlotter( const PhaChanPlotter& );
	PhaChanPlotter& operator=( const PhaChanPlotter& );
};



#endif
