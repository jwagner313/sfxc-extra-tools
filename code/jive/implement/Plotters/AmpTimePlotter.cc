//
//  Implementation of the AmpTimePlotter....
//
// 
//
#include <jive/Plotters/AmpTimePlotter.h>

#include <casa/Arrays/Vector.h>
#include <casa/Arrays/ArrayUtil.h>
#include <tables/Tables.h>

#include <casa/Containers/RecordField.h>
#include <tables/Tables/ScalarColumn.h>

#include <jive/ms2uvfitsimpl/DumpRecord.h>

#include <sstream>

using namespace std;
using namespace casa;

AmpTimePlotter::AmpTimePlotter() :
	PlotterIF( "amptime" )
{
	//
	//  Define the retval-layout
	//
	myRetvalLayout.addField( "amplitude", PlotterIF::PlotResult );
}

Block<PlotterIF::Axis>   AmpTimePlotter::bypassAxes( void ) const
{
	Block<PlotterIF::Axis>   retval( 1 );
	
	retval[0] = PlotterIF::TIME;
	return retval;
//	return Block<PlotterIF::Axis>();
}


Record AmpTimePlotter::plotResult( const Table& tab, Float wt,
								   const String& wtcolName,
								   const String& datacolName,
								   const Vector<Bool>& newplot,
								   const Vector<Int>& polSel,
								   const Vector<Int>& chanSel,
								   PlotterIF::AvgMethod ,
								   PlotterIF::AvgMethod chanAvg ) const
{
	//
	//  Some static stuff
	//
	static String                 plotName;
	static String                 datasetName;
	static Record                 emptyRec;
	static Record                 dataIndex( ::makeDataIndexLayout() );
	static IPosition              theShape;
	static Array<Bool>            theFlags;
	static Array<Float>           theWeight;
	static Array<Complex>         theData;
	static RecordFieldPtr<Int>    p( dataIndex, "P" );
	static RecordFieldPtr<Int>    ch( dataIndex, "CH" );
	static RecordFieldPtr<Int>    sb( dataIndex, "SB" );
	static RecordFieldPtr<Int>    fq( dataIndex, "FQ" );
	static RecordFieldPtr<Int>    src( dataIndex, "SRC" );
	static RecordFieldPtr<Int>    bl( dataIndex, "BL" );
	static RecordFieldPtr<Double>  time( dataIndex, "TIME" );
	static RecordFieldPtr<String> type( dataIndex, "TYPE" );

	//
	//  Get the data/weight etc. equal and get the flags as well...
	//
	if( !::shapeDataEtcEqual(theShape, theData, theWeight, theFlags,
							 wt, tab, wtcolName, datacolName) )
	{
		return emptyRec;
	}

	if( theShape.nelements()==0 )
	{
		return emptyRec;
	}

	Record                           pr;
	IPosition                        shp;
	IPosition                        resshp;
	ROScalarColumn<Int>              ant1( tab, "ANTENNA1" );
	ROScalarColumn<Int>              ant2( tab, "ANTENNA2" );
	ROScalarColumn<Int>              dd( tab, "DATA_DESC_ID" );
	ROScalarColumn<Int>              fld( tab, "FIELD_ID" );
	ROScalarColumn<Double>           timecol( tab, "TIME" );
	RecordFieldPtr<String>           comment;
	RecordFieldPtr<Array<Double> >   xptr;
	RecordFieldPtr<Array<Double> >   yptr;

	// 
	// Fill in stuff that will not change 
	// 
	*type = "amplitude";
	
	//
	//  Return the amp/time of the current selection...
	//
	Int           npol;
	Int           nchan;
	Int           curpol;
	Int           curchan;
	Bool          doAvg( chanAvg==PlotterIF::avgScalar ||
						 chanAvg==PlotterIF::avgVector );

	Double        plotvalue;
	//
	// This value will be used if Vector-channel averaging is requested
	// (if Scalar averaging is requested, we integrate directly into
	// plotvalue)
	//
	Complex       avgcmpl;
	IPosition     idx(1,0);

	//
	//  These arrays will be used for concatenation,
	//  that is: the time/plotvalues will be stored in these
	//  arrays after which these arrays are concatenated to
	//  any existing data
	//
	Array<Double> tmpx( IPosition(1,1) );
	Array<Double> tmpy( IPosition(1,1) );

	//
	//  String stuff, only used when averaging. We 
	//  will stuff into the comment which channels were 
	//  averaged and also how....
	//
	String        commentstr;
	ostringstream strm;
	

	//
	// Inquire the number of pols/channels
	//
	polSel.shape( npol );
	chanSel.shape( nchan );


	for( uInt row=0; row<tab.nrow(); row++ )
	{
		for( Int cp=0; cp<npol; cp++ )
		{
			//
			//  Waste some CPU-cycles; we could do this 
			//  conditionally...(doAvg)
			//
			plotvalue = 0.0;
			avgcmpl   = Complex(0.0, 0.0);

			for( Int cc=0; cc<nchan; cc++ )
			{	
				curpol  = polSel(cp);
				curchan = chanSel(cc);
				
				if( theFlags(IPosition(3,curpol,curchan,row)) )
				{
					//
					// If we're averaging we may as well break from the loop
					// since if one channel fails the weight-criterion the
					// whole sample should be discarded. If we're doing 
					// single channels we may as well just continue with the
					// next channel 
					//
					if( doAvg )
					{
						break;
					}
					else
					{
						continue;
					}
				}

				//
				// Pre-fetch the data; we need it anyhow, no matter what
				//
				Complex&  data( theData(IPosition(3,curpol,curchan,row)) );

				//
				// Ok.. if we're averaging channels we add up the values
				// until we've reached the last channel
				//
				if( doAvg )
				{
					switch( chanAvg )
					{
						case PlotterIF::avgVector:
							avgcmpl += data;
							break;

						case PlotterIF::avgScalar:
							plotvalue += abs(data);
							break;

						default:
							break;
					}

					//
					// Continue with next channel unless we've reached
					// the last channel
					//
					if( cc<(nchan-1) )
					{
						continue;
					}
				}

				//
				// If we end up here we have to do stuff!
				//
				// Stage 1) lets try to determine the value to be plotted.
				// Depending on wether or not averaging and if so, which 
				// kind of averaging we have to do a different calculation.	
				//
				// One thing we have to ascertain is that by the time we 
				// reach stage 2, we should have filled in the variable
				// 'plotvalue' since that is the value that will be appended
				// to the plots later on.
				//
				if( doAvg )
				{
					switch( chanAvg )
					{
						case PlotterIF::avgVector:
							avgcmpl /= ((Float)nchan);
							plotvalue = abs( avgcmpl );
							break;
												
						case PlotterIF::avgScalar:
							plotvalue /= (Double)nchan;
							break;
						
						default:
							plotvalue = -1.0;
							break;
					}
				}
				else
				{
					plotvalue = abs( data );
				}

				//
				//  This is where stage 2 starts. Prepare plots and append the
				//  value that we want to plot.
				//
				*p    = curpol;

				if( doAvg )
				{
					*ch   = 0;
				}
				else
				{
					*ch   = curchan;
				}
				*sb   = dd(row);
				*fq   = 0;
				*src  = fld(row);
				*bl   = 1000*ant1(row)+ant2(row);
				*time = timecol(row);

				plotName    = ::makePlotName( dataIndex, newplot, PlotterIF::TIME );
				datasetName = ::makeDatasetName( dataIndex, newplot, PlotterIF::TIME );

				if( !pr.isDefined(plotName) )
				{
					pr.defineRecord(plotName, Record() );
				}

				Record&   recref( pr.rwSubRecord(plotName) );

				if( !recref.isDefined(datasetName) )
				{
					recref.defineRecord(datasetName, Record(::makePlotResultLayout()) );
				}
		
				Record&   dsref( recref.rwSubRecord(datasetName) );

				xptr.attachToRecord( dsref, "xval" );
				yptr.attachToRecord( dsref, "yval" ); 

				tmpx( idx ) = timecol( row );	
				tmpy( idx ) = plotvalue;

				Array<Double>    resx( ::concatenateArray(*xptr, tmpx) );
				Array<Double>    resy( ::concatenateArray(*yptr, tmpy) );

				(*xptr).reference( resx );
				(*yptr).reference( resy );

				//
				// Optionally deal with comment
				//
				if( !doAvg )
				{
					continue;
				}

				comment.attachToRecord( dsref, "comment" );

				strm.str( string() );
				strm << chanAvg << "averaged " << nchan << " channels ("
					 << chanSel << ")\n";

				commentstr = strm.str();

				if( (*comment)!=commentstr )
				{
					(*comment) = (*comment) + commentstr;
				}
			}	
		}
	}
	return pr;	
}



AmpTimePlotter::~AmpTimePlotter()
{
}
