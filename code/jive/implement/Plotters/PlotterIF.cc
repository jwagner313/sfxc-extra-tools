//
//  'implementation' of the PlotterInterface....
//
//
//
#include <jive/Plotters/PlotterIF.h>
#include <jive/Plotters/AmpTimePlotter.h>
#include <jive/Plotters/PhaChanPlotter.h>
#include <jive/Plotters/WeightTimePlotter.h>
#include <jive/Plotters/PhaTimePlotter.h>
#include <jive/Plotters/AmpChanPlotter.h>
#include <jive/Plotters/ANPChanPlotter.h>
#include <jive/Plotters/ANPTimePlotter.h>

#include <casa/Arrays/ArrayLogical.h>
#include <casa/Utilities/DataType.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <tables/Tables.h>

#include <iomanip>
#include <sstream>

using namespace std;
using namespace casa;


const DataType    PlotterIF::labDataType[ (uInt)PlotterIF::NUMAXES ] = 
			              { TpInt, TpInt, TpInt, TpInt, TpInt, TpInt, 
							TpDouble, TpString };


ostream& operator<<( ostream& os, PlotterIF::Axis ax )
{
	os << PlotterIF::axisToString(ax);
	return os;
}

ostream& operator<<( ostream& os, PlotterIF::AvgMethod am )
{
	os << PlotterIF::avgToString(am);
	return os;
}



RecordDesc makePlotResultLayout( void )
{
	RecordDesc    retval;

	retval.addField( "xval", TpArrayDouble );
	retval.addField( "yval", TpArrayDouble );
	retval.addField( "comment", TpString );
	return retval;
}


RecordDesc makeDataIndexLayout( void )
{
	//
	// The return value
	//
	RecordDesc    retval;

	for( uInt i=0; i<(uInt)PlotterIF::NUMAXES; i++ )
	{
		retval.addField( PlotterIF::axisToString((PlotterIF::Axis)i), 
						 PlotterIF::labDataType[i] );
	}
	return retval;
}


//
//
//  Try to determine the name of the datacolumn to use
//
//  LAG_DATA takes precedence over DATA
//
String dataColumnName( const Table& tab )
{
	String           retval( "NONE" );
	const TableDesc& tdref( tab.tableDesc() );


	if( tdref.isColumn("LAG_DATA") )
	{
		retval = "LAG_DATA";
	}
	else if( tdref.isColumn("DATA") )
	{
		retval = "DATA";
	}
	return retval;	
}


//
//
//  The purpose of this function is to make sure that data/weight and flags
//  are shaped all equal (and if necessary values are replicated).
//
//  I decided to make it so since in this case the 'client' function (i.e. the caller) 
//  can always be sure after calling this function that all shapes are equal no
//  matter what the underlying data in the MS contained
//
//  Shapes will be of length 0 in case of an error! Also, a False caode wil be 
//  returned..
//
//
Bool shapeDataEtcEqual( IPosition& ipos, Array<Complex>& data, Array<Float>& weight,
						Array<Bool>& flags, Float wt, const Table& tab, 
						const String& wtcolName, const String& datacolName )
{
	//
	// Pre-create a lot of objects to squeeze as much speed out of it 
	// as possible...
	//
	static Bool                     error;
	static IPosition                oldIPos;
	static IPosition                newIPos;
	static IPosition                dataIPos;
	static IPosition                tmpIPos;
	static Array<Float>             orgweight;
	static Array<Complex>           orgdata;
	static ROArrayColumn<Float>     wtcol;
	static ROArrayColumn<Complex>   datacol;
	

	//
	//  Automayica variable - the return value
	//
	Bool                          retval;

	//
	//  Resize arrays
	//
	data.resize( IPosition(0,0) );
	weight.resize( IPosition(0,0) );
	flags.resize( IPosition(0,0) );
	ipos.resize(0);
	retval = False;

	if( !tab.nrow() )
	{
		return retval;
	}

	//
	//  Attach columns, read data and reform...
	//
	wtcol.attach( tab, wtcolName );
	datacol.attach( tab, datacolName );	

	//
	//  Get all of the data and weights
	//
	wtcol.getColumn( orgweight, True );
	datacol.getColumn( orgdata, True );

	//
	//  Start off with the data
	//
	newIPos = orgdata.shape();

	if( newIPos.nelements()==2 )
	{
		// Only two dimensions... implies a single time-axis.. append an axis of length
		// 1...
		newIPos.resize( 3 );
		newIPos(2) = 1;

		// re-shape data
		orgdata.reform( newIPos );
	}

	//
	// if the data does not have dimension 3, we're stuffed
	//
	if( newIPos.nelements()!=3 )
	{
		return retval;
	}
	
	//
	//  Re-shape the new weight...
	//
	weight.resize( newIPos );

	// get the shape of the old weight...
	oldIPos  = orgweight.shape();	
	
	// get the shape of the 'old' data
	dataIPos = orgdata.shape();


	//
	//  Try to think of cases of weightshapes which we allow...
	//
	//  1) wt is vector with length == nr-of-polarizations in the data =>
	//     one weight per pol
	//  2) wt is matrix of npol*nchan -> one weight per datapoint
	//     (only valid when nintg==1)
	//  3) wt is three dimensional in which case all dimensions of old/new 
	//     IPos should be equal
	//
	error = False;
	switch( oldIPos.nelements() )
	{
		case 1:
			if( (error=(oldIPos(0)!=dataIPos(1) || dataIPos(2)!=1))==False )
			{
				//
				//  Ok. Replicate the weight
				//
				for( Int t=0; t<newIPos(2); t++ )
				{
					for( Int p=0; p<newIPos(0); p++ )
					{
						for( Int c=0; c<newIPos(1); c++ )
						{
							weight( IPosition(3,p,c,t) ) = orgweight( IPosition(1,p) );
						}
					}
				}
			}
			break;

		case 2:
			//
			//  Weight is matrix.
			//
			//  Dim(0) = nr-of-polarization (=datashape(0))
			//  Dim(1) = nr-of-integrations (=datashape(2))
			//
			if( (error=(oldIPos(0)!=dataIPos(0) || oldIPos(1)!=dataIPos(2)))==False )
			{
				//
				//  Ok.. Replicate weight...
				//
				for( Int t=0; t<newIPos(2); t++ )
				{
					for( Int p=0; p<newIPos(0); p++ )
					{
						for( Int c=0; c<newIPos(1); c++ )
						{
							weight( IPosition(3,p,c,t) ) = orgweight( IPosition(2,p,t) );
						}
					}
				}
				
			}
			break;

		case 3:
			//
			//  Verify shapes are equal:
			//
			if( (error=(oldIPos!=dataIPos))==False )
			{
				weight = orgweight;
			}
			break;

		default:
			//
			// Dang! Incompatible shape!
			//
			error = True;
			break;
	}

	if( error )
	{
		return retval;
	}

	retval = True;
	//
	// Now it's about time to start copying result values into
	// the caller's variables...
	//
	ipos.resize( newIPos.nelements() );
	ipos   = newIPos;
	data   = orgdata;
	// weight -> is already set
	
	//
	//  Need to reform+set flags. Flags will be set according to
	//  user-specified weigth-threshold. Datapoints with weight<threshold
	//  will be set to True
	//
	flags.resize( newIPos );
	flags = (Bool)False;

	//cout << "flags (before)=" << flags << endl;
	//cout << "orgweight=" << orgweight << endl
	//	 << "weight=" << weight << endl
	//	 << "threshold=" << wt << endl;
	flags = (weight<wt);
	//cout << "flags (after)=" << flags << endl;
	
	return retval;
}



String makePlotName( const Record& cur, const Vector<Bool>& newplot, PlotterIF::Axis xaxis )
{
	ostringstream  plotnamestrm;

	plotnamestrm << "PLOT=";

	for( uInt i=0; i<(uInt)PlotterIF::NUMAXES; i++ )
	{
		if( !newplot(i) || ((PlotterIF::Axis)i)==xaxis )
		{
			continue;
		}
		switch( PlotterIF::labDataType[i] )
		{
			case TpInt:
				{
					Int   tmp;

					cur.get( i, tmp );
					plotnamestrm << ',' 
								 << PlotterIF::axisToString( (PlotterIF::Axis)i ) 
								 << '=' << tmp;
				}
				break;

			case TpDouble:
				{
					Double  tmp;

					cur.get( i, tmp );
					plotnamestrm << ','
								 << PlotterIF::axisToString( (PlotterIF::Axis)i )
								 << '=' << setw(15) << setprecision(15) << tmp;
				}
				break;

			case TpString:
				{
					String   tmp;

					cur.get( i, tmp );
					plotnamestrm << ','
								 << PlotterIF::axisToString( (PlotterIF::Axis)i )
								 << '=' << tmp;
				}
				break;

			default:
				plotnamestrm << i << "=UNKNOWN";
				break;
		}	
	}
	return plotnamestrm.str();
}


String makeDatasetName( const Record& cur, const Vector<Bool>& newplot, PlotterIF::Axis xaxis )
{
	ostringstream  plotnamestrm;

	plotnamestrm << "DATASET=";
	for( uInt i=0; i<(uInt)PlotterIF::NUMAXES; i++ )
	{
		if( newplot(i) || ((PlotterIF::Axis)i)==xaxis )
		{
			continue;
		}
		switch( PlotterIF::labDataType[i] )
		{
			case TpInt:
				{
					Int   tmp;

					cur.get( i, tmp );
					plotnamestrm << ',' 
								 << PlotterIF::axisToString( (PlotterIF::Axis)i ) 
								 << '=' << tmp;
				}
				break;

			case TpDouble:
				{
					Double  tmp;

					cur.get( i, tmp );
					plotnamestrm << ','
								 << PlotterIF::axisToString( (PlotterIF::Axis)i )
								 << '=' << setw(15) << setprecision(15) <<  tmp;
				}
				break;

			case TpString:
				{
					String   tmp;

					cur.get( i, tmp );
					plotnamestrm << ','
								 << PlotterIF::axisToString( (PlotterIF::Axis)i )
								 << '=' << tmp;
				}
				break;

			default:
				plotnamestrm << i << "=UNKNOWN";
				break;
		}	
	}
	return plotnamestrm.str();
}


//
//  Allocate space for the static RecordDesc thingy...
//
RecordDesc PlotterIF::PlotResult = ::makePlotResultLayout();


static uInt          nplotters( 0 );
static PlotterIF**   plotters;


class PlotRegistrar
{
public:
	PlotRegistrar();
	~PlotRegistrar();

private:
	PlotRegistrar( const PlotRegistrar& );
	PlotRegistrar& operator=( const PlotRegistrar& );
};

PlotRegistrar::PlotRegistrar()
{
	if( !nplotters )
	{
		nplotters = 7;

		plotters = new PlotterIF*[nplotters];

		plotters[0] = new AmpTimePlotter();
		plotters[1] = new PhaChanPlotter();
		plotters[2] = new WeightTimePlotter();
		plotters[3] = new PhaTimePlotter();
		plotters[4] = new AmpChanPlotter();
		plotters[5] = new ANPChanPlotter();
		plotters[6] = new ANPTimePlotter();
	}
}

PlotRegistrar::~PlotRegistrar()
{
	for( uInt i=0; i<nplotters; i++ )
	{
		delete plotters[i];
	}
	delete plotters;
	plotters  = 0;
	nplotters = 0;
}

//
//  The quick-n-dirty hack, of course, being that we have a global (yuck!)
//  variable which controls allocating/de-allocating the plotters!
//
static PlotRegistrar    theRegistrar;

//
//  Static functions
//
String PlotterIF::axisToString( PlotterIF::Axis axis )
{
	String   retval("<INVALID>");

	switch( axis )
	{
		case PlotterIF::P:
			retval = "P";
			break;

		case PlotterIF::CH:
			retval = "CH";
			break;

		case PlotterIF::SB:
			retval = "SB";
			break;

		case PlotterIF::FQ:
			retval = "FQ";
			break;

		case PlotterIF::SRC:
			retval = "SRC";
			break;

		case PlotterIF::BL:
			retval = "BL";
			break;

		case PlotterIF::TIME:
			retval = "TIME";
			break;

		case PlotterIF::TYPE:
			retval = "TYPE";
			break;

		default:
			break;
	}
	return retval;
}


PlotterIF::Axis PlotterIF::stringToAxis( const String& str )
{
	PlotterIF::Axis  retval = PlotterIF::NONE;

	if( str=="P" )
	{
		retval = PlotterIF::P;
	}
	else if( str=="CH" )
	{
		retval = PlotterIF::CH;
	}
	else if( str=="SB" )
	{
		retval = PlotterIF::SB;
	}
	else if( str=="FQ" )
	{
		retval = PlotterIF::FQ;
	}
	else if( str=="SRC" )
	{
		retval = PlotterIF::SRC;
	}
	else if( str=="BL" )
	{
		retval = PlotterIF::BL;
	}
	else if( str=="TIME" )
	{
		retval = PlotterIF::TIME;
	}
	else if( str=="TYPE" )
	{
		retval = PlotterIF::TYPE;
	}
	return retval;
}


String PlotterIF::avgToString( PlotterIF::AvgMethod method )
{
	String    retval( "<INVALID" );

	switch( method )
	{
		case PlotterIF::avgNone:
			retval = "None";
			break;

		case PlotterIF::avgScalar:
			retval = "Scalar";
			break;

		case PlotterIF::avgVector:
			retval = "Vector";
			break;
		
		default:
			break;
	}
	return retval;
}


PlotterIF::AvgMethod PlotterIF::stringToAvg( const String& str )
{
	PlotterIF::AvgMethod    retval( PlotterIF::avgNone );

	if( str=="None" )
	{
		retval = PlotterIF::avgNone;
	}
	else if( str=="Scalar" )
	{
		retval = PlotterIF::avgScalar;
	}
	else if( str=="Vector" )
	{
		retval = PlotterIF::avgVector;
	}
	return retval;
}




//
//  The Plotter factory....
//
const PlotterIF* PlotterIF::makePlotter( const String& plotterId )
{
	//
	//  Automatic variables
	//
	const PlotterIF*   retval( 0 );

	for( uInt i=0; i<nplotters; i++ )
	{
		if( plotters[i] && (plotters[i]->getID()==plotterId) )
		{
			retval = (const PlotterIF*)plotters[i];
			break;
		}
	}
	return retval;
}




//
//  The plotter interface class
//
PlotterIF::PlotterIF( const String& plotterid ) :
	 myID( plotterid )
{
}

const String& PlotterIF::getID( void ) const
{
	return myID;
}

//
//  Nothing to do (yet)
//
PlotterIF::~PlotterIF()
{
}
