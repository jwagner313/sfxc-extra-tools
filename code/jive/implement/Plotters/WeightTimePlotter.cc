//
//  Implementation of the WeightTimePlotter....
//
// 
//
#include <jive/Plotters/WeightTimePlotter.h>

#include <casa/Arrays/Vector.h>
#include <casa/Arrays/ArrayUtil.h>
#include <tables/Tables.h>

#include <casa/Containers/RecordField.h>
#include <tables/Tables/ScalarColumn.h>

#include <jive/ms2uvfitsimpl/DumpRecord.h>

#include <iomanip>

using namespace std;
using namespace casa;


WeightTimePlotter::WeightTimePlotter() :
	PlotterIF( "wttime" )
{
	//
	//  Define the retval-layout
	//
	myRetvalLayout.addField( "weight", PlotterIF::PlotResult );
}

Block<PlotterIF::Axis>   WeightTimePlotter::bypassAxes( void ) const
{
	return Block<PlotterIF::Axis>();

//	Block<PlotterIF::Axis>   retval( 1 );
	
//	retval[0] = PlotterIF::TIME;
//	return retval;
}


Record WeightTimePlotter::plotResult( const Table& tab, Float,
									  const String& wtcolName,
									  const String& ,
									  const Vector<Bool>& newplot,
									  const Vector<Int>& polSel,
									  const Vector<Int>& ,
									  PlotterIF::AvgMethod ,
									  PlotterIF::AvgMethod ) const
{
	//
	//  Some static stuff
	//
	static String                 plotName;
	static String                 datasetName;
	static Record                 emptyRec;
	static Record                 dataIndex( ::makeDataIndexLayout() );
	static IPosition              theShape;
	static Array<Float>           theWeight;
	static Vector<Double>         theTime;
	static RecordFieldPtr<Int>    p( dataIndex, "P" );
	static RecordFieldPtr<Int>    ch( dataIndex, "CH" );
	static RecordFieldPtr<Int>    sb( dataIndex, "SB" );
	static RecordFieldPtr<Int>    fq( dataIndex, "FQ" );
	static RecordFieldPtr<Int>    src( dataIndex, "SRC" );
	static RecordFieldPtr<Int>    bl( dataIndex, "BL" );
	static ROArrayColumn<Float>   wtcol;
	static ROScalarColumn<Double> timcol;
	static RecordFieldPtr<Double> time( dataIndex, "TIME" );
	static RecordFieldPtr<String> type( dataIndex, "TYPE" );

	//
	//  If nothing to do: get the heck out of here as fast as we can....
	//
	if( !tab.nrow() )
	{
		return emptyRec;
	}

	//
	//  Resize array and attach to columns in table
	//
	theWeight.resize( IPosition(0,0) );

	wtcol.attach( tab, wtcolName );
	timcol.attach( tab, "TIME" );

	//
	//  Lets get the weight and the times out of the table!
	//
	wtcol.getColumn( theWeight, True );
	timcol.getColumn( theTime, True );

	theShape = theWeight.shape();

	//
	// Return value stuff....
	//
	Int                              npol;
	Int                              curpol;
	Int                              nint;
	Record                           pr;
	IPosition                        shp( 1, 0 );
	IPosition                        resshp;
	ROScalarColumn<Int>              ant1( tab, "ANTENNA1" );
	ROScalarColumn<Int>              ant2( tab, "ANTENNA2" );
	ROScalarColumn<Int>              dd( tab, "DATA_DESC_ID" );
	ROScalarColumn<Int>              fld( tab, "FIELD_ID" );
	RecordFieldPtr<Array<Double> >   xptr;
	RecordFieldPtr<Array<Double> >   yptr;

	// 
	// Fill in stuff that will not change 
	// 
	*type = "weight";

	//
	//  These arrays will be used for concatenation,
	//  that is: the time/plotvalues will be stored in these
	//  arrays after which these arrays are concatenated to
	//  any existing data
	//
	Array<Float>  shit;
	Array<Double> tmpy;

	//
	// Inquire the number of pols and the number of integrations
	//
	polSel.shape( npol );
	nint = tab.nrow();

	//
	//  We can already do this; the result array will always be this shape...
	//
	tmpy.resize( IPosition(1,nint) );

	for( Int cp=0; cp<npol; cp++ )
	{
		curpol = polSel(cp);

		//
		//  Depending on the shape of the weight we have to do stuff...
		//  the gist being that whatever happens, after the swithch statement
		//  the tmpy array should contain an vector of values with a length of
		//  'nrow' if you will
		//
		switch( theShape.nelements() )
		{
			case 1:
				//
				// Basically, one weight for the whole matrix
				// replicate the values...
				//
				shit = theWeight;
				break;

			case 2:
				{
					//
					//  Hmmm... looks like one weight per pol/time 
					//
					IPosition    begin( 2, curpol, 0 );
					IPosition    end( 2, curpol, nint-1 );
					IPosition    inc( 2, 1, 1 );

					shit = theWeight( begin, end, inc );
				}
				break;

			case 3:
				{
					//
					//  One weight per pol/chan/time
					//  just take the first channel
					//
					IPosition    begin( 3, curpol, 0,0 );
					IPosition    end( 3, curpol, 0, nint-1 );
					IPosition    inc( 3, 1, 1, 1 );

					shit = theWeight( begin, end, inc );
				}
				break;
				
			default:
				//
				//  Unsupported. Go on with next
				//
				continue;
		}

		//
		//  Daah! Need to transform the Float array into a Double array...
		//  Need to do that on an element by element basis...
		//
		Vector<Float>   shit2( shit.reform(IPosition(1,nint)) );


		for( shp(0)=0; shp(0)<nint; shp(0)++ )
		{
			tmpy( shp ) = (Double)(shit2(shp));
		}

		*p    = curpol;
		*ch   = 0;
		*sb   = dd(0);
		*fq   = 0;
		*src  = fld(0);
		*bl   = 1000*ant1(0)+ant2(0);
		*time = 0.0;

		plotName    = ::makePlotName( dataIndex, newplot, PlotterIF::TIME );
		datasetName = ::makeDatasetName( dataIndex, newplot, PlotterIF::TIME );

		if( !pr.isDefined(plotName) )
		{
			pr.defineRecord(plotName, Record() );
		}

		Record&   recref( pr.rwSubRecord(plotName) );


		if( !recref.isDefined(datasetName) )
		{
			recref.defineRecord(datasetName, Record(::makePlotResultLayout()) );
		}
		
		Record&   dsref( recref.rwSubRecord(datasetName) );


		xptr.attachToRecord( dsref, "xval" );
		yptr.attachToRecord( dsref, "yval" ); 

		Array<Double>    resx( ::concatenateArray(*xptr, theTime) );
		Array<Double>    resy( ::concatenateArray(*yptr, tmpy) );

		(*xptr).reference( resx );
		(*yptr).reference( resy );
	}

	return pr;	
}



WeightTimePlotter::~WeightTimePlotter()
{
}
