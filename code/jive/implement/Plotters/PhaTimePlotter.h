//
//  PhapTimePlotter - Specialization of the PloterIF-class:
//
//  This one makes plots of amp/time... (sheeeee)
//
//
#ifndef PHATIMEPLOTTER_H
#define PHATIMEPLOTTER_H

#include <jive/Plotters/PlotterIF.h>
#include <casa/Containers/RecordDesc.h>


class PhaTimePlotter :
	public PlotterIF
{
public:

	//
	//  Construct an PhaTimePlotter object...
	//	
	PhaTimePlotter();


	virtual casa::Record plotResult( const casa::Table& tab, casa::Float wt, 
						       const casa::String& wtcolName,
						       const casa::String& datacolName,
							   const casa::Vector<casa::Bool>& newplot,
							   const casa::Vector<casa::Int>& polSel,
							   const casa::Vector<casa::Int>& chanSel,
							   PlotterIF::AvgMethod timeAvg,
							   PlotterIF::AvgMethod chanAvg ) const;


	virtual casa::Block<PlotterIF::Axis>   bypassAxes( void ) const;

	virtual ~PhaTimePlotter();

private:
	//
	//  Nothing (yet)
	//
	casa::RecordDesc      myRetvalLayout;

	//
	//  Prohibit these:
	//
	PhaTimePlotter( const PhaTimePlotter& );
	PhaTimePlotter& operator=( const PhaTimePlotter& );
};



#endif
