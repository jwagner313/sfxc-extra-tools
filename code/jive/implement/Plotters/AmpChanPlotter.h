//
//  AmppTimePlotter - Specialization of the PloterIF-class:
//
//  This one makes plots of amp/time... (sheeeee)
//
//
#ifndef AMPCHANPLOTTER_H
#define AMPCHANPLOTTER_H

#include <jive/Plotters/PlotterIF.h>
#include <casa/Containers/RecordDesc.h>


class AmpChanPlotter :
	public PlotterIF
{
public:

	//
	//  Construct an AmpChanPlotter object...
	//	
	AmpChanPlotter();


	virtual casa::Record plotResult( const casa::Table& tab, casa::Float wt, 
						       const casa::String& wtcolName,
						       const casa::String& datacolName,
							   const casa::Vector<casa::Bool>& newplot,
							   const casa::Vector<casa::Int>& polSel,
							   const casa::Vector<casa::Int>& chanSel,
							   PlotterIF::AvgMethod timeAvg,
							   PlotterIF::AvgMethod chanAvg ) const;

	virtual casa::Block<PlotterIF::Axis>   bypassAxes( void ) const;

	virtual ~AmpChanPlotter();

private:
	//
	//  Nothing (yet)
	//
	casa::RecordDesc      myRetvalLayout;

	//
	//  Prohibit these:
	//
	AmpChanPlotter( const AmpChanPlotter& );
	AmpChanPlotter& operator=( const AmpChanPlotter& );
};



#endif
