//
//  ANPpTimePlotter - Specialization of the PloterIF-class:
//
//  This one makes plots of ANP/time... (sheeeee)
//
//
#ifndef ANPCHANPLOTTER_H
#define ANPCHANPLOTTER_H

#include <jive/Plotters/PlotterIF.h>
#include <casa/Containers/RecordDesc.h>


class ANPChanPlotter :
	public PlotterIF
{
public:

	//
	//  Construct an ANPChanPlotter object...
	//	
	ANPChanPlotter();


	virtual casa::Record plotResult( const casa::Table& tab, casa::Float wt, 
						       const casa::String& wtcolName,
						       const casa::String& datacolName,
							   const casa::Vector<casa::Bool>& newplot,
							   const casa::Vector<casa::Int>& polSel,
							   const casa::Vector<casa::Int>& chanSel,
							   PlotterIF::AvgMethod timeAvg,
							   PlotterIF::AvgMethod chanAvg ) const;

	virtual casa::Block<PlotterIF::Axis>   bypassAxes( void ) const;

	virtual ~ANPChanPlotter();

private:
	//
	//  Nothing (yet)
	//
	casa::RecordDesc      myRetvalLayout;

	//
	//  Prohibit these:
	//
	ANPChanPlotter( const ANPChanPlotter& );
	ANPChanPlotter& operator=( const ANPChanPlotter& );
};



#endif
