//
//  WeightpTimePlotter - Specialization of the PloterIF-class:
//
//  This one makes plots of weight/time... (sheeeee)
//
//
#ifndef WEIGHTTIMEPLOTTER_H
#define WEIGHTTIMEPLOTTER_H

#include <jive/Plotters/PlotterIF.h>
#include <casa/Containers/RecordDesc.h>


class WeightTimePlotter :
	public PlotterIF
{
public:

	//
	//  Construct an WeightTimePlotter object...
	//	
	WeightTimePlotter();


	//
	//  As you may see we leave some of the arguments unnamed:
	//  that is to indicate that these arguments are discarded...
	//
	virtual casa::Record plotResult( const casa::Table& tab, casa::Float wt, 
						       const casa::String& wtcolName,
						       const casa::String& ,
							   const casa::Vector<casa::Bool>& newplot,
							   const casa::Vector<casa::Int>& polSel,
							   const casa::Vector<casa::Int>& ,
							   PlotterIF::AvgMethod ,
							   PlotterIF::AvgMethod ) const;


	virtual casa::Block<PlotterIF::Axis>   bypassAxes( void ) const;

	virtual ~WeightTimePlotter();

private:
	//
	//  Nothing (yet)
	//
	casa::RecordDesc      myRetvalLayout;

	//
	//  Prohibit these:
	//
	WeightTimePlotter( const WeightTimePlotter& );
	WeightTimePlotter& operator=( const WeightTimePlotter& );
};



#endif
