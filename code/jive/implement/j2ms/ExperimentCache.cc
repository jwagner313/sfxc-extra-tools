//  Implementation of the ExperimentCache - classes...
#include <jive/j2ms/ExperimentCache.h>

#include <casa/Arrays/Vector.h>
#include <casa/Exceptions.h>

#include <stdlib.h>

using std::make_pair;
using namespace casa;

//  Init ScanCacheElement.....
_ScanCacheElement::_ScanCacheElement() :
    outEpochRef( MEpoch::GAST ),
    prefixtimeadjust( 0 ),
    postfixtimeadjust( 0 ),
    samplecount( 0 ),
    itsLastIntegration( (unsigned int)-1 ),
    itsLastendbocf( (unsigned int)-1 )
{
    // Check if the aipspath-environment variable is set. If it isn't,
    // we cant do the conversions (likely to crash..)
    if( !getenv("AIPSPATH") )
    	throw( AipsError( String("Environment var. `AIPSPATH' not set. source an aipsinit.csh -")
			             +String(" script to set it for you.")) );
    
    //  Okiedokie, set references
    timeCvt.setOut( outEpochRef );
}


_ScanCacheElement::~_ScanCacheElement()
{}

// add the scan to the cache, if not existent yet
ScanCacheElement* _ExperimentCacheElement::addScan( const j2ms::Scan& scanref )
{
    scancache_t::iterator  curscan;

    if( (curscan=scancache.find(scanref.getScanId()))==scancache.end() ) {
	    bool                             found;
    	const Experiment&                expref( scanref.getExperiment() );
        pair<scancache_t::iterator,bool> insres;
	
        insres = scancache.insert( make_pair(scanref.getScanId(), new ScanCacheElement()) );
        if( !insres.second ) {
            ostringstream err;

            err << "ExperimentCacheElement::addScan(" << scanref.getScanId() << ")/Failed to "
                << "insert a new entry in the cache (map.insert() failed)";
            throw (AipsError(err.str()));
        }
        curscan = insres.first;

        // create ref to newly created "ScanCacheElement"
        ScanCacheElement& sce( *curscan->second );
	
    	sce.itsScanCode = scanref.getScanId();
	
    	//  Attempt to locate the source-info
    	found = expref.getSourceDB().searchSource( sce.itsSourceInfo,
                                                   scanref.getSourceCode() );

    	if( !found ) {
    	    cerr << "ExperimentCacheElement::addScan(" << scanref.getScanId()
                 << ")/Failed to find SourceInfo for source `"
                 << scanref.getSourceCode() << "'" << endl;
    	}
    }
    return curscan->second;
}

ScanCacheElement* _ExperimentCacheElement::locateScan( const String& scancode ) {
    ScanCacheElement*     retval( 0 );
    scancache_t::iterator curscan;
    
    if( (curscan=scancache.find(scancode))!=scancache.end() ) 
        retval = curscan->second;
    return retval;
}



_ExperimentCacheElement::~_ExperimentCacheElement()
{
    // Clean the scan-cache
    scancache_t::iterator curscan;

    for( curscan=scancache.begin();
         curscan!=scancache.end();
         curscan++ ) {
        delete curscan->second;
    }
}


//  The experiment cache-class

//  Add an element into the cache (if it wasn't there already)
ExperimentCacheElement* ExperimentCache::addExperiment( const Experiment& expref )
{
    expcache_t::iterator      curexp;

    if( (curexp=expcache.find(expref.getCode()))==expcache.end() ) {
    	//  Get the antennacodes
        pair<expcache_t::iterator,bool> insres;
	
	    //  Make room for a new element
        insres = expcache.insert( make_pair(expref.getCode(), new ExperimentCacheElement()) );

        if( !insres.second ) {
            ostringstream err;

            err << "ExperimentCache::addExperiment(" << expref.getCode() << ")/Failed to "
                << "insert a new entry in the cache (map.insert() failed)";
            throw (AipsError(err.str()));
        }

        // let curexp point at the newly inserted element
        curexp = insres.first;
	
        // Now fill it in
        ExperimentCacheElement&  ece( *curexp->second );

    	ece.itsCode = expref.getCode();
    }
    return curexp->second;
}

//  Locate an experiment based on its ExperimentCode
ExperimentCacheElement* ExperimentCache::locateExperiment( const String& expcode ) const {
    ExperimentCacheElement*    retval( 0 );
    expcache_t::const_iterator curexp;

    if( (curexp=expcache.find(expcode))!=expcache.end() )
        retval = curexp->second;
    return retval; 
}


//  Delete all cached objects
ExperimentCache::~ExperimentCache()
{
    expcache_t::iterator  curexp;

    for( curexp=expcache.begin();
         curexp!=expcache.end();
         curexp++ )
    {
        delete curexp->second;
    }
}


