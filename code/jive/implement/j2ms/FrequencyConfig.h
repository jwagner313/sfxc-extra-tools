//
//  FrequencyConfig -> class that holds a number of basebands (and thus
//  a frequency setup)
//
//
//  Author:   Harro Verkouter, 2-12-1998
//
//  $Id: FrequencyConfig.h,v 1.10 2011/10/12 12:45:29 jive_cc Exp $
//
//
#ifndef FREQUENCYCONFIG_H
#define FREQUENCYCONFIG_H


#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <jive/j2ms/BaseBand.h>
#include <jive/labels/polarization.h>
#include <iostream>
#include <vector>

class FrequencyConfig
{
    //  Output the Frequencysetup!
    friend std::ostream& operator<<( std::ostream& os,
                                     const FrequencyConfig& fcref );
public:
    //  An empty frequency config does not exist, it must have at
    //  least a name by which we can identify it! If the name is
    //  empty, an exception is thrown!
    //  FreqGroupNr defaults to -1
    FrequencyConfig( const casa::String& name="<DEF>",
                     casa::Int fqgrpnr = -1 );
    
    //  Copy the whole frequency config
    FrequencyConfig( const FrequencyConfig& other );
    
    //  Assignment...
    const FrequencyConfig& operator=( const FrequencyConfig& other );

    //  Add a *copy* of the baseband to this frequency setup!
    bool             addBaseBand( const BaseBand& newbb );

    //  Set the polarizations, only if it wasn't set yet - if it was
    //  already set, further attempts to modify it will be ignored/
    //  you get a false return value
    bool                         setRecordedPols( const j2ms::polarizations_t& rp );
    const j2ms::polarizations_t& getRecordedPols( void ) const;

    //  return a pointer to a const baseband, the outsideworld may
    //  read what's in here but not edit it. If the indexed baseband
    //  is not present, a null-pointer value is returned.
	BaseBand*        getBaseBand( casa::uInt bbnr );
    const BaseBand*  getBaseBand( casa::uInt bbnr ) const;

    // get read-only access to the defined basebands
    const basebandlist_t&  getBaseBands( void ) const;
    
    unsigned int     getNrBaseBands( void ) const;

    //  How many polarizations were configured???
    //
    //  Possible return values are: 0, 1, or 2
    unsigned int     getNrPolarizations( void ) const;

    //  Get the 'name' of this configuration
    const casa::String&    getCode( void ) const;

    // ...
    casa::Int              getFreqGroupNr( void ) const;

    //  Delete all resources used by this object
    ~FrequencyConfig();
   
private:
    //  The private parts
    casa::Int             itsGroupNr;
    casa::String          itsName;

    //  The configured basebands
    basebandlist_t        itsBaseBands;
    //  Recorded polarizations
    j2ms::polarizations_t itsRecordedPols;
    //  Private methods
};

#endif
