// implementation
#include <jive/j2ms/aipspppol2j2mspol.h>

#include <map>

namespace aipsppmap {
    typedef std::map<casa::Stokes::StokesTypes, CorrelationCode>  maptype;

    static maptype  __mapping = maptype();

    
    maptype make_map( void ) {
        maptype   rv;
       
        using namespace std;
        using namespace casa;

        rv.insert( make_pair(Stokes::RR, CorrelationCode(j2ms::r, j2ms::r)) );
        rv.insert( make_pair(Stokes::RL, CorrelationCode(j2ms::r, j2ms::l)) );
        rv.insert( make_pair(Stokes::LR, CorrelationCode(j2ms::l, j2ms::r)) );
        rv.insert( make_pair(Stokes::LL, CorrelationCode(j2ms::l, j2ms::l)) );

        rv.insert( make_pair(Stokes::XX, CorrelationCode(j2ms::x, j2ms::x)) );
        rv.insert( make_pair(Stokes::XY, CorrelationCode(j2ms::x, j2ms::y)) );
        rv.insert( make_pair(Stokes::YX, CorrelationCode(j2ms::y, j2ms::x)) );
        rv.insert( make_pair(Stokes::YY, CorrelationCode(j2ms::y, j2ms::y)) );

        return rv;
    }
}


CorrelationCode aipspppol2j2mspol( casa::Stokes::StokesTypes aipspp ) {
    CorrelationCode                     cc;
    aipsppmap::maptype::const_iterator  ptr;
    
    if( aipsppmap::__mapping.size()==0 )
        aipsppmap::__mapping = aipsppmap::make_map();

    if( (ptr=aipsppmap::__mapping.find(aipspp))!=aipsppmap::__mapping.end() )
        cc = ptr->second;
    return cc;
}

correlations_t aipspppol2j2mspol( const casa::Vector<casa::Int>& corrtypes ) {
    casa::Int      ncorr;
    correlations_t rv;
    
    corrtypes.shape( ncorr );
    rv.resize( ncorr );
    for(casa::Int i=0; i<ncorr; i++)
        rv[i] = aipspppol2j2mspol( (casa::Stokes::StokesTypes)corrtypes[i] );
    return rv;
}
