//
// Implementation of the sourcefilter class
//
// Author: H. Verkouter, 12-06-2001
//
// $Id: SourceFilter.cc,v 1.3 2006/01/13 11:35:39 verkout Exp $
//
// $Log: SourceFilter.cc,v $
// Revision 1.3  2006/01/13 11:35:39  verkout
// HV: CASAfied version of the implement
//
// Revision 1.2  2003/09/12 07:27:26  verkout
// HV: Code had to be made gcc3.* compliant.
//
// Revision 1.1  2001/07/09 07:53:01  verkout
// HV: Shitload of mods:
// - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
// - Enabled passing of options from j2ms2 commandline to an experiment
// - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
// - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
// - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
// - Enabled visibility filtering. At the moment only a source filter is implemented
//
//
#include <jive/j2ms/SourceFilter.h>
#include <jive/j2ms/Visibility.h>
#include <jive/j2ms/VisibilityBuffer.h>
#include <jive/j2ms/Experiment.h>
#include <jive/j2ms/Scan.h>

using namespace std;
using namespace casa;

SourceFilter::SourceFilter( const String& filteropt ) :
	Filter( "source" )
{
	//
	// Parse the filteroptions. Expect a comma separated list of
	// sources to pass....
	//
	if( filteropt.size()>0 )
	{
		char*     copy = ::strdup( filteropt.c_str() );
		char*     token;

		token = ::strtok( copy, "," );
		while( token )
		{
			acceptedSrces.push_back( token );
			token = ::strtok( 0, "," );
		}

		::free( copy );
	}

	//
	// Issue a warning if no sources are accepted!
	//
	if( acceptedSrces.size()==0 )
	{
		cout << "SourceFilter: No sources in acceptance list (ie. no source-selection)!"
			 << endl;
	}
}


Bool SourceFilter::letPass( const Visibility& vis )
{
	//
	// If no visibilityBuffer attached -> dump it right away!
	//
	if( !vis.hasVisibilityBuffer() )
	{
		return False;
	}

	//
	// Get the visibilitybuffer and the experiment and
	// check if we already have the list of sources for
	// that experiment
	//
	const VisibilityBuffer&     vbref( vis.getVisibilityBuffer() );

	//
	// If no current scan, butt out!
	//
	const j2ms::Scan*           scanptr = vbref.getScanptr();

	if( !scanptr )
	{
		return False;
	} 
	
	//
	// Ok. Now check the cache
	//	
	string                      expid;
	const Experiment&           expref( vbref.getExperiment() );
	vector<cacheElem>::iterator curelem = myCache.begin();

	expid = expref.getCode().c_str();
	while( curelem!=myCache.end() )
	{
		if( curelem->experId==expid )
		{
			break;
		}
		curelem++;
	}

	if( curelem==myCache.end() )
	{
		// Experiment not found in cache -> make list!
		curelem = this->addExperimentToCache( expref );
	}

	//
	// Now find out if source for this scan is to be let pass
	//
	Bool                        letpass = False;
	string                      src = scanptr->getSourceCode().c_str();
	map<string,int>&            mapref = curelem->srcidmap;
	map<string,int>::iterator   mapel;

	//
	// map<T1,T2>::iterator is pointer to a pair<T1,T2> struct;
	// we can access the T1-member with '->first' and the T2-member
	// with '->second'. In our case, we have a map<string,int> and
	// hence, *if* we find an entry in the map for the 'src'-key, we'll
	// find its numerical index in the 'second' element of the pair!
	//
	if( (mapel=mapref.find(src))!=mapref.end() )
	{
		int      srcindex = mapel->second;

		if( srcindex>=0 && ((unsigned int)srcindex)<(curelem->pass.size()) )
		{
			letpass = curelem->pass[ srcindex ];
		}
	}
	return letpass;
}

SourceFilter::~SourceFilter()
{
}



//
// Private methods
//
//SourceFilter::cacheElem* SourceFilter::addExperimentToCache( const Experiment& expref )
vector<SourceFilter::cacheElem>::iterator SourceFilter::addExperimentToCache(
				const Experiment& expref )
{
	Int                   shp;
	cacheElem             newel;
	vector<bool>          flags;
	const Vector<String>& srces( expref.getSources() );

	newel.experId = expref.getCode().c_str();

	srces.shape( shp );
	flags.resize( shp );

	for( int i=0; i<shp; i++ )
	{
		bool                      pass    = false;
		string                    tmp     = (srces(i)).c_str();
		vector<string>::iterator  curpass = acceptedSrces.begin();

		//
		// Define an entry in the source map
		//
		newel.srcidmap[ tmp ] = i;

		// see if we let this one pass
		while( curpass<acceptedSrces.end() )
		{
			if( tmp==*curpass )
			{
				pass = true;
				break;
			}
			curpass++;
		}

		flags[i] = pass;
	}
	
	newel.pass = flags;

	myCache.push_back( newel );

	return myCache.end()-1;	
}
