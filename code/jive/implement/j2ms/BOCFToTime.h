//
//  BOCFtoTime -> a translator class that translates correlator
//  framenrs to 'real' time.
//
//
//  Author:   Harro Verkouter,  11-03-1999
//  
//  $Id: BOCFToTime.h,v 1.5 2006/03/02 14:21:38 verkout Exp $
//
#ifndef BOCFTOTIME_H
#define BOCFTOTIME_H


//
//  We must keep a MEpoch (as the zero-point for the bocf count,
//  therefore, we must include this file...
//
#include <measures/Measures/MEpoch.h>


class BOCFToTime
{
public:
    //
    //  Empty translator -> useless....
    //
    BOCFToTime();
    
    //
    //  The copy-ctor -> copy values
    //
    BOCFToTime( const BOCFToTime& other );
    
    //
    //  Assignment...
    //
    BOCFToTime& operator=( const BOCFToTime& other );
    
    //
    //  In order to construct a sensible bocf->time translator, we
    //  need some info:
    //
    //  An approximate start-of-scan time: the system will
    //  automatically decide what the zero-point for the bocf count
    //  will be. The bocf count is zero every integral multiple
    //  of 10min of 'wall clock time'.
    //  So bocf=0 is (e.g.) 11:10, 11:20, 11:30 etc.
    //
    //  A bocf-rate: if it is zero, an exception is thrown!!!
    //
    //
    BOCFToTime( const casa::MEpoch& approxstart, casa::Double bocfrate );
    
    //
    //  The max bocf nr that can occur. Depends on bocf rate
    //
    unsigned int        getMaxBOCFnr( void ) const;

    //
    //  Get the current BOCF rate
    //
    casa::Double        getBOCFRate( void ) const;
    
    //
    //  This function transforms the bocf to a completely defined
    //  instance in time
    //
    //  NOTE: if 'anybocf' is larger than MaxBocf, it will be
    //  automatically truncated to a value in the interval [0,MaxBocf]
    //  (inclusive). Same holds for 'anybocf'<0.0 -> value will be
    //  changed so it will fall in the range [0,MaxBocf].
    //
    //  NOTE: out of speed considerations, the return value is a
    //  reference to a local static copy and hence the value will be
    //  overwritten by every successive function call. Therefore, copy
    //  the value to your local area and use that or be happy with the
    //  fact that time's changing (Ref (a CD): Tesla - Psychotic
    //  supper)
    //
    const casa::MEpoch&  operator()( casa::Double anybocf ) const;

    //
    //  We may need to advance/set back the zeropoint, suppose we have
    //  detected a zero-crossing of the bocf count, then the zero
    //  point should be incremented/decremented by 10min.
    //
    void          advanceZeroPoint( void );
    void          rewindZeroPoint( void );

    //
    //  Commodity function: adjust the zero point in units of 10min
    //  intervals.
    // 
    //     <0 indicates going back in time
    //     >0 indocates going ahead in time
    //
    void          adjust( casa::Int nrintervals );

    //
    //  Delete any allocated resources
    //
    ~BOCFToTime();

private:
    //
    //  A couple of things that we would like to keep private....
    //
    casa::MEpoch     itsBOCFZeroPoint;
    
    //
    //  The BOCF rate, in frames/sec, allow for fractianl rates (don't
    //  know if that EVER's going to happen but at least we're
    //  prepared for it...
    //
    casa::Double     itsBOCFRate;
};



#endif
