// Define free function to transform a JCCS ROT into a casa::MEpoch
//
// Author: H. Verkouter
//         11/08/2006
//
// In the JIVE Correlator online system the unit of time is SYSCLICKS.
// There are 32M SYSCLICKS per second wallclock-time
// The time is kept in SYSCLICKS since the start of the year, the 
// Short-form-ROT, of SFROT for short.
// There is a possibility of defining an instance in time in Full-Form ROT
// (FFROT) which is the SFROT + the year (integer)
// 
#ifndef J2MS_ROT2MEPOCH_H
#define J2MS_ROT2MEPOCH_H

// necessary includes

// the casa stuff
#include <casa/aips.h>
#include <measures/Measures/MEpoch.h>

// JCCS stuff
// The FFROT and SFROT (Full-Form and Short-Form Replayed Observing Time)
// thingies are defined in DataFile.h
#include <data_handler/DataFile.h>

// Transform a Fullform ROT into a MEpoch
casa::MEpoch rot2mepoch( const FFROT& ffrot );

// If you want to transform a SFROT to an instance in
// time, you'd better tell us which year...
casa::MEpoch rot2mepoch( const SFROT& rot, int year );


#endif
