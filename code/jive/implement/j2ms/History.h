//
//   History - keep track of which frequency setups with which
//   correlator setups are written into which measurementSet
//
//  The idea was to have this struct only defined within a function
//  but then the template instantiation cannot do it's work because
//  they don't know the struct definition. That's why I had to put it
//  into a separate file (this one...)
//
//
//  Author: Harro Verkouter,  23-07-1999
//
//   $Id: History.h,v 1.3 2006/01/13 11:35:38 verkout Exp $
//
//   $Log: History.h,v $
//   Revision 1.3  2006/01/13 11:35:38  verkout
//   HV: CASAfied version of the implement
//
//   Revision 1.2  2001/05/30 11:47:41  verkout
//   HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//   Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//   HV: imported aips++ implement stuff
//
//
//
#ifndef HISTORY_H
#define HISTORY_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <jive/j2ms/CorrelatorSetup.h>


//
//  Bookkeeping struct. Need to cache what frequencysetup we wrote
//  to which MS...
//
typedef struct _History
{
    casa::String          msName;
    casa::String          frequencySetup;
    CorrelatorSetup corSetup;
} History;

inline casa::Bool operator==(const History& lhside, const History& rhside)
{
    return (lhside.msName==rhside.msName) &&
		   (lhside.frequencySetup==rhside.frequencySetup) &&
		   (lhside.corSetup==rhside.corSetup);
}

inline casa::Bool operator!=(const History& lhside, const History& rhside)
{
    return !(lhside==rhside);
}

    

#endif
