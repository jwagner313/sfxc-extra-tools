#include <jive/j2ms/scanlist.h>
#include <jive/j2ms/DayConversion.h>
#include <jive/Utilities/jexcept.h>

#include <iostream>
#include <sstream>
#include <set>
#include <vector>

#include <casa/Quanta/MVTime.h>

// need this for VexPlus class
#include <prep_job/vexplus.h>

using namespace std;
using namespace j2ms;
using namespace casa;


namespace j2ms {

    stationscaninfo_t::stationscaninfo_t() :
        scanStart( 0.0 ),
        scanEnd( 0.0 )
    {}

    stationscaninfo_t::stationscaninfo_t( const MVEpoch& s,
                                          const MVEpoch& e ):
        scanStart( s ),
        scanEnd( e )
    {}

    stationscaninfo_t::stationscaninfo_t( const MVEpoch& s,
                                          const Quantity& d ):
        scanStart( s ),
        scanEnd( s+d )
    {}
}


scaninfolist_t  scanlist( const string& vexfilename,
                          /*const CorrelatorSetup& correlatorsetup,*/
                          bool verbose )
{
    int               result;
    scaninfolist_t    retval;

    if( vexfilename.empty() )
        THROW_JEXCEPT("Empty vexfilename!");

    // Read the full scanlist from the vexfile
    VexPlus  vexfile( vexfilename );

    if( (result=vexfile.parseVex())!=0 ) {
        ostringstream strm;

		strm << "scanlist()/";
		
		switch( result ) {
			case -1:
				strm << "could not open file " << vexfilename;
				break;
		
			case -2:
				strm << "error parsing vex-file " << vexfilename << " to memory";
				break;
		
			default:
				strm << "Unknown error?! Check VexPlus::parseVex() for error"
					 << " return codes (" << result << ") !!";
				break;
		}
        THROW_JEXCEPT(strm.str());
    }

    // Find the scanlist by retaining the scans by 'scanName'
    // rather than by any other identifying piece-o'-info
    // Loop over all stations and for all scans defined for the station,
    // check if we already have a scan by that name. If not, add it to 
    // the list.
	set<string>   scanList;

    for( int stationcnt=0;
         stationcnt<vexfile.N_Stations();
         stationcnt++ ) {
		int     nscans;
		string  curstation;
	
		curstation = vexfile.Station( stationcnt );

		if( curstation.empty() ) 
			THROW_JEXCEPT("scanlist()/Failed to get Station name from VEXfile\n"
				 << "           for station #" << stationcnt);

        // this is merely a warning
		if( (nscans=(vexfile.N_Scans(curstation)))==0 ) {
            if( verbose ) {
    			cerr << "scanlist()/Station " << curstation << " has no scans?!"
	    			 << endl;
            }
			continue;
		}

        if( verbose ) {
    		cout << "scanlist()/Reading " << nscans << " scans for "
    		   	 << curstation << " (" << stationcnt << "/" << vexfile.N_Stations() << ")"
    			 << endl;
        }
		
		for( int i=0; i<nscans; i++ ) {
			int          year, daynr, hour, minute, second;
			double       dayfraction;
			double       scanduration;
			string       scanstart;
			string       scanlab;
            MVTime       scanmvtime;
			ScanInfo     si;
			unsigned int month, day;
	
	
			scanlab   = vexfile.ScanName(curstation, i);
			if( scanlab.empty() ) 
				THROW_JEXCEPT("Failed to get ScanName from VEXfile for station '"
					            << curstation << "', scan #" << i);

			// Ok. Test if we did not already have this scan...
			//
			// HV: 15/03/2005 - Now the key is "scanName" and not "scanstart"
			//                  anymore
			set<string>::iterator  siptr = scanList.find( scanlab );

			if( siptr!=scanList.end() )
				continue;
			
			scanstart = vexfile.ScanStart(curstation, i);
			
			if( scanstart.empty() )
				THROW_JEXCEPT("Failed to get ScanStart from VEXfile for station '"
                              << curstation << "', scan #" << i);
	
			// brrrrm... nice to check if the thing appears in scanList, but.. errr..
			// if you forget to add a new one everytime you find one, the list
			// will stay empty, won't it??!
			scanList.insert( scanlab );
			
			// Ok. Apparently found new scan
			scanduration = vexfile.ScanDuration(curstation, i);
	
			//  Transform the scanstart into a mvepoch
			if( ::sscanf(scanstart.c_str(), "%dy%dd%dh%dm%ds", &year, &daynr, &hour,
				   		&minute, &second)!=5 ) {
                THROW_JEXCEPT("Error in format, could not decode 'scanstart' " 
                              << "for: station '" << curstation << "', scan #" << i);
            }
	
			DayConversion::dayNrToMonthDay( month, day, daynr, year );
	
			dayfraction=((hour*3600.0)+(minute*60.0)+
						(double)second)/(double)DayConversion::secondsPerDay;
		
			scanmvtime = MVTime( year, month+1, day, dayfraction );

			//  Fill in the struct
			si.scanNo          = scanList.size()+1;
			si.scanStart       = MVEpoch( scanmvtime.day() );
			si.scanEnd         = si.scanStart + Quantity(scanduration, "s");
			si.scanId          = scanlab;
			si.frequencyCode   = vexfile.ScanMode(curstation, i);
			si.sourceCode      = vexfile.ScanSource(curstation, i);
//			si.correlatorSetup = correlatorsetup;

            retval.push_back(si);
        }
    }
    return retval;
}


// local function
stationscanlist_t  stationscanlist( const string& vexfilename,
                                    int station,
                                    bool verbose = false )
{
    int               result;
    stationscanlist_t retval;

    if( vexfilename.empty() )
        THROW_JEXCEPT("Empty vexfilename!");

    // Read the full stationscanlist from the vexfile
    VexPlus   vexfile( vexfilename );

    if( (result=vexfile.parseVex())!=0 ) {
        ostringstream strm;

		strm << "stationscanlist(" << station << ")/";
		
		switch( result ) {
			case -1:
				strm << "could not open file " << vexfilename;
				break;
		
			case -2:
				strm << "error parsing vex-file " << vexfilename << " to memory";
				break;
		
			default:
				strm << "Unknown error?! Check VexPlus::parseVex() for error"
					 << " return codes (" << result << ") !!";
				break;
		}
        THROW_JEXCEPT(strm.str());
    }

    // Attempt to read the scaninfo for station 'station'
	int     nscans;
    string  curstation;
    
    curstation = vexfile.Station( station );
    if( curstation.empty() ) 
        THROW_JEXCEPT("ERR: stationscanlist(" << station << ")/Station has no name "
             << "and hence no scans can be retrieved (can only be done by name)");

	if( (nscans=(vexfile.N_Scans(curstation)))==0 ) {
        if( verbose ) {
    		cout << "WARN: stationscanlist(" << station
                 << "[" << curstation << "])/Station has no scans"
    			 << endl;
        }
		return retval;
	}

	for( int i=0; i<nscans; i++ ) {
		int          year, daynr, hour, minute, second;
		double       dayfraction;
		double       scanduration;
		string       scanstart;
        MVTime       scanmvtime;
        MVEpoch      startepoch;
		unsigned int month, day;
	
	
		scanstart = vexfile.ScanStart(curstation, i);
		if( scanstart.empty() ) 
			THROW_JEXCEPT("Failed to get ScanStart from VEXfile for station '"
                          << station << "' [" << curstation << "]" << ", scan #" << i);
	
		scanduration = vexfile.ScanDuration(curstation, i);
	
		//  Ok. Transform the scanstart into a mvepoch
		if( ::sscanf(scanstart.c_str(), "%dy%dd%dh%dm%ds", &year, &daynr, &hour,
			   		&minute, &second)!=5 ) {
            THROW_JEXCEPT("Error in format, could not decode 'scanstart' for: station '"
                           << station << "', scan #" << i);
		}
	
		DayConversion::dayNrToMonthDay( month, day, daynr, year );
	
		dayfraction= ((hour*3600.0)+(minute*60.0)+(double)second) / 
                     (double)DayConversion::secondsPerDay;
		
		scanmvtime = MVTime( year, month+1, day, dayfraction );
        startepoch = MVEpoch( scanmvtime.day() );
        
        retval.push_back( stationscaninfo_t(startepoch,
                                            Quantity(scanduration, "s")) ); 
    }
    
    return retval;
}

// local function
stationscanlist_t  stationscanlist( const VexPlus& vexfile,
                                    int station,
                                    bool verbose )
{
    stationscanlist_t retval;

    // Read the full stationscanlist from the vexfile

    // Attempt to read the scaninfo for station 'station'
	int     nscans;
    string  curstation;

    curstation = vexfile.Station(station);
    if( curstation.empty() ) 
        THROW_JEXCEPT("ERR: stationscanlist( const VexPlus&, " << station
             << ")/Station has no name "
             << "and hence no scans can be retrieved (can only be done by name)");

	if( (nscans=(vexfile.N_Scans(curstation)))==0 )
	{
        if( verbose ) {
    		cout << "WARN: stationscanlist(" << station << ")/Station has no scans"
    			 << endl;
        }
		return retval;
	}

	for( int i=0; i<nscans; i++ ) {
		int          year, daynr, hour, minute, second;
		double       dayfraction;
		double       scanduration;
		string       scanstart;
        MVTime       scanmvtime;
        MVEpoch      startepoch;
		unsigned int month, day;
	
	
		scanstart = vexfile.ScanStart(curstation, i);
		if( scanstart.empty() )
			THROW_JEXCEPT("Failed to get ScanStart for station '" << station
                          << "[" << curstation << "]" << ", scan #" << i);
	
		scanduration = vexfile.ScanDuration(curstation, i);
	
		//  Ok. Transform the scanstart into a mvepoch
		if( ::sscanf(scanstart.c_str(), "%dy%dd%dh%dm%ds", &year, &daynr, &hour,
			   		&minute, &second)!=5 ) {
            THROW_JEXCEPT("Error in format, could not decode 'scanstart' for station '"
                          << station << "[" << curstation << "]" << ", scan #" << i);
		}
	
		DayConversion::dayNrToMonthDay( month, day, daynr, year );
	
		dayfraction= ((hour*3600.0)+(minute*60.0)+(double)second) / 
                     (double)DayConversion::secondsPerDay;
		
		scanmvtime = MVTime( year, month+1, day, dayfraction );
        startepoch = MVEpoch( scanmvtime.day() );
        
        retval.push_back( stationscaninfo_t(startepoch,
                                            Quantity(scanduration, "s")) ); 
    }
    
    return retval;
}

// Read the station-based scanlists for all stations
station2listmap_t  stationscanmap( const string& vexfilename, bool verbose ) {
    int               result;
    station2listmap_t retval;

    if( vexfilename.empty() )
        THROW_JEXCEPT("Empty vexfilename!");

    // Read the full station-based scan map from the vexfile
    VexPlus  vexfile( vexfilename );

    if( (result=vexfile.parseVex())!=0 ) {
        ostringstream strm;

		strm << "stationscanmap(" << vexfilename << ")/";
		
		switch( result ) {
			case -1:
				strm << "could not open file " << vexfilename;
				break;
			case -2:
				strm << "error parsing vex-file " << vexfilename << " to memory";
				break;
			default:
				strm << "Unknown error?! Check VexPlus::parseVex() for error"
					 << " return codes (" << result << ") !!";
				break;
		}
        THROW_JEXCEPT(strm.str());
    }

    // Attempt to read the scaninfo for station 'station'
	int     nstations;

	if( (nstations=(vexfile.N_Stations()))==0 ) {
        if( verbose ) {
    		cout << "WARN: stationscanmap(" << vexfilename << ")/No stations?!"
	    		 << endl;
        }
		return retval;
	}

    for(int i=0; i<nstations; i++) {
        stationscanlist_t tmp;

        tmp = stationscanlist( vexfile, i, verbose );
        if( !tmp.empty() ) {
            pair<station2listmap_t::iterator,bool> insres;

            // attempt to insert
            insres = retval.insert( make_pair(i, tmp) );

            if( !insres.second ) 
                THROW_JEXCEPT("ERROR: stationscanmap(" << vexfilename << ")/Failed to "
                     << "insert stationbased scanlist into the map for station #"
                     << i);
        }
    }
    return retval;
}

