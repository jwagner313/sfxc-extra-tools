//
// $Id: PCIntVisBuffer.cc,v 1.6 2011/10/12 12:45:30 jive_cc Exp $
//
// Author: Harro Verkouter
//
// This class implements how to read data from PCInt datafiles
//

// do we need to build PCInt support at all?
#ifdef PCINT

// CASA includes
#include <jive/j2ms/PCIntVisBuffer.h>
#include <jive/j2ms/scanlist.h>
#include <jive/j2ms/COFScan.h>
#include <jive/j2ms/VEXperiment.h>
#include <jive/Utilities/jexcept.h>

#include <casa/OS/Time.h>
#include <casa/Quanta/MVTime.h>
#include <casa/Quanta/MVEpoch.h>
#include <measures/Measures/MEpoch.h>

// for the polarization stuff
#include <jive/labels/polarization.h>

//
// JCCS includes
// 

// for parsing the vexfile
#include <prep_job/vexplus.h>

// for accessing the statistics database
#include <stats/stats.h>

//
// PCInt includes
//
#include <pcint/GlobalArea.h>

// for retrieving mappings 
// from CorrelatorBoard -> DataStreamId
// and from DataStreamId -> where the stuff was stored
#include <pcint/GlobalArea.h>

#include <myutil/printfunctor.h>
#include <myutil/stringutil.h>
#include <myutil/RegularExpression.h>
#include <myutil/fieldmatcher.h>
#include <types/SBCManagerTypesAlgorithms.h>
#include <types/AlbertTypes.h>

#include <cbtypes/CBDataBlock.h>
#include <cbtypes/RAWDataBlock.h>

#include <labelset/printable.h>

// PCInt's loadable module support
#include <module/ModuleStack.h>

// constants for EVN specific datastreams/hardware
#include <evnstream/corconsts.h>

// std C++ includes
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <set>
#include <algorithm>


// other cruft
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>

using namespace std;
using namespace pcint;
using namespace j2ms;
using namespace casa;
using namespace DDDManagerTypes;
using namespace SBCManagerTypes;


// 'labelset' stuff
// Mark a label of type pcint::DTD printable
// (it isn't by default). From here on, if we
// print out a label(set) and the label(one of 
// the labels) has type 'pcint::DTD' we will see
// the string "pcint::DTD-object" printed, rather
// than nothing.
DECLARE_PRINTABLE(pcint::DTD);

ostream& operator<<(ostream& o, const pcint::DTD&)
{
    return (o << "pcint::DTD-object");
}

// some exceptions
struct failed_to_insert_into_map:
    public std::exception
{
    failed_to_insert_into_map(const std::string& m):
        __message("Failed to insert new entry in map: " + m)
    {}

    virtual const char* what( void ) const throw()
    {
        return __message.c_str();
    }
    virtual ~failed_to_insert_into_map() throw()
    {}

    std::string __message;
};


struct JCCS_database_inaccessible:
    public std::exception
{
};

struct JCCS_database_query_failure:
    public std::exception
{
};



// default processing chain for raw evn data
const vector<string>& default_recipe( void )
{
    static vector<string>  the_recipe;

    if( the_recipe.empty() )
    {
        static const char* mods[] = {
                               //"debug/printenv.so",
                               //"debug/printmetadata.so",
                               "processing/evn/sort.so", // sort raw data
                               "processing/evn/cvt_to_fraction.so", // convert to %-correlation
                               "processing/evn/scale.so", // scale the *validities*
                               "processing/evn/normalize.so", // normalize the *data*
                               "processing/evn/fix_xcs.so",// fix order for
                                                           // bottom half of cross correlations
                                                           // (computed in reverse time order)
                               // when adding/removing stages, leave this'un last:
                               0
                            };
        for( const char** cm=mods; *cm!=0; cm++ )
            the_recipe.push_back( *cm );
    }
    return the_recipe;
}

vector<string> read_recipe( const string& filename )
{
    string              tmp;
    ifstream            r( filename.c_str() );
    vector<string>      rv;
    string::size_type   hash;

    if( r.bad() || !r.good() || r.fail() )
    {
        // maybe it wasn't a file with a recipe but just
        // a module name... test that hypothesis
        try {
            Module      m(filename);

            // ok.. Module loaded ok. So the usr just wanted to 
            // run a single module
            rv.push_back(filename);
        }
        catch( ... ) {
            cerr << "read_recipe: Arg?\n"
                 << "  The location '" << filename << "' doesn't \n"
                 << "   refer to a file nor does it refer a loadable \n"
                 << "   module?!\n";
        }
        return rv;
    }

    while( r >> tmp )
    {
        if( (hash=tmp.find('#'))!=string::npos )
            tmp = tmp.substr(0, hash);

        if( tmp.size() )
            rv.push_back(tmp);
    }
    return rv;
}

struct input_data {
    // NOTE NOTE NOTE NOTE NOTE
    // 
    // the input_data struct
    // *takes over* responsibility for the memory
    // management of the pointers in the datafilemap!!!!!
    // !!!!!
    //
    // NOTE NOTE NOTE
    input_data( const datafilemap& dfm ):
        give( true ),
        dfMap( dfm ),
        curDatafile( dfMap.begin() )
    {
    }

    bool                           give;
    datafilemap                    dfMap;
    datafilemap::iterator          curDatafile;

    // need a list of baselines
    
    // efficiency measure: a set of logical-hwids
    // that we may deliver so downstream processing
    // may know which hwids's to expect
    // [processing must first gather data for 
    //  all datablocks for one instance in time
    //  before getting out the *baselines*
    ~input_data()
    {
        datafilemap::iterator   curdf;

        for( curdf=dfMap.begin();
             curdf!=dfMap.end();
             curdf++ )
        {
            delete curdf->second;
        }
    }

    private:
        // no default c'tor, copy c'tor or assignment.
        input_data();
        input_data( const input_data& );
        const input_data& operator=( const input_data& );
};



// struct to be able to return
// multiple results from one call to
// an analization function
struct analres_t {    // pun intended ...
    unsigned int       totalsize;
    pcint_datablockset datablocks;
    pcint_baselinelist baselines;
   
    analres_t():
        totalsize( 0U )
    {}
};

// Note: x,y will be reordered
// to y,x if y<x!!!
// Note: this condition will
// be flagged because if this
// happens the polarization codes
// must be swapped also!
//
// x is short for X-Antenna
// y    ..        Y-   ..
// sb   ..        Subband
struct blcoord {
    int  __x, __y, __sb;
    bool __swapped;

    blcoord():
        __x( -1 ),
        __y( -1 ),
        __sb( -1 ),
        __swapped( false )
    {}

    // Note: x,y will be reordered
    // to y,x if y<x!!!
    blcoord( int x, int y, int sb ):
        __x( std::min(x,y) ),
        __y( std::max(x,y) ),
        __sb( sb ),
        __swapped( y<x )
    {
    }
};
// implements streak-weak-ordering
// for blcoords:
// * 1st sort by ascending 'x'
// * 2nd sort by ascending 'y'
// * 3rd sort by ascending 'sb'
//
struct less_than_blcoord {
    bool operator()( const blcoord& l, const blcoord& r ) const
    {
        return ( (l.__x<r.__x) ||
                 (l.__x==r.__x && l.__y<r.__y) ||
                 (l.__x==r.__x && l.__y==r.__y && l.__sb<r.__sb) );
    }
};

ostream& operator<<(ostream& os, const blcoord& blc )
{
    return os << "(__x:" << blc.__x << ", __y: " << blc.__y
              << ", __sb: " << blc.__sb << ", __swapped=" << blc.__swapped << ")";
}

// transform a pcint correlatorboardnumber + recirculation cycle
// into a LOGHWID (as per Albert's defn)
albert::loghwid_t  tohwid( unsigned int cbnr, unsigned int recirc ) {
    return albert::generate_loghwid( cbnr%8, albert::CORR_BOARD,
                                     cbnr/8, albert::COR_UNIT,
                                     recirc );
}



// Find the list of baselines that
// consist only of interferometers 
// found in datafiles in the map
//
namespace pcint {
    enum anal_policy { loose, strict };
}

analres_t  analyze_this( const datafilemap& dfmap,
                         const pcint::DTD&  dtd,
                         const j2ms::log2polmap& l2p,
                         const pcint::anal_policy policy = pcint::loose )
{
    // temporary mapping from blcoord to baseline object
    // temporary mapping from blcoord to pcint_ifmap [polarization product -> interferometer]
//    typedef std::map<blcoord,pcint_baseline,less_than_blcoord>  blcache_t;
    typedef std::map<blcoord,pcint_ifmap,less_than_blcoord>     blcache_t;
    // using std::vector's constructor from two iterators (which can
    // be ordinary pointers!) we can construct the vector and
    // benefit of aaalllll STL's goodies (eg like searching..)
    typedef std::vector< ::DB_DATADESCR_TYPE>                    dbvector_t;
    typedef std::vector< ::INTFR_DATADESCR_TYPE>                 intfvector_t;

    // automatic variables
    blcache_t                    blcache;
    analres_t                    result;
    dbvector_t                   orgdblist( &dtd->DB_DATADESCR_ARRAY[0],
                                            // note: the end iterator must point
                                            // 'one-past-the-end' 
                                            // in the statement below but since
                                            // we're zero-based, the index NDATABLOCK
                                            // puts us exactly one-past-the-end...
                                            &dtd->DB_DATADESCR_ARRAY[dtd->NDATABLOCK] );
    intfvector_t                 orgintflist( &dtd->INTFR_DATADESCR_ARRAY[0],
                                              // see comment above..
                                              &dtd->INTFR_DATADESCR_ARRAY[dtd->NINTFR] );
    // Keep the array of found correlations
    correlations_t               corrsfound;
    albert::loghwid_t            hwid;
    datafilemap::const_iterator  curdf;

    // analysis starts here
    
    // we will, on the fly, deduce how many polarizations
    // per baseline to expect (could be either 1, 2 or 4)
    // [we can't tell it beforehand]
    // Afterwards, when inspecting the baselines,
    // we can verify if they have the number of
    // polarizations expected. Depending on policy
    // we could do one of two things
    // strict: throw out any baseline which doesn't
    //  have that number of polarizations, or,
    // loose:
    //  if a baseline is missing some polarizations,
    //  pad it with 'invalid' interferometers such
    //  that downstream we can insert default data
    //  and flag the data
    //
    // This remains to be decided

    // for each datafile...
    for( curdf=dfmap.begin();
         curdf!=dfmap.end();
         curdf++ )
    {
        // loop over all possible "recirculation cycles" ...
        for( unsigned int recirc=0;
             recirc < albert::maxrecirc;
             recirc++ )
        {
            unsigned int                 dboffs_words;
            dbvector_t::const_iterator   dbptr;
            intfvector_t::const_iterator ifptr;

            // check if that specific datablock is in the DTD
            // Note: the Key in the datafilemap is the PCInt Correlator BOARDNUMBER
            // which must be decomposed into a (unitnr,boardnr_within_unit)
            // pair..
            hwid = tohwid( curdf->first, recirc );
#if 0
            hwid = albert::generate_loghwid( curdf->first%8, albert::CORR_BOARD,
                                             curdf->first/8, albert::COR_UNIT,
                                             recirc);
#endif

            // try to find definition of this block in the datablocklist
            dbptr = find_if( orgdblist.begin(), orgdblist.end(),
                             Field(&::DB_DATADESCR_TYPE::LOGDBLOCKID)==hwid );

            // not found? break from the loop completely!
            // we're iterating over recirculation cycles and
            // it is impossible for a block for recirculation cycle 
            // n to exist IF any of the blocks for cycle {m | m<n}
            // does NOT exist!
            // That is to say: if block for cycle n does NOT exist,
            // then *certainly* the block for cycle n+1 does NOT exist
            // and by induction follows that blah-di-blah ...
            if( dbptr==orgdblist.end() )
                break;
            
            // add this datablock to the ones we expect
            //   we place it at the end of the already
            //   existing 'total datablock' after
            //   that we update the size
            //
            //   NOTE NOTE: Albert lists sizes in
            //   units of 32bit words, we work with
            //   bytes so make that translation
            //   (see "DB_TSIZE*4", below)
            pair<pcint_datablockset::iterator,bool>  dbinsres;
            
            dbinsres = result.datablocks.insert( pcint_datablock(hwid,
                                                                 result.totalsize,
                                                                 (unsigned int)dbptr->DB_TSIZE*4) );
            if( !dbinsres.second )
                throw failed_to_insert_into_map("PCIntVisBuffer.cc:analyze_this()"
                                                "/inserting new datablock into set!");
#if 0
            cerr << "analyze_this()/Inserted block ["
                 << "hwid=" << hwid << ", sz=" << dbinsres.first->size
                 << ", offs=" << dbinsres.first->offset
                 << endl;
            cerr << "   " << albert::decoded_hwid(hwid) << endl;
#endif

            // update the total datablock size (it has grown by 
            // a new block, after all...)
            // NOTE: we follow the value inserted into the map
            // and not the 'dbptr->DB_TSIZE' since we'll assume
            // that the value in the object in the map will be 
            // "leading" as that is the value that will be used
            // by other code in the processing chain!
            result.totalsize += dbinsres.first->size;

            // compute the offset of the start of this
            // newly inserted datablock in words such
            // that we may add this offset to the relative
            // offsets found in the DTD such as to arrive
            // at the direct offset to whatever we're looking for
            // (data, weights)
            dboffs_words = dbinsres.first->offset / 4;
            
            // for all interferometers in the block ....
            for( ifptr=orgintflist.begin() + dbptr->IF_DINDEX;
                 ifptr!=orgintflist.begin() + dbptr->IF_DINDEX + dbptr->NINTFR;
                 ifptr++ )
            {
                blcoord             blc( ifptr->XANTNR, ifptr->YANTNR, ifptr->SBANDNR );
                blcache_t::iterator curbl;

                // see if we already had this baseline
                if( (curbl=blcache.find(blc))==blcache.end() )
                {
                    // nope, insert a new entry
                    pair<blcache_t::iterator,bool>  insres;

                    insres = blcache.insert( make_pair(blc, pcint_ifmap()) );
                    if( !insres.second )
                    {
                        // aaaai big trouble!
                        // throw up!
                        throw failed_to_insert_into_map("PCIntVisBuffer.cc:analyze_this()");
                    }
                    // curbl point at newly inserted mapentry
                    curbl = insres.first;
                }

                // From here on we refer to 'curbl->first' for the current 
                // 'baseline coordinate' [blcoord]
                // and to 'curbl->second' if we want to access the 
                // 'polarizationcode-to-associated_interferometer mapping'
                // [pcint_ifmap].
                // 
                // Now add the interferometer to the baselineobject
                // insert it by correlation code and honour the 
                // __swapped property ...
                // NOTE: we must propagate the swapped property
                // so the rest of the system, namely the
                // part that interpretes the bytes from the 
                // correlator since this info is crucial
                // in delivering the lags in the right
                // order!
                //
                // And we must propagate the 'hardware id'
                // such that, later on, we may be able to tell
                // if data for this one had come in at all
                pcint_intf                       intf( ifptr->IF_DATATYPE,
                                                       ifptr->NLAG,
                                                       ifptr->IF_DATAOFFS + dboffs_words,
                                                       ifptr->VALCNTR + dboffs_words,
                                                       ifptr->INTFR,
                                                       curbl->first.__swapped,
                                                       hwid ); 
                CorrelationCode                  cc( l2p.find(ifptr->XPOLNR),
                                                     l2p.find(ifptr->YPOLNR) );
                pair<pcint_ifmap::iterator,bool> ifinsres;

                // if the interferometer was 'swapped', so is
                // the polarization code
                if( curbl->first.__swapped )
                    cc.swap();

                ifinsres = curbl->second.insert( make_pair(cc, intf) );
                if( !ifinsres.second )
                {
                    cerr << "\nDuplicate entry for baseline " << curbl->first
                         << ", correlation code=" << cc
                         << endl
                         << "[" << intf << "]"
                         << endl
                         << "Contents so far:" << endl;
                    for( pcint_ifmap::iterator ptr=curbl->second.begin();
                         ptr!=curbl->second.end();
                         ptr++ )
                    {
                        cerr << " CC[" << ptr->first << "] -> INTF=" << ptr->second << "\n";
                    }
                    throw failed_to_insert_into_map("PCIntVisBuffer.cc:analyze_this()/"
                                                    "Inserting pcint_intf into map?!");
                }
                // check if this correlation was already found and
                // append it to those found if not
                if( std::find(corrsfound.begin(), corrsfound.end(), cc)==corrsfound.end() )
                    corrsfound.push_back(cc);

                // Ow bugger!
                // Forgot a detail.... (smart thingy)
                // Albert is so smart that, for an
                // auto-correlation/cross-polarization
                // (so it's not *really* an auto-correlation
                //  but an auto-baseline, if you will)
                // the product is computed *ONLY ONCE*
                // because the other product is necessarily
                // identical so there's no use in wasting
                // correlator-space on computing it.
                // As said: we want nice, square, data-matrices
                // with the same number of interferometers per
                // baseline so we just add this fourth product
                // for auto-baseline/crosspol but with
                // the polarization labels reversed and let
                // it point at the same data as the 'third'
                // product! (where 'third' basically is
                // the "first non-auto-polarization product",
                // both "auto-polarization" products being
                // "first" and "second")

                // Albert's datatype bit encodings:
                //  bit0: set = autocorrelation, cross otherwise
                //  bit1: set = complex data, real data otherwise
                //  bit2: set = 1-bit data, 2-bit otherwise
                //
                // If this condition (see below) holds, it means
                // that we have Xant==Yant (ie auto-baseline)
                // and the "autocorrelation" flag is not set
                // therefore *implying* that we're looking at 
                // the cross-polarization product for a given antenna
                if( (curbl->first.__x==curbl->first.__y) &&
                    (ifptr->IF_DATATYPE&0x1)!=0x1 )
                {
                    // let's add the "reversed" polaration product
                    // and let it point at the same interferometer
                    cc.swap();

                    ifinsres = curbl->second.insert( make_pair(cc, intf) );
                    if( !ifinsres.second )
                    {
                        cerr << "\nWhilst inserting (non-physically-computed) fourth "
                             << "polarization product:\n"
                             << "Duplicate entry for baseline " << curbl->first
                             << ", correlation code=" << cc
                             << endl
                             << "[" << intf << "]"
                             << endl
                             << "Contents so far:" << endl;
                        for( pcint_ifmap::iterator ptr=curbl->second.begin();
                             ptr!=curbl->second.end();
                             ptr++ )
                        {
                            cerr << " CC[" << ptr->first << "] -> INTF=" << ptr->second << "\n";
                        }
                        throw failed_to_insert_into_map("PCIntVisBuffer.cc:analyze_this()/"
                                                        "Inserting fourth polarization into map?!");
                    }
                    // check if *this* correlation was already found and
                    // append it to those found if not
                    if( std::find(corrsfound.begin(), corrsfound.end(), cc)==corrsfound.end() )
                        corrsfound.push_back(cc);
                }
            } // loop over all interferometers in da blok
        } // loop over all recirculation cycles
    } // loop over all datafiles


    // ok now loop over all baselines in the cache
    // and decide what to to with baselines that
    // don't have 'npolperbaseline' interferometers;
    // see 'strict:' or 'loose:' in the comments above.
    //
    // We transfer, based on the by the policy accepted,
    // baselines from the temporary cache to the returnvalue
    blcache_t::iterator  curbl;

    for( curbl=blcache.begin();
         curbl!=blcache.end();
         curbl++ )
    {
        // start off with 'strict' policy
        //
        // HV 16/05/2006:
        //     Huib said that we want to
        //     recover as much as possible,
        //     so we'll use loose from now on

        // If the condition below holds, the baseline under consideriation
        // can be transferred to the return value.
        // Assert that ALL baselines in the resultset have identical
        // number-of-interferometers-per-baseline. 
        if( (policy==pcint::loose) ||
            (policy==pcint::strict && curbl->second.size()==corrsfound.size()) )
        {
            correlations_t::const_iterator  curcor;
            
            // if the number of interferometers for this baseline
            // is NOT equal to what we expect, fill it up with
            // default pcint_intfs() for the correlations that were
            // not present
            for( curcor=corrsfound.begin();
                 curbl->second.size()!=corrsfound.size();
                 curcor++ ) {
                if( curbl->second.find(*curcor)==curbl->second.end() )
                    curbl->second.insert( make_pair(*curcor, pcint_intf()) );
            }
            // Now we can construct the final pcint_baseline from
            // the 'blcoord' (which is the key in the cache) and
            // the pcint_ifmap (which is the mapped type in the cache).
            pcint_baseline   finalbl(curbl->first.__x, curbl->first.__y, curbl->first.__sb);
            
            finalbl.ifmap = curbl->second;

            result.baselines.push_back( finalbl );
        }
    }
    cerr << "\n\n******** PCINT ANALYZE_THIS FOUND THE FOLLOWING ********\n\n"
         << "  datablocks: " << result.datablocks.size() << " (out of "
         << orgdblist.size() << ")\n"
         << "   totalsize: " << result.totalsize << "\n"
         << "   baselines: " << result.baselines.size() << " (out of "
         << blcache.size() << " in cache)\n"
         << "   processed: " << orgintflist.size() << " INTFS\n"
         << "\n**********************************************************\n";
    // done!
    return result;
}




// local "Module"; we can use ordinary functions
// behaving as if they were a PCInt loadable module
// This one is for runmethod of the "input" chain of a PCIntProcessor
// The instance specific data should point to a 
// 'input_data'. The function will read a block from each
// file and transforms it into a datum which will then
// be sent down the processing chain
//
//
// Actually, we should compile a list of *baselines*
// that we should use and save that in the environment:
//
//    don't waste time/energy on (trying to) read baselines
//    for which we don't even have data: we have datafiles per 
//    correlatorboard and not all datafiles may be present and
//    it's not a valid assumption to assume that all interferometers
//    making up a baseline (1..4 interferometers/baseline, depending
//    on observation (multiple polarizations observed?) and correlator
//    configuration (compute cross poln's?)) are computed on a
//    single correlatorboard.
//    So we build a list of *baselines* that consist only of
//    interferometers we have datafiles for.
//    Do this by looping over "albert::maxrecirc" recirculation
//    cycles for each correlator board, check the DTD if there
//    is a DATABLOCK defined for this combination and if so,
//    investigate all INTERFEROMETERS in this datablock and
//    build baselines out of those.
//
typedef std::list<datafilemap::iterator> erasefilelist_t;

Retval<datumlist_t> read_data( processing& env, const moduleDef* mdef )
{
    input_data*                    ipdata( (input_data*)mdef->instanceSpecificData );
    datumlist_t                    rv;
    erasefilelist_t                files2erase;
    datafilemap::iterator          blokfile;
    erasefilelist_t::iterator      df2erase;
    pcint::blocklist_t::value_type blok;

    if( !ipdata )
        return Retval<datumlist_t>("read_data: No instance specific data");

    if( ipdata->dfMap.size()==0 )
        return Retval<datumlist_t>("read_data: All datafiles exhausted");

    // Keep on giving data as long as we haven't sucessfully
    // decoded a block (there is a "Module" which is excecuted
    // after a block has been succesfully decoded which sets this
    // flag to false. At this point, when we re-enter the "read_data" stage
    // and the flag is false, we'll return a "false" return value but set
    // the flag to true again such that upon a successive call to us,
    // we WILL (try to) give again until something successfully decoded
    if( !ipdata->give )
    {
        ipdata->give = true;
        return Retval<datumlist_t>("");
    }
    
    // attempt to read a block from the current datafile
    // if it's empty, remove the datafile from the
    // list (since giving an empty block signals
    // the fact that the datafile is-at EOF),
    // then try the next datafile.
    // If no datafiles left, we give up
    do
    {
        // if we did iterate past end-of-list, start again
        // (the iterator is incremented post- using it so
        //  we must check pre- using it)
        if( ipdata->curDatafile==ipdata->dfMap.end() )
            ipdata->curDatafile = ipdata->dfMap.begin();
        
        // read a block and save the current iterator
        // so we know whence the block came!
        blokfile = ipdata->curDatafile;
        blok     = blokfile->second->getNextBlock();

        // if the block's empty, this datafile is
        // exhausted. Schedule it for erasing.
        if( blok.size()==0 )
            files2erase.push_back( blokfile );

        // already move current datafileptr ahead so it's ready 
        // for when we next want to read a block
        ipdata->curDatafile++;
    }
    // loop until we've either successfully got a new block (.size()!=0)
    // OR all datafiles exhausted which will be signalled by the condition
    // that the number of datafiles to erase is equal to the number of
    // datafiles left in the datafilemap, 'dfMap'
    while( blok.size()==0 && (files2erase.size()!=ipdata->dfMap.size()) );

    // deal with deletion of datafiles. 
    // We remove the 'hardware-ids' associated with the data expected
    // from these files from the set of hardware-ids we expect.
    // (hardware ID is combination of correlator board number, unit number
    //  and recirculation cycle)
    for( df2erase=files2erase.begin();
         df2erase!=files2erase.end();
         df2erase++ ) {
       
        // before erasing, take out the hardware ids
        try {
            labelset_t<>&                ls( env.symbol_table );
            pcint_datablockset&          dbset( ls.get<pcint_datablockset>("datablockset") );
            pcint_datablockset::iterator curdb;

            // we can just loop over recirc = 0 to whatever until
            // we cannot find the hardwareid anymore since
            // if the hardwareid for recirc = n isn't present,
            // then the hardwareid for recirc = n+1 cannot be present
            for( unsigned int recirc=0;
                 (curdb=finddb_byhwid(dbset, tohwid((*df2erase)->first, recirc)))!=dbset.end();
                 recirc++ ) {
                dbset.erase(curdb);
            }
        }
        catch( ... ) {
            // if 'n exception thrown that's not very harmfull, it basically
            // means that the 'datablockset' wasn't there in the first place
            // so we don't have to delete stuff from it...
        }

        cout << "\nread_data: " << (*df2erase)->second->dfName() << " exhausted\n";
        // ok, now delete the datafilepointer, we don't need it
        // no more, now do we?
        delete (*df2erase)->second;

        // and remove the entry from da map
        ipdata->dfMap.erase( (*df2erase) );
    }
    
    // Only transform the blok into a 'datum_t' if we had one
    if( blok.size() )
    {
        // change the 'blok' (which is of type CBDataBlock)
        // into a 'datum_t'
        datum_t     d;
        
        d.data    = RAWDataBlock( blok.data.begin(), blok.data.size() );
        
        // for the evn processing modules, we must attach the 
        // LOGHWID of this block to the metadata
        // NOTE: the blokfile->first is the "pcint physical correlator
        // board number (0..31)" but in alberts logical hardware id,
        // boardnumber ranges from (0..<num_boards_per_unit>), which is
        // '8' so we must transform it from a linear index to
        // a (UNIT, BOARD) tuple where UNIT={UNIT| UNIT in [0..3]} and BOARD={BOARD| BOARD in [0..7]}
        if( env.debug )
        {
            cerr << "\nread_data: Setting metadata 'loghwid' for "
                 << "first=" << blokfile->first << ", blok.info.recirc_cycle="
                 << blok.info.recirc_cycle
                 << endl;
        }
        d.metadata.addLabel<albert::loghwid_t>( "loghwid",
                                                albert::generate_loghwid(
                                                    blokfile->first%evn::NBOARD_PER_UNIT,
                                                    albert::CORR_BOARD,
                                                    blokfile->first/evn::NBOARD_PER_UNIT,
                                                    albert::COR_UNIT,
                                                    blok.info.recirc_cycle) );
        rv.push_back( d );
    }

    return Retval<datumlist_t>(rv);
}

Retval<datumlist_t> read_data_org( processing& env, const moduleDef* mdef )
{
    bool                           allempty, did_reset;
    input_data*                    ipdata( (input_data*)mdef->instanceSpecificData );
    datumlist_t                    rv;
    datafilemap::const_iterator    blokfile;
    pcint::blocklist_t::value_type blok;

#if 0
static unsigned int     num( 0 );
if( num<10 ) {
    num++;
} else {
    cout << '.' << flush;
    return Retval<datumlist_t>("Exhausted!");
}
#endif
    if( !ipdata )
        return Retval<datumlist_t>("read_data: No instance specific data");

    // Keep on giving data as long as we haven't sucessfully
    // decoded a block (there is a "Module" which is excecuted
    // after a block has been succesfully decoded which sets this
    // flag to false. At this point, when we re-enter the "read_data" stage
    // and the flag is false, we'll return a "false" return value but set
    // the flag to true again such that upon a successive call to us,
    // we WILL give (again)
    if( !ipdata->give )
    {
        ipdata->give = true;
        return Retval<datumlist_t>("");
    }
    
    // attempt to read a block from the current datafile
    // if it's empty try the next datafile and if none
    // of the datafiles gives a non-empty block
    // (ie they're all exhausted) we give up
    allempty  = false;
    did_reset = false;
    do
    {
        if( ipdata->curDatafile==ipdata->dfMap.end() )
        {
            // if we did a reset and we enter this condition
            // again this implies we've gone round all of the
            // datafiles and none of them gave a non-empty block
            // which in turn implies that all of them are empty!
            if( did_reset )
            {
                allempty = true;
                continue;
            }
            ipdata->curDatafile = ipdata->dfMap.begin();
            did_reset           = true;
        }
        
        // read a block and save the current iterator
        // so we know whence the block came!
        blokfile = ipdata->curDatafile;
        blok     = blokfile->second->getNextBlock();

        // already move current datafileptr ahead so it's ready 
        // for when we next want to read a block
        ipdata->curDatafile++;
    }
    while( blok.size()==0 && !allempty );

    if( blok.size() )
    {
        // change the 'blok' (which is of type CBDataBlock)
        // into a 'datum_t'
        datum_t     d;
        
        d.data    = RAWDataBlock( blok.data.begin(), blok.data.size() );
        
        // for the evn processing modules, we must attach the 
        // LOGHWID of this block to the metadata
        // NOTE: the blokfile->first is the "pcint physical correlator
        // board number (0..31)" but in alberts logical hardware id,
        // boardnumber ranges from (0..<num_boards_per_unit>), which is
        // '8' so we must transform it from a linear index to
        // a (UNIT, BOARD) tuple where UNIT={UNIT| UNIT in [0..3]} and BOARD={BOARD| BOARD in [0..7]}
        if( env.debug )
        {
            cerr << "\nread_data: Setting metadata 'loghwid' for "
                 << "first=" << blokfile->first << ", blok.info.recirc_cycle="
                 << blok.info.recirc_cycle
                 << endl;
        }
        d.metadata.addLabel<albert::loghwid_t>( "loghwid",
                                                albert::generate_loghwid(
                                                    blokfile->first%evn::NBOARD_PER_UNIT,
                                                    albert::CORR_BOARD,
                                                    blokfile->first/evn::NBOARD_PER_UNIT,
                                                    albert::COR_UNIT,
                                                    blok.info.recirc_cycle) );
        rv.push_back( d );
    }

    return Retval<datumlist_t>(rv);
}
// this "module" is supposed to be executed after
// a "append_block_to_list", it tells the
// 'input_data' (the instance specific data should
// point to an instance of input data and it will
// set the "give" member to false such that
// the read_data method will stop giving data.
// In turn, the read_data will reset it to true
// (if it was false). As a result, the 'read_data'
// method will continue to give data until
// this module has been executed
Retval<bool> inhibit_read_data( pcint::datum_t&, pcint::processing&, const moduleDef* mptr )
{
    Retval<bool>   rv( (mptr!=0) );
    input_data*    ipdata( ((bool)rv)?((input_data*)mptr->instanceSpecificData):(0) );

    if( (rv=(ipdata!=0))==true )
    {
        ipdata->give = false;
    }
    return rv;
}



void cleanup_input_data( const moduleDef* mdef )
{
    input_data*                    ipdata( (input_data*)mdef->instanceSpecificData );

    cerr << "Cleaning up input data" << endl;
    // destroy the ipdata thingy.
    // The d'tor of ipdata makes sure that
    // the entries in the map get deleted
    delete ipdata;
}

#if 0
// This pseudomodule (read: function) appends the datum to
// a datumlist (which could be a list
// in a different object)
// The instance-specific data should point
// at a datumlist_t
Retval<bool> append_datum_to_list( pcint::datum_t& d, pcint::processing&, const moduleDef* mptr )
{
    Retval<bool>   rv( (mptr!=0) );
    datumlist_t*   dlptr( ((bool)rv)?((datumlist_t*)mptr->instanceSpecificData):(0) );

    if( dlptr )
    {
        dlptr->push_back( d );
        rv = true;
    }
    return rv;
}
#endif

// This pseudomodule (attempts to) aggregate datums into 
// a pcint_integration. A datum basically is one integration
// of one recirculation cycle from one correlator board
// so, in general, one integration needs data from 
//    n_cb * n_recirc datums
// where
//   n_cb      is the number of correlator boards read out
//   n_recirc  the recirculation factor (which may be >1 ...!)
//
// This 'module' inspects the (potentially processed) datums
// and attempts to glue them together into a single
// integration. If all datums for the integration are found,
// the integration is appended to a list of integrations
// (which would typically be a datamember of a PCIntVisBuffer object)
//
// If a datum is found that is already present in the current
// integration this is taken to mean that the previous integration
// is NOT complete and as such it is dropped and a new integration cycle
// is started.
//
// When a new integration is started is it initialized with
// the current values of 'baselinelist' and 'datablockset'
// found in the environment
// This allows the "input" chain to define the settings for
// the current integration, per integration [optional].
//  (ie if the input detects that some data is corrupt,
//   it could build a new baselinelist based on the 
//   remaining datastreams to salvage whatever data it 
//   can)
//
//
// Note: the (partial) integrations are stored in the environment
// such that this (pseudo) module may be run in parallel in
// different environments making sure they do not mess each other's
// state up
//
// The instance specific data for this module
// should point at an instance of a list of pcint_integrations
// (such that if we have a full integration we can append
//  it to that list)

struct failed_to_add_label:
    public std::exception
{};
struct failed_to_insert_new_integration:
    public std::exception
{};

// the key to an integration: the start/end framenumber + 
// a windingnumber
struct intgkey {
    int          windingnumber;
    unsigned int start_bocf;
    unsigned int end_bocf;

    intgkey():
        windingnumber( 0 ),
        start_bocf( 0xffffffff ),
        end_bocf( 0xffffffff )
    {}

    intgkey( unsigned int s, unsigned int e, int w ):
        windingnumber( w ),
        start_bocf( s ),
        end_bocf( e )
    {}
};

std::ostream& operator<<(std::ostream& os, const intgkey& k ) {
    return os << "(s/e=" << k.start_bocf << '/' << k.end_bocf
              << " @" << k.windingnumber << ')';
}

// need to have a comparator which implements 'strict weak
// ordering' for intgkey
// We order integrations by their start-framenumber plus
// winding number [the bocf numbers wrap every whole
// 10 minutes of wall-clock time (Replayed Observing Time,
// really]
struct less_than_intgkey {
    bool operator()( const intgkey& l, const intgkey& r ) const {
        return ( l.windingnumber<r.windingnumber ||
                 (l.windingnumber==r.windingnumber && l.start_bocf<r.start_bocf) );
    }
};

// Keep track of windingnumber per datafile such
// as to increase the framenumber range by 
// 2*10^9 .... ;)
struct windingnumber_t {
    int          windingnumber;
    unsigned int last_start_bocf;

    windingnumber_t():
        windingnumber( 0 ),
        last_start_bocf( 0xffffffff )
    {}
     
    windingnumber_t( unsigned int s, int w ):
        windingnumber( w ),
        last_start_bocf( s )
    {}
};

// and stick those in a map.
// The Key is 'correlatorboardnumber'
// or datafilenumber or anything which maps
// onto identifying which file the data came from
typedef std::map<int,windingnumber_t> windingmap_t;

// (incomplete) integrations are put in a map
// of intgkey -> pcint_integration
typedef std::map<intgkey, pcint_integration,less_than_intgkey> integration_map;

// predicate functor which returns true
// if the arguments's second value has 'data_present==true'
struct has_data_present {
    bool operator()( const pcint_ifmap::value_type& t ) const {
        return t.second.data_present;
    }
};


Retval<bool> integrate_datums_fake( pcint::datum_t&, pcint::processing& env,
                               const moduleDef* mptr )
{
    Retval<bool>   rv( false );

    // if no instance specific data -> get the beep outta here!
    if( (!mptr) || (mptr && !mptr->instanceSpecificData) )
    {
        return Retval<bool>("PCIntVisBuffer.cc:integrate_datums()/No instance specific data?");
    }
    
    // ok let's see what we can make of this
    try
    {
        static int              count( 5068 );
        labelset_t<>&           ls( env.symbol_table );
        pcint_integrationlist*  ilptr( (pcint_integrationlist*)mptr->instanceSpecificData );

        if( count && !(count%20) ) {
            unsigned int         dbsize( ls.get<unsigned int>("datablocksize") );
            pcint_baselinelist   blist( ls.get<pcint_baselinelist>("baselinelist") );
            pcint_datablockset   dbset( ls.get<pcint_datablockset>("datablockset") );
            pcint_integration    newintg( dbsize, dbset, blist );

            newintg.start_bocf = count;
            newintg.end_bocf   = count+1;
            ilptr->push_back( newintg );
            
            newintg.start_bocf = count+1;
            newintg.end_bocf   = count+2;
            ilptr->push_back( newintg );


            if( count>600*16 )
                count=0;
            rv = true;
        }
        count++;
    }
    catch( ... )
    {
        rv = Retval<bool>("Caught exception");
    }
    return rv;
}

Retval<bool> integrate_datums( pcint::datum_t& d, pcint::processing& env,
                               const moduleDef* mptr )
{
    Retval<bool>   rv( (mptr!=0)?(mptr->instanceSpecificData!=0):(false) );

    // if no instance specific data -> get the beep outta here!
    if( !rv )
    {
        return Retval<bool>("PCIntVisBuffer.cc:integrate_datums()/No instance specific data?");
    }
    
    // ok let's see what we can make of this
    try
    {
        labelset_t<>&           ls( env.symbol_table );
        labelset_t<>&           blockinfo( d.metadata );

        // retrieve some of the metadata and 'global' evn processing
        // parameters
        unsigned int            start_bocf( blockinfo.get<unsigned int>("start_bocf") );
        unsigned int            end_bocf( blockinfo.get<unsigned int>("end_bocf") );
        unsigned int            l_end_bocf;
        unsigned int            bocfpintg( ls.get<unsigned int>("bocfpintg") );
        unsigned int            bocfrate( ls.get<unsigned int>("bocfrate") );
        albert::loghwid_t       curhwid( blockinfo.get<albert::loghwid_t>("loghwid") );
        albert::decoded_hwid    decodedid( curhwid );
       
        // framenumber wrap detection stuff
        //  (the assertions make sure that if the label wasn't
        //   there yet it will be created with the value given,
        //   if the label was there, it's left alone)
        //
        windingmap_t&           windingmap( ls.assert_label<windingmap_t>("windingmap") );
        windingmap_t::iterator  curwinding;
        unsigned int&           g_start( ls.assert_label<unsigned int>("g_start", 0xffffffff) );

        // this is where we're supposed to put our stuff
        pcint_integrationlist*  ilptr( (pcint_integrationlist*)mptr->instanceSpecificData );

        // if no valid bocfs, nothing to do here anyway
        // we discard that data
        if( start_bocf==0xffffffff || end_bocf==0xffffffff )
            return Retval<bool>("No framenumbers. Discarding data");

        // verify that the difference between start and end bocf are what we
        // expect. Otherwise we might confuse our integration algorithm
        // with bogus data (ie it would start a new integration based
        // on bogus framenumbers, as the pair 'start,end' is taken to
        // be the key to an integration, in order to glue data
        // together)
        //
        // Note: we keep the (possibly) modified last end bocf as
        // a local variable.
        //
        // For l_end_bocf the following invariant holds:
        //    start_bocf <= l_end_bocf
        // (for end_bocf this may not hold because of framenumber wrap)

        l_end_bocf = end_bocf;
        
        if( l_end_bocf<start_bocf )
            l_end_bocf += 600 * bocfrate;
        
        if(  (l_end_bocf-start_bocf) != (bocfpintg-1) ) {
            return Retval<bool>("Framenumbers not consistent with #-of-bocfs-per-integration");
        }


        // find (and possibly initialize) and
        // possibly update the windingnumber for the datafile
        // this datum came from. We use the correlatorboard number.
        // Assumption is that it's uniquely enough...
        int    boardnum = decodedid.boardnr + 32*decodedid.unitnr;
        if( (curwinding=windingmap.find(boardnum))==windingmap.end() ) {
            // attempt the determine the windingnumber.
            int                                wnum( 0 );
            pair<windingmap_t::iterator,bool>  insres;

            // ok must try to determine the windingnumber of 
            // this datafile; we haven't seen data from it
            // before.
            // If we haven't seen any data in this environment
            // before, we initialize the whole shebang to this'un :)
            if( g_start!=0xffffffff ) {
                double    last_sec, now_sec, diff;

                last_sec = (double)g_start/(double)bocfrate;
                now_sec  = (double)start_bocf/(double)bocfrate;

                diff = now_sec - last_sec;

                // Ok. See what we got.
                // Under normal circumstances (and/or) dataloss
                // we may have framenumbers jumping around by some
                // amount. The assumption, though, is, that the data
                // from the different files should be roughly at the
                // same time. Ie jumps of more than 300seconds
                // (in either direction) are taken to be data from
                // a previous or next 10-minute-interval. 
                // If we loose more than 5 minutes of data out of
                // a stream we're propably up the creek anyhow!
                // Most scans nowadays don't even last that long...
                if( ::fabs(diff)>300.0 ) {
                    if( diff<0.0 ) {
                        // Jump of more than 300seconds back in time?
                        // It's more likely that the data we got was 
                        // from the new (next) 10 minute interval
                        wnum = + 1;
                    } else {
                        wnum = - 1;
                    }
                }
            } else {
                // initialize g_last_start
                // global winding number is initialized to zero
                // anyhow
                g_start = start_bocf;
            }

            // Ok, we now have a windingnumber + a last start_bocf
            // for the current datafile
            insres = windingmap.insert( make_pair(boardnum, windingnumber_t(start_bocf, wnum)) );
            if( !insres.second ) {
                cerr << "PCIntVisBuffer.cc/integrate_datums() - Failed to insert "
                     << "new entry in windingnumber map for decoded hardware ID "
                     << decodedid << endl;
                throw failed_to_insert_into_map(" windingnumber_t into windingmap.");
            }
            curwinding = insres.first;
        }
       
        // now that we have a windingnumber_t (which is per
        // datafile so we may rest that there will be no
        // negative jumps, other than when the framenumbers wrap.
        // If the new start bocf is < last recorded bocf for a datastream,
        // the framenumbers will have wrapped. [== is acceptable since
        // when using recirculation, the same framenumber will be seen
        // 'num_recirc' times]
        if( start_bocf<curwinding->second.last_start_bocf ) {
            curwinding->second.windingnumber++;
        }
        // WTF! Did forget to update the last known good
        // start BOCF for the current datafile/correlatorboard.
        // Bugger!
        curwinding->second.last_start_bocf = start_bocf;

        // first, make sure there exists
        // a 'integration_map' thingy
        // in the environment
        // and given the windingnumber for the current file,
        // we may *finally* construct the integration key!
        intgkey                    curkey( start_bocf, end_bocf, curwinding->second.windingnumber );
        integration_map&           intgmap( ls.assert_label<integration_map>("intgmap") );
        integration_map::iterator  curintg;

        // see if we already got a definition for this integrationkey
        if( (curintg=intgmap.find(curkey))==intgmap.end() ) {
            // nope. add a new one
            //
            // If we need to add a new integration
            // we take its construction parameters
            // from the environment too
            //
            // Note: these methods throw up if they
            // can't find the label or it has the
            // wrong type...
            unsigned int         dbsize( ls.get<unsigned int>("datablocksize") );
            pcint_baselinelist   blist( ls.get<pcint_baselinelist>("baselinelist") );
            pcint_datablockset   dbset( ls.get<pcint_datablockset>("datablockset") );
            std::pair<integration_map::iterator, bool> insres;

            insres = intgmap.insert( make_pair(curkey,
                                               pcint_integration(dbsize, dbset, blist)) );
            if( !insres.second ) {
                cerr << "PCIntVisBuffer::integrate_datums()/"
                     << "Failed to add new integration to map "
                     << "for " << curkey << endl;
                throw failed_to_insert_new_integration();
            }

            curintg = insres.first;
            cout << "intgmap: added new integration, key=" << curkey << ", size="
                 << intgmap.size() << endl;
        }

        // cur_intg points at a <Key,Value> pair <first,second>
        // where the Key is the integration key (the pair of start/end
        // framenumbers+windingnumber) and the Value is a pcint integration.

        
        // if we get data which we don't expect, discard it!
        pcint_integration&           intgref( curintg->second );
        pcint_datablockset::iterator dbiter;

        if( (dbiter=finddb_byhwid(intgref.datablocks, curhwid))==intgref.datablocks.end() )
        {
            if( env.debug ) {
                cout << "PCIntVisBuffer::integrate_datums()/ "
                     << "Found unexpected datablock? The current "
                     << "datablock is not found in the required "
                     << "set. Current datablock="
                     << albert::decoded_hwid(curhwid)
                     << endl;
            }
            return Retval<bool>("Unexpected data. Discarding it");
        }
       
        // ok check if the datablock's present flag
        // was set or not
        // Really it shouldn't be, since that would indicate
        // that the 'hardwareID','start_bocf','end_bocf' triplet
        // arrived twice; ie the datablock for a certain correlator board
        // in the current integration was already seen, which is a
        // violation as each datablock should be unique within
        // any given integration.
        if( dbiter->present ) {
            if( env.debug ) {
                cout << "PCIntVisBuffer.cc:integrate_datums()/Found duplicate "
                     << "datablock for hardwareid " << albert::decoded_hwid(curhwid)
                     << " and integration " << curkey
                     << endl;
            }
            return Retval<bool>("Duplicate data! Discarding it");
        }

        // dbiter points at a pcint_datablock which is not yet present
        // set present flag to true and copy the datum's data into the
        // 'big' datablock at the specified location
        // Note: if the supplied datablock is bigger than what was
        // found during analysis of the DataTransferDescriptor the
        // remainder of the supplied datum is NOT copied across.
        dbiter->present = true;
        ::memcpy( intgref.data.begin()+dbiter->offset,
                  d.data.begin(), min(dbiter->size, d.data.size()) );

        // now be smart (tm) [famous last words eh] ...
        //
        // In lieu of data-loss and asynchronicity
        // we must be able to cope with the fact that we may be 
        // integration a number of integrations simultaneuosly,
        // waiting for remaining data to arrive.
        //
        // If we end up here, *some* data has arrived
        // We can now go through the list of pending
        // integrations and for those who are waiting
        // for data from this hardware id, remove it
        // from their list [since it ain't going to 
        // arrive anymore - we've lost data up till
        // whatever BOCF we have at hand now].
        //
        // If, after removal of this datablock from
        // the list of expected datablocks, all remaing
        // blocks' present flags are 'true', we may
        // pass this integration along.
        //
        // The integrations are put in the map
        // in time order (taking into account of the 
        // windingnumber of the bocf-framenumbers)
        // so we may iterate over the integrations
        // with a Key less than the current one
        // and remove the current hardwareid 
        // (if the present flag was false).
        //
        integration_map::iterator tmpintg;

        for( tmpintg=intgmap.begin();
             tmpintg!=intgmap.upper_bound(curkey);
             tmpintg++ ) {

            // look in the integration's datablocklist
            // for the current hardwareid
            pcint_datablockset::iterator tmpdbiter;

            // if the hardware-ID is in the list
            // AND it was not flagged as present yet,
            // we must remove it since it ain't gonna
            // arrive no more.
            if( (tmpdbiter=finddb_byhwid(tmpintg->second.datablocks, curhwid)) !=
                 tmpintg->second.datablocks.end() &&
                tmpdbiter->present==false ) {
                tmpintg->second.datablocks.erase( tmpdbiter );
            }
        }

        // Loop again over (this time: all) the integrations and check if
        // they're complete
        // We must keep a list of pointers to erase
        // since looping over thing you're (potentially) erasing stuff
        // from is bad practice (and if you're using STL containers
        // it will even fail to do the right thing....)
        integration_map&                               igmap( ls.get<integration_map>("intgmap") );
        std::list<integration_map::iterator>           toremove;
        std::list<integration_map::iterator>::iterator currem;
        
        for( tmpintg=igmap.begin();
             tmpintg!=igmap.end();
             tmpintg++ )
        {
            pcint_integration&           iref( tmpintg->second );
            pcint_datablockset::iterator tmpdbiter;

            // that is: if we find a block whose presence==false
            // the integration is not yet complete!
            tmpdbiter = find_if( iref.datablocks.begin(),
                                 iref.datablocks.end(),
                                 Field(&pcint_datablock::present)==false );

            if( tmpdbiter==iref.datablocks.end() ) {
                set<albert::loghwid_t>               hwids;
                pcint_baselinelist::iterator         curbl;
                list<pcint_baselinelist::iterator>   bls2remove;
                
                // transfer the framenumbers to the integration
                // This really sucks, I realise, but so far
                // the framenumbers were kept as the Key to the
                // integration
                iref.start_bocf = tmpintg->first.start_bocf;
                iref.end_bocf   = tmpintg->first.end_bocf;

                // Gather a list of hardwareids that are present
                // in this integration such that we may flag all
                // interferometers that are present.
                // Note: we reuse the 'tmpdbiter' since we
                // don't need that value anymore
                for( tmpdbiter=iref.datablocks.begin();
                     tmpdbiter!=iref.datablocks.end();
                     tmpdbiter++ ) {
                    hwids.insert( tmpdbiter->loghwid );
                }
        
                // now loop over all baselines and interferometers
                // and if the hwid of the interferometer is in the
                // set-of-present-hardware-ids then flag it as
                // having data.
                // Whilst we're at it, we may as well count
                // the number of interferometers of a baseline
                // that have data and if it equals zero,
                // we dispose of that baseline altogether
                for( curbl=iref.baselines.begin();
                     curbl!=iref.baselines.end();
                     curbl++ )
                {
                    unsigned int           num_ifs_with_data( 0 );   
                    pcint_ifmap::iterator  curif;
                    for( curif=curbl->ifmap.begin();
                         curif!=curbl->ifmap.end();
                         curif++ ) {
                        if( hwids.find(curif->second.loghwid)!=hwids.end() ) {
                            curif->second.data_present = true;
                            num_ifs_with_data++;
                        }
                    }
                    if( num_ifs_with_data==0 )
                        bls2remove.push_back( curbl );
                }
    
                // now really remove them from the list
                list<pcint_baselinelist::iterator>::iterator tmpbl;

                for( tmpbl=bls2remove.begin();
                     tmpbl!=bls2remove.end();
                     tmpbl++ ) {
                    iref.baselines.erase( *tmpbl );
                }
                if( iref.baselines.size()>0 ) {
                    cout << "\nintegrate_datums: full integration at " << tmpintg->first << endl;
                    // stick the integration at the end of the list
                    ilptr->push_back( iref );
                } 
                // and list this one as 'to-be-removed-from-the-
                // list-of-pending-integrations'
                toremove.push_back( tmpintg );
            }
        }

        unsigned int   psiz = igmap.size();
        for( currem=toremove.begin();
             currem!=toremove.end();
             currem++ ) {
            igmap.erase( *currem );
        }
        if( igmap.size()!=psiz )
            cout << "\nintegrate_datums: intgmap.size() went from "
                 << psiz << " -> " << igmap.size()
                 << endl;

        // do not forget to set the returnvalue.
        // We should return true if we found
        // one or more complete integrations
        rv = (ilptr->size()>0);
    }
    catch( const std::exception& e )
    {
        rv = Retval<bool>(string("Caught an exception: ")+e.what());
    }
    catch( ... )
    {
        rv = Retval<bool>("Caught an unknown exception: ");
    }
    return rv;
}



//
//
//
//
//               Here starts the 'real' PCIntVisBuffer object
//
//
//
//




PCIntVisBuffer::PCIntVisBuffer( unsigned int jobid, unsigned int sjid,
                                const VEXperiment* experimentptr,
                                const string& vbopt ) :
    VisibilityBuffer( experimentptr ),
    myJobID( jobid ),
    mySubJobID( sjid ),
    myOptions( ::split(vbopt,',') ),
    last_end_bocf( 0xffffffff )
{
    string       vexfile;
    stringstream strm;
    
    //  Form the directoryname where we should look for data in the first
    //  place...
    strm.str( string() );
    strm << this->getExperiment().getRootDir() << "/"
         << myJobID << "/"
		 << mySubJobID;
    
    myRootDir = strm.str();

    // First things first - get the correlator setup from the o/p VEXfile
    // if that fails -> something's stuffed
    strm.str( string() );
    strm << this->getExperiment().getRootDir() << "/" << myJobID << "/"
         << this->getExperiment().getCode() << "_" << myJobID << ".vex";
    vexfile = strm.str();

    // ...::correlatorSetupFromFile() returns default CorrelatorSetup()
    // if failure to read
    mySetup = CorrelatorSetup::correlatorSetupFromFile( vexfile );
    if( mySetup==CorrelatorSetup() )
    {
        THROW_JEXCEPT( "PCIntVisBuffer - Failed to read correlatorsetup?!" );
    }
    
    // Should try to retrieve the mappings
    // The pcint uses string as job/subjod id... so transform 'm
    string           jobid_str;
    string           subjobid_str;
    GlobalArea       ga;
    DTDQueryResult   qdtdr;
    CBDSQueryResult  qcbr;
    DSDDQueryResult  qdsr;
    
    strm.str( string() );
    strm << myJobID;
    jobid_str = strm.str();

    strm.str( string() );
    strm << mySubJobID;
    subjobid_str = strm.str();

    // attempt to fetch...
    qcbr = ga.queryCBDSMapping( string(this->getExperiment().getCode()),
                                jobid_str, subjobid_str );
    qdsr = ga.queryDSDDMapping( string(this->getExperiment().getCode()),
                                jobid_str );
    qdtdr = ga.queryDTD( string(this->getExperiment().getCode()), jobid_str );

    if( !qcbr || !qdsr || !qdtdr )
    {
        strm.str( string() );
        strm << "PCIntVisBuffer/Failed to retrieve mapping(s) - \n";
        if( !qcbr )
        {
            strm << "CBDSMapping> " <<  qcbr.getMessage() << "\n";
        }
        if( !qdsr )
        {
            strm << "DSDDMapping> " << qdsr.getMessage() << "\n";
        }
        if( !qdtdr )
        {
            strm << "DTD> " << qdtdr.getMessage() << "\n";
        }
        THROW_JEXCEPT( strm.str() );
    }

    myCBDSmap = qcbr.get();
    myDSDDmap = qdsr.get();
    myDTD     = qdtdr.get();

    // read scans from the vexfile
    myScanList = scanlist( vexfile /*, mySetup*/ );
    if( myScanList.empty() )
    {
        THROW_JEXCEPT( "PCIntVisBuffer/No scans were found in VEXfile '"
                       << vexfile << "'" );
    }

    // create the COFScans
    scaninfolist_t::const_iterator cursi;

    for( cursi=myScanList.begin();
         cursi!=myScanList.end();
         cursi++ )
    {
        myScanPtrs.push_back( new j2ms::COFScan(this, *cursi) );
    }

    // well err... that's it?
    // for now... we *definitely* need to to MUCH more before
    // we even can read back a single visibility ;)

    // for each file in the DSDDMap, attempt to create
    // a 'datafile' entry for it eh
    // we do this like eh by first unmapping the 
    // DataStreamId to like eh the CorrelatorBoardID
    // using the CBDS-map and then inserting
    // an entry in the datafilemap which goes from
    // CB-id directly to the datafile (the stream ID
    // is irrelevant)
    datafilemap               dfmap;
    dsddmap_t::const_iterator curdsdd;

    // loop over all the DSDDmappings and like
    // find the cbdsmapentry to go with it eh
    for( curdsdd=myDSDDmap.begin();
         curdsdd!=myDSDDmap.end();
         curdsdd++ )
    {
        ostringstream                    dfname;
        cbdsmap_t::const_iterator        curcbds;
        pair<datafilemap::iterator,bool> insres;

        // use like the fieldmatcher from 'algortihmhelper.h',
        // found in PCInt eh to find the CBDS that describes
        // the current DS
        curcbds = find_cbds_byds(myCBDSmap, curdsdd->dataStream);
        if( curcbds==myCBDSmap.end() )
        {
            cout << "CBDSMap:" << endl;
            for_each(myCBDSmap.begin(), myCBDSmap.end(),
                     printfunctor(cout, ";") );
            cout << "DSDDMap:" << endl;
            for_each(myDSDDmap.begin(), myDSDDmap.end(),
                     printfunctor(cout, ";") );
            // aaaargh!
            THROW_JEXCEPT("\n\nConfig error?\n"
                          << "Failed to find an entry in the CBDSmap for DataStream '"
                          << curdsdd->dataStream << "' whilst the DataStream was found "
                          << "in the DSDD mapping?!");
        }

        // generate filename. DDDProcess creates
        // <dddDisk>/<expId>/<jobId>/<subjobId>/[raw]CDF-<dataStream>
        dfname << curdsdd->dddDisk
               << "/" << this->getExperiment().getCode()
               << "/" << myJobID
               << "/" << mySubJobID
               << "/rawCDF." << curdsdd->dataStream;
        insres = dfmap.insert( make_pair(curcbds->physCB.physicalPosition,
                                         new datafile(dfname.str())) );
        if( !insres.second )
        {
            THROW_JEXCEPT("\n\nFailed to insert entry in datafilemap for stream "
                           << curdsdd->dataStream << "\n\n");
        }
        cerr << "DataFilemap-entry: " << curdsdd->dataStream
             << " [" << curdsdd->dddDisk << "]" << endl;
    }
    // if we, like, end up here we sort of did create all
    // the datafiles OK eh? otherwise sum dumb 'ception would
    // be thrown eh?

    // Now get extra info from the JCCS database:
    // we need: the bocfrate and the (scheduled)
    // scan-start-time.
    // This info is needed to set up the BOCF-to-time
    // translator. 
    // We use the JCCS code for that (encapsulates
    // the database access)
    MVTime            scanstart;
    SQLRow            rij;
    SQLLnkId          jccsdb;
    SQLResId          sqlresult;
    unsigned int      bocfrate( 1 );
    unsigned int      bocfpintg( 1 );
    ostringstream     sql;

    if( (jccsdb=SQLOpenConn())==0 ) {
        // cannot reach the JCCS database. We're stuffed
        cerr << "PCIntVisBuffer/ Failed to open connection to JCCS Database" << endl;
        throw JCCS_database_inaccessible();
    }

    // now look for the entry for our job-id
    sql << "SELECT  PJ.jobid, SJ.bocfrate, "
        << "DATE_FORMAT(SJ.schedstart, '%Y/%m/%d/%T') FROM ProcJob AS PJ "
        << "LEFT JOIN SubJob AS SJ ON PJ.pjid=SJ.pjid WHERE PJ.jobid='"
        << myJobID << "';";

    if( (sqlresult=SQLQuery(jccsdb, sql.str()))==0 ) {
        cerr << endl
             << "PCIntVisBuffer: Failed to query JCCS database for vital info "
             << "regarding jobId" << myJobID << endl;
        throw JCCS_database_query_failure();
    }

    // ok now get the results out of the query
    if( SQLNumRows(sqlresult)!=1 ) {
        // database skrewt -> not exactly one match for jobid 'myJobID'
        // but the jobID is supposed to be unique.
        // if zero matches found: no info known, if >1 found, jobID is not
        // unique...
        cerr << "PCIntVisBuffer: Query for job-related info from JCCS DB "
             << "did not yield exactly one match, it found " << SQLNumRows(sqlresult)
             << endl
             << "0  implies: nothing known about this job" << endl
             << ">1 implies: the jobID is not unique whereas it should be" << endl;
        throw JCCS_database_query_failure();
    }

    // database no longer needed so we can close the connection
    SQLCloseConn(jccsdb);

    // we have ascertained ourselves we have exactly one row in our resultset
    // and in the query we explicity set the order of the fields we
    // required.
    // Field 0: the jobid
    // Field 1: the bocfrate
    // Field 2: the scheduled-scan-start-time [formatted such that the MVTime
    //          class from casa will be able to parse it]
    rij = SQLFetchRow(sqlresult);
   
    // get the bocfrate
    SQLGetField(&bocfrate, rij, 1, ::atoi);

    // and the scanstart
    std::istringstream   datestring( rij[2] );
    datestring >> scanstart;

    // do NOT forget to save it in our datamember,
    // anticipated_scanstart....
    anticipated_scanstart = MEpoch( MVEpoch(scanstart), MEpoch::UTC );

    // Since we have the 'scheduled scan start time' and the
    // bocfrate, we may set up the initial BOCF-to-time translation
    // (will be verified upon first dataarrival)
    bocf2time = BOCFToTime( MEpoch(MVEpoch(scanstart), MEpoch::UTC), bocfrate );


    // now, given the bocfrate from the DB and the integrationtime
    // from the correlatorsetup, we may infer the 'number of BOCFs
    // per integration' because that is the kind of info the processing
    // routine(s) [the ones that scale the raw data] need.
    //
    //   'setup.getIntegrationTime()' is in units of seconds
    //   'bocfrate' is in units of s^-1 so no scaling or adaptation
    //   needed
    bocfpintg = (unsigned int)(mySetup.getIntegrationTime() * (double)bocfrate);

    // Now we can set up 
    // 1a) the modulestacks
    // 1b) the pcint-processor
    // 2) the processing environment
    input_data*    ipdata;
    ModuleStack    ip;
    ModuleStack    proc;

    // for the inputstack, we use the pseudomodule "read_data"
    // We can safely pass the "new input_data()" thingy since
    // the 'cleanup_input_data' takes care of deleting
    // that thing!
    ipdata = new input_data(dfmap);
    ip.push_back( Module("PCIntVisBuffer.cc:read_data",
                         (void*)&read_data, (void*)&cleanup_input_data, ipdata) );

    // no more inputs.
    //
    // Now on to the processing chain..
    // load the predifined evn processing steps and
    // finally add our own module(s) add the end, the one
    // which appends the processed block to our internal
    // blocklist!
    bool                     seenrecipe( false );
    vector<string>           mods = default_recipe();
    MyRegularExpression      rx_recipe("^recipe=");
    MyRegularExpression      rx_dbg("^debug$");
    vector<string>::iterator mptr;

    // check if usr desired to override the recipe
    for( mptr=myOptions.begin();
         mptr!=myOptions.end();
         mptr++ )
    {
        if( *mptr==rx_recipe && !seenrecipe ) 
        {
            string            recipe_file;
            string::size_type equalsign( mptr->find('=') );

            recipe_file = mptr->substr(equalsign+1);

            cout << "\nPCIntVisBuffer: Overriding default recipe with recipe from file '"
                 << recipe_file
                 << "'"
                 << endl;
            mods = read_recipe(recipe_file);
            // only allow one recipe override!
            seenrecipe = true;
        }
        else if( *mptr==rx_dbg )
        {
            environment.debug = true;
        }
    }
    
    // finally, load the recipe!
    for( mptr=mods.begin();
         mptr!=mods.end();
         mptr++ )
    {
        cout << "Recipe: " << *mptr << endl;
        proc.push_back( Module(*mptr) );
    }
    // append our local module which will integrate individual
    // datums into a full pcint integration after which it
    // will add it to our list
    proc.push_back( Module("PCIntVisBuffer.cc:integrate_datums",
                           (void*)&integrate_datums, 0, &integrationlist) );
    // and finally, after a block has succesfully been appended
    // to the list, we'll inhibit further reading until this block
    // 's dealt with
    proc.push_back( Module("PCIntVisBuffer.cc:inhibit_read_data",
                           (void*)&inhibit_read_data, 0, ipdata) );

    // ModuleStacks created ok,
    // Now create the "PCIntProcessor" from these two stacks
    processor = PCIntProcessor(ip, proc);

    // now set up the processing environment,
    // we have to provide at least the DTD,
    // the bocfrate and the number of bocfs/integration
    environment.symbol_table.addLabel<pcint::DTD>("dtd", myDTD);

    // transfer inferred values to the runtime environment
    environment.symbol_table.addLabel<unsigned int>("bocfrate", bocfrate);
    environment.symbol_table.addLabel<unsigned int>("bocfpintg", bocfpintg);

    // need to analyze the 'datafilemap' and the 'DTD'
    // to build a list of baselines and a datablockset
    // Those need to be stored in the environment
    // as well
    // So let's first build those things
    analres_t    r = analyze_this(dfmap, myDTD, mySetup.l2pmap());

    // and write those into the environment as well
    environment.symbol_table.addLabel<unsigned int>("datablocksize", r.totalsize);
    environment.symbol_table.addLabel<pcint_baselinelist>("baselinelist", r.baselines);
    environment.symbol_table.addLabel<pcint_datablockset>("datablockset", r.datablocks);
    
    // we must pre-read some data. The JIVEMSFiller's loop goes something like
    //  while( visbuf.getCurrentVisibility() )
    //  {
    //      visbuf.next();
    //  }
    //  so we must ascertain that *some* data is available!
    //  At least... next() skips to the next (in this case: first)
    //  piece of data. If it so happens that there's nothing to read,
    //  that won't be a problem!

    // this condition triggers 'attempt to read data'
    // in the 'next()' method
    // 'next()' will also set the cur_intg and
    // cur_baseline iterators adequately
    cur_intg     = integrationlist.end();
    this->next();

    // this is not correct. Really it should be determined from 
    // the time of the first datapoint but we don't have that yet...
//    this->setCurrentScanptr( *myScanPtrs.begin() );
}


// there are no more visibilities if
//  'mycurblock' points at the end of the blocklist
//  the 'next()' method makes sure that it *never* points
//  at the end of the list unless there's no more data
//  to read
casa::Bool PCIntVisBuffer::getCurrentVisibility( Visibility& vref )
{
    Bool                 rv( cur_intg!=integrationlist.end() );

    if( rv )
        rv = (cur_baseline != cur_intg->baselines.end());

    if( rv )
    {
        MEpoch                vt;
        ExtVisLabel           evl;
        Vector<Float>         vf;
        correlations_t        cors;
        Matrix<Complex>       m( this->getData(vf) );
        pcint_ifmap::iterator curintf;

        // The interferometers are put in the 'ifmap'
        // keyed on "CorrelationCode" so retrieve that
        // value to append it to the correlations
        // for this baseline
        for( curintf=cur_baseline->ifmap.begin();
             curintf!=cur_baseline->ifmap.end();
             curintf++ ) {
            cors.push_back( curintf->first );
        }
        
        // Fill in the Extended Visibility Label
        evl.baseline     = BaseLineIdx( cur_baseline->xant, cur_baseline->yant );
        evl.correlations = cors;
        evl.subbandnr    = cur_baseline->subband;
        evl.jobnr        = 0;
        
        // the visibility time
        vref = Visibility( evl, m, Visibility::time, False, vf, cur_vistime, Muvw(), this );
    }
    return rv;
}

casa::Bool PCIntVisBuffer::next( void )
{
    // save pointer to current integration 
    // (so we can check if we've started
    //  processing a new integration
    //  later on)
    pcint_integrationlist::iterator last_intg = cur_intg;
    
    // if we're looking at an integration
    // and we haven't dealt with the last
    // baseline in that integration yet,
    // move to the next baseline
    if( cur_intg!=integrationlist.end() &&
        cur_baseline!=cur_intg->baselines.end() )
    {
        cur_baseline++;
    }

    // if we're looking at an integration and
    // have just reached 'at-end' of baselines,
    // try to advance to the next integration
    if( cur_intg!=integrationlist.end() &&
        cur_baseline==cur_intg->baselines.end() )
    {
        cur_intg++;
        // if we haven't moved off the end of the list,
        // we should reset the baselineiterater to
        // point at the beginning of the baselinelist
        // of the new integration....
        if( cur_intg!=integrationlist.end() )
            cur_baseline = cur_intg->baselines.begin();
    }

    // Have we reached end of integrationlist?
    if( cur_intg==integrationlist.end() )
    {
        // remove all items from the integrationlist; we've cycled
        // through them didn't we?
        integrationlist.clear();

        // execute the processing chain and hope it will
        // give us (a) new block(s) [the chain automagically
        // modifies our "integrationlist" variable...
        // Beware of Multi Threading this'un ... !!!!!!
        // (ie: DON'T DO THAT!)
        processor.run( environment );

        // if the processing chain didn't append anything.. 
        // that's bummer! but really, that means that there's
        // nothing more to be read!
        // Anyway, for an empty list the following holds:
        // begin()==end() so we can blindly set
        // 'cur_intg' to 'begin()' because it either
        // points at a new block of data or at the end of
        // the list, both of which conditions are equally well
        // dealt with in other pieces of the code...
        cur_intg  = integrationlist.begin();

        // make sure that last_intg (which 
        // was the saved value of cur_intg
        // when we entered the function)
        // *never* points at something which
        // might be equal to 'integrationlist.begin()'
        // so as to make sure that the
        // condition
        // 'cur_intg!=last_intg' (which SHOULD
        // hold now, unless there really is no
        // more data, which is dealt with correctly too)
        // always holds in the case of new data
        last_intg = integrationlist.end();

        // *if* there's data available,
        // let the baselineiterator point at the
        // first baseline in the first integration
        if( cur_intg!=integrationlist.end() )
        {
            // ok we have a new integration, set
            // the current scanpointer and the 
            // start/end time of this integration
            // [and all other properties that are common
            //  to all baselines in this integration]
            //  Note: this will be taken care of below
            cur_baseline = cur_intg->baselines.begin();
        }
    } 
    
    // if this condition holds, we have a new
    // integration and we must (try to) work
    // out what the time of the visibility is.
    // Once we've done that, we must look to the
    // list of scans to find out which scan this
    // data is from
    if( cur_intg!=last_intg ) {
        // decode the time
        if( this->decode_visibility_time(*cur_intg) ) {
            // and (try to) find the scan based on the time
            // (if 'decode_visibility_time()' is successfull
            //  it set the cur_vistime appropriately)
            this->setCurrentScanptr( this->find_scan_by_time(cur_vistime,
                                     mySetup.getIntegrationTime()) );

        } else {
            // unsucessfull in decoding the time,
            // set scanptr to 0 to indicate we
            // don't know where the heck this 
            // data came from
            this->setCurrentScanptr( 0 );
        }
    }
    
    // return true as long as there are 
    // unhandled blocks in the list
    // (no need to check for 'cur_baseline==cur_intg->baselinelist.end()'
    //  because cur_intg will eventually only be advanced to
    //  'intlist.end()' *after* the baseline iterator has
    //  reached the end of the baselinelist, ie after all
    //  baselines in an integration have been cycled through
    //  and then either there are more integrations or not...)
    return (cur_intg!=integrationlist.end());
}


// get data for the 'cur_baseline' in the 'cur_intg'
const Matrix<Complex>& PCIntVisBuffer::getData( Vector<Float>& weight )
{
    static Matrix<Complex>   rv;

    unsigned int                row;
    pcint_ifmap::const_iterator curintf;

    rv.resize(0, 0);
    weight.resize( 0 );
    if( cur_intg==integrationlist.end() )
    {
        return rv;
    }
    if( cur_baseline->ifmap.size()>4 )
        cerr << "getData() - AAAAAAAAarrrrghhhhhh! size=" << cur_baseline->ifmap.size() << endl;
    // we know the number of lags
    rv.resize( cur_baseline->ifmap.size(), mySetup.nrOfSpectralPoints()*2 );
    weight.resize( cur_baseline->ifmap.size() );
    // init weights to -6.28
    weight = -6.28;
    // and data to 0.0+0.0i
    rv = Complex(0.0, 0.0);

    // Ok loop over all interferometers for this baseline
    for( curintf=cur_baseline->ifmap.begin(), row=0;
         curintf!=cur_baseline->ifmap.end();
         curintf++, row++ )
    {
		bool                      swapit( false );
		bool                      autocorrelation;
		bool                      crosspolarization;
		bool                      expand;
        float*                    ifdataptr;

        const CorrelationCode&    cc( curintf->first );
        const pcint_intf&         ifref( curintf->second );
       
        // if no data present, skip this'un
        // (or the interferometerindex < 0, since
        // that means the thing was never initialized
        // with 'real' values')
        if( !ifref.data_present || ifref.ab_intf<0 )
            continue;

		//  Cache these values
        //    nrfloats and nlag get initially the same value
        //    but they are separate variables since the NEED NOT
        //    be the same; if the interferometer sais it has
        //    complex data, the number of floats is twice
        //    the number of lags [1 float for real, 1 float for
        //    complex, per lag]
        //
        //  NOTE: the definition of 'nlag' as used in this
        //  function is the one used by Albert; it is basically
        //  the number of floating point numbers associated
        //  with a certain interferometer.
        //  Depending on the interferometer's datatype
        //  the interpretation may vary:
        //
        //  - wether or not the interferometer is auto- or
        //    cross correlation
        //    eg for an auto-correlation/complexdata interferometer
        //     only the real part is given since the imaginary
        //     part is necessarily zero so 'nlag' in this
        //     case indeed is 'the number of lags'
        //  - wether the product is complex or real data
        //    in the first case (complex) the number of
        //    lags is 'half the number of floats' ie
        //    'nlag/2' where 'nlag' is the value given
        //    in the datatransferdescriptor
        //  
		unsigned int              nlag( ifref.nlag );
		unsigned int              nrfloats( ifref.nlag );
        
        // albert has a bit in the interferometer datatype which
        // indicates if it was a auto-correlation
        //
        // Bit definitions for the 'datatype' of an interferometer:
        //  bit0: set = autocorrelation, cross otherwise
        //  bit1: set = complex data, real data otherwise
        //  bit2: set = 1-bit data, 2-bit otherwise
        //autocorrelation   = (cur_baseline->xant == cur_baseline->yant);
        autocorrelation   = ((ifref.datatype&0x1) == 0x1);

        crosspolarization = cc.is_crosspol();

        // autocorrelations only yield half the number of lags
        // but we like to give the user a nice square matrix where *all*
        // interferometers have the same number of lags, so
        // for autocorrelations we have to 'expand' the data
		expand            = autocorrelation;

        // check if we're dealing with complex data
        // if so, then the true number of lags
        // is half the number of floats
        if( (ifref.datatype&0x2)==0x2 )
        {
            nlag = nrfloats/2;
        }

        // Still need to work out if we need to
        // swap the interferometer's data.
        // This happens when crosspols are computed;
        // in this case, *one* of the possible four
        // interferometers (for the four polarization
        // combinations) is computed in the 
        // "wrong" order, that is, with the baseline
        // reversed
        swapit = ifref.swapped;

        // point at the data
        ifdataptr = (float*)cur_intg->data.begin() + ifref.offset;
        
        // possibly shift the data (Vernier delay etc) ...
        // still use old meaning of 'nlag'

        //        .....
        
        // Ok source data is prepared
        // now we can update nlag to
        // be the value we want (possibly expanded)
        if( expand )
            nlag *= 2;

        Complex*   cmplxreordered = new Complex[nlag];

		//  The only exeptional case is 'expand' -> we deal with that occasion
		//  differently....
		//
		//  Note: the following code features some duplication but it enables
		//  us to form all dataarrays with one loop (only one for-loop will
		//  be executed per case!) Otherwise there might have been occasions
		//  where we have to execute two for-loops
		if( expand )
		{
			unsigned int    centrallag( nlag/2 );
			unsigned int    lags2do( (nlag/2)-1 );
	
			for( unsigned int i=0; i<=lags2do; i++ )
			{
				cmplxreordered[ centrallag+i ] = Complex( ifdataptr[i], 0.0f );
				cmplxreordered[ centrallag-i ] = Complex( ifdataptr[i], 0.0f );
			}
			cmplxreordered[ 0 ] = Complex( 0.0f, 0.0f );
		}
		else
		{
			//   Remeber: the structure of the data in memory is this -
			//
			//   <real><real>.....<real><imag><imag>....<imag>
			//     |                 |     |               |
			//     ------ nlag ------      ----- nlag ------
			//
			//
			//  Note: ifdataptr = ptr to float!
			//
			//  Ok. Vernier-delay has been dealt with. Now either copy
			//  the data or fill it in swapped...
			//
			//  If we need to swap it then we must mirror the lags
			//  about the central lag (which is different from just
			//  reversing the lag order (guess how we found *that*
			//  out... by experience) and take the complex
			//  conjugate. Reversing would have done the trick if this
			//  were an odd-sized array of samples but alas, the
			//  number of lags is always even....
			if( swapit )
			{
				unsigned int    lagcnt;
				unsigned int    zerolag( nlag/2 );
				unsigned int    lags2do( (nlag/2)-1 );
	
	
				for( lagcnt=0; lagcnt<=lags2do; lagcnt++ )
				{
					cmplxreordered[ zerolag+lagcnt ] = Complex( ifdataptr[zerolag-lagcnt],
													   -1.0*ifdataptr[nlag+zerolag-lagcnt]);
					cmplxreordered[ zerolag-lagcnt ] = Complex( ifdataptr[zerolag+lagcnt],
													   -1.0*ifdataptr[nlag+zerolag+lagcnt]);
				}
				//
				//  Only the zeroth point in the data has not been filled in....
				//
				//  HV: 22-03-2001 - After dicussion with Chris and Bob, decided
				//                   to take this statement out. The first lag
				//                   *is* filled in! So no need to force it to
				//                   zero! NOOOOOH! It must be filled in with data
				//                   from the buffer.....
				//
				//cmplxreordered[ 0 ] = Complex( 0.0f, 0.0f );
				cmplxreordered[ 0 ] = Complex( ifdataptr[0], ifdataptr[nlag-1] );
			}
			else
			{
				//  Ultimately simple!
				for( unsigned int lagcnt=0; lagcnt<nlag; lagcnt++ )
				{
					cmplxreordered[ lagcnt ] = Complex( ifdataptr[lagcnt],
													ifdataptr[nlag+lagcnt] );
				}
			}
		} // else part of "if( expand )" (ie NOT expand ;))

        // the cmplxreordered array is filled in with
        // data!
        // Transfer it to the result matrix
        rv.row( row ) = Vector<Complex>(IPosition(1,nlag), cmplxreordered, SHARE);

        weight[ row ] = ((float*)cur_intg->data.begin())[ ifref.woffset ];

        delete [] cmplxreordered;
    }
    return rv;
}



casa::Int PCIntVisBuffer::getVisBufId( void ) const {
    return myJobID;
}

CorrelatorSetup PCIntVisBuffer::correlatorSetup( void ) const {
    return mySetup;
}

unsigned int PCIntVisBuffer::getJobID( void ) const {
    return myJobID;
}

unsigned int PCIntVisBuffer::getSubJobID( void ) const {
    return mySubJobID;
}

const casa::String& PCIntVisBuffer::getRootDir( void ) const {
    static String hack;
    hack = myRootDir;
    return hack;
}

const j2ms::scaninfolist_t& PCIntVisBuffer::getScanlist( void ) const {
    return myScanList;
}

PCIntVisBuffer::~PCIntVisBuffer() {
    std::list<j2ms::COFScan*>::iterator curscan;
    
    // delete all scanpointers
    for( curscan=myScanPtrs.begin();
         curscan!=myScanPtrs.end();
         curscan++ ) {
        delete *curscan;
    }
}




bool PCIntVisBuffer::decode_visibility_time( const pcint_integration& intg ) {
    static bool    verified_zeropoint( false );
    unsigned int   start_bocf( intg.start_bocf );
    unsigned int   end_bocf( intg.end_bocf );

    // if either one or both of the framenrs are
    // invalid, there's nothing to do, really
    if( start_bocf==0xffffffff ||
        end_bocf==0xffffffff ) {
        return false;
    }

    // given the fact that we seem to have 'sensible'
    // framenrs, check that we've set up the zero-point
    // of framenr-to-time translation
    if( !verified_zeropoint ) {
        // Ok. Apparently these are the first valid
        // framenrs to come in. Try to verify, to
        // best effort, if the current zero-point seems
        // to be valid. 
        // ASSUMPTION: the bocf2time is set up with
        // - the correct bocfrate
        // - the anticipated scan-start.
        // AND
        // - the system has saved the original anticipated
        //   scanstart
        //
        // Compute the current time, based on the 
        // framenrs and the current bocf2time settings.
        // If the results match up (within a sensible
        // range) we assume the chosen zero-point is
        // ok. Otherwise, advance/recede the zero-point
        // as often as necessary to make the
        // computed time comparable to the anticipated
        // scan start.
        //
        // Note: if you start tinkering with the values below
        // (without touching the algorithm) remember that
        // the algoritm works based on the predicate that
        // margin is a positive value
        // and the initial condition that distance is a 
        // value whose absolute value is less than 'margin'
        //
        double         distance = 0.0;
        Quantity       computed_start;
        BOCFToTime     saved( bocf2time );
        const double   margin( 250.0 );// default margin of +- 250 seconds
        const Quantity anticipated_start( anticipated_scanstart.getValue().getTime("s") );

        // Loop until distance <= margin
        do {
            double   newdist;
            // if distance <> 0 [really: >margin or <-margin]
            // then adjust the zeropoint in the opposite direction:
            // if computed time too small compared to what's expected, the 
            // current zero-point is too low so we must increase it to move
            // it closer to what we're expecting and vice versa.
            // The first time this loop is entered, the distance is 0.0
            // so the zero-point won't be tinkered with and if,
            // after computation, the abs(distance) is *still* < margin
            // we say the current zeropoint is Ok.
            // Otherwise, the zeropoint is changed and the distance 
            // re-computed
            if( distance<-margin ) {
                bocf2time.advanceZeroPoint();
            } else if( distance>margin ) {
                bocf2time.rewindZeroPoint();
            }
            computed_start = bocf2time( start_bocf ).getValue().getTime("s");

            // first compute the new distance and compare
            // with the previous such that we can detect
            // 'overshooting' to prevent this loop from
            // oscillation; (if we overshoot and start
            // compensating in the other direction
            // the loop may never terminate: it is not guaranteed
            // that we *will* find a suitable zeropoint).
            newdist = computed_start.getValue() - anticipated_start.getValue();

            // only do the check if we were busy adjusting the
            // zeropoint.
            // If the new distance is still out-of-bounds AND
            // the two distances have opposite signs
            // we're in trouble since the new distance would
            // require adjusting the zeropoint in the direction
            // we were just coming from and neither gave a
            // valid solution, ie a computed time
            // within the range
            // [anticipated - margin, anticipated + margin]
            if( ::fabs(distance)>margin && ::fabs(newdist)>margin &&
                ((distance>0.0 && newdist<0.0) || (distance<0.0 && newdist>0.0)) ) {

                // No use in continuing the search for a valid endpoint
                break;
            }

            // now set the current distance to the newly computed distance
            // and re-evaluate the loop condition
            distance = newdist;
        } while( ::fabs(distance)>margin );

        // We need to (yet again...) re-evaluate the post-condition
        // since we may arrive here with a false post-condition
        // [ie: the current zeropoint being validated] 
        if( ::fabs(distance)<margin ) {
            verified_zeropoint = true;
        } else {
            // reset the bocf2time to original setting
            // so's we can have another go at it the next
            // time some framenumbers come in
            bocf2time = saved;
        }
    }

    // if the zeropoint (still) not verified, get the beeeeeeep
    // outta here
    if( !verified_zeropoint ) {
        return false;
    }

    // Phew. Now go on doing the hard stuff
    //
    // We must be able to detect bocf-framenr wraps.
    // If the framenumbers wrap, the following condition holds:
    //
    // start_bocf < last_end_bocf since within a 10minute interval,
    // the framenrs increase monotonically and hence the start
    // framenr of a new integration can never be less than the
    // end of an earlier integration (unless there had been a wrap...)
    //
    // Before we continue on computing, we must realize that:
    // (a) the end framenumber is the framenumber at the *beginning* 
    //     of the last frame that went into the integration, so, timewise,
    //     we have an extra timeduration of one BOCF, therefore,
    //     the end framenumber must be incremented by one
    // (b) but before we do that, we must realize that the bocf-wrap
    //     could also occur *within* the integration.
    //     This is signalled by the end framenumber being less than
    //     the start framenumber
    //   
    bool      wrapdetected( false );

    // if this holds: two things need be done
    //   * signalling that a wrap occurred
    //   * adapt the end framenumber to be as it
    //     came from the current 10 minute interval
    //     (makes time computation simpler)
    if( end_bocf<start_bocf ) {
        // realization (b) of above
        wrapdetected  = true;
        end_bocf     += bocf2time.getMaxBOCFnr();
    }
    
    // if the last-end-bocf was set and no wrap detected yet,
    // we must check if a wrap occurred since the last time
    // we seen some valid framenumbers
    if( last_end_bocf!=0xffffffff && !wrapdetected ) {
        // if the difference between the current start
        // and the last end is 'on the order of'
        // [with quite a crude definition of 'on the order of'
        //  being "half the interval"] a jump in the framenumbers
        // we say a jump occurred.
        // If we could guarantee that we get *every* integration 
        // out of the correlator without *any* problems, this
        // 'jump margin' could be much narrower. However, we may
        // miss integrations, data may be corrupted, whatever,
        // so we want to detect a jump during an interval as long
        // as possible.
        //
        // The maximum jump is from the maximum framenumber for
        // the given bocfrate to zero. However, if we miss integrations,
        // the jump may be smaller (because the new start framenumber has
        // increased without us seeing the intermediate integrations).
        //
        // I'd say that if the jump we find is at least half the maximum
        // framenumber, we may conclude a jump. This allows for approx
        // 5 minutes of dataloss and still be able to pick up a wrap.
        // If we loose more than 5 minutes of data, I guess we're skrewt
        // anyway.
        wrapdetected = ( start_bocf<last_end_bocf &&
                         (last_end_bocf-start_bocf)>bocf2time.getMaxBOCFnr()/2 );
    }

    // Compute the time
    // (dealing with wraps etc is done after this)
    // Note: in the computation of the duration
    //       of this integration realization (a)
    //       of above must be taken into account
    //       Note: we must not increment the 
    //       end_bocf (but just do the computation
    //       with end_bocf+1, since the original
    //       value of end_bocf must be saved for
    //       the next iteration.
    //
    // Also note: this version is not Speed-up safe!!!!
    // It assumes that the bocfrate is also wall-clock-rate
    // (ie one bocf == 1/bocfrate seconds). For speed-up
    // we must take the speedup factor into account
    double    duration( ((end_bocf-start_bocf)+1) * bocf2time.getBOCFRate() );
    MVEpoch   start_epoch( bocf2time(start_bocf).getValue() );

    // now form the current visibility time, being the midpoint of the
    // integration
    cur_vistime = MEpoch( MVEpoch(start_epoch.getTime("s")+Quantity(duration/2.0, "s")), MEpoch::UTC );

    // if a wrap was detected, advance the zeropoint
    if( wrapdetected )
        bocf2time.advanceZeroPoint();

    // and save the last end bocf
    last_end_bocf = end_bocf;

    return true;
}

j2ms::COFScan* PCIntVisBuffer::find_scan_by_time( const casa::MEpoch& tm,
                                                  const double interval ) const 
{
    j2ms::COFScan*                            rv( 0 );
    casa::Quantity                            scanstart, scanend;
    // .getValue() gives a MVEpoch,
    // .getTime("s") gives the time in seconds from the MVEpoch
    const casa::Quantity                      iv( interval/2.0, "s" );
    const casa::Quantity                      vistime( tm.getValue().getTime("s") );
    const casa::Quantity                      visstart( vistime - iv );
    const casa::Quantity                      visend( vistime + iv );
    j2ms::scaninfolist_t::const_iterator      scaninfoptr;
    std::list<j2ms::COFScan*>::const_iterator scanptrptr;

    for( scaninfoptr=myScanList.begin(), scanptrptr=myScanPtrs.begin();
         scaninfoptr!=myScanList.end();
         scaninfoptr++, scanptrptr++ )
    {
        scanstart = scaninfoptr->scanStart.getTime("s");
        scanend   = scaninfoptr->scanEnd.getTime("s");

        // if this holds, we've found the blighter
        if( visstart>=scanstart && visend<=scanend ) {
            rv = *scanptrptr;
            break;
        }
    }
    return rv;
}

#endif // PCInt support required?
