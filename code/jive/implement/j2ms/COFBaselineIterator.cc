#if 0
//  Implementation of COFBaseline* stuff
//
//  Author:  Harro Verkouter 01-06-1999
//
//  $Id: COFBaselineIterator.cc,v 1.10 2007/02/26 10:55:41 jive_cc Exp $
#include <jive/j2ms/COFBaselineIterator.h>
#include <jive/j2ms/jccspol2j2mspol.h>
#include <data_handler/IFIterator.h>
#include <data_handler/InterferometerDescriptor.h>
#include <casa/Exceptions.h>

#include <cassert>
#include <algorithm>

using std::cerr;
using std::endl;
using std::ostream;
using std::flush;
using std::pair;
using std::make_pair;
using namespace casa;


// the blkey_type helper struct
blkey_type::blkey_type():
    XAnt(-1), YAnt(-1), Subband((unsigned int)-1)
{}

blkey_type::blkey_type(int xant, int yant, unsigned int sb):
    XAnt(xant), YAnt(yant), Subband(sb)
{}

// The global operators "<" and "==" for the blkey_type thingies
bool operator<(const blkey_type& l, const blkey_type& r) {
    return (l.XAnt<r.XAnt ||
            (l.XAnt==r.XAnt && l.YAnt<r.YAnt) ||
            (l.XAnt==r.XAnt && l.YAnt==r.YAnt && l.Subband<r.Subband));
}
bool operator==(const blkey_type& l, const blkey_type& r) {
    return (l.XAnt==r.XAnt && l.YAnt==r.YAnt && l.Subband==r.Subband);
}


//  Show the contents of a COFBaseline struct in HRF on a stream!
ostream& operator<<( ostream& os, const COFBaseline& cbref ) {
    os << "Baseline: " << cbref.getIndex()
       << " SUBBAND=" << cbref.getSubbandIndex() 
       << " CORRS=" << as_corrcode(cbref.getCorrelations());
    return os;
}


//  Show a complete baseline set on a stream (in HRF)
ostream& operator<<( ostream& os, const COFBaselineSet& cbsetref ) {
    COFBaselineIterator   blptr( cbsetref.begin() );
    
    os << "\n\nShowing contents of COFBaselineSet...\n";
    
    while( blptr!=cbsetref.end() ) {
    	os << *blptr;
    	blptr++;
    }
    return os;
}


//  The COF-Baseline struct
COFBaseline::COFBaseline( const BaseLineIdx& blidx, unsigned int subbandindex ) :
    myKey( blidx.idx1(), blidx.idx2(), subbandindex )
{}

unsigned int COFBaseline::getSubbandIndex( void ) const {
    return myKey.Subband;
}


BaseLineIdx COFBaseline::getIndex( void ) const {
    return BaseLineIdx(myKey.XAnt, myKey.YAnt);
}

const blkey_type& COFBaseline::getKey( void ) const {
    return myKey;
}

const COFBaseline::intflist_t& COFBaseline::getIntfList( void ) const {
    return intflist;
}

correlations_t COFBaseline::getCorrelations( void ) const {
    correlations_t             retval;
    intflist_t::const_iterator curintf;

    // retrieve all the keys that are present in 
    // the map; the keys are the ordinal numbers!
    for( curintf=intflist.begin(); curintf!=intflist.end(); curintf++ )
        retval.push_back( curintf->first );
    return retval;
}
/*
unsigned int COFBaseline::getFileOffsetInBytes( unsigned int idx ) const
{
    unsigned int               retval( 0 );
    intflist_t::const_iterator curintf;

    if( (curintf=intflist.find(idx))!=intflist.end() )
    {
        retval = curintf->second.myFileOffsetInBytes;
    }
    return retval;
}
*/
InterferometerDescriptor COFBaseline::getInterferometer( const CorrelationCode& c ) const {
    InterferometerDescriptor   retval;
    intflist_t::const_iterator curintf;

    if( (curintf=intflist.find(c))!=intflist.end() )
        retval = curintf->second.myInterferometer;
    return retval;
}



Bool COFBaseline::addInterferometer( const InterferometerDescriptor& ifdescr ) {
    CorrelationCode                 cc;
    pair<intflist_t::iterator,bool> insres;

    cc = CorrelationCode( jccspol2j2mspol(ifdescr.getXPolarization()),
                          jccspol2j2mspol(ifdescr.getYPolarization()) );
    insres = intflist.insert( make_pair(cc, _ifinfo_t(0, ifdescr)) );
    
    return insres.second;
}



//  The COF-baseline-set class
COFBaselineSet::COFBaselineSet() 
{}

COFBaselineSet::~COFBaselineSet() 
{}

COFBaselineSet::COFBaselineSet( const DataDescriptor& ddref ) {
    this->attachDataDescriptor( ddref );
}

COFBaselineSet& COFBaselineSet::operator=( const DataDescriptor& ddref ) {
    this->attachDataDescriptor( ddref );
    return *this;
}



//                     Private method section

// stl helper stuff


struct blfinder_t {
    public:
        blfinder_t( unsigned int sb, const BaseLineIdx& idx ):
            __key(idx.idx1(), idx.idx2(), sb)
        {}

        bool operator()( const COFBaseline& cb ) const {
            return ( cb.getKey()==__key );
        }
    
    private:
        blkey_type     __key;
    
        blfinder_t();
};

// find the Baseline with subband='b' and baselineidx='c'
// in the list of baselines 'a'
#define find_baseline(a,b,c) \
    std::find_if(a.begin(), a.end(), blfinder_t(b,c))

//  Parse the DataDescriptor and form all the baselines
Bool COFBaselineSet::attachDataDescriptor( const DataDescriptor& ddref ) {
    IFIterator         ifit( ddref.begin() );
    BaseLineIdx        blidx;

    //  Clear the baseline list...
    cofbaselines.clear();

    //  Process all interferometers in the DataDescriptor
    while( ifit ) {
        // Note: below we make a copy of the current
        // interferometerdescriptor - we may need to
        // make modifications to it
    	Bool                             autocorrelation;
    	Bool                             crosspolarization;
        InterferometerDescriptor         ifdesc( *ifit );
        cofbaselinelist_t::iterator      blptr;

        // If this interferometer was computed the wrong way round,
        // fix it now..
        if( ifdesc.getXAntenna()>ifdesc.getYAntenna() )
            ifdesc = ifdesc.swap();
	
    	blidx = BaseLineIdx(ifdesc.getXAntenna(), ifdesc.getYAntenna());

        // Note: the baselines are kept keyed on
        // BaseLineIdx and per baseline we hold the
        // interferometers, keyed on polarization
        // combination (correlation code). For the
        // individual interferometers, albeit they
        // also have X/Y antenna properties, these
        // are not taken into account. Only the
        // X/Y polarization indices of the descriptors
        // need to be ok.
        // Really, we don't need to keep the full
        // InterferometerDescriptors but otherwise
        // we would have to create yet-another-sort-
        // of-interferometer-descriptor keeping
        // just the info we need. Maybe we should
        // do that... but for now, no.
    	autocorrelation   = (blidx.idx1()==blidx.idx2());
    	crosspolarization = (ifdesc.getXPolarization()!=ifdesc.getYPolarization());
	
        // see if we have this baseline already
        blptr = find_baseline(cofbaselines, ifdesc.getSubband(), blidx);

    	if( blptr==cofbaselines.end() ) {
    	    //  Not found. Add it...
            blptr = cofbaselines.insert( cofbaselines.end(),
                                         COFBaseline(blidx, ifdesc.getSubband()) );
	    }
	
    	//  Add the correlation to the baseline
    	if( !blptr->addInterferometer(ifdesc) ) {
            // aieeee....
            cerr << "COFBaselineSet::attachDataDescriptor()/Failed to add\n"
                 << "   interferometer to baseline?!\n"
                 << "   BaseLineIdx: " << blidx << "\n"
                 << "   Interferometer: " << ifdesc << "\n";
            break;
        }
	
    	//  For autocorrelations/crosspolarizations, Albert only
    	//  calculates one of the products, therefore, iin order to
    	//  keep the shape constant, when we find one of the
    	//  cross-polarizations on a autocorrelation, we automatically
    	//  add the other polarization with the same interferometer
    	if( autocorrelation && crosspolarization ) {
            ifdesc = ifdesc.swap();
    	    if( !blptr->addInterferometer(ifdesc) ) {
                cerr << "COFBaselineSet::attachDataDescriptor()/Failed to add "
                     << "hypothetical 4th product [autobaseline,crosspol]\n"
                     << "   interferometer to baseline?!\n"
                     << "   BaseLineIdx: " << blidx << "\n"
                     << "   Interferometer: " << ifdesc << "\n";
                break;
            }
    	}

    	//  *sigh* .. do NOT forget to increment
    	//  iterator.... otherwise you may end up in an infinite
    	//  loop...... (guess what happend before I found THAT out...)
    	++ifit;
    }
    // ifit==false if end-of-list reached
    // if we prematurely stopped processing the list,
    // ifif will be non-null, or true
    return !ifit;
}

COFBaselineIterator COFBaselineSet::begin( void ) const {
    return cofbaselines.begin();
}

COFBaselineIterator COFBaselineSet::end( void ) const {
    return cofbaselines.end();
}

bool COFBaselineSet::empty( void ) const {
    return cofbaselines.empty();
}
#endif
