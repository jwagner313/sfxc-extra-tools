//  Experiment - Abstract base-class, specifying the kind information
//  a user can get from an experiment
//
//   Author:  Harro Verkouter, 8-12-1998
//
//   $Id: Experiment.h,v 1.9 2011/10/12 12:45:29 jive_cc Exp $
//
#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <casa/Arrays/Vector.h>
#include <casa/Logging.h>

#include <jive/j2ms/Visibility.h>
#include <jive/j2ms/SourceDB.h>
#include <jive/j2ms/AntennaDB.h>
#include <jive/j2ms/FrequencyDB.h>
#include <jive/j2ms/Scan.h>
#include <jive/j2ms/scanlist.h>
#include <jive/labels/polarization.h>
#include <jive/labels/CorrelationCode.h>

#include <map>
#include <string>
#include <iostream>




//  The class itself
class Experiment
{
public:
    //  Create an Experiment. If the default is used, the Current
    //  Working directory is used as exp.root dir.
    Experiment( const casa::String& opt="", const casa::String& exprootdir="" );

	
	// For a specific option, what was the value?
	// If the option was not set, return empty string
	casa::String                optionValue( const casa::String& opt ) const;
	//const String&                  options( void ) const;

    //  Provide access to the properties. Note: these methods must be
    //  implemented by a derived class!
    virtual const casa::String&          getCode( void ) const = 0;

    //  Hrmpff... design error (c) Harro V....
    //
    //  These functions should be located somewhere else but here;
    //  they control the output the JIVE MS-filler produces. May need
    //  a revision sometime... The MS-Filler should ask the visibility
    //  buffer to deliver the data in a certain domain *and* the
    //  MS-Filler should be capable of deciding on it's own where to
    //  store the data...
    virtual const casa::String&    getMSname( void ) const = 0;
    virtual void                   setMSname( const casa::String& newmsname ) = 0;

    virtual Visibility::domain     getDestinationDomain( void ) const = 0;
    virtual casa::Bool             setDestinationDomain( Visibility::domain newoutputdomain ) = 0;


    //  Ok... these belong here!
    virtual const casa::Vector<casa::String>&  getAntennas( void ) const = 0;
    virtual const casa::Vector<casa::String>&  getSources( void ) const = 0;

    // The experiment MUST have its own frequency DB!
    virtual const FrequencyDB&     getFrequencyDB( void ) const = 0;

    // The experiment MUST have an antennadatabase.
    virtual const AntennaDB&       getAntennaDB( void ) const = 0;

    //  Also, the experiment MUST have a source DB
    virtual const SourceDB&        getSourceDB( void ) const = 0;

    //  VisibilityBuffers's are a part of the experiment, therefore we
    //  should ask the Experiment if it would be so kind as to create
    //  the indicated VisibilityBuffer. If it fails to create it, a
    //  null-pointer is returned, otherwise a pointer to the newly
    //  created Buffer is returned!
    //
    //  NOTE: the caller is responsible for deleting the
    //  resources!!!!!!
    virtual VisibilityBuffer*      getVisibilityBuffer( const casa::String& visbufname ) = 0;


    // Experiment can give us a list of scans. VisibilityBuffers can do that too.
    // Default VisibilityBuffer 'getScanlist()' implementation will return
    // the experiment's getScanlist().
    // Specific VisBuffer objects may decide to return their own list (eg 
    // the COFVisibilityBuffer does that).
    // This one *may* be overridden. Default implementation gives an empty list
    virtual const j2ms::scaninfolist_t& getScanlist( void ) const;

    //  Tells us whether or not the Vernier delay was applied.
    //  Also, this shouldn't live here i guess
    virtual casa::Bool             hasVernierDelay( void ) const = 0;

    // The array-code (eg. EVN, GLOBAL, VLBA).
    //
    //  Note: derived classes *may* override this one, but don't have
    //  to. The return value defaults to EVN....(since we're at JIVE
    //  here...)
    virtual const casa::String&          getArrayCode( void ) const;

    //  These methods are NOT virtual!
    const casa::String&           getRootDir( void ) const;

    //  Log a message (if logging is enabled); usr. need not worry
    //  whether or not logging is enabled, function will verify all
    //  that's necessary)
    void                    logLogMessage( const casa::String& msg,
										   const casa::LogOrigin& org=casa::LogOrigin() ) const;

    //  Delete the resources used by this experiment
    virtual ~Experiment();

private:
    //  The experiment's private parts
	casa::String     myOptions;
    casa::String     myRootDir;
    std::ostream**  myLogStreamptr;

	// Map of string-to-string; the 'myOptions' split up;
	// <key>=<value> will be translated to map[<key>] = <value>
	map<std::string,std::string>  myOptionValues;

    //  Prohibit these
    Experiment( const Experiment& );
    const Experiment& operator=( const Experiment& );
};

//  Print some diagnostics on a stream
std::ostream& operator<<( std::ostream& os, const Experiment& expref );


#endif
