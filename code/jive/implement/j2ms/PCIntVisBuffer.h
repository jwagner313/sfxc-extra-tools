// VisibilityBuffer specific to PCInt datafiles
//  $Id: PCIntVisBuffer.h,v 1.5 2007/05/25 09:40:16 jive_cc Exp $
//
//  $Log: PCIntVisBuffer.h,v $
//  Revision 1.5  2007/05/25 09:40:16  jive_cc
//  HV: - compiling PCInt support is now optional.
//      If you want it, type make PCINT=1.
//      By making it optional it removes a physical dependency for
//      places where you dont need/want it
//
//  Revision 1.4  2007/02/26 13:19:47  jive_cc
//  HV: - Adhere to new polarization handling scheme
//      - Furthermore mostly cosmetic changes
//
//  Revision 1.3  2006/08/17 13:39:28  verkout
//  HV: First version to succesfully support PCInt dataformat
//
//  Revision 1.2  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.1  2006/02/14 13:33:25  verkout
//  HV: Start adding support for PCInt based datafiles
//
//
//

// Does PCInt support need to be compiled in anyway?!
#ifdef PCINT

#ifndef PCINTVISBUFFER_H
#define PCINTVISBUFFER_H




//  Bool, Double etc...
#include <casa/aips.h>
#include <casa/BasicSL/String.h>

//  This is our base-class so we need it
#include <jive/j2ms/VisibilityBuffer.h>

// need to be able to translated BOCF framenrs
// to time
#include <jive/j2ms/BOCFToTime.h>

#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
#include <casa/BasicSL/Complex.h>
#include <measures/Measures/MEpoch.h>


#include <jive/j2ms/CorrelatorSetup.h>
#include <jive/j2ms/scanlist.h>
#include <jive/j2ms/pcint_crud.h>

namespace j2ms {
    class COFScan;
}
class VEXperiment;

// PCInt stuff

// for CorrelatorBoard -> DataStream mapping
#include <types/SBCManagerTypes.h>
// for DataStream -> DDD mapping (where did the stream
//                   get stored)
#include <types/DDDManagerTypes.h>
// for Albert's DataTransferDescriptor
#include <types/AlbertTypes.h>

// PCInt's loadable module support cruft
#include <module/Module.h>

// the input/processing chain processor
#include <pcintprocessing/PCIntProcessor.h>

// standard C++ stuff
#include <string>





class PCIntVisBuffer :
    public VisibilityBuffer
{
public:

    //
    //  Create a PCInt visibility buffer
    //
    // 'vbopt' is short for
    // "VisibilityBuffer Option" ie we may pass
    // visibility-buffer specific options to
    // an object (defaults to nothing)
    PCIntVisBuffer( unsigned int jobid, unsigned int subjobid,
			        const VEXperiment* experimentptr=0,
                    const std::string& vbopt = std::string() );
    
    //
    //  Get the current visibility from the buffer
    //
    virtual casa::Bool          getCurrentVisibility( Visibility& vref );
    
    //
    // Advance to next visibility
    //
    virtual casa::Bool          next( void );

    //
    //  What's the directory of the visibilitybuffer??
	//  Hmmmm.. under PCInt this is not quite uniquely defined
	//  but who cares??
    //
    virtual const casa::String& getRootDir( void ) const;

	// Return the visbuffer-Id() (=jobid!)
	virtual casa::Int           getVisBufId( void ) const;

    virtual CorrelatorSetup     correlatorSetup( void ) const;

    //  These are COFVisibilityBuffer specific
	//  Not sure we need 'm in PCInt...
    unsigned int                getJobID( void ) const;
    unsigned int                getSubJobID( void ) const;

    // the list of scans in this visbuffer
    virtual const j2ms::scaninfolist_t&
                                getScanlist( void ) const;
    
    //  Delete all allocated resources
    virtual ~PCIntVisBuffer();
    
private:
    //  Our private parts
    BOCFToTime               bocf2time;
    unsigned int             myJobID;
    unsigned int             mySubJobID;
    std::string              myRootDir;
	CorrelatorSetup          mySetup;
    std::vector<std::string> myOptions;


    //  The list of scans found in the vexfile...
    j2ms::scaninfolist_t              myScanList;
    std::list<j2ms::COFScan*>         myScanPtrs;

    // Mappings from the PCInt system, necessary to
    // find out *where* which data went
    // Also need Alberts datatransferdescriptor...
    SBCManagerTypes::cbdsmap_t        myCBDSmap;
    DDDManagerTypes::dsddmap_t        myDSDDmap;
    pcint::DTD                        myDTD;

    // stuff needed for dealing with PCInt processing
    PCIntProcessor                    processor;
    pcint::processing                 environment;
    pcint_integrationlist             integrationlist;

#if 0
    // cache analization results
    unsigned int                      dbsize;
    pcint_datablockset                datablocks;
    pcint_baselinelist                baselinelist;
#endif        
    // these iterators point at the
    // 'current integration' (if any)
    // and to the 'current_baseline' in the
    // 'current_integration' such that we
    // can nicely iterate over all baselines
    pcint_baselinelist::iterator      cur_baseline;
    pcint_integrationlist::iterator   cur_intg;

    // Also we cache the time of the current
    // integration (all visibilities from one
    // integration share the same time - d'oh)
    // And we need some more stuff to do our
    // bocf-to-time deadreckoning; we must
    // keep the last known good end-bocf
    // (to detect bocf wraps, which happen
    //  each integral 10minutes of ROT)
    unsigned int                      last_end_bocf;
    casa::MEpoch                      anticipated_scanstart;
    casa::MEpoch                      cur_vistime;

    //  Private methods


    //  Get all the data for the configured interferometers in the baseline
    //
#if 0
    const casa::Matrix<casa::Complex>& getBaselineData( casa::Vector<casa::Float>& weight,
                                                        const COFBaseline& cbref ) const;
#endif
    const casa::Matrix<casa::Complex>& getData( casa::Vector<casa::Float>& weight );


    // set the current visibility time, given the integration.
    // Currently uses the BOCF framenrs
    bool                               decode_visibility_time( const pcint_integration& intg );
    // find the scan which this time belongs to.
    // Assumes that the 'tm' is the midpoint of the visibility
    // and 'interval' the exposure or integration time
    j2ms::COFScan*                     find_scan_by_time( const casa::MEpoch& tm,
                                                          const double interval ) const;

    //  A struct, used to pass decoded mapping information to the
    //  caller
    struct _MapInfo
    {
		unsigned int     XTelescopeNr;
		unsigned int     YTelescopeNr;
		unsigned int     XDataSourceId;
		unsigned int     YDataSourceId;
		unsigned int     XLogicalChannel;
		unsigned int     YLogicalChannel;
	
		//  default c'tor -> initialize properly; unitialized entries
		//  are set to (unsigned int)-1
		_MapInfo();
    };
    typedef _MapInfo MapInfo;
    

    //
    //  Prohibit these
    //
    PCIntVisBuffer();
    PCIntVisBuffer( const PCIntVisBuffer& );
    const PCIntVisBuffer& operator=( const PCIntVisBuffer& );
};



#endif //includeguard

#endif //PCINT support req'd?
