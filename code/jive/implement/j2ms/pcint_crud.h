// this stuff is needed to do the bookkeeping and transforming
// pcint correlatorboard datastreams into blocks of baselines
//
//
// $Id: pcint_crud.h,v 1.3 2011/10/12 12:45:32 jive_cc Exp $
//
//

// only build if PCInt support req'd
#ifdef PCINT

#ifndef J2MS_PCINT_CRUD_H
#define J2MS_PCINT_CRUD_H

// for jive specific stuff
#include <jive/labels/CorrelationCode.h>

// C++ stuff
#include <list>
#include <string>
#include <map>
#include <set>
#include <algorithm>

// PCInt (tm) includes ..
#include <module/Module.h>
#include <types/AlbertTypes.h>
#include <cbtypes/CBDataBlock.h>
#include <cbtypes/RAWDataBlock.h>
#include <pcintprocessing/datum.h>
#include <myutil/fieldmatcher.h>

// for UINT_MAX
#include <limits.h>

// for each datafile we need a decoder 
// and possibly keep track of other stuff
// 
// let's stick 'm in a map<K,V> where
// the K(ey) is the CB-number
// the V(alue) is a struct where we can
// stick whatever we need on a per-datafile
// basis
struct datafile {

    // ensure proper initialization
    //  have to pass at least the filename 
    //   [the thing will test if we have read-access to
    //    it], and optionally you can override the
    // decoder used to decode the data
    datafile( const std::string& filenm,
              const std::string& decodernm="decoder/evn.so" );

    // get next datablock from the file. 
    // empty block if nothing more to read
    pcint::blocklist_t::value_type getNextBlock( void );

    const std::string& dfName( void ) const
    {
        return filename;
    }

    // cleans up after ourselves
    ~datafile();
        
    private:
        int                          fd;
        std::string                  filename;
        Module                       decoder;
        // we remember the blocklist and where we left off
        pcint::blocklist_t           decodedblocks;
        pcint::blocklist_t::iterator block2get;

        // prohibit default c'tor!
        datafile();

        // and prohibit copy/assignment
        datafile( const datafile& );
        const datafile& operator=( const datafile& );
};
typedef std::map<unsigned int,datafile*> datafilemap;



// Let's define a reworked "data block",
// a pcint integration.
// Contains a set of "logical hardware IDs"
// that make up the set of datablocks that describe/
// make up the full datapart of the integration
// Also contains a list of baselines which describes
// the baselines that can be found in this datablock
// and each baseline contains up to four 'interferometers'
// which are ordered by "linear correlation code"
// (see 'labels/CorrelationCode.h, code2linear()')
// and hold 'offset,nlag' for each of the dataparts
// The baseline has properties 'xant', 'yant' and
// 'subband', the general props of this'un

// pcint interferometer
struct pcint_intf {
    // describes 'nlag' of data
    // at offset 'offset' wrt
    // 'pcint_integration.datablock.begin()'
    // NOTE: datatype is A.Bos' datatype
    // with bitflags telling you what
    // kind of interferometer it is..
    int               datatype;
    unsigned int      nlag;
    unsigned int      offset;
    unsigned int      woffset; // offset to the weight
    int               ab_intf;
    bool              swapped;
    // keep track if data did arrive
    // for this'un
    bool              data_present;
    // have to keep track in which
    // datablock this interferometer's 
    // data is located
    albert::loghwid_t loghwid;

    pcint_intf():
        datatype( 0 ),
        nlag( 0 ),
        offset( 0 ),
        woffset( 0 ),
        ab_intf( -1 ),
        swapped( false ),
        data_present( false ),
        loghwid( albert::generate_loghwid(UINT_MAX,albert::MAX_BOARD_TYPES,
                                          UINT_MAX,albert::MAX_UNIT_TYPES,
                                          UINT_MAX) )
    {
    }

    pcint_intf( int dt, unsigned int nl,
                unsigned int o, unsigned int wo, int abi,
                bool swp, albert::loghwid_t hwid ):
        datatype( dt ),
        nlag( nl ),
        offset( o ),
        woffset( wo ),
        ab_intf( abi ),
        swapped( swp ),
        data_present( false ),
        loghwid( hwid )
    {
    }
};

std::ostream& operator<<(std::ostream& os, const pcint_intf& pi );

// the baseline holds a number of interferometers
// they're accessed by "correlation code", which
// is j2ms2 speak for "polarization-combination"
// There is a global 'operator<()' for CorrelationCodes
// which therefore should implement the strict weak 
// ordering necessary for the map.
typedef std::map<CorrelationCode,pcint_intf> pcint_ifmap;

struct pcint_baseline {
    int         xant, yant, subband;
    pcint_ifmap ifmap;

    // indices of antennas and subband can never be <0
    // so by default set them to 'invalid' parameters
    // such that they may be detected at runtime.
    // (0 *is* a valid index..)
    pcint_baseline():
        xant( -1 ),
        yant( -1 ),
        subband( -1 )
    {}

    pcint_baseline( int x, int y, int sb ):
        xant( x ),
        yant( y ),
        subband( sb )
    {}
};

std::ostream& operator<<( std::ostream& os, const pcint_baseline& pbl );

// introduce a 'descriptive name' by introducing a typedef
typedef std::list<pcint_baseline>   pcint_baselinelist;


// define what's contained in a pcint_integration:
// - a list of baselines found in this integration
// - a datapart which contains the data for this integration
// - auxiliary information:
//      * set of 'datablocks' which partake in this integration
//        [let's use std::set and make stuff in the datablock
//         mutable: std::set's elements are const, however,
//         WE know that we'll insert them keyed on "loghwid"
//         and the other datamembers do not take part in key-indexing.
//         I bring this up because the reason for std::set to have its
//         datamembers be 'const' is to prevent the user from
//         messing up the sort-order.. In std::set, (which is basically 
//         a map<Key, Value> where the 'key' *is* the 'value'), as said,
//         the key by which elements are inserted is the value and as such,
//         if you tinker with the value, you can mess up the internals
//         of the set; the algorithms won't work as expected anymore
//         or at least, it cannot be guaranteed since they rely on the 
//         fact that a set is 'a model of a sorted, unique, associative 
//         container']
//      * and their offset into the datapart
//      combine these two into a map of
//       'loghwid' (which uniquely identifies a 
//                  datablock)
//       -> {offset,present}
//       [we must be able to tell whether or not data
//        for a datablock has arrived; in PCInt there's
//        no guarantee that we will always have all
//        data]

struct pcint_datablock {
    // this will be the 'key' used for sorting
    // the datablocks
    albert::loghwid_t   loghwid;
    
    // these properties must be mutable
    // (once the 'datablock' is in
    // the set, we must be able to
    // change these mothers)
    mutable bool          present;

    // this needn't be mutable,
    // once the datablock is in the
    // set, you shouldn't be able
    // to tinker with *these* members
    unsigned int          offset;
    unsigned int          size;

    // default object: hwid set to invalid
    // (if that's possible at all... basically
    //  all values encoded therein, even when
    //  "filled to the max" (ie: 0xffffffff)
    //  still represent valid values..)
    pcint_datablock():
        loghwid( albert::generate_loghwid(UINT_MAX,albert::MAX_BOARD_TYPES,
                                          UINT_MAX,albert::MAX_UNIT_TYPES,
                                          UINT_MAX) ),
        present( false ),
        offset( (unsigned int)-1 ),
        size( (unsigned int)-1 )
    {
    }

    // fully construct
    pcint_datablock( albert::loghwid_t hwid, unsigned int o, unsigned int s ):
        loghwid( hwid ),
        present( false ),
        offset( o ),
        size( s )
    {
    }
};

// a functor which implements the 'strict weak ordering' for
// pcint_datablock's, we use the 'loghwid' field for
// deciding which datablock is "less than" an other datablock
struct less_than_datablock {
    bool operator()( const pcint_datablock& l, const pcint_datablock& r ) const
    {
        return (l.loghwid<r.loghwid);
    }
};

// introduce 'readability' by introducing (yet another...) typedef
typedef std::set<pcint_datablock,less_than_datablock>  pcint_datablockset;

// find a datablock by loghwid
#define finddb_byhwid(a, b) \
    std::find_if(a.begin(), a.end(), pcint::Field(&pcint_datablock::loghwid)==b)

//
// *finally* we have all stuff defined that
// makes up a PCInt integration
//
struct pcint_integration {
   unsigned int         start_bocf;
   unsigned int         end_bocf;
   RAWDataBlock         data;
   pcint_baselinelist   baselines;
   pcint_datablockset   datablocks;

   pcint_integration( unsigned int dbsize,
                      const pcint_datablockset& dbset,
                      const pcint_baselinelist& blist ):
       start_bocf( 0xffffffff ),
       end_bocf( 0xffffffff ),
       data( dbsize ),
       baselines( blist ),
       datablocks( dbset )
   {
   }
};
std::ostream& operator<<(std::ostream& os, const pcint_integration& intg);

// and a list of those mothers!
typedef std::list<pcint_integration> pcint_integrationlist;


#endif // include guard

#endif // PCInt support req'd?
