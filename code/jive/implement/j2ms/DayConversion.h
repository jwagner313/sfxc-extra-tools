//
//      Provide transformations from daynr to (day, month) and vice versa
//
//  Author:  Harro Verkouter,   15-6-1999
//
//  $Id: DayConversion.h,v 1.4 2006/03/02 14:21:39 verkout Exp $
//
#ifndef DAYCONVERSION_H
#define DAYCONVERSION_H


//
// Time conversion stuff
//
typedef struct _DayConversion
{
public:
    //
    //  Only allowed c'tor -> default -> init everything (if that's necessary)....
    //
    _DayConversion();

    //
    //  Some useful constants....
    //
    static const unsigned int secondsPerDay;

    //  Convert daynr to day/month and vice versa
    //
    //  Month 0 == January
    //  Day 0   == first day of month
    //  Daynr 0 == January first of year
    static bool  dayNrToMonthDay( unsigned int& month, unsigned int& day,
                                  unsigned int daynr, int year );
    static bool  dayMonthDayToNr( unsigned int& daynr, unsigned int month,
                                  unsigned int day, int year );
    
    //
    // Tell us if the year 'year' is a leap year or not...
    //
    static bool  isLeapYear( int year );

    //  If We Allocated stuff, release it
    ~_DayConversion();
    
private:
    //
    //  All our private parts
    //
    static const unsigned int       daysPerMonth[ ];
    static const unsigned int       daysPerMonthLeap[ ];

    //
    //  Prohibit these functions....
    //
    _DayConversion( const _DayConversion& );
    _DayConversion& operator=( const _DayConversion& );
} DayConversion;



#endif // DAYCONVERSION_H
