//  AntennaInfo:   a class describing the properties of an antenna
#ifndef JIVE_J2MS_ANTENNAINFO_H
#define JIVE_J2MS_ANTENNAINFO_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <measures/Measures/MPosition.h>

#include <iostream>

class AntennaInfo
{
    // Show this station's info neatly formatted on an outputstream
    friend std::ostream& operator<<( std::ostream& os, const AntennaInfo& airef );

public:
    //  Create an empty AntennaInfo.
    //  As '0' would be a valid Id, this field is set to -1
    AntennaInfo();
    
    //  Create a 'fully' defined antenna info object. NOTE: 'tlc' =
    //  t(wo) l(etter) c(ode)!
    //  HV 17/01/2007 - Decided to make all arguments obligatory.
    //                  [all but name & tlc used to be voluntary]
    //                - Added the antId parameter. We must be 
    //                  able to tell which Id the antenna was
    //                  [used to be the array-index, better let the
    //                   antenna-database constructor decide what the
    //                   (numeric) Id of this antenna is]
    //                 - Passing in an offset-array of incorrect length
    //                   (ie: not length 3) throws an exception
    //                 - The TLC is not retained in the MSAntenna table
    //                   so we should feature a c'tor that may leave it
    //                   out AND we should ignore the TLC when deciding
    //                   if two antennas are equal
    AntennaInfo( const casa::String& name, 
				 const casa::String& station,
                 const casa::MPosition& pos,
				 casa::Double diameter,
                 const casa::String& mount,
                 const casa::Vector<casa::Double>& offset, 
                 casa::Int antId,
                 const casa::String& tlc = "" );

    //  Get access to the private parts of this object:
    casa::Double                getDiameter( void ) const;
    const casa::String&         getName( void ) const;
    const casa::String&         getTLC( void ) const;
    const casa::String&         getStation( void ) const;
    const casa::String&         getMount( void ) const;
    const casa::MPosition&      getPosition( void ) const;

    const casa::Vector<casa::Double>& getOffset( void ) const;

    casa::Int                   getId( void ) const;

    // Comparison
    bool operator==( const AntennaInfo& other ) const;
    bool operator!=( const AntennaInfo& other ) const;

    ~AntennaInfo();

private:
    // MPosition already keeps track of Units & reference frame
    // so we do not have to record that separately - and we don't care
    casa::MPosition         itsPosition;

    // There are a few "name" like properties
    //  The name of this particular antenna, like WSRT8
    casa::String            itsName;
    
    //  The official two-lettercode of this antenna
    casa::String            itsTLC;
    
    //  The station name, like VLA or WSRT, or Jodrell Bank
    casa::String            itsStation;

    //  The mount. Basically any string could be used...
    casa::String            itsMount;

    //  The diameter of the dish in meters
    casa::Double            itsDiameter;

    //  The XYZ-offset in meters. 
    casa::Vector<casa::Double>    itsOffset;

    casa::Int               itsId;
};

#endif
