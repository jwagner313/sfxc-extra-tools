//  Scan - Abstract baseclass that defines the interface for accessing
//  the particular properties of a scan.
//
//  Make derived class as necessary...
//
//
//
//  NOTE:  Read through the class interface to find out which functions
//  *must* be implemented.
//
//  NOTE2: You can overload 'operator<<( ostream&, const <your class>&
//  )' -> merely showing info about the Scan in HRF, usually used for
//  displaying purposes.
//
//  NOTE3: There is no public c'tor -> this is to ensure that only
//  experiments can create valid Scan-objects.
//
//
//
//  Author:   Harro Verkouter,  7-12-1998
//
//  $Id: Scan.h,v 1.8 2007/02/26 13:23:38 jive_cc Exp $
//
#ifndef SCAN_H
#define SCAN_H


#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <measures/Measures/MEpoch.h>
#include <casa/Quanta/MVEpoch.h>
#include <casa/Quanta/QLogical.h>
//#include <jive/j2ms/CorrelatorSetup.h>
//#include <jive/j2ms/VisibilityBuffer.h>

#include <iostream>

class Experiment;
class VisibilityBuffer;


//  This struct can be used as a utility; it holds all the basic
//  scanparameters. Use it to cache scan-information or pass it roud
//  or whatever you feel it fit for....
typedef struct _ScanInfo {
	casa::Int             scanNo;
    casa::String          scanId;
    casa::String          sourceCode;
    casa::String          frequencyCode;
    casa::MVEpoch         scanStart;
    casa::MVEpoch         scanEnd;
//    CorrelatorSetup correlatorSetup;
} ScanInfo;





// HV: 06-06-2001 - Drat! Namespace problems. Conflict with Scan-object
//                  in job_descr......
//                  Solution: put our own definition of Scan into a 
//                  separate namespace...
namespace j2ms
{
    //  Defines the interface to a Scan
    class Scan {

        public:
            //  (RO)Access to properties of this scan.

            //  Textual representation of the scanId
            virtual const casa::String&     getScanId( void ) const = 0;

            //  Numerical representation of the scanId, could be written
            //  to e.g. MeasurmentSet as scanindicator....
            virtual casa::Int               getScanNo( void ) const = 0;

            //  What source were we looking at?
            virtual const casa::String&     getSourceCode( void ) const = 0;
    
            //  What was the frequency setup?
            virtual const casa::String&     getFrequencyCode( void ) const = 0;

            //  What was the Correlator setup used when correlating this scan?
//            virtual const CorrelatorSetup& getCorrelatorSetup( void ) const = 0;

            //  ...
            virtual const casa::MEpoch&     getStartTime( void ) const = 0;
            virtual const casa::MEpoch&     getEndTime( void ) const = 0;

            //  The scan should have a visibility-buffer where the system can
            //  read visibilities from!
            const VisibilityBuffer&         getVisibilityBuffer( void ) const;

            //  Be able to retrieve experiment info via this:
            //
            //  NOTE: This is NOT a virtual function; you're not supposed to
            //  implement it. The Experiment will be stored in this class and
            //  is a required constructor parameter!
            const Experiment&               getExperiment( void ) const;
    
            //  Give derived classes a chance to delete resources
            virtual ~Scan();

        protected:
            //  Create a Scan, telling the Scan which VisBuffer it came
            //  from. It will take the "Experiment" from that
            Scan( const VisibilityBuffer* visbufptr );

        private:
            // A scan is connected to a 'parent' VisibilityBuffer
            const VisibilityBuffer* itsVisibilityBufferptr;

            //  Empty scans don't exist nor makes it sense to copy them
            Scan();
            Scan( const Scan& );
            const Scan& operator=( const Scan& );
    };

    //  Show summary of the scan on a stream
    std::ostream& operator<<( std::ostream& os, const Scan& sref );

// End of namespace j2ms
};

// helper functor to decide/find scanInfo's by time!
//
// Note: this is NOT meant for deciding wether a *visibility*
// falls completely within a scan (visibility has a duration)
// but wether e.g. a modelpolynomial record applies to a scan.
// 
// Note: A poly-record has a duration too but it is not
// quite important wether or not the record falls completely
// within the scanboundaries. For Visibilities this is different.
//
// Note: We do not have to anticipate the condition
// (start_of_model_record + duration_of_model_record)>=scanStart.
// Model-computation is always done from scanStart onwards.
// There are no entries overlapping from a previous scan. Well, there
// can be, but they are not taken to be valid for the following scan.
struct mepoch_in_scaninfo {
   
    mepoch_in_scaninfo( const casa::MVEpoch& mv ):
        __timeinst( mv.getTime("s") )
    {}

    // .getValue() gives a MVEpoch,
    // .getTime("s") gives the time in seconds from the MVEpoch
    mepoch_in_scaninfo( const casa::MEpoch& me ):
        __timeinst( me.getValue().getTime("s") )
    {}
    
    // return true iff __timeinst falls in the
    // si.scanStart/si.scanEnd range
    // ie: >= si.scanStart && <=si.scanEnd
    // Note: the values in the ScanInfo are
    // MVEpochs so we only need to '.getTime("s")' on them
    // Deliberately chose to include <=scanEnd because if
    // the condition == scanEnd holds, the record DOES describe
    // (albeit a very small) part of the scan. 
    // In this case, the record describes the model at the end
    // of the scan and hence can be used as boundary condition
    // when fitting the modelpolys (if necessary).
    bool operator()( const ScanInfo& si ) const {
        casa::Quantity   scanstart( si.scanStart.getTime("s") );
        casa::Quantity   scanend( si.scanEnd.getTime("s") );
        
        return (__timeinst>=scanstart && __timeinst<=scanend);
    }

    // Also supports checking if the condition holds for 
    // a Scan...
    bool operator()( const j2ms::Scan& s ) const {
        casa::Quantity   scanstart( s.getStartTime().getValue().getTime("s") );
        casa::Quantity   scanend( s.getEndTime().getValue().getTime("s") );
        
        return (__timeinst>=scanstart && __timeinst<=scanend);
    }
    // Or a pointer to scan!
    bool operator()( const j2ms::Scan* sptr ) const {
        return (*this)(*sptr);
    }

    casa::Quantity  __timeinst;
};

// find an entry in a scanlist for which the 'mepoch_in_scaninfo'
// holds.
#define find_scaninfo_by_time(a,b) \
    std::find_if(a.begin(), a.end(), mepoch_in_scaninfo(b))

#endif
