//  Implementation of the SourceInfo class...
//
//
//  Author:   Harro Verkouter, 2-12-1998
//
//  $Id: SourceInfo.cc,v 1.5 2007/02/26 13:26:15 jive_cc Exp $
//
#include <jive/j2ms/SourceInfo.h>

#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/ArrayLogical.h>

#include <iomanip>

using namespace std;
using namespace casa;

ostream& operator<<( ostream& os, const SourceInfo& siref ) {
    os << siref.itsCode <<  "(#" << siref.itsId << "): "
       << siref.itsName << " is at " << siref.itsPosition;
    return os;
}

// Default Construct a complete object
SourceInfo::SourceInfo( ) :
    itsId( -1 ),
    itsName( "" ),
    itsCode( "" )
{}

//  Construct a complete object
SourceInfo::SourceInfo( Int id, const String& name, const String& code, const MDirection& pos ) :
    itsId( id ),
    itsPosition( pos ),
    itsName( name ),
    itsCode( code )
{}

Int SourceInfo::getId( void ) const {
    return itsId;
}

const String& SourceInfo::getName( void ) const {
    return itsName;
}

const String& SourceInfo::getCode( void ) const {
    return itsCode;
}

const MDirection& SourceInfo::getDirection( void ) const {
    return itsPosition;
}

bool SourceInfo::operator==( const SourceInfo& other ) const {
    return ( itsId==other.itsId &&
             itsName==other.itsName &&
             allNear(itsPosition.getValue().getValue(),
                     other.itsPosition.getValue().getValue(), 1.0e-13) );
}

bool SourceInfo::operator!=( const SourceInfo& other ) const {
    return !(this->operator==(other));
}

SourceInfo::~SourceInfo()
{}

