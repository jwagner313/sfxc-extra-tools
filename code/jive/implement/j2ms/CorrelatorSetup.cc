//  Implementation of the CorrelatorSetup - class
//
//  Author:   Harro Verkouter,  13-07-1999
//
//
//    $Id: CorrelatorSetup.cc,v 1.17 2011/10/12 12:45:28 jive_cc Exp $
//
//
//    $Log: CorrelatorSetup.cc,v $
//    Revision 1.17  2011/10/12 12:45:28  jive_cc
//    HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//          changed into 'labels' for more genericity
//        * can now be built either as part of AIPS++/CASA [using makefile]
//          or can be built as standalone set of libs + binaries and linking
//          against casacore [using Makefile]
//          check 'code/
//
//    Revision 1.16  2009-07-03 10:23:55  jive_cc
//    HV: 2/7/2009 - Albert version 4.0 (recirculation) => NO offset between data and header.
//
//    Revision 1.15  2008-08-08 11:26:07  jive_cc
//    HV:  output operator for CorrelatorSetup changed, it now also displays
//         the 'delta-t' value: the offset between headers and data in
//         units of BOCFs ("dT")
//
//    Revision 1.14  2008-07-31 10:47:36  jive_cc
//    HV - 31/07/2008  New version of Albertsoftware (3.0): the diff between
//                     header & data is now only 1 BOCF, rather than 2, what
//    		 it used to be up until now. Modifications implemented
//    		 that set the actual value of the diff based on AB
//    		 s/w version. Name of the entry in the JIVE_PROCESSING table
//    		 (2BOCF_FIX or something) left unchanged but the actual
//    		 amount of time-fixing *is* altered. The name of the entry
//    		 left untouched to not harm downstream apps that may
//    		 rely on this name.
//
//    Revision 1.13  2007/05/25 09:35:28  jive_cc
//    HV: - CorrelatorSetup now also has bocfrate (<0 => unknown,
//        is for backward compat) [if known, we can do the 2BOCF
//        fix JIVE specific processing]
//        - move away from homegrown RegularExpression class to
//        casa::Regex
//
//    Revision 1.12  2007/03/15 16:31:46  jive_cc
//    HV:  * CorSWVersion removed as nested class of CorrelatorSetup
//         * Replaced by more generic SWVersion class
//         * rewrote relational operators for SWVersion in expressions
//           only involving the basic "less than" operator for SWVersions
//
//    Revision 1.11  2007/02/26 11:22:10  jive_cc
//    HV: Now uses sequences of physical polarization combinations to
//        keep track of computed correlator products. The data for the
//        visibilities is delivered in the order of "getCorrelations()"
//        Requires (and can give you) the logical-to-physical polarization
//        mapping to map between logical indices (usually '0' and '1')
//        and physical polarizations they represent
//
//    Revision 1.10  2006/03/02 14:21:39  verkout
//    HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//    Revision 1.9  2006/02/17 12:41:26  verkout
//    HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//
//    Revision 1.8  2006/02/10 08:53:43  verkout
//    HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//    Revision 1.7  2006/01/13 11:35:37  verkout
//    HV: CASAfied version of the implement
//
//    Revision 1.6  2004/09/29 15:45:59  verkout
//    HV: Hmmm.. 'major' and 'minor' are macros under Linux.. changed the memberfunctions to getMajor/getMinor :(
//
//    Revision 1.5  2004/08/25 05:57:44  verkout
//    HV: * Got rid of 'uint16' (replaced with 'unsigned int').
//          (not everywhere yet)
//        * Started using CountedPointers to DataFileRecords in
//          COF* classes (such that datafilerecords, read from file
//          get automagically deleted when nobody refers to them
//          anymore
//        * Added some exception catching here and there...
//
//    Revision 1.4  2004/01/05 15:01:53  verkout
//    HV: Probs. with templates. Moved Container to different namespace.
//
//    Revision 1.3  2003/09/12 07:26:42  verkout
//    HV: Code had to be made gcc3.* compliant.
//
//    Revision 1.2  2001/05/30 11:47:09  verkout
//    HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//    Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//    HV: imported aips++ implement stuff
//
//
#include <jive/j2ms/CorrelatorSetup.h>
#include <casa/Utilities/Regex.h>


#include <iostream>
#include <fstream>
#include <algorithm>
#include <iterator>

#include <ctype.h>
#include <string.h>

using namespace std;
using namespace casa;

// ***************************
// Correlator software version
// ***************************

SWVersion::SWVersion(unsigned int mjr, unsigned int mnr):
    myMajor(mjr), myMinor(mnr)
{}

unsigned int SWVersion::getMajor( void ) const {
    return myMajor;
}
unsigned int SWVersion::getMinor( void ) const {
    return myMinor;
}

ostream& operator<<(ostream& os, const SWVersion& sw ) {
    return os << sw.getMajor() << "." << sw.getMinor();
}

// This is the only real operator. The rest of the comparisons
// derive their value from applying variations of this operator
bool operator<(const SWVersion& l, const SWVersion& r) {
    return (l.getMajor()<r.getMajor() ||
            (l.getMajor()==r.getMajor() && l.getMinor()<r.getMinor()));
}
// equality is implied when neither is < the other..
bool operator==(const SWVersion& l, const SWVersion& r) {
    return ((l<r)==false) && ((r<l)==false);
}
// inequality is implied when either l<r or r<l
// tecnically speaking, they can never be both true,
// unless there is a programming error in "operator<()"
bool operator!=(const SWVersion& l, const SWVersion& r) {
    return ((l<r) || (r<l));
}
// less-than-or-equal is implied by
// l actually being less-than r
// OR r not being less-than l
// [making use of the fact that the second only
//  gets evaluated if the first condition (l<r)
//  evaluates to false]
bool operator<=(const SWVersion& l, const SWVersion& r) {
    return ((l<r) || (r<l)==false);
}

// l>r is implied by r < l
bool operator>(const SWVersion& l, const SWVersion& r) {
    return (r<l);
}
// l>=r is implied by r being less-than l 
// OR l not being less-than r,
// again making use of the fact that the second
// is only evaluated if the first evaluates to false
bool operator>=(const SWVersion& l, const SWVersion& r) {
    return ((r<l) || (l<r)==false);
}







//  Alloc space for and initalize static datamembers of CorrelatorSetup-class
const SWVersion CorrelatorSetup::bypassVersion =  SWVersion(12345, 67890);


static const char* verswarn = "**** WARNING: SYSTEM IS IN BYPASS MODE!\n"
							  "     DATA WILL BE IN RAW FORMAT; NO CORRECTIONS APPLIED!";



ostream& operator<<( ostream& os, const CorrelatorSetup& csref ) {
    string           brs( "**" );
	const SWVersion  swver( csref.getSoftwareVersion() );

    if( csref.getBocfRate()>0 ) {
        ostringstream    oss;
        oss << csref.getBocfRate() << "Hz";
        brs = oss.str();
    }

    os << "Nf " << csref.nrOfSpectralPoints() << "/"
       << "Ti " << csref.getIntegrationTime() << "s/"
       << "P " << as_corrcode(csref.getCorrelations()) << "/"
       << "ABsw v. " << swver << "/"
       << "BOCF " << brs << "/"
	   << "dT " << csref.headerDataBocfDiff();
    if( swver==CorrelatorSetup::bypassVersion )
        os << endl << verswarn << endl;
    return os;
}

//  Comparison
Bool operator==( const CorrelatorSetup& lhside, const CorrelatorSetup& rhside ) {
    Bool                     cond1;
    Bool                     cond2;
    Bool                     cond3;
    Bool                     cond4;
    Bool                     cond5;

    cond1 = (lhside.myNumberOfSpectralPoints == rhside.myNumberOfSpectralPoints);
    cond2 = (lhside.myIntegrationTime == rhside.myIntegrationTime);
    cond3 = (lhside.myCorrelations == rhside.myCorrelations);
	cond4 = (lhside.mySoftwareVersion == rhside.mySoftwareVersion);

    // Only compare bocfrates if both>1 (ie, known).
    // comparing to (at least) an unknown bocfrate will always evaluate
    // to false
    int  lbr=lhside.getBocfRate();
    int  rbr=rhside.getBocfRate();
    
    if( (cond5=(lbr>0 && rbr>0)) )
        cond5 = (lbr==rbr);

    return (cond1 && cond2 && cond3 && cond4 && cond5);
}


Bool operator!=( const CorrelatorSetup& lhside, const CorrelatorSetup& rhside ) {
    return !(lhside==rhside);
}




CorrelatorSetup::CorrelatorSetup() :
    myNumberOfSpectralPoints( (unsigned int)-1 ),
    myIntegrationTime( 0.0 ),
    myBocfRate( 0 ), myHeaderDataBocfDiff( 0 )
{}

CorrelatorSetup::CorrelatorSetup( unsigned int nrofspectralpoints,
                                  const correlationset_t& correlations,
								  double integrationtime,
								  const SWVersion& swver,
                                  const j2ms::log2polmap& l2p,
                                  int bocfrate ) :
    myNumberOfSpectralPoints( nrofspectralpoints ),
    myCorrelations( correlations ),
    myIntegrationTime( integrationtime ),
	mySoftwareVersion( swver ),
    myL2PMap( l2p ),
    myBocfRate( bocfrate ),
    myHeaderDataBocfDiff( 2 )
{
    // As of AB software version 3.0, the diff between data-header is only one bocf
    if( mySoftwareVersion>=SWVersion(3, 0) )
        myHeaderDataBocfDiff = 1;
    // *sigh* 03072009 - After yet another Albert-update (recirculation)
    //                   the offset is at zero. Its version shall be >= 4.0
    if( mySoftwareVersion>=SWVersion(4,0) ) 
		myHeaderDataBocfDiff = 0;
}

unsigned int CorrelatorSetup::nrOfSpectralPoints( void ) const {
    return myNumberOfSpectralPoints;
}

const correlationset_t& CorrelatorSetup::getCorrelations( void ) const {
    return myCorrelations;
}

double CorrelatorSetup::getIntegrationTime( void ) const {
    return myIntegrationTime;
}

const SWVersion& CorrelatorSetup::getSoftwareVersion( void ) const {
	return mySoftwareVersion;
}

const j2ms::log2polmap& CorrelatorSetup::l2pmap( void ) const {
    return myL2PMap;
}

int CorrelatorSetup::getBocfRate( void )const {
    return myBocfRate;
}

int CorrelatorSetup::headerDataBocfDiff( void ) const {
    return myHeaderDataBocfDiff;
}

//  Delete any allocated resources...
CorrelatorSetup::~CorrelatorSetup()
{}


/***************************************/
//
//    STATIC METHOD(s)
//
/***************************************/
CorrelatorSetup CorrelatorSetup::correlatorModeToSetup( int correlatormode,
                                                        double integrationtime,
                                                        const j2ms::log2polmap& l2p,
                                                        int bocfrate )
{
	SWVersion         swver;
    unsigned int      nrcombinations( 0 );
    unsigned int      nrfreqpoints( 0 );
    correlationset_t  correlations;
    
    switch( correlatormode )
    {
	case 201:
	case 203:
	    nrfreqpoints = 32;
	    nrcombinations = 1;
	    break;
	    
	case 207:
	case 209:
	    nrfreqpoints = 32;
	    nrcombinations = 2;
	    break;

	case 213:
	case 215:
	case 217:
	case 219:
	    nrfreqpoints = 32;
	    nrcombinations = 4;
	    break;
	    
	case 221:
	case 223:
	case 225:
	    nrfreqpoints = 64;
	    nrcombinations = 1;
	    break;

	case 229:
	case 231:
	case 233:
	    nrfreqpoints = 64;
	    nrcombinations = 2;
	    break;

	case 237:
	case 239:
	case 241:
	    nrfreqpoints = 64;
	    nrcombinations = 4;
	    break;
	    
	case 243:
	case 245:
	case 247:
	case 249:
	    nrfreqpoints = 128;
	    nrcombinations = 1;
	    break;

	case 251:
	case 253:
	case 255:
	    nrfreqpoints = 128;
	    nrcombinations = 2;
	    break;

	case 257:
	case 259:
	    nrfreqpoints = 128;
	    nrcombinations = 4;
	    break;
	    
	case 261:
	case 263:
	case 265:
	    nrfreqpoints = 256;
	    nrcombinations = 1;
	    break;

	case 267:
	case 269:
	    nrfreqpoints = 256;
	    nrcombinations = 2;
	    break;

	case 271:
	    nrfreqpoints = 256;
	    nrcombinations = 4;
	    break;
	    
	case 273:
	case 275:
	    nrfreqpoints = 512;
	    nrcombinations = 1;
	    break;
	    
	case 277:
	    nrfreqpoints = 512;
	    nrcombinations = 2;
	    break;
	    
	case 279:
	    nrfreqpoints = 1024;
	    nrcombinations = 1;
	    break;
	    
	default:
	    break;
    }

    if( nrcombinations<1 ) {
        cerr << "correlatorModeToSetup()/correlator mode #" << correlatormode << endl
             << "apparently not recognized, #-of-polarization-combinations==0!" << endl;
        return CorrelatorSetup();
    }
    if( nrfreqpoints==0 ) {
        cerr << "correlatorModeToSetup()/correlator mode #" << correlatormode << endl
             << "apparently not recognized, #-of-frequencypoints==0!" << endl;
        return CorrelatorSetup();
    }
    // now create da darn thang! 
    j2ms::polarization                p0 = l2p.find(0);
    j2ms::polarization                p1 = l2p.find(1);
    insert_iterator<correlationset_t> builder( correlations, correlations.begin() );
        
    if( nrcombinations>=1 ) {
        *builder++ = CorrelationCode(p0, p0);
    }
    if( nrcombinations>=2 ) {
        *builder++ = CorrelationCode(p1, p1);
    }
    if( nrcombinations==4 ) {
        *builder++ = CorrelationCode(p0, p1);
        *builder++ = CorrelationCode(p1, p0);
    }
    return CorrelatorSetup(nrfreqpoints, correlations, integrationtime, swver, l2p, bocfrate);
}

// read vexfile and filter out our stuff...
CorrelatorSetup CorrelatorSetup::correlatorSetupFromFile( const string& vexfilename ) {
    ifstream        f;
    CorrelatorSetup rv;

    //  Init result to empty correlatorsetup
    if( vexfilename.empty() ) {
		cout << "CorrelatorSetup::correlatorSetupFromFile() - No vexfilename specified!" << endl;
		return rv;
    }

    //  Now attempt to open the file and see what's it got in store
    //  for us 
    f.open( vexfilename.c_str() );
    
    if( !f.good() ) {
		cout << "CorrelatorSetup::correlatorSetupFromFile() - failed to open "
	    	 << vexfilename << endl;
		return rv;
    }
    cout << "readCorrelatorSetup: '" << vexfilename << "'" << endl;

    //  Need to be able to read old and new format...
    typedef enum _format {
		unknown, oldformat, newformat
    } format;

    typedef enum _crosscorrelations {
		notset, present, notpresent
    } crosscorrelations;
    
    //  Variables that will be filled in from the vex-file
    //  comments/prep_job command line..
    int               cormode;
    int               nlag;
    int               nrecpol;
    int               bocfrate;
    char              linebuf[ 1024 ];
    char              recpol[3] = { "\0" };
    bool              done;
    double            integrationtime;
    format            fileformat = unknown;
	unsigned int      major = (unsigned int)-1;
	unsigned int      minor = (unsigned int)-1;
    crosscorrelations products = notset;
    
    //  Setup regex's for the things we want to look for
    const Regex       pjcmdline( "^\\*[ \t]*prep_job[^:]" );
    const Regex       newformatprefix( "^\\*[ \t]*prep_job::" );

    //  Init correlator-setup values to ludicrous values so we can
    //  easily detect if they've been filled in
    done            = false;
    cormode         = -1;
    nlag            = -1;
    nrecpol         = -1;
    bocfrate        = -1;
    integrationtime = -1.0;
    
    while( !done ) {
    	if( f.eof() ) {
    	    done = true;
    	    continue;
    	}

    	f.getline( linebuf, sizeof(linebuf)-1 );
	
    	if( String(linebuf).find(newformatprefix)!=String::npos ) {
    	    char    keyword[256];
    	    char    value[256];
    	    bool    alreadyset( false );
    	    char*   ptr;
    	    char*   equal;

    	    if( fileformat!=unknown && fileformat!=newformat ) {
        		//  Old/new format mixed...
        		done = true;
        		fileformat = unknown;
        		cout << "readCorrelatorSetup: parse error - old and new formats mixed" << endl;
        		continue;
            }

    	    //  Passed that test. Now check which value is
    	    //  mentioned...
    	    fileformat = newformat;
	    
    	    //  We can do this safely since the RE matched and hence
    	    //  we MUST have at least ONE occurrence of `::' in
    	    //  linebuf, so no check for NULL should have to be
    	    //  done....
    	    ptr   = ::strstr( linebuf, "::" );
    	    equal = ::strchr( ptr, '=' );

    	    if( !equal ) {
        		cout << "readCorrelatorSetup: parse error - failed to get key/value\n"
        		     << "readCorrelatorsetup: in line " << linebuf << endl;
        		continue;
            }

    	    //  Get key/value
    	    *equal = '\0';
    	    ::strncpy( keyword, ptr+2, sizeof(keyword)-2 );
    	    ::strncpy( value, equal+1, sizeof(value)-2 );

            keyword[sizeof(keyword)-1] = '\0';
            value[sizeof(value)-1] = '\0';
	    
    	    //  filter out whitespace...
    	    //  Note: we don't need ptr anymore so we can (re)use that
    	    //  var as a tmp...
    	    ptr = &keyword[ ::strlen(keyword)-1 ];
    	    while( (ptr>=keyword) && isspace(*ptr) ) {
        		ptr--;
    	    }
    	    //  ptr poinnts now *at* the last non-whitespace character
    	    //  in keyword. So let's put a '\0' in after that one!
    	    *(ptr+1) = '\0';
	    
    	    //  Filter out value
    	    ptr = value;
    	    while( isspace(*ptr) ) {
        		ptr++;
    	    }
    	    //  ptr now points at the first non-whitespace char in
    	    //  value... move all the characters to the
            //  beginning of the string
    	    ::memmove( (void *)value, ptr, ::strlen(value)-(ptr-value)+1 );
	    
    	    //  Now remove trailing whitespace
    	    ptr = &value[ ::strlen(value)-1 ];
    	    while( (ptr>=value) && isspace(*ptr) ) {
        		ptr--;
            }
    	    *(ptr+1) = '\0';

    	    //  Now see what we really got...
            //cout << "Decoded: keyword='" << keyword << "', value='" << value << "'" << endl;

    	    if( ::strcmp(keyword, "integration_time")==0 &&
	        	(alreadyset=(integrationtime!=-1.0))==false ) {
        		integrationtime = ::atof( value );
    	    } else if( ::strcmp(keyword, "number_of_lags")==0 && 
        		       (alreadyset=(nlag!=-1))==false ) {
        		nlag = ::atoi( value );
    	    } else if( ::strcmp(keyword, "recorded_polarizations")==0 &&
        		       (alreadyset=(nrecpol!=-1))==false ) {
        		int      cnt;
        		bool     valid;
		
        		//  Find out which polarizations were recorded...
        		nrecpol = ::strlen(value);
		
        		valid = (nrecpol==1 || nrecpol==2);
		
        		for( cnt=0; valid && cnt<nrecpol; cnt++ ) {
        		    recpol[ cnt ] = ::toupper(value[cnt]);
		    
        		    if( !::strchr("RL", recpol[cnt]) ) {
            			break;
    		        }
        		}
		
        		if( valid && cnt<nrecpol ) {
        		    cout << "readCorrelatorSetup: parse error - Value of " << keyword << " out\n"
        			     << "readCorrelatorSetup: of range. Valid values are [L|R|LR|RL]. Found: '"
                         << value << "'" << endl;
        		    recpol[0] = '\0';
        		    nrecpol   = -1;
        		}
    	    } else if( ::strcmp(keyword, "cross_polarizations")==0 &&
        		       (alreadyset=(products!=notset))==false ) {
        		if( ::strcmp(value, "true")==0 ) {
        		    products = present;
        		} else if( ::strcmp(value, "false")==0 ) {
        		    products = notpresent;
        		} else {
        		    cout << "readCorrelatorSetup: parse error - Value of " << keyword << " out\n"
            			 << "readCorrelatorSetup: of range. Valid values are [true|false]. Found '"
                         << value << "'" << endl;
        		    products = notset;
        		}
    	    } else if( ::strcmp(keyword, "ABMajor")==0 &&
    				   (alreadyset=(major!=(unsigned int)-1))==false ) {
    			major = (unsigned int)::atoi( value );
    		} else if( ::strcmp(keyword, "ABMinor")==0 &&
    				   (alreadyset=(minor!=(unsigned int)-1))==false ) {
    			minor = (unsigned int)::atoi( value );
    		} else if( ::strcmp(keyword, "bocf_rate")==0 &&
                       (alreadyset=(bocfrate!=-1))==false ) {
                bocfrate = ::atoi( value );
    		} else {
        		if( alreadyset ) {
        		    cout << "readCorrelatorSetup: parse error - value for " << keyword << "\n"
        	    		 << "readCorrelatorSetup: was alreay set. Discarding value " << value << endl;
        		} else {
        		    cout << "readCorrelatorSetup: Skipping unknown  " << keyword << " = " << value << endl;
        		}
    	    }	    
    	}
    	else if( String(linebuf).find(pjcmdline)!=String::npos ) {
    	    //  We allow only one prep_job commandline!
    	    switch( fileformat )
    	    {
        		case newformat:
        		    cout << "readCorrelatorSetup: parse error - old and new formats mixed" << endl;
        		    fileformat = unknown;
        		    break;

        		case oldformat:
        		    cout << "readCorrelatorSetup: parse error - old format allows only\n"
            			 << "readCorrelatorsetup: one prep_job cmd-line. Discarding this one" << endl;
        		    fileformat = unknown;
        		    break;
		
        		case unknown:
           		default:
        		    fileformat = oldformat;
        		    break;
    	    }
	    
    	    //  If fileformat==unknown at this stage, an error has
    	    //  occurred and we're done!
    	    if( fileformat==unknown ) {
        		done = true;
        		continue;
    	    }

    	    //  Now find out the correlation parameters
    	    char*  cormodeptr( ::strstr(linebuf, "-c") );
    	    char*  inttimptr( ::strstr(linebuf, "-i") );
            char*  bocfrateptr( ::strstr(linebuf, "-b" ) );
	    
            // skip the '-c' and '-i' on both pointers
            // (that is, if they were found at all
    	    if( cormodeptr ) 
                cormodeptr += 2;
            if( inttimptr ) 
                inttimptr += 2;
            if( bocfrateptr )
                bocfrateptr += 2;
            
            // skip spaces on both pointers
            while( cormodeptr && isspace(*cormodeptr) ) 
                cormodeptr++;
            while( inttimptr && isspace(*inttimptr) ) 
                inttimptr++;
            while( bocfrateptr && isspace(*bocfrateptr) ) 
                bocfrateptr++;

            // and get out the values, if any
            (void)(cormodeptr && (cormode=::atoi(cormodeptr)));
            (void)(inttimptr  && (integrationtime=::atof(inttimptr)));
            (void)(bocfrateptr  && (bocfrate=::atoi(bocfrateptr)));

            // done
    	    done = true;
    	}
    }
    f.close();

    //  Phew... now see what we found!
    if( fileformat==unknown ) {
		// Error occurred. Bummer.  We don't mention it since whilst
		// parsing, the user should have found at that something's
		// wrong...
		cout << "COFVisibilityBuffer::readCorrelatorSetup: Unknown fileformat encountered..." << endl;
		return rv;
    }
    
    if( fileformat==newformat ) {
        bool    retval;

		//  test if we got all the info we required from the new fileformat:
		//
		//   nlag            should not be equal to -1
		//   nrecpol         should not be equal to -1
		//   integrationtime should be not equal to -1.0
		//   products        should not be equal to 'notset'
		//   major/minor     well.. this one is different...
		//                   older versions of prep_job did not write
		//                   these values so we must be able to allow
		//                   these to be not present. However, if either
		//                   of the one is unset (i.e. equal to -1) it means
		//                   not both numbers were correctly decoded and hence 
		//                   we should give an error. If both are -1 or both
		//                   are not equal to -1, we accept the values...
        //   bocfrate        optional so not checked. Take '-1' to
        //                   mean 'unknown'
		retval = ( (nlag!=-1) && (nrecpol!=-1) && 
		 		   (integrationtime!=-1.0)  && (products!=notset) && 
			       !((major==(unsigned int)-1 && minor!=(unsigned int)-1) ||
				     (major!=(unsigned int)-1 && minor==(unsigned int)-1))
				);
	
		if( retval ) {
			correlationset_t                  correlations;
            j2ms::log2polmap                  l2p;
            j2ms::polarization                p0 = j2ms::unknown;
            j2ms::polarization                p1 = j2ms::unknown;
            insert_iterator<correlationset_t> builder( correlations, correlations.begin() );

            // nrecpol is *at least* 1
            p0 = j2ms::char2polcode( recpol[0] );
            if( nrecpol>1 )
                p1 = j2ms::char2polcode( recpol[1] );

            switch( nrecpol ) {
                case 1:
                    l2p = j2ms::log2polmap( make_pair(0, p0) );
                    break;
                case 2:
                    l2p = j2ms::log2polmap( make_pair(0, p0),
                                            make_pair(1, p1) );
                    break;
                default:
                   cerr << "readCorrelatorSetup: (newformat) Invalid number of " << endl
                        << "  recorded polarizations " << nrecpol << ". Should be 1 or 2" << endl;
                   return CorrelatorSetup();
            }
	    
			//  nrecpol is either 1,2 or -1...
			//  if it == -1, we don't arrive here...
			*builder++ = CorrelationCode(p0, p0);
            if( nrecpol>1 ) {
				//  We have at least p1,p1 as well
				*builder++ = CorrelationCode(p1, p1);

				//  If cross-polarizations were recorded, add those to the
				//  list!
				if( products==present ) {
					*builder++ = CorrelationCode(p0, p1);
					*builder++ = CorrelationCode(p1, p0);
				}
			}
	    
			//  For backward compatibility we translate a software version
			//  that was not set to vs. 0.0 (older versions of prep_job
			//  did not write the ABMajor/ABMinor keywords in the vex-file).
			//  So if we did not find them, major/minor should be -1.
			//  It suffices to test if only one of them equals -1 since the
			//  test of whether they're both unequal to -1 or both equal to
			//  -1 was already performed.
			if( major==(unsigned int)-1 ) {
				major = 0;
				minor = 0;
			}
			//  Now we can fully create the setup!
			rv = CorrelatorSetup( nlag/2, correlations, integrationtime,
								  SWVersion(major, minor), l2p, bocfrate );
		} else {
			cout << "readCorrelatorSetup: Not all required information found in new format"
				 << endl;
			if( nlag==-1 ) {
				cout << "readCorrelatorSetup: missing number_of_lags keyword\n" << endl;
			}
			if( nrecpol==-1 ) {
				cout << "readCorrelatorSetup: missing recorded_polarizations keyword\n" 
					 << endl;
			}
			if( integrationtime==-1.0 ) {
				cout << "readCorrelatorSetup: missing integration_time keyword\n" 
					 << endl;
			}
			if( products==notset ) {
				cout << "readCorrelatorSetup: missing cross_polarizations keyword\n" 
					 << endl;
			}
			if( major==(unsigned int)-1 ) {
				cout << "readCorrelatorSetup: missing ABMajor keyword\n" 
					 << endl;
			}
			if( minor==(unsigned int)-1 ) {
				cout << "readCorrelatorSetup: missing ABMinor keyword\n" 
					 << endl;
			}
		}
    } else {
        bool retval;
    
        //  Old format, consisting of correlatormode + integration
    	//  time. 
        //  bocfrate is recovered as well but not checked as it is
        //  optional
    	retval = ( (cormode!=-1) && (integrationtime!=-1.0) );
	
    	if( retval ) {
            // log2polmap is default here
            // (so with no entries). In case we end up here
            // the system won't work but really, this
            // method shouldn't be used since the code that
            // used this really had no idea if polarization 0
            // was 'L' or 'R' or whatever...
            // So better let this one crash so it's pretty clear
            // that something very wrong's going on
            j2ms::log2polmap  l2p;
    	    rv = CorrelatorSetup::correlatorModeToSetup(cormode, integrationtime, l2p, bocfrate);

    	    if( rv.nrOfSpectralPoints()==(unsigned int)-1 ) {
        		cout << "readCorrelatorSetup: no CorrelatorSetup found for correlator mode #"
        		     << cormode << endl;

        		rv = CorrelatorSetup();
        		retval = false;
    	    }
    	} else {
    	    cout << "readCorrelatorSetup: Not all required information found in new format"
        		 << endl;
    	}
    }
    return rv;
}
