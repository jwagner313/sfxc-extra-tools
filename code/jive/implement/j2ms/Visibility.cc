//
//  implementation of the Visibility class
//
//
//  Author: Harro Verkouter,  18-11-1998
//
//
#include <jive/j2ms/Visibility.h>

#include <iostream>

using namespace std;
using namespace casa;

ostream& operator<<( ostream& os, const Visibility::domain& domainref ) {
    switch( domainref ) {
	    case Visibility::time:
    	    os << "time";
    	    break;
	    case Visibility::frequency:
    	    os << "frequency";
    	    break;
    	default:
    	    os << "<INVALID>";
    	    break;
    }
    return os;
}


ostream& operator<<( ostream& os, const Visibility& visref ) {
    os << visref.getLabel() << endl 
       << visref.getData() << endl
       << visref.getWeight() << endl;
    return os;
}

Visibility::Visibility() :
    itsFlag( False ),
    itsUVW( j2ms::noMuvw ),
    itsTime( MEpoch() ),
    itsBufferptr( 0 )
{}


Visibility::Visibility( const ExtVisLabel& label, const Matrix<Complex>& data,
			Visibility::domain datadomain, Bool flag,
			const Vector<Float>& weight,
			const MEpoch& time, const Muvw& uvw,
			const VisibilityBuffer* buffer ) :
    itsFlag( flag ),
    itsUVW( uvw ),
    itsTime( time ),
    itsLabel( label ),
    itsDatadomain( datadomain ),
    itsBufferptr( buffer )
{
    itsData.resize( 0, 0 );
    itsData = data;

    itsWeight.resize( 0 );
    itsWeight = weight;
}

Visibility::Visibility( const Visibility& other ) :
    itsFlag( other.itsFlag ),
    itsUVW( other.itsUVW ),
    itsTime( other.itsTime ),
    itsLabel( other.itsLabel ),
    itsDatadomain( other.itsDatadomain ),
    itsBufferptr( other.itsBufferptr )
{
    itsData.resize( 0, 0 );
    itsData = other.itsData;

    itsWeight.resize( 0 );
    itsWeight = other.itsWeight;
}

const Visibility& Visibility::operator=( const Visibility& other )
{
    if( this!=&other ) {
    	itsFlag = other.itsFlag;
    	itsLabel = other.itsLabel;
    	itsDatadomain = other.itsDatadomain;
	
    	itsData.resize( 0, 0 );
    	itsData = other.itsData;

    	itsWeight.resize( 0 );
    	itsWeight = other.itsWeight;
	
    	itsUVW = other.itsUVW;
    	itsTime = other.itsTime;

    	itsBufferptr = other.itsBufferptr;
    }
    return *this;
}

const ExtVisLabel& Visibility::getLabel( void ) const {
    return itsLabel;
}

const Matrix<Complex>& Visibility::getData( void ) const {
    return itsData;
}

Bool Visibility::isAutoCorrelation( void ) const {
    return ( itsLabel.baseline.idx1()==itsLabel.baseline.idx2() );
}

Visibility::domain Visibility::getDatadomain( void ) const {
    return itsDatadomain;
}

Bool Visibility::getFlag( void ) const {
    return itsFlag;
}

const Vector<Float>& Visibility::getWeight( void ) const {
    return itsWeight;
}

const MEpoch& Visibility::getTime( void ) const {
    return itsTime;
}

const Muvw& Visibility::getUVW( void ) const {
    return itsUVW;
}

Bool Visibility::hasVisibilityBuffer( void ) const {
    return ( itsBufferptr!=0 );
}

const VisibilityBuffer& Visibility::getVisibilityBuffer( void ) const {
    return *itsBufferptr;
}



Visibility::~Visibility()
{}
