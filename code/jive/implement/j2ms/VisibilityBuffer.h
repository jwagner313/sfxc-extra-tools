//
//  VisibilityBuffer - an abstract baseclass that defines the
//  interface for classes that would like to act as a visibility
//  buffer (i.e. deliverers of Visibilities!)
//
//
//
#ifndef VISIBILITYBUFFER_H
#define VISIBILITYBUFFER_H

#include <casa/aips.h>
#include <jive/j2ms/scanlist.h>
#include <jive/j2ms/correlatormodel.h>
#include <jive/j2ms/CorrelatorSetup.h>
#include <jive/j2ms/VBufProcessingStep.h>
#include <jive/labels/polarization.h>

//  Forward declarations
namespace j2ms
{
	class Scan;
};
class Visibility;
class Experiment;

class VisibilityBuffer
{
public:

    //  Visibility buffers *must* be attached to an Experiment
    VisibilityBuffer( const Experiment* experiment );
    
    //  Retrieve the experiment
    const Experiment&  getExperiment( void ) const;
    

    // Tell the current scan (if any)
    // may be NULL if either no scan known, or typically,
    // data found outside scanboundaries
    const j2ms::Scan*  getScanptr( void ) const;

    //  Tells us about vernier delay
    casa::Bool               getVernierDelay( void ) const;
    
    //  The VisibilityBuffer must at least implement these functions:

    //  retrieve the current visibility, do NOT increment/move to the
    //  next. NOTE: it changes the argument to be a COPY of the
    //  visibility so the user can mess around with it! Returns True
    //  if the argument 'vis' has been filled in.
    virtual casa::Bool          getCurrentVisibility( Visibility& vis ) = 0;
    
    //  Tell buffer to prepare to get next visibility. NOTE: may also
    //  be used as a check to see if a next visibility is available.
    virtual casa::Bool          next( void ) = 0;

    //  Tell the directory where the datafiles for this
    //  visibilitybuffer actually live!
    virtual const casa::String& getRootDir( void ) const = 0;

	//  Return a unique visibilitybuffer-identifier
	//  May have various uses: if stored together with separate
	//  visibilities, we're always able to work back from which 
	//  specific dataset a visibility originated...
	virtual casa::Int           getVisBufId( void ) const = 0;

	virtual casa::String	    getProcessorSubType( void ) const = 0;

    // It is not unreasonable to assume that the correlator-setup
    // does not vary within a VisibilityBuffer.
    virtual CorrelatorSetup     correlatorSetup( void ) const = 0;
    
    //          ====================== 
    // The methods below _may_ be overridden but have
    // default implementations
    //          ====================== 

    // Return a stationbased scaninfolist
    // (do it by const ref such that we dont
    //  have to copy the data over and over
    //  again)
    //  The default implementation is to
    //  return an empty list.... so only classes
    //  that really can fill it in have to implement
    //  their own vsn.
    virtual const j2ms::stationscanlist_t&
                                getStationScanlist( int station ) const;

    // return the list of scans that could be found in this 
    // visbuffer. default implementation returns empty list.
    // Concrete implementations may deliver the scanlist as they
    // see fit.
    // The list must contain *at least* the scans that may
    // be found in this visbuffer. 
    virtual const j2ms::scaninfolist_t&
                                getScanlist( void ) const;

    // Access to modelrecords.
    // This default implementation returns an empty list, so
    // you only need to do something if you have something
    // to return.
    virtual const j2ms::modeldata_t&
                                getModelData( void ) const;

    // Processing steps, describing "non-standard" processing tasks
    // that the visibilitybuffer may have applied to the data between
    // reading it from disk and feeding it to the filler.
    // Default implementation is to return an empty list.
    virtual processing_steps    getProcessingSteps( void ) const;

    // The JIVEMSFiller will 
    //  Destruct the buffer
    virtual ~VisibilityBuffer();

protected:
    //  Derived classes may set the current scanptr to point at the
    //  scan where the current visibility is attached to
    //  NOTE: the pointer is NOT owned; the usr. is still responsible
    //  for deleting it!
    void setCurrentScanptr( const j2ms::Scan* scanptr ) {
		itsScanptr = scanptr;
    }
    
    
private:

    //  If we are attached to a scan, this ptr points at it. Otherwise
    //  we just don't know!
    const j2ms::Scan*   itsScanptr;
    const Experiment*   itsExperimentptr;
    
    //  Prohibit these
    VisibilityBuffer( const VisibilityBuffer& );
    const VisibilityBuffer& operator=( const VisibilityBuffer& );
};



#endif
