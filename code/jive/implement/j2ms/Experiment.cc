//  Implementation of the experiment class
//
//  Author:  Harro Verkouter, 8-12-1998
//
//
#include <jive/j2ms/Experiment.h>
#include <jive/Utilities/stringutil.h>
#include <jive/Utilities/jexcept.h>


#include <iostream>
#include <fstream>
#include <sstream>

#include <unistd.h>

using namespace std;
using namespace casa;

ostream& operator<<( ostream& os, const Experiment& expref ) {
    os << "************ Experiment " << expref.getCode() << " ************" << endl;
    //os << "Antennas: " << expref.getAntennas() << endl;
    //os << "Sources: " << expref.getSources() << endl;
    os << "Output is placed in: " << expref.getMSname() << endl;
    os << "Data is written in the MS in the " << expref.getDestinationDomain() << " domain" << endl;
    return os;
}




Experiment::Experiment( const String& opt, const String& exprootdir ) :
	myOptions( opt ),
    myRootDir( exprootdir ),
    myLogStreamptr( new ostream* )
{
    (*myLogStreamptr) = (ostream *)0;
    
    //  If the exp. root dir. not set, determine the cwd
    if( myRootDir=="" ) {
		char*   cwd( getcwd((char*)0, 1024) );
	
		if( cwd ) {
			myRootDir = (const char *)cwd;
			::free( cwd );
		}
    }

	// Parse the option string, splitting it at ';'s, getting <key>=<value> pairs
	// First, remove all the whitespace
	string           optcopy;
	string::iterator curchar = myOptions.begin();
	
	while( curchar!=myOptions.end() ) {
		if( !::strchr(" \t\n\f", *curchar) ) {
			//optcopy.push_back( *curchar );
			optcopy.append( 1, *curchar );
		}
		curchar++;
	}

	// Now split the 'optcopy' at ';'s...
	vector<string>           opts;
	vector<string>           kv;
	vector<string>::iterator curopt;

	opts   = ::split( optcopy, ';' );
	for( curopt=opts.begin(); curopt!=opts.end(); curopt++ ) {
		// if the string has no length, skip it
		if( curopt->size()==0 )
			continue;

		// split at =
		kv = ::split( *curopt, '=' );
		
		// if we do not have exactly 2 entries, the option is malformed?
		if( kv.size()!=2 ) {
			cout << "Experiment::Experiment - Malformed option " << *curopt << endl
				 << "                         Not exactly one '='-sign. Expect "
				 << "<key>=<value>" << endl;
			continue;
		}

		const string&   key = kv[0];
		const string&   val = kv[1];

		if( key.size()==0 || val.size()==0 ) {
			cout << "Experiment::Experiment - Malformed option " << *curopt << endl
			     << "                         either <key> or <value> is zero-length!"
			     << endl;
			continue;
		}

		// Add the <key>=<value> entry to the map (if not already defined!)
		map<string,string>::iterator  mapel = myOptionValues.find( key );

		if( mapel!=myOptionValues.end() ) {
			cout << "Experiment::Experiment - AArgh: duplicate definition for key="
				 << key << endl
				 << "                         Discarding value " << val << endl;
			curopt++;
		}

		// Now we can safely define the <key>=<value> pair!
		myOptionValues[ key ] = val;
	}
}

// myOptionValues = map<string,string>
//
// myOptionValues[ <option> ] = <value>
//
// map<T1,T2>::iterator is pointer to pair<T1,T2> struct.
// In this struct we can access the T1 member with 'first' and T2 with 'second', where
// the 'first' is the <key> and the 'second' is the <value>
String Experiment::optionValue( const String& opt ) const {
	String                             retval;
	map<string,string>::const_iterator optptr = myOptionValues.find( opt );

	if( optptr!=myOptionValues.end() )
		retval = optptr->second;
	return retval;
}

const String& Experiment::getArrayCode( void ) const {
    static String   defaultArrayCode( "EVN" );
    
    return defaultArrayCode;
}


const String& Experiment::getRootDir( void ) const {
    return myRootDir;
}


const j2ms::scaninfolist_t& Experiment::getScanlist( void ) const {
    static const j2ms::scaninfolist_t  empty_scan_list;
    return empty_scan_list;
}


void Experiment::logLogMessage( const String& msg, const LogOrigin& org ) const {
    if( !(*myLogStreamptr) ) {
        //  Create a suitable logfile....
        ostringstream strm;
	
        strm << myRootDir << "/" << this->getMSname() << ".log";

        (*myLogStreamptr) = new fstream(strm.str().c_str(), ios::app);
    }
  
    *(*myLogStreamptr) << LogMessage(msg, org) << flush;
    return;
}


Experiment::~Experiment() {
    // close the log-file
    delete (*myLogStreamptr);
    delete myLogStreamptr;
}

