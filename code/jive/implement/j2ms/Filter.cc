// implementation of the Filter-base class
//
// Author: H. Verkouter, 12-06-2001
//
// $Id: Filter.cc,v 1.3 2006/02/10 08:53:43 verkout Exp $
//
// $Log: Filter.cc,v $
// Revision 1.3  2006/02/10 08:53:43  verkout
// HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
// Revision 1.2  2006/01/13 11:35:38  verkout
// HV: CASAfied version of the implement
//
// Revision 1.1  2001/07/09 07:52:43  verkout
// HV: Shitload of mods:
// - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
// - Enabled passing of options from j2ms2 commandline to an experiment
// - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
// - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
// - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
// - Enabled visibility filtering. At the moment only a source filter is implemented
//
//
#include <jive/j2ms/Filter.h>
#include <jive/Utilities/jexcept.h>

using namespace casa;

Filter::Filter( const String& id ) :
	myId( id )
{
	if( myId.size()==0 )
	{
		THROW_JEXCEPT("Filter::Filter() - No Filter-id given!");
	}
}


const String& Filter::id( void ) const
{
	return myId;
}


//
// Nothing to delete (yet)
//
Filter::~Filter()
{
}
