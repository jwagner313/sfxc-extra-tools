//  Implementation of the VEXSourceDB - class
//
//  Author: Harro Verkouter,   6-7-1999
//
//  $Id: VEXSourceDB.cc,v 1.10 2007/02/26 15:04:36 jive_cc Exp $
//
//  $Log: VEXSourceDB.cc,v $
//  Revision 1.10  2007/02/26 15:04:36  jive_cc
//  HV: - enable lookup via Id
//      - cosmetic changes
//      - throws if something fishy as fishyness at this level
//        is unacceptable
//
//  Revision 1.9  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.8  2006/02/10 08:53:44  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.7  2006/01/13 11:35:40  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.6  2004/08/25 05:58:06  verkout
//  HV: * Got rid of 'uint16' (replaced with 'unsigned int').
//        (not everywhere yet)
//      * Started using CountedPointers to DataFileRecords in
//        COF* classes (such that datafilerecords, read from file
//        get automagically deleted when nobody refers to them
//        anymore
//      * Added some exception catching here and there...
//
//  Revision 1.5  2004/01/05 15:02:20  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.4  2003/09/12 07:27:37  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.3  2003/02/14 15:41:53  verkout
//  HV: vex++ now uses std::string. Have to change to using std::string as well.
//
//  Revision 1.2  2001/05/30 11:48:22  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//  HV: imported aips++ implement stuff
//
//
#include <jive/j2ms/VEXSourceDB.h>
#include <jive/Utilities/jexcept.h>
#include <prep_job/vexplus.h>

#include <iostream>
#include <iomanip>

using namespace std;
using namespace casa;

// Read source info from vexfile. Throw a jexcept exception
// if unrecoverable error occurs. Some info may be defaulted,
// usr will be warned if this occurs
VEXSourceDB::VEXSourceDB( const String& vexfilename ) :
    SourceDB()
{
    VexPlus     vexfile( vexfilename );
   
    if( vexfile.parseVex()<0 )
        THROW_JEXCEPT("Failed to read/parse VEX-file " << vexfilename);

    String            srcname;
    String            srccode;
    String            epochstr;
    MDirection        srcdir;
    MVDirection       srcvdir;
    const String      defepoch( "J2000" );
    MDirection::Types epoch;

    for( int i=0; i<vexfile.N_Sources(); i++ ) {
        srcname  = vexfile.SourceName( i );
        srccode  = vexfile.IAUName( i );
        epochstr = vexfile.RefEpoch( i );

        if( srcname.empty() )
            THROW_JEXCEPT("Unable to get source name for source #" << i);
        
        // this is a mere warning
        if( epochstr.empty() ) {
            epochstr = defepoch;
            cerr << vexfilename << ": Missing epoch for source #" << i << "; defaulting "
                 << "to " << epochstr << endl;
        }
        //  SOURCE-code is optional; defaults to sourcename
        if( srccode.empty() )
            srccode = srcname;

        if( !(MDirection::getType(epoch, epochstr)) ) 
            THROW_JEXCEPT("Unrecognized epoch '" << epochstr << "'");
	    
        srcvdir = MVDirection( vexfile.Source_RA(i), vexfile.Source_Dec(i) );
        srcdir  = MDirection( srcvdir, epoch );

        // and attempt to stick in the mapping
        SourceInfo                        src(i, srcname, srccode, srcdir);
        pair<idmap_t::iterator,bool>      insres2;
        pair<sourcelist_t::iterator,bool> insres;

        insres = mySourceList.insert( make_pair(srcname, src) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to insert sourceinfo for '" << srcname << "'");

        insres2 = myIdMap.insert( make_pair(src.getId(), insres.first) );
        if( !insres2.second )
            THROW_JEXCEPT("Failed to insert Id-to-source entry into mapping");
    }
    //  Done!
}

//  Look up stuff!
SourceInfo VEXSourceDB::operator[]( const String& key ) const {
    SourceInfo                   retval;
    sourcelist_t::const_iterator cursrc;
    
    if( (cursrc=mySourceList.find(key))!=mySourceList.end() )
	    retval = cursrc->second;
    return retval;
}

bool VEXSourceDB::searchSource( SourceInfo& siref, const String& key ) const {
    sourcelist_t::const_iterator cursrc;
    
    if( (cursrc=mySourceList.find(key))!=mySourceList.end() )
	    siref = cursrc->second;
    return (cursrc!=mySourceList.end());
}

bool VEXSourceDB::searchSource( SourceInfo& siref, Int id ) const {
    idmap_t::const_iterator curid;
    
    if( (curid=myIdMap.find(id))!=myIdMap.end() )
	    siref = (curid->second)->second;
    return (curid!=myIdMap.end());
}

size_t VEXSourceDB::nrSources( void ) const {
    return mySourceList.size();
}

//  Clean up...
VEXSourceDB::~VEXSourceDB()
{}
