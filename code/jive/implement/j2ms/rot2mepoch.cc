#if 0
// actual implementation
//
// H Verkouter
// 11/08/2006
//
#include <jive/j2ms/rot2mepoch.h>
#include <jive/j2ms/DayConversion.h>
#include <casa/Quanta/MVTime.h>

using namespace casa;



MEpoch rot2mepoch( const FFROT& ffrot ) {
    //  Prevent these from being created over and over again...
    static MEpoch        retval;
    static MVTime        mvtime;
    static MVEpoch       mvepoch;
    static DayConversion dayConversion;

    // Okiedokie, do the dirty work...
    int          year( 0 );
    Double       remaininsysclks;
    Double       sysclksperday( (Double)SYSCLK_FREQ*(Double)DayConversion::secondsPerDay );
    Double       daywithfraction;  // daywithfraction = <day in month>.<fractional day>
    unsigned int month, day;       // month 0..11, day=1..31
    unsigned int nrwholedays;
    
    
    //  Ok. The short-form rot is the time in SYSCLKS since the start
    //  of the YEAR.... so work out the daynr from here...
    nrwholedays     = (unsigned int)((ffrot.get_sysclks())/sysclksperday);
    remaininsysclks = (ffrot.get_sysclks()) - (Double)nrwholedays*sysclksperday;

    //  Base date is the year???
    if( ffrot.get_base_date()!=-1 ) {
		year = ffrot.get_base_date();
    }

    // for MVTime we must know month + day in month
    // Iff dayNrToMonthDay() is succesfull, it fills in
    // 'month' and 'day' with the appropriate values so
    // in that case we only need to work out the
    // 'daywithfraction'
    if( DayConversion::dayNrToMonthDay(month, day, nrwholedays+1, year) ) {
		daywithfraction = (Double)day + remaininsysclks/sysclksperday;
    } else {
        day             = 0;
		month           = 0;
		daywithfraction = 0.0;
    }
    // so now we can form the time in UTC
    mvtime  = MVTime( year, month+1, daywithfraction );
    mvepoch = MVEpoch( mvtime.day() );
    retval  = MEpoch( mvepoch, MEpoch::UTC );

    // and return a copy of the result
    return retval;
}

// for SFROT
MEpoch rot2mepoch(const SFROT& rot, int year ) {
    return rot2mepoch( FFROT(rot, year) );
}
#endif
