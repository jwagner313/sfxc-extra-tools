// implementation
//
// $Id: GhostBusterFilter.cc,v 1.2 2006/03/02 14:21:39 verkout Exp $
//
// $Log: GhostBusterFilter.cc,v $
// Revision 1.2  2006/03/02 14:21:39  verkout
// HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
// Revision 1.1  2006/02/14 13:30:49  verkout
// HV: Implemented a filter which removes ghostdata based on individual scan-lists (scanlist per station). If any of the participating stations in a baseline has no scan defined the visibility is discarded
//
//
#include <jive/j2ms/GhostBusterFilter.h>
#include <jive/j2ms/scanlist.h>
#include <jive/j2ms/VisibilityBuffer.h>

#include <casa/Quanta.h>

#include <algorithm>
#include <iostream>

using std::cerr;
using std::endl;

using namespace j2ms;
using namespace casa;


// Use stl's algo stuff to facilitate finding stuff
// in containers...

struct _time_finder {

    // note: even though this *is* a struct I'll make 
    // an explicit public section to indicate that this
    // thing also has a *private* section...
    public:
        // look for a given time
        _time_finder( const MVEpoch& t ) :
            thetime( t )
        {
        }
    
        // check if the searched time is whithin the range
        // [ssi.scanStart , ssi.scanEnd)
        bool operator()( const stationscaninfo_t& ssi ) const
        {
            double  tt( thetime.get() );
            return (tt>=ssi.scanStart.get() && tt<ssi.scanEnd.get());
/* Should be: */
//          return (thetime>=ssi.scanStart && thetime<ssi.scanEnd);
        }

    private:
        MVEpoch   thetime;

        // *no* default c'tor
        _time_finder();
};

// a nifty 'define' to make things readable...
// find a scan in container 'a' by time 'b'
#define find_scan_by_time(a,b) \
    std::find_if<>(a.begin(), a.end(), _time_finder(b))


GhostBusterFilter::GhostBusterFilter() :
    Filter( "stationscan" )
{
}

GhostBusterFilter::GhostBusterFilter( const casa::String& ) :
    Filter( "stationscan" )
{
}

//
// Extract the X- and Y- station of the baseline and the
// time of the visibility
// and verify that *both* have a valid scan definition
// for that time
casa::Bool GhostBusterFilter::letPass( const Visibility& vis )
{
    // if the vis. has NO visibilitybuffer, we cannot continue
    // since we can't get the station-based scanlist (that should
    // be available from the visbuffer). In this case we will
    // NOT let the visibility pass!
    if( !vis.hasVisibilityBuffer() )
    {
        return False;
    }

    // ok get sum refs
    const MEpoch&            vtime( vis.getTime() );
    const BaseLineIdx&       bl( vis.getLabel().baseline );
    const VisibilityBuffer&  vbref( vis.getVisibilityBuffer() );

    if( !bl.is_sane() )
    {
        cerr << "GhostBusterFilter: Visibility baselineidx is NOT sane?\n"
             << "  " << vis.getLabel() << endl;
        return False;
    }

    // Now extract the X/Y antennas and the MVEpoch
    MVEpoch         t;
    unsigned int    x,y;

    x = bl[0];
    y = bl[1];
    t = vtime.getValue();

    // now (try) to find scan(s) in the stationbased scanlist
    const stationscanlist_t&          xlist( vbref.getStationScanlist((int)x) );
    stationscanlist_t::const_iterator xscan;

    xscan = find_scan_by_time(xlist, t);

    // only *really* look for yscan (and test) if not X and Y equal
    if( y!=x )
    {
        const stationscanlist_t&          ylist( vbref.getStationScanlist((int)y) );
        stationscanlist_t::const_iterator yscan;
        
        yscan = find_scan_by_time(ylist, t);

        return (xscan!=xlist.end() && yscan!=ylist.end());
    }
    return (xscan!=xlist.end());
}

GhostBusterFilter::~GhostBusterFilter()
{
}
