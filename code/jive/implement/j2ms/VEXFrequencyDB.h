//
//  VEXFrequencyDB - Specialization of the FrequencyDB class, reading
//  frequency configurations from a VEX-file....
//
//  Author:  Harro Verkouter,  6-7-1999
//
//  $Id: VEXFrequencyDB.h,v 1.6 2007/02/26 15:02:18 jive_cc Exp $
//
//  $Log: VEXFrequencyDB.h,v $
//  Revision 1.6  2007/02/26 15:02:18  jive_cc
//  HV: - adher to new interface functions
//      - cosmetic changes
//      - throw if something's fishy since fishyness at
//        this level is unacceptable
//
//  Revision 1.5  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.4  2006/01/13 11:35:40  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.3  2004/01/05 15:02:18  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.2  2001/05/30 11:48:20  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//  HV: imported aips++ implement stuff
//
//
#ifndef VEXFREQUENCYDB_H
#define VEXFREQUENCYDB_H

#include <casa/aips.h>
#include <jive/j2ms/FrequencyDB.h>
#include <casa/BasicSL/String.h>
#include <jive/j2ms/FrequencyConfig.h>

#include <vector>


class VEXFrequencyDB :
    public FrequencyDB
{
public:
    //  Create the database from the vexfile!
    VEXFrequencyDB( const casa::String& vexfilename );

    //  Provide these functions

    // lookup by frequency-code
    virtual FrequencyConfig  operator[]( const casa::String& key ) const;
    virtual casa::Bool       searchFrequencyConfig( FrequencyConfig& fcref,
                                                    const casa::String& key ) const;
    // lookup by freq-group-number
    virtual casa::Bool       searchFrequencyConfig( FrequencyConfig& fcref,
                                                    casa::Int key ) const;
    //  Can access them by index 0..(nEntries()-1)
    //  Note: frequency-group-number for entry #n may or may not be equal to 'n'.
    //  Don't count on it.
    virtual casa::uInt       nEntries( void ) const;
    virtual FrequencyConfig  operator[]( casa::uInt idx) const;

    // the recorded polarizations
    const j2ms::polarizations_t& recordedPolarizations( void ) const;

    //  Delete the resources for this database
    virtual ~VEXFrequencyDB();

private:
    //  The private parts
    //  Stick the frequency configs in a vector
    //  (we must be able to adress the n-th entry...
    //  and map<> destroys the order we insert'm in)
    typedef std::vector<FrequencyConfig>  configurations_t;

    casa::String          myVexFileName;
    configurations_t      myConfigurations;
    j2ms::polarizations_t myRecordedPolarizations;
    
    //  Prohibit these
    VEXFrequencyDB();
    VEXFrequencyDB( const VEXFrequencyDB& );
    const VEXFrequencyDB& operator=( const VEXFrequencyDB& );
};


#endif
