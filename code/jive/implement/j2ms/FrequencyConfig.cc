//
//  Implementation of the frequency config class
//
//  Author:   Harro Verkouter,   2-12-1998
//
//  $Id: FrequencyConfig.cc,v 1.8 2007/02/26 13:01:47 jive_cc Exp $
//
#include <jive/j2ms/FrequencyConfig.h>
#include <jive/Utilities/jexcept.h>

#include <iostream>
#include <ctype.h>

using namespace std;
using namespace casa;

ostream& operator<<( ostream& os, const FrequencyConfig& fcref ) {
    basebandlist_t::const_iterator curbb;

    os << "FrequencyConfig: `" << fcref.itsName << "' (#"
       << fcref.getFreqGroupNr() << ")"
       << " recorded " << fcref.itsRecordedPols << endl;

    for( curbb=fcref.itsBaseBands.begin();
         curbb!=fcref.itsBaseBands.end();
         curbb++ ) {
		os << "     [" << *curbb << "]" << endl;
    }
    return os;
}

FrequencyConfig::FrequencyConfig( const String& name, Int fqgrpnr ) :
    itsGroupNr( fqgrpnr ),
    itsName( name )
{
    if( name.size()==0 )
        THROW_JEXCEPT("Empty frequencyconfig name!");
}

FrequencyConfig::FrequencyConfig( const FrequencyConfig& other ) :
    itsGroupNr( other.itsGroupNr ),
    itsName( other.itsName ),
    itsBaseBands( other.itsBaseBands ),
    itsRecordedPols( other.itsRecordedPols )
{}

const FrequencyConfig& FrequencyConfig::operator=( const FrequencyConfig& other ) {
    if( this!=&other ) {
        itsGroupNr      = other.itsGroupNr;
    	itsName         = other.itsName;
    	itsBaseBands    = other.itsBaseBands;
        itsRecordedPols = other.itsRecordedPols;
    }
    return *this;
}

bool FrequencyConfig::addBaseBand( const BaseBand& newbb ) {
    // push back a copy of a (tmp) baseband created from
    // baseband data in 'newbb' and bookkeeping members
    // initialized with values appropropriate for *this*
    // object
    itsBaseBands.push_back( BaseBand(this, itsBaseBands.size(), newbb) );
    return true;
}

bool FrequencyConfig::setRecordedPols( const j2ms::polarizations_t& pmref ) {
    bool     alreadyset( itsRecordedPols.size()!=0 );

    if( !alreadyset ) 
        itsRecordedPols = pmref;
    return !alreadyset;
}

const j2ms::polarizations_t& FrequencyConfig::getRecordedPols( void ) const {
    return itsRecordedPols;
}

BaseBand* FrequencyConfig::getBaseBand( uInt bbnr ) {
    BaseBand*   retval( 0 );
    
    if( bbnr<itsBaseBands.size() ) 
		retval = &itsBaseBands[bbnr];
    return retval;
}

const BaseBand* FrequencyConfig::getBaseBand( uInt bbnr ) const {
    const BaseBand*   retval( 0 );
    
    if( bbnr<itsBaseBands.size() ) 
    	retval = &itsBaseBands[bbnr];
    return retval;
}

const basebandlist_t& FrequencyConfig::getBaseBands( void ) const {
    return itsBaseBands;
}

unsigned int FrequencyConfig::getNrBaseBands( void ) const {
    return itsBaseBands.size();
}


unsigned int FrequencyConfig::getNrPolarizations( void ) const {
    return itsRecordedPols.size();
}


const String& FrequencyConfig::getCode( void ) const {
    return itsName;
}
   
Int FrequencyConfig::getFreqGroupNr( void ) const {
    return itsGroupNr;
}

FrequencyConfig::~FrequencyConfig()
{}

