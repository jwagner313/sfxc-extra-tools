// function to read all scans from a vexfile
//
// $Id: scanlist.h,v 1.2 2007/02/26 15:56:48 jive_cc Exp $
//
// Author: H Verkouter
//
#ifndef J2MS_SCANLIST_H
#define J2MS_SCANLIST_H

#include <list>
#include <map>
#include <string>
#include <jive/j2ms/Scan.h>
//#include <jive/j2ms/CorrelatorSetup.h>
#include <casa/Quanta/MVEpoch.h>
#include <casa/Quanta/Quantum.h>

// stick the typedef in a namespace
namespace j2ms {
    typedef std::list<ScanInfo>  scaninfolist_t;

    // (small) struct to keep track of station-based
    // scaninfo. For now we only need the station-based
    // start/stop times such that we can easily check
    // if a station was scheduled at some instance of time
    // or not
    struct stationscaninfo_t {
        casa::MVEpoch   scanStart;
        casa::MVEpoch   scanEnd;

        // sets scanStart/scanEnd to '0'
        stationscaninfo_t();

        // create from 'start' and 'end'
        stationscaninfo_t( const casa::MVEpoch& s, const casa::MVEpoch& e );
        // create from 'start' + 'duration'
        stationscaninfo_t( const casa::MVEpoch& s, const casa::Quantity& d );
    };

    // define a stationscanlist
    typedef std::list<stationscaninfo_t>      stationscanlist_t;

    // and all the scans per station grouped in a map
    // Key = station number, Value = stationscanlist_t
    typedef std::map<int, stationscanlist_t>  station2listmap_t;
}

// If something's fishy (ie: cannot be read from the VEXfile,
//  a j2except exception [derived of std::exception] will be thrown)
j2ms::scaninfolist_t scanlist( const std::string& vexfile,
                               /*const CorrelatorSetup& correlatorsetup,*/
                               bool verbose = false );

// read the stationbased scanlist for a given station
// Note: if _no_ scans are present for a station, this will NOT
// be treated as an error. 
// Anything else (failure to access info from the vexfile, for instance)
// warrants throwing of an exception (same type as thrown from 'scanlist()'))
j2ms::station2listmap_t stationscanmap( const std::string& vexfile, bool verbose = false );

#endif
