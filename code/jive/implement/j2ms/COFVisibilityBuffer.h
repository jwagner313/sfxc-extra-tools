//  COF VisibilityBuffer - Correlator OutputFormat visibility buffer
//
//  Read visibilities from a file set known as the JIVE Correlator
//  Output Format.
//
//  This is a specialization of the VisibilityBuffer class
//
//
//  Author:   Harro Verkouter, 31-05-1999
//
//
//  $Id: COFVisibilityBuffer.h,v 1.16 2011/03/02 16:09:58 jive_cc Exp $
#ifndef COFVISIBILITYBUFFER_H
#define COFVISIBILITYBUFFER_H

//  Bool, Double etc...
#include <casa/aips.h>
#include <casa/BasicSL/String.h>

//  This is our base-class so we need it
#include <jive/j2ms/VisibilityBuffer.h>

//  We keep a baseline set and a baseline iterator since we iterate
//  through the set baseline by baseline
#include <jive/j2ms/COFBaselineIterator.h>
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
#include <casa/Quanta/MVEpoch.h>
#include <casa/BasicSL/Complex.h>
#include <measures/Measures/MEpoch.h>


#include <data_handler/DataFile.h>
#include <data_handler/DataDescriptor.h>
#include <data_handler/CorrelatedDataRecord.h>
#include <data_handler/MappingEntry.h>
#include <data_handler/CountedPointer.h>

#include <jive/j2ms/Scan.h>
#include <jive/j2ms/scanlist.h>
#include <jive/j2ms/CorrelatorSetup.h>


#include <vector>
#include <string>
#include <map>

// Struct mfcSetup is filled in by parsing an MFC-setup file (if the data was
// correlated with MFC's and the user of j2ms2 indicates in the the experiment
// option).
// 
// In this MFC setup it will tell us which sources were 'aliased' (i.e. were
// correlated with different phase-centers). If we find a scan with name 'orgname', we
// will silently insert <num-phase-center> scans with the right sources, forgetting
// the original scan....
//
// Also, it should tell us how the logical channels were replicated at the SU-output,
// since from Albert we only get the ID of the physical output. Normally we use just
// the mappingsfile to go from physical output to logical channel (in order to find
// out the SUBBAND of the datum). Now we have an additional mapping because the data
// is 'multiplexed': datastreams with the same 'physical' labels
// (XANT/YANT/XPOL/YPOL/SUBBAND) are duplicated into different Albert-labels
// (=XSIGINP/YSIGINP). We need to be able to tell from the (X|Y)SIGINP labels what the
// real labels of the stream were.
//
// Further, we must know which 'scan' to take. What happens is that each scan will
// have <num-phase-center> datums per interferometer/time pair. We need to be able to
// label the streams with the correct phase-center. Thats where the physToFieldCenter
// map comes in: it tell us (sort of) which polynomials were sent to which SU-output.
// So by backtracing this mapping, we can find out from the output what the real
// coordinates of the phase-center were!
typedef std::map<std::string, std::vector<std::string> > alias_type;

struct mfcSetup {
	// Default c'tor. Will initialize the stuff such that the mappings do not take any
	// effect. The source-aliases are empty. 
	// This has the effect that we can *always* use an mfcSetup object.
	//
	// Either it is a default object (nothing happens) OR
	// it is overwritten by stuff read from an MFC setup file.... and then mappings
	// come into play!
	mfcSetup();

	// maps physical output of SU to logical vex-channel
	// Assumption: physical output is the arrayindex into this vector
	//
	// We use the logical vex-channel to find out what the 'real' subband nr of this
	// datum is
	std::vector<int>    physToLogMap;
	
	// For each physical output of the SU, tell us which 'aliased' source (hence
	// field-center) this output contained data for (which field).
	std::vector<int>    physToFieldCenterMap;

	// Map from sourcename to list of aliases
	alias_type          srcToAliasMap;
};

//  Forward declarations
namespace j2ms
{
	class COFScan;
};
class VEXperiment;
class MappingsFile;
class CorrelatedDataFile;

// this thing will be thrown if the VisBuffer is NOT
// COF!
struct job_is_not_COF {};


class COFVisibilityBuffer :
    public VisibilityBuffer
{
public:
	// Static methods that construct default mappings for a MFC setup that has no
	// effect
	static std::vector<int>          defaultPhysToLogMap( void );	
	static std::vector<int>          defaultPhysToSrcMap( void );	

    //  Create a COF visibility buffer
    //  
    //  Note: 'vbopt' is short for "visibility buffer option(s)"
    //  it enables us to pass visibilitybuffer specific
    //  options
    COFVisibilityBuffer( unsigned int jobid, unsigned int subjobid,
			             const VEXperiment* experimentptr=0,
                         const std::string& vbopt = std::string() );
    
    //  Get the current visibility from the buffer
    virtual casa::Bool          getCurrentVisibility( Visibility& vref );
    
    // Advance to next visibility
    virtual casa::Bool          next( void );

    //  What's the directory of the visibilitybuffer??
    virtual const casa::String& getRootDir( void ) const;

	// Return the visbuffer-Id() (=jobid!)
	virtual casa::Int           getVisBufId( void ) const;

	virtual casa::String        getProcessorSubType( void ) const;

    // Return a copy of the correlator-setup
    virtual CorrelatorSetup     correlatorSetup( void ) const;

    // different scanlists:
    // those per station and the one for the whole job
    virtual const j2ms::stationscanlist_t&
                                getStationScanlist( int station ) const;

    virtual const j2ms::scaninfolist_t&
                                getScanlist( void ) const;

    // provide correlator models for this job, if any.
    virtual const j2ms::modeldata_t&
                                getModelData( void ) const;

    // return what we did (non standard/optional) to the data
    virtual processing_steps    getProcessingSteps( void ) const;
    
    //  These are COFVisibilityBuffer specific
    unsigned int                getJobID( void ) const;
    unsigned int                getSubJobID( void ) const;
    
    //  Delete all allocated resources
    virtual ~COFVisibilityBuffer();
    
private:
    // Keep a vex-to-subband mapping for a frequency-config
    typedef std::map<unsigned int, unsigned int> vex2sb_type;
    typedef std::map<std::string, vex2sb_type>   fq2vexmap_type;

    // List of pointers to scans. Memory management is left up to user
    // [you keep track wether or not you 0wn the pointer :)]
    typedef std::vector<j2ms::COFScan*>  scanptrlist_type;

    // Whenever the mapping changes, we have to re-initialize this
    // one. For every baseline (XAnt,YAnt,Subband combination) 
    // it's verified that the unmapped values for the participating
    // interferometers unmap to the same logical values.
    // (unmapped using the Mapping data record).
    // Contains the unmapped logical SU-channel and antenna-ids.
    // Note: the su-channels will have to be unmapped to vexchannel
    // using the phys-to-log mapping [not always there's a 1:1
    // mapping between su-channel and vex channel..] and 
    // to possibly another source.
    // Note: currently there is no support for per antenna
    // physical-SU-channel to logical-channel mappings AND there
    // is only one logical-su-channel-to-vex-channel mapping
    // (per frequency setup), the unmapped x-su-channel and y-su-channel
    // must be identical in order to give the same "subband-index".
    // Same argument holds for the source.
    struct unmapped_type {
        unsigned int   XAnt, YAnt;
        unsigned int   Subband;
        unsigned int   Source;

        unmapped_type(unsigned int xant, unsigned int yant, unsigned int sb, unsigned int src);
        private:
            // prohibit use of default c'tor ;)
            // So this struct can only be used as (eg)
            // mapping.insert( make_pair(k, unmapped_type(x,y,s,src)) );
            // guaranteeing proper initialization!
            unmapped_type();
    };
    // We keep a mapping of blkey_type's to
    // unmapped properties - read above
    typedef std::map<blkey_type,unmapped_type>  unmap_type;

    // Caching of values that only change when we read a new integration
    struct intgcache_type {
        casa::MEpoch          visibilitytime;
        casa::MEpoch          visstart;
        casa::MEpoch          visend;
        casa::MVEpoch         mvepoch;
        casa::MVEpoch         mvstart;
        casa::MVEpoch         mvend;
        SFROT                 startROT;
        SFROT                 endROT;
        // set when delta-T between start/end ROT > 1.2 times integration time
        // set in the correlator setup
        bool                  inttimflag;

        // short-list of scans that apply to the given time.
        // In MFC cases we have >1 scan at one instance of time
        // only with different source-coordinates.
        // Assume they are in here in the alias-order
        // [such that if we deduce from the MFC setup
        // that the current visibility is from the 
        // n-th source, the accompanying scan is
        // the n-the in the shortlist
        scanptrlist_type       shortlist;

        // Keep track of the vex2sb mapping.
        // This assumes that all scans in the shortlist
        // share the same frequency config.
        fq2vexmap_type::iterator  v2sptr;
    };

    //  Our private parts
    unsigned int          myJobID;
    unsigned int          mySubJobID;
    casa::String          myRootDir;
    MappingsFile*         myMappingsFileptr;
	CorrelatorSetup       mySetup;
    CorrelatedDataFile*   myCorrelatedDataFileptr;

    // Pointers to current info
	JCCS::CountedPointer<CorrelatedDataRecord> myCurrentCorrelatedDataRecordptr;
	JCCS::CountedPointer<DataDescriptor>       myCurrentDataDescriptorptr;
	JCCS::CountedPointer<MappingEntry>         myCurrentMappingptr;

    //  Iteration stuff. The current baseline set and the iterator
    //  that iterates over the set.
    COFBaselineSet        myBaselineSet;
    COFBaselineIterator   myBaselineIterator;

    //  The list of scans found in the vexfile.
    //  Scans processed with MultipleFieldCenters
    //  will have more scan-definitions because each original
    //  scan will be expanded to a number of (distinguishable) scans
    //  with all properties identical to the "original" scan but
    //  the source-position and the 'scanId'.
    scanptrlist_type                  myScanPtrs;
    j2ms::scaninfolist_t              myScanInfoList;

	// Just have an object of this type. By default, the mappings found in this object
	// are initialised such that they (should) have no effect. In the case of MFC
	// data, the mappings do change... (this setup is filled in from parsing an
	// mfc-file) = EXTERNALCONFIG in the JCCS system
	mfcSetup                          myMFCSetup;
    intgcache_type                    intgcache;
    fq2vexmap_type                    fq2vex;
    unmap_type                        unmap;

    // The mapping of station-id to stationbased info
    j2ms::station2listmap_t           station2listmap;

    // And modeldata. Make this mutable such that it will
    // only be filled in/cached when the user calls 'getModelData()'
    mutable j2ms::modeldata_t   modeldata;

    
    //  Get data for the requested polarization products ("cs")
    //  in the baseline
    const casa::Matrix<casa::Complex>& getBaselineData( casa::Vector<casa::Float>& weight,
                                                        const COFBaseline& cbref,
                                                        bool swap_due_to_vex,
                                                        const correlationset_t& cs ) const;

    //  Static method -> transfrom a FFROT (presumably DataDescriptor.getStartROT()) and
    //  two SFROTs (start/end SFROT) into an MEpoch-measure
    //  Need the FFROT for the base_date, ie. the *year* [SFROT's are wrt to 
    //  start-of-year...]
    static const casa::MEpoch&   calcMidPointAsMEpoch( const FFROT& ffrot,
                                                       const SFROT& startrot,
                                                       const SFROT& endrot );

	// Parse the file, assuming it is a MFC-setup file
	static bool                 readMFCSetup( mfcSetup& result,
                                              const casa::String& mfcfile );
	

    //  Prohibit these
    COFVisibilityBuffer();
    COFVisibilityBuffer( const COFVisibilityBuffer& );
    const COFVisibilityBuffer& operator=( const COFVisibilityBuffer& );
};



#endif
