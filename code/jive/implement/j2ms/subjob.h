// subjob specification for translating databuffers
//
// $Id: subjob.h,v 1.2 2006/01/13 11:35:41 verkout Exp $
//
// $Log: subjob.h,v $
// Revision 1.2  2006/01/13 11:35:41  verkout
// HV: CASAfied version of the implement
//
//
#ifndef J2MS_SUBJOB_H
#define J2MS_SUBJOB_H

#include <string>

// support for first->last scan selection
// empty implies 'discard'
typedef struct _subjob_t {
	std::string   jobName;
	std::string   firstScan;
	std::string   lastScan;

	// backward compatibility mode: f/l scan not filled in
	_subjob_t( const std::string& job );

	// f/l scan defined (new default runmode?)
	_subjob_t( const std::string& job, const std::string& f, const std::string& l );


	// return first scan nr (if firstScan==empty
	// return INT_MIN such that a comparison like
	// currentScan->scanNumber()>=cursubjob->firstScanNo()
	// always evaluates to true...
	int firstScanNo( void ) const;

	// return last scan nr. if last scan not set
	// (ie empty) return INT_MAX such that
	//  currentScan->scanNumber()<=cursubjob->lastScanNo()
	//  is always true
	int lastScanNo( void ) const;
} subjob_t;
#endif
