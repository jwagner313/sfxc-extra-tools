//  AntennaDB -> Abstract base-class [interface] for classes that whish to act as
//  an AntennaDatabase 
//
//  Must be able to deliver the requested info.
//
//
//  Author:   Harro Verkouter, 1-12-1998
//
//
//   $Id: AntennaDB.h,v 1.4 2007/02/26 10:37:37 jive_cc Exp $
//
#ifndef JIVE_J2MS_ANTENNADB_H
#define JIVE_J2MS_ANTENNADB_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <jive/j2ms/AntennaInfo.h>


class AntennaDB
{
public:
    //  Create a database object...
    AntennaDB();

    //  Do a lookup based on code
    virtual AntennaInfo operator[]( const casa::String& key ) const = 0;

    // search in fill in if antenna is found. Should return wether or not 
    // the 'airef' was filled in, ie, if the antenna with name 'key' was found
    virtual casa::Bool searchAntenna( AntennaInfo& airef,
                                      const casa::String& key ) const = 0;

    // we must also be able to find Antenna's keyed on ID
    //  [note: for the moment (18/01/2007) decided not to overload operator[] with
    //   a "operator[](casa::Int id)" version since the [] would suggest "give me
    //   the id'th antenna from your list, whereas the "searchAntenna(.., casa::Int id)"
    //   will return the antenna with (you guessed it) .getAntennaId()==id. The two
    //   indexing schemes need not be identical]
    virtual casa::Bool searchAntenna( AntennaInfo& airef, casa::Int id ) const = 0;

    // How many antenna's in the DB?
    virtual size_t     nrAntennas( void ) const = 0;

    //  Delete the resources this database used....
    virtual ~AntennaDB();

private:
    //  Let's prohibit these, it's not wise to copy databases, or assign
    //  to them
    AntennaDB( const AntennaDB& );
    const AntennaDB& operator=( const AntennaDB& );
};



#endif
