//  Visibility - a class that describes a visibility: it holds a label
//  (a VisLabel) that holds the logical indices telling us the origin
//  of the visibility, PLUS a matrix holding the visibility data (the
//  shape of the matrix is nr. correlations. times nr of spectral
//  channels)
//
//
//  Author:   Harro Verkouter, 18-11-1998
//
//
//  $Id: Visibility.h,v 1.6 2011/10/12 12:45:31 jive_cc Exp $
//
#ifndef VISIBILITY_H
#define VISIBILITY_H


#include <jive/labels/VisLabel.h>
#include <jive/j2ms/noUVW.h>

#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
#include <measures/Measures.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/Muvw.h>

#include <iostream>


//  Forward declarations
class VisibilityBuffer;


class Visibility
{
public:
    //  We can tell in what domain the visibility data is:
    typedef enum _domain {
	    undefined, frequency, time
    } domain;
    
    //  An empty visibility
    //  UVW will be set to JIVEMSFiller::noUVW
    Visibility();

    //  A defined visibility.
    //
    //  NOTE: a visibility MAY have a buffer attached, bust is not
    //  obliged to have one!
    //
    //  NOTE: If the UVW is omitted, it will be initialized from
    //  JIVEMSFiller::noUVW
    Visibility( const ExtVisLabel& label,
			    const casa::Matrix<casa::Complex>& data,
				Visibility::domain datadomain=Visibility::undefined,
				casa::Bool flag=casa::False,
				const casa::Vector<casa::Float>& weight=casa::Vector<casa::Float>(),
				const casa::MEpoch& time=casa::MEpoch(),
				const casa::Muvw& uvw=j2ms::noMuvw,
				const VisibilityBuffer* buffer=0 );
    
    //  Copy & assignement
    Visibility( const Visibility& other );
    const Visibility& operator=( const Visibility& other );

    //  Get the label/data
    const ExtVisLabel&                 getLabel( void ) const;
    const casa::Matrix<casa::Complex>& getData( void ) const;

    //  Is this an autocorrelation?
    casa::Bool                         isAutoCorrelation( void ) const;

    Visibility::domain                 getDatadomain( void ) const;

    //  Get the weight for the whole visibility and if the visibility
    //  was flagged.
    //
    //  Defaults are:
    //
    //  weight = 1.0
    //  flag   = false (i.e. not flagged)
    casa::Bool                          getFlag( void ) const;
    const casa::Vector<casa::Float>&    getWeight( void ) const;

    //  Return the time of the visibility
    const casa::MEpoch&                 getTime( void ) const;
    
    //  And an UVW-value
    const casa::Muvw&                   getUVW( void ) const;
    
    //  Tells us if we have a buffer where we came from + access to it
    casa::Bool                          hasVisibilityBuffer( void ) const;
    const VisibilityBuffer&             getVisibilityBuffer( void ) const;

    //
    //  Clear all resources
    //
    ~Visibility();
    
private:
    //
    //  The visibilities private parts
    //
    casa::Bool               itsFlag;
    casa::Muvw               itsUVW;
    casa::MEpoch             itsTime;
    ExtVisLabel        itsLabel;
    casa::Vector<casa::Float>      itsWeight;
    casa::Matrix<casa::Complex>    itsData;
    Visibility::domain itsDatadomain;
    
    //  Optional pointer to buffer where we came from...
    const VisibilityBuffer*  itsBufferptr;
    
    // Disallow these:

};

//
//  Output function!
//
std::ostream& operator<<( std::ostream& os, const Visibility& visref );
std::ostream& operator<<( std::ostream& os, const Visibility::domain& domainref );

#endif
