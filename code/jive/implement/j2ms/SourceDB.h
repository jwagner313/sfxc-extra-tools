//
//  SourceDB -> Abstract base-class for classes that whish to act as
//  a SourceDatabase server.
//
//  Must be able to deliver the requested info.
//
//
//  Author:   Harro Verkouter, 2-12-1998
//
//
//   $Id: SourceDB.h,v 1.4 2007/02/26 13:24:40 jive_cc Exp $
//
#ifndef SOURCEDB_H
#define SOURCEDB_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <jive/j2ms/SourceInfo.h>



class SourceDB
{
public:
    //  Create a database object...
    SourceDB();

    //  Derived classes _must_ implement these interface functions
    virtual SourceInfo operator[]( const casa::String& key ) const = 0;

    // find sources based on code or on id
    virtual bool       searchSource( SourceInfo& siref, const casa::String& key ) const = 0;
    virtual bool       searchSource( SourceInfo& siref, casa::Int id ) const = 0;

    virtual size_t     nrSources( void ) const = 0;
    
    //  Delete the resources this database used....
    virtual ~SourceDB();

private:
    //  Let's prohibit these, it's not wise to copy databases, assign
    //  to them
    SourceDB( const SourceDB& );
    const SourceDB& operator=( const SourceDB& );
};



#endif
