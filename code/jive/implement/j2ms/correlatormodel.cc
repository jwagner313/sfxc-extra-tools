// implementation

#include <jive/j2ms/correlatormodel.h>
#include <string.h>

namespace j2ms {

    // polypair
    polypair::polypair():
        phase( 0.0 ),
        delay( 0.0 )
    {}
    polypair::polypair( const coefficients_type& phs,
                        const coefficients_type& del ):
        phase( phs ),
        delay( del )
    {}
    
    
    // the modeldatarecord
    modeldatarecord::modeldatarecord():
        startMJDInSeconds( 0.0 ),
        durationInSeconds( 0.0 ),
        wetAtmosphere( 0.0 ),
        dryAtmosphere( 0.0 ),
        clockOffset( 0.0 )
    {}

} // end-of-namespace
