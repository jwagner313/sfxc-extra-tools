//  implementation of the COFScan class
//
//
//  Author:   Harro Verkouter,  2-6-1999
//
//   $Id: COFScan.cc,v 1.10 2012/07/05 12:23:56 jive_cc Exp $
//
//   $Log: COFScan.cc,v $
//   Revision 1.10  2012/07/05 12:23:56  jive_cc
//   kettenis: Remove #ifdef'ed out code that uses COFVisibilityBuffer and remove
//   the COFVisibilityBuffer.h include.
//
//   Revision 1.9  2007/05/25 09:32:03  jive_cc
//   HV: - cleaning up
//       - remove homegrown Regexp-wrapper and start
//        using casa::Regex
//
//   Revision 1.8  2007/02/26 11:01:36  jive_cc
//   HV: Changed output. Removed superfluous comment.
//
//   Revision 1.7  2006/02/10 08:53:43  verkout
//   HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//   Revision 1.6  2006/01/13 11:35:36  verkout
//   HV: CASAfied version of the implement
//
//   Revision 1.5  2005/03/15 14:43:09  verkout
//   HV: * include statements cleaned up
//       * More importantly: the 'getScanNo()' now returns the
//         "decoded" integer from the 'getScanId()'! (so the
//         'id' argument to the c'tor is "lost". Had to be done for
//         scan-based filtering. The 'scanId' is now taken from the
//         vex-file and the number contained therein is taken to be
//         THE scan number. Supported "scanId" formats are
//         "No[0-9]+" and "[0-9]+".
//
//   Revision 1.4  2003/09/12 07:26:34  verkout
//   HV: Code had to be made gcc3.* compliant.
//
//   Revision 1.3  2001/07/09 07:52:24  verkout
//   HV: Shitload of mods:
//   - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
//   - Enabled passing of options from j2ms2 commandline to an experiment
//   - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
//   - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
//   - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
//   - Enabled visibility filtering. At the moment only a source filter is implemented
//
//   Revision 1.2  2001/05/30 11:47:00  verkout
//   HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//   Revision 1.1.1.1  2000/03/20 15:14:35  verkout
//   HV: imported aips++ implement stuff
//
//
#include <jive/j2ms/COFScan.h>
#include <jive/j2ms/Experiment.h>
#include <jive/j2ms/Visibility.h>
#include <jive/j2ms/DayConversion.h>

#include <prep_job/vexplus.h>
#include <measures/Measures/MEpoch.h>
#include <casa/Quanta/MVTime.h>
#include <casa/Quanta.h>

#include <sstream>
#include <fstream>
#include <iomanip>
#include <sys/types.h>
#include <sys/stat.h>

using std::ostream;
using std::flush;
using namespace casa;

namespace j2ms
{

    ostream& operator<<( ostream& os, const COFScan& csref ) {
        os << "COFScan  :" << csref.getScanId() << "\n"
           << "SRC/FREQ :" << csref.getSourceCode() << "/" << csref.getFrequencyCode() << "\n"
           << flush;
        return os;
    }

    COFScan::COFScan( const VisibilityBuffer* visbufptr, const ScanInfo& si ) :
        j2ms::Scan( visbufptr ),
    	myScanNo( si.scanNo ),
        myScanID( si.scanId ),
        mySourceCode( si.sourceCode ),
        myFrequencyCode( si.frequencyCode ),
        myStartTime( MEpoch(si.scanStart, MEpoch::UTC) ),
        myEndTime( MEpoch(si.scanEnd, MEpoch::UTC) )//,
//    myCorrelatorSetup( si.correlatorSetup )
    {}

    Int COFScan::getScanNo( void ) const {
    	int          scanno( -1 );
    	const char*  formats[] = { "No%d", "%d", 0 };
    	const char** fmt;

    	for( fmt=formats; *fmt!=0; fmt++ ) 
    		if( ::sscanf(myScanID.c_str(),*fmt,&scanno)==1 )
    			break;
    	return scanno;
    }

    const String& COFScan::getScanId( void ) const {
        return myScanID;
    }

    const String& COFScan::getSourceCode( void ) const {
        return mySourceCode;
    }

    const String& COFScan::getFrequencyCode( void ) const {
        return myFrequencyCode;
    }

    const MEpoch& COFScan::getStartTime( void ) const {
        return myStartTime;
    }
    const MEpoch& COFScan::getEndTime( void ) const {
        return myEndTime;
    }

#if 0
const CorrelatorSetup& COFScan::getCorrelatorSetup( void ) const {
    return myCorrelatorSetup;
}
#endif

    //  delete allocated resources
    COFScan::~COFScan()
    {}
};
