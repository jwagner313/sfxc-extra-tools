//
//  FrequencyDB
//
//  A database interface to locate frequency setups
//
//  Author:   Harro Verkouter,   3-12-1998
//
//  $Id: FrequencyDB.h,v 1.5 2011/10/12 12:45:29 jive_cc Exp $
//
#ifndef FREQUENCYDB_H
#define FREQUENCYDB_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <jive/j2ms/FrequencyConfig.h>
#include <jive/labels/polarization.h>


class FrequencyDB
{
public:
    //  Create a database object...
    FrequencyDB();

    //  Do a lookup based on code
    virtual FrequencyConfig operator[]( const casa::String& key ) const = 0;
    virtual casa::Bool            searchFrequencyConfig( FrequencyConfig& fcref,
						                                 const casa::String& key ) const = 0;
   
    // Do a lookup based on freqgroupnumber
    virtual casa::Bool            searchFrequencyConfig( FrequencyConfig& fcref,
						                                 casa::Int fqgrp ) const = 0;
    
    //  Allow looping over the FrequencyConfigs:
    //
    //  Look up by index (ie, get the n-th frequency config.
    //  If you want the FrequencyConfig with a given freqgroupnr, use 
    //  "searchFrequencyConfig()".
    //  Note: frequency-group-number for entry #n may or may not be equal to 'n'.
    //  Don't count on it.
    virtual casa::uInt      nEntries( void ) const = 0;
    virtual FrequencyConfig operator[]( casa::uInt index ) const = 0;

    // Get the recorded polarizations
    virtual const j2ms::polarizations_t&  recordedPolarizations( void ) const = 0; 

    //  Delete the resources this database used....
    virtual ~FrequencyDB();

private:
    //  Let's prohibit these, it's not wise to copy databases, assign
    //  to them
    FrequencyDB( const FrequencyDB& );
    const FrequencyDB& operator=( const FrequencyDB& );
};

#endif
