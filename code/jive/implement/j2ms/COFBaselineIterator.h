//  COFBaselineIterator - in order to be able to iterate over the
//  baselines in a CorrelatedDataRecord (part of the COF)
//
//  Since the 'shape' or 'layout' of a correlated data record is
//  determined by the last DataDescriptor encountered in the
//  CorrelatedDataFile, we attach the COFBaselineIterator to a
//  DataDescriptor. The data for the baseline is gotten by indexing
//  into the CorrelatedDataRecord of you choice!
#ifndef COFBASELINEITERATOR_H
#define COFBASELINEITERATOR_H

//  Bool etc.
#include <casa/aips.h>

#include <jive/labels/BaseLineIdx.h>
#include <data_handler/DataDescriptor.h>
#include <jive/labels/CorrelationCode.h>
#include <data_handler/InterferometerDescriptor.h>

#include <iostream>
#include <map>
#include <list>


// define a BaselineKey, a combination
// of XAntenna, YAntenna and subband.
// The implied ordering is:
// by XAntenna, then by YAntenna, then by subband.
// This can be used for keys into maps
struct blkey_type {
    int          XAnt, YAnt;
    unsigned int Subband;

    // default: all indices set to -1 to
    // indicate invalidness
    blkey_type();

    // fully defined..
    blkey_type( int xant, int yant, unsigned int sb );
};
// define a StrictWeakOrdering for blkey's
// Could be done with a functor but then the user
// of blkey's would have to add the functor to
// the map/set's template parameters
bool operator<(const blkey_type& l, const blkey_type& r);

// and equality
bool operator==(const blkey_type& l, const blkey_type& r);


struct COFBaseline {
public:
    struct _ifinfo_t {
        _ifinfo_t( unsigned int fo,
                   const InterferometerDescriptor ifd ):
            myFileOffsetInBytes( fo ),
            myInterferometer( ifd )
        {}

        unsigned int             myFileOffsetInBytes;
        InterferometerDescriptor myInterferometer;
    };

    // keep the configured interferometers in a 
    // map<K,V> with
    // K == CorrelationCode, ie polarization combination
    // V == _ifinfo_t, the fileoffset and the interferometernr
    typedef std::map<CorrelationCode, _ifinfo_t>  intflist_t;


    //  do proper initialization
    COFBaseline( const BaseLineIdx& blidx, unsigned int subbandindex );

    //  Attempt to add a correlation to the Baseline
    //  Will be stored keyed on the correlationcode
    //  created from the ifdescr's X/Y polarization codes
    casa::Bool     addInterferometer( const InterferometerDescriptor& ifdescr );

    // Read-only access to private parts
    unsigned int        getSubbandIndex( void ) const;
    BaseLineIdx         getIndex( void ) const;

    // return the key for this thing
    const blkey_type&   getKey( void ) const;

    //  Return the correlations found for this
    //  baseline
    correlations_t      getCorrelations( void ) const;
   
    // Be able to iterate over the configured
    // interferometers
    const intflist_t&   getIntfList( void ) const;

    //  Return the indexed InterferometerDescriptor 
    //  - that is, indexed by polarization product
    InterferometerDescriptor  getInterferometer( const CorrelationCode& c ) const;
    
private:
    //  Our private parts
    blkey_type    myKey;
    intflist_t    intflist;

    //  Prohibit these
    COFBaseline();
};

// show contents of a COFBaseline thingy in HRF
std::ostream& operator<<( std::ostream& os, const COFBaseline& cbref );

// typedefs for the container that really holds 
// the COFBaselines
typedef std::list<COFBaseline>            cofbaselinelist_t;
typedef cofbaselinelist_t::const_iterator COFBaselineIterator;

//
//  Parse all the entries in the dataDescriptor to form a baseline set
//  rather than an interferometerset!
//
class COFBaselineSet
{
public:
    //  An empty baseline set
    COFBaselineSet();

    //  create from a datadescriptor
    COFBaselineSet( const DataDescriptor& ddref );
    
    //  re-init, make a new set from the give datadescriptor
    COFBaselineSet& operator=( const DataDescriptor& ddref );
   
    // is the set empty?
    bool                 empty( void ) const;

    //  return an iterator for the first/1-past-last baseline in the set!
    COFBaselineIterator  begin( void ) const;
    COFBaselineIterator  end( void ) const;

    // De-allocate resources
    ~COFBaselineSet();

private:
    //  Our private parts
    cofbaselinelist_t   cofbaselines;
    
    //  Private methods
    //
    //  This method alters the state of this object such that it
    //  represents all the baselines present in the mentiones
    //  datadescriptor
    casa::Bool  attachDataDescriptor( const DataDescriptor& ddref );

    //  Addthe pointer to our list. NOTE: the object takes over the
    //  responsibility so don't delete the pointer out from under us!
    void        addBaselineptr( COFBaseline* baselptr );

    //  Prohibit these
    COFBaselineSet( const COFBaselineSet& );
    const COFBaselineSet& operator=( const COFBaselineSet& );    
};

//
//  Dump the contents of the set on a stream (in HRF)
//
std::ostream& operator<<( std::ostream& os, const COFBaselineSet& cbsetref );


#endif
