// filters out data produced from one or more station(s) without scheduled scan
//
// $Id: GhostBusterFilter.h,v 1.1 2006/02/14 13:30:49 verkout Exp $
//
// $Log: GhostBusterFilter.h,v $
// Revision 1.1  2006/02/14 13:30:49  verkout
// HV: Implemented a filter which removes ghostdata based on individual scan-lists (scanlist per station). If any of the participating stations in a baseline has no scan defined the visibility is discarded
//
//
#ifndef J2MS_STATIONSCANFILTER_H
#define J2MS_STATIONSCANFILTER_H

#include <jive/j2ms/Filter.h>


class GhostBusterFilter:
    public Filter
{
    public:

        // this string is neglected -
        //  this filter supports no options
        GhostBusterFilter( const casa::String& );

        // and because this filter doesn't support
        // options, we might as well support a default
        // c'tor...
        GhostBusterFilter();

        virtual casa::Bool    letPass( const Visibility& vis );

        virtual ~GhostBusterFilter();

    private:
        // nothing, really...
        // apart from protecting these...
        GhostBusterFilter( const GhostBusterFilter& );
        const GhostBusterFilter& operator=( const GhostBusterFilter& );
};


#endif
