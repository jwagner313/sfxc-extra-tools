//
// SourceFilter - specialization of the Filter class that
// filters sources.....
//
// Author:  H. Verkouter, 12-06-2001
//
// $Id: SourceFilter.h,v 1.3 2006/01/13 11:35:39 verkout Exp $
//
// $Log: SourceFilter.h,v $
// Revision 1.3  2006/01/13 11:35:39  verkout
// HV: CASAfied version of the implement
//
// Revision 1.2  2003/09/12 07:27:28  verkout
// HV: Code had to be made gcc3.* compliant.
//
// Revision 1.1  2001/07/09 07:53:03  verkout
// HV: Shitload of mods:
// - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
// - Enabled passing of options from j2ms2 commandline to an experiment
// - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
// - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
// - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
// - Enabled visibility filtering. At the moment only a source filter is implemented
//
//
#ifndef SOURCEFILTER_H
#define SOURCEFILTER_H

#include <jive/j2ms/Filter.h>
#include <jive/j2ms/Experiment.h>


//
// other stuff
//
#include <string>
#include <vector>
#include <map>


//
// Class Experiment is only used in the interface and also only as a reference
// and hence we can get by by using only a forward decl!
//
//class Experiment;


class SourceFilter : 
	public Filter
{
public:

	SourceFilter( const casa::String& filteropt );

	virtual casa::Bool   letPass( const Visibility& vis );

	virtual ~SourceFilter();

private:
	//
	// A struct, representing a 'cache' element:
	// we keep an array of booleans for every experiment
	// (one bool per source-id) to indicate if that one
	// should pass or not....
	//	
	struct _cacheElem
	{
		std::string               experId;
		std::vector<bool>         pass;
		std::map<std::string,int> srcidmap;
	};
	typedef struct _cacheElem   cacheElem;

	//
	// Our private parts
	//
	std::vector<std::string>  acceptedSrces;
	std::vector<cacheElem>    myCache;

	//
	// Private methods
	//
	// HV 28-04-2003   HMMMM... GCC iterator != <pointer-to-elem>....
	//                 *drat*!
	//
	std::vector<cacheElem>::iterator  addExperimentToCache( const Experiment& expref );
//	cacheElem*         addExperimentToCache( const Experiment& expref );

	// 
	// These make no sense; prohibit them
	// 
	SourceFilter();
	SourceFilter( const SourceFilter& );
	SourceFilter& operator=( const SourceFilter& );	
};



#endif
