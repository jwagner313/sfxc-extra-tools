// VisBuffer object for SFXC/SoftwareCorrelator output datafiles
//
// HV 13/2/2007   initial version
//
#ifndef JIVE_J2MS_SFXCVISBUFFER_H
#define JIVE_J2MS_SFXCVISBUFFER_H

#include <jive/j2ms/VisibilityBuffer.h>
#include <jive/j2ms/CorrelatorSetup.h>
#include <jive/j2ms/scanlist.h>

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <casa/Arrays/Vector.h>
#include <casa/Arrays/Matrix.h>
#include <measures/Measures/Muvw.h>

#include <string>
#include <stdint.h>

// have to forward declare this, otherwise we get
// circular include-probs
namespace j2ms {
    class COFScan;
}

struct SFXCKey
{
  uint8_t antenna;
  int polarisation;
  std::pair<int, int> subband;
};

struct SFXCBaselineKey
{
  std::pair<uint8_t, uint8_t> baseline;
  std::pair<int, int> polarisation;
  std::pair<int, int> subband;
};

class SFXCVisBuffer: public VisibilityBuffer
{
 public: 
  // we support this construction signature
  SFXCVisBuffer(const casa::String& vb, const Experiment* exp,
		const std::string& vbopt = std::string());

  // We *must* supply these [the VisBuffer interface]
  virtual casa::Bool                   getCurrentVisibility( Visibility& v );
  virtual casa::Bool                   next( void );
  virtual const casa::String&          getRootDir( void ) const;
  virtual casa::Int                    getVisBufId( void ) const;
  virtual casa::String                 getProcessorSubType( void ) const;
  virtual CorrelatorSetup              correlatorSetup( void ) const;
  // Do not override this one. Let the default impl do its work
  // (refers back to the thing the experiment gives)
  // virtual const j2ms::scaninfolist_t&  getScanlist( void ) const;
  virtual const j2ms::stationscanlist_t& getStationScanlist(int) const;
  virtual ~SFXCVisBuffer();

 private:
  // List of pointers to scans. Memory management is left up to user
  // [you keep track wether or not you 0wn the pointer :)]
  typedef std::vector<j2ms::COFScan*>  scanptrlist_type;

  // CorrelatorSetup is fixed per visbuffer
  //  fabricdata                 __fabdata;
  std::string                __mode;
  casa::String               __rootdir;
  CorrelatorSetup            __setup;
  scanptrlist_type           __scanptrlist;
  j2ms::stationscanlist_t stationscanlist;

  // We have same temporary storage that we can
  // pre-allocate. We do not want to alloc this
  // over and over again. Once we know the
  // nr-of-channels we can alloc these
  typedef casa::Vector<casa::Complex>  complexvector_type;
  typedef casa::Vector<casa::Float>    floatvector_type;
  typedef casa::Matrix<casa::Complex>  complexmatrix_type;

  complexvector_type            cplxbuf; 
  floatvector_type              weight;
  complexmatrix_type            data;
  complexvector_type            cplxnullen;

  // Current status

  // Helper functions
  void createAntennaMapping(void);
  std::vector<casa::Int> antenna_map;

  void createSubbandMapping(void);
  std::map<std::pair<int, int>, casa::uInt> subband_map;

  std::map<casa::Int, casa::MVuvw> uvw_map;
  int next_uvws;

  std::map<SFXCKey, double> weight_map;
  std::map<SFXCKey, int32_t> ntotal_map;
  std::map<SFXCBaselineKey, double> baseline_weight_map;
  int next_stats;

  int fd;
  std::vector<float *> visibilities;
  std::vector<SFXCBaselineKey> keys;
  int num_channels;

  unsigned int current_baseline, next_baselines;
  int current_slice, next_slice;

  std::set<std::pair<uint8_t, uint8_t> > baselines;
  std::set<std::pair<int, int> > subbands;

  std::set<std::pair<uint8_t, uint8_t> >::const_iterator baseline;
  std::set<std::pair<int, int> >::const_iterator subband;

  casa::Int start_year, start_month, start_day, start_sec;
  casa::Double integration_time;
  casa::MVEpoch slice_time;

  int32_t correlator_version;

  void read_timeslice(void);
  void read_timeslice_header(void);
  void read_uvws(void);
  void read_stats(void);
  void read_baseline(void);

  // Prohibit these
  SFXCVisBuffer();
  SFXCVisBuffer(const SFXCVisBuffer&);
  const SFXCVisBuffer& operator=(const SFXCVisBuffer&);
};

#endif
