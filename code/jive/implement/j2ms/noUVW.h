// shared definition of what constitutes a no-UVW-value present...

#ifndef JIVE_J2MS_NOUVW_H
#define JIVE_J2MS_NOUVW_H

#include <casa/Arrays/Vector.h>
#include <casa/Quanta/MVuvw.h>
#include <measures/Measures/Muvw.h>
#include <float.h>

namespace j2ms {
    // This denotes - for our purposes - a 'non existent' UVW value
    // it's a UVW value that we take to be well beyond any real values...
    static const casa::Vector<casa::Double>  noUVWv(3, -DBL_MAX);
    
    // id. but now as MVuvw
    static const casa::MVuvw                 noMVuvw( noUVWv );

    // and as full blown measure
    static const casa::Muvw                  noMuvw( noMVuvw );
}

#endif
