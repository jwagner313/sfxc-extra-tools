//
//  BaseBand -> a class describing the structure of a baseband:
//
//  bandwidth, startfreq, nr. of channels, polarization combinations
//
//  Assume startfreq is always the LOWER frequency edge of the band
//
//  Author:  Harro Verkouter,    2-12-1998
//
//  $Id: BaseBand.h,v 1.8 2011/10/12 12:45:27 jive_cc Exp $
//
#ifndef BASEBAND_H
#define BASEBAND_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>

#include <jive/labels/polarization.h>

#include <vector>
#include <map>
#include <iostream>

// forward declaration
class FrequencyConfig;

class BaseBand
{
    //  FrequencyConfig is our friend!
    friend class FrequencyConfig;
    
public:
    //  BaseBands are upper or lower sideband or not initialised!
    typedef enum _SideBand
    {
		none, upper, lower
    } SideBand;

    // need to be able to link vex channel to recorded polarization
    typedef std::map<int,j2ms::polarization>  vex2polmap;
    typedef vex2polmap::value_type            v2pmapentry;

    static const v2pmapentry                  nomapping;
    
    //  Map string to SideBand
    static BaseBand::SideBand  stringToSideBand( const casa::String& sbtxt );

    //  Empty baseband
    BaseBand();

    //  Copy c'tor
    BaseBand( const BaseBand& other );
    
    //  Create a (potentially) fully defined baseband
    //  The vex-to-polarization map entries are invalid
    //  by default, in which case you may define them
    //  later on by using 'registerVexChannel()'
    //
    //  Note: if (any of) the v2pmapentries fails to
    //  insert the app will be abort'ed(). That is: an entry
    //  will be attempted to insert if it is not 'nomapping'
    //  and if that fails: it's finito. Har har... (evil grin)
    //
    //  * All freq's are in Hz
    BaseBand( casa::Double startfreq, casa::Double bandwidth,
              BaseBand::SideBand sideband, 
              const v2pmapentry& v0top0 = nomapping,
              const v2pmapentry& v1top1 = nomapping );

    // Assignment
    const BaseBand& operator=( const BaseBand& other );

	casa::uInt                     getBaseBandNr( void ) const;
    casa::Double                   getStartfreq( void ) const;
    casa::Double                   getBandwidth( void ) const;
	const vex2polmap&              getVexChannels( void ) const;
    BaseBand::SideBand             getSideBand( void ) const; 

    //  Test if this BaseBand has a parent configuration...
    casa::Bool                     hasConfiguration( void ) const;

    //  Get the parent configuration.
    //
    //  NOTE: throws AipsError if no parentconfiguration!!!!!
    const FrequencyConfig& getConfiguration( void ) const;

    //  Compare with another BaseBand
    //
    //  Return true id frequency/bandwidth/sideband are the same for
    //  both sides of the equation...
    bool    operator==( const BaseBand& other );

    // (attempt to)register a specific vexchannel->polarization
    // mapping entry
	bool    registerVexChannel( const v2pmapentry& v2p );
	
    //  Delete the baseband
    ~BaseBand();


protected:
    //  Only FrequencyConfig's can create a REALLY fully defined
    //  BaseBand. The frequencyconfig will fit the baseband with a) a
    //  subbandnr (or ID) and b) a pointer to itself, so the baseband
    //  knows who's his parent.
    //
    //  All the frequency settings/polarizations are COPIED from
    //  bbdata!
    BaseBand( const FrequencyConfig* ptr2parent,
			  casa::uInt basebandnr,
			  const BaseBand& bbdata );
    

private:    
    //
    //  The private parts
    //

    //
    //  These settings describe the BaseBand
    //
    vex2polmap         itsVex2PolMap;
    casa::Double       itsStartfreq;
    casa::Double       itsBandwidth;
    BaseBand::SideBand itsSideBand;
    
    //  These internals can only be set by a FrequencyConfig (i.e. not
    //  configurable by the user)
    casa::uInt             itsBaseBandnr;
    const FrequencyConfig* itsParentConfigurationptr;
};

//  Output on a stream
std::ostream& operator<<( std::ostream& os, const BaseBand& bbref );
std::ostream& operator<<( std::ostream& os, const BaseBand::SideBand& sbref );

// comparison on v2pmapentries
bool operator==(const BaseBand::v2pmapentry& l, const BaseBand::v2pmapentry& r );
bool operator!=(const BaseBand::v2pmapentry& l, const BaseBand::v2pmapentry& r );
std::ostream& operator<<( std::ostream& os, const BaseBand::v2pmapentry& v2p );

// And add a typedef for a list/collection of sidebands
typedef std::vector<BaseBand> basebandlist_t;

#endif
