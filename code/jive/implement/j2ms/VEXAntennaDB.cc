//  Implementation of the VEXAntennaDB - class
//
//  Author: Harro Verkouter,   6-7-1999
//
//  $Id: VEXAntennaDB.cc,v 1.11 2007/02/26 13:28:39 jive_cc Exp $
//
//  $Log: VEXAntennaDB.cc,v $
//  Revision 1.11  2007/02/26 13:28:39  jive_cc
//  HV:  - adhere to new I/F
//       - re-implemented
//       - Now throws if something fishy because
//         things shouldn't be fishy at THIS level
//
//  Revision 1.10  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.9  2006/02/10 08:53:44  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.8  2006/01/13 11:35:40  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.7  2004/08/25 05:58:03  verkout
//  HV: * Got rid of 'uint16' (replaced with 'unsigned int').
//        (not everywhere yet)
//      * Started using CountedPointers to DataFileRecords in
//        COF* classes (such that datafilerecords, read from file
//        get automagically deleted when nobody refers to them
//        anymore
//      * Added some exception catching here and there...
//
//  Revision 1.6  2004/01/05 15:02:11  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.5  2003/09/12 07:27:32  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.4  2003/02/14 15:41:37  verkout
//  HV: vex++ now uses std::string. Have to change to using std::string as well.
//
//  Revision 1.3  2001/05/30 11:48:14  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.2  2000/03/29 09:57:43  verkout
//  HV: Added extra antenna info like mount and axisoffset. Mounts are now passed on from VEX-file to FITSfile ok..(i hope..)
//
//  Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//  HV: imported aips++ implement stuff
//
//
#include <jive/j2ms/VEXAntennaDB.h>
#include <jive/Utilities/jexcept.h>

#include <prep_job/vexplus.h>
#include <casa/Arrays/Vector.h>

#include <iostream>
#include <iomanip>

using namespace std;
using namespace casa;


VEXAntennaDB::VEXAntennaDB( const String& vexfilename ) :
    AntennaDB()
{
    int         parseres;
    VexPlus     vexfile( vexfilename );

    //  Attempt to open/parse and read VEX-file!
    if( (parseres=vexfile.parseVex())!=0 )
        THROW_JEXCEPT("Failed to open and/or parse '" << vexfilename << "' code="
                      << parseres);
    
    // would have liked the following 'const' but then we cannot assign them
    // to 'stationptr' or 'mountptr'  :/
    String         station;
    String         site;
    String         tlc;
    String         mount;
    MPosition      position;
    Vector<Double> pos( 3 );
    Vector<Double> off( 3 );
    const String   defsite( "<UNKNOWN>" );
    const String   defmount( "none" );

    for( int i=0; i<vexfile.N_Stations(); i++ ) {
        station = vexfile.Station( i );
    	site    = vexfile.Site( station );
    	tlc     = vexfile.TwoLetter( station );
    	mount   = vexfile.AxisMount( station );
	   
        if( station.empty() )
            THROW_JEXCEPT("Missing station name for antenna #" << i);
        // cannot invent a default mount...
        if( mount.empty() )
            THROW_JEXCEPT("Missing mount for " << station);

        // the following may be defaulted, but warn user
        if( site.empty() ) {
            site = defsite;
            cerr << vexfilename << ": Missing site for " << station << ", defaulting to '"
                 << site << "'\n";
        }
        if( tlc.empty() ) {
            tlc = station;
            cerr << vexfilename << ": Missing two-letter-code, defaulting to '" << tlc << "'\n";
        }

    	//  Transfrom Vex-style mount defns to AIPS++ (and in the
    	//  end FITS)-style mount-strings
        //  Warn if they're not standard - the MS will still be usable
    	if( mount=="az_el" ) 
            mount = "alt-az";
    	else if( mount=="ha_dec" )
            mount = "equatorial";
    	else {
            cerr << "VEXAntennaDB: Mount type " << mount << " not recognized\n"
                 << "              for antenna " << station << " (#" << i << ")\n"
        		 << "              It is likely it will show up incorrectly\n"
        		 << "              in any exported dataformat (e.g. FITS)\n";
	    }
	    
        pos( 0 ) = vexfile.SiteX( station );
        pos( 1 ) = vexfile.SiteY( station );
        pos( 2 ) = vexfile.SiteZ( station );	    

    	off = 0.0;
    	off( 0 ) = vexfile.AxisOffset( station );
    	    
    	position = MPosition( MVPosition(pos(0), pos(1), pos(2)), MPosition::ITRF );
   
        // and stick in the map
        AntennaInfo                        ai(station, site, position, 0.0,
                                              mount, off, i, tlc);
        pair<idmap_t::iterator,bool>       insres2;
        pair<antennalist_t::iterator,bool> insres;

        // Create the AntennaInfo with 'id' equal to the arrayindex in the VEXfile.
        // This should not be broken [well, if you did, then the JIVEMSFiller would
        // start to complain] because the antennaId's in the MS are _direct_ indices
        // in the MSAntenna table. Hence, the only way to create a consistent dataset
        // is to make sure the antenna at row 'i' will have an associated ID 'i'.
        // We will assign the IDs in vex-file order and leave it up the MSFiller to
        // put the antenna with ID 'i' at row 'i'.... *sigh*
        insres = myAntennaList.insert( make_pair(station, ai) );
        if( !insres.second ) 
            THROW_JEXCEPT("Failed to insert entry into station->antennainfo mapping");

        insres2 = myIdMap.insert( make_pair(ai.getId(), insres.first) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to insert entry into Id->AntennaInfo map");
    }
    // okiedokie, done!
}

//  Look up stuff!
AntennaInfo VEXAntennaDB::operator[]( const String& key ) const {
    AntennaInfo                   retval;
    antennalist_t::const_iterator curant;
   
    if( (curant=myAntennaList.find(key))!=myAntennaList.end() )
        retval = curant->second;
    return retval;
}

Bool VEXAntennaDB::searchAntenna( AntennaInfo& airef, const String& key ) const {
    antennalist_t::const_iterator curant;
   
    if( (curant=myAntennaList.find(key))!=myAntennaList.end() )
        airef = curant->second;
    return (curant!=myAntennaList.end());
}

Bool VEXAntennaDB::searchAntenna( AntennaInfo& airef, const Int id ) const {
    idmap_t::const_iterator curant;
  
    if( (curant=myIdMap.find(id))!=myIdMap.end() )
        airef = (curant->second)->second;
    return (curant!=myIdMap.end());
}

size_t VEXAntennaDB::nrAntennas( void ) const {
    return myAntennaList.size();
}

//  Clean up...
VEXAntennaDB::~VEXAntennaDB()
{}
