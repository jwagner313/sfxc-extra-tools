//  CorrelatorSetup - describes how a scan was correlated, holds info
//  like nr of lags, which polarizations were formed, integrationtime
//  etc.
//
//  In short: it is more an aggregation than an abstraction, therefore
//  I probably should have made it a struct rather than an object.
//
//  Author:  Harro Verkouter,   13-07-1999
//
//  $Id: CorrelatorSetup.h,v 1.14 2011/10/12 12:45:28 jive_cc Exp $
//
//  $Log: CorrelatorSetup.h,v $
//  Revision 1.14  2011/10/12 12:45:28  jive_cc
//  HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//        changed into 'labels' for more genericity
//      * can now be built either as part of AIPS++/CASA [using makefile]
//        or can be built as standalone set of libs + binaries and linking
//        against casacore [using Makefile]
//        check 'code/
//
//  Revision 1.13  2008-07-31 10:47:36  jive_cc
//  HV - 31/07/2008  New version of Albertsoftware (3.0): the diff between
//                   header & data is now only 1 BOCF, rather than 2, what
//  		 it used to be up until now. Modifications implemented
//  		 that set the actual value of the diff based on AB
//  		 s/w version. Name of the entry in the JIVE_PROCESSING table
//  		 (2BOCF_FIX or something) left unchanged but the actual
//  		 amount of time-fixing *is* altered. The name of the entry
//  		 left untouched to not harm downstream apps that may
//  		 rely on this name.
//
//  Revision 1.12  2007/05/25 09:35:28  jive_cc
//  HV: - CorrelatorSetup now also has bocfrate (<0 => unknown,
//      is for backward compat) [if known, we can do the 2BOCF
//      fix JIVE specific processing]
//      - move away from homegrown RegularExpression class to
//      casa::Regex
//
//  Revision 1.11  2007/03/15 16:31:46  jive_cc
//  HV:  * CorSWVersion removed as nested class of CorrelatorSetup
//       * Replaced by more generic SWVersion class
//       * rewrote relational operators for SWVersion in expressions
//         only involving the basic "less than" operator for SWVersions
//
//  Revision 1.10  2007/02/26 11:22:10  jive_cc
//  HV: Now uses sequences of physical polarization combinations to
//      keep track of computed correlator products. The data for the
//      visibilities is delivered in the order of "getCorrelations()"
//      Requires (and can give you) the logical-to-physical polarization
//      mapping to map between logical indices (usually '0' and '1')
//      and physical polarizations they represent
//
//  Revision 1.9  2006/03/02 14:21:39  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.8  2006/02/10 08:53:43  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.7  2006/01/13 11:35:37  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.6  2004/09/29 15:45:59  verkout
//  HV: Hmmm.. 'major' and 'minor' are macros under Linux.. changed the memberfunctions to getMajor/getMinor :(
//
//  Revision 1.5  2004/08/25 05:57:46  verkout
//  HV: * Got rid of 'uint16' (replaced with 'unsigned int').
//        (not everywhere yet)
//      * Started using CountedPointers to DataFileRecords in
//        COF* classes (such that datafilerecords, read from file
//        get automagically deleted when nobody refers to them
//        anymore
//      * Added some exception catching here and there...
//
//  Revision 1.4  2004/01/05 15:01:56  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.3  2003/09/12 07:26:44  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.2  2001/05/30 11:47:10  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//  HV: imported aips++ implement stuff
//
//
//
#ifndef CORRELATORSETUP_H
#define CORRELATORSETUP_H

#include <casa/aips.h>
#include <jive/labels/polarization.h>
#include <jive/labels/CorrelationCode.h>
#include <iostream>
#include <string>

//  ***************************
//  Correlator software version
//  ***************************
struct SWVersion {
    // even though stuff is public by default in a struct,
    // we explicitly mention it here to stress the fact
    // that we also have private stuff!
    public:
        SWVersion(unsigned int mjr=0, unsigned int mnr=0);

        //  Allow read-only access....
        unsigned int getMajor( void ) const;
        unsigned int getMinor( void ) const;


    private:
        unsigned int myMajor;
        unsigned int myMinor;
};

// show in HRF
std::ostream& operator<<(std::ostream& os, const SWVersion& v);

// comparison operators for Software versions
bool operator==(const SWVersion& l, const SWVersion& r);
bool operator!=(const SWVersion& l, const SWVersion& r);
bool operator<(const SWVersion& l, const SWVersion& r);
bool operator<=(const SWVersion& l, const SWVersion& r);
bool operator>(const SWVersion& l, const SWVersion& r);
bool operator>=(const SWVersion& l, const SWVersion& r);



class CorrelatorSetup
{
    //  Comparison
    friend casa::Bool operator==(const CorrelatorSetup& lhside, const CorrelatorSetup& rhside);


public:
    //  An empty correlatorsetup can exist..
    //
    //  However, values will be set to nothing or ludicrous values to
    //  indicate that they are not set. Look at the accessor methods
    //  to find out specifics
    CorrelatorSetup();
   
    // create a fully defined correlatorsetup
    CorrelatorSetup( unsigned int nrspectralpoints,
                     const correlationset_t& correlations,
		     		 double integrationtime,
					 const SWVersion& swver,
                     const j2ms::log2polmap& l2p,
                     int bocfrate );

    // Compiler generated copy c'tor and assignment oper. are perfectly Ok
    // since all of our datamembers have well defined copy/assignment operators.
    // [and we do not use dynamic memory]

    //  How many spectral points?  NB: we're safe up to (max unsigned int)-1
    //  lags... which is approx. 4.2billion lags... (at least, with 32bit
    //  unsigned ints)
    //
    //  If not initialized by usr, it is set to (unsigned int)-1
    unsigned int             nrOfSpectralPoints( void ) const;

    //  Retrieve the correlations that were formed during this
    //  correlator setup..
    const correlationset_t&   getCorrelations( void ) const;

    // return the polarization mapping for this Scan
    const j2ms::log2polmap&   l2pmap( void ) const;

    //  Retrieve the integrationtime for this setup (units is seconds)
    double                   getIntegrationTime( void ) const;

	const SWVersion&         getSoftwareVersion( void ) const;

    // Returns the bocfrate with which the correlator was run in this
    // setup.
    // May return -1 (or, <=0, to be more general) if the system did
    // not know what the bocfrate was.. (bocfrate==0 is well, somewhat
    // invalid i'd say so include that value to mean 'unknown' as well)
    int                      getBocfRate( void ) const;

    // The offset between model and data in units of BOCF, if known.
    // Primarily derived from AB s/w version. For all versions of AB s/w >= 3.0
    // this is 1, otherwise it is 2.
    int                      headerDataBocfDiff( void ) const;

    //  De-allocate any allocated resources (if any)
    ~CorrelatorSetup();


    /**********************************/
    //          STATIC METHOD(S)
    /**********************************/
    
    //  From some predefined correlator modes (see Huib's document),
    //  return the accompanying Setup.
    //
    //  NOTE: you still need to supply the integrationtime, since that
    //  cannot be derived from the correlatormode...
    //  NOTE: and you must supply a logical-to-physicalpolarization-map
    //  which tells the system what polarization '0' and '1' are
    //   By the way, if the number of polarization products >=2,
    //   they are inserted in the following order:
    //   (p0,p0), (p1,p1)[,(p0,p1),(p1,p0)]
    //   with p0 the polarization associated with logical label '0'
    //   in the map and '1' well.. you figure it out :)
    static CorrelatorSetup   correlatorModeToSetup( int correlatormode,
                                                    double integrationtime,
                                                    const j2ms::log2polmap& l2p,
                                                    int bocfrate);

	// Read a CorrelatorSetup from an (output)VEXfile. Filter out the lines that
	// prep_job (from JCCS) writes in the (output)VEXfile and work out the correlator
	// setup from that.
	static CorrelatorSetup   correlatorSetupFromFile( const std::string& vexfilename );

	/**********************************/
	//       STATIC MEMBER(S)
	/**********************************/
	//
	//  Dedicated unique software version that (should) put the system
	//  in bypass mode: i.e. all corrections after reading the data 
	//  (e.g. vernier delay, rotating of the fourth polarizationproduct)
	//  should be discarded.....
	//
	//  Check CorrelatorSetup.cc for actual definition of the version number,
	//  I started off with: ABMajor=12345, ABMinor=67890.
	static const SWVersion  bypassVersion;

    
private:

    //  How many spectral points were produced? (NOT the number of
    //  lags!!)
    unsigned int            myNumberOfSpectralPoints;    

    //  Which polarization combinations were formed by the correlator?
    //  Note: we use the *ordered* set, thereby fixing the order in 
    //  which the polarization products are delivered upstream.
    //  [The visibilitybuffers should take care that row #n in a
    //   datamatrix contains the data for polarization product #n
    //   in this set]
    correlationset_t        myCorrelations;

    //  Eevery correlator setup has an integration time associated
    //  with it...
    //
    //  NOTE: Units is seconds!
    double                  myIntegrationTime;

	//  Correlator software version -> can be used to base
	//  decisions on...
	SWVersion               mySoftwareVersion;

    // remember the logical->physical mapping with which this
    // setup was created so we can always find back, for this
    // setup, what the logical label of a physical polarization
    // was
    j2ms::log2polmap        myL2PMap;

    // the bocfrate that was used.
    // <=0 implies "we don't know what the bocfrate was"
    int                     myBocfRate;

    // offset between model and data: ie the correction to apply to the data-timetag
    int                     myHeaderDataBocfDiff;
};

//  Show the correlator setup in Human Readable Format
std::ostream& operator<<( std::ostream& os, const CorrelatorSetup& csref );

//  This one doesn't need to be a friend!
casa::Bool operator!=(const CorrelatorSetup& lhside, const CorrelatorSetup& rhside);

#endif
