// Support for correlator model poly's + other model info
//
// $Id: correlatormodel.h,v 1.2 2011/10/12 12:45:32 jive_cc Exp $
//
// H. Verkouter
// 01/11/2006
// 
#ifndef JIVE_J2MS_CORRELATORMODEL_H
#define JIVE_J2MS_CORRELATORMODEL_H

#include <cstring>
#include <vector>
#include <list>
#include <jive/j2ms/carraywrap.h>
#include <jive/labels/polarization.h>


// stick this stuff in own namespace
//
// In order to be able to stick the modelpolynomials 
// to the MS, we will follow the following method:
//
// * JIVEMSFiller is the thing that actually writes
//   the MS. It reads from a VisibilityBuffer and
//   writes to a MS
// * The actual VisibilityBuffer may or may not
//   produce modeldatarecords. If it does, the JIVEMSFiller
//   will write those into the appropriate MS tables.
//
// * The 'stationID' property of a modeldatarecord is 
//   interpreted as an arrayindex into the Experiment's
//   antennaarray. As such, the actual VisibilityBuffer 
//   should make sure that the 'stationID' indeed point
//   at the correct entry, ie it produces a consistent
//   set of data.
namespace j2ms {

    // A correlatormodel polynomial's coefficients.
    // They describe the polynomial for a given subband/polarization
    // combination.
    // 
    // Note: in the data we can find the 'logical' subband
    // by using the mappings file, which allows us to translate
    // from SU channel to vexchannel, and using the FrequencyDB,
    // we can look for the specific BaseBand this vexchannel
    // belongs to. The '__subband' will be interpreted as
    // index into the BaseBand array of the FrequencyConfig
    // of the Scan that this particular polynomial was part of.
    typedef j2ms::carraywrap<double, 6>   coefficients_type;
   
    // Let's reorganize things a bit.
    // In the MS JIVE_MODEL table we have to write rows for antenna/subband
    // combinations, containing the polynomials for all
    // polarizations. Also, the polynomials must be reordered
    // such that the polynomial at row #n in the matrix applies
    // to the polarization #n as listed in the MSFeed table-row 
    // which applies to the station/antenna.
    // As we have short-circuited by saying that for all antennas
    // the polarizations are written in the same order [see
    // "writeAntennaAndFeedTable()" in JIVEMSFiller.cc] we do not
    // have to care about changing the order per station but we
    // MUST write the polynomials in the same order as
    // writeAntennaAndFeedTable() does. writeAntenna...() 
    // iterates over the log2pol map for the visibilitybuffer and
    // writes'm in that order [NOTE: the log2polmap is an 
    // *ordered* set so it does not matter in which order the
    // entries get inserted].

    // per subband/polarization we keep one of these babies:
    // both phase & delay polynomials
    struct polypair {
        // default: all polycoefficients set to 0.0
        polypair();
        polypair( const coefficients_type& phs,
                  const coefficients_type& del );
        
        coefficients_type   phase;
        coefficients_type   delay;
    };
    
    // record a polypair per polarization
    typedef std::map<j2ms::polarization, polypair> pol2polymap_type;

    // and we keep one of *those* (pol2polymap_type) per subband
    typedef std::map<int, pol2polymap_type>        sb2polymap_type;

    // and we could have a set of those (sb2polymap_type) per source!
    // with MFC, at the same time, sb, pol, correlations may be formed
    // to a different phase-centre .. :)
    typedef std::map<int, sb2polymap_type>         src2sbpolymap_type;

    // value + derivative [supports 1st order Taylor series expansion]
    typedef j2ms::carraywrap<double, 2>  taylor_1;

    // now we can form the modeldatarecord, which describes one
    // set of model-properties valid for one station for the
    // given time interval.
    // Only the models for channels that actually carried data
    // (not always all maximum 16 SU channels are active) should
    // be in here. 
    // The JIVEMSFiller writes all entries found in this object
    // into the JIVE_MODEL subtable.
    struct modeldatarecord {
        // empty record
        modeldatarecord();

        int                stationID;
        double             startMJDInSeconds;
        double             durationInSeconds;
        taylor_1           wetAtmosphere;
        taylor_1           dryAtmosphere;
        taylor_1           clockOffset;

        // The polys for delay & phase, sorted by source, subband & polarization
        // which allows for simple iteration! [and insertion]
        //
        // [they *may* be different but then again, they
        //  may be the same, bar a constant factor wavelength or frequency]
        src2sbpolymap_type src2sbpolymap;
    };
   
    // VisibilityBuffers may produce a list of modeldatarecords.
    // They may be unordered so a list is just fine.
    typedef std::list<modeldatarecord>  modeldata_t;
} // end of namespace

#endif
