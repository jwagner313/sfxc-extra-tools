#include <jive/j2ms/VBufProcessingStep.h>

using std::string;

VBufProcessingStep::VBufProcessingStep( const string& t,
                                        const string& p ):
    task( t ), parameters( p )
{}
