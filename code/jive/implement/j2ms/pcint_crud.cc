// implementations
//
// $Id: pcint_crud.cc,v 1.2 2007/05/25 09:47:19 jive_cc Exp $
//

// only compile usefull stuff if PCInt support req'd
#ifdef PCINT

#include <jive/j2ms/pcint_crud.h>

// for throwing JIVE-Exceptions (c)
#include <jive/Utilities/jexcept.h>

// handy utility from PCInt 
#include <myutil/hex.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>


using namespace std;
using namespace pcint;


// The datafile thingy
datafile::datafile( const string& filenm,
                    const string& decodernm ):
    fd( -1 ),
    filename( filenm ),
    decoder( decodernm, pcint::stringToParameterlist("verbose=false") ),
    decodedblocks(),
    block2get( decodedblocks.end() )
{
    struct stat   df;
    
    // if we got here, the Module was loaded ok ;)
    //  the c'tor of Module throws up if attempt to 
    //  load a non-existant module..

    //  we only need read-only access
    if( (fd=::open(filename.c_str(), O_RDONLY|O_LARGEFILE))<0 )
    {
        THROW_JEXCEPT("\n\nFailed to open '" << filename << "' - "
                      << ::strerror(errno) << "\n\n");
    }
    if( ::fstat(fd, &df)!=0 )
    {
        THROW_JEXCEPT("\n\nFailed to stat '" << filename << "' - "
                      << ::strerror(errno) << "\n\n");
    }
    if( !S_ISREG(df.st_mode) )
    {
        THROW_JEXCEPT("\n\n'" << filename << "' - "
                      << "is NOT a regular file!\n\n");
    }
    // that's it for now?
}

blocklist_t::value_type datafile::getNextBlock( void )
{
    if( block2get!=decodedblocks.end() )
    {
        // remember current block-pointer
        blocklist_t::iterator  save = block2get;

        // advance pointer to next block in the list
        block2get++;
        
        // and return the block we were pointing at before
        // we incremented the internal pointer
        return *save;
    }

    // ok internal blocklist exhausted
    // attempt to read sum data from da file and
    // try to decode...
    const unsigned int  bsize( 1024*1024 );
    ssize_t             nread;
    RAWDataBlock        rdb;
    
    // first: clear internal list of blocks
    decodedblocks.clear();

    while( decodedblocks.size()==0 &&
           rdb.resize(bsize) &&
           (nread=::read(fd, rdb.begin(), rdb.size()))>0 )
    {
        // bit stoopid that this has to be done but the RAWDataBlock
        // really doesn't know what has been written to it...
        rdb.resize(nread);
        // anyway, now we can try to decode. If nothing found,
        // da decoder will give back an empty list and we'll try
        // to read a next chunk of data etc. until either da file
        // is, like, exhausted or we do have sum data decoded eh
        decodedblocks = decoder.run<blocklist_t, const RAWDataBlock&>( rdb );
    }

    // if loop, like, terminated without giving us any blocks, there's prolly
    // nothing more to give eh?
    if( decodedblocks.size()==0 )
    {
        blocklist_t::value_type  rv;

        // resize the data in the block to zip
        rv.data.resize(0);
        
        // let the internal block-pointer point to the end 
        // of the empty list such that if the caller, foolish
        // as he/she may be, decides to call this method *again*
        // we won't fall over
        block2get = decodedblocks.end();
        return rv;
    }

    // Set internal block pointer to the *second* block
    // in the list because we're going to return the
    // *first* one. The internal pointer always points
    // at the *next* block to give (or: end-of-list, if
    // no more blocks)
    block2get = decodedblocks.begin();
    block2get++;
    return *decodedblocks.begin();
}

datafile::~datafile()
{
    // d'tor only called if object succesfully created
    // and (and of the) post-condition of succesfull creation
    // is that 'fd' points to succsfully opened file...
    ::close(fd);
}


ostream& operator<<(ostream& os, const pcint_intf& pi )
{
    return os << "(" << pi.nlag << "lag @" << pi.offset
              << ", #" << pi.ab_intf
              << ", dt=" << hex_t(pi.datatype)
              << ", data: " << ((pi.data_present)?("Yes"):("No")) << ")";
}

ostream& operator<<(ostream& os, const pcint_baseline& pbl )
{
    return os << "(Xant=" << pbl.xant << ", Yant=" << pbl.yant << ", Sb=" << pbl.subband << ")";
}

ostream& operator<<(ostream& os, const pcint_integration& intg)
{
    os << "Intg[s/e bocf=" << intg.start_bocf << "/" << intg.end_bocf << ", "
       << intg.datablocks.size() << " datablocks, " << intg.baselines.size()
       << " baselines] ";
    return os;
}

#endif
