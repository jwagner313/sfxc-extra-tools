//  Struct? ExperimentCache -> used to cache experiment properties,
//  like antennatable/sourcetable/dataorder
//
//
#ifndef EXPERIMENTCACHE_H
#define EXPERIMENTCACHE_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <jive/j2ms/AntennaInfo.h>
#include <jive/j2ms/SourceInfo.h>
#include <jive/j2ms/BOCFToTime.h>

#include <jive/j2ms/Scan.h>
#include <jive/j2ms/SourceDB.h>
#include <jive/j2ms/AntennaDB.h>
#include <jive/j2ms/Experiment.h>

#include <jive/labels/CorrelationCode.h>

//  Ok. We drag in al the measures stuff; we need it for the
//  uvw-calculation (on a per scan basis)
#include <measures/Measures.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/MDirection.h>
#include <measures/Measures/MBaseline.h>
#include <measures/Measures/MCEpoch.h>
#include <measures/Measures/MEpoch.h>


#include <map>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>

class ExperimentCache;

struct _ExperimentCacheElement;


typedef struct _ScanCacheElement
{

public:
    //  Alloc/init
    _ScanCacheElement();
    
    //  Public datamembers
    casa::String        itsScanCode;
    SourceInfo          itsSourceInfo;

    //  Machinery for UVW-calculation
	casa::MEpoch::Ref         outEpochRef;
	casa::MEpoch::Convert     timeCvt;

    //  Variables needed for BOCF to time translation
    int                 prefixtimeadjust;
    int                 postfixtimeadjust;
    unsigned int        samplecount;
    unsigned int        itsLastIntegration;
    unsigned int        itsLastendbocf;
    BOCFToTime          bocf2time;

    //  Free resources
    ~_ScanCacheElement();
    
private:
    //  cache-elements cannot be copied/assigned to
    _ScanCacheElement( const _ScanCacheElement& );
    const _ScanCacheElement& operator=( const _ScanCacheElement& );
} ScanCacheElement;


// The per experiment cached stuff

// the antenna table
typedef std::vector<AntennaInfo> antennalist_t;

// keep a mapping of frequencysetup[name] to mapped
// properties such that we can easily go from
// (Scan -> frequencySetupCode) + subband => (SPECTRAL_WINDOW_ID,FEED_ID,...)

// each subband may be correlated with different polarization setups
// [in dual pol experiments you could decide to correlate with or without
//  cross pols for example]
//
// so we use the correlations_t as key in this mapping ->
// each set of correlations. As order _is_ important here,
// we use the correlations_t and NOT the correlationset_t. See
// CorrelationCode.h for details

struct cordetails {
    int     __polId; // associated polarization id in the MS
    int     __ddId;  // associated data description id in the MS

    cordetails():
        __polId( -1 ), __ddId( -1 )
    {}
    cordetails( int p, int dd ):
        __polId( p ), __ddId( dd )
    {}
};
typedef std::map<correlations_t, cordetails>   definedcorrelations_t;

inline std::ostream& operator<<(std::ostream& os, const cordetails& cd) {
    return os << "[CORDETAIL: P=" << cd.__polId << ", DD=" << cd.__ddId << "]";
}
inline std::ostream& operator<<(std::ostream& os, const definedcorrelations_t::value_type& vt) {
    return os << "{" << as_corrcode(vt.first) << " => " << vt.second << "}";
}
inline std::ostream& operator<<(std::ostream& os, const definedcorrelations_t& dc) {
    std::copy(dc.begin(), dc.end(),
              std::ostream_iterator<definedcorrelations_t::value_type>(os, ", "));
    return os;
}

// subband properties
// * the spectral window id
// * which correlations were formed with this one
struct sbprops_type {
    int                   __spwId; // associated spectral window id in the MS
    definedcorrelations_t __correlations;

    sbprops_type( int spw = -1 ):
        __spwId( spw )
    {}
};
inline std::ostream& operator<<(std::ostream& os, const sbprops_type& sb) {
    return os << "SPW#" << sb.__spwId << ": " << sb.__correlations;
}

// keep the subband properties per subband
// => map<K,V> with K==subband, V==sbprops_type
typedef std::map<int, sbprops_type>         fqprops_type;

inline std::ostream& operator<<(std::ostream& os, const fqprops_type::value_type& vt) {
    return os << "Subband#" << vt.first << " (" << vt.second << ")";
}
inline std::ostream& operator<<(std::ostream& os, const fqprops_type& ft) {
    std::copy(ft.begin(), ft.end(),
              std::ostream_iterator<fqprops_type::value_type>(os, "\n"));
    return os;
}

// and for a full cache, map frequency-setup-name to
// a fqprops_type
typedef std::map<std::string, fqprops_type> fqcache_type;

inline std::ostream& operator<<(std::ostream& os, const fqcache_type::value_type& vt) {
    return os << vt.first << " defines " << vt.second;
}
inline std::ostream& operator<<(std::ostream& os, const fqcache_type& fq) {
    std::copy(fq.begin(), fq.end(),
              std::ostream_iterator<fqcache_type::value_type>(os, "\n"));
    return os;
}

typedef struct _ExperimentCacheElement
{
public:
    
    //  Alloc/init a new element
    _ExperimentCacheElement()
    {}
    
    //  Struct datamembers, make them public to speed up access
    //  (there is a private member too)
    casa::String    itsCode;
    fqcache_type    freqCache;

    //  add a scan
    ScanCacheElement*        addScan( const j2ms::Scan& scanref );

    //  Returns pointer to cached scan (if in cache).
    //  
    //  null-pointer if scan not found in cache
    ScanCacheElement*        locateScan( const casa::String& scancode );

    //  De-allocate stuff
    ~_ExperimentCacheElement();
    
private:
    // cache scans by code
    typedef std::map<std::string, ScanCacheElement*> scancache_t;

    scancache_t      scancache;

    //  We don't like it if elements are copied/assigned!!!!
    _ExperimentCacheElement( const _ExperimentCacheElement& );
    const _ExperimentCacheElement& operator=( const _ExperimentCacheElement& );
} ExperimentCacheElement;



class ExperimentCache
{
public:
    //  Init to empty cache
    ExperimentCache()
    {}

    //  Attempt to add the experiment in the cache.
    ExperimentCacheElement*  addExperiment( const Experiment& expref );

    //  Do a lookup, based on experiment-code.
    //
    //  Returns null-pointer if indicated experiment not found in cache.
    ExperimentCacheElement*  locateExperiment( const casa::String& expcode ) const;

    //  Delete the cache
    ~ExperimentCache();
    
private:
    // keep cache elements associated by experiment-id
    // ie stuff 'm in a map<K,V> where
    // K == the experiment's ID (string)
    // V == the pointer to the cache-element
    typedef std::map<std::string, ExperimentCacheElement*> expcache_t;

    expcache_t      expcache;

    // Prohibit copying/assigning of Experiment caches
    ExperimentCache( const ExperimentCache& );
    const ExperimentCache& operator=( const ExperimentCache& );
};




#endif
