//  SourceInfo:   a class describing the properties of a source
//
// $Id: SourceInfo.h,v 1.3 2007/02/26 13:26:15 jive_cc Exp $
//
// $Log: SourceInfo.h,v $
// Revision 1.3  2007/02/26 13:26:15  jive_cc
// HV: - cosmetic changes
//     - added a source-id property; it is now
//       kept as part of the source-info
//     - define comparison == and !=
//
// Revision 1.2  2006/01/13 11:35:40  verkout
// HV: CASAfied version of the implement
//
//
#ifndef JIVE_J2MS_SOURCEINFO_H
#define JIVE_J2MS_SOURCEINFO_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <measures/Measures/MDirection.h>

#include <iostream>

class SourceInfo
{
    // Show in HRF
    friend std::ostream& operator<<( std::ostream& os, const SourceInfo& siref );

public:
    //  Create an empty SourceInfo
    //  id will be set to '-1' to indicate invalidness
    SourceInfo();
   
    //  Create a 'fully' defined source info object. 'code' =
    //  something like 3C83 or basically, the search string.
    SourceInfo( casa::Int id,
                const casa::String& name,
				const casa::String& code,
				const casa::MDirection& pos=casa::MDirection() );

    //  Get access to the private parts of this object:
    casa::Int                   getId( void ) const;
    const casa::String&         getName( void ) const;
    const casa::String&         getCode( void ) const;
    const casa::MDirection&     getDirection( void ) const;

    // comparison. Does not take into account 'itsCode'
    // as that is not (necessarily) retained in the MS
    bool operator==( const SourceInfo& other ) const;
    bool operator!=( const SourceInfo& other ) const;
    
    ~SourceInfo();

private:
    // The private parts
    casa::Int               itsId;
    casa::MDirection        itsPosition;
    //  The name of this particular source
    casa::String            itsName;
    //  The official code of this source
    casa::String            itsCode;
};

#endif
