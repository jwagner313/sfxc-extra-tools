// helper struct for lis-file based first/last scan filtering in j2ms
// $Id: subjob.cc,v 1.4 2011/10/12 12:45:32 jive_cc Exp $
//
// $Log: subjob.cc,v $
// Revision 1.4  2011/10/12 12:45:32  jive_cc
// HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//       changed into 'labels' for more genericity
//     * can now be built either as part of AIPS++/CASA [using makefile]
//       or can be built as standalone set of libs + binaries and linking
//       against casacore [using Makefile]
//       check 'code/
//
// Revision 1.3  2006-02-10 08:53:44  verkout
// HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
// Revision 1.2  2006/01/13 11:35:41  verkout
// HV: CASAfied version of the implement
//
//
#include <jive/j2ms/subjob.h>
#include <jive/Utilities/jexcept.h>

#include <climits>
#include <stdio.h>


using namespace std;

// backward compatibility mode: f/l scan not filled in
_subjob_t::_subjob_t( const string& job ) :
	jobName( job )
{
}

// f/l scan defined (new default runmode?)
_subjob_t::_subjob_t( const string& job, const string& f, const string& l ) :
	jobName( job ),
	firstScan( f ),
	lastScan( l )
{
}

int _subjob_t::firstScanNo( void ) const
{
	int     rv( INT_MIN );

	if( firstScan.size() )
	{
		int    tmp( -1 );

		if( ::sscanf(firstScan.c_str(), "No%d", &tmp)!=1 )
		{
			if( ::sscanf(firstScan.c_str(), "%d", &tmp)!=1 )
			{
                THROW_JEXCEPT("subjob_t::firstScanNo()/Could not decode '"
                        << firstScan << "'");
			}
		}
		rv = tmp;
	}
	return rv;	
}

int _subjob_t::lastScanNo( void ) const
{
	int     rv( INT_MAX );

	if( lastScan.size() )
	{
		int    tmp( -1 );

		if( ::sscanf(lastScan.c_str(), "No%d", &tmp)!=1 )
		{
			if( ::sscanf(lastScan.c_str(), "%d", &tmp)!=1 )
			{
                THROW_JEXCEPT("subjob_t::lastScanNo()/Could not decode '"
                        << firstScan << "'");
			}
		}
		rv = tmp;
	}
	return rv;
}
