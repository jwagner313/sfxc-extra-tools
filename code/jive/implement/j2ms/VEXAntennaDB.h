//  VEXAntennaDB - Specialization of the antennaDB class that reads
//  the antenna info from a VEX-file
//
//  Author: Harro Verkouter,   6-7-1999
//
//  $Id: VEXAntennaDB.h,v 1.6 2007/02/26 13:28:39 jive_cc Exp $
//
//  $Log: VEXAntennaDB.h,v $
//  Revision 1.6  2007/02/26 13:28:39  jive_cc
//  HV:  - adhere to new I/F
//       - re-implemented
//       - Now throws if something fishy because
//         things shouldn't be fishy at THIS level
//
//  Revision 1.5  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.4  2006/01/13 11:35:40  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.3  2004/01/05 15:02:13  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.2  2001/05/30 11:48:16  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//  HV: imported aips++ implement stuff
//
//
#ifndef JIVE_J2MS_VEXANTENNADB_H
#define JIVE_J2MS_VEXANTENNADB_H

#include <casa/aips.h>

#include <jive/j2ms/AntennaDB.h>
#include <jive/j2ms/AntennaInfo.h>

#include <map>
#include <string>

class VEXAntennaDB :
    public AntennaDB
{
public:
    //  Create from a file.
    //  If failure to open and/or parse a jexcept-typed
    //  exception is thrown.
    VEXAntennaDB( const casa::String& vexfilename );
    
    //  Lookup something....
    virtual AntennaInfo  operator[]( const casa::String& key ) const;
    virtual casa::Bool   searchAntenna( AntennaInfo& airef, const casa::String& key ) const;
    virtual casa::Bool   searchAntenna( AntennaInfo& airef, const casa::Int id ) const;

    virtual size_t       nrAntennas( void ) const;

    //  Destruct the object:
    virtual ~VEXAntennaDB();

private:

    // store the antennainfo keyed on 'name'
    // (beccause that's the key we look for in
    // the search methods)
    typedef std::map<std::string, AntennaInfo> antennalist_t;
    typedef std::map<casa::Int, antennalist_t::const_iterator> idmap_t;

    idmap_t         myIdMap;
    antennalist_t   myAntennaList;

    // Prohibit these:
    VEXAntennaDB();
    VEXAntennaDB( const VEXAntennaDB& );
    const VEXAntennaDB& operator=( const VEXAntennaDB& );
};


#endif
