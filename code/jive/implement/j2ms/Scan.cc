//
//  Implementation of the Scan-class
//
//  Author:  Harro Verkouter,  7-12-1998
//
//  $Id: Scan.cc,v 1.7 2007/02/26 13:23:38 jive_cc Exp $
//
#include <jive/j2ms/Scan.h>
#include <jive/j2ms/VisibilityBuffer.h>
#include <jive/Utilities/jexcept.h>
#include <measures/Measures/MEpoch.h>
#include <casa/Quanta/MVTime.h>
#include <iostream>

using namespace std;
using namespace casa;

namespace j2ms
{

ostream& operator<<( ostream& os, const Scan& sref ) {
   	MVTime   tim( sref.getStartTime().getValue().getTime() );
 
    os << "Scan " << sref.getScanId() << " (#" << sref.getScanNo() << "): "
       << sref.getSourceCode() << "/" << sref.getFrequencyCode()
       << " ** " << MVTime::setFormat(MVTime::DMY) << tim << flush;
    return os;
}


Scan::Scan( const VisibilityBuffer* visbufptr ) :
    itsVisibilityBufferptr( visbufptr )
{
    //  If we don't have a parent-visibilitybuffer: we throw up!
    if( !itsVisibilityBufferptr )
    	THROW_JEXCEPT("Scan::Scan(): No VisibilityBuffer given!");
}

//  De-reference the Experiment-ptr -> usr has RO-access to the
//  experiment properties
const Experiment& Scan::getExperiment( void ) const {
    return itsVisibilityBufferptr->getExperiment();
}

const VisibilityBuffer& Scan::getVisibilityBuffer( void ) const {
    return *itsVisibilityBufferptr;
}


// D'tor doesn't do anything [we do not 0wn the visbufptr so
// do not delete it]
Scan::~Scan()
{}

// End of j2ms namespace
};
