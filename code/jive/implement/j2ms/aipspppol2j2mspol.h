// define translation between aips++ Stokes::StokesTypes correlation products
// and CorrelationCodes
#ifndef JIVE_J2MS_AIPSPPPOL2J2MSPOL_H
#define JIVE_J2MS_AIPSPPPOL2J2MSPOL_H

// for the aips++ Stokes stuff
#include <measures/Measures/Stokes.h>

// for the j2ms definitions
#include <jive/labels/CorrelationCode.h>

// support conversion of a casa::Vector<casa::Int> 
// into correlations_t: the Vector<Int> is supposedly
// from the corrType() column of the Polarization table
#include <casa/Arrays/Vector.h>

// global function to go from aips++ -> j2ms
// Currently only supports combinations of linear and circular polarizations
// [RR,RL,LR,LL] and [XX,XY,YX,YY].
// May return an empty CorrelationCode if you pass it an unknown
// StokesTypes thingy (like Stokes::I, or Stokes::PP ...)
CorrelationCode aipspppol2j2mspol( casa::Stokes::StokesTypes aipspp );

// translate a Vector in one go
correlations_t aipspppol2j2mspol( const casa::Vector<casa::Int>& corrtypes );
#endif
