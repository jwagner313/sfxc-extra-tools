//  VEXperiment - a specialization from Experiment; reads it's values
//  from a VEX-file
//
//
//  Author:   Harro Verkouter,   05-07-1999
//
//
//   $Id: VEXperiment.h,v 1.7 2007/03/15 16:35:43 jive_cc Exp $
//
//
//   $Log: VEXperiment.h,v $
//   Revision 1.7  2007/03/15 16:35:43  jive_cc
//   VisibilityBuffer.cc
//
//   Revision 1.6  2007/02/26 15:36:59  jive_cc
//   HV: - cosmetic changes
//       - removed "has*DB()" methods - an experiment just HAS
//         to have those DBs
//
//   Revision 1.5  2006/01/13 11:35:40  verkout
//   HV: CASAfied version of the implement
//
//   Revision 1.4  2002/08/06 06:47:51  verkout
//   HV: * Built in support for passing options to the experiment from j2ms2
//   * In lieu of Huib's streamlining thingy, built in support to specify which toplevel VEXFile to use. Defaults to basename($CWD).vix.
//
//   Revision 1.3  2001/07/09 07:53:05  verkout
//   HV: Shitload of mods:
//   - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
//   - Enabled passing of options from j2ms2 commandline to an experiment
//   - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
//   - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
//   - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
//   - Enabled visibility filtering. At the moment only a source filter is implemented
//
//   Revision 1.2  2001/05/30 11:48:27  verkout
//   HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//   Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//   HV: imported aips++ implement stuff
//
//
#ifndef VEXPERIMENT_H
#define VEXPERIMENT_H


#include <jive/j2ms/Experiment.h>
#include <jive/j2ms/Visibility.h>
#include <casa/BasicSL/String.h>
#include <casa/Arrays/Vector.h>

#include <jive/j2ms/VEXSourceDB.h>
#include <jive/j2ms/VEXAntennaDB.h>
#include <jive/j2ms/VEXFrequencyDB.h>

#include <prep_job/vexplus.h>


class VEXperiment :
    public Experiment
{
public:
    //  Create a VEXperiment. (obviously) has defaults for everything...
    //  By default it will look at 'getcwd()', infer the name of it's
    //  parent directory and look for a vexfile named '<parentdir>.vix'
    //  in the current dir.
    //  Also, by default, no options are passed to the experiment.
    VEXperiment( const casa::String& opt="",
                 const casa::String& vexfile="",
                 const casa::String& exprootdir="" );
    
    //  Provide access to the properties
    virtual const casa::String&    getCode( void ) const;

    virtual const casa::String&    getMSname( void ) const;
    virtual void                   setMSname( const casa::String& newmsname );

    virtual Visibility::domain     getDestinationDomain( void ) const;
    virtual casa::Bool             setDestinationDomain( Visibility::domain newoutputdomain );

    virtual const casa::Vector<casa::String>&  getAntennas( void ) const;
    virtual const casa::Vector<casa::String>&  getSources( void ) const;

    // The experiment must have its own frequency DB!
    virtual const FrequencyDB&     getFrequencyDB( void ) const;

    // Source & Antenna DBs are non optional anymore
    virtual const AntennaDB&       getAntennaDB( void ) const;
    virtual const SourceDB&        getSourceDB( void ) const;

    //  VisibilityBuffer's are a part of the experiment, therefore we should ask
    //  the Experiment if it would be so kind as to create the
    //  indicated buffer. If it fails to create the buffer, a null-pointer
    //  is returned, otherwise a pointer to the newly created VisibilityBuffer is
    //  returned! 
    //
    //  NOTE: the caller is responsible for deleting the
    //  resources!!!!!!
    virtual VisibilityBuffer*      getVisibilityBuffer( const casa::String& visbufname );

    // Return the list of scans defined in our vexfile!
    virtual const j2ms::scaninfolist_t& getScanlist( void ) const;

    //  Tells us whether or not the Vernier delay was applied.
    virtual casa::Bool             hasVernierDelay( void ) const;
    
    //  Delete the resources allocated by this object
    virtual ~VEXperiment();
    
private:
    //  The VEXperiment's private parts
    casa::String                myCode;
    casa::String                myMSname;
    VexPlus*                    myVexFileptr;
    casa::Vector<casa::String>  myAntennas;
    casa::Vector<casa::String>  mySources;
    
    //  databases
    VEXSourceDB*                mySourceDBptr;
    VEXAntennaDB*               myAntennaDBptr;
    VEXFrequencyDB*             myFrequencyDBptr;
    j2ms::scaninfolist_t        myScanList;

    //  In what domain should the data be written?
    Visibility::domain          myDestinationDomain;
    

    //  Static functions; merely helper stuff. They compile a vector
    //  of strings of the indicated type from the VexFile that you
    //  pass, based on the contents of that file.
    //
    //  NOTE: These functions return a reference to a static Vector
    //  object whose contents may change from function-call to
    //  function-call. Therefore, use this return value as input for
    //  an assignment or copy to your local data-area to ensure proper
    //  operation of your program.
    static const casa::Vector<casa::String>& getAntennaList( const VexPlus& vpref );
    static const casa::Vector<casa::String>& getSourceList( const VexPlus& vpref );    

    //  It's safest to dis-allow these...
    VEXperiment( const VEXperiment& );
    VEXperiment& operator=( const VEXperiment& );
};




#endif
