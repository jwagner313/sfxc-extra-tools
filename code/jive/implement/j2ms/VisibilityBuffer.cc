// implementation of the non-virtual VisibilityBuffer class methods
//
// $Id: VisibilityBuffer.cc,v 1.6 2007/05/25 09:42:41 jive_cc Exp $
//
// $Log: VisibilityBuffer.cc,v $
// Revision 1.6  2007/05/25 09:42:41  jive_cc
// HV: - Added interface for requesting which processing steps have been
//     done for the given visibility buffer. Default returns empty
//     list of processing steps.
//
// Revision 1.5  2007/03/15 16:35:43  jive_cc
// VisibilityBuffer.cc
//
// Revision 1.4  2007/02/26 15:45:09  jive_cc
// HV: - cosmetic changes
//     - interface change: a visibilitybuffer may now
//       also generate modelrecords
//
// Revision 1.3  2006/08/17 13:41:14  verkout
// HV: * commentchanges * Added interface function to which should return the full list of scans that may be found in the VisBuf
//
// Revision 1.2  2006/03/02 14:21:40  verkout
// HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
// Revision 1.1  2006/02/14 13:32:49  verkout
// HV: Really dunno why this'un wasn't there before. Anyway, it contains code which is common to all VisibilityBuffers
//
//
#include <jive/j2ms/VisibilityBuffer.h>
#include <jive/j2ms/Experiment.h>

#include <casa/Exceptions.h>

//  Visibility buffers must be attached to an Experiment
VisibilityBuffer::VisibilityBuffer( const Experiment* experiment ) :
	itsScanptr( 0 ),
	itsExperimentptr( experiment )
{
	//  If no experiment -> we give up!
	if( !itsExperimentptr )
		throw( casa::AipsError("VisibilityBuffer: No experiment given!") );
}


//  Can do this because we MUST have an experiment
const Experiment&  VisibilityBuffer::getExperiment( void ) const {
	return *itsExperimentptr;
}
    

//  Tell the current scan (if any)
const j2ms::Scan*  VisibilityBuffer::getScanptr( void ) const {
	return itsScanptr;
}

//  Tells us about vernier delay
casa::Bool VisibilityBuffer::getVernierDelay( void ) const {
	return itsExperimentptr->hasVernierDelay();
}

// since the default implementation of getStationScanlist ignores it's
// argument, we just don't name it so the compiler won't complain about
// it not being used.
const j2ms::stationscanlist_t& VisibilityBuffer::getStationScanlist( int ) const {
    static const j2ms::stationscanlist_t   empty;

    return empty;
}

// HV: Changed. Rather than returning an empty one, return the list
// gotten from the experiment
const j2ms::scaninfolist_t& VisibilityBuffer::getScanlist( void ) const {
    return itsExperimentptr->getScanlist();
}

const j2ms::modeldata_t& VisibilityBuffer::getModelData( void ) const {
    static const j2ms::modeldata_t     empty;
    return empty;
}

processing_steps VisibilityBuffer::getProcessingSteps( void ) const {
    static const processing_steps     empty;
    return empty;
}

VisibilityBuffer::~VisibilityBuffer() {}
