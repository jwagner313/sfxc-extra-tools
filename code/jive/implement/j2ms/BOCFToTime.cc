//
//  Implementation of the BOCF->time class
//
//  Author: Harro Verkouter, 11-03-1999
//
//  $Id: BOCFToTime.cc,v 1.6 2006/03/02 14:21:38 verkout Exp $
//
#include <jive/j2ms/BOCFToTime.h>
#include <jive/Utilities/jexcept.h>
#include <casa/Quanta.h>

#include <iostream>

using std::endl;
using std::cout;
using namespace casa;

BOCFToTime::BOCFToTime() :
    itsBOCFZeroPoint( MEpoch( Quantity( 0.0, "s"), MEpoch::UTC ) ),
    itsBOCFRate( 1.0 )
{
}

BOCFToTime::BOCFToTime( const BOCFToTime& other ) :
    itsBOCFZeroPoint( other.itsBOCFZeroPoint ),
    itsBOCFRate( other.itsBOCFRate )
{
}



BOCFToTime::BOCFToTime( const MEpoch& approxstart, Double bocfrate ) :
    itsBOCFRate( bocfrate )
{
    //
    //  Check if we do have a valid bocf rate...
    //
    if( itsBOCFRate==0.0 )
    {
    	THROW_JEXCEPT("BOCF rate of zero not supported...");
    }
    

    //
    //  Ok. Find out, from the approx. start of scan what the
    //  zeropoint of bocf should be.... it should be the last instance
    //  of time that was a multiple of 10min (or 600s)
    //
    Double       kut;
    Quantity     tmp;
    unsigned int tenminuteints;
    
    kut           = approxstart.get("s").getValue();
    tenminuteints = (unsigned int)( kut/600.0 );
    tmp           = Quantity( (Double)tenminuteints*600.0, "s" );
    
    itsBOCFZeroPoint = MEpoch( tmp, approxstart.getRef() );
}

//
//  Assignment....
//
BOCFToTime& BOCFToTime::operator=( const BOCFToTime& other )
{
    if( this!=&other )
    {
    	itsBOCFZeroPoint = other.itsBOCFZeroPoint;
    	itsBOCFRate      = other.itsBOCFRate;
    }
    return *this;
}


unsigned int BOCFToTime::getMaxBOCFnr( void ) const
{
    return (unsigned int)(600.0*itsBOCFRate) - 1;
}

Double BOCFToTime::getBOCFRate( void ) const
{
    return itsBOCFRate;
}

const MEpoch& BOCFToTime::operator()( Double anybocf ) const
{
    Quantity       secs;
    static MEpoch  retval;

    secs  = Quantity( itsBOCFZeroPoint.get("s") );
    secs += Quantity( anybocf/itsBOCFRate, "s" );
    
    retval = MEpoch( secs, itsBOCFZeroPoint.getRef() );
    
    return retval;
}

void BOCFToTime::advanceZeroPoint( void )
{
    Double     tmp;
    Quantity   ntime;

    tmp = itsBOCFZeroPoint.get("s").getValue();
    tmp += 600.0;
    
    ntime = Quantity( tmp, "s" );
    itsBOCFZeroPoint = MEpoch( ntime, itsBOCFZeroPoint.getRef() );
    return;
}

void BOCFToTime::rewindZeroPoint( void )
{
    Double     tmp;
    Quantity   ntime;

    tmp = itsBOCFZeroPoint.get("s").getValue();
    tmp -= 600.0;
    
    ntime = Quantity( tmp, "s" );
    itsBOCFZeroPoint = MEpoch( ntime, itsBOCFZeroPoint.getRef() );
    return;
}

void BOCFToTime::adjust( Int nrintervals )
{
    //  Do clock adjustments
    while( nrintervals )
    {
    	if( nrintervals<0 )
    	{
    	    cout << "** Setting BOCF zeropoint back" << endl;
	    
    	    this->rewindZeroPoint();
    	    nrintervals++;
    	}
    	else
    	{
    	    cout << "** Advancing BOCF zeropoint" << endl;
    	    this->advanceZeroPoint();
    	    nrintervals--;
    	}
    }
    return;
}



BOCFToTime::~BOCFToTime()
{
    //
    //  Nothing to do yet....
    //
}
