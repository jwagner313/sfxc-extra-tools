//
//          Implementation of DayConversion 
//
//  Author:  Harro Verkouter, 15-06-1999
//
//  $Id: DayConversion.cc,v 1.6 2012/07/04 11:44:09 jive_cc Exp $
//

#include <jive/j2ms/DayConversion.h>


//
//  Note: month 0 is January!
//    
const unsigned int _DayConversion::secondsPerDay          = 24*60*60;
const unsigned int _DayConversion::daysPerMonth[ 12 ]     = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
const unsigned int _DayConversion::daysPerMonthLeap[ 12 ] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

_DayConversion::_DayConversion()
{
}

// year may be negative (BC!)
bool _DayConversion::dayNrToMonthDay( unsigned int& month, unsigned int& day,
                                      unsigned int daynr, int year )
{
    bool                 retval( false );
    unsigned int         monthcnt;
    unsigned int         cumulativeday;
    const unsigned int*  daysPerMonthptr( _DayConversion::daysPerMonth );
   
    // init default/error return values
    month = (unsigned int)-1;
    day   = (unsigned int)-1;

    // see what we can make of it
    if( _DayConversion::isLeapYear(year) )
    {
    	daysPerMonthptr = _DayConversion::daysPerMonthLeap;
    }
    
    for( monthcnt=0, cumulativeday=0;
         cumulativeday<daynr && monthcnt<12;
    	 (cumulativeday+=daysPerMonthptr[monthcnt++]) )
    {
    	if( (cumulativeday+daysPerMonthptr[monthcnt])>=daynr )
    	{
    	    break;
    	}
    }
    
    if( monthcnt<12 )
    {
    	retval = true;
	
    	month  = monthcnt;
    	day    = (daynr-cumulativeday);
    }
    return retval;
}

// year could be negative...
bool _DayConversion::dayMonthDayToNr( unsigned int& daynr, unsigned int month,
                                      unsigned int day, int year )
{
    unsigned int         monthcnt;
    const unsigned int*  daysPerMonthptr( _DayConversion::daysPerMonth );
    
    if( _DayConversion::isLeapYear(year) )
    {
    	daysPerMonthptr = _DayConversion::daysPerMonthLeap;
    }
    
    for( monthcnt=0, daynr=day;
         monthcnt<month && monthcnt<12;
         daynr+=daysPerMonthptr[monthcnt++] );
   
    if (monthcnt>11 )
    {
	daynr=(unsigned int)-1;
    }

    return (daynr!=(unsigned int)-1);
}


bool _DayConversion::isLeapYear( int year )
{
    //  Year is leap year if:
    //
    //  1) It's divisible by 4
    //  2) but not if it's divisible by 100 (centuries)
    //  2) but then again it is if it is divisible by 400
    //  
    return ( ((year%4)==0) && (!((year%100)==0) || ((year%400)==0)) );
}


_DayConversion::~_DayConversion()
{
}

