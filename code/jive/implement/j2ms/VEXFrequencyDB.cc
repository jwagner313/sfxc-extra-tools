//  Implementation oif the VEXFrequencyDB - class
//
//  Author: Harro Verkouter, 6-7-1999
//
//  $Id: VEXFrequencyDB.cc,v 1.10 2007/02/26 15:02:18 jive_cc Exp $
//
//  $Log: VEXFrequencyDB.cc,v $
//  Revision 1.10  2007/02/26 15:02:18  jive_cc
//  HV: - adher to new interface functions
//      - cosmetic changes
//      - throw if something's fishy since fishyness at
//        this level is unacceptable
//
//  Revision 1.9  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.8  2006/02/10 08:53:44  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.7  2006/01/13 11:35:40  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.6  2004/01/05 15:02:15  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.5  2003/09/12 07:27:34  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.4  2003/02/14 15:41:52  verkout
//  HV: vex++ now uses std::string. Have to change to using std::string as well.
//
//  Revision 1.3  2002/08/06 06:44:40  verkout
//  HV: The vexchannels are now read in and saved in the FrequencyDB
//
//  Revision 1.2  2001/05/30 11:48:18  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:35  verkout
//  HV: imported aips++ implement stuff
//
//
//
#include <jive/j2ms/VEXFrequencyDB.h>
#include <prep_job/vexplus.h>
#include <jive/j2ms/FrequencyConfig.h>
#include <jive/j2ms/BaseBand.h>
#include <jive/Utilities/jexcept.h>

#include <iostream>
#include <iomanip>
#include <ctype.h>
#include <string>
// for find, set_union
#include <algorithm>
// for inserter<>
#include <iterator>

using namespace std;
using namespace casa;


struct modefinder_t {
    public:
        modefinder_t( const string& m ):
            __mode2look4(m)
        {}

        bool operator()( const FrequencyConfig& fcref ) const {
            return (fcref.getCode()==__mode2look4);
        }

    private:
        casa::String __mode2look4;

        modefinder_t();
};
struct fqgrpfinder_t {
    public:
        fqgrpfinder_t( int fqgrp ):
            __grp2look4( fqgrp )
        {}

        bool operator()( const FrequencyConfig& fcref ) const {
            return (fcref.getFreqGroupNr()==__grp2look4);
        }

    private:
        int __grp2look4;

        fqgrpfinder_t();
};


#define find_fc_by_mode(a,b) \
    find_if(a.begin(), a.end(), modefinder_t(b))

#define find_fc_by_fqgrp(a,b) \
    find_if(a.begin(), a.end(), fqgrpfinder_t(b))

VEXFrequencyDB::VEXFrequencyDB( const String& vexfilename ) :
    myVexFileName( vexfilename )
{
    VexPlus     vexfile( myVexFileName );
    static bool filenameShown( false );    

    filenameShown = false;
   
    if( vexfile.parseVex()!=0 )
        THROW_JEXCEPT("Failed to parse vexfile " << myVexFileName);
    
    // Attempt to parse and read into mem!
    String           mode;
    BaseBand         bb;
    std::string      firststation;
    FrequencyConfig  fc;
	
    if( vexfile.N_Stations()>=1 )
        firststation = vexfile.Station( 0 );
	
    for( int i=0; firststation.size()>0 && i<vexfile.N_Modes(); i++ ) {
        int                   bbcnt;
        j2ms::polarizations_t recpols;

        mode = vexfile.Mode( i );
	
        if( mode.empty() ) {
            if( !filenameShown ) {
					cout << "Error in file " << myVexFileName << "\n";
					filenameShown = true;
		    }
		    cout << "No modename for mode #" << setw(2) << i << "\n" << flush;
            continue;
        }

        fc = FrequencyConfig(mode, i);
	    
        //  Ok. Fill all basebands....
        for( bbcnt=0; bbcnt<vexfile.N_FreqChans(firststation, mode); bbcnt++ ) {
		    Double             frequency;
            Double             bandwidth;
            std::string        sidebandtxt;
            std::string        polarization;
            j2ms::polarization j2pol;
            BaseBand::SideBand sideband;

            sidebandtxt  = vexfile.SideBand( firststation, mode, bbcnt );
            polarization = vexfile.Pol_by_Freq( firststation, mode, bbcnt );
            frequency    = vexfile.SkyFreq( firststation, mode, bbcnt )*1.0E6; 
            bandwidth    = vexfile.BW( firststation, mode, bbcnt );
            
            if( sidebandtxt.empty() || polarization.empty() ) {
                if( !filenameShown ) {
                    cout << "Error in file " << myVexFileName << "\n";
                    filenameShown = true;
                }

                //  Error detected...
                cout << "Error in mode " << mode << ": chan_def #" << setw(2)
                     << bbcnt << "\n";
                if( sidebandtxt.empty() )
                    cout << "> Missing sideband information\n";
                if( polarization.empty() )
                    cout << "> Missing polarization information\n";
                cout << flush;
                // no use in going on
                break;
            }
		
            // go from string/char -> enumerated values
            sideband = BaseBand::stringToSideBand( sidebandtxt );
            j2pol    = j2ms::char2polcode( polarization[0] );
		
            if( sideband==BaseBand::lower )
                frequency -= bandwidth;
		
            //  Check if we already have a baseband with the same
            //  frequency/bandwidth/sideband; it could be the
            //  other hand of polarization....
            //  [note: bbcnt is misnamed, it really is 'vexchannel']
            uInt      cnt;
            BaseBand  tmp(frequency, bandwidth, sideband, 
                          std::make_pair(bbcnt, j2pol) );
	
            for( cnt=0; cnt<fc.getNrBaseBands(); cnt++ ) {
                BaseBand*   bbptr;
                if( (bbptr=fc.getBaseBand(cnt))!=0 && tmp==*bbptr ) {
                // supposedly this is another vexchannel that goes into this
                // logical subband (baseband). As such, attempt to register
                // the current vexchannel with this BaseBand.
                if( !bbptr->registerVexChannel(std::make_pair(bbcnt, j2pol)) ) {
                    if( !filenameShown ) {
                        cout << "Error in file " << myVexFileName << "\n";
                        filenameShown = true;
                    }
                    cout << "VEXFrequencyDB - Too much vexchannels for "
                         << "subband?\n"
                         << "  mode=" << mode << "; current vexchannel="
                         << bbcnt << endl
                         << "  BaseBand=" << *bbptr << endl;
                    }
                    // not much sense in further processing of this config
                    break;
                }
            }
	        // if it wasn't the other hand of polarization we must
            // add a/this new baseband to the current frequency config
            if( cnt==fc.getNrBaseBands() ) 
                fc.addBaseBand( tmp );

            // make sure the current polarization is added to
            // the set of recorded polarizations
            recpols.insert( j2pol );
        }
        if( bbcnt!=vexfile.N_FreqChans(firststation, mode) ) {
            cout << "Error in file " << myVexFileName << "\n"
                 << "  Incomplete mode " << mode << "?"
                 << endl;
            continue;
        }
        fc.setRecordedPols( recpols );

        //  And add it to the list
        myConfigurations.push_back( fc );
    } // done processing this mode

    // Now loop over all frequency configs and find
    // the union of all recorded polarizations
    configurations_t::const_iterator   curfc;

    for( curfc=myConfigurations.begin();
         curfc!=myConfigurations.end();
         curfc++ ) {
        const j2ms::polarizations_t&  rps( curfc->getRecordedPols() );
       
        // this computes the union of the already known recorded pols 
        // (myRecordedPolarizations) and the recorded polarizations of
        // the current frequencyconfig and stuffs the result in
        // myRecordedPolarizations
        set_union(myRecordedPolarizations.begin(), myRecordedPolarizations.end(),
                  rps.begin(), rps.end(),
                  inserter(myRecordedPolarizations, myRecordedPolarizations.begin()));
    }
}


FrequencyConfig VEXFrequencyDB::operator[]( const String& key ) const {
    FrequencyConfig                  retval;
    configurations_t::const_iterator curfc;
   
    curfc = find_fc_by_mode(myConfigurations, key);
    if( curfc!=myConfigurations.end() )
        retval = *curfc;
    return retval;
}


Bool VEXFrequencyDB::searchFrequencyConfig( FrequencyConfig& fcref, const String& key ) const {
    configurations_t::const_iterator curfc;
    
    curfc = find_fc_by_mode(myConfigurations, key);
    if( curfc!=myConfigurations.end() )
        fcref = *curfc;
    return (curfc!=myConfigurations.end());
}

Bool VEXFrequencyDB::searchFrequencyConfig( FrequencyConfig& fcref, Int key ) const {
    configurations_t::const_iterator curfc;
    
    curfc = find_fc_by_fqgrp(myConfigurations, key);
    if( curfc!=myConfigurations.end() )
        fcref = *curfc;
    return (curfc!=myConfigurations.end());
}

uInt VEXFrequencyDB::nEntries( void ) const {
    return myConfigurations.size();
}
FrequencyConfig VEXFrequencyDB::operator[]( uInt index ) const {
    FrequencyConfig  rv;
    if( index<myConfigurations.size() )
        rv = myConfigurations[index];
    return rv;
}

const j2ms::polarizations_t& VEXFrequencyDB::recordedPolarizations( void ) const {
    return myRecordedPolarizations;
}

//  Clean up...
VEXFrequencyDB::~VEXFrequencyDB()
{}
