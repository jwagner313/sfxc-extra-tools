//
//  Implementation of the baseband class
//
//  Author:    Harro Verkouter, 2-12-1998
//
//  $Id: BaseBand.cc,v 1.9 2007/02/26 10:45:03 jive_cc Exp $
//
#include <jive/j2ms/BaseBand.h>


#include <casa/Exceptions.h>
#include <jive/Utilities/sciprint.h>
#include <jive/assertfail.h>

#include <cmath>

using std::pair;
using std::make_pair;
using std::vector;
using std::ostream;
using namespace casa;

// the static const 'nomapping' value, that is,
// the value that we take to represent the fact that
// it is an invalid mapping
const BaseBand::v2pmapentry BaseBand::nomapping = std::make_pair(-1, j2ms::unknown);



ostream& operator<<( ostream& os, const BaseBand& bbref )
{
    const BaseBand::vex2polmap&          vexchannels( bbref.getVexChannels() );
    BaseBand::vex2polmap::const_iterator curvexch;
    os << "#" << bbref.getBaseBandNr() << ": "
       << j2ms::sciprintd(bbref.getBandwidth(), "Hz") << ", from "
	   << j2ms::sciprintd(bbref.getStartfreq(), "Hz") << ", "
       << bbref.getSideBand() << " (VEX channels: ";
    
    for( curvexch=vexchannels.begin();
         curvexch!=vexchannels.end();
         curvexch++ ) {
		os << *curvexch;
	}
	os << ")";
    return os;
}

ostream& operator<<( ostream& os, const BaseBand::SideBand& sbref )
{
    switch( sbref )
    {
	case BaseBand::upper:
	    os << "USB";
	    break;
	    
	case BaseBand::lower:
	    os << "LSB";
	    break;
	    
	case BaseBand::none:
	    os << "<not set>";
	    break;
	    
	default:
	    os << "<INVALID SIDEBAND>";
	    break;
    }
    return os;
}


BaseBand::SideBand BaseBand::stringToSideBand( const String& sbtxt )
{
    BaseBand::SideBand   retval( BaseBand::none );
    
    if( sbtxt=="USB" || sbtxt=="U" || sbtxt=="u" ) {
		retval = BaseBand::upper;
    } else if( sbtxt=="LSB" || sbtxt=="L" || sbtxt=="l" ) {
		retval = BaseBand::lower;
    }
    return retval;
}



BaseBand::BaseBand() :
    itsStartfreq( 0.0 ),
    itsBandwidth( 0.0 ),
    itsSideBand( BaseBand::none ),
    itsBaseBandnr( (uInt)-1 ),
    itsParentConfigurationptr( 0 )
{
}

BaseBand::BaseBand( const BaseBand& other ) :
    itsVex2PolMap( other.itsVex2PolMap ),
    itsStartfreq( other.itsStartfreq ),
    itsBandwidth( other.itsBandwidth ),
    itsSideBand( other.itsSideBand ),
    itsBaseBandnr( other.itsBaseBandnr ),
    itsParentConfigurationptr( other.itsParentConfigurationptr )
{
}

BaseBand::BaseBand( Double startfreq, Double bandwidth, BaseBand::SideBand sideband,
				    const v2pmapentry& v0top0, const v2pmapentry& v1top1 ) :
    itsStartfreq( startfreq ),
    itsBandwidth( bandwidth ),
    itsSideBand( sideband ),
    itsBaseBandnr( (uInt)-1 ),
    itsParentConfigurationptr( 0 )
{
    int                              ninsert;
    int                              nsuccess;
    pair<vex2polmap::iterator,bool>  insres;

    ninsert = nsuccess = 0;
    if( v0top0!=BaseBand::nomapping ) {
        ninsert++;
        insres = itsVex2PolMap.insert( v0top0 );
        if( insres.second )
            nsuccess++;
    }
    if( v1top1!=BaseBand::nomapping ) {
        ninsert++;
        insres = itsVex2PolMap.insert( v1top1 );
        if( insres.second )
            nsuccess++;
    }
    if( ninsert!=nsuccess )
        ASSERTFAIL("Failed to insert (at least one) " <<
                   "vex->polarization map entry:\n" << v0top0 << " or " <<
                   v1top1);
}

//
//  The protected c'tor -> Can only be used by FrequencyConfig-class
//  because that class has all the info needed to fill in these
//  details!
//
BaseBand::BaseBand( const FrequencyConfig* ptr2parent, uInt basebandnr, const BaseBand& bbdata ) :
    itsVex2PolMap( bbdata.itsVex2PolMap ),
    itsStartfreq( bbdata.itsStartfreq ),
    itsBandwidth( bbdata.itsBandwidth ),
    itsSideBand( bbdata.itsSideBand ),
    itsBaseBandnr( basebandnr ),
    itsParentConfigurationptr( ptr2parent )
{
}

const BaseBand& BaseBand::operator=( const BaseBand& other )
{
    if( this!=&other ) {
        itsVex2PolMap             = other.itsVex2PolMap;
		itsStartfreq              = other.itsStartfreq;
		itsBandwidth              = other.itsBandwidth;
		itsSideBand               = other.itsSideBand;
		itsBaseBandnr             = other.itsBaseBandnr;
		itsParentConfigurationptr = other.itsParentConfigurationptr;
    }
    return *this;
}

const BaseBand::vex2polmap& BaseBand::getVexChannels( void ) const {
	return itsVex2PolMap;
}

bool BaseBand::registerVexChannel( const BaseBand::v2pmapentry& v2p ) {
    pair<vex2polmap::iterator,bool> insres;

    insres = itsVex2PolMap.insert( v2p );
    return insres.second;
}

uInt BaseBand::getBaseBandNr( void ) const {
	return itsBaseBandnr;
}

Double BaseBand::getStartfreq( void ) const {
    return itsStartfreq;
}

Double BaseBand::getBandwidth( void ) const {
    return itsBandwidth;
}

BaseBand::SideBand BaseBand::getSideBand( void ) const {
    return itsSideBand;
}

Bool BaseBand::hasConfiguration( void ) const {
    return ( itsParentConfigurationptr!=0 );
}

const FrequencyConfig& BaseBand::getConfiguration( void ) const {
    if( !itsParentConfigurationptr )
	    throw( AipsError("BaseBand - attempt to get parent whilst no parent configured!") );
    return *itsParentConfigurationptr;
}

bool BaseBand::operator==( const BaseBand& other ) {
    // our frequency units are in Hz
    // so we compare frequencies equal
    // if they match up to ~ 10-6Hz,
    // microhertz... 
    const double tolerance( 1.0E-6 );
    
    bool     freqsIdentical( ::fabs(itsStartfreq-other.itsStartfreq)<=tolerance );
    bool     bwIdentical( ::fabs(itsBandwidth-other.itsBandwidth)<=tolerance );
    bool     sbIdentical( (itsSideBand==other.itsSideBand) );
    
    return (freqsIdentical && bwIdentical && sbIdentical);
}



BaseBand::~BaseBand()
{
}


// Global operators
bool operator==(const BaseBand::v2pmapentry& l, const BaseBand::v2pmapentry& r) {
    return (l.first==r.first && l.second==r.second);
}

bool operator!=(const BaseBand::v2pmapentry& l, const BaseBand::v2pmapentry& r) {
    return !(l==r);
}

ostream& operator<<(ostream& os, const BaseBand::v2pmapentry& v2p) {
    return os << "<" << v2p.first << ":" << v2p.second << ">";
}
