//
//  COFScan - A specialization of Scan, properly describing a scan
//  saved in the Correlator Output Format.
//
//
//  Author:   Harro Verkouter,  2-6-1999
//
//  $Id: COFScan.h,v 1.12 2011/10/12 12:45:28 jive_cc Exp $
//
//  $Log: COFScan.h,v $
//  Revision 1.12  2011/10/12 12:45:28  jive_cc
//  HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//        changed into 'labels' for more genericity
//      * can now be built either as part of AIPS++/CASA [using makefile]
//        or can be built as standalone set of libs + binaries and linking
//        against casacore [using Makefile]
//        check 'code/
//
//  Revision 1.11  2008-03-25 13:30:11  jive_cc
//  kettenis: Implement conversion of the new SFXC software correlator output
//  format.  Disables the old FABRIC code.
//
//  Revision 1.10  2007/03/15 16:29:18  jive_cc
//  HV: Support for FABRIC VisibilityBuffers
//
//  Revision 1.9  2007/02/26 11:01:36  jive_cc
//  HV: Changed output. Removed superfluous comment.
//
//  Revision 1.8  2006/02/10 08:53:43  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.7  2006/01/13 11:35:36  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.6  2005/03/15 14:39:12  verkout
//  HV: include statements cleaned up
//
//  Revision 1.5  2004/08/25 05:57:37  verkout
//  HV: * Got rid of 'uint16' (replaced with 'unsigned int').
//        (not everywhere yet)
//      * Started using CountedPointers to DataFileRecords in
//        COF* classes (such that datafilerecords, read from file
//        get automagically deleted when nobody refers to them
//        anymore
//      * Added some exception catching here and there...
//
//  Revision 1.4  2003/09/12 07:26:37  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.3  2001/07/09 07:52:26  verkout
//  HV: Shitload of mods:
//  - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
//  - Enabled passing of options from j2ms2 commandline to an experiment
//  - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
//  - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
//  - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
//  - Enabled visibility filtering. At the moment only a source filter is implemented
//
//  Revision 1.2  2001/05/30 11:47:02  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:35  verkout
//  HV: imported aips++ implement stuff
//
//
#ifndef COFSCAN_H
#define COFSCAN_H


//  This is our base-clas...
#include <jive/j2ms/Scan.h>
//#include <jive/j2ms/CorrelatorSetup.h>


#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <measures/Measures/MEpoch.h>

#include <iostream>


//  Forward declarations
#ifdef MARK4
class COFVisibilityBuffer;
#endif

class SFXCVisBuffer;

#ifdef PCINT
class PCIntVisBuffer;
#endif

namespace j2ms
{

    class COFScan :
        public j2ms::Scan
    {
        //  Only the COF- and PCInt VisBuffer classes can
        //  create valid COFScans
#ifdef MARK4
        friend class ::COFVisibilityBuffer;
#endif
        friend class ::SFXCVisBuffer;
#ifdef PCINT
        friend class ::PCIntVisBuffer;
#endif 
    public:
        //  These methods are inherited from the base-class; we MUST implement them!!
    	virtual casa::Int               getScanNo( void ) const;
        virtual const casa::String&     getScanId( void ) const;
        virtual const casa::String&     getSourceCode( void ) const;
        virtual const casa::String&     getFrequencyCode( void ) const;
        virtual const casa::MEpoch&     getStartTime( void ) const;
        virtual const casa::MEpoch&     getEndTime( void ) const;

        //  Delete all the resources that were allocated by this object
        virtual ~COFScan();
    
    protected:
        // The COF/PCInt VisibilityBuffers will create fully defined COFScans
        COFScan( const VisibilityBuffer* visbufptr, const ScanInfo& si );

    private:
        // The COFScan's private parts
    	casa::Int             myScanNo;
        casa::String          myScanID;
        casa::String          mySourceCode;
        casa::String          myFrequencyCode;
        casa::MEpoch          myStartTime;
        casa::MEpoch          myEndTime;
    
        //  Private methods

        //  These are undefined and therefore we prohibit use of them
        COFScan();
        COFScan( const COFScan& );
        const COFScan& operator=( const COFScan& );
    };

    //  Show on a stream in HRF
    std::ostream& operator<<( std::ostream& os, const COFScan& csref );

};

#endif
