//  VEXSourceDB - Specialization of the SourceDB class that reads
//  the antenna info from a VEX-file
//
//  Author: Harro Verkouter,   6-7-1999
//
//  $Id: VEXSourceDB.h,v 1.6 2007/02/26 15:04:36 jive_cc Exp $
//
//  $Log: VEXSourceDB.h,v $
//  Revision 1.6  2007/02/26 15:04:36  jive_cc
//  HV: - enable lookup via Id
//      - cosmetic changes
//      - throws if something fishy as fishyness at this level
//        is unacceptable
//
//  Revision 1.5  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.4  2006/01/13 11:35:40  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.3  2004/01/05 15:02:22  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.2  2001/05/30 11:48:24  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//  HV: imported aips++ implement stuff
//
//
#ifndef JIVE_J2MS_VEXSOURCEDB_H
#define JIVE_J2MS_VEXSOURCEDB_H

#include <casa/aips.h>
#include <jive/j2ms/SourceDB.h>
#include <jive/j2ms/SourceInfo.h>

#include <map>
#include <string>


class VEXSourceDB :
    public SourceDB
{
public:
    //  Create from a file
    VEXSourceDB( const casa::String& vexfilename );
    
    //  Lookup something....
    virtual SourceInfo  operator[]( const casa::String& key ) const;
    virtual bool        searchSource( SourceInfo& airef, const casa::String& key ) const;
    virtual bool        searchSource( SourceInfo& airef, casa::Int id ) const;

    virtual size_t      nrSources( void ) const;

    //  Destruct the object:
    virtual ~VEXSourceDB();

private:
    // store the sourceinfo keyed on sourcename...
    // and also keep a mapping of ID->sourcename
    typedef std::map<std::string,SourceInfo> sourcelist_t;
    typedef std::map<casa::Int, sourcelist_t::const_iterator> idmap_t;

    idmap_t        myIdMap;
    sourcelist_t   mySourceList;

    // Prohibit these:
    VEXSourceDB();
    VEXSourceDB( const VEXSourceDB& );
    const VEXSourceDB& operator=( const VEXSourceDB& );
};


#endif
