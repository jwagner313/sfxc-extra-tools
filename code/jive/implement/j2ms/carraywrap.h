// define a parametrized wrapper around C-arrays so's we can
// support assignment [we change 'm into a struct]
// JCCS has this too but we do not want entanglement between j2ms & jccs, or at least,
// not for code that shouldn't need jccs
#ifndef JIVE_J2MS_CARRAYWRAP_H
#define JIVE_J2MS_CARRAYWRAP_H

namespace j2ms {
    template <typename T, size_t N>
    struct carraywrap {
        typedef T Type[N];
   
        // use ::nrElements to access size ...
        enum { nrElements = N };

        carraywrap( const T& t = T() ) {
            for( unsigned int i=0; i<N; ++i )
                data[i] = t;
        }
        // This will be one of the trickier bits...
        // the compiler will call this as (T*)
        // so we have no idea if the amount storage that
        // t points at is sufficiently large.. [ie: >= N]
        // Some holds for assignment
        carraywrap( Type t ) {
            ::memcpy(data, t, sizeof(Type));
        }
        carraywrap( const Type t ) {
            ::memcpy(data, t, sizeof(Type));
        }
        carraywrap( const carraywrap<T, N>& other ) {
            ::memcpy(data, other.data, sizeof(Type));
        }

        const carraywrap<T,N>& operator=( const carraywrap<T,N>& other ) {
            if( this!=&other )
                ::memcpy(data, other.data, sizeof(Type));
            return *this;
        }
   
        T& operator[](unsigned int n) {
            if( n<N )
                return data[n];
            throw std::exception("carraywrap/out-of-bounds adressing");
        }
        const T& operator[](unsigned int n) const {
            if( n<N )
                return data[n];
            throw std::exception("carraywrap/out-of-bounds adressing (const)");
        }

        // this is the plain-old-C array
        Type   data;
    };
} // end-of-namespace


#endif
