#include <jive/j2ms/SFXCVisBuffer.h>
#include <jive/j2ms/Experiment.h>
#include <jive/j2ms/COFScan.h>
#include <jive/j2ms/Scan.h>
#include <jive/j2ms/DayConversion.h>
#include <jive/labels/CorrelationCode.h>
#include <jive/Utilities/jexcept.h>

#include <casa/Quanta/MVTime.h>
#include <casa/Utilities/Regex.h>

#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>

#include <cmath>
#include <list>
#include <string>
#include <algorithm>

#include <jive/j2ms/sfxc/output_header.h>

bool operator<(const SFXCKey &a, const SFXCKey &b)
{
  if (a.antenna == b.antenna)
    {
      if (a.polarisation == b.polarisation)
	return a.subband < b.subband;
      else
	return a.polarisation < b.polarisation;
    }

  return a.antenna < b.antenna;
}

bool operator!=(const SFXCKey &a, const SFXCKey &b)
{
  return (a.antenna != b.antenna || a.polarisation != b.polarisation ||
	  a.subband != b.subband);
}

bool operator<(const SFXCBaselineKey &a, const SFXCBaselineKey &b)
{
  if (a.baseline.first != b.baseline.first)
    return a.baseline.first < b.baseline.first;

  if (a.baseline.second != b.baseline.second)
    return a.baseline.second < b.baseline.second;

  if (a.polarisation.first != b.polarisation.first)
    return a.polarisation.first < b.polarisation.first;

  if (a.polarisation.second != b.polarisation.second)
    return a.polarisation.second < b.polarisation.second;

  if (a.subband.first != b.subband.first)
    return a.subband.first < b.subband.first;

  return a.subband.second < b.subband.second;
}

SFXCVisBuffer::SFXCVisBuffer(const casa::String &vb,
			     const Experiment *exp,
			     const string& /*vbo*/) :
  VisibilityBuffer(exp),
  __rootdir(exp->getRootDir())
{
    // Transform the experiment's scanlist into a list of scanpointers
    const j2ms::scaninfolist_t& scanlist(exp->getScanlist());
    j2ms::scaninfolist_t::const_iterator scan;
    
    for (scan = scanlist.begin(); scan != scanlist.end(); scan++)
      __scanptrlist.push_back(new j2ms::COFScan(this, *scan));

    // Construct a list of all scans to fool the Ghostbuster filter.
    for (scan = scanlist.begin(); scan != scanlist.end(); scan++)
      stationscanlist.push_back(j2ms::stationscaninfo_t((*scan).scanStart, (*scan).scanEnd));

    char buf[PATH_MAX];
    if (::realpath(__rootdir.c_str(), buf) == NULL)
      THROW_JEXCEPT("Unresolvable path" << __rootdir);

    const char *name = ::basename(buf);
    if (name == NULL)
      THROW_JEXCEPT("Cannot get base portion of directory name " << __rootdir);

    string path = __rootdir + "/" + vb;
    fd = ::open(path.c_str(), O_RDONLY | O_LARGEFILE);
    if (fd == -1)
      THROW_JEXCEPT("Cannot open " << path);

    struct Output_header_global hdr;
    ssize_t nbytes = ::read(fd, &hdr, sizeof(hdr));
    if (nbytes != sizeof(hdr))
      THROW_JEXCEPT("Cannot read global haeder");

    if (hdr.header_size < (ssize_t)sizeof(hdr))
      THROW_JEXCEPT("Global header size invalid (is " << hdr.header_size
		    << " expected " << sizeof(hdr) << ")");
    if (hdr.header_size > (ssize_t)sizeof(hdr))
      ::lseek(fd, hdr.header_size, SEEK_SET);

    if (hdr.output_format_version != OUTPUT_FORMAT_VERSION)
      THROW_JEXCEPT("Global header version invalid (is " << hdr.output_format_version
		    << " expected " << OUTPUT_FORMAT_VERSION << ")");

    correlator_version = hdr.correlator_version;
    integration_time = hdr.integration_time * 1e-6;

    unsigned month, day;
    DayConversion::dayNrToMonthDay(month, day, hdr.start_day, hdr.start_year);
    start_year = hdr.start_year;
    start_month = month + 1;
    start_day = day;
    start_sec = hdr.start_time;
    casa::Double dayfraction = (start_sec + 0.5 * integration_time) / DayConversion::secondsPerDay;
    casa::MVEpoch start_time = casa::MVEpoch(casa::MVTime(hdr.start_year, month + 1, day, dayfraction));

    mepoch_in_scaninfo cond(start_time);
    scanptrlist_type::const_iterator scanptr;
    scanptr = find_scaninfo_by_time(__scanptrlist, cond);
    __mode = (*scanptr)->getFrequencyCode();

    createAntennaMapping();
    createSubbandMapping();

    baseline = baselines.end();
    subband = subbands.end();

    num_channels = hdr.number_channels;

    correlationset_t pols;
    switch (hdr.polarisation_type)
      {
      case Output_header_global::LEFT_POLARISATION:
	pols.insert(CorrelationCode(j2ms::l, j2ms::l));
	break;
      case Output_header_global::RIGHT_POLARISATION:
	pols.insert(CorrelationCode(j2ms::r, j2ms::r));
	break;
      case Output_header_global::LEFT_RIGHT_POLARISATION:
	pols.insert(CorrelationCode(j2ms::r, j2ms::r));
	pols.insert(CorrelationCode(j2ms::l, j2ms::l));
	break;
      case Output_header_global::LEFT_RIGHT_POLARISATION_WITH_CROSSES:
	pols.insert(CorrelationCode(j2ms::r, j2ms::r));
	pols.insert(CorrelationCode(j2ms::r, j2ms::l));
	pols.insert(CorrelationCode(j2ms::l, j2ms::r));
	pols.insert(CorrelationCode(j2ms::l, j2ms::l));
	break;
      default:
	THROW_JEXCEPT("Unknown polarisations in header");
    }

    j2ms::log2polmap l2p;
    switch (hdr.polarisation_type)
      {
      case Output_header_global::LEFT_POLARISATION:
	l2p.insert(std::make_pair(0, j2ms::l));
	break;
      case Output_header_global::RIGHT_POLARISATION:
	l2p.insert(std::make_pair(0, j2ms::r));
	break;
      default:
	l2p.insert(std::make_pair(0, j2ms::r));
	l2p.insert(std::make_pair(1, j2ms::l));
	break;
      }

    __setup = CorrelatorSetup(num_channels, pols, integration_time,
			      CorrelatorSetup::bypassVersion, l2p, -1);

    // Now we can pre-allocate the temporary storage.
    cplxbuf.resize(__setup.nrOfSpectralPoints());
    weight.resize(__setup.getCorrelations().size());
    data.resize(__setup.getCorrelations().size(),  __setup.nrOfSpectralPoints());
    cplxnullen = complexvector_type(__setup.nrOfSpectralPoints(), casa::Complex(0.0f, 0.0f));

    // Move forward to first visibility.
    next();
}

void
SFXCVisBuffer::createAntennaMapping(void)
{
  const Experiment& exp(getExperiment());
  const AntennaDB& adb(exp.getAntennaDB());
  std::vector<casa::String> antennas;
  std::vector<casa::String>::const_iterator antenna;
  AntennaInfo ai;

  // Convert from casa::Vector to std::vector.
  exp.getAntennas().tovector(antennas);

  // Sort antennas in alphabetical order.
  std::sort(antennas.begin(), antennas.end());

  for (antenna = antennas.begin(); antenna != antennas.end(); antenna++)
    {
      adb.searchAntenna(ai, *antenna);
      antenna_map.push_back(ai.getId());
    }
}

void
SFXCVisBuffer::createSubbandMapping(void)
{
  const Experiment& exp(getExperiment());
  const FrequencyDB& fdb(exp.getFrequencyDB());
  FrequencyConfig fc;

  fdb.searchFrequencyConfig(fc, __mode);
  const basebandlist_t& bbs(fc.getBaseBands());
  basebandlist_t::const_iterator bb;

  // Sort frequencies.
  std::set<casa::Double> freqs;
  for (bb = bbs.begin(); bb != bbs.end(); bb++) {
    if ((*bb).getSideBand() == BaseBand::lower)
      freqs.insert((*bb).getStartfreq() + (*bb).getBandwidth());
    else
      freqs.insert((*bb).getStartfreq());
  }

  // And build a mapping based on the ordered frequencies.
  std::set<casa::Double>::const_iterator freq;
  int frequency_nr = 0;
  for (freq = freqs.begin(); freq != freqs.end(); freq++)
    {
      for (bb = bbs.begin(); bb != bbs.end(); bb++)
	{
	  int sideband;
	  casa::Double frequency;

	  if ((*bb).getSideBand() == BaseBand::lower) {
	    sideband = 0;
	    frequency = (*bb).getStartfreq() + (*bb).getBandwidth();
	  } else {
	    sideband = 1;
	    frequency = (*bb).getStartfreq();
	  }

	  if (frequency == *freq)
	    {
	      std::pair<int, int> sb = std::make_pair(frequency_nr, sideband);
	      subband_map[sb] = (*bb).getBaseBandNr();
	    }
	}
      frequency_nr++;
    }
}

casa::Bool
SFXCVisBuffer::getCurrentVisibility(Visibility &v)
{
  if (subbands.empty() || baseline == baselines.end())
    return casa::False;

  if ((*baseline).first >= antenna_map.size()
      || (*baseline).second >= antenna_map.size())
    THROW_JEXCEPT("Unobserved station");
  if (subband_map.count(*subband) == 0)
    THROW_JEXCEPT("Unobserved subband");

  const correlationset_t &pols(__setup.getCorrelations());
  correlationset_t::const_iterator pol;

  bool swap = true;
  casa::Int ant1 = antenna_map[(*baseline).first];
  casa::Int ant2 = antenna_map[(*baseline).second];
  if (ant1 > ant2)
    {
      swap = false;
      ant1 = antenna_map[(*baseline).second];
      ant2 = antenna_map[(*baseline).first];
    }

  int i;
  for (pol = pols.begin(), i = 0; pol != pols.end(); pol++, i++)
    {
      std::vector<SFXCBaselineKey>::const_iterator key;

      std::pair<int, int> polarisation = std::make_pair((*pol)[0] == j2ms::l, (*pol)[1] == j2ms::l);
      if (!swap)
	polarisation = std::make_pair((*pol)[1] == j2ms::l, (*pol)[0] == j2ms::l);

      int j;
      for (key = keys.begin(), j = 0; key != keys.end(); key++, j++)
	{
	  if ((*key).baseline != (*baseline) || (*key).subband != (*subband))
	    continue;
	  
	  if ((*key).polarisation == polarisation)
	    break;
	}

      if (key == keys.end())
	{
	  data.row(i) = cplxnullen;
	  weight(i) = 0.0;
	  continue;
	}

      if ((*key).subband.second == 0)
	for (int channel = 0; channel < num_channels; channel++)
	  cplxbuf[num_channels - channel - 1] = casa::Complex(visibilities[j][channel * 2], -visibilities[j][channel * 2 + 1]);
      else
	for (int channel = 0; channel < num_channels; channel++)
	  cplxbuf[channel] = casa::Complex(visibilities[j][channel * 2], visibilities[j][channel * 2 + 1]);

      if (swap)
	{
	  for (int channel = 0; channel < num_channels; channel++)
	    cplxbuf[channel] = std::conj(cplxbuf[channel]);
	}

      SFXCKey key1, key2;
      key1.antenna = (*key).baseline.first;
      key1.polarisation = (*key).polarisation.first;
      key1.subband = (*key).subband;
      key2.antenna = (*key).baseline.second;
      key2.polarisation = (*key).polarisation.second;
      key2.subband = (*key).subband;

      if (key1 != key2) {
	// Cross-correlations need to have a quantization (Van Vleck)
	// correction applied.  The factor here is taken from equation
	// A8.13 in Thompson, Moran and Swenson, "Interferometry and
	// Synthesis in Radio Astronomy", 2nd Edition and assumes
	// 2-bit sampling and "ideal" sampler statistics.
	for (int channel = 0; channel < num_channels; channel++)
	  cplxbuf[channel] *= 1.1329552;
      }

      data.row(i) = cplxbuf;

      if (ant1 == ant2)
	weight(i) = weight_map[key1];
      else {
	// Correlator versions before SVN r1129 did not write out the
	// number of correlated valid samples, so we have to estimate
	// the baseline weight based on the weights of the
	// autocorrelations.  This assumes invalid samples in each
	// datastreams are uncorrelated.
	if (correlator_version < 1119)
	  weight(i) = weight_map[key1] * weight_map[key2];
	else
	  weight(i) = baseline_weight_map[(*key)];
      }
    }

  ExtVisLabel lab;

  lab.correlations.clear();
  for (pol = pols.begin(); pol != pols.end(); pol++)
    lab.correlations.push_back(*pol);
  lab.subbandnr = subband_map[*subband];
  lab.baseline = BaseLineIdx(ant1, ant2);

  if (uvw_map.count(ant1) == 0 || uvw_map.count(ant2) == 0)
    THROW_JEXCEPT("Missing uvw");

  casa::Muvw::Ref ref(casa::Muvw::J2000);
  casa::Muvw uvw = casa::Muvw(uvw_map[ant1] - uvw_map[ant2], ref);
  v = Visibility(lab, data, Visibility::frequency, casa::False, weight, slice_time, uvw, this);

  return casa::True;
}

void
SFXCVisBuffer::read_timeslice_header(void)
{
  struct Output_header_timeslice hdr;
  ssize_t nbytes = ::read(fd, &hdr, sizeof(hdr));
  if (nbytes != sizeof(hdr))
    {
      if (nbytes == 0)
	{
	  next_slice = -1;
	  return;
	}
      THROW_JEXCEPT("Cannot read timeslice header");
    }

  next_slice = hdr.integration_slice;
  next_baselines = hdr.number_baselines;
  next_uvws = hdr.number_uvw_coordinates;
  next_stats = hdr.number_statistics;
}

void
SFXCVisBuffer::read_uvws(void)
{
  for (int i = 0; i < next_uvws; i++)
    {
      struct Output_uvw_coordinates uvw;
      ssize_t nbytes = ::read(fd, &uvw, sizeof(uvw));
      if (nbytes != sizeof(uvw))
	THROW_JEXCEPT("Cannot read uvw");

      if ((unsigned int)uvw.station_nr >= antenna_map.size())
	THROW_JEXCEPT("Unobserved station");
      uvw_map[antenna_map[uvw.station_nr]] = casa::MVuvw(uvw.u, uvw.v, uvw.w);
    }
}

void
SFXCVisBuffer::read_stats(void)
{
  for (int i = 0; i < next_stats; i++)
    {
      struct Output_header_bitstatistics stats;
      ssize_t nbytes = ::read(fd, &stats, sizeof(stats));
      if (nbytes != sizeof(stats))
	THROW_JEXCEPT("Cannot read stats");

      int32_t nvalid = 0;
      for (int i = 0; i < 4; i++)
	nvalid += stats.levels[i];

      if ((unsigned int)stats.station_nr >= antenna_map.size())
	THROW_JEXCEPT("Unobserved station");
      SFXCKey key;
      key.antenna = stats.station_nr;
      key.polarisation = stats.polarisation;
      key.subband = std::make_pair(stats.frequency_nr, stats.sideband);
      ntotal_map[key] = nvalid + stats.n_invalid;
      weight_map[key] = (double)nvalid / ntotal_map[key];
    }
}

void
SFXCVisBuffer::read_baseline(void)
{
  struct Output_header_baseline hdr;
  ssize_t nbytes = ::read(fd, &hdr, sizeof(hdr));
  if (nbytes != sizeof(hdr))
    THROW_JEXCEPT("Cannot read baseline header");

  if (current_baseline >= visibilities.size())
      visibilities.push_back(new float[(num_channels + 1) * 2]);

  nbytes = ::read(fd, visibilities[current_baseline], (num_channels + 1) * 2 * sizeof(float));
  if (nbytes != (ssize_t)((num_channels + 1) * 2 * sizeof(float)))
    THROW_JEXCEPT("Cannot read baseline visibilities");

  SFXCBaselineKey key;
  key.baseline = std::make_pair(hdr.station_nr1, hdr.station_nr2);
  key.polarisation = std::make_pair(hdr.polarisation1, hdr.polarisation2);
  key.subband = std::make_pair(hdr.frequency_nr, hdr.sideband);
  keys.push_back(key);

  baselines.insert(key.baseline);
  subbands.insert(key.subband);

  SFXCKey key1, key2;
  key1.antenna = key.baseline.first;
  key1.polarisation = key.polarisation.first;
  key1.subband = key.subband;
  key2.antenna = key.baseline.second;
  key2.polarisation = key.polarisation.second;
  key2.subband = key.subband;
  uint32_t ntotal = std::max(ntotal_map[key1], ntotal_map[key2]);
  //baseline_weight_map[key] = (double)hdr.nvalid / ntotal;
  baseline_weight_map[key] = (double)hdr.weight / ntotal;

  baseline = baselines.begin();
  subband = subbands.begin();

  current_baseline++;
}

void
SFXCVisBuffer::read_timeslice(void)
{
  current_baseline = 0;

  uvw_map.clear();
  ntotal_map.clear();
  weight_map.clear();
  baseline_weight_map.clear();

  do {
    read_uvws();
    read_stats();
    while (next_baselines--)
      read_baseline();
    read_timeslice_header();
  } while (current_slice == next_slice);
}

casa::Bool
SFXCVisBuffer::next(void)
{
  if (subbands.empty())
    {
      read_timeslice_header();
    }
  else
    {
      subband++;
      if (subband == subbands.end())
	{
	  baseline++;
	  subband = subbands.begin();
	}
    }

  if (baseline == baselines.end())
    {
      keys.clear();
      baselines.clear();
      subbands.clear();

      if (next_slice == -1)
	return casa::False;

      current_slice = next_slice;
      casa::Double dayfraction = (start_sec + (current_slice + 0.5) * integration_time) / DayConversion::secondsPerDay;
      slice_time = casa::MVEpoch(casa::MVTime(start_year, start_month, start_day, dayfraction));
      read_timeslice();

      mepoch_in_scaninfo cond(slice_time);
      scanptrlist_type::const_iterator scan;
      scan = find_scaninfo_by_time(__scanptrlist, cond);
      if (scan != __scanptrlist.end())
	  setCurrentScanptr(*scan);
    }

  return casa::True;
}

const casa::String&
SFXCVisBuffer::getRootDir() const
{
  return __rootdir;
}

casa::Int
SFXCVisBuffer::getVisBufId() const
{
  return 0;
}

casa::String
SFXCVisBuffer::getProcessorSubType() const
{
  return "SFXC";
}

CorrelatorSetup
SFXCVisBuffer::correlatorSetup() const
{
  return __setup;
}

const j2ms::stationscanlist_t&
SFXCVisBuffer::getStationScanlist(int) const
{
  // Simply return a list with all scans in the experiment.  This
  // should disable the Ghostbuster filter completely.
  return stationscanlist;
}

SFXCVisBuffer::~SFXCVisBuffer()
{
  scanptrlist_type::iterator scan;

  for(scan = __scanptrlist.begin(); scan != __scanptrlist.end(); scan++) 
    delete *scan;
}
