//
// Filter - base class that describes the Filter-interface
//
// The goal is to have the JIVEMSFiller deal with filtering
// visibilities. The real filters are derived classes from 
// this one.
//
// Author:  H. Verkouter, 12-06-2001
//
// $Id: Filter.h,v 1.2 2006/01/13 11:35:38 verkout Exp $
//
// $Log: Filter.h,v $
// Revision 1.2  2006/01/13 11:35:38  verkout
// HV: CASAfied version of the implement
//
// Revision 1.1  2001/07/09 07:52:45  verkout
// HV: Shitload of mods:
// - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
// - Enabled passing of options from j2ms2 commandline to an experiment
// - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
// - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
// - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
// - Enabled visibility filtering. At the moment only a source filter is implemented
//
//
//
#ifndef FILTER_H
#define FILTER_H

#include <casa/BasicSL/String.h>
#include <jive/j2ms/Visibility.h>


class Filter
{
public:

	//
	// C'tor with string. Is required; the string is
	// the 'identifier' i.e. the thing by which the 
	// system can discriminate between different filters
	//
	Filter( const casa::String& id );

	//
	// Return the Id of this filter
	//
	const casa::String&  id( void ) const;

	//
	// This is what's it all about!
	// The system asks the filter to process the visibility
	// and tell it whether or not to filter it.
	//
	// The filter should return False if the visibility is to
	// be filtered (i.e. SKIPPED), True otherwise
	//
	// Note: The function is NOT CONST so the filter can
	// 		 modify itself (for e.g. caching etc)
	//
	virtual casa::Bool    letPass( const Visibility& vis ) = 0;

	//
	// This class is meant to be derived from so this
	// must be virtual....
	//
	virtual ~Filter();

private:
	//
	// Our private parts
	//
	casa::String    myId;

	//
	// Prohibit these; they don't make too much sense
	//
	Filter();
	Filter( const Filter& );
	Filter& operator=( const Filter& );
};

#endif
 
