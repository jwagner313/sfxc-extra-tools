#if 0
// implementation
#include <jive/j2ms/jccspol2j2mspol.h>

#include <map>

j2ms::polarization jccspol2j2mspol( InterferometerDescriptor::Polarization p ) {
    typedef std::map<InterferometerDescriptor::Polarization,j2ms::polarization>  maptype;
   
    static maptype   __m;

    if( __m.empty() ) {
        __m.insert( std::make_pair(InterferometerDescriptor::x, j2ms::x) );
        __m.insert( std::make_pair(InterferometerDescriptor::y, j2ms::y) );
        __m.insert( std::make_pair(InterferometerDescriptor::L, j2ms::l) );
        __m.insert( std::make_pair(InterferometerDescriptor::R, j2ms::r) );
    }

    j2ms::polarization       r( j2ms::unknown );
    maptype::const_iterator  pptr;

    if( (pptr=__m.find(p))!=__m.end() )
        r = pptr->second;
    return r;
}
#endif
