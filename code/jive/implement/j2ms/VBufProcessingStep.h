// describes a (non-standard/optional) processing step a VisibilityBuffer applied to the data
// between reading it from disk and feeding it to the Filler
//
// $Log: VBufProcessingStep.h,v $
// Revision 1.1  2007/05/25 09:29:52  jive_cc
// HV: Support for adding JIVE specific processing history
//
//
#ifndef JIVE_J2MS_VBUFPROCESSINGSTEP_H
#define JIVE_J2MS_VBUFPROCESSINGSTEP_H

#include <string>
#include <vector>

struct VBufProcessingStep {
    std::string  task;
    std::string  parameters;

    // task name is obligatory, parameter(s) are optional
    VBufProcessingStep( const std::string& t,
                        const std::string& p = "" );
};

typedef std::vector<VBufProcessingStep>  processing_steps;


#endif
