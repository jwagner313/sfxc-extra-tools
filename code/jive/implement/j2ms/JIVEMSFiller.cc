//
//  Implementation of the JIVEMSFiller-class
//
//  Author:  Harro Verkouter,   10-12-1998
//
//  $Id: JIVEMSFiller.cc,v 1.24 2011/10/12 12:45:29 jive_cc Exp $
//
#include <jive/j2ms/JIVEMSFiller.h>
#include <jive/j2ms/CorrelatorSetup.h>
#include <jive/j2ms/History.h>
#include <jive/j2ms/Scan.h>
#include <jive/j2ms/FrequencyDB.h>
#include <jive/j2ms/AntennaDB.h>
#include <jive/j2ms/SourceDB.h>
//#include <jive/j2ms/rot2mepoch.h>
#include <jive/j2ms/noUVW.h>
#include <jive/j2ms/aipspppol2j2mspol.h>
#include <jive/Utilities/FreqCounter.h>
#include <jive/Utilities/jexcept.h>
#include <jive/Utilities/stringutil.h>
#include <jive/Utilities/sciprint.h>
#include <jive/labels/CorrelationCode.h>
#include <jive/labels/VisLabel.h>

#include <jive/assertfail.h>



// Filters recognized by us
#include <jive/j2ms/SourceFilter.h>
#include <jive/j2ms/GhostBusterFilter.h>
#include <jive/j2ms/AntennaInfo.h>

#if 0
// COF stuff
#include <data_handler/MediumStatsFile.h>
#include <data_handler/MappingsFile.h>
#include <data_handler/MediumStatDataRecord.h>
#include <data_handler/CountedPointer.h>
#include <data_handler/MappingEntry.h>
#include <data_handler/ModelFile.h>
#include <data_handler/ModelDataRecord.h>
#endif

// CASA stuff
#include <ms/MeasurementSets.h>
#include <ms/MeasurementSets/MSPolarization.h>
#include <ms/MeasurementSets/MSSpectralWindow.h>
#include <ms/MeasurementSets/MSDataDescription.h>
#include <ms/MeasurementSets/MSPolColumns.h>
#include <ms/MeasurementSets/MSSpWindowColumns.h>
#include <ms/MeasurementSets/MSDataDescColumns.h>
#include <ms/MeasurementSets/MSAntenna.h>
#include <ms/MeasurementSets/MSAntennaColumns.h>
#include <ms/MeasurementSets/MSFeed.h>
#include <ms/MeasurementSets/MSFeedColumns.h>
#include <ms/MeasurementSets/MSField.h>
#include <ms/MeasurementSets/MSFieldColumns.h>
#include <ms/MeasurementSets/MSSource.h>
#include <ms/MeasurementSets/MSSourceColumns.h>
#include <ms/MeasurementSets/MSProcessor.h>
#include <ms/MeasurementSets/MSProcessorColumns.h>
#include <ms/MeasurementSets/MSObservation.h>
#include <ms/MeasurementSets/MSObsColumns.h>
#include <ms/MeasurementSets/MSState.h>
#include <ms/MeasurementSets/MSStateColumns.h>

#include <casa/Quanta.h>
#include <casa/Quanta/MVAngle.h>
#include <casa/Quanta/Quantum.h>
#include <casa/Quanta/Unit.h>
#include <casa/Quanta/Euler.h>
#include <casa/Quanta/QMath.h>
#include <casa/Quanta/RotMatrix.h>
#include <casa/Quanta/MVTime.h>
#include <casa/Quanta/MVEpoch.h>

#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/ArrayLogical.h>
#include <casa/Arrays/ArrayUtil.h>

#include <casa/Utilities/Regex.h>

#include <casa/Exceptions.h>
#include <casa/OS/Time.h>

#include <measures/Measures/MeasMath.h>
#include <measures/Measures/Stokes.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/Muvw.h>

#include <scimath/Mathematics/FFTServer.h>

#include <tables/Tables.h>
#include <tables/Tables/ExprNode.h>
#include <tables/Tables/TableColumn.h>
#include <tables/Tables/TableDesc.h>
#include <tables/Tables/TableIter.h>



// system headers

#include <iomanip>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <dirent.h>
#include <libgen.h>
#include <time.h>
#include <limits.h>

// C++ and STL stuff
#include <set>
#include <sstream>
#include <algorithm>
#include <vector>
#include <cassert>

using namespace std;
using namespace casa;
//using JCCS::CountedPointer;


// Static stuff
ExperimentCache      JIVEMSFiller::theirCache;



// If the linker wont recognize our template, let's stuff'm in here!
// (and see if that works)
const char* translate2ascii( const String& arg ) {
	return arg.chars();
}


//  Define RotMatrix*Vector (how to do it...)
Vector<Double> operator*( const RotMatrix& lhside, const Vector<Double>& rhside ) {
    Vector<Double>   retval(3);
    
    retval = 0.0;
    
    for( int i=0; i<3; i++ ) {
		for( int j=0; j<3; j++ ) {
			retval(i) += lhside( i, j )*rhside(j);
		}
    }
    return retval;
}




JIVEMSFiller::JIVEMSFiller( const String& opt ):
	myOptions( opt )
{
	//	cout << "Created JIVEMSFiller with opt=" << myOptions << endl;
	//
	// Parse the options (if any)
	
	// Let's remove all whitespace stuff!
	string           optcopy;
	string::iterator curchar = myOptions.begin();

	while( curchar<myOptions.end() ) {
		if( !::strchr(" \t\n\f", *curchar) ) {
			optcopy.append( 1, *curchar );
		}
		curchar++;
	}

	// Step one: split the options at semicolons ';' -> they're the delimiters
	// between separate options.
	Regex                    rx_filterwopt( "^filter/([^=]+)=(.+)$" );
	Regex                    rx_filter( "^filter/(.+)$" );
	vector<string>           opts;
	vector<string>::iterator curopt;

	opts   = ::split( optcopy, ';' );
	curopt = opts.begin();
	while( curopt!=opts.end() ) {
        String  tmp( *curopt );
        string  fspec, fopt;

        if( tmp.matches(rx_filterwopt) ) {
            Int    s, l;
            // filterspec is the first matchgroup
            rx_filterwopt.match_info(s, l, 1);
            fspec = tmp(s, l);
            // and the actual options are the second matchgroup
            rx_filterwopt.match_info(s, l, 2);
            fopt  = tmp(s, l);
        } else if( tmp.matches(rx_filter) ) {
            Int    s, l;
            // filterspec is the first matchgroup
            rx_filter.match_info(s, l, 1);
            fspec = tmp(s, l);
        } else if( curopt->size() ) {
			cerr << "JIVEMSFiller: unsupported option `" << *curopt << "'" << endl;
		}

        if( !fspec.empty() ) {
            Filter*  fptr( 0 );

			// See if we can recognize the filter....
			if( fspec=="source" ) {
				fptr = new SourceFilter( fopt );
                cout << "JIVEMSFiller: Adding SourceFilter [" << fopt << "]" << endl;
			} else if ( fspec=="ghostbuster" ) {
				fptr = new GhostBusterFilter( fopt );
                cout << "JIVEMSFiller: Adding GhostBusterFilter [" << fopt << "]" << endl;
            } else {
				cout << "JIVEMSFiller: unrecognized filter `" << fspec << "'"
					 << endl;
			}
			if( fptr )
                myFilters.push_back(fptr);
		}

		// Do *NOT* forget to move on! (Guess (again) how I found this one out...)
		curopt++;
	}
}

//  Do the job of actually making a new MS or concatenating it to an
//  existing one for the indicated scan in the given experiment....
Bool JIVEMSFiller::fillDataToExperimentsMS( Experiment& expref,
											const subjob_t& sjdescr,
											Bool ask )
{
    Bool              retval( False );
    VisibilityBuffer* ptr2buffer( 0 );
    
    // The very first check: if the indicated visibility buffer is
    // present in the experiment
    ptr2buffer = expref.getVisibilityBuffer( sjdescr.jobName );
    
    if( !ptr2buffer ) {
		expref.logLogMessage( "No datafile "+sjdescr.jobName+" found in experiment!" );
        cerr << "\n"
             << "WARN WARN WARN WARN WARN WARN WARN WARN WARN WARN WARN\n\n"
             << "Failed to find VisibilityBuffer '" << sjdescr.jobName << "' ?!?!\n\n"
             << "WARN WARN WARN WARN WARN WARN WARN WARN WARN WARN WARN\n\n";

		return retval;
    }

    //  Ok. Locate an MS where to stuff the new data....
    MS*       ptr2ms( this->makeMS(expref, ask) );
    
    if( !ptr2ms ) {
	    cout << "Failed to create output MS for the experiment!" << endl;
	    expref.logLogMessage( "Failed to create output MS for the experiment!" );
    	delete ptr2buffer;
    	return retval;
    }
    
    //  MS was created/opened succesfully, so continue!
    expref.logLogMessage( "Start filling datafile "+sjdescr.jobName+" to MS.." );
  
    try {
        retval = this->fillMSfromVisibilityBuffer( *ptr2ms,
												   *ptr2buffer,
												   sjdescr );
    }
    catch( const std::exception& se ) {
        cout << "standard exception: " << se.what() << endl;
    }
    catch( const char* s ) {
        cout << "Caught a string: " << s << endl;
    }
    catch( ... ) {
        cout << "Caught unknown exception?" << endl;
    }

    //  We don't need the pointer to the MS anymore....
    delete ptr2ms;

    //  Nor do we need the pointer to the scan anymore
    delete ptr2buffer;
    
    //  Tell'm what happened!
    return retval;
}



JIVEMSFiller::~JIVEMSFiller()
{}


//  The private methods!!!
MS* JIVEMSFiller::makeMS( const Experiment& expref, Bool ask ) {
    //  First we follow the method of TMS/NFRA, see MSDefiner{.cc,.h}
    MS*                   retval( 0 );
    Table*                jivetable( 0 );
    Table*                modeltable( 0 );
    Table*                proctable( 0 );
    String                key;
    TableDesc             td( MS::requiredTableDesc() );
    TableDesc             statstable( "JIVE_TAPESTATS", TableDesc::Scratch );
    TableDesc             modeldesc( "JIVE_MODEL", TableDesc::Scratch );
    TableDesc             procdesc( "JIVE_PROCESSING", TableDesc::Scratch );
    SetupNewTable*        tablesetupptr( 0 );
    SetupNewTable*        jivetablesetup( 0 );
    SetupNewTable*        modeltablesetup( 0 );
    SetupNewTable*        proctablesetup( 0 );
	MS::PredefinedColumns datacolEnum = MS::DATA;


    //  Create a sub-table (JIVE specific) that contains
    //  tapestatistics!
    statstable.addColumn( (ScalarColumnDesc<uInt>("SUID", "Station Unit ID")) );
    statstable.addColumn( (ScalarColumnDesc<Int>("ANTENNA_ID", "Antenna ID")) );
    statstable.addColumn( (ScalarColumnDesc<Double>("TOT", "Telescope time (short form ROT)")) );
    statstable.addColumn( (ScalarColumnDesc<Double>("ROT", "ROT (short form ROT)")) );
    statstable.addColumn( (ScalarColumnDesc<Double>("SERVO_ERROR", "Current servo error")) );
    statstable.addColumn( (ScalarColumnDesc<uInt>("N_HDSTK", "Number of headstacks")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("HDSTK_POS", "Headstack positions")) );
    statstable.addColumn( (ScalarColumnDesc<uInt>("FOOTAGE", "Tape footage")) );
    statstable.addColumn( (ScalarColumnDesc<uInt>("SU_STATE", "Station Unit status")) );
    statstable.addColumn( (ScalarColumnDesc<uInt>("SUIM_STATE", "SUIM Status")) );
    statstable.addColumn( (ScalarColumnDesc<uInt>("N_TRACK", "Number of tracks")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("ORG_TRACKNO", "The original tracknumber")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("NR_FRAMES", "The number of frames averaged")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("PE_COUNT", "The number of parity errors")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("US_COUNT", "Unexpected syncs count")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("MS_COUNT", "Missing syncs count")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("CRC_ERR", "Number of CRC errors")) );
    statstable.addColumn( (ArrayColumnDesc<uInt>("DBF_COUNT", "Discarded Bad Frame count")) );
	statstable.addColumn( (ScalarColumnDesc<Int>("PROCESSOR_ID", "Job where this data applies to")) );

    // And the defintion of the JIVE_MODEL for 
    // storing the model used whilst correlating
    modeldesc.addColumn( (ScalarColumnDesc<Int>("ANTENNA_ID", "Antenna ID")) );
    modeldesc.addColumn( (ScalarColumnDesc<Int>("FEED_ID", "Feed ID")) );
    modeldesc.addColumn( (ScalarColumnDesc<Int>("SPECTRAL_WINDOW_ID", "Spectral window ID")) );
    modeldesc.addColumn( (ScalarColumnDesc<Double>("TIME", "Starttime of interval for which polys are valid")) );
    modeldesc.addColumn( (ScalarColumnDesc<Double>("INTERVAL", "Duration of interval")) );
    modeldesc.addColumn( (ArrayColumnDesc<Double>("DRY_ATMOS_DELAY", "'dry' atmospheric delay")) );
    modeldesc.addColumn( (ArrayColumnDesc<Double>("WET_ATMOS_DELAY", "'wet' atmospheric delay")) );
    modeldesc.addColumn( (ArrayColumnDesc<Double>("CLOCK_OFFSET", "Clock offset")) );
    modeldesc.addColumn( (ArrayColumnDesc<Double>("GROUP_DELAY", "Group delay polynomials")) );
    modeldesc.addColumn( (ArrayColumnDesc<Double>("PHASE_DELAY", "Phase delay polynomials")) );
	modeldesc.addColumn( (ScalarColumnDesc<Int>("PROCESSOR_ID", "Job where this data applies to")) );

    // Table to hold processing steps that were applied to the data in this MS
    // processing steps == non-standard/optional JIVE-specific processing that
    // happened to the data.
    procdesc.addColumn( ScalarColumnDesc<Int>("JOBID", "JIVE specific JobId") );
    procdesc.addColumn( ScalarColumnDesc<Double>("TIME", "MJD when this task was run") );
    procdesc.addColumn( ScalarColumnDesc<String>("PROCESSING", "The processing step that was run") );
    procdesc.addColumn( ScalarColumnDesc<String>("PROCESSING_PARAMS", "(optional) parameter(s) of the task") );

    // Ok, since this is the JIVE-filler maybe it's safe to assume
    // that we ALWAYS have complex data?! And we're filling
    // Visibilities, who always have a matrix-shaped block of data,
    // therefore we add a complex array column of dimensionality 2.
    //
    //  HV: 19-07-2000  With the arrival of MSv2.0 time/frequency data
    //                  has to be stored in different columns....
    String    datacolumnname = "";
    
    switch( expref.getDestinationDomain() )
    {
		case Visibility::time:
		    datacolumnname = "LAG_DATA";
			datacolEnum = MS::LAG_DATA;
		    break;
	    
		case Visibility::frequency:
			datacolumnname = "DATA";
			datacolEnum = MS::DATA;
			break;
	    
		default:
			//  Yikes! Cannot do anything with this!
			//
			//  We just let the datacolumnname be empty so we can test
			//  on that after the switch statement and throw up after
			//  that!
			break;	    
    }
	
    if( datacolumnname=="" ) {
		cout << "Yikes! Experiment's destination datadomain is unknown!\n"
	    	 << "Hence we cannot decode on in what column to store the data...\n";	
		return 0;
    }
    
	// Add our datacolumn to the table!
	MS::addColumnToDesc( td, datacolEnum, 2 );
    //td.addColumn( ArrayColumnDesc<Complex>(datacolumnname), 2 );
    //td.addColumn( ArrayColumnDesc<Double>( "JIVE_POLWEIGHT", 1) );

    //
    //  Our data, when it comes from the Correlators, is UNCALIBrated
    //
    //key = "UNIT";
    //td.rwColumnDesc(datacolumnname).rwKeywordSet().define( key, "UNCALIB" );
    
    //
    //  Replace the existing flagcolumn by a two-dimensional array-column
    //
    //td.removeColumn( MS::columnName(MS::FLAG_ROW) );
    //td.addColumn( ScalarColumnDesc<Bool>(MS::columnName(MS::FLAG_ROW)) );
    //td.removeColumn( MS::columnName(MS::FLAG) );
    //td.addColumn( ArrayColumnDesc<Bool>(MS::columnName(MS::FLAG), 2) );
    //MS::addColumnToDesc( td, MS::FLAG, 2 );


    //
    //  Same goes for the SIGMA-column
    //
    //td.removeColumn( MS::columnName(MS::SIGMA) );
    //MS::addColumnToDesc( td, MS::SIGMA, 1 );

    //
    //  Weight
    //
    //td.removeColumn( MS::columnName(MS::WEIGHT) );
    //td.addColumn( ScalarColumnDesc<Double>(MS::columnName(MS::WEIGHT)) );

    //
    // Add measure reference keyword to time column
    // descriptor. Theoretically, this should be gotten from the
    // scan-starttime.... For now, we default to UTC. If the
    // scan-starttime deviates, we'll have to convert the start-time
    //
    //key = "MEASURE_REFERENCE";

    //td.rwColumnDesc(MS::columnName(MS::TIME)).rwKeywordSet().define( key, "UTC" );
	

    //
    //  Ok table descriptor set up. Now we first try to create the
    //  table as NewNoReplace, which will throw an exception if the
    //  table already exists. So if we catch an exception, we prompt the user
    //
    try
    {
		//  Ok. We want to use the TiledShapeStorage manager.  This
		//  requires us to define a hypercube (yummie..), so let's do
		//  that now
		td.defineHypercolumn( String("JIVEData"), (uInt)3,
							 ::stringToVector(datacolumnname+",FLAG") );


		tablesetupptr  = new SetupNewTable( expref.getMSname(), td, Table::NewNoReplace );	
		jivetablesetup = new SetupNewTable( expref.getMSname()+"/"+"JIVE_TAPESTATS",
						                    statstable, Table::NewNoReplace );
        modeltablesetup = new SetupNewTable( expref.getMSname()+"/"+"JIVE_MODEL",
                                             modeldesc, Table::NewNoReplace );
        proctablesetup = new SetupNewTable( expref.getMSname()+"/"+"JIVE_PROCESSING",
                                             procdesc, Table::NewNoReplace );

		//  If we get to here, we are creating the table!
		//  Continue on, make the storage manager!
		//
		//  The TiledShapeStorageManager will be acting on the
		//  (LAG_)DATA and FLAG columns. We specify a default shape of
		//  (4,64).. (during filling we will reset the shape or we do
		//  not and rely on the system do that for us?)
		TiledShapeStMan  tsm( "JIVEData", IPosition(3,1,4,64) );
	
		//  Let's bind the data/flag columns to the correct storage
		//  manager!
		tablesetupptr->bindColumn( datacolumnname, tsm );
		tablesetupptr->bindColumn( MS::columnName(MS::FLAG), tsm );
	
		//  Ok! Create the MS and JIVE specific tables
		retval     = new MeasurementSet( *tablesetupptr );
		jivetable  = new Table( *jivetablesetup );
        modeltable = new Table( *modeltablesetup );
        proctable  = new Table( *proctablesetup );
	
		//  Create default tables
		retval->createDefaultSubtables( Table::New );

		//  Add the sourcetable????
#if 0
		SetupNewTable  srctab( retval->sourceTableName(),
							   MSSource::requiredTableDesc(), Table::New );
		retval->rwKeywordSet().defineTable( MS::keywordName(MS::SOURCE),
											Table(srctab) );	
#endif
		//  Attach our own table!
		retval->rwKeywordSet().defineTable( "JIVE_TAPESTATS", *jivetable );
		retval->rwKeywordSet().defineTable( "JIVE_MODEL", *modeltable );
		retval->rwKeywordSet().defineTable( "JIVE_PROCESSING", *proctable );
		retval->initRefs();

		//  Add a column to the DATADESCRIPTION subtable if we're
		//  writing LAG_DATA..?
		if( expref.getDestinationDomain()==Visibility::time )
		    retval->dataDescription().addColumn( ScalarColumnDesc<Int>("LAG_ID") );

		// Let's add a pass Id column to the processor-table
		retval->processor().addColumn( ScalarColumnDesc<Int>("PASS_ID") );

		//  Let's set some reference-keywords in the various columns
		//MSColumns   mscol( *retval );
		//mscol.spectralWindow().chanFreq().rwKeywordSet().define( "MEASURE_REFERENCE", "TOPO");
		//mscol.spectralWindow().refFrequency().rwKeywordSet().define( "MEASURE_REFERENCE", "TOPO");
	
		//  And we're done!
    }
    catch( const std::exception& x ) {
		//  This call is to make sure that a) the compiler does not
		//  complain, and b) to make sure the compiler does not
		//  optimize it out?!
		cout << "*** " << x.what() << endl;
	
		//  Make really sure that tablesetupptr = 0, I don't trust it
		//  to leave the value intact in case of an exception...
		tablesetupptr = 0;
    }
    
    //  If tablesetupptr = 0, the table already existed. Ask the user
    //  for confirmation to concatenate
    if( !tablesetupptr ) {
		//  Table existed!

		//  HV: 21-07-2000  With the arrival of MS v 2.0 we have to be a little
		//                  more careful here... before we go on and ask the usr
		//                  if he/she wishes to concatenate the data we will have
		//                  to varify that we *can* concatenate (since the data
		//                  can be stored in an other column as the one we need
		//                  to be writing to). Depending on the desired output
		//                  domain, we check if the associated datacolumn indeed
		//                  is in the table. If it is not, we do not ask for
		//                  confirmation but return an error...
		Bool    precondition1;
		Bool    precondition2;
		Bool    precondition3;
	
	
		retval = new MeasurementSet( expref.getMSname() );

		precondition1 = retval->tableDesc().isColumn(datacolumnname);
		//precondition2 = retval->isColumnWritable(datacolumnname);
		precondition2 = true;
	 	precondition3 = precondition1 &&
				( (expref.getDestinationDomain()==Visibility::time && 
			       retval->dataDescription().tableDesc().isColumn("LAG_ID")) ||
				  (expref.getDestinationDomain()==Visibility::frequency) ); 

		//cout << "Found pre1/2/3 = " << precondition1 << "/"  
	 	//	 << precondition2 << "/" << precondition3 << endl;
		//cout << "datadomain=" << expref.getDestinationDomain() << endl
		//	 << "isColumn=" << retval->dataDescription().tableDesc().isColumn("LAG_ID")
		//	 << endl;

		//
		//  At this point we can safely delete the MS pointed to by retval
		//  since if we are going to use it we will have to open
		//  the MS with Table::Update anyway....
		//
		delete retval;
		retval = 0;

		if( precondition1 && precondition2 && precondition3 ) {
		    //  Ok.. datacolumn exists and is writable...
		    //  In that case it makes sense to go on and ask for confirmation!
		    Bool       concatenate( (ask==False) );
	
		    if( ask ) {
				String     ans;
		
				cin.tie( &cout );
		
				cout << "MS `" << expref.getMSname()
					 << "' already exists. Concatenate data? [yn] :";
				cin >> ans;
		
				cin.tie( 0 );
		
				if( ans=="y" || ans=="Y" )
				    concatenate = True;
	    	}
	
		    if( concatenate ) {
				// Ok... do concatenation!
				retval = new MeasurementSet( expref.getMSname(), Table::Update );
		    }
		}
		else
		{
		    cout << "**** Ok.. something's clearly wrong here...\n";
		    cout << "**** trying to concatenate data to MS '"
				 << expref.getMSname() << "'\n";
	    
		    if( !precondition1 )
				cout << "> MS already contains data from other datadomain!\n";
		    if( !precondition2 )
				cout << "> Datacolumn '" << datacolumnname << "' is not writable?!\n";
		    if( !precondition3 )
				cout << "> MS is internally inconsistent. Appears to contain LAG_DATA\n"
			 		<< "  but has no LAG_ID in DATA_DESCRIPTION subtable!\n";
	    
		    cout << flush;
		}
    }


    //  Clean up...
    delete tablesetupptr;
    delete jivetablesetup;
    delete jivetable;
    delete modeltablesetup;
    delete modeltable;
    delete proctablesetup;
    delete proctable;
    
    return retval;
}

typedef std::map<unsigned int, Int> sb2ddid_type;

// Only fill data from the visbuf where
// the scans are in the range sjdescr.firstScanNo() -> sjdescr.lastScanNo()
// (note: other filters may apply as well!! (see expirent/filler options)
Bool JIVEMSFiller::fillMSfromVisibilityBuffer( MeasurementSet& msref,
											   VisibilityBuffer& visbuf,
											   const subjob_t& sjdescr )
{
    uInt                 currentrownr;
    double               integrationtime;
    String               lastScanId;
    Visibility           cv;
	sb2ddid_type         sb2ddid;
    const j2ms::Scan*    curScanptr;
    const Experiment&    expref( visbuf.getExperiment() );
    j2ms::FreqCounter    sourceidcounter;

    // Make a number of column objects, to speed up access
    ScalarColumn<Int>     antenna1( msref, MS::columnName(MS::ANTENNA1) );
    ScalarColumn<Int>     antenna2( msref, MS::columnName(MS::ANTENNA2) );
    ScalarColumn<Int>     arrayid( msref, MS::columnName(MS::ARRAY_ID) );
    ScalarColumn<Int>     feed1( msref, MS::columnName(MS::FEED1) );
    ScalarColumn<Int>     feed2( msref, MS::columnName(MS::FEED2) );
    ScalarColumn<Int>     fieldid( msref, MS::columnName(MS::FIELD_ID) );
    ScalarColumn<Int>     observationidcol( msref, MS::columnName(MS::OBSERVATION_ID) );
    ScalarColumn<Int>     scannumber( msref, MS::columnName(MS::SCAN_NUMBER) );
    ScalarColumn<Int>     datadescid( msref, MS::columnName(MS::DATA_DESC_ID) );
    ScalarColumn<Int>     processorid( msref, MS::columnName(MS::PROCESSOR_ID) );
    ScalarColumn<Double>  timecentroid( msref, MS::columnName(MS::TIME_CENTROID) );
    ScalarColumn<Int>     observationid( msref, MS::columnName(MS::OBSERVATION_ID) );
    ScalarColumn<Int>     stateid( msref, MS::columnName(MS::STATE_ID) );
    ArrayColumn<Bool>     flag( msref, MS::columnName(MS::FLAG) );
    ScalarColumn<Bool>    flagrow( msref, MS::columnName(MS::FLAG_ROW) );
    ArrayColumn<Double>   uvw( msref, MS::columnName(MS::UVW) );
    ArrayColumn<Float>    weight( msref, MS::columnName(MS::WEIGHT) );
    ArrayColumn<Float>    sigma( msref, MS::columnName(MS::SIGMA) );
    ScalarColumn<Double>  exposure( msref, MS::columnName(MS::EXPOSURE) );
    ScalarColumn<Double>  intervalcol( msref, MS::columnName(MS::INTERVAL) );
    ScalarColumn<Double>  time( msref, MS::columnName(MS::TIME) );

    //  Leave the datacolumn unbound; we will find out later what the
    //  *real* datacolumn will eb (LAG_DATA or DATA)
    ArrayColumn<Complex>  data;

    //  Source stuff...
    uInt                      currsourceindex;

    //  Vector of Floats: the SIGMA-vector
    IPosition                 datashape;
    Vector<Float>             sigmavalues;

    //  Matrix<Bool> -> for FLAG stuff...
    Bool                     flagvalue;
    Matrix<Bool>             flagmatrix;
   
    // experiment cache
    ExperimentCacheElement*  ptr2element; 

    //  Ok. Get data from the cache
    ptr2element = theirCache.locateExperiment( expref.getCode() );
    if( !ptr2element ) 
		ptr2element = theirCache.addExperiment( expref );
    if( !ptr2element ) {
		cout << "fillMSFromVisibilityBufer: Failed to add experiment to cache?!" << endl;
        cout << "   Cannot continue translating this buffer!" << endl;
		return False;
    }

    // And init the lookuptable!
    // Throws if something seriously wrong!
    JIVEMSFiller::buildFreqMapping( msref, visbuf );

    //  If the exp. wants lag-data we need to attach the lag-data column from the 
    //  main table, otherwise the plain-old- data column
    if( expref.getDestinationDomain()==Visibility::time )
		data.attach( msref, MS::columnName(MS::LAG_DATA) );
    else if( expref.getDestinationDomain()==Visibility::frequency )
		data.attach( msref, MS::columnName(MS::DATA) );
    
    
    //  Okiedokie: read visibilities from the scan-buffer until there
    //  are no more left!
    currentrownr = msref.nrow();
    
    //  Take some variables out of the loop to prevent them being put
    //  on the stack over and over again
	Int              scanno;
	Int              fScan;
	Int              lScan;
    Bool             available;

	// Filter variables:
	// - whether or not the visibility passed all filters
	// - filteriterator
	// - number of filtered visibilities
	bool                      pass; 
	uLong                     numWritten;
	uLong                     numFiltered;
	uLong                     numOutsideScan;
    uLong                     numOutsideSubjob;
	vector<Filter*>::iterator curfilt;
    
    // Track translation speed
    uLong            nrbytes( 0 );
    struct timeval   starttime,endtime;
    
    //  tell the system to disp. DMY...
    MVTime::setFormat(MVTime::DMY);

    available = True;
    lastScanId = "No scan yet";

    //  Write or verify the FIELD table:
    //  - for all fields found in the table (if any) verify that the
    //    contents are consistent with those in the experiment
    //  - write missing field(s) (sources) into the table
    //  Throws up if something's fishy
    JIVEMSFiller::writeFieldAndSourceTable(msref, expref);

    //  Make sure the antennatable for the visbuf is written
    //  19/01/2007 HV - This method now throws if something fishy
    //                  so no need to check retval.
    JIVEMSFiller::writeAntennaAndFeedTable(msref, visbuf);
    
	// Let's read whatever's present in the processor table (in case 
	// we're appending to an existing MS...)
    //
	// Note: we only have to check the current pass here since it
	// *should* not vary across the visibilitybuffer
	Int                     curpassid;
	Int                     curprocid;
	map<int,int>            pass2pid;
	map<int,int>::iterator  ptr;

	{
		const MSProcessor&      proc( msref.processor() );
		ROMSProcessorColumns    proccols( proc );

		for( uInt i=0; i<proccols.nrow(); i++ ) {
			pass2pid[ (int)(proccols.passId()(i)) ] = (int)i;
		}
	}

	curpassid = visbuf.getVisBufId();
	ptr       = pass2pid.find(curpassid);

	if( ptr==pass2pid.end() ) {
		// was not yet defined...

		// add to table
		curprocid = this->updateProcessorTable( msref, curpassid, visbuf.getProcessorSubType() );

		// and add to map
		pass2pid[ curpassid ] = (Int)curprocid;
	} else {
		curprocid = ptr->second;
	}

    //  And the State table...?!
    if( !this->writeStateTable(msref, expref) ) {
		cout << "*** WARN: Failed to write STATE table to MS "
			 << expref.getMSname() << "\n"
			 << "    for experiment " << expref.getCode() << endl;
		cout << "    This will not affect the data; some auxiliary info may be "
			 << "    displayed incorrectly later on..." << endl;
    }
    
    //  Go!
	scanno           = -1;
	numWritten       = 0;
	numFiltered      = 0;
	numOutsideScan   = 0;
    numOutsideSubjob = 0;
	fScan            = sjdescr.firstScanNo();
	lScan            = sjdescr.lastScanNo();
	currsourceindex  = (uInt)-1;
	::gettimeofday(&starttime, 0);

	try
	{
        while( available ) {
    		available = visbuf.getCurrentVisibility( cv );

            // no more visibilities?
    		if( !available )
    			break;

            // Check for new possible new scan
    		curScanptr = visbuf.getScanptr();
    		if( curScanptr && (curScanptr->getScanId()!=lastScanId) ) {
                String             scode( curScanptr->getSourceCode() );
                String             fcode( curScanptr->getFrequencyCode() );
                SourceInfo         si;
                FrequencyConfig    fc;
                const SourceDB&    sdb( expref.getSourceDB() );
                const FrequencyDB& fdb( expref.getFrequencyDB() );
            
    			lastScanId = curScanptr->getScanId();

    			// Show usr. we detected a new scan...
                ::printf("\r%80s", " ");
    			cout << endl
                     << "===> Find: " << *curScanptr << endl
                     << "     Corr: " << visbuf.correlatorSetup() << endl;
	    
    			//  Look up the source of this scan in the experiment's source-list
                if( !sdb.searchSource(si, scode) ) {
    				cout << "Source " << scode << " not found; will set index to "
                         << "invalid value" << endl;
			    	currsourceindex = (uInt)-1;
    			} else {
                    currsourceindex = si.getId();
                }
    			//  Try to get the frequencysetup for this scan....
    			if( !fdb.searchFrequencyConfig(fc, fcode) ) {
    				cout << "Failed to locate setup " << fcode << endl;
			    } else {
        			cout << "\n" << fc << endl;
                }
			    //integrationtime = curScanptr->getCorrelatorSetup().getIntegrationTime();
			    integrationtime = visbuf.correlatorSetup().getIntegrationTime();

    			// Re-initialize lookuptable for subband -> datadescriptionId.
    			sb2ddid.clear();
			
		    	// save scannumber
    			scanno = curScanptr->getScanNo();
            } else if( !curScanptr ) {
        	    if( lastScanId!="" ) {
			    	lastScanId = "";
                    // clear the line
                    ::printf("\r%80s", " ");
			    	cout << endl
			    		 << "===> Finding visibilities without a Scan [not writing 'm]"
                         << endl;
		
    				integrationtime  = 0.0;
    				currsourceindex  = (uInt)-1;
    				scanno           = -1;
    				sb2ddid.clear();
			    }
		    }

            // Print counter *before* we can get into the 'continue'
            // thingy (theoretically we could show nothing,
            // if *everything* would be filtered or without scan)
            if( !((numWritten+numFiltered+numOutsideScan+numOutsideSubjob)%100) ) {
		    	cout << "ToMS: " << setw(8) << numWritten << " "
                     << "Filt: " << setw(8) << numFiltered << " "
                     << "NoSCN: " << setw(8) << numOutsideScan << " "
                     << "OOSubjobRNG: " << setw(8) << numOutsideSubjob
                     << "     \r"
                     << flush;
	    	}

    		if( !curScanptr ) {
    			numOutsideScan++;
    			visbuf.next();
    			continue;
	    	}

            // Check #1: is the scan within 'subjob' range?
    		//           the fScan/lScan are cached from
    		//           sjdescr.firstScanNo()/lastScanNo()
    		//           where sjdescr is a calling parameter
    		//           of this function.
    		//           
    		//           NOTE NOTE NOTE NOTE NOTE NOTE NOTE
    		//           
    		//           Do not try to optimize the condition
    		//           below since the system relies on the fact
    		//           that if (backwards compatibility mode!)
    		//           *no* scan-number based filtering is to be
    		//           done, the fScan==INT_MIN (such that the 
    		//           test scanno>=fScan is always true) and 
    		//           that lScan==INT_MAX (hence scanno<=lScan
    		//           always true). If you re-order the condition
    		//           it might stop working...
    		if( !(scanno>=fScan && scanno<=lScan) ) {
                numOutsideSubjob++;
    			visbuf.next();
    			continue;
    		}
		
            // Run the visibility through our filters! See if it passes
        	// all filters. If it does, then continue on!
    		pass    = true;
    		curfilt = myFilters.begin();
    		while( curfilt!=myFilters.end() && pass ) {
    			pass = pass && (*curfilt)->letPass(cv);
    			curfilt++;
    		}
		
    		if( !pass ) {
    			numFiltered++;
    			visbuf.next();
    			continue;
    		}

            //  Cache a reference to the visibilitylabel
            const ExtVisLabel&     label( cv.getLabel() );
            sb2ddid_type::iterator cursb2dd;
	
            //  (try to) get the datadescriptor-id for the subband
            if( (cursb2dd=sb2ddid.find(label.subbandnr))==sb2ddid.end() ) {
                Int                                ddId;
                pair<sb2ddid_type::iterator, bool> insres;

                // Let the system come up with the correct ddId
                ddId = JIVEMSFiller::updateSpectralAndPolarizationTable(msref, cv, *curScanptr);

                // Warn if we couldn't find it
				if( ddId==-1 ) {
					cout << "JIVEMSFiller: AAAARGHHH? Failed to get a datadescid "
                         << "for subband#" << label.subbandnr << endl;
				}
                insres = sb2ddid.insert( make_pair(label.subbandnr, ddId) );
                if( !insres.second )
                    THROW_JEXCEPT("Failed to insert entry in subband->datadescId cache!");
                cursb2dd = insres.first;
            }

            //  Ok. Dump the data in the MS!
            msref.addRow();
	
    		//  Write the columns!
    		time.put( currentrownr, (cv.getTime().get("s")).getValue() );
    		antenna1.put( currentrownr, label.baseline.idx1() );
    		antenna2.put( currentrownr, label.baseline.idx2() );
    	
    		//  Assume: our antennas have only 1 feed (with max. two pols) so default
    		//  to using feed 0 on the current antennae....
    		feed1.put( currentrownr, 0 );
    		feed2.put( currentrownr, 0 );
    		datadescid.put( currentrownr, cursb2dd->second );
    	
    		// [TODO: document this]
    		processorid.put( currentrownr, curprocid );
    		
    		fieldid.put( currentrownr, currsourceindex );
    		intervalcol.put( currentrownr, integrationtime );
    		exposure.put( currentrownr, integrationtime );
    		timecentroid.put( currentrownr, integrationtime );
    		scannumber.put( currentrownr, scanno );
    		arrayid.put( currentrownr, 0 );
    		observationid.put( currentrownr, 0 );
    		stateid.put( currentrownr, -1 );
    	
    		//  We need to calculate UVW....
            //  That is, unless the visibility has it's UVW
            //  part filled in
            if( MVuvw(cv.getUVW().get("m")).nearAbs(j2ms::noMVuvw) )
        		uvw.put( currentrownr, JIVEMSFiller::calcUVW(cv) );
            else
                uvw.put( currentrownr, cv.getUVW().get("m").getValue() );
		
    		//  The makeData() function makes sure that the visibilitydata
    		//  is conform the requested domain as indicated in the
    		//  experiment AND it will reorder the visibility data
            //  in the order the experiment dictates.
    		const Matrix<Complex>& dataref( JIVEMSFiller::makeData(expref, cv) );

            data.put( currentrownr, dataref );
		
    		//  Get the shape
    		datashape = dataref.shape();
	
    		//  Resize sigma...
    		sigmavalues.resize( datashape(0) );
    		sigmavalues = 0.0f;
	
    		sigma.put( currentrownr, sigmavalues );
    		weight.put( currentrownr, cv.getWeight() );	
	
            //  Resize FLAG-matrix
        	flagmatrix.resize( datashape );
	    
    		flagvalue  = cv.getFlag();
    		flagmatrix = flagvalue;
		
    		flag.put( currentrownr, flagmatrix );
    		flagrow.put( currentrownr, flagvalue );
	
            // Increment the counter that keeps track of how many bytes
        	// were translated
    		nrbytes += dataref.shape().product()*sizeof(Complex);
    		numWritten++;
	
    		//  Move on to next visibility
    		currentrownr++;
    		available = visbuf.next();
    	}
	}
	catch( AipsError& z ) {
		cout << "*gasp* got an aips exception!" << z.getMesg() << endl;
	}
    catch( const std::exception& e ) {
        cout << "Caught deadly std::exception: " << e.what() << endl;
    }
    //  get time!
	::gettimeofday(&endtime, 0);

	//  Update the observation table (we do this postfix because the
	//  observation start/end need to be in here... shiiiit.. is this
	//  dumb or what?) We do not really know these so we just derive
	//  these values from the first/last time in the current MS...
	if( !this->updateObservationTable(msref, expref) ) {
		cout << "*** Huh? Failed to update OBSERVATION-table in MS " << expref.getMSname() << "\n"
			<< "    for experiment " << expref.getCode() << endl;
		cout << "*** Some info maybe be stored incorrectly..." << endl;
		cout << "*** NOTE: THIS DOES NOT AFFECT THE DATA!" << endl;
		cout << "*** This is merely auxiliary data...." << endl;
    }
    

	double   ssec, esec;

	ssec = starttime.tv_sec + starttime.tv_usec/1E6;
	esec = endtime.tv_sec + endtime.tv_usec/1E6;
    ::printf("\r%80s", " ");
    cout << endl << "Translated @ "
		 << j2ms::sciprint<long double>(((long double)nrbytes)/(esec-ssec) , "bytes/s")
         << endl;

    cout << endl
         << "--------====== Summary ======------" << endl;
    ::printf("ToMS:        %9lu\n", numWritten);
    ::printf("Filt:        %9lu\n", numFiltered);
    ::printf("NoSCN:       %9lu\n", numOutsideScan);
    ::printf("OOSubjobRNG: %9lu\n", numOutsideSubjob);
    ::printf("\n");

    // Update the JIVE_PROCESSING table
    try {
        // Only try to access the table if there really *is*
        // something to write [does not break backward compat;
        // old MSs do not have this table but usually their
        // jobs do not have sufficient info to do processing...]
        processing_steps  ps( visbuf.getProcessingSteps() );

        if( ps.size() ) {
            Int                              cjid;
            Table                            ptab( msref.rwKeywordSet().asTable("JIVE_PROCESSING") );
            Double                           now;
            unsigned int                     row( ptab.nrow() );
            ScalarColumn<Int>                jid( ptab, "JOBID" );
            ScalarColumn<Double>             tm( ptab, "TIME" );
            ScalarColumn<String>             task( ptab, "PROCESSING" );
            ScalarColumn<String>             parms( ptab, "PROCESSING_PARAMS" );
            processing_steps::const_iterator curstep;
            
            // already reserve space for the new rows
            ptab.addRow( ps.size() );
            now  = (Time()).modifiedJulianDay();
            cjid = visbuf.getVisBufId();
            // Append data for the steps
            for(curstep=ps.begin(); curstep!=ps.end(); curstep++, row++) {
                jid.put(row, cjid);
                tm.put(row, now);
                task.put(row, curstep->task);
                parms.put(row, curstep->parameters );
            }
            ptab.flush();
        }
    }
    catch( ... ) {
        // no JIVE-PROCESSING table => bummer
    }

    // append even more stuff...
    JIVEMSFiller::writeModel( msref, visbuf );
    
    return True;
}

// read the SPECTRAL_WINDOW table & the experiments FrequencyDB
// and see if we can match'm up

// baseband properties
// Let's assume that these are the primary key
// [that is: for each frequency config, the combination
// of these values occurs only once]
struct bbprops {
    unsigned int bbId;
    double       bandwidth;
    double       freq;

    // construct from existing BaseBand object
    bbprops( const BaseBand& bb ):
        bbId( bb.getBaseBandNr() ),
        bandwidth( bb.getBandwidth() ),
        freq( bb.getStartfreq() )
    {}

    bbprops(unsigned int id, double bw, double f):
        bbId( id ), bandwidth( bw ), freq( f )
    {}
};
ostream& operator<<(ostream& os, const bbprops& b) {
    return os << "BB#" << b.bbId << "=" << j2ms::sciprintd(b.freq, "Hz")
              << " (" << j2ms::sciprintd(b.bandwidth, "Hz") << ")";
}
// implement operator< (implements the less-than-comparable concept)
// for strict weak ordering
struct _lt_bbprops {
    
    // order by id, freq and then bandwidth
    bool operator()(const bbprops& l, const bbprops& r ) const {
        return ( l.bbId<r.bbId ||
                 (l.bbId==r.bbId && l.freq<r.freq) ||
                 (l.bbId==r.bbId && near(l.freq, r.freq) && l.bandwidth<r.bandwidth) );
    }
};

// following the assumption [bbprops are unique within a FrequencyConfig]
// we can define a set of these for easy finding/caching
typedef std::set<bbprops, _lt_bbprops>  bbcache_type;

ostream& operator<<(ostream& os, const bbcache_type& b) {
    copy(b.begin(), b.end(),
         ostream_iterator<bbcache_type::value_type>(os, "\n"));
    return os;
}


// find a bbprops by frequency (+tolerance)
struct freqfinder {
    freqfinder(double f0, double bw, double tol):
        __f0(f0), __bandwidth(bw), __tolerance(tol)
    {}

    bool operator()( const bbprops& bb ) const {
        return (nearAbs(bb.freq, __f0, __tolerance) && nearAbs(bb.bandwidth, __bandwidth));
    }
    private:
        double   __f0;
        double   __bandwidth;
        double   __tolerance;
        // do not allow default c'tors
        freqfinder();
};
#define find_bb_by_freq(l, f0, bw, tol) \
    std::find_if(l.begin(), l.end(), freqfinder(f0, bw, tol))

void JIVEMSFiller::buildFreqMapping( const MeasurementSet& ms,
                                     const VisibilityBuffer& vb ) {
    // experiment cache
    const Experiment&        expref( vb.getExperiment() );
    const FrequencyDB&       freqdb( expref.getFrequencyDB() );
    ExperimentCacheElement*  elptr;

    //  Ok. Make sure we have an entry in the
    //  experiment cache that we can write to
    //  If that fails: we're buggered!
    elptr = theirCache.locateExperiment( expref.getCode() );
    if( !elptr )
		elptr = theirCache.addExperiment( expref );
    if( !elptr ) 
        THROW_JEXCEPT("buildFreqMapping: Failed to add experiment to cache?");

    // Iterate over the spectral window table by freq-group-name
    TableIterator            iter( ms.spectralWindow(), "FREQ_GROUP_NAME" );
    MSPolarization           poltab( ms.polarization() );
    MSDataDescription        ddtab( ms.dataDescription() );
    ROMSPolarizationColumns  polcols( poltab );
    
    while( !iter.pastEnd() ) {
        Table                   subtab( iter.table() );
        bbcache_type            bbcache;
        Vector<uInt>            spwIds( subtab.rowNumbers() );
        Vector<double>          freqs;
        FrequencyConfig         fc;
        ROArrayColumn<Double>   cfreq( subtab, "CHAN_FREQ" );
        ROScalarColumn<Double>  totbw( subtab, "TOTAL_BANDWIDTH" );
        ROScalarColumn<String>  fqname( subtab, "FREQ_GROUP_NAME" );

        // now what? how strict should we be?
        // If the MS contains a frequency-setup that the
        // experiment doesn't have, what do we do?
        // My feeling is: warn the user but do go on and try to
        // find as much info as we can. 
        //
        // It _may_ affect writing of modelfiles [they need to
        // be able to label the model with the correct spectral
        // window id and feed id]
        if( !freqdb.searchFrequencyConfig(fc, fqname(0)) ) {
            cerr << "WARN: Whilst trying to build frequency-setup cache:\n"
                 << "      Frequency setup '" << fqname(0) << "' in MS not found in Experiment"
                 << endl;
            // move on to next frequency setup
            iter.next();
            continue;
        }
        // Process the subbands from the FrequencyConfig
        const basebandlist_t&           basebands( fc.getBaseBands() );
        bbcache_type::const_iterator    cachedbb;
        basebandlist_t::const_iterator  curbb;
       
        // curbb is only a tmp here. Don't expect it to point at something
        // sensible after finishing this loop. It will be reused later on,
        // also as temporary.
        for( curbb=basebands.begin(); curbb!=basebands.end(); curbb++ ) {
            pair<bbcache_type::iterator,bool> insres;

            insres = bbcache.insert( bbprops(*curbb) );
            if( !insres.second )
                THROW_JEXCEPT("Internal error: failed to insert entry in cache for BaseBand #"
                              << curbb->getBaseBandNr() << " in mode  " << fqname(0));
        }
        // ok, process each subband found in the spectral window table
        for( uInt row=0; row<subtab.nrow(); row++ ) {
            // get the spectral window id
            Int                  spwId( (Int)spwIds(row) );
            Vector<Double>       freeks( cfreq(row) );
            // query the datadescriptions that exist with this spectral window
            // (combinations of spwId & polarizationId)
            Table                ddidtab( ddtab(ddtab.col("SPECTRAL_WINDOW_ID")==spwId) );
            Vector<uInt>         ddids( ddidtab.rowNumbers(ddtab) );
            ROScalarColumn<Int>  polidcol(ddidtab, "POLARIZATION_ID");

            // If there are no datadescription IDs associated with this
            // spectral window ID there's no need to bother - nothing
            // needs to be cached.
            if( ddidtab.nrow()==0 )
                continue;

            // attempt to find it in the list of freqgroup's basebands.
            // We must allow for a channelwidth of tolerance in the start-frequency
            // matching because of upper/lower sidebands:
            //   the VEXFrequencyDB subtracts, in lower-side-band-case, the bandwidth
            //   from the listed frequency and the "updateSpectralAndPolarization..()" method
            //   that writes the freqs into the MS adds a channel's width of frequency
            //   to the lowest frequency in lower-side-band case [so the highest frequency
            //   in that band will be the DC frequency]
            double    f0( freeks(0) );
            double    channelwidth( ::fabs(freeks(1) - f0) );
            double    bw( totbw(row) );

            cachedbb = find_bb_by_freq(bbcache,  f0, bw, channelwidth);
            // if we can't find it, warn the user about it but do not throw up -
            // see comment about 'strictiness' above
            if( cachedbb==bbcache.end() ) {
                cerr << "WARN: Spectral window #" << spwId << " not present in experiment's"
                     << " mode " << fqname(0) << endl;
                continue;
            }

            // weehaa
            // Loop over all entries in the datadescription table,
            // form the set of correlations (from the polarization table),
            // find (if necessary, insert new) entry in the ExperimentCacheElement's
            // freqcache for the current frequencysetup,
            // find/insert the baseband, insert entry for the 
            // correlations, may check for duplicates, as in, that shouldn't 
            // happen....
            for( uInt r=0; r<ddidtab.nrow(); r++ ) {
                Int            ddId( (Int)ddids(r) );
                Int            polId( polidcol(r) );
                Vector<Int>    aipsppcorr( polcols.corrType()(polidcol(r)) );
                correlations_t j2mscorr;

                // translate the aips++ polarization products into 
                // j2ms approved stuff
                j2mscorr = aipspppol2j2mspol( aipsppcorr );

                // now it's time to start find()ing &&/|| insert()ing stuff
                fqcache_type&                   fqcache( elptr->freqCache );
                fqcache_type::iterator          curfq;
                fqprops_type::iterator          curfqprop;
                definedcorrelations_t::iterator curcorr;

                if( (curfq=fqcache.find(fqname(0)))==fqcache.end() ) {
                    // attempt insert
                    pair<fqcache_type::iterator,bool>  insres;
                    insres = fqcache.insert( make_pair(fqname(0), fqprops_type()) );
                    if( !insres.second )
                        THROW_JEXCEPT("Failed to insert modename -> properties entry into map");
                    curfq = insres.first;
                }
                // found the fqcache_type entry, locate the current subband
                // Note: cachedbb->bbId is the current baseband number
                fqprops_type&      fqprops( curfq->second );

                if( (curfqprop=fqprops.find(cachedbb->bbId))==fqprops.end() ) {
                    // insert definition for this subband
                    pair<fqprops_type::iterator, bool> insres;

                    insres = fqprops.insert( make_pair(cachedbb->bbId, sbprops_type(spwId)) );
                    if( !insres.second )
                        THROW_JEXCEPT("Failed to insert sbprops for subband #" << cachedbb->bbId
                                       << " of mode " << fqname(0) << " [MS: spwId,ddId="
                                       << spwId << ", " << ddId << "]");
                    curfqprop = insres.first;
                } else {
                    // verify that stuff in the cache matches whatever we think
                    // it should be..
                    if( curfqprop->second.__spwId!=spwId )
                        THROW_JEXCEPT("Internally inconsistent: cached entry "
                                      << *curfqprop << "'s spwId does not match expected spwId " 
                                      << spwId << " for baseband #" << cachedbb->bbId);
                }
                // ok, now finally check for the correlations
                definedcorrelations_t&   corrs( curfqprop->second.__correlations );

                if( (curcorr=corrs.find(j2mscorr))==corrs.end() ) {
                    // define an entry for the current correlation setup
                    pair<definedcorrelations_t::iterator, bool> insres;

                    insres = corrs.insert( make_pair(j2mscorr, cordetails(polId, ddId)) );
                    if( !insres.second )
                        THROW_JEXCEPT("Failed to insert entry for correlations "
                                      << as_corrcode(j2mscorr) << " in subband #"
                                      << cachedbb->bbId << " of mode " << fqname(0)
                                      << " [MS: spwId,polId,ddId=" << spwId << ", "
                                      << polId << ", " << ddId << "]");
                } else {
                    // Entry was already in cache. Verify that it matches up
                    // with what we expect!
                    const cordetails&  cdet( curcorr->second );
                    
                    if( cdet.__polId!=polId ||
                        cdet.__ddId!=ddId )
                        THROW_JEXCEPT("Internal inconsitency - cached entry " << *curcorr
                                      << " does not match with expected polId=" << polId
                                      << "/ddId=" << ddId << " for setup "
                                      << fqname(0) << ", subband #" << cachedbb->bbId << endl
                                      << " (f0: " << j2ms::sciprintd(cachedbb->freq, "Hz") << ", "
                                      << "BW: " << j2ms::sciprintd(cachedbb->bandwidth, "Hz")
                                      << ")" << endl);
                }
            }
        }
        // *sigh* After having processed an iteration, can we move on please???
        // [go figure how i found this one out...]
        iter.next();
    }
    return;
}


//  Attempt to find out what the datadescriptionId for this specific
//  Visibility is (yes... datadescriptionId varies on a visibility
//  basis... yummy) Actually, the filler caches this; basically
//  what we need is a mapping of subband+polarization setup to 
//  data_description id
//
//  We need additional info in order to determine the
//  datadescriptionId. I would have preferred to pass less arguments
//  to this function but out of speed considerations I'd decided to
//  pass all extra info to the fn (since they are already known at the
//  time of calling this function). It is therefore important that the
//  user, calling this function makes sure all the bits are
//  consistent. 
//
//  Note: as said, I could do with less arguments but that would mean
//  extra work, lookups (for the FrequencyConfig in the experiment's
//  database), and some other stuff...
Int JIVEMSFiller::updateSpectralAndPolarizationTable( MS& msref,
						      const Visibility& vref,
						      const j2ms::Scan& scanref )
{
    //  Before we spend to much time, verify a couple of things...
    String                  fqcode( scanref.getFrequencyCode() );
    const ExtVisLabel&      label( vref.getLabel() );
    const Experiment&       expref( scanref.getExperiment() );
    const FrequencyDB&      freqdb( expref.getFrequencyDB() );
//    const CorrelatorSetup&  correlatorsetup( scanref.getCorrelatorSetup() );
    const CorrelatorSetup&  correlatorsetup( scanref.getVisibilityBuffer().correlatorSetup() );
    ExperimentCacheElement* elptr;

    //  Ok. Make sure we have an entry in the
    //  experiment cache that we can write to
    //  If that fails: we're buggered!
    elptr = theirCache.locateExperiment( expref.getCode() );
    if( !elptr )
		elptr = theirCache.addExperiment( expref );
    if( !elptr ) 
        THROW_JEXCEPT("updateSpectralAndPolarizationTable: Failed to add experiment to cache?");

    // First look in the experiment cache to see if we already had this one
    // As we NEED to have the vector of polarization products in order to
    // look up in the cache, let's make that one right away
    fqcache_type&           fqcache( elptr->freqCache );
    correlations_t          j2mscorr; 
    fqcache_type::iterator  curfq;
    const correlationset_t& prods( correlatorsetup.getCorrelations() );

    // As the correlatorsetup gives an ordered set of products, we
    // can take them as-is and put them in an unordered sequence but
    // still retain the correct order.
    j2mscorr.clear();
    std::copy( prods.begin(), prods.end(),
               back_insert_iterator<correlations_t>(j2mscorr) );

    // Make sure an entry for the current frequency config is present
    if( (curfq=fqcache.find(fqcode))==fqcache.end() ) {
        FrequencyConfig                    fc;
        pair<fqcache_type::iterator, bool> insres;

        // If freqDB does not have definition for FrequencyConfig,
        // bail out
        if( freqdb.searchFrequencyConfig(fc, fqcode)==False ) {
#if 0
            THROW_JEXCEPT("updateSpectralAndPolarizationTable: No definition found for mode '"
             << scanref.getFrequencyCode() << " in experiment " << expref.getCode()
             << endl);
#else
            return -1;
#endif
        }

        insres = fqcache.insert( make_pair(fqcode, fqprops_type()) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to insert entry for mode " << fqcode << " in cache");
        curfq = insres.first;
    }

    // Next up is the subband. If no definition found (yet), add one
    fqprops_type&           fqprops( curfq->second );
    fqprops_type::iterator  sbpropsptr;

    if( (sbpropsptr=fqprops.find(label.subbandnr))==fqprops.end() ) {
        // No definition for the current subband.
        // Scan through the MSSpectralWindow table to find it, adding
        // a row, if necessary
        const BaseBand*         bbptr;
        FrequencyConfig         fc;
        MSSpectralWindow&       spwintable( msref.spectralWindow() );
        MSSpWindowColumns       spwcol( spwintable );

        // If freqDB does not have definition for FrequencyConfig,
        // bail out
        if( freqdb.searchFrequencyConfig(fc, fqcode)==False ) {
#if 0
            THROW_JEXCEPT("updateSpectralAndPolarizationTable: No definition found for mode '"
             << fqcode << " in experiment " << expref.getCode()
             << endl);
#else
            cerr << "No definition found for mode " << fqcode << endl;
            return -1;
#endif
        }
        //  Lookup baseband in frequency config
        if( (bbptr=fc.getBaseBand(label.subbandnr))==0 ) {
#if 0
    	    THROW_JEXCEPT("updateSpectralAndPolarizationTable failed; cannot lookup subband #"
    			 << label.subbandnr << " in mode " << fqcode 
    			 << " in experiment " << expref.getCode() << "\n");
#else
            cerr << "cannot lookup subband #" << label.subbandnr << " in mode " << fqcode << endl;
            return -1;
#endif
        }
        
        // Gather all the info we need to match up the BaseBand's properties
        // with a (possible) row in the spectral window table
        Int             sideband;
        uInt            row; // will be the spectral window id, eventually!
        uInt            numchannels( (uInt)correlatorsetup.nrOfSpectralPoints() );
        Double          totalbandwidth( bbptr->getBandwidth() );
        Double          startfreq( bbptr->getStartfreq() );
        Double          channelwidth( totalbandwidth/(Double)numchannels );
        Array<Double>   centerfreqs( IPosition(1,numchannels) );
        Array<Double>   chwidths( IPosition(1,numchannels) );
        
        // all channels have the same resolution
        chwidths = channelwidth;    

        //  Compute the centerfrequencies. Take care of upper/lower side band
        if( bbptr->getSideBand()==BaseBand::upper ) {
    		sideband = 1;
		    for( uInt i=0; i<numchannels; i++ ) 
    		    centerfreqs( IPosition(1,i) )= startfreq + ((Double)i)*channelwidth;
	    } else if( bbptr->getSideBand()==BaseBand::lower ) {
		    sideband = 0;
		    for( Int j=(numchannels-1); j>=0; j-- ) 
    		    centerfreqs( IPosition(1,(uInt)j) )= startfreq + ((Double)j+1)*channelwidth;
        } else {
#if 0
	        THROW_JEXCEPT("sideband information is not set for subband #"
                          << bbptr->getBaseBandNr() << " in mode " << fqcode);
#else
            cerr << "sideband information is not set for subband #"
                 << bbptr->getBaseBandNr() << " in mode " << fqcode;
            return -1;
#endif
        }

        // Now look through the spectral window table for a match
        for( row=0; row<spwintable.nrow(); row++ ) {
		    Array<Double>    freqs;
            Array<Double>    widths;

    		spwcol.chanFreq().get( row, freqs );
    		spwcol.chanWidth().get( row, widths );
            // if frequencies match up to the milliHz we could say they're equal
            // i hope [this assumes the frequencies are written in Hz, obviously]
		    if( ::allNearAbs(freqs, centerfreqs, 1.0e-6) &&
                ::allNearAbs(widths, chwidths, 1.0e-6) &&
                spwcol.freqGroupName().asString(row)==fqcode &&
                spwcol.netSideband().asInt(row)==sideband ) 
                break;
		}
        if( row==spwintable.nrow() ) {
            // not found. Add row with definition of current
            // spectral window
            spwintable.addRow();

    		spwcol.numChan().put( row, numchannels );
            // we do not give names to the spectral windows
		    spwcol.name().put( row, "" );
		    spwcol.refFrequency().put( row, 0.0);
		    spwcol.chanFreq().put( row, centerfreqs );	
	
    		//  Frequency reference? Heck.. i don't know... just put in
    		//  RESTfrequency..
    		spwcol.measFreqRef().put( row, (Int)MFrequency::REST );
    		spwcol.chanWidth().put( row, chwidths );	
    		spwcol.effectiveBW().put( row, chwidths );
    		spwcol.resolution().put( row, chwidths );	
    		spwcol.totalBandwidth().put( row, totalbandwidth );
    		spwcol.netSideband().put( row, sideband );
    		spwcol.ifConvChain().put(row, 0 );
    		spwcol.freqGroup().put( row, fc.getFreqGroupNr() );
    		spwcol.freqGroupName().put( row, fqcode);
    		spwcol.flagRow().put( row, False );
	
		    spwintable.flush();
        }
		// 'row' now holds the value of the spectral window.
        // Now we can add a proper 'sbprops' type to the cache
        pair<fqprops_type::iterator, bool>  insres;

        insres = fqprops.insert( make_pair(label.subbandnr,
                                           sbprops_type((Int)row)) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to insert entry for subband #" << label.subbandnr
                          << " of mode " << fqcode << " into cache");
        sbpropsptr = insres.first;
    }

    // We have the spectral window id
    // Now look for the polarization stuff.
    // See if it is in the sbprops_type thing
    definedcorrelations_t&          corrs( sbpropsptr->second.__correlations );
    definedcorrelations_t::iterator curcor;

    // NOTE: j2mscorr is taken to contain an ordered sequence albeit that
    // the container itself is not explicitly ordered. This is to say: the
    // order in j2mscorr is essential but not enforced; it is up to us, the user
    // to put the elemnts in there in the correct order.
    if( (curcor=corrs.find(j2mscorr))==corrs.end() ) {
        // Not in cache yet, browse Polarization table and if necessary
        // add a row
        uInt                           row; // will be the polarization-id, eventually
        Array<Int>                     prods( IPosition(2, 2, j2mscorr.size()) );
        unsigned int                   i;
        MSPolarization                 polarization( msref.polarization() );
        MSPolarizationColumns          polcols( polarization );
        const j2ms::log2polmap&        polmap( correlatorsetup.l2pmap() );
        correlations_t::const_iterator corptr;
    

        // Compute the pairs of arrayindices into the MSFeed::POLARIZATION_TYPE array.
        // The polarization-types are written in the same order for all
        // antennas so no worries about that, but we must still find the indices of
        // the polarizations of the parts of the polarization products.
        for(i=0, corptr=j2mscorr.begin(); corptr!=j2mscorr.end(); i++, corptr++) {
            // for the given polarization product (ie: "*corptr"), get the
            // inidividual polarizations ( "[0]" and "[1]") and look up what the
            // corresponding array-index of that polarization is.
            // The "writeAntennaAndFeed()" table uses the same polmap to
            // write entries into the MSFeed table so this should
            // give us internal consistency.
    		prods( IPosition(2,0,i) ) = polmap.index_of( (*corptr)[0] );
    		prods( IPosition(2,1,i) ) = polmap.index_of( (*corptr)[1] );
        }
        for( row=0; row<polarization.nrow(); row++ ) {
            Array<Int>        prod_fromtab;
            Vector<Int>       types;
            correlations_t    types_fromtab;
           
            polcols.corrType().get(row, types);
            polcols.corrProduct().get(row, prod_fromtab);

            // either we change the Vector<Int> into a 'correlations_t' (j2mscorr's type)
            // or we change j2mscorr into a Vector<Int> ...  Chose the first, as it
            // was already there...
            types_fromtab = aipspppol2j2mspol( types );
            if( types_fromtab==j2mscorr &&
                ::allEQ(prod_fromtab, prods) )
                break;
        }
        if( row==polarization.nrow() ) {
            // ow bugger. need to add a row!
            // Re-use the 'prods' variable; it has been pre-computed
            // to hold the array of receptor products
            // Still left to do is to change the products in j2ms-format
            // (found in j2mscorr) into aips++ Stokes::StokesTypes form
        
            //  figure out what the (aips++)-polarizations are
            Vector<Int>                      corrtypes( IPosition(1,j2mscorr.size()) );
            unsigned int                     i;
   
            for( corptr=j2mscorr.begin(), i=0; corptr!=j2mscorr.end(); corptr++, i++ ) {
                // The aips++ Stokes stuff support constructing from string
                // and the j2ms stuff (the current correlation) supports
                // printing as string...
                ostringstream  strm;

        		strm << *corptr;

    		    //  Transform the string to aips++ internal enum
        		corrtypes( IPosition(1,i) )  = (Int)Stokes::type( strm.str() );
            }

            // Now we have enough info to write a row
            polarization.addRow();

		    polcols.numCorr().put( row, j2mscorr.size() );
    		polcols.corrType().put( row, corrtypes );
    		polcols.corrProduct().put( row, prods );
    		polcols.flagRow().put( row, False );
	
    		//  Make sure polarization is is set to correct value...
    		polarization.flush();
        }
        // 'row' now holds the value of the polarization id.
        // Now we must decide on the datadescriptor Id..
        // Look for a match in the DATA_DESCRIPTION table for the
        // key (SPWid, POLid)
        Int                ddId;
        Int                spwId( sbpropsptr->second.__spwId );
        Int                polId( (Int)row );
        Table              dd;
        MSDataDescription  msdd( msref.dataDescription() );

        dd = msdd( msdd.col("SPECTRAL_WINDOW_ID")==spwId &&
                   msdd.col("POLARIZATION_ID")==polId );
       
        // if we have >0 matches (could be even >1 in which case but they're all equal so
        // we just warn about it I guess; it only means that >1 datadescriptionIds
        // refer to same data. It is not forbidden only inconvenient for the user)
        // we just take the first entry that matches.
        // Otherwise insert a new row
        if( dd.nrow()==0 ) {
            // add a row to the datadescription table
            MSDataDescColumns  msddcol( msdd );
            
            ddId = (Int)msdd.nrow();

            msdd.addRow();
            msddcol.spectralWindowId().put(ddId, spwId);
            msddcol.polarizationId().put(ddId, polId);
            msdd.flush();
        } else {
            // take the first matching ddId
            ddId = dd.rowNumbers( msdd )[0];
            if( dd.nrow()>1 ) {
                cerr << "WARN: More than 1 DataDescriptionIds point to the same "
                     << "SPWId/PolId combi (" << spwId << "/" << polId << ") which is "
                     << "harmless but potentially inconvenient for the user." << endl
                     << "By the way: decided to use ddId[0] =" << ddId << " of "
                     << dd.nrow() << " available." << endl;
            }
        }    
        // finally we can add an entry to the mapping
        pair<definedcorrelations_t::iterator,bool> insres;

        insres = corrs.insert( make_pair(j2mscorr, cordetails(polId, ddId)) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to insert entry into definedcorrelations_t cache");
        curcor = insres.first;
    }
    // phew. Found everything!
    return curcor->second.__ddId;
}


// Writes the ANTENNA and FEED tables for the given experiment into the
// indicated MS.
//
// HV: 24/08/2006  Polarization handling reworked. 
//                 In the MS 2.0 defn the combination
//                 between FEED/POLARIZATION/DATA_DESCRIPTION
//                 is somewhat ... non-orthogonal.
//                 Illustrational example below, by the way.
//                 
//                 In the POLARIZATION table you [have to] specify
//                 the physical polarization products [XX,RL etc]
//                 _as well as_ an array of 0,1 combinations which
//                 are indices in the feedtable-polarizationtypecolumn
//                 vector. However, the POLARIZATION table has no
//                 ANTENNA dependency whereas the FEED table *does*.
//                 Basically, the polarizationtype-column contents can vary
//                 by antenna and hence the polarization listed in index '0'
//                 'does not necessarily need to be the same for all antennas'.
//                 However, the row in the POLARIZATION table in itself cannot
//                 cope with this fact; it could lead to an internal inconsistency.
//
//                 For now, the simple solution is to *enforce* the polarization-type
//                 columns for each antenna be the same, ie that polarization-type[0]
//                 is equal for all antennas and, in the case of dual polarization,
//                 that polarization-type[1] is also equal for all antennas.
//                 [The 'difficult' solution would be to create different datadescription-ids
//                  for baselines to antenna(e) who have differing polarization-type
//                  columnvalues in the FEED table]
//
//                 ============ Example =============
//
//                 * you must know that the 'lookup' of what the data in the MAIN
//                 table means goes like
//                 - in row in maintable, find the DATA_DESC_ID
//                 - this points at a row in the DATA_DESCRIPTION_TABLE
//                 - this row contains a combination of pointers into the 
//                   SPECTRAL_WINDOW (for freq) table and POLARIZATION table
//                   (to find out which polarization combinations are 
//                   in the datamatrix in the row in the maintable)
//                 - the row in the POLARIZATION table contains the
//                   polarization combinations as well as pointers into
//                   the polarization-type column of *any* row in the
//                   FEED table
//
//                Look at the following.
//                Assume there are 2 spectral windows defined [0,1] in the spectral
//                window table - SPECTRAL_WINDOW_ID is unique for each spectral window.
//                
//                Assume that for each spectral window the same polarization combinations
//                are computed by the correlator => POLARIZATION_ID is equal for both.
//                
//                As per the previous explanation, this implies we have two datadescription
//                IDs, since each [not enforced by the MS but it is best practice to
//                strive for database orthogonality (no duplicate info kept, basically)]
//                unique combination of (SPECTRAL_WINDOW_ID,POLARIZATION_ID) should give
//                a unique DATA_DESC(ription)_ID
//
//                Now assume the following table contents [irrelevant tables/columns left out]
//                Note: all *_ID values are direct indices into the tables they point at, 
//                ie they are rownumbers.
//
//                 MAIN-table:
//
//                      ANTENNA1  ANTENNA2  [..other cols..] DATA_DESC_ID
//                          0         1                           0
//                          0         2                           0
//                          0         1                           1
//                          0         2                           1
//                   * this describes four visibilities on two baselines,
//                     one visibility per baseline per subband [spectral window]
//                   * note: this is about the only case where a non *_ID column
//                     also is a direct index; the ANTENNA[1|2] columns are also
//                     direct indices (into the ANTENNA table of course...)
//                     
//                 FEED-table:
//
//                      ANTENNA_ID     [..other cols..]    POLARIZATION_TYPE
//                          0                                  [R,L]
//                          1                                  [R,L]
//                          2                                  [L,R]
//                    * describes the feeds on the individual antennea,
//                      antenna identified by ANTENNA_ID
//                    Note: this set of values is perfectly acceptable
//                    for casa/aips++, no errors/warnings etc
//
//                DATA_DESCRIPTION table
//                
//                      SPECTRAL_WINDOW_ID  POLARIZATION_ID
//                             0                 0
//                             1                 0
//                    * maps a single index to a two-index tuple
//
//               POLARIZATION table
//                     
//                    NUM_CORR   CORR_TYPE         CORR_PRODUCT
//                               (Int[NUM_CORR])   (Int[2,NUM_CORR])
//                        4      [6,7,8,9]         [(0,0), (1,1), (0,1), (1,0)]
//
//                    * added column array-dimensions here for clarity
//                    * CORR_TYPE contains integers which map uniquely
//                      to polarization types/combinations
//                      eg: 0=XX, 1=YY .. 6=RR, 7=LL, 8=RL .. 10=I, 11=Q (etc)
//                    * CORR_PRODUCT is an array of two-element tuples,
//                      each of the values in such a tuple is an index
//                      into the POLARIZATION_TYPE column in the FEED table.
//                      The first element of a tuple is the index for the
//                      first antenna in the product and the second index
//                      is the index for the second antenna.
//                    * note: order is important here: element 'i' in any of
//                      the arrays describes properties of row 'i' in the
//                      visibility datamatrix.
//
//
//            Good, still with me? Great.
//
//            Now lets take a closer look at baseline 0-2. There are two
//            visibilities: 1 and 3 counting from zero (which is default 
//            throughout).
//            Lets follow up the settings for visibility #1.
//            According to the main table it has DATA_DESC_ID 0.
//
//            So: Row #0 in the DATA_DESCRITPTION table tells us that the associated
//            POLARIZATION_ID is 0 [forget the spectral window id, it's irrelevant].
//            
//            Row #0 in the DATA_DESCRIPTION table tells us [leaving out irrelevant stuff]
//            that the CORR_PRODUCT for this polarization setup are
//                 (0,0) (1,1) (0,1) (1,0)
//            and the associated polarizationcombinations are
//                  RR     LL    RL    LR
//
//            Now looking in the feed table for the rows of the
//            participating antennea in this baseline (0 and 2)
//            this would map to:
//                antenna(0)  => poltype[0] = 'R'
//                               poltype[1] = 'L'
//                antenna(2)  => poltype[0] = 'L'
//                               poltype[1] = 'R'
//            hence unmapping the CORR_PRODUCT tuples according to *this*
//            you would find the polarization combinations for this polarization
//            setup are (again: order is important):
//                 RL     LR     RR    LL
//            which is obviously an internal inconsistency, QED.
//
//        Solution(s)
//
//        * remind yourself to write the same polarization_type column values for
//          all antennae in your FEED table. Simplest to do since as far as I know
//          we do not really care in which order the receptors are described.
//          
//        * If you wanted to, you could find out that in order to produce the correct
//          sequence 'RR LL RL LR' for baseline 0-2 you would have to write an
//          extra row in the POLARIZATION table with the following props
//
//               NUM_CORR     CORR_TYPES     CORR_PRODUCT
//                   4         [6,7,8,9]     [(0,1), (1,0), (0,0), (1,1)]
//          but now you also have to create two new rows in the DATA_DESCRIPTION table
//          (one for each spectral window id,polarization_id combination...)
//               SPECTRAL_WINDOW_ID   POLARIZATION_ID
//                      0                    1
//                      1                    1
//
//          and main would look like:
//
//               ANTENNA1       ANTENNA2   DATA_DESC_ID
//                  0               1          0
//                  0               2          2
//                  0               1          1
//                  0               2          3
//
//         which is both cumbersome for the software engineer but also for the
//         user since (a lot of) casa data selections are made by DATA_DESC_ID...
//         so you would have to find out that if you wanted data for a given 
//         subband/IF/spectral window, you'd have to find all the DATA_DESC_IDs
//         that have the SPECTRAL_WINDOW_ID you're interested in.
//
//    Just thought you should know...
void JIVEMSFiller::writeAntennaAndFeedTable( MeasurementSet& msref, const VisibilityBuffer& visbuf ) {
    MSAntenna&              antabref( msref.antenna() );
    const Experiment&       expref( visbuf.getExperiment() );
    const FrequencyDB&      freqdb( expref.getFrequencyDB() );
    ROMSAntennaColumns      roant( antabref );

    if( freqdb.nEntries()==0 ) 
        THROW_JEXCEPT("Experiment has no Frequency information? Cannot sensibly go on.");

    //  [Assumption: if antenna table has nr of rows != to zero, it has already been filled in...]
    //  18/01/2007 HV: Decided to change this. Let's ascertain that
    //                 [eg when concatenating datasets] whatever the
    //                 user wants to write is consistent with whatever
    //                 was already present in the MS.
    //                 The bummer is that we cannot let the Experiment decide the AntennaID -
    //                 in the MS it is a direct index [GRMBL!] so the only way we can have
    //                 a consistent labelling scheme is when the antenna at row 'i' in the
    //                 MS has an associated antennaId of 'i' in the experiment's AntennaDB.
    //                 Over here (ie in this method) we enforce that.
    AntennaInfo        expinfo;
    AntennaInfo        msinfo;
    const AntennaDB&   antdb( expref.getAntennaDB() );

    // Verify that antennas in the existing antennatable match up with what
    // the experiment has.
    // Note: we do not, at this point, check the following:
    //   1) if an existing antenna in the Antennatable has an (at least one)
    //      associated record in the FEED table, nor,
    //   2) check if that record, if it existed, is equal to what the
    //      experiment thinks it should be.
    // The loopholes here are:
    //   - we are appending data to a MS that we did not write ourselves
    //     [then we're not quite sure of _anything_, really]
    //   - we are appending data and user has modified the frequency setup
    //     for the current experiment between the last run and this one
    // Just how likely are these? The latter is not unthinkable... however, for
    // both loopholes you could argue: what the hell is the user doing?!
    //
    // This method makes sure that _if_ it adds an
    // entry to the antennatable, immediately an entry in the FEED table is
    // written too
    for( uInt i=0; i<antabref.nrow(); ++i ) {
        // if the antenna with ID 'i' is not found in the experiment that
        // is not a problem [assuming that it will never give data for that
        // antenna, as long as all the IDs present in the exp. match whatever's in
        // the MS
        if( !antdb.searchAntenna(expinfo, (const Int)i) )
            continue;

        // now verify it's the same in the MS & the experiment
        msinfo = AntennaInfo( roant.name()(i),
                              roant.station()(i),
                              MPosition( MVPosition((const Vector<Double>&)roant.position()(i)),
                                         MPosition::ITRF),
                              roant.dishDiameter()(i),
                              roant.mount()(i),
                              roant.offset()(i),
                              (Int)i );
        if( expinfo!=msinfo )
            THROW_JEXCEPT("Antenna with ID" << i << " is not the same in MS and the " <<
                          "experiment's database");
    }
   
    // ok - having checked that, it may be that the experiment has more antennas than
    // we have rows in the Antenna table (especially true if the antenna table is still
    // empty :)). In that case, we have to add the Antennas
    // that the Experiment defines to the MS's Antenna table.
    //
    // Turn the row<n> must hold antenna with id <n> argument around and bail out 
    // if there's nothing more to do
    if( antdb.nrAntennas()<=antabref.nrow() )
        return;
    
    // If we add an antenna, we will immediately write a corresponding entry
    // in the FEED table.
    // 
    // So we start at the current size of the antenna-table and loop until we
    // get at expref's AntennaDB's number-of-antennas. As mentioned before,
    // the antenna's MUST obey the following rule:
    //   ID_of_antenna_at_row<n> MUST BE <n> !!!!
    // [so the IDs of the antennas must be 0 -> (n-1)]
    // 
    //  [ASSUMPTION]
    //
    //  For now we will assume that our antennas have only one feed
    //  with up to two polarizations/feed. Feed #0 will be the feed used?
    //  Yes that's true. It sais explicitly so in the MS vs 2.0 def'n
    j2ms::log2polmap                 pmap0( visbuf.correlatorSetup().l2pmap() );
    MSFeed&                          feedref( msref.feed() );
    const Int                        numreceptors = pmap0.size();
    unsigned int                     idx;
    const Complex                    one = Complex(1.0, 0.0);
    const Complex                    nul = Complex(0.0, 0.0);
    MSFeedColumns                    feedcols( feedref );
	Vector<Double>                   vd;
    Vector<String>                   poltype( numreceptors );
    Array<Complex>                   dterms( IPosition(2,numreceptors,numreceptors) );
    MSAntennaColumns                 antabcols( antabref );
    j2ms::log2polmap::const_iterator curpmap;
   
    //  Form the vector of strings..
    //  Iterate over the l2pmap and write 'm in the order they appear.
    //  log2polmap is an ordered container so no worries about how the
    //  entries are inserted
    //  NOTE: do it like this because we may have a log2polmap with only
    //  one entry defining a logical index mapping of '1' ('one') to some
    //  j2ms::[xylr] which will have to be re-mapped to entry #0  :)
    for( curpmap=pmap0.begin(), idx=0; curpmap!=pmap0.end(); idx++, curpmap++ ) {
        ostringstream poltypstrm;

        poltypstrm << curpmap->second;
        poltype(idx) = poltypstrm.str();
    }
    //  Fill in the D-terms matrix.
    //  Assume our antennas are perfect... (ie no leakage ie. polresponse is ideal!)
    for( Int r1=0; r1<numreceptors; r1++ ) 
		for( Int r2=0; r2<numreceptors; r2++ )
	    	dterms( IPosition(2, r1, r2) ) = ((r1==r2)?(one):(nul));

    // ====> Now we start adding stuff to the tables; we've precomputed all we can
    for( uInt id = antabref.nrow(); id<antdb.nrAntennas(); id++ ) {
        uInt    feedrow;

        if( !antdb.searchAntenna(expinfo, (const Int)id) )
            THROW_JEXCEPT("Failed to locate info for Antenna w. ID " << id << " in experiment");
        
        // Get X,Y,Z position in meters
		vd = expinfo.getPosition().get("m").getValue();

        // whack the Antenna data into the antennatable!
        antabref.addRow();

		antabcols.name().put( id, expinfo.getName() );
		antabcols.dishDiameter().put( id, expinfo.getDiameter() );
		antabcols.mount().put( id, expinfo.getMount() );
		antabcols.offset().put( id, expinfo.getOffset() );
		antabcols.position().put( id, vd );
		antabcols.station().put( id, expinfo.getStation() );
		antabcols.flagRow().put( id, False );
		antabcols.type().put( id, "GROUND-BASED" );

        // and the FEED stuff
        feedrow = feedref.nrow();
        feedref.addRow();
        
		feedcols.antennaId().put( feedrow, id );
	
		//  [[ASSUMPTION]] Only one Feed for now
		feedcols.feedId().put( feedrow, 0 );

		//  [[ASSUMPTION]] Entries are valid for *all* spectral
		//  windows (indicated by a -1 value) for the whole
        //  experiment (time/interval == 0.0)
		feedcols.spectralWindowId().put( feedrow, -1 );
		feedcols.time().put( feedrow, 0.0 );
		feedcols.interval().put( feedrow, 0.0 );
		//  [[ASSUMPTION]] No BEAM associated
		Array<Double>     boffset( IPosition(2, numreceptors, numreceptors) );
		boffset = 0.0;
		feedcols.beamId().put( feedrow, -1 );
		feedcols.beamOffset().put( feedrow, boffset );

        // polarization stuff
		feedcols.numReceptors().put( feedrow, numreceptors );
		feedcols.polarizationType().put( feedrow, poltype );
		feedcols.polResponse().put( feedrow, dterms );
		feedcols.position().put( feedrow, Vector<Double>((uInt)3, 0.0) );
		feedcols.receptorAngle().put( feedrow, Vector<Double>((uInt)numreceptors, 0.0) );
    }

    //  Flush the table
    antabref.flush();
    feedref.flush();
    return;
}




const Matrix<Complex>& JIVEMSFiller::makeData( const Experiment& expref,
                                               const Visibility& vref )
{
    //  These are always there....
    static Matrix<Complex>          srcdata;
    static Matrix<Complex>          fftdata;
    static Matrix<Complex>          resdata;
    static FFTServer<float,Complex> transformator;

    //  Cache the visibility datadomains
    bool                    needfft;
    Visibility::domain      srcdomain( vref.getDatadomain() );
    Visibility::domain      dstdomain( expref.getDestinationDomain() );
    const Matrix<Complex>&  visdata( vref.getData() );

    // pre-empty the result data for 'error' return values
    resdata.resize(0, 0);

    //  Perform sanity checks
    if( srcdomain==Visibility::undefined || dstdomain==Visibility::undefined ) 
        return resdata;
  
    needfft = (dstdomain!=srcdomain);
    
    // If we have to do something but we
    // cannot do it because of lack of info, we return the empty
    // data as well.
    // There will be other cases of when we do not have enough
    // info to complete the request but we cannot do that in one
    // statement.
    if( needfft &&
        (!vref.hasVisibilityBuffer() || 
         (vref.hasVisibilityBuffer() && !vref.getVisibilityBuffer().getScanptr())) ) 
        return resdata;

    //  Locate the baseband we're looking at....
    //  If we cannot find it or it lacks certain
    const BaseBand*          bbptr( 0 );
    FrequencyConfig          fc;
    const j2ms::Scan&        scanref( *(vref.getVisibilityBuffer().getScanptr()) );
    const FrequencyDB&       freqdb( expref.getFrequencyDB() );
  
    // At this point we still may or may not need to do a FFT.
    // The following tests basically only apply when we need to
    // do a FFT
    if( needfft ) {
        if( !freqdb.searchFrequencyConfig(fc, scanref.getFrequencyCode()) )
            return resdata;
    
        if( (bbptr=fc.getBaseBand(vref.getLabel().subbandnr))==0 )
            return resdata;
    
        if( bbptr->getSideBand()==BaseBand::none )
            return resdata;
    }

    // NOW we have arrived at a point where we're certain that WHATEVER we
    // need to do (FFT or not) we have sufficient info to DO it.
    //
    // Whatever we need to do we MUST reorder (potentially...) the visibility
    // datamatrix in order to guarantee its rows are in the order the
    // experiment dictates.
    //  The simplest case is when the visibility data-domain and the
    //  desired output domain match...

    // HV: As of January 2007 decided to let the visibilitybuffer 
    //     deliver the data in a predefined order, we are not going
    //     to mess around with it; we just take the data as-is.
    //     We just copy it across to our working array...

    // be sure to resize the 'srcdata' thingy otherwise the 
    // assignment will throw at some point...
    srcdata.resize( IPosition(2,0,0) );
    srcdata = visdata.copy();

    // Now we may rest assured that the data in 'srcdata'
    // is in the order the experiment expects it.
    // NOW is the time to get the f**k out if we do not
    // have to do a FFT (but still returning the
    // data in the correct order)
    if( dstdomain==srcdomain )
	    return srcdata;

    //  When we arrive at this point in the program we've established
    //  a couple of facts:
    //
    //  1) source and destination domain for the data are NOT the
    //  same, hence we *need* to do a FFT
    //
    //  2) neither source nor destination domain is unknown, so no
    //  surprises are to be expected from those
    //
    //  3) we have located the baseband this data belongs to so we
    //  know exactly which part of the data we need to cut out (*if*
    //  we have to cut, that is)
    uInt                     fftsize;
    uInt                     nrffts;
    Bool                     expand;
    Bool                     fftflag;
    const Matrix<Complex>*   retvalptr;
    IPosition                srcshape( srcdata.shape() );
    
    //  What we do is: check if we need to expand the src data-matrix,
    //  i.e. if the source dat is an autocorrelation and the data is
    //  in time-domain, we need to expand the data (because we have
    //  already verified that the destination domain and the source
    //  domain are not equal and since the source domain (in this
    //  case) is time, the other must be frequency).
    nrffts  = srcdata.nrow();
    fftsize = srcdata.ncolumn();
    
    //  We need to expand the data if either of the following
    //  conditions holds:
    //
    //  1) we're going from time->frequency AND this is an
    //  autocorrelation (Albert gives us only half the number of lags
    //  in this case)
    //
    //  2) we're going from frequency->time domain
    //
    //  NOTE: the following two boolean variables will tell which (if
    //  any) of the two aforementioned conditions holds, they can
    //  *never* hold both (we make use of this property later on)
    Bool       expandcondition1;
    Bool       expandcondition2;
    
    //  30-09-1999: HV, explicitly set expandcondition 1 to False
    //  since this is (from this date on) already covered by the
    //  COFVisibilityBuffer class. Remains to be done for the
    //  DTDVisibilityBuffer!!! We can expect problems if people start
    //  reading data from this format! Changes to that are slim so
    //  forget about it for now!
    //
    //
    //expandcondition1 = ( vref.isAutoCorrelation() && srcdomain==Visibility::time );
    expandcondition1 = False;
    
    expandcondition2 = ( srcdomain==Visibility::frequency );
    
    if( (expand=(expandcondition1 || expandcondition2))==True )
	    fftsize *= 2;
    
    //  Now we can properly resize and initialise the 'fftdata' matrix
    fftdata.resize( IPosition(2,nrffts,fftsize) );
    fftdata = Complex( 0.0f, 0.0f );
    
    //  If we need to expand, do it now!
    if( expand ) {
        uInt     nr2copy( srcdata.ncolumn() );
	
        //  Depending on which expandcondition holds we take action
        if( expandcondition1 ) {
	        //  Autocorrelation && time->frequency
	        for( uInt i=0; i<nrffts; i++ ) {
                //  Copy all channels but channel0
                for( uInt lagpoint=1; lagpoint<nr2copy; lagpoint++ ) {
                    fftdata( i, nr2copy+lagpoint ) = 
                    fftdata( i, nr2copy-lagpoint ) = srcdata( i, lagpoint );
                }
                //  Copy channel 0 into the middle of the fftarray
                fftdata( i, nr2copy ) = srcdata( i, 0 );
            }
        } else /* expandcondition2==true */ {
            //  frequency->time, need to check sideband to know where
            //  to copy the data to
            uInt    startpoint;
	    
            if( bbptr->getSideBand()==BaseBand::upper )
		        startpoint = nr2copy;
	        else
		        startpoint = 1;

            //  Ok. Copy stuff
            for( uInt i=0; i<nrffts; i++ )
                (fftdata.row(i))( Slice(startpoint, nr2copy) ) = srcdata.row(i);
	    }
	} else /* no expand needed */ {
	    fftdata = srcdata;
    }
    
    //  Ok. fftdata is now properly sized and filled in, now we
    //  determine the FFT-flag and do the FFT
    //
    //  fftflag (=toFrequency argument)
    //  
    //  True  implies forward transform == time -> frequency
    //  False implies backward transform == frequency -> time
    fftflag = ( srcdomain==Visibility::time );
    
    //  Do the fft
    //  Do this row by row. Because the casa::Array (and derivatives)
    //  use reference semantics by default, taking out a
    //  row of a matrix and operating on that, modifies the
    //  storage underlying the matrix.
    for( uInt i=0; i<nrffts; i++ ) {
		Vector<Complex>   schijt( fftdata.row(i) );
		transformator.fft( schijt, fftflag );
    }
    
    //  Now we have to get out the data. If we were doing
    //  timetofrequency, we must keep only half of the resultant
    //  values. datastart tells us where to start in the fft'ed
    //  data. The value of datastart is determined by the sideband of
    //  the data: USB -> we take the upper half of the array, LSB ->
    //  the lower half.
    uInt     datastart( 0 );
    uInt     datasize( fftsize );
    
    if( fftflag ) {
        datasize /= 2;
	
        //  Which half to take depends on the sideband
        if( bbptr->getSideBand()==BaseBand::upper ) 
            datastart = datasize;
        else
            datastart = 1;
    }
    
    // datasize!=0 && datasize unequal to fftsize this means that we
    // have to <snip> the array...
    if( datasize && datasize!=fftsize ) {
        //  Now we can resize and fill the 'resdata'-matrix
        resdata.resize( nrffts, datasize );

        for( uInt i=0; i<nrffts; i++ ) {
            //resdata.row(i) = (fftdata.row(i))( Slice(datastart, datasize) );
            for( uInt j=0; j<datasize; j++ )
		        resdata( i, j ) = fftdata( i, datastart+j );
	    }
        retvalptr = &resdata;
    } else if( datasize ) {
        retvalptr = &fftdata;
    } else /* error condition?! */ {
        // make really sure resdata is empty
        resdata.resize(0, 0);
	    retvalptr = &resdata;
    }

    //  Return the result....
    return *retvalptr;
}

const Vector<Double>& JIVEMSFiller::calcUVW( const Visibility& vref  ) {
    //  These statics are here to speed up UVW-calculation; we don't need to 
    //  recreate these each call
    static Double         gastInRadians;
    static Double         currentHA;
    static MEpoch         currentGAST;
    static MEpoch         currenttime;
    static RotMatrix      currentRotMatrix;
    static Vector<Double> sourceRADEC;
    static Vector<Double> uvw( 3 );
    static Vector<Double> ant1xyz( 3 );
    static Vector<Double> ant2xyz( 3 );
    static Vector<Double> baseline( 3 );    

    //  Init retval to a neat zero-value
    uvw = j2ms::noUVWv;

    if( !vref.hasVisibilityBuffer() ||
    	(vref.hasVisibilityBuffer() &&
         !vref.getVisibilityBuffer().getScanptr()) ) {
    	return uvw;
    }
   
    // at least there is something known about the visibility, try to 
    // go on
    const j2ms::Scan&       scanref( *(vref.getVisibilityBuffer().getScanptr()) );
    const Experiment&       itsexpref( scanref.getExperiment() );
    ScanCacheElement*       scancacheptr;
    const ExtVisLabel&      labelref( vref.getLabel() );
    ExperimentCacheElement* expcacheptr( theirCache.addExperiment(itsexpref) );
    
    //  (attempt to)locate the Scan in the Scan-cache, since that entry contains
    //  all the conversion stuff needed for the coordinate
    //  transformations.
    if( !expcacheptr )
    	return uvw;
    scancacheptr = expcacheptr->addScan( scanref ); 

    if( !scancacheptr )
    	return uvw;

    //  Verify that we can find the antennainfo....
    const AntennaDB&      antdb( itsexpref.getAntennaDB() );
    const SourceInfo&     sourceinforef( scancacheptr->itsSourceInfo );
    AntennaInfo           ant1, ant2;
    
    if( !antdb.searchAntenna(ant1, labelref.baseline.idx1()) ||
        !antdb.searchAntenna(ant2, labelref.baseline.idx2()) )
        return uvw;

    //  Ok. Convert the current visibility time to GAST (Greenwich Sidereal Time)
    currenttime = vref.getTime();
    currentGAST = scancacheptr->timeCvt( currenttime );

    //  Get the source's RA in radians.
    //
    //  NOTE: the source position should be precessed to the current
    //  visibility epoch!! Leave that for later (we're using 2000
    //  coordinates, mostly, so the difference should be very
    //  little...
    sourceRADEC = ((sourceinforef.getDirection()).getValue()).get();

    //  Convert GAST to radians as well.... (in order to be able to
    //  derive the HA as seen from Greenwich)
    //
    //  1) start off with the fractional day in GAST
    //  2) multiply by 2pi to get at an angle in radians
    gastInRadians  = (currentGAST.getValue()).getDayFraction();
    gastInRadians *= ( 2*C::pi );

    //  Now we can form the HA
    currentHA = gastInRadians - sourceRADEC(0);
    
    // Ok. Now attempt to form the baseline
    //
    // 1) get the antenna XYZ-positions
    // 2) subtract them to get the baseline vector
    ant1xyz = ((ant1.getPosition()).getValue()).getValue();
    ant2xyz = ((ant2.getPosition()).getValue()).getValue();
    
    baseline = ant2xyz - ant1xyz;
    
    //  Ok. All that's left is a) to set up the rotation matrix and b)
    //  do the rotation!
    currentRotMatrix = RotMatrix( Euler(-(C::pi_2-sourceRADEC(1)), 1, (currentHA - C::pi_2), 3) );

    //  Finally: do the rotation!
    uvw = currentRotMatrix*baseline;

    return uvw;
#if 0
    //  Step one: make sure that the time (whatever frame it was in)
    //  gets converted to UTC and make that the time-reference for all
    //  future (in this function) calculations.
    //
    //  Get the time of this visibility and convert to UTC
    currentUTCtime = scancacheptr->timeCvt( currenttime );

    //  Set as referenceframe
    scancacheptr->referenceFrame.set( currentUTCtime );

    //  Convert antennapositions to ITRF
    ant1ITRFpos = scancacheptr->positionCvt( ant1.getPosition() );
    ant2ITRFpos = scancacheptr->positionCvt( ant2.getPosition() );
    
    //  Form the baseline in ITRF
    curbaseline = MBaseline( MVBaseline(ant2ITRFpos.getValue(), ant1ITRFpos.getValue()),
    			     scancacheptr->baselineRef );
    
    // Convert the phase-center (is source direction) to our current
    // reference frame
    scancacheptr->directionFrame.set( currentUTCtime );
    scancacheptr->directionRef.set( scancacheptr->directionFrame );
    phasecentercvt = scancacheptr->directionCvt( sourceinforef.getDirection() );
    
    //  Now we can make the UVW...
    currentuvw = Muvw( MVuvw(curbaseline.getValue(), phasecentercvt.getValue()), Muvw::ITRF );

    return (currentuvw.getValue()).getValue();
#endif
}


//  HV:  21-07-2000  With the arrival of MS v 2.0 I decided to write the 
//                   field table in one go. 
//       29-01-2007  Throws up if a problem. Verifies that, _if_ there are
//                   rows in the field-table, they are the same as in the
//                   experiment. FIELD_ID column in the main table
//                   is a direct index by now so the same comments as
//                   under the antenna table apply.
//                   Also stopped writing the SOURCE table. It is not 
//                   required nor does it really add more information
//                   (for our purposes)
void JIVEMSFiller::writeFieldAndSourceTable( MeasurementSet& msref,
											 const Experiment& expref )
{
    MSField&              fieldtab( msref.field() );
    MSSource&             sourcetab( msref.source() );
    SourceInfo            expinfo;
    MSFieldColumns        fieldcolumns( fieldtab );
    const SourceDB&       srcdb( expref.getSourceDB() );
	const IPosition       raindex(2,0,0);
	const IPosition       decindex(2,1,0);
    MSSourceColumns       sourcecolumns( sourcetab );

    // Verify that whatever is in the field-table, is consistent
    // with what's in the experiment
    for( uInt i=0; i<fieldtab.nrow(); ++i ) {
        // if the field with ID 'i' is not found in the experiment that
        // is not a problem [assuming that it will never give data for that
        // field, as long as all the IDs that _are_ present in the exp. match
        // whatever's in the MS
        if( !srcdb.searchSource(expinfo, (const Int)i) )
            continue;

        // build the SourceInfo from the msfield columns
        MDirection   srcdir;
        SourceInfo   msinfo;
        
        // Note: Assume J2000 - that is, _we_ write J2000 so.. 
        // We could switch to using TableMeasures (or wossname?) 
        // but that's not backwards compat.
        srcdir = MDirection( Quantum<Vector<Double> >(fieldcolumns.delayDir()(i), "rad") );
        if( expinfo!=SourceInfo(i, fieldcolumns.name()(i), "", srcdir) )
            THROW_JEXCEPT("Source mismatch between experiment & MS for source #" << i);
    }

    // Having verified that...
    if( srcdb.nrSources()<=fieldtab.nrow() )
        return;
    
    //  Find sourceindex....
    for( uInt idx=fieldtab.nrow(); idx<srcdb.nrSources(); idx++ ) {
		//  Ok. Attempt to locate the source-info
		if( !srcdb.searchSource(expinfo, (Int)idx) )
		    THROW_JEXCEPT("SourceInfo for source #" << idx << " not found.");

		fieldtab.addRow();

		Array<Double>    delaydir( IPosition(2,2,1) );
		Array<Double>    phasedir( IPosition(2,2,1) );
		Array<Double>    referencedir( IPosition(2,2,1) );
		Vector<Double>   coordinates;
		Vector<Double>   propermotion( 2 );
		Vector<Double>   position( 3 );
	
		position     = 0.0;
		propermotion = 0.0;

		// First write all those who are zeroes
		fieldcolumns.numPoly().put( idx, 0 );

		//  At the moment we have no clue as to what the ref. time is :-(
		fieldcolumns.time().put( idx, 0.0 );

		//  Fill in the values from the sourceinfo
		coordinates = (expinfo.getDirection().getValue()).get();
	
		//  Transfer to the arrays
		delaydir( raindex )      = coordinates( 0 );
		delaydir( decindex )     = coordinates( 1 );
		phasedir( raindex )      = coordinates( 0 );
        phasedir( decindex )     = coordinates( 1 );
		referencedir( raindex )  = coordinates( 0 );
		referencedir( decindex ) = coordinates( 1 );
	
		fieldcolumns.delayDir().put( idx, delaydir );
		fieldcolumns.phaseDir().put( idx, phasedir );
		fieldcolumns.referenceDir().put( idx, referencedir );
	
		fieldcolumns.name().put( idx, expinfo.getName() );

		//  Point at no row in the sourcetable in particular
		fieldcolumns.sourceId().put( idx, -1 );

		fieldcolumns.flagRow().put( idx, False );
	}
	fieldtab.flush();
    return;
}


// Update the processor table with a new pass_id
//
// * NO CHECKING FOR DUPLICATES IS DONE HERE * 
// The return value
// will be the newly added 'PROCESSOR_ID'.... the caller could, if he/she
// so desired, keep a map from 'pass_id' -> 'processor_id' and only call
// this function if they need to write a new 'pass_id'... (just a hint)
Int JIVEMSFiller::updateProcessorTable( MeasurementSet& msref,
									    Int newpassid,
									    const String& subtype)
{
    //  Test if we can get out ASAP (without any intelligent testing
    //  whatsoever..)
	Int                retval;
    MSProcessor&       processor( msref.processor() );
    MSProcessorColumns proccols( processor );
    
    //  Write a single row...
	retval = processor.nrow();

    processor.addRow();
    
    proccols.type().put( retval, "CORRELATOR" );
    proccols.subType().put( retval, subtype );
    proccols.typeId().put( retval, 0 );
    proccols.modeId().put( retval, 0 );
    proccols.flagRow().put( retval, False );
    proccols.passId().put( retval, newpassid );
    processor.flush();

    return retval;
}


Bool JIVEMSFiller::updateObservationTable( MeasurementSet& msref,
										   const Experiment& expref )
{
    MSObservation&        observation( msref.observation() );
    MSObservationColumns  obscols( observation );
    
    //  If Observation has no rows, make it so...
    if( observation.nrow()==0 ) {
		char          buf1[1024];
		char*         basenameptr;
		Array<String> as( IPosition(1,1) );
		
		as( IPosition(1, 0) ) = "???";
		observation.addRow();
	
		//  Fill in basic stuff once
		obscols.telescopeName().put( 0, expref.getArrayCode() );
		obscols.timeRange().put( 0, Vector<Double>(2, 0.0) );
		obscols.observer().put( 0, "JIVE" );
		obscols.log().put( 0, as );
		obscols.scheduleType().put( 0, "VEX" );
	
		//  Create the vexfilename....
		//
		//  NOTE: We do not create the absolute path since that would
		//  be useless...
		::strncpy( buf1, expref.getRootDir().c_str(), sizeof(buf1) );
		
		as( IPosition(1, 0) ) = "<error in getting basename>";
	
		if( (basenameptr=::basename(buf1))!=0 ) {
		    ostringstream strm;

			strm << basenameptr << ".vix";
			
			as( IPosition(1, 0) ) = strm.str();    
		}
		obscols.schedule().put( 0, as );
		
		obscols.project().put( 0, expref.getCode() );
	
		//  Set target release date in four weeks time!
		//
		//MVTime      today( (Time()) );
		Time        t;
		MVTime      today( t );
		MVTime      fourweeks( Quantity(28.0, "d") );
		MVTime      target = (today.get()+fourweeks.get());
		Double      targetvalue = (target.get()).getValue("s");
	
		//obscols.releaseDate().put( 0, ((today+fourweeks).get()).getValue("s") );
		obscols.releaseDate().put( 0, targetvalue );
	
		obscols.flagRow().put( 0, False );
	}
    // Ok. The table already got a row filled in.
    // Now all that we do is updating the TIMERANGE column but we can
    // only do that if there are rows in the MS
    if( msref.nrow() ) {
        MSMainColumns    mscols( msref ); 
        Vector<Double>   times( mscols.time().getColumn() );
        Vector<Double>   tr( 2 );

        minMax( tr(0), tr(1), times );

        obscols.timeRange().put( 0, tr );
    }
    //  [[TODO]] Lets find out the min/max time? Consider this done as of 08/02/2002
    return True;
}

Bool JIVEMSFiller::writeStateTable( MeasurementSet& msref, const Experiment& ) {
    MSState&       state( msref.state() );

    if( state.nrow()>0 )
	    return True;
    
    MSStateColumns statecols( state );
    
    state.addRow();

    statecols.sig().put( 0, True );
    statecols.ref().put( 0, False );
    statecols.cal().put( 0, 0.0 );
    statecols.load().put( 0, 0.0 );
    statecols.subScan().put( 0, 0 );
    statecols.obsMode().put( 0, "ON-SOURCE" );
    statecols.flagRow().put( 0, False );

    return True;
}

// entries in the modeltable have a unique key
// (explained further down below)
struct modelkey {
    casa::Int    antenna_id;
    casa::Double time;
	casa::Int    processor_id;

    modelkey( casa::Int ant, casa::Double tm, casa::Int proc ):
        antenna_id( ant ), time( tm ), processor_id( proc )
    {}
    
    private:
        // do not allow default modelkeys.
        modelkey();
};

// Less-than functor which implements
// the < operator for _modelkey thingies
// First order by antenna-id, then by time
struct _lt_modelkey {
    bool operator()( const modelkey& l, const modelkey& r ) const {
        return ( l.antenna_id<r.antenna_id ||
                 (l.antenna_id==r.antenna_id && l.time<r.time) ||
				 (l.antenna_id==r.antenna_id && l.time==r.time && 
				    l.processor_id<r.processor_id) );
    }
};

// this defines the modelcache. It is sufficient to
// remember if we have an entry for the modelkey
// since if that's granted, all info for all spectral windows
// is taken to be there
typedef std::set<modelkey,_lt_modelkey> modelcache_t;

// and an insert-result of inserting something into the cache
// which is pair<iterator,bool>, where the bool indicates if
// the insert was ok, if yes, then iterator points at the
// inserted element
typedef std::pair<modelcache_t::iterator,bool>  modelcacheinsertres_t;

// extract all the modeldata from the VisibilityBuffer and write it
// to the modeltable (if we didn't do that earlier)
void JIVEMSFiller::writeModel( MeasurementSet& ms,  const VisibilityBuffer& vb ) {
    try {
        // if the VisibilityBuffer does not offer a model - don't do anything!
        unsigned int              nrWritten;
        unsigned int              alreadyInMS;
        modelcache_t              modelcache;
        const Experiment&         eref( vb.getExperiment() );
        ExperimentCacheElement*   elptr;
        const j2ms::modeldata_t&  model( vb.getModelData() );

        if( model.size()==0 ) {
            cout << "writeModel/No modelrecords available" << endl;
            return;
        }
        if( (elptr=JIVEMSFiller::theirCache.locateExperiment(eref.getCode()))==0 ) {
            cout << "writeModel/No experimentinfo available in cache?!" << endl;
            return;
        }
        // Create a ro-reference to the frequency-cache for the experiment
        const fqcache_type&          fqcache( elptr->freqCache );
        fqcache_type::const_iterator curfq;

        // Read the entries in the JIVE_MODEL table; build up a cache
        // to see if we already got the data for a given modelentry or not.
        // The modelpolynomials are uniquely keyed on (TIME,ANTENNA) [that's
        // not totally true - eg if we ever do multiple field centers, this
        // assumption DOES NOT HOLD]. However, when Mark K. decided to define
        // the JIVE_MODEL table format this was deemed as being unnecessary
        // [the unique key is (TIME,ANTENNA,SOURCE)] since we do not do it now
        // so we won't do it in the future.
        // Just so you know why the code over here doesn't deal with that just
        // now.
		//
		// 06/07/2009 - HV: In order to support telling data from reruns 
		//              apart - they may have been run with different MODELs -
		//              we'll add the PROCESSOR_ID to the unique key.
		
		// First, let's find the PROCESSOR_ID (row) for the
		// current JIVE JOB ID (visbuf.getVisBufId()/passID column)
		int            curpassid = -1;

		{
			const MSProcessor&      proc( ms.processor() );
			ROMSProcessorColumns    proccols( proc );

			for( uInt i=0; curpassid<0 && i<proccols.nrow(); i++ )
				if( (int)proccols.passId()(i)==vb.getVisBufId() )
					curpassid = (int)i;
		}
		// If the passid not found .. then what??
		if( curpassid<0 ) {
			cerr << "Error: JobId #" << vb.getVisBufId() << " could not "
				 << "be found in the PROCESSOR table. This is an internal "
				 << "inconsitency. Report to the author verkouter@jive.nl.\n"
				 << "THE MODEL IS NOT WRITTEN FOR THIS JOBID!\n";
			return;
		}
        // Anyway - whatever the unique key, we need to build up a cache of
        // entries (unique keys suffice) that we already DID write to the MS.
	    Table                      modeltable( ms.rwKeywordSet().asTable("JIVE_MODEL") );
        Table                      spwintab( ms.rwKeywordSet().asTable("SPECTRAL_WINDOW") );
    	ScalarColumn<Int>          antennaid( modeltable, "ANTENNA_ID" );
    	ScalarColumn<Int>          feedid( modeltable, "FEED_ID" );
    	ScalarColumn<Int>          spwid( modeltable, "SPECTRAL_WINDOW_ID" );
    	ScalarColumn<Double>       time( modeltable, "TIME" );
    	ScalarColumn<Double>       interval( modeltable, "INTERVAL" );
		ScalarColumn<Int>          processor( modeltable, "PROCESSOR_ID" );
    	ArrayColumn<Double>        dryatmdel( modeltable, "DRY_ATMOS_DELAY" );
    	ArrayColumn<Double>        wetatmdel( modeltable, "WET_ATMOS_DELAY" );
    	ArrayColumn<Double>        clockoffs( modeltable, "CLOCK_OFFSET" );
    	ArrayColumn<Double>        grpdel( modeltable, "GROUP_DELAY" );
    	ArrayColumn<Double>        phsdel( modeltable, "PHASE_DELAY" );

        // Well, we do not have to get the whole table, a strategically
        // formulated TaQL query might save us a lot of trouble.
        // Note: we may leave out the SPECTRAL_WINDOW_ID since if a record
        // is written for a specific ANTENNA,TIME combination,
        // all the SPECTRAL_WINDOW_IDs get written to this table.
        // I.e. the *unique* key is just the (ANTENNA,TIME) tuple (take
        // heed, read above...)
		//
		// 06/07/2009 - HV: (ANTENNA,TIME) => (ANTENNA,TIME,PROCESSOR_ID)
        Table                  qres;
        TaQLResult             taqlres;
        ROScalarColumn<Int>    antidcol;
		ROScalarColumn<Int>    proccol;
        ROScalarColumn<Double> timecol; 

        // Just get the distinct values in the two columns we need; no 
        // need to sort. The std::set() + our key_compare will do that.
        taqlres = ::tableCommand("SELECT DISTINCT ANTENNA_ID, TIME, PROCESSOR_ID FROM $1", modeltable);
        qres    = taqlres.table();

        antidcol.attach( qres, "ANTENNA_ID" );
        timecol.attach( qres, "TIME" );
		proccol.attach( qres, "PROCESSOR_ID" );
        for( uInt row=0; row<qres.nrow(); row++ ) {
            modelcacheinsertres_t   insres;
        
            insres = modelcache.insert( modelkey(antidcol(row),
												 timecol(row),
												 proccol(row)) );
            if( !insres.second ) {
                cerr << "writeModel()/Error building modelcache - " << endl
                     << " failed to insert (antenna_id,time)=("
                     << antidcol(row) << "," << timecol(row)
					 << "," << proccol(row) << ") " << endl
                     << "Usually this means a duplicate but.... "
                     << "this should NOT happen; it means that the casa "
                     << "Table Query Language command 'SELECT DISTINCT <...>' "
                     << "did not work as expected." << endl
                     << "Visit http://casa.nrao.edu and complain!"
                     << endl;
                THROW_JEXCEPT("Internal error inserting entry in modelcache");
            }
        }
        // done building modelcache
        // Now go over the entries from the visbuf
        Matrix<Double>                       grp_delay;
        Matrix<Double>                       phs_delay;
        // shapes of the arrays (vectors of lengts 2 && coefficients_type::nrElements)
        // are constant
        const IPosition                      taylor1(1, 2);
        const IPosition                      rowshp(1, j2ms::coefficients_type::nrElements);
        j2ms::log2polmap                     l2p( vb.correlatorSetup().l2pmap() );
        const j2ms::scaninfolist_t&          scanlist( vb.getScanlist() );
        modelcache_t::const_iterator         cacheptr;
        j2ms::modeldata_t::const_iterator    curmodel;
        j2ms::scaninfolist_t::const_iterator curscan;

        // the shape of the coeffient matrices is known now 
        // [and will not change during execution of this fn]
        grp_delay.resize( IPosition(2, l2p.size(), j2ms::coefficients_type::nrElements) );
        phs_delay.resize( IPosition(2, l2p.size(), j2ms::coefficients_type::nrElements) );
        
        curfq       = fqcache.end();
        curscan     = scanlist.end();
        nrWritten   = 0;
        alreadyInMS = 0;
        
        for( curmodel=model.begin(); curmodel!=model.end(); curmodel++ ) {
            MVEpoch            curMVepoch( Quantity(curmodel->startMJDInSeconds, "s") );
            modelkey           curkey( curmodel->stationID,
									   curmodel->startMJDInSeconds,
									   curpassid );
            mepoch_in_scaninfo condition( curMVepoch );

            // if in cache - discard
            if( (cacheptr=modelcache.find(curkey))!=modelcache.end() ) {
                alreadyInMS++;
                continue;
            }

            // Find out the scan so's we can unmap every experiment based value
            // (subbands in the modeldatarecord are with respect to the frequency-setup
            //  and have no relations whatsoever to the spectral-window-id in the MS).

            // if time of current modelrecord outside current scan, start looking
            // for new scan [includes triggering of search if no current scan yet]
            if( curscan!=scanlist.end() && !condition(*curscan) )
                curscan=scanlist.end();
            // if no current scan yet/anymore go find the/a new one
            if( curscan==scanlist.end() ) {
                curscan = find_scaninfo_by_time(scanlist, curMVepoch);
                // if curscan, by now, is _still_ scanlist.end(), we're up a certain
				// creek. w/o paddle, even.
                if( curscan==scanlist.end() ) {
                    cerr << "writeModel: No scan found for time "
                         << MVTime::setFormat(MVTime::DMY) << MVTime(curMVepoch) << "/Skipping modelrecord" 
                         << endl;
                    continue;
                }
                // reset curfq to trigger looking up a new one!
                curfq = fqcache.end();
            }
            // woohoo, got a scan. See if we can find the associated freqcache stuff etc
            // NOTE: curscan is assumed to point at something sensible from here on
            if( curfq==fqcache.end() ) {
                if( (curfq=fqcache.find(curscan->frequencyCode))==fqcache.end() ) {
                    cerr << "writeModel: No frequencyinfo found in cache for mode "
                         << curscan->frequencyCode << "/Skipping modelrecord" << endl;
                    continue;
                }
            }
            // Both curscan/curfq are valid. Now we can find the subbands and map 'm to
            // MS spectral-window-IDs
            
            // Iterator over the entries in the modeldatarecord
            const fqprops_type&                       fqprops( curfq->second );
            j2ms::sb2polymap_type::const_iterator     cursb;
            j2ms::src2sbpolymap_type::const_iterator  cursrc;
            for( cursrc=curmodel->src2sbpolymap.begin();
                 cursrc!=curmodel->src2sbpolymap.end();
                 cursrc++ ) {
                // cursrc->first == experiments sourceId == MSSourceId [there should be
                //                  1:1 mapping there]
                // cursrc->second contains records for all subbands that we have modeldata for
                for( cursb=cursrc->second.begin(); 
                     cursb!=cursrc->second.end();
                     cursb++ ) {
                    // cursb->first == baseband# in the frequencyconfig for the scan
                    //                 this record applies to. Must be translated to
                    //                 a MS Spectral Window ID. no 1:1 mapping here...
                    // cursb->second contains the polys, organized by polarization
                    fqprops_type::const_iterator           mssbptr; 
                    const j2ms::pol2polymap_type&          p2pmap( cursb->second );
                    j2ms::log2polmap::const_iterator       curpol;

                    // Try to look up the current subband in the
                    // current frequencysetup-cache-element
                    if( (mssbptr=fqprops.find(cursb->first))==fqprops.end() ) {
                        cerr << "writeModel/No info in MS for subband#" << cursb->first
                             << " in mode " << curfq->first << "/Skipping modeldatarecord" << endl;
                        continue;
                    }
                    // Loop over the polarizations && put the coefficients in the right place
                    for( curpol=l2p.begin(); curpol!=l2p.end(); curpol++ ) {
                        int                                    polidx = l2p.index_of( curpol->second );
                        j2ms::pol2polymap_type::const_iterator curpolypair;

                        // locate the arrayindex of the current polarization
                        if( polidx<0 ) 
                            THROW_JEXCEPT("writeModel/Internal inconsistency. log2polmap entry " << *curpol
                                          << " returns index_of(" << curpol->second << ") = " << polidx);
                        // locate the polynomial-pair for current polarization. If not found, there's
                        // just no data for this src/subband/polarization
                        if( (curpolypair=p2pmap.find(curpol->second))==p2pmap.end() )
                            break;
                        // Data for polarization curpol->second should be put in row #polidx in the
                        // array
                        grp_delay.row( polidx ) = Vector<Double>(rowshp, curpolypair->second.delay.data);
                        phs_delay.row( polidx ) = Vector<Double>(rowshp, curpolypair->second.phase.data);
                    }
                    // if the data for not all of the polarizations was found, don't write the
                    // polys
                    if( curpol!=l2p.end() ) {
                        cerr << "writeModel/Data for polarization " << curpol->second << " missing in "
                             << "subband#" << cursb->first
                             << " @" << MVTime::setFormat(MVTime::DMY) << curMVepoch << "/Skipping record" << endl;
                        continue;
                    }

                    // Phew. Now we can safely add a row to the modeltable
                    uInt        nwrow( modeltable.nrow() );
                    
                    modeltable.addRow();
                    antennaid.put(nwrow, curmodel->stationID);
                    spwid.put(nwrow, mssbptr->second.__spwId);
                    feedid.put(nwrow, 0);
					processor.put(nwrow, curpassid);
                    time.put(nwrow, curmodel->startMJDInSeconds);
                    interval.put(nwrow, curmodel->durationInSeconds);
                    dryatmdel.put(nwrow, Vector<Double>(taylor1, curmodel->dryAtmosphere.data));
                    wetatmdel.put(nwrow, Vector<Double>(taylor1, curmodel->wetAtmosphere.data));
                    clockoffs.put(nwrow, Vector<Double>(taylor1, curmodel->clockOffset.data));
                    grpdel.put(nwrow, grp_delay);
                    phsdel.put(nwrow, phs_delay);
                }
            }
            nrWritten++;
            // if succesfull, insert into cache as well
            modelcacheinsertres_t   ires;
            
            ires = modelcache.insert( modelkey(curmodel->stationID,
                                               curmodel->startMJDInSeconds,
											   curpassid) );
            if( !ires.second )
                THROW_JEXCEPT("Failed to insert new entry into modelcache?!");
        }
        cout << "writeModel/Wrote " << nrWritten << " modelentries out of " << model.size()
             << "  [" << alreadyInMS << " already in MS]"
             << endl;
    }
    catch( const std::exception& e ) {
        cerr << "writeModel: " << e.what() << endl;
    }
    return;
}
