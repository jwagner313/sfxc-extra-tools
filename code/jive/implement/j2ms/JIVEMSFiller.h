//  JIVEMSFiller - a MS filler, dedicated to deal with the situation @
//  JIVE
//
//  i.e. Must be able to cope with 'Bos-format (=raw binary dump)' +
//  COF (Correlator Output Format)
//
//  Author:   Harro Verkouter,   10-12-1998
//
//  $Id: JIVEMSFiller.h,v 1.10 2011/03/02 16:09:58 jive_cc Exp $
//
#ifndef JIVE_J2MS_JIVEMSFILLER_H
#define JIVE_J2MS_JIVEMSFILLER_H

#include <casa/aips.h>

#include <casa/BasicSL/String.h>
#include <casa/Arrays/Vector.h>

#include <ms/MeasurementSets.h>
#include <casa/Arrays/Matrix.h>
#include <jive/j2ms/Experiment.h>
#include <jive/j2ms/ExperimentCache.h>
#include <jive/j2ms/subjob.h>
#include <jive/j2ms/Visibility.h>
#include <jive/j2ms/FrequencyConfig.h>
#include <jive/j2ms/VisibilityBuffer.h>
#include <jive/j2ms/Filter.h>
#include <vector>


//  The Filler-class 
class JIVEMSFiller
{
public:
    //  Create a JIVEMSFiller. The string argument is optional and
    //  may contain filler options.
    //  Currently only filter options recognized
    JIVEMSFiller( const casa::String& opt = "" );

    //  Do the job of adding the indicated scan to the MS of the
    //  indicated experiment. The boolean argument controls whether or
    //  not the user is asked to concatenate (if the MS already
    //  exists). true -> user is prompted; false -> data is
    //  concatenated blindly
    casa::Bool       fillDataToExperimentsMS( Experiment& expref,
										const subjob_t& sjdescr,
										casa::Bool ask=casa::True );

    //  Delete all the resources used by the JIVEMSFiller...
    ~JIVEMSFiller();

private:
    //  The filler's private parts

	// Option passed when the Filler was created....
	casa::String           myOptions;

	// Optionally, if the usr wanted filters to be applied:
	// let's keep a list of'm!
	std::vector<Filter*>  myFilters;

    //  The set of global databases:
    static ExperimentCache    theirCache;

    //  The private methods

    //  Attempt to create/open the MS for the indicated Experiment
    //  and set it up according to JIVE specs
	casa::MeasurementSet* makeMS( const Experiment& expref, casa::Bool ask );

    //  This function makes sure the visibility data is in the domain
    //  requested by the experiment. 
    //
    //  NOTE: this is ONLY for the datamatrix, nothing else!
    //
    //  NOTE: The contents of the matrix that is returned may or may
    //  not vary, since a reference to a static object is returned and
    //  the contents of the matrix may be changed after a call to
    //  makeData(). Therefore, copy the contents of the matrix to your
    //  own local variable to ensure proper operation.
    static const casa::Matrix<casa::Complex>& 
			makeData( const Experiment& expref,
					  const Visibility& vref );


    //  Fill the MS with the data from the scan
    casa::Bool fillMSfromVisibilityBuffer( casa::MeasurementSet& msref,
							       		   VisibilityBuffer& visbuf,
								           const subjob_t& sjdescr );

    //  Write the indicated frequency setup of the scan in the spectralwindow
    //  table of the given MS
    casa::Int updateSpectralAndPolarizationTable( 
				casa::MeasurementSet& msref,
				const Visibility& vref,
				const j2ms::Scan& scanref );

    //  Write all antennas in the experiment to the antennatable in
    //  the indicated MS.
    //  19/01/2007 HV - Updated. Does not return anymore. Throws.
    //                  If antenna's are listed in the MS's Antenna
    //                  subtable it verifies they are the same as in
    //                  the experiment. 
    //                  If they're not, we're up the creek. Equally,
    //                  we're also up the creek if we fail to write/
    //                  append to this table at all. So throwing up
    //                  seems like the right thing to do.
    static void writeAntennaAndFeedTable( casa::MeasurementSet& msref,
						                  const VisibilityBuffer& visbuf );

    //  Write entries in the FIELD and SOURCE table
    //  Used to be casa::Bool return valued but see comment @writeAntennaAndFeedTable
    static void writeFieldAndSourceTable( casa::MeasurementSet& msref,
							              const Experiment& expref );

    // This one read the MSSpectralWindow table, the MSFeed table, the MSDataDescriptionTable,
    // MSPolarization and the experiment's frequency-setups and try to link them together.
    // Necessitated by the fact that eg a single mode may be correlated multiple times -> in
    // order to keep our indices correct, we MUST keep track of which combinations in the MS
    // map to the same observed frequency band(s).
    //
    // The mapping is saved in JIVEMSFiller's ExperimentCache.
    // 
    // The "updateSpectralAndPolarization()" method appends/inserts stuff in this same
    // mapping as well
    static void buildFreqMapping( const casa::MeasurementSet& ms,
                                  const VisibilityBuffer& vb );
    

	// Updates the Processor subtable with a new row for a new passid
	casa::Int  updateProcessorTable( casa::MeasurementSet& msref,
									 casa::Int newpassid,
					 				 const casa::String& subtype );

    //  update the Observation table (for the min/max time in the MS?)
    casa::Bool updateObservationTable( casa::MeasurementSet& msref,
							           const Experiment& expref );
    

    //  write the state table
    casa::Bool writeStateTable( casa::MeasurementSet& msref,
								const Experiment& expref );

    //  Calculate the UVW-value for the Scan/Visibility combination
    //  (NOTE: data is taken from the experiment/scan cache)
    //
    //  IMPORTANT NOTE: this function returns a reference to static
    //  Vector-object. The vector is modified everytime the 'calcUVW'
    //  method is invoked. Therefore, don't rely on the fact that the
    //  values found in the vector are always the same and if you want
    //  to keep the values, you should make a local copy (not the
    //  copy-c'tor, because that uses reference semantics. Great isn't
    //  it?! Marvellous!)
    //
    //  NOTE: It may return JIVEMSFiller::noUVW in case of error
    static const casa::Vector<casa::Double>& calcUVW( const Visibility& vref );

    static void writeModel( casa::MeasurementSet& ms,
                            const VisibilityBuffer& vb );

    // For now, calculate the time of the visibility in the scan/frequencyconfig
    /*
    const casa::MEpoch&         calcVisibilityTime( const Scan& scanref, const Visibility& vref,
					      const FrequencyConfig& fcref );
					      */

    //  Do not allow these object to be copied/assigned to!
    JIVEMSFiller( const JIVEMSFiller& );
    const JIVEMSFiller& operator=( const JIVEMSFiller& );
};


#endif
