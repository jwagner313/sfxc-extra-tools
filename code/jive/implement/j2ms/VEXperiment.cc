//  VEXperiment - implementation of the VEXperiment class
//
//  Author:  Harro Verkouter,  5-7-1999
//
//
//  $Id: VEXperiment.cc,v 1.14 2008/03/25 13:30:11 jive_cc Exp $
//
//  $Log: VEXperiment.cc,v $
//  Revision 1.14  2008/03/25 13:30:11  jive_cc
//  kettenis: Implement conversion of the new SFXC software correlator output
//  format.  Disables the old FABRIC code.
//
//  Revision 1.13  2007/05/25 09:41:27  jive_cc
//  HV: - PCInt support is now optional. Only try the PCInt dataformat
//      if support is compiled in.
//      - removed some dead code
//
//  Revision 1.12  2007/03/15 16:35:43  jive_cc
//  VisibilityBuffer.cc
//
//  Revision 1.11  2007/02/26 15:36:59  jive_cc
//  HV: - cosmetic changes
//      - removed "has*DB()" methods - an experiment just HAS
//        to have those DBs
//
//  Revision 1.10  2006/08/17 13:41:43  verkout
//  HV: First version to succesfully support PCInt dataformat
//
//  Revision 1.9  2006/03/02 14:21:40  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.8  2006/02/10 08:53:44  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.7  2006/01/13 11:35:40  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.6  2003/09/12 07:27:39  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.5  2003/02/14 15:41:54  verkout
//  HV: vex++ now uses std::string. Have to change to using std::string as well.
//
//  Revision 1.4  2002/08/06 06:47:32  verkout
//  HV: * Built in support for passing options to the experiment from j2ms2
//  * In lieu of Huib's streamlining thingy, built in support to specify which toplevel VEXFile to use. Defaults to basename($CWD).vix.
//
//  Revision 1.3  2001/07/09 07:53:04  verkout
//  HV: Shitload of mods:
//  - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
//  - Enabled passing of options from j2ms2 commandline to an experiment
//  - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
//  - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
//  - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
//  - Enabled visibility filtering. At the moment only a source filter is implemented
//
//  Revision 1.2  2001/05/30 11:48:26  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:14:36  verkout
//  HV: imported aips++ implement stuff
//
//
#include <jive/j2ms/VEXperiment.h>

#include <casa/Exceptions.h>
#ifdef MARK4
#include <jive/j2ms/COFVisibilityBuffer.h>
#endif
// PCInt support req'd?
#ifdef PCINT
    #include <jive/j2ms/PCIntVisBuffer.h>
    // pcint ExceptionObject
    #include <myutil/ExceptionObject.h>
#endif
#include <jive/j2ms/SFXCVisBuffer.h>

#include <jive/Utilities/jexcept.h>


#include <sstream>
#include <cassert>

#include <libgen.h>

using namespace std;
using namespace casa;

VEXperiment::VEXperiment( const String& opt, const String& vexfile,
                          const String& exprootdir ) :
    Experiment( opt, exprootdir ),
    myVexFileptr( 0 ),
    mySourceDBptr( 0 ),
    myAntennaDBptr( 0 ),
    myFrequencyDBptr( 0 ),
    myDestinationDomain( Visibility::frequency )
{
    int           result;
    char          buffer[ 1024 ];
    char*         basenameptr;
    String        vexfilename;
    ostringstream strm;
    
    //  Get the directory we're in (this is supposed to be the
    //  epxperiment root-dir) and strip the experiment name from it
    //  (so we can form the name of the .vix file we need to open!
    ::strncpy( buffer, this->getRootDir().c_str(), sizeof(buffer)-1 );
    
    if( (basenameptr=::basename(buffer))==0 )
		THROW_JEXCEPT("VEXperiment - FAIL: get basename for experimentdirectory:"
                       << this->getRootDir());

    // Ok. Let's attempt to open the VEX-file
    // If nothing specified (ie vexfile.size()==0)
    // then fall back to the default "experiment-directory-name".vix
	if( vexfile.size()==0 ) {
        strm.str( string() );
		strm << this->getRootDir() << "/" << basenameptr << ".vix";
		vexfilename = String( strm.str() );
	} else {
		vexfilename = vexfile;
	}
    myVexFileptr = new VexPlus( vexfilename );

    if( (result=myVexFileptr->parseVex())<0 ) {
        ostringstream errstrm;
	
        errstrm << "VEXperiment - ";
	
        switch( result ) {
            case -1:
                errstrm << "could not open file " << vexfilename;
                break;
            case -2:
                errstrm << "error parsing vex-file " << vexfilename << " to memory";
                break;
            default:
                errstrm << "Unknown error?! Check VexPlus::parseVex() for error return codes!!";
                break;
        }
        THROW_JEXCEPT(errstrm.str().c_str());
    }

	cout << "VEXperiment - Using VEXfile=" << vexfilename << endl;

    //  Ok. Vex-file parsed to memory Ok. Now look for the *official*
    //  experiment-code.
    //
    //  NOTE: we don't need the basenameptr anymore; we've got the
    //  VEXfilename in mem by name (it's held in 'vexfilename' )
	myCode = myVexFileptr->ExperName();
    if( myCode.empty() )
        THROW_JEXCEPT("VEXperiment - Failed to get ExperimentCode from VEX-file "
                << vexfilename);

    //  Form the MS-name
    myMSname = this->getRootDir()+"/"+myCode+".ms";
    
    //  Form the databases. These throw is sumthing's fishy
    mySourceDBptr    = new VEXSourceDB( vexfilename );
    myAntennaDBptr   = new VEXAntennaDB( vexfilename );
    myFrequencyDBptr = new VEXFrequencyDB( vexfilename );

    //  And we're all set?
    //  No. Compile the antenna/source lists
    myAntennas = VEXperiment::getAntennaList( *myVexFileptr ).copy();
    mySources  = VEXperiment::getSourceList( *myVexFileptr ).copy();

    // and read the list of scans
    myScanList = scanlist( vexfilename );
}


const String& VEXperiment::getCode( void ) const {
    return myCode;
}

const String& VEXperiment::getMSname( void ) const {
    return myMSname;
}

void VEXperiment::setMSname( const String& newmsname ) {
    myMSname = newmsname;
    return;
}


Visibility::domain VEXperiment::getDestinationDomain( void ) const {
    return myDestinationDomain;
}

Bool VEXperiment::setDestinationDomain( Visibility::domain newoutputdomain ) {
    myDestinationDomain = newoutputdomain;
    cout << "VEXperiment " << myCode << " - data will now be written in the "
	 << myDestinationDomain << " domain.\n" << flush;
    return True;
}

const Vector<String>& VEXperiment::getAntennas( void ) const {
    return myAntennas;
}

const Vector<String>& VEXperiment::getSources( void ) const {
    return mySources;
}

const FrequencyDB& VEXperiment::getFrequencyDB( void ) const {
    return *myFrequencyDBptr;
}


const AntennaDB& VEXperiment::getAntennaDB( void ) const {
    return *myAntennaDBptr;
}

const SourceDB& VEXperiment::getSourceDB( void ) const {
    return *mySourceDBptr;
}

// VEXperiment should support PCInt datafiles as well as COF datafiles...
// Sooooh... let's firts try the COF -> if these don't seem to be 
// present, attempt PCInt datafile(s) [if support built in], 
// then try SFXC/SoftwareCorrelator format.
// If that's a no-go too, then give up!
VisibilityBuffer* VEXperiment::getVisibilityBuffer( const String& visbufname ) {
    string            vbo;
    String            vb( visbufname );
	unsigned int      jobid( 0 );
	unsigned int      subjobid( 0 );
    ostringstream     estrm;
    VisibilityBuffer* retval( 0 );

    // the visibility-buffer-identification
    // may have included options; the visbufid would look
    // like:
    // <jobid>/<subjobid>%<commaseparated options>
    string::size_type  percent = visbufname.find('%');
    if( percent!=string::npos ) {
        vb  = visbufname.substr(0, percent); 
        vbo = visbufname.substr(percent+1);
    }

    //  Both COF/PCInt visbuffers are identified by job/subjobid.
    //  SFXCVisBuffers may or may not have those. 
    //  If we cannot find the "jobid/subjobid" format, do not
    //  try COF/PCInt. SFXC is always tried
    //
	//  Parse the string; it should be in the form:
	//  <jobid>/<subjobid>
	if( ::sscanf(vb.c_str(), "%u/%u", &jobid, &subjobid)!=2 ) {
        // make sure these are set to '0' (zero); this will
        // indicate "do not check COF/PCInt dataformat" later on
        jobid    = 0;
        subjobid = 0;
    }

#ifdef MARK4
    // let's try to create a COFVisibilityBuffer first,
    // if that throws a "job_is_not_COF" exception,
    // try PCIntVisBuffer. Any other exception is
    // a problem
    try {
        //  Attempt the COFVisibilityBuffer...
        if( jobid && subjobid )
    	    retval = new COFVisibilityBuffer(jobid, subjobid, this, vbo);
	}
    catch( const job_is_not_COF& ) {
        cerr << "(COF-format): This job doesn't seem to be in COF format" << endl;
        retval = 0;
    }
#endif
// If PCInt support built in, try that!
#ifdef PCINT
    if( !retval ) {
        // try PCInt...
        try {
            if( jobid && subjobid )
                retval = new PCIntVisBuffer(jobid, subjobid, this, vbo);
        }
        catch( const std::exception& e ) {
            estrm << "(PCInt-format): " << e.what() << endl;
            retval = 0;
        }
        catch( const ExceptionObject& eo ) {
            estrm << "(PCInt-format): " << eo << endl;
            retval = 0;
        }
        catch( ... ) {
            estrm << "(PCInt-format): Caught unknown type of exception" << endl;
            retval = 0;
        }
    }
#endif
    if( !retval ) {
        // Try SFXC...
        try {
            retval = new SFXCVisBuffer(vb, this, vbo);
        }
        catch( const std::exception& e ) {
            estrm << "(SFXC-format): " << e.what() << endl;
            retval = 0;
        }
        catch( ... ) {
            estrm << "(SFXC-format): Caught unknown type of exception" << endl;
            retval = 0;
        }
    }
    if( !retval ) {
        cerr << estrm.str();
    }
    return retval;
}

const j2ms::scaninfolist_t& VEXperiment::getScanlist( void ) const {
    return myScanList;
}

Bool VEXperiment::hasVernierDelay( void ) const {
    return True;
}




//  Delete allocated resources
VEXperiment::~VEXperiment() {
    delete myVexFileptr;
    delete mySourceDBptr;
    delete myAntennaDBptr;
    delete myFrequencyDBptr;
}


//  The private methods
const Vector<String>& VEXperiment::getAntennaList( const VexPlus& vpref ) {
    int                     cnt;
    static Vector<String>   antennas;
    
    antennas.resize( vpref.N_Stations() );
    
    for( cnt=0; cnt<vpref.N_Stations(); cnt++ )
	    antennas( cnt ) = vpref.Station( cnt );
    return antennas;
}

const Vector<String>& VEXperiment::getSourceList( const VexPlus& vpref ) {
    int                     cnt;
    static Vector<String>   sources;
    
    sources.resize( vpref.N_Sources() );
    
    for( cnt=0; cnt<vpref.N_Sources(); cnt++ )
	    sources( cnt ) = vpref.SourceName( cnt );
    return sources;
}
