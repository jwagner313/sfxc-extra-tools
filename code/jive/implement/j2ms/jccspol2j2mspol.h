// define translation between JCCS::datahandler::InterferometerDescriptor::Polarization type
// and j2ms::polarization type
#ifndef JIVE_J2MS_JCCSPOL2J2MSPOL_H
#define JIVE_J2MS_JCCSPOL2J2MSPOL_H

// for the JCCS polarization defintions
#include <data_handler/InterferometerDescriptor.h>

// for the j2ms definitions
#include <jive/labels/polarization.h>

// global function to go from JCCS -> j2ms
// May return j2ms::unknown if p is something
// unrecognized
j2ms::polarization jccspol2j2mspol( InterferometerDescriptor::Polarization p );

#endif
