//  Implementation of the AntennaInfo class...
//
//
//  Author:   Harro Verkouter, 1-12-1998
//
//  $Id: AntennaInfo.cc,v 1.6 2007/02/26 10:40:37 jive_cc Exp $
//
#include <jive/j2ms/AntennaInfo.h>
#include <jive/Utilities/jexcept.h>

// for "nearAbs()"
#include <casa/BasicMath/Math.h>

// for "allNearAbs(Array<>&, Array<>&, tolerance)"
#include <casa/Arrays/ArrayLogical.h>

#include <iomanip>

using namespace std;
using namespace casa;

ostream& operator<<( ostream& os, const AntennaInfo& airef ) {
    os << "Antenna#" << airef.itsId << " = "
       << airef.itsTLC << " (" << airef.itsName << "@" 
       << airef.itsStation << ") POSn: " << airef.itsPosition
       << ", D:" << airef.itsDiameter << "m"
       << ", MOUNT:" << airef.itsMount << ", OFFSET:" << airef.itsOffset;
    return os;
}

// Default Construct a complete object
AntennaInfo::AntennaInfo( ) :
    itsName( "<UNSET>" ),
    itsTLC( "XX" ),
    itsStation( "<UNSET>" ),
    itsMount( "none" ),
    itsDiameter( 0.0 ),
    itsOffset( Vector<Double>(3, 0.0) ),
    itsId(-1)
{}

//  Construct a complete object
AntennaInfo::AntennaInfo( const String& name,
               const String& station,
			  const MPosition& pos, Double diameter,
			  const String& mount,
			  const Vector<Double>& offset,
              Int antId,
              const String& tlc) :
    itsPosition( pos ),
    itsName( name ),
    itsTLC( tlc ),
    itsStation( station ),
    itsMount( mount ),
    itsDiameter( diameter ),
    itsId( antId )
{
    Int   len;

    offset.shape( len );
    if( len!=3 )
        THROW_JEXCEPT("offset-vector does not have (expected) length 3; it is " << len);

    // having asserted that the vector 'offset' has the appropriate length, we
    // can simply copy it across into ourselves
    itsOffset = offset;
}

const String& AntennaInfo::getName( void ) const {
    return itsName;
}

const String& AntennaInfo::getTLC( void ) const {
    return itsTLC;
}


const String& AntennaInfo::getStation( void ) const {
    return itsStation;
}

const String& AntennaInfo::getMount( void ) const {
    return itsMount;
}

const MPosition& AntennaInfo::getPosition( void ) const {
    return itsPosition;
}

Double AntennaInfo::getDiameter( void ) const {
    return itsDiameter;
}

const Vector<Double>& AntennaInfo::getOffset( void ) const {
    return itsOffset;
}

Int AntennaInfo::getId( void ) const {
    return itsId;
}

bool AntennaInfo::operator==( const AntennaInfo& other ) const {
    // Note: the tolerances used here (1.0e-7) are _somewhat_
    // arbitrary albeit not totally -> they are of the order
    // of the numerical noise you get when rounding/computing with floats
    // on different CPUs. Eg same computation on UltraSPARC/amd64
    // may introduce numerical differences of this order of magnitude.
    // [or, doing a atof() on the same ASCII string may produce
    //  differences of this magnitude between systems in use @JIVE]
    //
    // We are using doubles for the values so the tolerance possibly
    // could be lower but I think it's accurate enough for now.
    return ( itsName==other.itsName &&
             /*itsTLC==other.itsTLC &&*/ /* See comment in AntennaInfo.h */
             itsStation==other.itsStation &&
             itsMount==other.itsMount &&
             nearAbs(itsDiameter, other.itsDiameter, 1.0e-7) &&
             allNearAbs(itsOffset, other.itsOffset, 1.0e-7) &&
             allNearAbs(itsPosition.get("m").getValue(),
                        other.itsPosition.get("m").getValue(),
                        1.0E-7) &&
             itsId==other.itsId );
}

bool AntennaInfo::operator!=( const AntennaInfo& other ) const {
    return !(*this==other);
}

AntennaInfo::~AntennaInfo()
{}

