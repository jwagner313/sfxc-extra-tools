#if 0
//  Implementation of the COF visibilitybuffer class
//
//  Author:  Harro Verkouter,  31-05-1999
//
//  $Id: COFVisibilityBuffer.cc,v 1.26 2011/10/12 12:45:28 jive_cc Exp $
//
//  $Log: COFVisibilityBuffer.cc,v $
//  Revision 1.26  2011/10/12 12:45:28  jive_cc
//  HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//        changed into 'labels' for more genericity
//      * can now be built either as part of AIPS++/CASA [using makefile]
//        or can be built as standalone set of libs + binaries and linking
//        against casacore [using Makefile]
//        check 'code/
//
//  Revision 1.25  2011-03-02 16:09:57  jive_cc
//  kettenis: Differentiate between output of the MarkIV correlator and SFXC by
//  setting the SUB_TYPE in the PROCESSOR table to "SFXC" for the latter.
//
//  Revision 1.24  2009/03/06 12:45:56  jive_cc
//  HV: * Aaaargh! There was a nasty bug in an (apparently!) untested codepath:
//        crosspolarization products would end up in the wrong row of the datamatrix
//        in the MS when vexfileorder and correlation order were different for
//        the baseline.
//        j2ms2 was fully prepared to handle this transpartently only the path
//        contained a bug. Appartently the "swap" condition was never triggered
//        until Albert decided, some time in 2008 to change the correlationorder
//        to not match the vexfileorder any more.
//        The code is now fixed and tested and as a result it now really does not
//        matter in which order the correlator computes the products, as was the
//        original idea.
//
//  Revision 1.23  2008-08-08 11:28:10  jive_cc
//  HV: aaaaiiiiii! Integer-divide bug! Bob reported that the visibility times
//      were not corrected anymore. I did an integer divide (delta-bocf over
//      bocfrate) which resulted in 0, multiply by SYSCLOCK_RATE = still 0!
//      Now fixed by making it a float divide .... Woops!
//
//  Revision 1.22  2008-07-31 10:47:34  jive_cc
//  HV - 31/07/2008  New version of Albertsoftware (3.0): the diff between
//                   header & data is now only 1 BOCF, rather than 2, what
//  		 it used to be up until now. Modifications implemented
//  		 that set the actual value of the diff based on AB
//  		 s/w version. Name of the entry in the JIVE_PROCESSING table
//  		 (2BOCF_FIX or something) left unchanged but the actual
//  		 amount of time-fixing *is* altered. The name of the entry
//  		 left untouched to not harm downstream apps that may
//  		 rely on this name.
//
//  Revision 1.21  2007-12-14 15:05:47  jive_cc
//  HV: * fixed bug where, if data was found *outside* any known scan, the
//        system would throw up, "failing to unmap". Currently, the system
//        signals �please build a mapping, as soon as you find data that
//        actually comes from a scan" [can't unmap to physical quantities
//        w/o the metadata from the scandefinition].
//        Visibilities for which no 'unmapping' is defined are labelled
//        with invalid labels + get tagged as having 'no scan'. The decision
//        of what to do with this data is (as it has been for ages) left
//        to higher-level code.
//      * Changed the flow a bit when a new integration has been read from
//        file: now doesn't have for/if/etc three levels deep, is more
//        clean now.
//
//  Revision 1.20  2007/05/25 09:33:22  jive_cc
//  HV: - cleaning up
//      - add support for recording JIVE specific processing steps
//      - change from homegrown RegularExpression class to casa::Regex
//
//  Revision 1.19  2007/03/15 16:32:43  jive_cc
//  HV: CorSWVersion removed as nested class of CorrelatorSetup and renamed to SWVersion instead
//
//  Revision 1.18  2007/02/26 12:46:50  jive_cc
//  HV: Aiieeeeee :) Totally reworked the implementation.
//      10 - 20 percent speedup achieved AS well as much
//      MUCH more improved consistency. Read on for what
//      happened:
//      * Changed definition of MFC-Setup. Simplified code
//      enormously: only one MFCSetup in use; used to be two
//      (default and optional MFC)
//      * readMFCSetup made more lenient, will now also accept
//      just a CRMSHUFFLE command - vital for replicating SU
//      channels w/o having to hack the vexfile and still get j2ms2
//      to label the channels with the proper frequency.
//      * Reduced per-visibility overhead SO MUCH... a lot of
//      values that only change per integration used to be
//      re-computed for every visibility, now they're cached and
//      refreshed only with a new integration.
//      * Unmapping is also pre-computed; used to be done
//      per-visibility as well whilst 99 times out of a 100
//      it is constant for the whole job. (unmapping from
//      indices found in the DataTransferDescriptor to
//      VEX-based indices, this is).
//      * Data delivery is now almost as efficient as can be:
//      all selecting, reordering and swapping is now taken
//      care of in one stage rather than two or three.
//      * Retained full MFC capability I hope.
//      * Rigorous consistency checks back in - they were
//      commented out.
//      * Removed superflous scanlists.
//      * Pre-compute and cache extra mappings eg vex-to-subband.
//      * Support for reading MODEL files and tag the records with
//      correct labels ie VEXbased indexing vs JCCSbased
//
//  Revision 1.17  2006/08/17 13:43:17  verkout
//  HV: Detection of wether or not the job is COF or PCInt * interface change
//
//  Revision 1.16  2006/03/02 14:21:38  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.15  2006/02/17 12:41:26  verkout
//  HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//
//  Revision 1.14  2006/02/10 08:53:43  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.13  2006/01/13 11:35:36  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.12  2005/11/02 08:21:02  verkout
//  HV: JCCS has moved on and the countedpointer now *is* in the JCCS namespace. Yippie.
//
//  Revision 1.11  2005/03/16 07:27:55  verkout
//  HV: *rats* the JCCS:: namespace for CountedPointer was not yet propagated to the JCCS codebase (it's in the Linux port of JCCS which is not yet merged) so had to take out these changes
//
//  Revision 1.10  2005/03/15 14:45:59  verkout
//  HV:  * made sure to use JCCS::CountedPointer (explicit scope)
//       * more importantly: scan-based filtering made possible.
//         when compiling the list of scans, the scanname is taken from
//         the vexfile and the number contained therein will be the
//         scannumber. Also the scanname is now the primary key in scan-lookup
//         rather than the scanstart-time.
//
//  Revision 1.9  2004/08/25 05:57:39  verkout
//  HV: * Got rid of 'uint16' (replaced with 'unsigned int').
//        (not everywhere yet)
//      * Started using CountedPointers to DataFileRecords in
//        COF* classes (such that datafilerecords, read from file
//        get automagically deleted when nobody refers to them
//        anymore
//      * Added some exception catching here and there...
//
//  Revision 1.8  2004/01/05 15:01:49  verkout
//  HV: Probs. with templates. Moved Container to different namespace.
//
//  Revision 1.7  2003/09/12 07:26:39  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.6  2003/02/14 15:38:46  verkout
//  HV: Use std::string
//
//  Revision 1.5  2002/08/06 06:31:24  verkout
//  HV: * Built in support for MultipleFieldCenter correlation.
//  * Released the requirement that there should be one station in the vexfile that is present in all scans, the system now just finds all scans.
//  * Fixed bug where visibilities whose midpoint (time) was _in_ a scan definition and one of the edges was outside were passed through. Visibilities are now only accepted if they are completely within the scandefinition (timewise)
//
//  Revision 1.4  2001/07/09 07:52:27  verkout
//  HV: Shitload of mods:
//  - Fixed name-clash between class in j2ms2 and class in JCCS-code; put the j2ms class in a namespace.
//  - Enabled passing of options from j2ms2 commandline to an experiment
//  - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
//  - Beautified output, most notacibly in printing out Scan info when j2ms2 detects a new scan whilst filling an MS
//  - Removed obligatory J2MSDBPATH env. var set before j2ms2 is run. Defaults to /tmp
//  - Enabled visibility filtering. At the moment only a source filter is implemented
//
//  Revision 1.3  2001/05/30 11:47:04  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.2  2000/06/07 15:12:07  verkout
//  HV: Drat! A nasty bug in getting the baseline data out of the files....But! It\'s fixed
//
//  Revision 1.1.1.1  2000/03/20 15:14:35  verkout
//  HV: imported aips++ implement stuff
//
//
//

// local
#include <jive/j2ms/COFVisibilityBuffer.h>
#include <jive/j2ms/Visibility.h>
#include <jive/j2ms/DayConversion.h>
#include <jive/j2ms/COFScan.h>
#include <jive/j2ms/CorrelatorSetup.h>
#include <jive/j2ms/scanlist.h>
#include <jive/j2ms/VEXperiment.h>
#include <jive/j2ms/FrequencyConfig.h>
#include <jive/j2ms/FrequencyDB.h>
#include <jive/j2ms/rot2mepoch.h>
#include <jive/j2ms/noUVW.h>
#include <jive/labels/CorrelationCode.h>
#include <jive/Utilities/jexcept.h>

// casa stuff
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Quanta/MVTime.h>

#include <measures/Measures/MEpoch.h>

#include <casa/Utilities/Regex.h>


//  JCCS includes (for COF etc)
#include <fundamental/ccc_constants.h>

#include <data_handler/InterferometerDescriptor.h>
#include <data_handler/MappingsFile.h>
#include <data_handler/MappingEntry.h>
#include <data_handler/CorrelatedDataFile.h>
#include <data_handler/DataFileRecord.h>
#include <data_handler/DataFile.h>
#include <data_handler/CorrelatedDataRecord.h>
#include <data_handler/DataDescriptor.h>
#include <data_handler/MappingEntry.h>
#include <data_handler/DataFile.h>
#include <data_handler/ModelFile.h>
#include <data_handler/ModelDataRecord.h>

// std c++ 
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>

#include <string.h>    
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>

// for, well, you figure it out...
#include <dirent.h>

// for dirname()
#include <libgen.h>


using std::cerr;
using std::endl;
using std::setw;
using std::flush;
using std::stringstream;
using std::ostringstream;
using std::istringstream;
using std::ends;
using std::make_pair;
using std::back_insert_iterator;

using j2ms::scaninfolist_t;
using j2ms::modeldata_t;

using JCCS::CountedPointer;

using namespace casa;


ostream& operator<<( ostream& os, const mfcSetup& s ) {
	alias_type::const_iterator   curalias;
	vector<int>::const_iterator  iter;
	
	os << "MFCSetup:" << endl;

	for( curalias=s.srcToAliasMap.begin();
		 curalias!=s.srcToAliasMap.end();
		 curalias++ ) {
		vector<string>::const_iterator  aptr;
		
		os << "Source: " << curalias->first << " - ";
        for( aptr=curalias->second.begin();
             aptr!=curalias->second.end();
             curalias++ )
                os << *aptr << " ";
		os << endl;
	}

	// list the crm-shuffle
	os << "CRM: ";
	iter = s.physToLogMap.begin();

	while( iter!=s.physToLogMap.end() ) {
		os << setw(2) << *iter << " ";
		iter++;
	}
	os << endl;
	
	// and the source shuffle
	os << "SRC: ";
	iter = s.physToFieldCenterMap.begin();

	while( iter!=s.physToFieldCenterMap.end() ) {
		os << setw(2) << *iter << " ";
		iter++;
	}
	os << endl;

	return os;
}

// Default mfcSetup
mfcSetup::mfcSetup() :
	physToLogMap( COFVisibilityBuffer::defaultPhysToLogMap() ),
	physToFieldCenterMap( COFVisibilityBuffer::defaultPhysToSrcMap() )
{}

COFVisibilityBuffer::unmapped_type::unmapped_type(unsigned int xant, unsigned yant,
                                                  unsigned int sb, unsigned int src):
    XAnt(xant), YAnt(yant), Subband(sb), Source(src)
{}

//  The implementation of the COFVisibilityBuffer class
//
// For now the 'vbopt' parameter is discarded by this
// type of visibilitybuffers
COFVisibilityBuffer::COFVisibilityBuffer( unsigned int jobid, unsigned int subjobid,
										  const VEXperiment* experimentptr,
                                          const std::string& /*vbopt*/) :
    VisibilityBuffer( experimentptr ),
    myJobID( jobid ),
    mySubJobID( subjobid ),
    myMappingsFileptr( 0 ),
    myCorrelatedDataFileptr( 0 )
{
    int             trycounter;
    int             lastvalid;
    bool            done;
    String          vexfilename;
    MVTime          scanmvtime;
    struct stat     statbuffer;
    ostringstream   strm;
    
    //  Form the directoryname where we should look for data in the first
    //  place...
    strm << this->getExperiment().getRootDir() << "/" << myJobID << "/"
		 << mySubJobID;
    
    myRootDir = strm.str();
    
    //  Ok. 'myRootDir' contains something like:
    //
    //  <experiment-root-dir>/<jobid>/<subjobid>
    //
    //  This is only the beginning, there may be multiple correlations
    //  present for this subjob. If more than one correlation is
    //  present, the data is found in directories named as follows:
    //
    //  <myRootDir>.<x>
    //
    //  where <x> is an integer >0. Attempt to find the highest number
    //  and we'll use that.
    //
    done = false;
    trycounter = 0;
    lastvalid = -1;

    while( !done ) {
		strm.str( string() );
		strm << myRootDir;

		if( trycounter )
			strm << "." << trycounter;
	
		//  Try to stat the thing. If it's a directory, we'll try the
		//  next one!
		if( ::stat(strm.str().c_str(), &statbuffer)<0 ) {
            if( errno!=ENOENT )
                THROW_JEXCEPT("Something's fishy with " << strm.str());
			//  Failed to stat -> we're done?!
			done = true;
			continue;
		}
	
		//  Ok. Stat succeeded. Now... if it's not a directory, we'll
		//  skip it (i.e. leave the 'lastvalid' variable alone)
		if( (statbuffer.st_mode&S_IFDIR)==S_IFDIR ) 
			lastvalid = trycounter;
		trycounter++;
	}

    //  Ok. 'lastvalid' holds the value of the last 'valid'
    //  directory. If it's less than zero -> nothing valid was found.
    //  If it is 'zero' it is just the initial path without any ".<digit>"
    //  suffix [important for a few lines below this!]
    if( lastvalid<0 )
		THROW_JEXCEPT( "COFVisibilityBuffer: " << myJobID << "/" << mySubJobID
			           << " not found?!" );
    
    //  Really form the full directory path
    strm.str( string() );
    strm << myRootDir;

    // if there multiple versions, take the last-known-valid
    // [see comment a few lines above]
    if( lastvalid )
		strm << "." << lastvalid;
    
    //  replace rootdir with finalized result...
    myRootDir = strm.str();

    // HV: 07/02/2006 - insert a check here.
    //                  We want to auto-detect if this job
    //                  is PCInt or COF.
    //                  If the job is COF, the MAP and CDF
    //                  files *must* be there and have a size
    //                  greater than just the headers!
    //                  (the typical header is ~ 64 bytes)
    //                  but if anything is written to the file,
    //                  a size of 128bytes is a "safe" *ahem*
    //                  minimum [the typical records written to
    //                  MAP and CDF are *much* larger than that]
    int           mapres, cdfres;
    struct stat   mapstat, cdfstat;

    if( (mapres=::stat((myRootDir+"/MAP").c_str(), &mapstat))!=0 ||
        (cdfres=::stat((myRootDir+"/CDF").c_str(), &cdfstat))!=0 ||
        (mapres==0 && mapstat.st_size<=128) ||
        (cdfres==0 && cdfstat.st_size<=128) )
    {
        // most likely not COF
        throw job_is_not_COF();
    }
    
    //  Attempt to create files. They throw exceptions when they fail!
    myMappingsFileptr = new MappingsFile( (myRootDir+"/MAP").c_str() );
    myCorrelatedDataFileptr = new CorrelatedDataFile( (myRootDir+"/CDF").c_str() );

    //  Build a list of scan start/stop times so we can easily check
    //  against the visibility-time if a new scan was detected...
    strm.str( string() );
    strm << this->getExperiment().getRootDir() << "/" << myJobID << "/"
		 << this->getExperiment().getCode() << "_" << myJobID << ".vex";
    
    vexfilename = strm.str();
    
    //  Read the correlator setup from the comment in the vexfile???
	mySetup = CorrelatorSetup::correlatorSetupFromFile( string(vexfilename.c_str()) );
	if( mySetup==CorrelatorSetup() )
		THROW_JEXCEPT("COFVisibilityBuffer - failed to read correlatorsetup!");

	// Now it's about time to start thinking about MultipleFieldCentre stuff. What we
	// do is check if the experiment has the mfc= option set. We assume that it
	// indicates the name of the MFC-config file.
	// From this file we deduce the source-aliases and the mapping of physical station
	// unit output to logical vex-channel number
	const Experiment& expref( this->getExperiment() );
	String            exproot = expref.getRootDir()+"/";
	String            mfcfile = expref.optionValue( "mfc" );

	if( !mfcfile.empty() ) {
		if( !COFVisibilityBuffer::readMFCSetup( myMFCSetup, exproot+mfcfile ) )
			THROW_JEXCEPT("Failed to read MFCfile '" << mfcfile << "' [mfc= option was set]");
        cerr << "Read MFC setup: " << endl << myMFCSetup << endl;
	}

    // get a reference to the (potentially) aliased sources
    // and read the scans from the  VEX-file
    bool                                 mfc;
    const alias_type&                    aliases( myMFCSetup.srcToAliasMap );
    j2ms::scaninfolist_t                 silist;
    j2ms::scaninfolist_t::const_iterator cursi;

    mfc    = (aliases.size()>0);
    silist = scanlist( vexfilename /*, mySetup*/ );
    if( silist.empty() )
        THROW_JEXCEPT( "COFVisibilityBuffer - No scans defined in output "
                       "vexfile '" << vexfilename << "' (silist.empty()==true)" );

    // loop over all identified scans
    // and check if they were 'aliased'
    // (or: processed with MFC processing)
    //
    // HV: 22/02/2007 - Changed this a bit. *if* there were
    //                  aliases sources, this implies that the whole
    //                  thing was correlated with a MFC setup. This
    //                  setup does not change on a per-scan basis.
    //                  Really this means that if we encounter a 
    //                  scan with a source that is NOT in the alias list
    //                  we have no *idea* how it was processed and
    //                  therefore we only accept [if there _are_ aliases]
    //                  scans for sources that are present in the alias-list
    //                  Other scans are NOT recognized in this case
    for( cursi=silist.begin();
         cursi!=silist.end();
         cursi++ ) {
    
        if( mfc ) {
			// For each aliased source, add a scandefinition
            // to the scanlist (with a unique ID)
   			int                                     seq;
            alias_type::const_iterator              curalias;
            alias_type::mapped_type::const_iterator realsrc;

            curalias = aliases.find(cursi->sourceCode);
            if( curalias==aliases.end() ) {
                cerr << "mfc-processed Job/Failed to find alias for source "
                     << cursi->sourceCode << " -> Skipping scan " 
                     << cursi->scanId << " (#" << cursi->scanNo << ")" << endl;
                continue;
            }

            // *now* process all the aliases and stick *those*
            // scandefs into the 'real' scanlist - suffixing the
            // scanIds with a sequence#
            for( realsrc=curalias->second.begin(), seq=0;
                 realsrc!=curalias->second.end();
                 realsrc++, seq++ ) {
    			//  Add the scan(s) to our list...
                ScanInfo        nscan;
                ostringstream   strm;
			
                // copy over all fields of original scan info
                nscan = *cursi;

                // replace the sourcecode with the current aliased
                // sourcecode
				nscan.sourceCode = *realsrc;
			    
                // and replace the scanId by a unique number,
                // let's use the original scanid and a 
                // sequence nr
                strm << cursi->scanId << "/" << seq;
                nscan.scanId = strm.str();
                
                // add the 'new' ScanInfo to the real
                // list of scans 
                // and add a COFScan to our list of COFScans...
				myScanPtrs.push_back( new j2ms::COFScan(this, nscan) );
                myScanInfoList.push_back( nscan );
			}
            // done processing all aliases
		} else {
            // not MFC processed job, just add 
            // a COFScan to go with it
            myScanPtrs.push_back( new j2ms::COFScan(this, *cursi) );
            myScanInfoList.push_back( *cursi );
        }
	}

	// Sanity checks?
	if( myScanPtrs.size()==0 )
		THROW_JEXCEPT( "COFVisibilityBuffer - No scans were found in the VEXfile "
                       << vexfilename << " (scanPtrs-list empty).");

    // Read all the stationbased info
    station2listmap = stationscanmap( vexfilename );
    if( station2listmap.empty() ) {
        cerr << "\n**********\n"
             << "COFVisibilityBuffer: NO STATIONBASED SCANINFO AVAIL.\n"
             << "   If you're filtering by 'scan' this will yield\n"
             << "   no data *at all*\n"
             << "**********\n";
    }

    //  Let the system find the first
    //  datadescriptor/correlated-data-record combination!
    myMappingsFileptr->seek( 0, ios::beg );
    myCorrelatedDataFileptr->seek( 0, ios::beg );
  
    // Let the initial iterator point at the end of the
    // (at this time) empty set to prevent it from
    // being increased
    myBaselineIterator = myBaselineSet.end();

    // the 'next()' method will attempt to find
    // the first DataDescriptor/CorrelatedDataRecord
    // if nothing loaded yet
    this->next();
}

Bool COFVisibilityBuffer::getCurrentVisibility( Visibility& vref ) {
    // This is the 'metadata' for a visibility that has no associated scan
    // (see below). Set all unsigned-int labels to "invalid", which we take to
    // be "-1".
    const unsigned int           invalid_uint( (unsigned int)-1 );
    static const unmapped_type   noscan_idx( 0, 0, invalid_uint, invalid_uint );
    //  Static stuff - prevents from being created over and over again
    static ExtVisLabel     lab;
    static Vector<Float>   blweight;
    static Matrix<Complex> bldata;

    // Will be set to true if the data needs to be
    // swapped due to the fact that the stations, when
    // unmapped to vexfile order, are in the "wrong" order
    // ("wrong" := (antenna1>antenna2)). The interferometers
    // in the data have labels XANT,YANT which are ALSO
    // reordered such that XANT<=YANT, which may
    // induce swapping too... However, XANT and YANT may
    // represent Antenna1/Antenna2 in the VEX file with
    // Antenna1 > Antenna2 :) Neat eh?
    bool                             swap_due_vex( false );
    Bool                             flag( false );
    BaseLineIdx                      blidx;
    unmap_type::const_iterator       mapptr;
    
	// Clear the retval; just to make sure that even if we return false
	// and the caller does not respond to that and still wants to use
	// the 'vref' they get squat!
	vref = Visibility();

    //  If no iterator, we must be out of range...
    if( myBaselineIterator==myBaselineSet.end() ||
        myCurrentCorrelatedDataRecordptr==0 ||
		myCurrentDataDescriptorptr==0 ||
        myCurrentMappingptr==0 )
            return False;


    // Look up the current COFBaseline in the unmapped thingy
    // to get at the unmapped indices [ie: the physical values
    // rather then the logical ones found in the data, where the
    // COFBaseline is formed from]
    // We should only treat this as an error if there is a
    // shortlist of scans. Namely, if there is no shortlist of scans,
    // this means that the current visibility could not be pinned to
    // a particular scan and as such, it is, by definition, impossible
    // to find any physical quantities it represents.
    // We pass the visibility on upwards albeit with a null-'current scan pointer'
    // so the caller *knows* that no actual metadata can be gotten.
    // We leave it up to the higher-level code to decide what to do with
    // this visibility
    mapptr = unmap.find( myBaselineIterator->getKey() );
    if( mapptr==unmap.end() && !intgcache.shortlist.empty() ) {
        cerr << "COFVisibilityBuffer/WTF? No unmapped info for baseline "
             << *myBaselineIterator << endl;
        return False;
    }
    // woohoo. We have all the indices (or not ;))!
    const unmapped_type&   indices( ((mapptr==unmap.end())?(noscan_idx):(mapptr->second)) );
    
    blidx = BaseLineIdx(indices.XAnt, indices.YAnt);
    if( blidx.idx1()>blidx.idx2() ) {
        swap_due_vex = true;
        blidx = blidx.swap();
    }

    // These are easy
    lab.baseline  = blidx;
    lab.subbandnr = indices.Subband;

    // Now set the scanptr. We find the mapptr->second.Source'th scan
    // in the shortlist, if such a scan exists. If not - bugger all but who
    // cares!
    // Note: we cannot use "std::advance()" because that fn does not check
    // wether it adresses the iterator beyond the container's ".end()"
    unsigned int                     n;
    const scanptrlist_type&          ptrlist( intgcache.shortlist );
    scanptrlist_type::const_iterator sptr;

    for(n=0, sptr=ptrlist.begin();
        n<indices.Source && sptr!=ptrlist.end();
        n++, sptr++);

    // save scanpointer as current scan pointer
    // [if any, that is]
    if( sptr!=ptrlist.end() )
		this->setCurrentScanptr( *sptr );
    else
		this->setCurrentScanptr( 0 );
	
    //  Great! 
    //  now read the data
	bldata.resize( 0, 0 );
	blweight.resize( 0 );   
  
    // the "getBaselineData()" function retrieves the requested
    // data for the current baseline. Takes care of swapping
    // the data (in case vex order dictates this) such that
    // we know that the data at row #n in the matrix
    // indeed holds data for requested product #n.
	bldata = this->getBaselineData(blweight, *myBaselineIterator,
                                   swap_due_vex, mySetup.getCorrelations());

    if( !bldata.shape().product() ) {
		cerr << "COFVisibilityBuffer - No data read for baseline " << endl
			 << *myBaselineIterator << endl;
		return False;
    }
  
    // Fill in the products 
    lab.correlations.clear();
    std::copy( mySetup.getCorrelations().begin(), mySetup.getCorrelations().end(),
               back_insert_iterator<correlations_t>(lab.correlations) );

    vref = Visibility(lab, bldata, Visibility::time, flag||intgcache.inttimflag,
                      blweight, intgcache.visibilitytime, j2ms::noMuvw, this);
    return True;
}


// Build a helper mapping of SIGINP -> physicalchannel,telescope pair [,DataSource]
// such we can quickly unmap XSIGINP/YSIGINP "pointers"
// Note: as we do not use the "datasource" we do not incorporate that into here.
// In case we need it, it's simple enough to add it
struct siginp_type {
    unsigned int  Channel;
    unsigned int  Telescope;

    siginp_type(unsigned int ch, unsigned int tel):
                Channel(ch), Telescope(tel)
    {}
    private:
        // no default stuff. We like proper initialization
        siginp_type();
};
// Maps "SIGINP" value to (channel,telescope) pair
// where "channel" is to be read as "physical SU channel"
// [will be unmapped to *logical* SU channel later on...]
typedef std::map<unsigned int, siginp_type> inpmap_type;

Bool COFVisibilityBuffer::next( void ) {
    // This static bool indicates "attempt to find the vex-to-subband
    // -mapping at your earliest convenience".
    // Whenever we find a DataDescriptor, we set this flag (first/new DataDescriptor
    // means that the content of the CorrelatedDataFile may have changed, hence
    // affecting the mapping).
    // The fact that we not necessarily can unmap immediately is due to the fact
    // that we *first* need to find data for which we know which scan it came from:
    // the vex-to-subband mapping can ONLY be gotten through the meta-information
    // found in the scan. 
    // Therefore we must raise this flag and keep it set until we have found
    // all the information necessary to actually build the requested mapping.
    // Initially it is false because we don't have a DataDescriptor yet.
    static bool      needToUnmap = false;
    // If the baselineiterator is not at the end of the current
    // integration, move on to the next baseline.
    // [this may mean it *will* be at the end so be aware
    //  of that..]
    if( myBaselineIterator!=myBaselineSet.end() )
        myBaselineIterator++;

    // If we (still) aren't at end-of-set, get the
    // f**k out as we don't have to do anything more 'n that!
    if( myBaselineIterator!=myBaselineSet.end() )
        return True;
   
    // Ok, we must attempt to read a new integration!
    bool                                  retval( false );
    JCCS::CountedPointer<DataFileRecord>  tmprecordptr;

    // Remove previous CorrelatedDataRecord
    myCurrentCorrelatedDataRecordptr = 0;

    //  Read record until we find a correlated data record!
    retval = false;
    while( !retval && !myCorrelatedDataFileptr->eof() ) {
        (*myCorrelatedDataFileptr) >> tmprecordptr;	

        if( !tmprecordptr )
            continue;
	
        switch( tmprecordptr->recordType() ) {
            case DataFileRecord::correlateddata:
        	    //  If the baselineset is empty (i.e. no begin) it
        	    //  has not been set yet, i.e. no data descriptor
        	    //  record has been found. Skip this integration!
        	    if( myBaselineSet.empty() ) {
                    cerr << "Skipping CorrelatedDatarecord for there's no "
                         << "DataDescriptor (yet)" << endl;
                    tmprecordptr = 0;
                } else {
                    //  Keep pointer to record (and upcast to CorrelatedDataRecord)
                    myCurrentCorrelatedDataRecordptr =
                        (JCCS::CountedPointer<CorrelatedDataRecord>)tmprecordptr;
		    
                    //  reset iterator
                    myBaselineIterator = myBaselineSet.begin();
		
                    // set return value so loop terminates
                    retval = true;
                }
                break;
		    
            case DataFileRecord::datadescriptor:
                // we don't have to explicitly set the 
                // pointer to 0 (or delete it) since
                // the ref-counted-ptr-stuff
                // (which is behind the 
                //  "myCurrentDataDescriptorptr")
                //  does take care of that for us
                myCurrentDataDescriptorptr =
                    (JCCS::CountedPointer<DataDescriptor>)tmprecordptr;

                //  We attach to the new data descriptor
                myBaselineSet = (*myCurrentDataDescriptorptr);

                // signal we must re-do the unmapping of
                // all baselines (ie [XAnt,YAnt,Subband] combis)
                // at the earliest convenient time after finding this
                // record ...
                needToUnmap = true;

                //  We found a new data-descriptor. However, we
                //  have no data (yet) so the iterator should be
                //  set to an empty value. Only if we have data we
                //  can safely iterate over the baselines!
                //  (the iterator will be set to 'begin()'
                //   if we find data)
                myBaselineIterator = myBaselineSet.end();
		    
                //  Last thing to do, is to read the accompanying
                //  MappingEntry!
                //  Remove the mapping if it's different
                //  from the one we need
                //  myCurrentMappingptr==0 triggers reading
                //  the mapping (so it also triggers reading
                //  if there was no mapping *yet*)
                if( myCurrentMappingptr &&
                    (myCurrentMappingptr->getOffsetInDataFile()!=
			         myCurrentDataDescriptorptr->getMappingEntryRef()) )
                        myCurrentMappingptr = 0;

                if( !myCurrentMappingptr ) {
                    myMappingsFileptr->seek(
                                myCurrentDataDescriptorptr->getMappingEntryRef(),
                				ios::beg );
                    // can safely re-use "tmprecordptr"
                    (*myMappingsFileptr) >> tmprecordptr;

                    if( !tmprecordptr ||
                        tmprecordptr->recordType()!=DataFileRecord::mappingentry ) {
                        // this is really serious!
                        THROW_JEXCEPT("COFVisibilityBuffer::next()/"
                                    << "DataFile corrupted."
                                    << " DataDescriptor::mappingEntry does NOT point at "
                                    << "an entry in the MAP file?!");
                    }
                    myCurrentMappingptr =
                            (JCCS::CountedPointer<MappingEntry>)tmprecordptr;
                }
                break;
		    
            default:
                tmprecordptr = 0;
                break;
        }
    }

    // If we read a new integration, update the values in the
    // per-integration cache.
    if( myCurrentCorrelatedDataRecordptr ) {
        scanptrlist_type::iterator    sptr;
        const DataDescriptor&         datadescr( *myCurrentDataDescriptorptr );
        const CorrelatedDataRecord&   corrdata( *myCurrentCorrelatedDataRecordptr );
       
        intgcache.inttimflag = false;
        
        //  First things first: let's find out in which scan we are!
        //  so: calculate the visibility time
        //
        //  HV: 14/05/2007 - If we know the BOCF-Rate we may correct for the
        //                   2 BOCF difference between data and model
        //                   (apparently, the model/bocfcount comes *two* bocfs
        //                    before the data)
        //      31/07/2008 - AB reworked the RT code and now the header-data delta
        //                   time is only 1 BOCF. In order to recognize this, the
        //                   AB version-number has been bumped to 3.0.
        //                   In short:
        //                     AB version >= 3.0 => 1BOCF delta, 2 BOCF otherwise
        //                                             
		intgcache.startROT = corrdata.getStartSFRot();
        intgcache.endROT   = corrdata.getEndSFRot();

        if( mySetup.getBocfRate()>0 ) {
            SFROT    deltaT = ((float)mySetup.headerDataBocfDiff()/(float)mySetup.getBocfRate()) * SYSCLK_FREQ;
            intgcache.startROT -= deltaT;
            intgcache.endROT   -= deltaT;
        }

		if( ::fabs(intgcache.endROT.get_sysclks()-intgcache.startROT.get_sysclks()) > 
            (1.2*mySetup.getIntegrationTime()*SYSCLK_FREQ) )
			    intgcache.inttimflag = true;

        // Convert from ROT to MEpoch
        intgcache.visibilitytime = 
    		COFVisibilityBuffer::calcMidPointAsMEpoch( datadescr.getStartRot(),
                                intgcache.startROT, intgcache.endROT );

        intgcache.visstart = rot2mepoch(intgcache.startROT,
                                        datadescr.getStartRot().get_base_date());

        intgcache.visend   = rot2mepoch(intgcache.endROT,
                                        datadescr.getStartRot().get_base_date());

        intgcache.mvepoch = intgcache.visibilitytime.getValue();
        intgcache.mvstart = intgcache.visstart.getValue();
        intgcache.mvend   = intgcache.visend.getValue();

        // clear the shortlist of possible scans
        intgcache.shortlist.clear();
        // No vex-to-subband mapping yet
        intgcache.v2sptr = fq2vex.end();

        // Look through the list of scans and find all that are applicable
        const MVEpoch&              mvs( intgcache.mvstart );
        const MVEpoch&              mve( intgcache.mvend );
        scanptrlist_type::iterator  scnptrptr;
        
        for( scnptrptr=myScanPtrs.begin();
             scnptrptr!=myScanPtrs.end();
             scnptrptr++ ) {
            j2ms::COFScan*   cscan( *scnptrptr );
          
            // If start-of-visibility beyond current scan or end-of-visibility
            // before current scan ... the visibility certainly is NOT within
            // the current scan :)
            if( mve.get()<cscan->getStartTime().getValue().get() ||
                mvs.get()>cscan->getEndTime().getValue().get() )
                continue;

            // cache the name of the frequency setup for the scan
            string                   mode( cscan->getFrequencyCode() );

            intgcache.shortlist.push_back( *scnptrptr );

            // If we already have a "vex2subband" mapping pointer,
            // verify it maps to the same thingies as for *this* scan, as we
            // urrently cannot not deal with >1 scan in the shortlist
            // with differing frequency-setups...
            if( intgcache.v2sptr!=fq2vex.end() && intgcache.v2sptr->first!=mode )
                THROW_JEXCEPT("Unsupported: >1 scan in the shortlist with "
                              "differing mode (frequency setup)");

            // Now we must find the appropriate vex2sb mapping pointer
            intgcache.v2sptr = fq2vex.find( mode );

            // See if we already did process this frequency setup
            if( intgcache.v2sptr!=fq2vex.end() )
                continue;

            // Bummer! First time we encounter this FrequencyConfig/mode.
            // Now we must build the vex-to-subband mapping for this one.
            vex2sb_type        value;
            FrequencyConfig    fc;
            const FrequencyDB& fdb( this->getExperiment().getFrequencyDB() );

            if( !fdb.searchFrequencyConfig(fc, mode) )
                THROW_JEXCEPT("Failed to find mode " << mode);

            // Analyze the subbands
            const basebandlist_t&          bbl( fc.getBaseBands() );
            basebandlist_t::const_iterator curbb;

            for( curbb=bbl.begin(); curbb!=bbl.end(); curbb++ ) {
                // Get the vexchannels
                unsigned int                         bbnum( curbb->getBaseBandNr() );
                const BaseBand::vex2polmap&          v2pmap( curbb->getVexChannels() );
                BaseBand::vex2polmap::const_iterator curv2p;

                // insert entries of the current vexchannel to map to the current subband
                for( curv2p=v2pmap.begin(); curv2p!=v2pmap.end(); curv2p++ )
                    value.insert( make_pair(curv2p->first, bbnum) );
            }

            // Okiedokie. Now insert the built-up vex2subband mapping into our cache
            pair<fq2vexmap_type::iterator, bool>  insres;

            insres = fq2vex.insert( make_pair(mode, value) );
            if( !insres.second )
                THROW_JEXCEPT("Failed to insert entry in mode to vex-to-subband-map map");
            // insres.first is now the iterator, pointing at the (newly inserted)
            // fq2vex-mapping-entry
            intgcache.v2sptr = insres.first;
        }
    }
    // We can only do a proper unmap if 1) we have a mappingentry (signalled by 
    // 'needToUnmap==true) and
    // 2) we have a vex-to-subband mapping [which should have been built
    // in the previous if() construct, if a scan could be found that
    // applies to the current CorrelatedDataRecord.]
    // Note: we do not have to check if the MappingEntryptr exists - the
    // reading code checs that a mappingrecord could be read back ok.
    // If this fails, an exception is thrown.
    if( needToUnmap && intgcache.v2sptr!=fq2vex.end() ) {
        inpmap_type           inpmap;
        // cache references to important maps
        // "vex-to-subband" mapping
        const vex2sb_type&    v2s( intgcache.v2sptr->second );
        // physical SU-channel to logical SU-channel [CRM shuffle!!!!]
        const vector<int>&    p2l( myMFCSetup.physToLogMap );
        // physical SU-channel to aliased-source-index [multiple field centers, MFC, mfc]
        const vector<int>&    p2s( myMFCSetup.physToFieldCenterMap );
        COFBaselineIterator   curbl;
        const MappingEntry&   maprecord( *myCurrentMappingptr );
        
        // Analyze the mappingentry and build the SIGINP -> (channel,telescope) mapping
        for(unsigned int i=0; i<maprecord.nrMappings(); i++) {
            unsigned int      curtel;
            const Mapping&    mapping( maprecord[i] );

            curtel = mapping.itsTelescopeNr;
            // Loop over all (=16) physical channels
            for(unsigned int ch=0; ch<16; ch++) {
                unsigned int                      pi = mapping.itsPhysicalInput[ch];
                pair<inpmap_type::iterator, bool> insres;
                // skip unused channels
                if( pi==(unsigned int)-1 )
                    continue;
                // Ok. (try to) insert a new entry!
                insres = inpmap.insert( make_pair(pi, siginp_type(ch, curtel)) );
                if( !insres.second )
                    THROW_JEXCEPT("Failed to insert entry into inputmap for "
                                  << "telescope " << curtel << ", ch=" << ch
                                  << ", PhysInp=" << pi);
            }
        }

        // Clear existing blkey -> unmapped values mapping
        unmap.clear();

        // Loop over baselines found in the baselineset
        // and verify that for all interferometers found there,
        // the values unmap to the same value
        for(curbl=myBaselineSet.begin(); curbl!=myBaselineSet.end(); curbl++) {
            blkey_type                     curkey( curbl->getKey() );
            const COFBaseline::intflist_t& iflist( curbl->getIntfList() );

            if( iflist.size()==0 )
                continue;

            // It is sufficient to init the indices once -> if
            // we find an interferometer which gives different
            // results, we're in trouble!
            // As argued before: both XSUBBAND/YSUBBAND and XSRC/YSRC
            // must all map to the same SUBBAND, SRC value so we do
            // not discriminate between x/y in these values.
            const unsigned int                      invalid((unsigned int)-1);
            unsigned int                            sb(invalid), src(invalid);
            unsigned int                            xant(invalid), yant(invalid);
            inpmap_type::const_iterator             xinpptr, yinpptr;
            COFBaseline::intflist_t::const_iterator curif;
            
            for(curif=iflist.begin(); curif!=iflist.end(); curif++) {
                unsigned int                xinp( curif->second.myInterferometer.getXInput() );
                unsigned int                yinp( curif->second.myInterferometer.getYInput() );
                unsigned int                lsb, lsrc, lxant, lyant;
                vex2sb_type::const_iterator xv2sb, yv2sb;
                
                // attempt unmap
                if( (xinpptr=inpmap.find(xinp))==inpmap.end() )
                    THROW_JEXCEPT("Failure to locate entry for (X)SIGINP=" << xinp << " in Mapping!");
                if( (yinpptr=inpmap.find(yinp))==inpmap.end() )
                    THROW_JEXCEPT("Failure to locate entry for (Y)SIGINP=" << yinp << " in Mapping!");

                const siginp_type&   xsig( xinpptr->second );
                const siginp_type&   ysig( yinpptr->second );

                // If they do not map onto the same channel, we're stuffed.
                if( xsig.Channel>=p2l.size() || ysig.Channel>=p2l.size() )
                    THROW_JEXCEPT("Channel out of range. Found X/YChannel=" << xsig.Channel
                                  << "/" << ysig.Channel << "; nr channels =" << p2l.size()
                                  << " X/YSIGINP=" << xinp << "/" << yinp);
                // Attempt to locate the vex-to-subband entries for the real logical
                // (ie, after unmapping physical->logical SU channel!) and make sure
                // that both X/Y refer to the same subband! They do not have to
                // map to the same logical SU channel as there may be two logical
                // channels mapping onto the same subband, namely, one for each
                // recorded polarization!
                if( (xv2sb=v2s.find(p2l[xsig.Channel]))==v2s.end() )
                    THROW_JEXCEPT("Failed to find entry in vex-to-subband map for vex-channel #"
                                  << p2l[xsig.Channel] << ", X-part of " << *curbl);
                if( (yv2sb=v2s.find(p2l[ysig.Channel]))==v2s.end() )
                    THROW_JEXCEPT("Failed to find entry in vex-to-subband map for vex-channel #"
                                  << p2l[ysig.Channel] << ", Y-part of " << *curbl);
                // Now assert both x/y refer to the same subband
                if( xv2sb->second!=yv2sb->second )
                    THROW_JEXCEPT("Inconsistent mapping: X/Y Channel map to different subbands! - "
                                  << "XCh:" << xv2sb->first << " => SB#" << xv2sb->second << ", YCh: "
                                  << yv2sb->first << " => SB#" << yv2sb->second << ", X/YSIGINP="
                                  << xinp << "/" << yinp);
                // repeat for source...
                // If they do not map onto the same source, we're stuffed.
                if( xsig.Channel>=p2s.size() || ysig.Channel>=p2s.size() )
                    THROW_JEXCEPT("Channel out of range. Found X/YChannel=" << xsig.Channel
                                  << "/" << ysig.Channel << "; nr sources =" << p2s.size()
                                  << " X/YSIGINP=" << xinp << "/" << yinp);
                if( p2s[xsig.Channel]!=p2s[ysig.Channel] )
                    THROW_JEXCEPT("Inconsistent mapping: X/Y Channel map to different logical sources - "
                                  << "X:" << xsig.Channel << " => " << p2s[xsig.Channel] << ", Y: "
                                  << ysig.Channel << " => " << p2s[ysig.Channel] << ", X/YSIGINP="
                                  << xinp << "/" << yinp);
                // Ok. Got & checked everything. Transfer to local variables, for sort-of
                // readability...
                lsb   = xv2sb->second;
                lsrc  = p2s[xsig.Channel];
                lxant = xsig.Telescope;
                lyant = ysig.Telescope;

                // Init before we start consistency checking between
                // multiple interferometers
                if( curif==iflist.begin() ) {
                    sb   = lsb;
                    src  = lsrc;
                    xant = lxant;
                    yant = lyant;
                }

                // Now check wether the unmapped values of the current
                // interferometer match up with everything unmapped before
                if( lsb!=sb || lsrc!=src || lxant!=xant || lyant!=yant )
                    THROW_JEXCEPT("Inconsistent mapping between interferometers in baseline "
                                  << *curbl << " - \n"
                                  << "Should be " 
                                  << " X/YAnt, SB, SRC = " << xant << "/" << yant << ", "
                                  << sb << ", " << src
                                  << "; found " << lxant << "/" << lyant << ", "
                                  << lsb << ", " << lsrc);
            }
            // If we make it to here we have a consistent entry!
            pair<unmap_type::iterator, bool> ires;
            
            ires = unmap.insert( make_pair(curkey, unmapped_type(xant, yant, sb, src)) );
            if( !ires.second )
                THROW_JEXCEPT("Failed to insert entry in 'unmap' for "
                              << *curbl << " (" << xant << '/' << yant << ", SB#" << sb << ", SRC#" << src);
        }
        // If we arrive here, we may safely assume that we
        // did build an 'unmap'.
        needToUnmap = false;
    }
    return retval;
}

const String& COFVisibilityBuffer::getRootDir( void ) const {
    return myRootDir;
}

Int COFVisibilityBuffer::getVisBufId( void ) const {
	return (Int)(myJobID);
}

String COFVisibilityBuffer::getProcessorSubType( void ) const {
	return "JIVE";
}

CorrelatorSetup COFVisibilityBuffer::correlatorSetup( void ) const {
    return mySetup;
}

const j2ms::stationscanlist_t&
            COFVisibilityBuffer::getStationScanlist( int station ) const {
    j2ms::station2listmap_t::const_iterator  curlist;

    if( (curlist=station2listmap.find(station))!=station2listmap.end() )
        return curlist->second;

    // delegate to baseclass (if any)
    return this->VisibilityBuffer::getStationScanlist(station);
}

// *.MODEL files have a station-id string in their
// header but we must write ANTENNA_ID into the tables.
// This mapping translates between these two
typedef std::map<std::string,int> sta2idmap_t;



// For each SU channel in each FrequencyConfig
// we must know the mapping of channel to
// subband & polarization & actual source-id.
// This takes care of multiple field centers: 
struct sbpol_t {
    int                __sb;
    int                __src;
    j2ms::polarization __polarization;

    sbpol_t():
        __sb(-1),
        __src(-1),
        __polarization( j2ms::unknown )
    {}

    sbpol_t( int sb, int src, j2ms::polarization p ):
        __sb( sb ),
        __src( src ),
        __polarization( p )
    {}
};

// for each FrequencyConfig, we define a mapping of
// SU channel -> (subband,polarization) datum
// The idea is to only insert used entries in here, so when
// dealing with a model datarecord, we can immediately write
// out only the used SU channels [those are the keys in this map].
//
// So when filling this in we have to look at the vexchannels 
// being used and unmap them to SU channel (not necessarily a 1:1
// mapping, especially under MFC conditions)
typedef std::map<int, sbpol_t> su2sb_t;

// And we record one of these (vex2sb_t) per
// frequency-config, identified by the frequency-config's
// name
typedef std::map<string, su2sb_t>  fccache_t;

// Provide the model. Do proper unmapping into exeperiment indices
const j2ms::modeldata_t& COFVisibilityBuffer::getModelData( void ) const {
    if( modeldata.size() )
        return modeldata;

    // We need the list-of-scans, otherwise we cannot
    // transform an instance of time (a model-polynomial
    // is only tagged by time in the modelfile)
    // into observational parameters like frequency, source
    // Cache pointer to last-scan to speed up lookup. Entries in
    // modelfile are always in increasing time order so that
    // should speed up access enormously
    fccache_t                        fccache;
    scanptrlist_type::const_iterator lastscan;

    if( myScanPtrs.empty() ) 
        THROW_JEXCEPT("No scans defined for this VisibilityBuffer");

    // 'modeldata' was not yet filled in, start reading the modelstuff
	DIR*                       dirptr;
	const Regex                rx_modelfile( ".+\\.MODEL" );
    sta2idmap_t                sta2idmap;
    ostringstream              strm;
	struct dirent*             entryptr;
    vector<string>             files2do;
    const Experiment&          eref( this->getExperiment() ); 
    const FrequencyDB&         freqdb( eref.getFrequencyDB() );
   
    // build up mappings we need to do proper unmapping:
    // * from 2/3 letter 'station ID' -> antenna number
    // * from vexchannel -> subband
    {
        uInt                  i;
        AntennaInfo           ai;
        const AntennaDB&      antdb( eref.getAntennaDB() );
        const Vector<String>  ants( eref.getAntennas() );

        for(i=0; i<ants.nelements(); i++) {
            if( !antdb.searchAntenna(ai, ants(i)) )
                break;
            // add it to the mapping
            sta2idmap.insert( make_pair(ai.getTLC(), ai.getId()) ); 
        }
        if( i!=ants.nelements() )
            THROW_JEXCEPT("Failed to locate info for Antenna '" << ants(i) << "'");
    }

    // form the directoryname where the .MODEL files should be located
    // they should be 1 level up from the directory where the visbuf-data
    // lives.
    // just create a temporary scope for stringhandling so we 
    // cannot accidentally access the temporary char* variables.
    // After the scope has been left the name of the directory
    // where the .MODEL files are is left in the 'strm' stringstream
    // variable
    {
        // must make copy since ::dirname(3) may make modifications
        // to the string given as argumen
        char*   cpy = ::strdup( this->getRootDir().c_str() );
        char*   mdldir;

        if( !cpy )
            abort();
        // according to man (3) dirname, dirname never returns 0/NULL
        // so's we just don't check.
        mdldir = ::dirname( cpy );
        
        // make sure the stringstream is empty
        strm.str( string() );
        strm << mdldir;

        // ...
        ::free( cpy );
    }
    // and attempt to open
    if( (dirptr=::opendir(strm.str().c_str()))==0 )
        THROW_JEXCEPT("Failed to open DIR '" << strm.str() << "'");

    // read the entries and filter all that look like a modelfile [name-wise, that is]
    while( (entryptr=::readdir(dirptr))!=0 ) {
        if( String(entryptr->d_name).matches(rx_modelfile) ) 
            files2do.push_back( strm.str()+'/'+string(entryptr->d_name) );
    }
    ::closedir( dirptr );

    // this is merely a warning. Does not warrant an exception I'd say
    if( files2do.empty() ) {
        cerr << "getModelData(): No MODEL files found in " << strm.str() << endl;
        return this->VisibilityBuffer::getModelData();
    }

    // Now process all the MODEL files
    for( vector<string>::iterator curfile = files2do.begin();
         curfile!=files2do.end();
         curfile++ ) {

        // initialize the cached iterator to invalid but known value
        // such that we can detect 'we need to find it first'
        lastscan = myScanPtrs.end();
        
        // ....
        try {
            int                                   year;
            Int                                   staid( -1 );
            MEpoch                                polytime;
            ModelFile                             mf( curfile->c_str() );
            fccache_t::iterator                   curfc( fccache.end() );
            sta2idmap_t::iterator                 staptr;
            JCCS::CountedPointer<DataFileRecord>  dfr;
         
//            cout << "getModelData()/Processing " << *curfile << endl;
            
            // let's see we if know the station at all
            if( (staptr=sta2idmap.find(String(mf.getStationId())))==sta2idmap.end() ) {
                cout << "getModelData()/Nothing known about station '"
                     << mf.getStationId() << "'! Skipping this file!"
                     << endl;
                continue;
            }
            // the timestamp in the modelfile propably is the
            // timestamp the file was written. we really
            // want the year-of-observation. Get the first
            // scan from the visbuf, extract the scanstart
            // and get the year from there.
            // The time in the modeldatarecords is a short-time-value,
            // ie a time-within-a-year without the year-info
            // attached... *sigh*
            // 
            // Note: we have already ascertained that the scanlist
            // is non-empty so the following statement is reasonably
            // safe...
            year = MVTime( (*myScanPtrs.begin())->getStartTime().getValue() ).year();

            // and cache the antenna-id so we do not have the 
            // penalty of de-referencing staptr->second everytime
            // we need the station's ID
            staid = staptr->second;

            // suck the datafile empty!
            while( !mf.eof() ) {
                // attempt to read a record
                mf >> dfr;
                
                // if no record read, read on until we hit End-Of-File
                // or if we didn't find a ModelDataRecord, we continue
                // as well
                if( !dfr || (dfr && dfr->recordType()!=DataFileRecord::model) )
                    continue;
                
                // we may rest assured that dfr points at a ModelDataRecord
                CountedPointer<ModelDataRecord>  mdr( (CountedPointer<ModelDataRecord>)dfr );

                // compute the full timestamp of the polynomial record
                polytime = rot2mepoch( mdr->startROT(), year );
               
                // The j2ms::modeldatarecord contains polynomials for the 
                // vexchannels used in a given scan. Need to work our way
                // back from SU-logical-channel [that's the order the poly's
                // are in the data-record AND there's always 16 polys in the
                // record (SU has 16 output channels) but not always all are 
                // in use] to vexchannel [we have a mapping for that] and
                // from vexchannel we can work out what the subband/polarization
                // is.
                
                // figure out which scan this modelentry is for. We need to
                // find out which frequency-setup was used (for the number
                // of IFs (BANDs)) and other necessary info
                if( lastscan!=myScanPtrs.end() ) {
                    // check if new modelentry still within scanrange
                    // otherwise trigger lookup of new scan
                    mepoch_in_scaninfo   condition_still_holds( polytime );

                    if( !condition_still_holds(*lastscan) )
                        lastscan = myScanPtrs.end();
                }
                // look for new scan based on time
                if( lastscan==myScanPtrs.end() ) {
                    casa::String   freqcode;

                    // invalidate this iterator
                    curfc = fccache.end();

                    if((lastscan=find_scaninfo_by_time(myScanPtrs, polytime))==myScanPtrs.end()) {
                        cerr << "getModelData()/No scan for model @ "
                             << MVTime::setFormat(MVTime::DMY) << MVTime(polytime.getValue()) << endl
                             << "     Skipping record" << endl;
                        continue;
                    }
                    freqcode = (*lastscan)->getFrequencyCode();

                    // Find the frequency-config for this scan.
                    // create-and-cache it if we haven't got it yet
                    // If failure - bunk out? Exception will be thrown
                    // if we cannot find all the info we need
                    if( (curfc=fccache.find(freqcode))==fccache.end() ) {
                        // not found, get info from the DB & cache it
                        FrequencyConfig   fc;
                        
                        if( freqdb.searchFrequencyConfig(fc, freqcode)==False )
                            THROW_JEXCEPT("Failed to find frequency config for " << freqcode);

                        // Now look at the frequency-config & stuff stuff in maps
                        uInt                          bbidx;
                        su2sb_t                       vexmap;
                        pair<su2sb_t::iterator, bool> insres;

                        // loop over all basebands (aka IFs, aka subbands, aka channels)
                        // Build up a mapping of vex-channel -> (subband/polarization)
                        // [NOTE: we _MISUSE_ the su2sb_t type, as it gives us the
                        // mapping we need and it's transient, stick in -1 for sourceid]
                        for( bbidx=0; bbidx<fc.getNrBaseBands(); bbidx++ ) {
                            const BaseBand*  bbptr = fc.getBaseBand(bbidx);

                            if( bbptr==0 ) 
                                THROW_JEXCEPT("Failed to find  BaseBand#" << bbidx << " in "
                                              << " mode=" << freqcode);

                            // and find out what's inside them!
                            // ie: gather the definitions for the vexchannels
                            // that are actually used and find out what the
                            // accompanying SU channel is
                            //
                            // v2pmap is a mapping of vexchannel(->first) to
                            // polarization (->second)
                            const BaseBand::vex2polmap&          v2p = bbptr->getVexChannels();
                            BaseBand::vex2polmap::const_iterator curv2p;

                            for( curv2p=v2p.begin(); curv2p!=v2p.end(); curv2p++ ) {
                                sbpol_t   props((int)bbidx, -1, curv2p->second);

                                insres = vexmap.insert( make_pair(curv2p->first, props) );
                                if( !insres.second )
                                    THROW_JEXCEPT("Failed to insert entry in vex->sbpol_t mapping");
                            }
                        }

                        // Ok, now we have a unique vex -> (subband/pol) mapping.
                        // Move on to the next step:
                        // Loop over all SU channels (0 -> 15), unmap to
                        // vex-channel & source, and combine that info
                        // into a final entry in the su2sb_t mapping that we
                        // need for this frequency-setup


                        // myMFCSetup.physToLogMap maps SU channels to VEX channels
                        // so we reverse the lookup here
                        int                     chan;
                        su2sb_t                 sumap;
                        SourceInfo              si;
                        const vector<int>&      ptlmap( myMFCSetup.physToLogMap );
                        const vector<int>&      ptfcmap( myMFCSetup.physToFieldCenterMap );
                        su2sb_t::const_iterator vptr;

                        for( chan=0;
                             chan<16 && (size_t)chan<ptlmap.size() && (size_t)chan<ptfcmap.size();
                             chan++ ) {
                            // unmap SU channel -> vexchannel
                            int           vexchannel = ptlmap[ chan ];
                            int           fieldcenter = ptfcmap[ chan ];
                            casa::String  srccode( (*lastscan)->getSourceCode() );
                            
                            // if vexchannel not in use, move on
                            if(  (vptr=vexmap.find(vexchannel))==vexmap.end() )
                                continue;

                            // multiple field centers not fully supported right now...
                            if( fieldcenter!=0 )
                                THROW_JEXCEPT("Multiple field centers not supported yet");
                            if( !eref.getSourceDB().searchSource(si, srccode) )
                                THROW_JEXCEPT("Failed to find sourceinfo for '"
                                              << srccode << "'");
                            // Now we have everything to build up a mapping from
                            // SU channel to (subband,source,polarization)
                            sbpol_t                        suval;
                            pair<su2sb_t::iterator, bool>  suinsres;

                            suval = sbpol_t( vptr->second.__sb,
                                             si.getId(),
                                             vptr->second.__polarization);
                            suinsres = sumap.insert( make_pair(chan, suval) );
                            if( !suinsres.second )
                                THROW_JEXCEPT("Failed to insert entry in channel -> (sb/pol/src) map");
                        }

                        // Only arrive here if all the info we require is found
                        // otherwise 'ceptions will be thrown
                        // Now insert an entry for the current frequency config
                        pair<fccache_t::iterator, bool>  fcinsres;

                        fcinsres = fccache.insert( make_pair(freqcode, sumap) );
                        if( !fcinsres.second )
                            THROW_JEXCEPT("Failed to insert entry into frequency-cache map");

                        // let the current frequency-config-cache-entry-pointer
                        // point at the newly inserted item
                        curfc = fcinsres.first;
                    }
                    // Ok, scan & frequency config checked out ok
                }

                // no need to check if 'curfc' and/or 'lastscan' are 
                // invalid because either:
                //  1) we found it in the map (ie it's valid)
                //  2) we tried to insert a new entry in the map and if
                //     that failed we wouldn't end up here [exception thrown]

                // this is what we're going to fill in
                j2ms::modeldatarecord            j2mdr;

                // stuff which is channel independent
                j2mdr.stationID         = staid;
                j2mdr.startMJDInSeconds = polytime.get("s").getValue();
                j2mdr.durationInSeconds = mdr->interval();
                j2mdr.wetAtmosphere     = j2ms::taylor_1(mdr->wet_atmosphere().data);
                j2mdr.dryAtmosphere     = j2ms::taylor_1(mdr->dry_atmosphere().data);
                j2mdr.clockOffset       = j2ms::taylor_1(mdr->clock_offset().data);

                for( su2sb_t::const_iterator curchan = curfc->second.begin();
                     curchan!=curfc->second.end();
                     curchan++ ) {
                    const sbpol_t&             sbpol( curchan->second );
                    j2ms::src2sbpolymap_type&  src2sb( j2mdr.src2sbpolymap );
                    j2ms::polypair&            pp((((src2sb[sbpol.__src])[sbpol.__sb])[sbpol.__polarization]));

                    // Insert the delay poly at the right place in the mapping
                    pp.delay = mdr->total_delay()[curchan->first].data;
                    // and the phase
                    pp.phase = mdr->total_phase()[curchan->first].data;
                }
                // Fully constructed a complete j2ms::modeldatarecord,
                // append it to the modeldata
                modeldata.push_back( j2mdr );
            }
            // totally read modelfile
        }
        catch( const std::exception& e ) {
            cerr << "getModelData: " << e.what() << endl;
        }
        catch( ... ) {
            cerr << "getModelData: Caught unknown exception?" << endl;
        }
    }
    return modeldata;
}

const j2ms::scaninfolist_t& COFVisibilityBuffer::getScanlist( void ) const {
    return myScanInfoList;
}

unsigned int COFVisibilityBuffer::getJobID( void ) const {
    return myJobID;
}

unsigned int COFVisibilityBuffer::getSubJobID( void ) const {
    return mySubJobID;
}

// tell upstream what we did to the data!
processing_steps COFVisibilityBuffer::getProcessingSteps( void ) const {
    processing_steps    rv;

    // If we knew the bocf-rate, we corrected for the 2-BOCF timeshift
    // between bocf-count/model and the data.
    // HV 31/07/2008 - Updated: take into account that the delta-bocf is
    //                 now AB software-version dependant. The details are
    //                 covered in 'CorrelatorSetup.{h,cc}'
    if( mySetup.getBocfRate()>0 ) {
        ostringstream   dt;
        dt << "delta-t = -" << (float)mySetup.headerDataBocfDiff()/(float)mySetup.getBocfRate()
			<< "s";
        rv.push_back( VBufProcessingStep("2BOCF-CORRECTION", dt.str()) );
    }
    return rv;
}

//  Delete allocated resources
COFVisibilityBuffer::~COFVisibilityBuffer() {
    vector<j2ms::COFScan*>::iterator  sptrptr;
    
    delete myMappingsFileptr;
    delete myCorrelatedDataFileptr;

    for( sptrptr=myScanPtrs.begin();
         sptrptr!=myScanPtrs.end();
         sptrptr++ )
            delete *sptrptr;
}



/******************************************************************************/
//
//             This is where the private section starts
//
/******************************************************************************/

// Determine in which row a certain product will end up.
// Build this from the cbref.getCorrelations() -> in THIS order
// the data must end up
typedef std::map<CorrelationCode,unsigned int> p2r_type;

// for a given set of correlations, remember the product-to-row mapping
// [such that we do not have to build it up over and over again]
typedef std::map<correlationset_t, p2r_type>   corr2p2r_type;

//  Get all the data for a given baseline.
//  The swap_due_to_vex is a hint from upstream that the data
//  should be swapped because the unmapped-to-vexfile-order
//  antennanumbers yield a "wrong" baselineindex (ie: antenna1>antenna2).
//  This method uses that info to short-circuit swapping such that no
//  unnecessary dataswaps take place (a double swap == a no-op nor is
//  both unswapped). "Both" refers to Baseline and interferometer; 
//  1 Baseline => 1+ Interferometers and each interferometer may or
//  may not be computed in the reverse direction.
//
//  ALSO: This method will reorder the data such that the order
//  in the matrix is compliant with the polarizationproducts
//  AFTER SWAPPING!!!!!
//  In short: it loops over the interferometers in the COFBaseline,
//  swaps the polarizationproduct [if swap_due_to_vex == true, that is]
//  and determines the destination row in the datamatrix based on
//  the (potentially) swapped polarization product.
//
//  The result being that the data delivered by this method has the
//  PHYSICAL properties in the same order to cbref.getCorrelations(),
//  AFTER (potentially) swapping 'm around. it is left up to the
//  caller to make sure the baselinelabel is made correct...
const Matrix<Complex>& COFVisibilityBuffer::getBaselineData( Vector<Float>& weight, 
							     const COFBaseline& cbref,
                                 bool swap_due_to_vex,
                                 const correlationset_t& cs ) const
{
    //  Prevent this one from being created over and over again...
    static corr2p2r_type                corr2p2r;
    static Matrix<Complex>   		    retval;
    
    //  These are the automatic vars
    Bool                     		 vernier( this->getVernierDelay() );
	SWVersion 		                 swver = mySetup.getSoftwareVersion();
	const SWVersion                  version1( 1, 0 );
    corr2p2r_type::iterator          mapptr;
    correlationset_t::const_iterator curreq;
    
    // Clear the return values
    retval.resize( 0, 0 );
    weight.resize( 0 );

    //  Check if we have anything to do at all
    if( !myCurrentCorrelatedDataRecordptr )
		return retval;

    // if nothing requested nor nothing available...
    if( cs.size()==0 || cbref.getCorrelations().size()==0 )
        return retval;

    // Find/create an entry in the "correlationset to product-to-row map"
    // such that we know where the products will have to end up
    if( (mapptr=corr2p2r.find(cs))==corr2p2r.end() ) {
        p2r_type                              p2r;
        unsigned int                          idx;
        correlationset_t::const_iterator      curprod;
        pair<corr2p2r_type::iterator, bool>   insres;

        // build the "value", ie the product-to-row mapping
        for( curprod=cs.begin(), idx=0; curprod!=cs.end(); curprod++, idx++ ) {
            pair<p2r_type::iterator,bool>  ires;
            ires = p2r.insert( make_pair(*curprod, idx) );
            if( !ires.second )
                THROW_JEXCEPT("Failed to insert entry in p2r_type mapping (product->row)?!");
        }
        insres = corr2p2r.insert( make_pair(cs, p2r) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to insert entry in the correlations to product-to-row mapping");
        mapptr = insres.first;
    }

    //  Now we know how many Interferometers need to be read, we can
    //  resize the weight vector (and theoretically the data
    //  matrix). We do not resize the data matrix since we will check
    //  later on that all the interferometers have the same amount of
    //  data! For the weights this is not so important; if we detect
    //  an error, we resize it back to zero-length!
    weight.resize( cs.size() );
    weight  = (Float)0.0;

    // Read all requested polarization products.
    // Take care of "swap_because_of_vex" since that may alter the 
    // polarization product.
    // (if we request RL and "swap_due_vex" == true, this means
    // that the product *after* swapping should read RL (because
    // the order in the vexfile dictates that the current baseline
    // should be swapped) and therefore, we must look for the LR
    // product.
    // Wether or not the data needs to be swapped depends on how this
    // specific product was computed:
    // if it was computed backwards AND swap_due_vex==true: nothing
    // needs to be swapped.
    // if it was computed in "normally" AND swap_due_vex==false, nothing
    // needs to be swapped either.
    for( curreq=cs.begin(); curreq!=cs.end(); curreq++ ) {
        CorrelationCode           finalcc( *curreq );
        if( swap_due_to_vex )
            finalcc.swap();
        // Now get the data and do stuff to it!
		bool                      swapit( false );
		bool                      autocorrelation;
		bool                      crosspolarization;
		bool                      expand;
		Float                     ifweight;
        float*                    ifdataptr;
		IPosition                 ipos;
        p2r_type::const_iterator  p2rptr;
		InterferometerDescriptor  ifref( cbref.getInterferometer(finalcc) );

        // If we got a "default" InterferometerDescriptor back this means
        // the indicated polarization could not be found and this means
        // we have a problem!
        if( ifref.getDataType()==InterferometerDescriptor::undetermined &&
            ifref.getValidity()==InterferometerDescriptor::invalid &&
            ifref.getCorrelation()==InterferometerDescriptor::nocorr )
            THROW_JEXCEPT("Failed to find requested product " << *curreq << " in baseline " << cbref);
        
		//  Cache these values
        //    nrfloats and nlag get initially the same value
        //    but they are separate variables since the NEED NOT
        //    be the same; if the interferometer sais it has
        //    complex data, the number of floats is twice
        //    the number of lags [1 float for real, 1 float for
        //    complex, per lag]
		unsigned int               nlag( ifref.getNumberOfLags() );
		unsigned int               nrfloats( ifref.getNumberOfLags() );
		unsigned int               nrweights;

        // If we're in bypassmode -> NEVER swap the data.
        // Otherwise, determine total "swappiness" of the interferometer
        // by combining the requirement to swap the whole baseline
        // [fn argument swap_due_to_vex] with the swapped property of the
        // interferometer.
        // For an even amount of swaps nothing needs to be done; for
        // an odd amount of swaps we need to swap the data.
        // (in this case equality/inequality of the booleans tells all)
        swapit            = ((ifref.swapped()!=swap_due_to_vex) && (swver!=CorrelatorSetup::bypassVersion));
		autocorrelation   = (ifref.getXAntenna()==ifref.getYAntenna());
		crosspolarization = (ifref.getXPolarization()!=ifref.getYPolarization());

        // If we cannot find the requested correlation-code in the product2row
        // map, we're stuffed! This means we wouldn't know where to put it!
        if( (p2rptr=mapptr->second.find(*curreq))==mapptr->second.end() )
            THROW_JEXCEPT("Failed to find entry in product2row map for requested "
                          << ((swap_due_to_vex==true)?("swapped "):(""))
                          << "product " << *curreq);

        // For autocorrelations only half the lags are computed
        // as they're symmetrical by definition. We must expand
        // those to match the shape of the cross-products.
        // Don't do that just now as we must remember how many 
        // datapoints there really are as opposed to how many we'd
        // like to have, but remember (expand==true) that we may
        // have to grow the data later.
		expand            = (autocorrelation && !crosspolarization);

		if( ifref.getDataType()==InterferometerDescriptor::complexdata )
			nrfloats*=2;

		switch( ifref.getValidity() ) {
			case InterferometerDescriptor::local:
				//nrweights = nlag;
				// HV: 13-03-2000 -> Chris told me that Albert told him that in
                //                   local validity also only ONE weight is given.....
				nrweights = 1;
				break;
		
			case InterferometerDescriptor::global:
				nrweights = 1;
				break;
		    
			default:
				nrweights = 0;
    			break;
		}
	    
		//  Get the data...
		//
		//  Detail: we receive a COPY of the data so we can mess
		//  around with it if we like!
		ifdataptr = myCurrentCorrelatedDataRecordptr->getInterferometerData( ifref );
	
		//  Do the weight-thing
        //  Take care to put the weight at the same row as the data!!!!
		ifweight = 0.0;
		for( unsigned int j=0; j<nrweights; j++ )
			ifweight += ifdataptr[ nrfloats+j ];

		if( nrweights>1 )
			ifweight/=nrweights;

		weight( p2rptr->second ) = ifweight;

		//  HV: 30-11-2000 - Decided to first work out the total amount
		//                   of datashift (since Albert's messing around
		//                   with the lag-spectrum....). After we that,
		//                   we can guarantee that the central-lag (=delay 0)
		//                   is at lag nr (NLAG/2). This had certain 
		//                   advantages :) (upstream operations like 
		//                   e.g. FFT, rotation become easier)
		//      22-03-2001 - NOTE! Built in support for bypass mode!
		//                   Only shows up at the very last stage, just before
		//                   starting to do the shift. Don't forget that the
		//                   the value of datashift may be overridden at the
		//                   last moment..... (no matter of other decisions 
		//                   and/or calculations made before that statement);
		//                   in short: don't let the code fool you!!!!!
		int         datashift( 0 );

		//  If vernier-delay was applied, it means the data has been shifted
		//  one lag to the right so we need a shift of -1 to get the central lag
		//  back at (NLAG/2)...
		if( vernier &&
            (!autocorrelation || (autocorrelation && crosspolarization)))
		        datashift -= 1;
			
		//  As of software version 1.0 Albert shifted *all* lagspectra one lag
		//  to the left! In this case we need a shift of +1 to get the central lag
		//  back at (NLAG/2)
		if( swver>=version1 &&
            (!autocorrelation || (autocorrelation&&crosspolarization)))
			    datashift += 1;

		//  If we detect the bypassversion, we completely discard all the results
		//  of all stuff before! Force datashift to 0 (so no datashift is performed)
		//
		//  **************************** NOTE **************************************
		//
		//  You MUST leave this statement as the last before actually starting to do
		//  the datashift (i.e. *just* before the 'if(datashift)...' statement!!
		//
		//  ************************************************************************ 
		if( swver==CorrelatorSetup::bypassVersion )
			datashift = 0;

		if( datashift ) {
			//  Ok! Data needs to be shifted!
			int       bsrc, bdst;
			int       ssrc, sdst;
			int       numshift( ::abs(datashift) );
			float     buffer[ numshift ];

			if( datashift<0 ) {
				// we need to buffer lags 0..abs(datashift)
				// so we can stick them at the back later on!
				bsrc = 0;
				bdst = nlag-numshift;
				
				// sstart = shiftstart = index from where the data needs 
				//      needs to be shifted
				ssrc = numshift;
				sdst = 0;
			} else {
				//  need to buffer the last values so we can stick them in
				//  front later on...
				bsrc = nlag-numshift;
				bdst = 0;

				// compute start and dest. indices of shift operation
				ssrc = 0;
				sdst = numshift;
			}

			//  Buffer values for real part (imag will be conditional...)
			::memcpy( (void*)buffer, (void*)(&ifdataptr[bsrc]), numshift*sizeof(float) );

			//  Move all the real-parts
			::memmove( (void*)(&ifdataptr[sdst]), (void*)(&ifdataptr[ssrc]), 
						   (nlag-numshift)*sizeof(float) );
	
			// Put back buffer at correct location
			::memcpy( (void*)(&ifdataptr[bdst]), (void*)buffer, numshift*sizeof(float) );
			
			//  Interferometers satisfying the following conditions have
			//  imaginary parts... All indices are increased by NLAG
			if( (ifref.getDataType()==InterferometerDescriptor::complexdata) &&
                (!autocorrelation || (autocorrelation && crosspolarization)) ) {
				bsrc += nlag;
				bdst += nlag;
				ssrc += nlag;
				sdst += nlag;

				//  Buffer values for imaginary part
				::memcpy( (void*)buffer, (void*)(&ifdataptr[bsrc]), numshift*sizeof(float));

				//  Move all the real-parts
				::memmove( (void*)(&ifdataptr[sdst]), (void*)(&ifdataptr[ssrc]), 
					 		(nlag-numshift)*sizeof(float) );
	
				// Put back buffer at correct location
				::memcpy( (void*)(&ifdataptr[bdst]), (void*)buffer, numshift*sizeof(float));
			}
		}

		//  Having dealt with that, we can now create the complex numbers!

		//  Create a buffer for the re-ordered complex numbers
        //  The raw data is properly processed so this might be an
        //  appropriate time to multiply nlag by two (if needed)
        //  because from here on we'll create the data-arrays
        //  for the caller
		if( expand )
			nlag *= 2;
       
        // If it's the first time we enter here we can (finally)
        // resize the matrix as we now know for sure how many
        // datapoints per product there should be!
        if( curreq==cs.begin() )
            retval.resize( cs.size(), nlag );

        // Get a reference to the correct row in the matrix
        Vector<Complex>  cmplxreordered( retval.row(p2rptr->second) );
	    
		//  The only exeptional case is 'expand' -> we deal with that occasion
		//  differently....
		//
		//  Note: the following code features some duplication but it enables
		//  us to form all dataarrays with one loop (only one for-loop will
		//  be executed per case!) Otherwise there might have been occasions
		//  where we have to execute two for-loops
		if( expand ) {
			unsigned int    centrallag( nlag/2 );
			unsigned int    lags2do( (nlag/2)-1 );
	
			for( unsigned int i=0; i<=lags2do; i++ ) {
				cmplxreordered[ centrallag+i ] = Complex( ifdataptr[i], 0.0f );
				cmplxreordered[ centrallag-i ] = Complex( ifdataptr[i], 0.0f );
			}
			cmplxreordered[ 0 ] = Complex( 0.0f, 0.0f );
		} else {
			//   Remeber: the structure of the data in memory is this -
			//
			//   <real><real>.....<real><imag><imag>....<imag>
			//     |                 |     |               |
			//     ------ nlag ------      ----- nlag ------
			//
			//
			//  Note: ifdataptr = ptr to float!
			//
			//  Ok. Vernier-delay has been dealt with. Now either copy
			//  the data or fill it in swapped...
			//
			//  If we need to swap it then we must mirror the lags
			//  about the central lag (which is different from just
			//  reversing the lag order (guess how we found *that*
			//  out... by experience) and take the complex
			//  conjugate. Reversing would have done the trick if this
			//  were an odd-sized array of samples but alas, the
			//  number of lags is always even....
			if( swapit ) {
				unsigned int    lagcnt;
				unsigned int    zerolag( nlag/2 );
				unsigned int    lags2do( (nlag/2)-1 );
	
	
				for( lagcnt=0; lagcnt<=lags2do; lagcnt++ ) {
					cmplxreordered[ zerolag+lagcnt ] = Complex( ifdataptr[zerolag-lagcnt],
													   -1.0*ifdataptr[nlag+zerolag-lagcnt]);
					cmplxreordered[ zerolag-lagcnt ] = Complex( ifdataptr[zerolag+lagcnt],
													   -1.0*ifdataptr[nlag+zerolag+lagcnt]);
				}
				//  Only the zeroth point in the data has not been filled in....
				//
				//  HV: 22-03-2001 - After dicussion with Chris and Bob, decided
				//                   to take this statement out. The first lag
				//                   *is* filled in! So no need to force it to
				//                   zero! NOOOOOH! It must be filled in with data
				//                   from the buffer.....
				//
				//cmplxreordered[ 0 ] = Complex( 0.0f, 0.0f );
				cmplxreordered[ 0 ] = Complex( ifdataptr[0], ifdataptr[nlag] );
			} else {
				//  Ultimately simple!
				for( unsigned int lagcnt=0; lagcnt<nlag; lagcnt++ ) {
					cmplxreordered[ lagcnt ] = Complex( ifdataptr[lagcnt],
													ifdataptr[nlag+lagcnt] );
				}
			}
		}
		//  Clean up, update loop stuff and go on
		delete [] ifdataptr;
    }
    return retval;
}
#if 0

    // ===========> above: new
    // ===========> below: old
    
    //  Read all interferometers
    for( curcor=cors2do.begin(), row=0;
         curcor!=cors2do.end();
         curcor++, row++ )
    {
		bool                      swapit( false );
		bool                      autocorrelation;
		bool                      crosspolarization;
		bool                      expand;
		Float                     ifweight;
        float*                    ifdataptr;
		Complex*                  cmplxreordered;
		IPosition                 ipos;
        p2r_type::const_iterator  p2rptr;
		InterferometerDescriptor  ifref( cbref.getInterferometer(*curcor) );

	    
        // "swapit" means "swap the data". However, if the baseline is to be
        // swapped, the polarizationlabels will swap and so potentially the
        // destination row will change. Here we form the "final" correlationcode,
        // ie, what the product will eventually mean, after potentially swapping
        // the data. based on this product we will find the destination row.
        // Note: the "swapped()" method of an interferometer basically tells
        // if the interferometer was inserted into the baseline with swapped
        // coordinates (X/Y ANT, X/Y POL) but it does not affect the data (yet).
        // Once we figure out where the data really should go (after a potential
        // second swap) we might deduce wether or not the underlying data should
        // be physically swapped or not [that's the 'swapit' variable].
        if( swap_due_to_vex )
            finalcc.swap();

        // If we cannot find the final correlation-code in the product2row
        // map, we're stuffed! This means we wouldn't know where to put it!
        if( (p2rptr=p2r.find(finalcc))==p2r.end() )
            THROW_JEXCEPT("Failed to find entry in product2row map for "
                          << ((swap_due_to_vex==true)?("swapped "):(""))
                          << "product " << finalcc);

        // autocorrelations only yield half the number of lags
        // (since they're symmetrical by definition) but we like
        // to give the user a nice square matrix where *all*
        // interferometers have the same number of lags, so
        // for autocorrelations we have to 'expand' the data
        //
        // NOTE: do NOT multiply the 'nlag' variable by two
        // yet (as needs to be done if 'expand==true' 
        // BECAUSE 'nlag' is first used as measure of how
        // many datapoints there really are in the data,
        // which is, as said before, only *half* of
        // what we'd have to give to the user if 'expand==true'.
        // Some actions (may) have to be performed on the
        // raw data, before we can actually expand it.
        // So, therefore, we just flag the fact that we
        // need to expand at some point and leave it up for 
        // later to multiply 'nlag' by 2 at the appropriate time
		expand            = (autocorrelation && !crosspolarization);

		if( ifref.getDataType()==InterferometerDescriptor::complexdata )
			nrfloats*=2;

		switch( ifref.getValidity() ) {
			case InterferometerDescriptor::local:
				//nrweights = nlag;
				// HV: 13-03-2000 -> Chris told me that Albert told him that in
                //                   local validity also only ONE weight is given.....
				nrweights = 1;
				break;
		
			case InterferometerDescriptor::global:
				nrweights = 1;
				break;
		    
			default:
				nrweights = 0;
    			break;
		}
	    
		//  Get the data...
		//
		//  Detail: we receive a COPY of the data so we can mess
		//  around with it if we like!
		ifdataptr = myCurrentCorrelatedDataRecordptr->getInterferometerData( ifref );
	
		//  Do the weight-thing
        //  Take care to put the weight at the same row as the data!!!!
		ifweight = 0.0;
		for( unsigned int j=0; j<nrweights; j++ )
			ifweight += ifdataptr[ nrfloats+j ];

		if( nrweights>1 )
			ifweight/=nrweights;

		weight( p2rptr->second ) = ifweight;

		//  HV: 30-11-2000 - Decided to first work out the total amount
		//                   of datashift (since Albert's messing around
		//                   with the lag-spectrum....). After we that,
		//                   we can guarantee that the central-lag (=delay 0)
		//                   is at lag nr (NLAG/2). This had certain 
		//                   advantages :) (upstream operations like 
		//                   e.g. FFT, rotation become easier)
		//
		//
		//      22-03-2001 - NOTE! Built in support for bypass mode!
		//                   Only shows up at the very last stage, just before
		//                   starting to do the shift. Don't forget that the
		//                   the value of datashift may be overridden at the
		//                   last moment..... (no matter of other decisions 
		//                   and/or calculations made before that statement);
		//                   in short: don't let the code fool you!!!!!
		int         datashift;

		datashift = 0;

		//  If vernier-delay was applied, it means the data has been shifted
		//  one lag to the right so we need a shift of -1 to get the central lag
		//  back at (NLAG/2)...
		if( vernier &&
            (!autocorrelation || (autocorrelation && crosspolarization)))
		        datashift -= 1;
			
		//  As of software version 1.0 Albert shifted *all* lagspectra one lag
		//  to the left! In this case we need a shift of +1 to get the central lag
		//  back at (NLAG/2)
		if( swver>=version1 &&
            (!autocorrelation || (autocorrelation&&crosspolarization)))
			    datashift += 1;

		//  If we detect the bypassversion, we completely discard all the results
		//  of all stuff before! Force datashift to 0 (so no datashift is performed)
		//
		//  **************************** NOTE **************************************
		//
		//  You MUST leave this statement as the last before actually starting to do
		//  the datashift (i.e. *just* before the 'if(datashift)...' statement!!
		//
		//  ************************************************************************ 
		if( swver==CorrelatorSetup::bypassVersion )
			datashift = 0;

		if( datashift ) {
			//
			//  Ok! Data needs to be shifted!
			//
			int       bsrc, bdst;
			int       ssrc, sdst;
			int       numshift( ::abs(datashift) );
			float     buffer[ numshift ];

			if( datashift<0 ) {
				// we need to buffer lags 0..abs(datashift)
				// so we can stick them at the back later on!
				bsrc = 0;
				bdst = nlag-numshift;
				
				// sstart = shiftstart = index from where the data needs 
				//      needs to be shifted
				ssrc = numshift;
				sdst = 0;
			} else {
				//  need to buffer the last values so we can stick them in
				//  front later on...
				bsrc = nlag-numshift;
				bdst = 0;

				// compute start and dest. indices of shift operation
				ssrc = 0;
				sdst = numshift;
			}

			//  Buffer values for real part (imag will be conditional...)
			::memcpy( (void*)buffer, (void*)(&ifdataptr[bsrc]), numshift*sizeof(float) );

			//  Move all the real-parts
			::memmove( (void*)(&ifdataptr[sdst]), (void*)(&ifdataptr[ssrc]), 
						   (nlag-numshift)*sizeof(float) );
	
			// Put back buffer at correct location
			::memcpy( (void*)(&ifdataptr[bdst]), (void*)buffer, numshift*sizeof(float) );
			
			//  Interferometers satisfying the following conditions have
			//  imaginary parts... All indices are increased by NLAG
			if( (ifref.getDataType()==InterferometerDescriptor::complexdata) &&
                (!autocorrelation || (autocorrelation && crosspolarization)) )
			{
				bsrc += nlag;
				bdst += nlag;
				ssrc += nlag;
				sdst += nlag;

				//  Buffer values for imaginary part
				::memcpy( (void*)buffer, (void*)(&ifdataptr[bsrc]), numshift*sizeof(float));

				//  Move all the real-parts
				::memmove( (void*)(&ifdataptr[sdst]), (void*)(&ifdataptr[ssrc]), 
					 		(nlag-numshift)*sizeof(float) );
	
				// Put back buffer at correct location
				::memcpy( (void*)(&ifdataptr[bdst]), (void*)buffer, numshift*sizeof(float));
			}
		}

		//  Having dealt with that, we can now create the complex numbers!

		//  Create a buffer for the re-ordered complex numbers
        //  The raw data is properly processed so this might be an
        //  appropriate time to multiply nlag by two (if needed)
        //  because from here on we'll create the data-arrays
        //  for the caller
		if( expand )
			nlag *= 2;
        
        // see! this is what he/she'll get :)
		cmplxreordered = new Complex[ nlag ];
	    
		// The size of the resulting Vector<Complex>
		ipos = IPosition( 1, nlag );

		//  The only exeptional case is 'expand' -> we deal with that occasion
		//  differently....
		//
		//  Note: the following code features some duplication but it enables
		//  us to form all dataarrays with one loop (only one for-loop will
		//  be executed per case!) Otherwise there might have been occasions
		//  where we have to execute two for-loops
		if( expand ) {
			unsigned int    centrallag( nlag/2 );
			unsigned int    lags2do( (nlag/2)-1 );
	
			for( unsigned int i=0; i<=lags2do; i++ ) {
				cmplxreordered[ centrallag+i ] = Complex( ifdataptr[i], 0.0f );
				cmplxreordered[ centrallag-i ] = Complex( ifdataptr[i], 0.0f );
			}
			cmplxreordered[ 0 ] = Complex( 0.0f, 0.0f );
		} else {
			//   Remeber: the structure of the data in memory is this -
			//
			//   <real><real>.....<real><imag><imag>....<imag>
			//     |                 |     |               |
			//     ------ nlag ------      ----- nlag ------
			//
			//
			//  Note: ifdataptr = ptr to float!
			//
			//  Ok. Vernier-delay has been dealt with. Now either copy
			//  the data or fill it in swapped...
			//
			//  If we need to swap it then we must mirror the lags
			//  about the central lag (which is different from just
			//  reversing the lag order (guess how we found *that*
			//  out... by experience) and take the complex
			//  conjugate. Reversing would have done the trick if this
			//  were an odd-sized array of samples but alas, the
			//  number of lags is always even....
			if( swapit ) {
				unsigned int    lagcnt;
				unsigned int    zerolag( nlag/2 );
				unsigned int    lags2do( (nlag/2)-1 );
	
	
				for( lagcnt=0; lagcnt<=lags2do; lagcnt++ ) {
					cmplxreordered[ zerolag+lagcnt ] = Complex( ifdataptr[zerolag-lagcnt],
													   -1.0*ifdataptr[nlag+zerolag-lagcnt]);
					cmplxreordered[ zerolag-lagcnt ] = Complex( ifdataptr[zerolag+lagcnt],
													   -1.0*ifdataptr[nlag+zerolag+lagcnt]);
				}
				//  Only the zeroth point in the data has not been filled in....
				//
				//  HV: 22-03-2001 - After dicussion with Chris and Bob, decided
				//                   to take this statement out. The first lag
				//                   *is* filled in! So no need to force it to
				//                   zero! NOOOOOH! It must be filled in with data
				//                   from the buffer.....
				//
				//cmplxreordered[ 0 ] = Complex( 0.0f, 0.0f );
				cmplxreordered[ 0 ] = Complex( ifdataptr[0], ifdataptr[nlag] );
			} else {
				//  Ultimately simple!
				for( unsigned int lagcnt=0; lagcnt<nlag; lagcnt++ ) {
					cmplxreordered[ lagcnt ] = Complex( ifdataptr[lagcnt],
													ifdataptr[nlag+lagcnt] );
				}
			}
		}
	
        // put the data for this interferometer at the correct row
        datarow[ p2rptr->second ] = new Vector<Complex>(ipos, cmplxreordered, COPY);

		//  Clean up, update loop stuff and go on
		delete [] ifdataptr;
		delete [] cmplxreordered;
    }

    //  Check shapes
    unsigned int        rowcnt;
    ifdata_t::iterator  currow;

    for( currow=datarow.begin(), rowcnt=0;
         currow!=datarow.end();
         currow++, rowcnt++ )
    {
        // take the shape of the first datarow and
        // use that to resize the resulting matrix
        if( currow==datarow.begin() ) 
            retval.resize( cors2do.size(),  (*currow)->size() );

        // and now copy data across
	    retval.row( rowcnt ) = *(*currow);

        // currow has been dealt with - release the memory
        // associated with it (and set the pointer to null
        // so we cannot accidentily re-use it without knowing
        // (at least we get a SIGFAULT if we erroneously
        //  try to use this slot after we've dealt with it..)
        delete (*currow);
        (*currow) = 0;
    }
    return retval;
#endif


/**************************************************************/
//
//
//              Static method(s)
//
//
/**************************************************************/


//
//  Transform the FF/SFROT into a midpoint time as MEpoch
//
const MEpoch& COFVisibilityBuffer::calcMidPointAsMEpoch( const FFROT& ffrot,
							 const SFROT& startrot,
							 const SFROT& endrot ) 
{
    //
    //  Prevent this one from being created over and over again...
    //
    static MEpoch        retval;
    static MVTime        mvtime;
    static MVEpoch       mvepoch;
    static DayConversion dayConversion;

    // Okiedokie, do the dirty work...
    int          year( 0 );
    SFROT        duration;
    Double       remaininsysclks;
    Double       sysclksperday( (Double)SYSCLK_FREQ*(Double)DayConversion::secondsPerDay );
    Double       daywithfraction;  // daywithfraction = <day in month>.<fractional day>
    double       halfduration;
    unsigned int month, day;       // month 0..11, day=1..31
    unsigned int nrwholedays;
    
    //  First determine the duration of the period
    duration     = endrot - startrot;
    halfduration = (duration.get_sysclks()) / 2;
    
    //  (mis)use the integrationDuration, and transform it such that
    //  it is the midpoint of the integration... (still in sysclks)
    duration     = (Short_Time_Value&)startrot + Short_Time_Value(halfduration);
    
    //  Ok. The short-form rot is the time in SYSCLKS since the start
    //  of the YEAR.... so work out the daynr from here...
    nrwholedays     = (unsigned int)((duration.get_sysclks())/sysclksperday);
    remaininsysclks = (duration.get_sysclks()) - (Double)nrwholedays*sysclksperday;

    //  Base date is the year???
    if( ffrot.get_base_date()!=-1 )
    {
    	year = ffrot.get_base_date();
    }

    if( DayConversion::dayNrToMonthDay(month, day, nrwholedays+1, year) )
    {
    	daywithfraction = (Double)day + remaininsysclks/sysclksperday;
    }
    else
    {
    	month           = 0;
    	daywithfraction = 0.0;
    }

    mvtime  = MVTime( year, month+1, daywithfraction );
    mvepoch = MVEpoch( mvtime.day() );
    retval  = MEpoch( mvepoch, MEpoch::UTC );

    return retval;
}


// Read stuff from MFC file. As of 21/02/2007, be more lenient.
// We required a full MFC-setup, but from this date on, we
// just pick up anything that's recognized by us.
// This allows the boys to shuffle the SU-channels around
// and without hacking the VEXfile still be able to get
// their data labelled properly.
bool COFVisibilityBuffer::readMFCSetup( mfcSetup& result, const String& mfcfile ) {
	// Check status of mfcfile
	struct stat    sb;

	if( ::stat(mfcfile.c_str(), &sb) ) {
		cerr << "readMFCSetup: Problem with " << mfcfile << "?\n"
			 << "              " << ::strerror(errno) << endl;
		return false;
	}

	if( (sb.st_mode&S_IFMT)!=S_IFREG ) {
		cerr << "readMFCSetup: File " << mfcfile << " is not a regular file?!"
			 << endl;
		return false;
	}

	// Typedef to indicate the status of the parser
	typedef enum _match {
		srcaliasbeg = 0x1, srcaliasend=(0x1<<2), crmshuffle=(0x1<<3),
		sourceshuffle=(0x1<<4), other=(0x1<<5)
	} match;

	// Open and parse the file....
	int           lineNr;
	bool          readalias;
	char          linebuf[1024];
	char*         commentptr;
	match         curmatch;
	Regex         rx_allws( "[ \t]*" );
	Regex         rx_srcaliasbeg( "^[ \t]*source_alias" );
	Regex         rx_srcaliasend( "^[ \t]*end_alias" );
	Regex         rx_crmshuffle( "^[ \t]*crm_override[ \t]+" );
	Regex         rx_srcshuffle( "^[ \t]*source_override[ \t]+" );
	ifstream      mfc( mfcfile.c_str(), ios::in );
	alias_type    alias;
	vector<int>   phys2log = vector<int>(16, -1);
	vector<int>   phys2src = vector<int>(16, -1);
	unsigned int  itemsRead;

    // Make sure that result is properly initialized
    result = mfcSetup();
    
	// Read until either end-of-file or we have read all we need to reed :)
	lineNr    = 0;
	readalias = false;
	itemsRead = 0;	

	while( !mfc.eof() ) {
		mfc.getline( linebuf, sizeof(linebuf)-1 );
		lineNr++;

		if( (commentptr=::strchr(linebuf,'#'))!=0 )
			*commentptr = '\0';
        
        // transform to casa::String so's we can use
        // the casa::Regex classes with it
        String   linestr( linebuf );
		
		// Nothing but whitespace? -> continue
		if( linestr.matches(rx_allws) )
			continue;

		// Find out what the line was...
		curmatch = other;
		if( linestr.find(rx_srcaliasbeg)!=String::npos ) {
			curmatch = srcaliasbeg;
		} else if( linestr.find(rx_srcaliasend)!=String::npos ) {
			curmatch = srcaliasend;
		} else if( linestr.find(rx_crmshuffle)!=String::npos ) {
			curmatch = crmshuffle;
		} else if( linestr.find(rx_srcshuffle)!=String::npos ) {
			curmatch = sourceshuffle;
		}
	
		// Now see how this fits into our current state....
		switch( curmatch ) {
			case srcaliasbeg:
				{
					if( readalias ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   Found start of sourcealias inside a sourcealias block!"
							<< endl;
						break;
					}
	
					if( itemsRead&srcaliasend ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
				   			<< "   Duplicate sourcealias block?!"
				   			<< endl;
						break;
					}
					readalias = true;
				}
				break;
					
			case srcaliasend:
				{
					if( !readalias ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   found end of sourcealias outside of sourcealias block!"
							<< endl;
						break;
					}
					if( itemsRead&srcaliasend ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   Found duplicate sourcealias end?!"
							<< endl;
						break;
					}
	
					// Switch off reading of sourcealiases
					// And indicate in itemsread that we've read a complete sourcealias
					// block
					readalias  = false;
					itemsRead |= srcaliasend;
				}
				break;
	
			case crmshuffle:
				{
					if( readalias ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   Found crm override inside a sourcealias block?!"
							<< endl;
						break;
					}
					// test if we already have seen a line 'quite like this'
					if( itemsRead&crmshuffle ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   Found duplicate crm override. Discarding this one."
							<< endl;
						break;
					}
					// The phys2log map has already been initialized with "unused"
                    // entries. We are going to overwrite it with entries from the
                    // crmshuffle command
					vector<int>::iterator   ptr;
		
					int            tmp;
					char*          args = ::strpbrk( linebuf, " \t" );
					istringstream  argstrm( args );
		
					ptr = phys2log.begin();
					while( !argstrm.eof() && ptr!=phys2log.end() ) {
						argstrm >> tmp;
						*ptr = tmp;
						ptr++;
					}
		
					// Indicate we have read another of the required items!
					itemsRead |= crmshuffle;
				}
				break;
	
			case sourceshuffle:
				{
					if( readalias ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   Found source override inside a sourcealias block?!"
							<< endl;
						break;
					}
					// test if we already have seen a line 'quite like this'
					if( itemsRead&sourceshuffle ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   Found duplicate source override. Discarding this one."
							<< endl;
						break;
					}
					// The phys2src map has already been initialized with "unused"
                    // entries. We are going to overwrite it with entries from the
                    // sourceshuffle command
					vector<int>::iterator   ptr = phys2src.begin();
		
					int            tmp;
					char*          args = ::strpbrk( linebuf, " \t" );
					istringstream  argstrm( args );
		
					ptr = phys2src.begin();
					while( !argstrm.eof() && ptr!=phys2src.end() ) {
						argstrm >> tmp;
						*ptr = tmp;
						ptr++;
					}
		
					// Indicate we have read another of the required items!
					itemsRead |= sourceshuffle;
				}
				break;
			
			case other:
				{
					// if we get to here we'd better be reading aliases....
					// otherwise the file is fuckedup!
					if( !readalias ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
							<< "   Unrecognized configline!"
							<< endl;
						break;
					}
					// format of the line should be:
					// <orgsrcname> <field1name> <field2name> ... <fieldNname>
					string                   src;
					istringstream            strm( linebuf );
					alias_type::iterator     ptr;
                    alias_type::mapped_type  curalias;
	
					// step 1: read the original sourcename
					strm >> src;

					// step 2: check if not duplicated
					if( (ptr=alias.find(src))!=alias.end() ) {
						cerr << "readMFCSetup(" << lineNr << "): Syntax error\n"
					   		<< "   Duplicate sourcealias line for source " << src
					   		<< endl;
						break;
					}
	
					// read all aliases...
					while( !strm.eof() ) {
						string   name;
							
						strm >> name;
						curalias.push_back( name );
					}
					
					// now define the map-entry
					alias[ src ] = curalias;
				}
				break;
					
			default:
				cerr << "readMFCSetup(" << lineNr << ") - case default! AARGH\n"
				     << "   call H. Verkouter +31521596516 (verkouter@jive.nl)"
					 << endl;
                break;
		}
	}
    if( readalias ) {
        cerr << "readMFCSetup/Unexpected end-of-file; Missing \"end_alias\" statement" << endl;
        return false;
    }
    // Pick up anything that was read from the file
    if( itemsRead&srcaliasend ) 
		result.srcToAliasMap = alias;
    if( itemsRead&crmshuffle )
		result.physToLogMap  = phys2log;
    if( itemsRead&sourceshuffle )
		result.physToFieldCenterMap = phys2src;
	return true;
}



vector<int> COFVisibilityBuffer::defaultPhysToLogMap( void ) {
	vector<int>            retval( 16 );
	vector<int>::iterator  ptr = retval.begin();

	for( int i=0; ptr!=retval.end(); *ptr=i, i++, ptr++ );

	return retval;
}

vector<int> COFVisibilityBuffer::defaultPhysToSrcMap( void ) {
	return vector<int>( 16, 0 );
}
#endif
