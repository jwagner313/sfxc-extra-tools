//
//  Define a global fn...
//
#include <jive/hvplugin/sizeSplit.h>

#include <casa/aips.h>

#include <tables/Tables/ExprNode.h>
#include <iostream>

#include <cstdlib>
#include <cmath>

#ifdef __sun__
#include <ieeefp.h>
#else
#include <cmath>
#endif

using namespace std;
using namespace casa;

#define DEBUG 1

#define SHOW(a,b) (cout << (#a) << '=' << (a) << (b) << endl)


// This method will attempt to break up the MS in chunks of
// max 2GB/piece
MSConversionInfo**  sizeSplit(const MeasurementSet& msref,
							  unsigned int& npieces) {
    uInt                 nrowperpiece;
    uInt                 nmsrow;
    double               tmp_npiece;
    double               size_per_row;
    double               overhead_per_row;
    double               size_per_visibility;
    // allow for ~200MB overhead per chunk?
    const double         max_size_per_chunk( (double)(((unsigned)1)<<31) - 200*(1024*1024) );
    MSConversionInfo     base( msref );
    MSConversionInfo**   retval( 0 );

    npieces = 0;
    nmsrow  = msref.nrow();
    if( !nmsrow )
        return 0;

    // Ok. From the MSConversionInfo we can derive the
    // approx size of a row in the MS...
    // Then we can easily work out how many rows of
    // the MS can be put in a single FITS-file
    // 
    // Note: there is a factor "nr-of-IFs" between the
    // two row-sizes.
    // fits-row-size = nrIFs * ms-row-size
    //
    // Note: you can check the 'uvtableGenerator.cc' source
    //       file to see what will get written to the FITS
    //       file
    //
    // Data is written as 3 Floats (Re/Im/Wgt) (=3*4bytes) per datapoint +
    //   a number of indices per row.
    // Number of datapoints in a single row: 
    //   number of stokes parms (number of polarizations) *
    //   number of channels  * nr-of-IFs
    size_per_visibility = (double)base.nrPolarizations() *
                          (double)base.nrChannels() *
                          (double)base.nrIFs() *
                          (3 * sizeof(Float));

    // The per-row overhead is a number of extra indices and
    // some per-visibility constant numbers (UVW, date, time)
    //
    // We have 4*Float (for U,V,W, and inttim),
    //  3*(unsigned)Int (baseline, sourceid, freqid),
    //  and 2 doubles (date,time)
    overhead_per_row = 4*sizeof(Float) +
                       3*sizeof(Int) +
                       2*sizeof(Double);
    
    // This gives us the nominal size_per_row:
    size_per_row = size_per_visibility + overhead_per_row;

    // Now make an estimate how many row we can fit in the
    // max size/chunk. Note: these are *fits* rows
    // [so they take up nr-of-IFs MS-rows]
    nrowperpiece = (uInt)(max_size_per_chunk/size_per_row);

    // The number of pieces we must break the MS up into,
    // is the number of rows in the MS divided by the number
    // of MS-rows that we can fit in a single FITS-chunk.
    // As 1 FITS row takes up nr-of-IF MS-rows, that's quite
    // a simple computation...
    tmp_npiece   = ::ceil( (double)msref.nrow()/(double)(nrowperpiece*base.nrIFs()) );
    if( isnan(tmp_npiece) ) {
        cout << "sizeSplit()/Aaaargh???? MS.nrow() / nrowperpiece yields NaN??? " << endl
             << "   Actual values are: MS.nrow()   =" << msref.nrow() << endl
             << "                      nrowperpiece=" << nrowperpiece << endl;
        return 0;
    }
    npieces      = (uInt)tmp_npiece;
#ifdef DEBUG
    cout << "sizeSplit: stats ..." << endl;
    SHOW(nmsrow," rows");
    SHOW(size_per_visibility, " bytes");
    SHOW(overhead_per_row, " bytes");
    SHOW(size_per_row," bytes");
    SHOW(max_size_per_chunk," bytes");
    SHOW(nrowperpiece," rows");
    SHOW(tmp_npiece,"");
    SHOW(npieces,"");
//    SHOW( ((unsigned)0x1<<31), " bytes = max 32bit );
#endif
    // Ok. Go ahead and create the pieces!
    retval = new MSConversionInfo*[ npieces ];
    
    cout << "Breaking MS up into " << npieces << " piece"
         << ((npieces!=1)?("s"):("")) << "..." << endl;

    // if only one piece, don't bother to select...
    if( npieces==1 ) {
        retval[ 0 ] = new MSConversionInfo(base, (MS&)msref, 0, 1);
        return retval;
    }
   
    // re-note: nrowperpiece is in units of FITS rows; that is,
    // there's num-if MS-rows to a single FITS row....
    for( uInt p=0; p<npieces; p++ ) {
        Table    tmp( (msref)((msref.nodeRownr()>=Int(p*nrowperpiece*base.nrIFs())) && 
                              (msref.nodeRownr()<Int((p+1)*nrowperpiece*base.nrIFs()))) );
	
		retval[ p ] = new MSConversionInfo(base, (MS&)tmp, p, npieces);
    }
    return retval;
}

