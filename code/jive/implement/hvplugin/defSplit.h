//
//  Function prototype for the default splitting routine
//
//
#ifndef DEFSPLIT_H
#define DEFSPLIT_H

#include <ms/MeasurementSets/MeasurementSet.h>
#include <casa/Exceptions.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

//
//  The defaultSplit routine -> we do not split at all
//
MSConversionInfo**  defSplit( const casa::MeasurementSet& msref,
		 					  unsigned int& npieces );

#endif
