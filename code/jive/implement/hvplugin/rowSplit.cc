//
//  Define a global fn...
//
#include <jive/hvplugin/rowSplit.h>

#include <casa/aips.h>

#include <tables/Tables/ExprNode.h>
#include <iostream>

#include <stdlib.h>


using namespace casa;

//
//  The rowSplit routine -> we split based on rownrs
//
MSConversionInfo**  rowSplit( const MeasurementSet& msref,
							  unsigned int& npieces )
{
    uInt                 nrowperpiece;
    uInt                 tmpnpiece;
    uInt                 nrrows;
    Bool                 done;
    Bool                 condition1;
    Bool                 condition2;
    Bool                 condition3;
	MSConversionInfo     base( msref );
    MSConversionInfo**   retval( 0 );

    npieces = 0;
    
    if( !msref.nrow() )
    {
	return retval;
    }


    srand( time(0) );
    
    //
    //  Make a guess about how many pieces/how many rows per piece
    //
    done = False;
    nrowperpiece = 2000;
    nrrows = msref.nrow();
    
    //cout << " Start loop: " << endl;
    
    while( !done )
    {
	tmpnpiece = nrrows/nrowperpiece;
	
	if( (nrrows%nrowperpiece)!=0 )
	{
	    tmpnpiece++;
	}
	
	//
	// if the number of pieces is too low or too high adapt the
	// value
	//
	condition1 = ( tmpnpiece<2 );
	condition2 = ( tmpnpiece>20 );
	condition3 = ( tmpnpiece>10 && nrowperpiece<=6000 );
	
	done = ( !condition1 && !condition2 && !condition3 );

	//cout << "tmpnpiece=" << tmpnpiece << " nrrows=" << nrrows << " nrowperpiece=" << nrowperpiece << endl;
	//cout << "condition1=" << condition1 << " condotion2=" << condition2 << " done=" << done << endl;
	
	if( !done )
	{
	    //
	    //  If the ratio of npieces/nrowsperpiece
	    //
	    if( condition3 )
	    {
		//
		//  nrows/piece too small; we get too many pieces
		//
		nrowperpiece += (rand()/((1<<15)-1) * (nrrows/6) );
	    }
	    else if( condition1 )
	    {
		//
		//  nrows/piece too big...
		//
		nrowperpiece -= (rand()/((1<<15)-1) * (nrrows/5) );
	    }
	    else if( condition2 )
	    {
		//
		//  nrows/piece too small; we get too many pieces
		//
		nrowperpiece += (rand()/((1<<15)-1) * (nrrows/6) );
	    }
	}
    }

    //
    //  Now create the various pieces
    //    
    npieces = tmpnpiece;
    
    retval = new MSConversionInfo*[ npieces ];
    
    cout << "Breaking MS up into " << npieces << " pieces.." << endl;

    for( uInt p=0; p<npieces; p++ )
    {
		Table    tmp( (msref)( (msref.nodeRownr()>=Int(p*nrowperpiece)) && 
					(msref.nodeRownr()<Int((p+1)*nrowperpiece)) ) );
	
//		retval[ p ] = new MSConversionInfo( (MS&)tmp, p, npieces );
		retval[ p ] = new MSConversionInfo( base, (MS&)tmp, p, npieces );
    }
    return retval;
}
