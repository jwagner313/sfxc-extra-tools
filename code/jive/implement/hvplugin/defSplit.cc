//
//  Define a global fn...
//
#include <jive/hvplugin/defSplit.h>

using namespace casa;

//
//  The defaultSplit routine -> we do not split at all
//
MSConversionInfo**  defSplit( const MeasurementSet& msref,
							  unsigned int& npieces )
{
    MSConversionInfo**   retval( 0 );

    retval = new MSConversionInfo*[1];
    
    retval[ 0 ] = new MSConversionInfo( (MS&)msref, 0, 1 );
    
    npieces = 1;
    
    return retval;
}




