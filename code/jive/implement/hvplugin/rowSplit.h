//
//  Function prototype for the row splitting routine
//
//
#ifndef ROWSPLIT_H
#define ROWSPLIT_H

#include <ms/MeasurementSets/MeasurementSet.h>

#include <casa/Exceptions.h>

#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

//
//  split based on #-of-rows
//
MSConversionInfo**  rowSplit( const casa::MeasurementSet& msref,
		                      unsigned int& npieces );

#endif
