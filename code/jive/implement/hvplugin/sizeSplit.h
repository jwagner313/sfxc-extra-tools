//
//  Function prototype for the size splitting routine
//  Splits MS in chunks of max 2GB/piece..
//
// $Id: sizeSplit.h,v 1.2 2006/01/13 11:35:36 verkout Exp $
//
// $Log: sizeSplit.h,v $
// Revision 1.2  2006/01/13 11:35:36  verkout
// HV: CASAfied version of the implement
//
// Revision 1.1  2005/01/28 16:05:43  verkout
// HV: Added 'splitter' which splits the MS in chunks of ~2GB/piece maximum.
//     Use '-size' commandline option of tConvert to trigger using this one,
//     insofar it's not the default already....
//
//
#ifndef SIZESPLIT_H
#define SIZESPLIT_H

#include <ms/MeasurementSets/MeasurementSet.h>

#include <casa/Exceptions.h>

#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

//
//  The sizeSplit routine
//
MSConversionInfo**  sizeSplit( const casa::MeasurementSet& msref,
							   unsigned int& npieces );

#endif
