//
//  Define a global fn...
//

#include <aips/MeasurementSets/MeasurementSet.h>



MS*  splitFunction( const char* file, unsigned int npiece )
{
    MS*   retval( 0 );


    if( !file )
    {
	return retval;
    }

    retval = new MS( file, Table::NewNoReplace );
    
    return retval;
}
