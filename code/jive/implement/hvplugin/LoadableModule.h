//
//  class LoadableModule:
//
//    enable support for dynamically loadable modules (tricky but true and useful!)
//
// Author:  Harro Verkouter, 06-12-1999
//
//
#ifndef LOADABLEMODULE_H
#define LOADABLEMODULE_H


//
//  Define the interface for loadable modules....
//

class LoadableModule
{
public:
    //
    //  This is how we like'm! Give us a filename ans we'll try to
    //  load it!
    //
    //  As we do not like to be interrupted, we do *not* throw
    //  exceptions when the module fails to load, ratjer, if somebody
    //  attempts to get Symbols from us, we return empty symbols. In
    //  this way the usr him/her self can check what's going wrong
    //
    LoadableModule( const char* filename );
    
    //
    //  Quick'n dirty -> get the symbol's value. If unsuccessful,
    //  returns NULL
    //
    void*     getSymbol( const char* symbol );

    //
    //  Delete resources (and unload the stuff)
    //
    ~LoadableModule();

private:
    //
    //  We keep the name of the original file as a private member
    //
    char*      myFileName;
    
    //
    //  The handle returned to us from the os
    //
    void*      myHandle;
    
    //
    //  Copying/assigning to LoadableModules makes no sense: let's
    //  make these inaccessible! As well as a default module -> we
    //  don't buy that!
    //
    LoadableModule();
    LoadableModule( const LoadableModule& );
    LoadableModule& operator=( const LoadableModule& );
};




#endif
