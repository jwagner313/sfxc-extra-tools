//
//  Implementation of the LoadableModule class
//
//
#ifndef LOADABLEMODULE_H
#include <jive/hvplugin/LoadableModule.h>
#endif

#include <iostream>
#include <strings.h>
#include <stdlib.h>
#include <string.h>

//
//  This is SUN-specific. May need #ifdef's 
//
#include <dlfcn.h>

using namespace std;

LoadableModule::LoadableModule( const char* filename ) :
    myFileName( 0 ),
    myHandle( 0 )
{
    //
    //  If a filename was given, attempt to open it!
    //
    if( filename && ::strlen(filename) )
    {
	myFileName = ::strdup( filename );
    }
    
    if( myFileName )
    {
	//
	// Attempt to open (do it *now* and do it *good*)!
	//
	if( (myHandle=::dlopen( myFileName, RTLD_NOW|RTLD_GLOBAL ))==0 )
	{
	    cout << "Huh? " << ::dlerror() << endl;
	}
    }
}


void* LoadableModule::getSymbol( const char* symbol )
{
    return ::dlsym( myHandle, symbol );
}



LoadableModule::~LoadableModule()
{
    //
    //  If a name, free it
    //
    if( myFileName )
    {
	::free( myFileName );
	myFileName = 0;
    }
    
    if( myHandle )
    {
	::dlclose( myHandle );
	myHandle = 0;
    }
}
