//
// A templated class that describes a value
// with constraints.
// Supposedly it can be just used as an 
// ordinary value but assignment to is
// restricted!
//
// the range is currently:
//
// [lower,upper>
// ie including lower, excluding upper
//
//  value >= lower && value < upper
//
//
// Oh yes... throws 'n exception if it
// detects an out-of-range assignment!
//
// $Id: restrictedvalue.h,v 1.2 2004/09/29 07:11:11 verkout Exp $
//
// $Log: restrictedvalue.h,v $
// Revision 1.2  2004/09/29 07:11:11  verkout
// HV: Changed the ostream<<(restricted_value) because it showed the lower/upper bounds of the value as well, which made the output of programs puzzling.
//
// Revision 1.1  2004/08/25 06:11:37  verkout
// HV: Added a new templated type, restrictedvalue. Can specify an upper/lower bound and the objects accepts only values from that range. Assignment of an out-of-range value results in throwing of an exception. The restrictedvalue can be used as a regular numerical value.
//
//
#ifndef TMPLSRC_RESTRICTEDVALUE_H
#define TMPLSRC_RESTRICTEDVALUE_H

#include <iostream>
#include <algorithm>
#include <sstream>


template <typename T>
struct restrictedvalue_t;

template <typename T>
std::ostream& operator<<(std::ostream&, const struct restrictedvalue_t<T>&);

template <typename T>
bool operator==( const restrictedvalue_t<T>& l, const restrictedvalue_t<T>& r);

template <typename T>
bool operator!=( const restrictedvalue_t<T>& l, const restrictedvalue_t<T>& r);

template <typename T>
struct restrictedvalue_t
{
	friend std::ostream& operator<<<T>( std::ostream&, const struct restrictedvalue_t<T>& );
	friend bool operator==<T>( const restrictedvalue_t<T>& l, const restrictedvalue_t<T>& r );
	friend bool operator!=<T>( const restrictedvalue_t<T>& l, const restrictedvalue_t<T>& r );

	public:
		//
		// Default c'tor sets 'sane' to false to indicate that the 
		// values contained can not be trusted to contain 'sane' values
		//
		restrictedvalue_t() :
			sane( false )
		{
		}
		
		restrictedvalue_t( const T& l, const T& u ) :
			lower( l ),
			upper( u ),
			val( T() ),
			sane( true )
		{
			if( lower>upper )
			{
				std::swap( upper, lower );
			}
		}

		restrictedvalue_t( const T& l, const T& u, const T& v ) :
			lower( l ),
			upper( u ),
			val( v ),
			sane( true )
		{
			if( lower>upper )
			{
				std::swap( upper, lower );
			}
		}

		//
		// cast to const T& => can be used as R-value
		//
		operator const T&( void ) const
		{
			if( !sane )
			{
				throw( "restrictedvalue_t::operator const T& on uninitialized object!" );
			}
			return val;
		}

		//
		// assignment from another restrictedvalue
		//
		const restrictedvalue_t<T>& operator=( const restrictedvalue_t<T>& other )
		{
			if( this!=&other )
			{
				lower = other.lower;
				upper = other.upper;
				val   = other.val;
				sane  = other.sane;
			}
			return *this;
		}

		//
		// assignment from a T, such that the object
		// can be used as an L-value
		//
		const restrictedvalue_t<T>& operator=( const T& o )
		{
			if( !sane )
			{
				throw( "restrictedvalue_t::operator=(const T&) on uninitialized object!" );
			}
			if( o<lower || o>=upper )
			{
                                std::ostringstream  e;

                                e << "restrictedvalue:operator=()/"
                                  << "assignment of out-of-range value"
                                  << " ["<< lower << "," << upper 
                                  << "> = " << o << std::ends;
                                throw( e.str().c_str() );
			}
			val = o;
			return *this;
		}

		//
		// RO-access to bounds
		//
		const T& lower_bound( void ) const
		{
			if( !sane )
			{
				throw( "restrictedvalue_t::lower_bound() on uninitialized object!" );
			}
			return lower;
		}
		const T& upper_bound( void ) const
		{
			if( !sane )
			{
				throw( "restrictedvalue_t::upper_bound() on uninitialized object!" );
			}
			return upper;
		}

        bool is_sane( void ) const
        {
            return sane;
        }

		~restrictedvalue_t()
		{
		}
		
	private:
		T    lower, upper, val;
		bool sane;

};

template <typename T>
std::ostream& operator<<( std::ostream& os, const restrictedvalue_t<T>& v )
{
//	os << v.val << " [" << v.lower << ", " << v.upper << ">";
	if( v.sane )
	{
		os << v.val;
	}
	else
	{
		os << "** [NOT sane!]";
	}
	return os;
}

template <typename T>
bool operator==( const restrictedvalue_t<T>& l, const restrictedvalue_t<T>& r )
{
	return (l.sane && r.sane &&
			l.lower==r.lower &&
			l.upper==r.upper &&
			l.val==r.val);
}

template <typename T>
bool operator!=( const restrictedvalue_t<T>& l, const restrictedvalue_t<T>& r )
{
	return !(operator==(l,r));
}

#endif
