//    Author:   Harro Verkouter    12-3-1998
//
//    $Id: MSConversionInfo.cc,v 1.18 2011/10/12 12:45:34 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

#include <jive/Utilities/jexcept.h>
#include <jive/Utilities/sciprint.h>

#include <casa/aips.h>
#include <casa/BasicMath/Math.h>
#include <casa/BasicSL/String.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/ArrayLogical.h>
#include <casa/BasicSL/Constants.h>
#include <casa/OS/Time.h>
#include <casa/Utilities/GenSort.h>

#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <ms/MeasurementSets/MSSpectralWindow.h>
#include <ms/MeasurementSets/MSSpWindowColumns.h>
#include <ms/MeasurementSets/MSObservation.h>
#include <ms/MeasurementSets/MSObsColumns.h>
#include <ms/MeasurementSets/MSDataDescColumns.h>
#include <ms/MeasurementSets/MSPolColumns.h>

#include <measures/Measures.h>
#include <measures/Measures/Stokes.h>

#include <tables/Tables.h>
#include <tables/Tables/TableIter.h>

#ifndef BUILDTIME
#define BUILDTIME "<no buildtime info present>"
#endif

#include <iomanip>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;
using namespace casa;

// Declare and init statistics.
uInt MSConversionInfo::lastVisibility = 0;


Double round2int( Double r ) {
    Double   retval;
    Double   delta;

    if( r<0.0 ) {
        retval = ::ceil( r );
        delta  = -1.0;
    } else {
        retval = ::floor( r );
        delta  = 1.0;
    }
    if( ::fabs(r-retval)>=0.5 )
        retval += delta;
    return retval;
}


// Support classes/structs

msid_type::msid_type( Int sid, Int did, Int pid, Double f, Double channelinc, Double bandwidth ):
    spw( sid ), dd( did ), pol( pid ), freq( f ), chinc( channelinc ), bw( bandwidth )
{}

bool msidtype_compare::operator()(const msid_type& l, const msid_type& r) const {
    return ( l.freq<r.freq ||
             (nearAbs(l.freq, r.freq) && l.spw<r.spw) );
}

ostream& operator<<(ostream& os, const msid_type& mid ) {
    return os << "[FREQ=" << j2ms::sciprint<Double>(mid.freq, "Hz")
              << " is DDId=" << mid.dd << " => SPWId=" << mid.spw << " + POLId=" << mid.pol << "]";
}

freqgroup_type::freqgroup_type():
    aipsfreqgrp( -1 )
{}

freqgroup_type::freqgroup_type( Int afqgrp ):
    aipsfreqgrp( afqgrp )
{}


//  Attempt to create the MS. The MeasurementSet ctor throws an
//  exception if it fails to open
MSConversionInfo::MSConversionInfo(const String& msname) :
    itsROColumnsptr( 0 ),
    itsNumberOfPolarizations( 0 ),
    itsNumberOfChannels( 0 ),
    itsNumberOfIFs( 0 ),
    itsReferenceFrequency( 0.0 ),
    itsTotalBandwidth( 0.0 ),
    itsChannelincrement( 0.0 ),
    myPieceNr( 0 ),
    nrPieces( 1 ),
    myFirstStokes( 0 ),
    myStokesInc( 0 )
{
    // throws if something fishy 
    this->attachMS( MeasurementSet(msname, Table::Old) );
}

MSConversionInfo::MSConversionInfo(const MS& msref) :
    itsROColumnsptr( 0 ),
    itsNumberOfPolarizations( 0 ),
    itsNumberOfChannels( 0 ),
    itsNumberOfIFs( 0 ),
    itsReferenceFrequency( 0.0 ),
    itsTotalBandwidth( 0.0 ),
    itsChannelincrement( 0.0 ),
    myPieceNr( 0 ),
    nrPieces( 1 ),
    myFirstStokes( 0 ),
    myStokesInc( 0 )
{
    this->attachMS(msref);
}

MSConversionInfo::MSConversionInfo(MS& msref, uInt thispiece, uInt maxpiece) :
    itsROColumnsptr( 0 ),
    itsNumberOfPolarizations( 0 ),
    itsNumberOfChannels( 0 ),
    itsNumberOfIFs( 0 ),
    itsReferenceFrequency( 0.0 ),
    itsTotalBandwidth( 0.0 ),
    itsChannelincrement( 0.0 ),
    myPieceNr( thispiece ),
    nrPieces( maxpiece ),
    myFirstStokes( 0 ),
    myStokesInc( 0 )
{
    this->attachMS(msref);
}

// Do not call "attachMS()" but take over analyzation results from other
MSConversionInfo::MSConversionInfo(const MSConversionInfo& other, MS& msref,
								   uInt thispiece, uInt maxpiece) :
	itsMS( msref ),
	itsROColumnsptr( new ROMSColumns(itsMS) ),
	itsNumberOfPolarizations( other.itsNumberOfPolarizations ),
	itsNumberOfChannels( other.itsNumberOfChannels ),
	itsNumberOfIFs( other.itsNumberOfIFs ),
	itsReferenceFrequency( other.itsReferenceFrequency ),
	itsTotalBandwidth( other.itsTotalBandwidth ),
	itsChannelincrement( other.itsChannelincrement ),
	itsObservingDate( other.itsObservingDate ),
	itsDataColumnName( other.itsDataColumnName ),
	myPieceNr( thispiece ),
	nrPieces( maxpiece ),
	myObsCode( other.myObsCode ),
	myFirstStokes( other.myFirstStokes ),
	myStokesInc( other.myStokesInc ),
	myList( other.myList ),
	spwMap( other.spwMap ),
	productMap( other.productMap ),
	scans( other.scans ),
	refTime( other.refTime )
{}


//  The name of the MS
const String& MSConversionInfo::getMSname( void ) const {
    return itsMS.tableName();
}

ROMSColumns& MSConversionInfo::getROColumns( void ) const {
    return *itsROColumnsptr;
}

//  Ok. Return a ref. to the MS contained in this object!
const MeasurementSet& MSConversionInfo::getMS( void ) const {
    return itsMS;
}

//  Read-only access to cached derived quantities
Int MSConversionInfo::getFirstStokes( void ) const {
    return myFirstStokes;
}
Int MSConversionInfo::stokesIncrement( void ) const {
    return myStokesInc;
}


uInt MSConversionInfo::nrPolarizations( void ) const {
    return itsNumberOfPolarizations;
}

uInt MSConversionInfo::nrChannels( void ) const {
    return itsNumberOfChannels;
}

uInt MSConversionInfo::nrIFs( void ) const {
    return itsNumberOfIFs;
}


const MVTime& MSConversionInfo::observingDate( void ) const {
    return itsObservingDate;
}

const String& MSConversionInfo::getObsCode( void ) const {
    return myObsCode;
}

const String MSConversionInfo::getTConvertVersion( void ) const
{
     return string(BUILDTIME);
}

Double MSConversionInfo::getReferenceFrequency( MSConversionInfo::reference r ) const {
	Double   retval = itsReferenceFrequency;

	// Internally, the reference frequency is the band-edge, so if
	// user wants the band-center, give it to him/her
	switch( r ) {
        case MSConversionInfo::CenterChannel:
            retval += this->getChannelwidth()/2.0;
            break;
        case MSConversionInfo::CenterBand:
            retval += itsTotalBandwidth/2.0;
            break;
        case MSConversionInfo::Edge:
        default:
            break;
     }
     return retval;
}

Double MSConversionInfo::getTotalBandwidth( void ) const {
    return itsTotalBandwidth;
}

Double MSConversionInfo::getChannelwidth( void ) const {
    Double  retval( 0.0 );

    if( itsNumberOfChannels )
	    retval = itsTotalBandwidth/itsNumberOfChannels;
    return retval;
}


Double MSConversionInfo::getChannelincrement( void ) const {
    return itsChannelincrement;
}


const String& MSConversionInfo::getDataColumnName( void ) const {
    return itsDataColumnName;
}

uInt MSConversionInfo::itsPieceNr( void ) const {
    return myPieceNr;
}

uInt MSConversionInfo::maxPieceNr( void ) const {
    return nrPieces;
}

uInt MSConversionInfo::getNrOfFreqGroups( void ) const {
    return myList.size();
}


const fqmap_type& MSConversionInfo::getFreqGroups( void ) const {
    return myList;
}

const product_map& MSConversionInfo::getProductMap( void ) const {
    return productMap;
}


//  Delete the object...
MSConversionInfo::~MSConversionInfo() {
    delete itsROColumnsptr;
}



// Attach to a MS
void MSConversionInfo::attachMS( const MS& msref ) {
    // Reset to empty state
    delete itsROColumnsptr;
    myList.clear();
    spwMap.clear();
    productMap.clear();
   
    // Assign the MS to our datamember
    itsMS           = msref;
    itsROColumnsptr = new ROMSColumns( itsMS );

    cout << endl << "Start checking '" << itsMS.tableName() << "' ...." << endl;

    if( !itsMS.nrow() ) {
        cout << " MS Contains no rows" << endl;
        return;
    }

    // For less typing...
    ROMSColumns&   romsc( *itsROColumnsptr );
    
    //  Attempt to fill in some of the more general info that is
    //  expected to be accessed more than once....
    //myObsCode = "tConvert (c)";
	const ROMSObservationColumns&   obscols( romsc.observation() );

	myObsCode = "<unknown>";
	if( obscols.nrow()>0 ) {
		obscols.project().get(0, myObsCode);
		if( obscols.nrow()>1 ) {
			cout << "MSConversionInfo: detected multiple rows in Observation "
			     << "subtable. Using entry found at row 0 for obscode: " << endl
				 << "  OBSCODE=" << myObsCode << endl;
		}
	}

    // Find out which datadescription IDs are used in the main table.
    // They will eventually point out which frequency-groups/setups
    // are actually *in use*. Those frequency groups will be analyzed
    // and it will be verified that they all can be represented in FITS.
    //
    // That is: each freq-group must have the same number of subbands/SpectralWindows/IFs
    // Also: the correlation parameters must be checked: the number of channels AND
    // the number of polarization products computed must match up for
    // FITS representability

    // First things first: which freq-groups are in use
    vector<Int>                 ddids;
    vector<Int>::const_iterator curdd;

    // but do it in a limited scope
    {
        //  get *all* datadescription id's
        Array<Int>    tmpids = romsc.dataDescId().getColumn();
        vector<Int>   allddids;

        tmpids.tovector( allddids );

        // call STL's sort on them
        sort(allddids.begin(), allddids.end());

        // now uniquefy 'm and use the unique_copy
        // such that the unique values end up
        // in the (outer scope) variable 'ddids' :)
        unique_copy( allddids.begin(), allddids.end(),
                     back_insert_iterator<vector<Int> >(ddids) );
    }
    
    // Now retrieve the relevant info for all the DDIds, that is,
    // sorting them into the freq-groups whence they came.
    // NOTE: it is not necessary for a MS to be FITS representable
    // to use all spectral windows in the data that are defined
    // in the freq-group. If > 1 freqgroup is in use in the main table,
    // then, of course, the same *number* of spectral windows must be
    // in use for each used frequency-group.
    // (eg: 2 modes with 4 IFs/SPWs defined, but only one
    //  subband was correlated out of each -> that is 
    //  perfectly representable in FITS)
    for( curdd=ddids.begin(); curdd!=ddids.end(); curdd++ ) {
        // for the current datadescid, get the IDs
        Int                  spwid = romsc.dataDescription().spectralWindowId()( *curdd );
        Int                  fqgrp = romsc.spectralWindow().freqGroup()( spwid );
        fqmap_type::iterator fqptr;

        // see if freq-group already in map, if not, add it 
        if( (fqptr=myList.find(fqgrp))==myList.end() ) {
            pair<fqmap_type::iterator, bool> insres;

            // Note: at this point we do not know what the associated
            // aips-frequency-group-number will be. Leave it undefined but
            // remember to fill it in later....
            insres = myList.insert( make_pair(fqgrp, freqgroup_type()) );
            if( !insres.second )
                THROW_JEXCEPT("Failed to insert entry for freqgroup #" << fqgrp);
            fqptr = insres.first;
        }

        // Insert a definition for the current spectral window into
        // the freqgroup (attempt to)
        Int                               pid   = romsc.dataDescription().polarizationId()( *curdd );
        Double                            f0;
        Double                            finc;
        Double                            bw    = romsc.spectralWindow().totalBandwidth()( spwid );
        Vector<Double>                    freqs = romsc.spectralWindow().chanFreq()( spwid );
        pair<sbset_type::iterator, bool>  insres;
        
        f0   = freqs[0];
        finc = freqs[1] - freqs[0];
        
        insres = fqptr->second.subbands.insert( msid_type(spwid, *curdd, pid, f0, finc, bw) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to insert entry for SPWId " << spwid << " in fqgroup-thingy");
    }

    if( myList.empty() ) {
        // No frequency groups in use? Bizarre...
        cout << "No frequency-groups were in use? [myList == empty]."
             << endl;
        return;
    }
    // There is at least 1 frequency group in use. As freq-groups are only
    // inserted in myList iff there is a spectral window from this group
    // in use in the main table, we may, without thinking about it,
    // address the first subband in the first frequency group. It's guaranteed
    // to exist.
    Int                            nc0;
    Double                         totbw0;
    Double                         chinc0;
    Vector<Int>                    ctype0;
    const freqgroup_type&          reffq( myList.begin()->second );
    const msid_type&               refsb( *reffq.subbands.begin() );
    fqmap_type::const_iterator     curfq;
    sbset_type::const_iterator     cursb;
    const ROMSSpWindowColumns&     spwc( romsc.spectralWindow() );
    const ROMSPolarizationColumns& polc( romsc.polarization() );
    
    // Check #-of-frequency channels
    nc0 = spwc.numChan()( refsb.spw );
    if( nc0<=1 )
        THROW_JEXCEPT("reference spectral window (#" << refsb.spw << ") has not enough channels: "
                      << nc0 << " (need >1)");

    // recorded bandwidth
    totbw0 = spwc.totalBandwidth()( refsb.spw );
    if( nearAbs(totbw0, 0.0) )
        THROW_JEXCEPT("total bandwidth is 0.0? [sais spectral window #" << refsb.spw << "]");
   
    // compute the channel increment in a temp. scope
    {
        Vector<Double>  chfreqs = spwc.chanFreq()( refsb.spw );
        chinc0 = chfreqs[1] - chfreqs[0];
    }
    // And get the correlation types
    ctype0 = polc.corrType()( refsb.pol );

    // If none - there's not a lot to do, is there?
    if( ctype0.nelements()==0 )
        THROW_JEXCEPT("No correlation products?? corrType for POLId=" << refsb.pol << " is empty");

    // Loop over all subbands for all freqgroups and verify that the shape
    // does not vary
    for( curfq=myList.begin(); curfq!=myList.end(); curfq++ ) {
        pair<spwbyfq_type::iterator, bool>  insres;
        
        if( curfq->second.subbands.size()!=reffq.subbands.size() )
            THROW_JEXCEPT("#-of-subbands per frequency-group varies across what is in use. "
                          "Cannot represent that in FITS");
        // append this freqgroup to the spwbyfq_type mapping
        insres = spwMap.insert( make_pair(curfq->first, spwid_list()) );
        if( !insres.second )
            THROW_JEXCEPT("Failed to add new entry into spw-by-freqgroup mapping for freqgroup "
                          << curfq->first);
        // check each subband
        for( cursb=curfq->second.subbands.begin(); cursb!=curfq->second.subbands.end(); cursb++ ) {
            Int              nc;
            Vector<Int>      ct;
            Vector<Double>   cfrq,cfrq2;
            Vector<Double>   chincs;
            
            if( spwc.numChan()(cursb->spw)!=nc0 )
                THROW_JEXCEPT("#-of-frequencychannels varies across spectral windows in use. "
                              "Cannot represent this in FITS");

            // See what hoops one has to jump through to do some
            // simple vector-base stuff... GRRRRRR!!!!
            cfrq  = spwc.chanFreq()(cursb->spw);
            cfrq.shape( nc );
            // yes, you must explicitly resize it otherwise assignment etc
            // will fail [in this particular case it is 
            cfrq2.resize( nc );
            cfrq2 = 0.0;
            // This is more reliable than using Slice() - never
            // can get that one right
            for( int t=0; t<(nc-2); t++ )
                cfrq2[t] = cfrq[t+1];
            // compute the adjacent diffs [as we have shifted one
            // of the operands already, that's just a normal '-' operation]
            chincs = cfrq - cfrq2;
            // shrink the array by one; we should not test
            // the last value...
            chincs.resize( nc-2, True );
            if( !allNearAbs(chincs, chincs[0], 1.0e-7) )
                THROW_JEXCEPT("Channel increments vary across spectral window #" << cursb->spw
                              << ". This is not representable in FITS");
            // Test the correlation types
            ct = polc.corrType()(cursb->pol);
            if( !allEQ(ct, ctype0) )
                THROW_JEXCEPT("Correlation types vary across correlations in use."
                              "Cannot represent this in FITS");

            // append this spw to the associated list of spwid - map
            insres.first->second.push_back( cursb->spw );
        }
    }
    // Everything tested Ok (otherwise we wouldn't end up here; 'n exception
    // would've been thrown)
    // Now we can safely relabel the casa frequency groups to
    // a neato 1..n aipsfreqgroup
    Int                  aipsfq( 1 );
    fqmap_type::iterator fqptr;

    for( fqptr=myList.begin(); fqptr!=myList.end(); fqptr++ ) {
        if( fqptr->first!=aipsfq )
            cout << "WARN: Relabeling MS frequency-group " << fqptr->first << " to ClassicAIPS FQ #" << aipsfq << endl;
        fqptr->second.aipsfreqgrp = aipsfq++;
    }

    //  transfer to private parts :)
    itsNumberOfChannels      = nc0;
    itsNumberOfIFs           = reffq.subbands.size();
    itsNumberOfPolarizations = ctype0.nelements();
    itsChannelincrement      = chinc0;
    itsReferenceFrequency    = refsb.freq - chinc0/2.0;
    itsTotalBandwidth        = totbw0;
    
	// Work out myFirstStokes...
    // (the polarization products) 
    // We verify that they can be FITS-ified.
    // If we detect that we must re-order them, then we must
    // re-order the data as well!!!!

    // First, turn the aips++ enums (the values in the 'ctype0' vector)
    // into their FITS equivalents. Re-use the vector itself for that
	Int          nctype;
	Vector<uInt> idx;

	ctype0.shape( nctype );
    for( int i=0; i<nctype; i++)
		ctype0[i] = Stokes::FITSValue( Stokes::StokesTypes(ctype0[i]) );

    //  OK, get an _index vector_ that sorts these in ascending order if
    //  stokes(0) >= 0, or descending order if < 0.
    //  (in FITS, it is only supported that if the start >= 0, then inc > 0
    //   and if start < 0, then inc < 0, [you should read "should be" in
    //   front of the relational operator for 'inc'])
    if( ctype0[0]>=0 )
		GenSortIndirect<Int>::sort( idx, ctype0 );
    else
		GenSortIndirect<Int>::sort( idx, ctype0, Sort::Descending );

    // Now we need to make sure we can represent the stokes in FITS
    // Technically only need to check if we have > 2 elements.
    // but do it also when we have just 2 elements => we must
    // determine the "stokes increment" when we have >1 polarization product
	myFirstStokes = ctype0[ idx[0] ];
    myStokesInc   = 0;
    if( nctype>1 ) {
		Int  idiff( ctype0[idx[1]]-ctype0[idx[0]] );
	
        for( Int j=2; j<nctype; j++)
		    if( (ctype0[idx[j]] - ctype0[idx[j-1]])!=idiff )
                THROW_JEXCEPT("The STOKES parameters are not"
						      " representable in FITS.");
        myStokesInc = idiff;
    }


    // The original order of the products was just as they were
    // in ctype0. So row #n contained data for correlation product ctype0[ n ]
    // Now we have sorted them into FITS order and we must build up the mapping
    // of where the products would end up in the FITS file.
    // Actually, it just *is* the index vector, really...
    idx.tovector( productMap );

    //  Ok. The odds are that the DATA column is the column
    //  containing the data. We may as well find 'FLOAT_DATA'
    //  and/or 'LAG_DATA', but mainly it will be 'DATA'.
    bool   hasdata( itsMS.isColumn(MS::DATA) );
    bool   hasfloatdata( itsMS.isColumn(MS::FLOAT_DATA) );
    bool   haslagdata( itsMS.isColumn(MS::LAG_DATA) );
    
    //  Introduce some help-booleans
    Bool   apparentSDdata(  ((hasdata &&  hasfloatdata && !haslagdata) || 
                           (hasdata && !hasfloatdata && !haslagdata) ||
                           (!hasdata &&  hasfloatdata && !haslagdata) ) );
    Bool   apparentIFdata( (hasdata && !hasfloatdata && !haslagdata) );
    Bool   apparentRawdata( (!hasdata && !hasfloatdata && haslagdata) );
   
    cout << "MS appears to contain ";
    if( apparentIFdata ) {
        cout << "interferometric data. Using cmplx 'DATA' column";
        itsDataColumnName="DATA";
    } else if( apparentSDdata ) {
        cout << "SingleDish data. ";
        if( !hasdata ) {
            cout << "Using float 'FLOAT_DATA' column.";
            itsDataColumnName="FLOAT_DATA";
        } else {
            cout << "Using cmplx 'DATA' column.";
            itsDataColumnName="DATA";
        }
    } else if( apparentRawdata ) {
        cout << "raw correlator data. Using cmplx 'LAG_DATA' column";
        itsDataColumnName="LAG_DATA";
    } else 
        THROW_JEXCEPT("Sorry. The converter could not determine the type of data found in this MS");

    cout << endl;
    cout << "ConversionInfo deduced:\n"
         << "                    n_ch x n_pol: " << itsNumberOfChannels << " x " << itsNumberOfPolarizations << endl;
    cout << "                       ref. freq: " << j2ms::sciprint<Double>(itsReferenceFrequency, "Hz") << endl
         << "                       bandwidth: " << j2ms::sciprint<Double>(itsTotalBandwidth, "Hz") << endl
         << "                       ch. inc. : " << j2ms::sciprint<Double>(itsChannelincrement, "Hz")
         << "  [NOTE: this is only for the first\n"
         << "                                   IF in the first FQ_GROUP]" << endl;

    //  We need to find out the observing date!
    //const Double  JDofMJD0( 2400000.5 );
	if( romsc.time().isDefined(0) ) {
        Double   mjdinseconds( romsc.time()(0) );
        
        //  MVTime assumes units of MJD to be in days, from MS we get
        //  seconds....
        itsObservingDate = MVTime( mjdinseconds/C::day );
    } else {
        cout <<"Could not deduce Observing date; first cell in TIME column not defined!"
             << endl << "Using today as default." << endl;

	  itsObservingDate = MVTime( Time() );
     }
    
    // Build a list of all scans in this MeasurementSet.
    //
    buildScanSet();

    cout << "Deduced observing date: " << itsObservingDate << endl;
    return;
}


// Return the maximum number of receptors per antenna feed for
// MeasurementSet MS.  If MS does not contain any antenna feeds,
// return -1.
//
Int
MSConversionInfo::getNumReceptors() const
{
  ROMSFeedColumns fc (itsMS.feed());
  Int numReceptors = -1;
  
  for (uInt row = 0; row < fc.nrow(); row++)
    {
      if (numReceptors < fc.numReceptors()(row))
	numReceptors = fc.numReceptors()(row);
    }

  return numReceptors;
}

// Return true if the polarizations for the antenne feed specified by
// ANTENNAID, FEEDID and SPECTRALWINDOWID should be switched.
//
// The reason for switching polarizations is that the FITS-IDI
// specification says that: "If 2 orthogonal polarizations are used,
// it is strongly recommended that feed A shall be `R' or `X' and feed
// B be `L' or `Y'."
//
// This function uses a cache, since doing lookups in the FEED table
// is expensive.
//
Bool
MSConversionInfo::switchPolarizations(Int antennaId, Int feedId,
				      Int spWindowId) const
{
  FeedKey key = { antennaId, feedId, spWindowId };
  MSFeed feed(itsMS.feed());

  if (switchCache.count(key) > 0)
    return switchCache[key];

  switchCache[key] = False;

  MSFeed tmp = feed(feed.col("ANTENNA_ID") == antennaId
		    && feed.col("FEED_ID") == feedId
		    && feed.col("SPECTRAL_WINDOW_ID") == spWindowId);
  if (spWindowId != -1 && tmp.nrow() == 0)
    tmp = feed(feed.col("ANTENNA_ID") == antennaId
	       && feed.col("FEED_ID") == feedId
	       && feed.col("SPECTRAL_WINDOW_ID") == -1);

  ROMSFeedColumns fc(tmp);
  if (fc.nrow() > 0 && fc.numReceptors()(0) > 1)
    {
      AlwaysAssertExit (fc.nrow() == 1);

      // The FITS-IDI specification says that: "The two feeds may
      // either be circular polarized or linearly polarized.  Mixtures
      // of linear and circular polarizations are forbidden."  So we
      // only have to switch for the combinations `L'/`R' and `Y'/`X'.
      // Also switch if the first polarization isn't set.  That
      // probably shouldn't happen, but there are MeasurementSets
      // where only the second polarization is specified.
      //
      Vector<String> pt = fc.polarizationType()(0);
      Bool doSwitch = ((pt(0).compare("L") == 0 && pt(1).compare("R") == 0)
		       || (pt(0).compare("Y") == 0 && pt(1).compare("X") == 0)
		       || pt(0).compare("") == 0);
      switchCache[key] = doSwitch;
    }

  return switchCache[key];
}


// Build an index of scans.
//
void
MSConversionInfo::buildScanSet()
{
  // Iterate over all scans.  Allow for subnetting by including the
  // array ID.
  //
  Block<String> iterKeys(2);
  iterKeys[0] = "SCAN_NUMBER";
  iterKeys[1] = "ARRAY_ID";
  TableIterator iter(itsMS, iterKeys);

  while (!iter.pastEnd())
    {
      MeasurementSet ms = iter.table().sort("TIME");
      ROMSMainColumns msc(ms);
      uInt last = msc.nrow() - 1;

      const Int option = Sort::HeapSort | Sort::NoDuplicates;
      const Sort::Order order = Sort::Ascending;

      {
	// Sanity check.  There should be only a single field for this
	// (sub)array in this scan.
	//
	Vector<Int> fieldIds = msc.fieldId().getColumn();
	uInt n = GenSort<Int>::sort(fieldIds, order, option);
	AlwaysAssertExit(n == 1);
      }

      // ??? Should we also look at the ANTENNA2 column?
      //
      Vector<Int> antennaIds = msc.antenna1().getColumn();
      uInt n = GenSort<Int>::sort(antennaIds, order, option);
      antennaIds.resize(n, True);

      // Fill in the preliminary details for this scan.
      //
      ScanKey scan;
      scan.antennaIds.insert(antennaIds.cbegin(), antennaIds.cend());
      scan.arrayId = msc.arrayId()(0);
      scan.startTime = msc.time()(0) - (msc.interval()(0) / 2);
      scan.endTime = msc.time()(last) + (msc.interval()(last) / 2);
      scan.fieldId = msc.fieldId()(0);
      scans.insert(scans.end(), scan);

      iter++;
    }

  // Where is the telescope pointed at during the gap between two
  // scans?  We cannot be sure, but in some cases we need to pretend
  // that we know.  For example, at the EVN telescopes, the best
  // measurements of the system temperature are made in between scans;
  // these may even be the only measurements available.  For
  // calibration purposes, these measurements are mostly useless if we
  // don't know the direction in which the telescope was pointed.
  // Therefore we extend the scans such that they extend a bit into
  // the gap at both sides of the interval.
  //
  if (!scans.empty())
    {
      ScanSet extendedScans;

      ScanSet::iterator scan = scans.begin();
      ScanKey extendedScan = *scan++;

      /// Make the first scan start a bit earlier.
      extendedScan.startTime -= fuzz;

      while (scan != scans.end())
	{
	  // Calculate the gap and split the difference.
	  Double gap = (*scan).startTime - extendedScan.endTime;
	  Double delta = max(gap / 2, fuzz);

	  extendedScan.endTime += delta;
	  extendedScans.insert(extendedScans.end(), extendedScan);
	  
	  extendedScan = *scan++;
	  extendedScan.startTime -= delta;
	}

      // Make the last scan end a bit later.
      extendedScan.endTime += fuzz;
      extendedScans.insert(extendedScans.end(), extendedScan);

      // Replace the preliminary scans with the extended scans.
      scans.swap(extendedScans);
    }
}

// Return the field ID for the specified antenna and time.
//
Int
MSConversionInfo::getFieldId(Int antennaId, Double time) const
{
  // Since we typically iterate over tables in the MeasurementSet in a
  // time-ordered fashion, it's very likely that the field ID we're
  // looking for is identical to the previous one.  Therefore it's not
  // an insignificant optimization to continue looking at the scan
  // that we looked at in the last invocation instead of always
  // iterating from the first scan.
  //
  static ScanSet::const_iterator scan;
  static ScanSet::const_iterator begin;

  // If we're looking at a different set of scans, iterate from the
  // first scan.
  //
  if (begin != scans.begin())
    scan = begin = scans.begin();

  // If we're at the end of the scans or if we're looking for a time
  // before the current scan, iterate from the first scan.
  //
  if (scan == scans.end() || time < (*scan).startTime)
    scan = begin;

  while (scan != scans.end())
    {
      if ((*scan).startTime <= time && time < (*scan).endTime
	  && (*scan).antennaIds.count(antennaId))
	return (*scan).fieldId;

      scan++;
    }

  return -1;
}

// Return TIME in days relative to the reference date.
//
Double
MSConversionInfo::getIDITime(const MEpoch& time) const
{
  // Only a single EPOCH reference is allowed in the MS.
  AlwaysAssertExit(time.myType() == refTime.myType());

  return time.get("d").getValue() - floor(refTime.get("d").getValue());
}

// Scan fuzziness.  This is the maximum number of seconds used to
// extend scans in order to include calibration data (like system
// temperatures) measured just before or after scans.
//
Double MSConversionInfo::fuzz = 30.0;
