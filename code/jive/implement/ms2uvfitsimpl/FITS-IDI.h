#ifndef FITS_IDI_H
#define FITS_IDI_H

#include <casa/aips.h>
#include <fits/FITS/fits.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>

// Add common FITS-IDI keywords.
//
extern void addCommonKeywords(FITSBinaryTableHDU *tab, casa::Int tabrev,
			      casa::String& obscode, casa::Int no_stkd,
			      casa::Int stk_1, casa::Int no_band,
			      casa::Int no_chan, casa::Float ref_freq,
			      casa::Float chan_bw, casa::Float ref_pixl);

// Add an extra FITS-IDI keyword.
//
extern void addExtraKeyword(FITSBinaryTableHDU *tab, const char *n,
			    casa::Int v);

#endif
