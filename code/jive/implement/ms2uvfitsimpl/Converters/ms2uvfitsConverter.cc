//  Implementation of the ms2uvfitsConverter class
//
//  Harro Verkouter 11-3-1998
//
//     $Id: ms2uvfitsConverter.cc,v 1.14 2007/07/04 07:54:51 jive_cc Exp $
#include <jive/ms2uvfitsimpl/Converters/ms2uvfitsConverter.h>



#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>

#include <jive/ms2uvfitsimpl/Generators/antabGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/maingroupGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/asciitabGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/fqtabGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/tytabGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/gctabGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/sutabGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idihdrGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idisourceGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idifrequencyGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idityGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idiantennaGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idigaincGenerator.h>

#include <jive/ms2uvfitsimpl/Generators/idiFlagGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idiGainCurveGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idiPhaseCalGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/idiSysTempGenerator.h>

#include <jive/ms2uvfitsimpl/Generators/uvtableGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/arraygeomGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

#include <casa/IO/RegularFileIO.h>

#include <jive/Utilities/jexcept.h>
#include <casa/BasicSL/String.h>

// Need this for the default split function
#include <jive/hvplugin/defSplit.h>
#include <tables/Tables/ExprNode.h>

#include <jive/Utilities/streamutil.h>

#include <sstream>


using namespace std;
using namespace casa;


ms2uvfitsConverter::ms2uvfitsConverter() :
    FileFormatConverter(),
    itsFragmentationFn( defSplit )
{}

ms2uvfitsConverter::ms2uvfitsConverter( const MS2UVF::generatorlist_t& hdus,
                                        fragmentfn fptr ):
    FileFormatConverter(),
    hdus2do( hdus ),
    itsFragmentationFn( ((fptr==0)?(defSplit):(fptr)) )
{}

//  This function really converts the MS into a UVFITS file....
bool ms2uvfitsConverter::convert( const String& fitsfilename,
                                  const String& MSfilename ) const {
    bool    retval( false );
    
    if( !itsFragmentationFn ) {
        cout << "Huh? No fragmentation ptr?! Internal state disrupted!" << endl;
        return retval;
    }
    

    //  Now we face the task of converting the named MS to the named
    //  UVFITS-file...
    try {
        cout << "Start conversion attempt.." << endl;

    	//  Ok. Now we're facing the task of checking the MS and
    	//  eventually splitting it up and then process the separate
    	//  parts
    	MS*                 msptr;
        //nrpieces needs to be initialized!
    	uInt                nrpieces=0; 
    	ostringstream       strm;
    	MSConversionInfo**  pieces;

        // open the orig. MS
    	msptr = new MS(MSfilename, Table::Old);
	
        // split it up in pieces
    	pieces = itsFragmentationFn(*msptr, nrpieces);
	
    	//  Reset visibility counter.... brrrr what a SHITTY soln but
    	//  hope it works...
    	MSConversionInfo::lastVisibility = 0;

        // For each piece the splitter created, form all the requested
        // FITS HDU's
    	for( uInt piececnt=0; piececnt<nrpieces; piececnt++ ) {
    	    ByteIO*                         ptr2fitsoutput;
            unsigned int                    gencount;
            generatorlist_t::const_iterator curgen;

    	    //  Form the current fitsfilename
    	    strm.str( string() );
    	    strm << fitsfilename;
    
    	    if( nrpieces>1 )
        		strm << piececnt+1;

    	    //  Ok. First things first: try to create fits-file
    	    ptr2fitsoutput = new RegularFileIO( RegularFile(strm.str()),
                                                ByteIO::NewNoReplace );
    	    
    	    //  If the fits-file could be created ok, we could try to open
    	    //  the MS.
    	    MSConversionInfo&     thispiece( *(pieces[piececnt]) );
    
    
    	    cout << endl;
    	    cout << "--------------------------------" << endl;
    	    cout << "FITS HDU's will be generated...." << endl;
    	    cout << "Local time: " << Time() << endl;
    	    
    	    for( curgen=hdus2do.begin(), gencount=0;
                 curgen!=hdus2do.end();
                 curgen++, gencount++ ) {
        		//  The pointer to the produced HDU(s) (if succesfull)
        		hdulist_t                result;
                unsigned int             hducount;
                hduGenerator*            genptr;
                hdulist_t::iterator      curhdu;

                if( (genptr=Registrar::find_ID(*curgen))==0 ) {
        		    cout << "No generator found for HDU #" << gencount+1
                         << " [" << *curgen << "]" << endl;
        		    continue;
                }

                
        		result = genptr->generate( thispiece );
                if( result.size()==0 ) {
        		    cout << "Generator[" << gencount+1
                         << "] did not generate any HDU's...." << endl;
        		    continue;
        		}

        		//  Loop over the generated HDU's
                //  Note: the iterator *points* at
                //  a pointer... so if we want to
                //  use the HDUObject, we must double
                //  de-reference...
        		for( curhdu=result.begin(), hducount=0;
                     curhdu!=result.end();
                     curhdu++, hducount++ ) {
        		    if( !(*curhdu) ) {
        				cout << j2ms::printeffer("Generator #%2d failed to generate HDU "
								   "#%2d (out of %d)",
                                   gencount+1, hducount+1, result.size())
        					<< endl;
        				continue;
        		    }
        		    //  Write the result to the resulting FITS-output
        		    //  file
            	    (*ptr2fitsoutput) << *(*curhdu);
        		    cout << "Generator[" << gencount << "]"
                         << ": HDU " << hducount << " written" << endl;

                    // immediately delete the HDUObject..
                    delete (*curhdu);
                    // No, do *NOT* erase the entry; that messes up
                    // the loop because the 'curhdu!=result.end()'
                    // fuks up [prolly compiler optimization, which
                    // calls '.end()' only once (seeing there's a 
                    // "const" method for that as well) but as
                    // you erase stuff, ".end()" changes... so the
                    // end condition is never met...
                    // anyway, set the pointer to '0' to indicate
                    // that it was already delete'd
                    (*curhdu) = 0;
        		}
            }
    	    cout << "Ends at " << Time() << endl;
    	    cout << "----------------------------------------" << endl << endl;

    	    //  We don't need those anymore!
    	    delete ptr2fitsoutput;
        }

    	//  Delete all the conversion info objects...
    	for( uInt pcnt=0; pcnt<nrpieces; pcnt++ )
    	    delete pieces[ pcnt ];
    	delete [] pieces;

    	//  Don't need the MS anymore...
    	delete msptr;
	
        //  Prepare returncode...
        retval=true;
    }
    catch( const std::exception& x ) {
        cout << "Darn. Error: " << x.what() << endl;
    }
    
     return retval;
}

//  Configure ourselves such that the conversion is run according to
//  the user's wishes!
Bool ms2uvfitsConverter::setConversion( const MS2UVF::generatorlist_t& hdus ) {
    unsigned int                    gencount;
    generatorlist_t::const_iterator curgen;
    
     cout << endl;
     cout << "Next conversion will produce, in the following order:" << endl;
    
     hdus2do = hdus;
    
    for( curgen=hdus2do.begin(), gencount=0;
         curgen!=hdus2do.end();
         curgen++, gencount++ ) {
		cout << j2ms::printeffer("%3d: ",gencount) << *curgen << endl;
    }
    return True;
}


Bool ms2uvfitsConverter::setFragmenter( fragmentfn fnptr ) {
    if( fnptr ) 
    	itsFragmentationFn = fnptr;
    return (fnptr!=0);
}


//  Destruct the conversion engine...
ms2uvfitsConverter::~ms2uvfitsConverter()
{}

