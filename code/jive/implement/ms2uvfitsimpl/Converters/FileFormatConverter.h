// FileFormatConverter: baseclass for fileformat converters
//
//      Harro Verkouter, 11-3-1998
//
//   $Id: FileFormatConverter.h,v 1.5 2006/11/14 08:44:09 jive_cc Exp $
#ifndef FILEFORMATCONVERTER_H_INC
#define FILEFORMATCONVERTER_H_INC

#include <casa/aips.h>
#include <casa/BasicSL/String.h>

//  Present the interface ->
//   take two strings and return a bool
//   (input/output filename) + return success
class FileFormatConverter 
{
public:
    // default c'tor 
    FileFormatConverter();

    // force implementors to implement this'un
    virtual  bool convert( const casa::String& outputfilename,
                           const casa::String& inputfilename ) const = 0;

    //  The destructor.. release stuff we have allocated (if any)
    virtual ~FileFormatConverter();
    
private:
    //  Prevent copying and assignment
    FileFormatConverter( const FileFormatConverter& );
    const FileFormatConverter& operator=( const FileFormatConverter& );
};


#endif
