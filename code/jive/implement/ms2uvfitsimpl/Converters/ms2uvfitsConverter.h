// convert a MS to UV|IDI FITS
//
//    Harro Verkouter, 11-3-1998
//
//  $Id: ms2uvfitsConverter.h,v 1.7 2007/03/13 09:18:20 jive_cc Exp $
//
#ifndef MS2UVFITSCONVERTER_H_INC
#define MS2UVFITSCONVERTER_H_INC


//  Aips++ includes
#include <casa/aips.h>
#include <casa/Arrays/Vector.h>
#include <ms/MeasurementSets/MeasurementSet.h>

//  Need this for the base-class declaration
#include <jive/ms2uvfitsimpl/Converters/FileFormatConverter.h>


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <casa/Containers/SimOrdMap.h>

#include <iostream>
#include <list>

//  And this one as the type of functionptr we expect for fragmenting a MS
typedef MSConversionInfo** (*fragmentfn)(const casa::MeasurementSet& msref,
										 unsigned int& npieces);



class ms2uvfitsConverter :
    public FileFormatConverter
{

public:
   
    typedef hduGenerator::idtype_t   generator_t;
    
    // Keep a list of things to generate
    typedef std::list<generator_t>   generatorlist_t;

    //  Default ctor: this is the only thing needed.
    ms2uvfitsConverter();

    // construct with specific hdulist_t and optional fragmentfn
    // (built-in default is 'defSplit', used if "fptr==0")
    ms2uvfitsConverter( const generatorlist_t& hdus,
                        fragmentfn fptr=0 );
    
    //  Do convert the MS into a UVFITS-file
    bool  convert( const casa::String& fitsfilename,
                   const casa::String& MSfilename ) const;

    //  Replace the current generators by those listed in the
    //  block. The conversion class will set up the engine in such a
    //  way that when the convert method is run, the (eventually)
    //  produced FITS-file will contain those HDU's U specified in the
    //  'hdus' parameter (and in the same order).
    casa::Bool   setConversion( const generatorlist_t& hdus );

    // replace the fragmentation function by a new one - will
    // only affect the following run(s) of 'convert()'
    casa::Bool   setFragmenter( fragmentfn fnptr );

    //  Destruct the conversionengine
    virtual ~ms2uvfitsConverter();
    
private:
    //  The objects private parts

    // remember list of HDU's to generator for a conversion.
    // can be replaced by call to "setConversion"
    generatorlist_t       hdus2do;
    
    //  The current fragmentation function
    fragmentfn        itsFragmentationFn;
    

    //*****************************************************************
    //              The object's private methods...
    //*****************************************************************
    //  Prevent copying and assignment
    ms2uvfitsConverter( const ms2uvfitsConverter& );
    const ms2uvfitsConverter& operator=( const ms2uvfitsConverter& );
};

//  Introduce this typedef as shorthand...
typedef ms2uvfitsConverter MS2UVF;


#endif
