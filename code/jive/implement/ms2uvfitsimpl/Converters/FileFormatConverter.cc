//
//
//  FileFormatConverter
//
//
//  $Id: FileFormatConverter.cc,v 1.5 2006/02/10 08:53:44 verkout Exp $
//
#ifndef FILEFORMATCONVERTER_H
#include <jive/ms2uvfitsimpl/Converters/FileFormatConverter.h>
#endif

//
//  System includes
//
#include <iostream>
#include <fstream>

//
// Local includes
//
#include <casa/OS/RegularFile.h>
#include <casa/BasicSL/String.h>
#include <jive/Utilities/jexcept.h>


using namespace casa;

//
//  The default constructor: nothing to do
//
FileFormatConverter::FileFormatConverter()
{
}


//
//  The base class convert-method: do not convert; copy!
//
Bool FileFormatConverter::convert( const String& outputfilename, const String& inputfilename ) const
{
    Bool        retval( False );
    RegularFile infile( inputfilename );

    try
    {
        infile.copy( outputfilename, False );
        retval=True;
    }
    catch( const std::exception& x)
    {
        cerr << x.what() << endl;
    }

    return retval;
}


//
//  The desctructor: nothing to do!
//
FileFormatConverter::~FileFormatConverter()
{
}


