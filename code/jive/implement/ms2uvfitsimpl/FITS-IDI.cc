// FITS-IDI helper functions.
//
// Written by: Mark Kettenis
//

#define RCSID(string) \
  static char rcsid[] __attribute__ ((unused)) = string

RCSID("@(#) $Id: FITS-IDI.cc,v 1.2 2007/07/04 07:54:50 jive_cc Exp $");

#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/FITS-IDI.h>

using namespace casa;

// Add common FITS-IDI keywords.
//
void
addCommonKeywords(FITSBinaryTableHDU *tab, Int tabrev, String& obscode,
		  Int no_stkd, Int stk_1, Int no_band, Int no_chan,
		  Float ref_freq, Float chan_bw, Float ref_pixl)
{
  FitsKeywordList kwList;
  FitsKeyword *kw;

  kwList.mk("TABREV", tabrev);
  kwList.mk("OBSCODE", obscode.c_str());
  kwList.mk("NO_STKD", no_stkd);
  kwList.mk("STK_1", stk_1);
  kwList.mk("NO_BAND", no_band);
  kwList.mk("NO_CHAN", no_chan);
  kwList.mk("REF_FREQ", ref_freq);
  kwList.mk("CHAN_BW", chan_bw);
  kwList.mk("REF_PIXL", ref_pixl);
  kwList.first();
  kw = kwList.next();
  while (kw) {
    tab->addExtraKeyword(*kw);
    kw = kwList.next();
  }
}

// Add an extra FITS-IDI keyword.
//
void
addExtraKeyword(FITSBinaryTableHDU *tab, const char *n, Int v)
{
  FitsKeywordList kwList;

  kwList.mk(n, v);
  kwList.first();
  tab->addExtraKeyword (*kwList.next());
}
