// Global function:
//
//           operator<<  defined for Record's
//
// Purpose:
//
//           Show the Record's contents on any outputstream
//
//
// Author:
//
//           Harro Verkouter,    16-04-1998
//
//
//           $Id: DumpRecord.h,v 1.4 2007/03/13 09:18:20 jive_cc Exp $
//
#ifndef DUMPRECORD_H_INC
#define DUMPRECORD_H_INC

#include <iostream>
#include <casa/Containers/RecordInterface.h>


namespace j2ms {
    //  The global function
    std::ostream& operator<<( std::ostream& osref,
	    	                  const casa::RecordInterface& riref );
}


#endif
