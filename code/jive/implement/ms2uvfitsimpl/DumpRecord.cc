//
//  Implementation of the global ostream<< operator for recordinterface-classes
//
//
//  Author:
//
//         Harro Verkouter     16-04-1998
//
//
//     $Id: DumpRecord.cc,v 1.5 2011/10/12 12:45:34 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/DumpRecord.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Arrays/Slicer.h>
#include <casa/Arrays/ArrayIO.h>
#include <iomanip>

using namespace std;
using namespace casa;

//  Make a maximum index (say max. 'maxelements' elements from the
//  IPosition passed as argument).
//  Basically, it makes a "slice" where 'maxelements' are contained,
//  if such a slice can be found.
IPosition makeMaximumIndex( const IPosition arrayshape, Int maxelements ) {
    Int         nelem( 1 );
    Int         nd( arrayshape.nelements() );
    IPosition   retval(nd, 1);
   
    //  Loop until we have enough elements!
    for( Int dim=0; dim<nd && nelem<maxelements; dim++ ) {
        Int   nel_dim( arrayshape[dim] );

        // see if adding this dimension will still fit
        if( (nelem*nel_dim)>maxelements )
            break;
        // yes
        retval[dim]  = nel_dim-1;
        nelem       *= nel_dim;
    }
    return retval;
}


namespace j2ms {

    ostream& operator<<( ostream& osref, const RecordInterface& riref ) {
        Int        nrfields( riref.nfields() );

        //  Limit display of array values to 10 elements
        //  [unused at the moment :(]
        IPosition  currentshape;
    
        //  Ok, show all fields of the record!
        for( Int i=0; i<nrfields; i++ ) {
            //  First, show general info about the field
            osref << setw(2) << i << ": " << riref.name(i) << " [";
        
            //  Make sure that currentshape is set to an empty IPosition
            //  in order to prevent arrayconformance exceptions to be
            //  thrown
            currentshape.resize( 0, False );
        
            //  Do something, depending on type of the record
            //  [hopefully, DoTheRightThing]
            switch( riref.type(i) ) {
                // Scalars
                case TpBool:
                    osref << riref.asBool( i );
                    break;
                case TpUChar:
                    osref << riref.asuChar( i );
                    break;
                case TpShort:
                    osref << riref.asShort( i );
                    break;
                case TpInt:
                    osref << riref.asInt( i );
                    break;
                case TpUInt:
                    osref << riref.asuInt( i );
                    break;
                case TpFloat:
                    osref << riref.asFloat( i );
                    break;
                case TpDouble:
                    osref << riref.asDouble( i );
                    break;
                case TpComplex:
                    osref << riref.asComplex( i );
                    break;
                case TpDComplex:
                    osref << riref.asDComplex( i );
                    break;
                case TpString:
                    osref << riref.asString( i );
                    break;
                //  Now follow the arrays.....
                case TpArrayBool: {
                        const Array<Bool>& tmp( riref.asArrayBool(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayUChar: {
					    const Array<uChar>& tmp( riref.asArrayuChar(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayShort: {
                        const Array<Short>& tmp( riref.asArrayShort(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayInt: {
                        const Array<Int>& tmp( riref.asArrayInt(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayUInt: {
                        const Array<uInt>& tmp( riref.asArrayuInt(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayFloat: {
                        const Array<Float>& tmp( riref.asArrayFloat(i) );		    
                        osref << tmp;
                    }
                    break;
                case TpArrayDouble: {
                        const Array<Double>& tmp( riref.asArrayDouble(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayComplex: {
                        const Array<Complex>& tmp( riref.asArrayComplex(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayDComplex: {
                        const Array<DComplex>& tmp( riref.asArrayDComplex(i) );
                        osref << tmp;
                    }
                    break;
                case TpArrayString: {
                        const Array<String>& tmp( riref.asArrayString(i) );
                        osref << tmp;
                    }
                    break;
                case TpRecord: {
                        const RecordInterface&      tmp( riref.asRecord(i) );
                        // recursively call self :)
                        j2ms::operator<<(osref, tmp) << endl;
                    }
                    break;
                default:
                    osref << "<Unhandled type>";
                    break;
            }
            // processed the field - terminate opening '['
            osref << "] " << endl;
        }
        // done all fields
        return osref;
    }
} // end-of-namespace
