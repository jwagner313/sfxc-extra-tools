// fqtabGenerator.cc
//
// Implementation of the fqtabGenerator class. See fqtabGenerator.h for details.
//
//
// Author:
//
//      Harro Verkouter   12-3-1998
//
// $Id: fqtabGenerator.cc,v 1.8 2007/03/13 09:18:21 jive_cc Exp $

#include <jive/ms2uvfitsimpl/Generators/fqtabGenerator.h>
//
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/ArrayUtil.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/BasicMath/Math.h>

#include <ms/MeasurementSets/MSSpectralWindow.h>


#include <jive/ms2uvfitsimpl/DumpRecord.h>


using namespace std;
using namespace casa;


DEFINE_GENERATOR(fqtabGenerator);

//  This function will do all the work of getting all the necessary
//  frequency information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
hdulist_t fqtabGenerator::generate( const MSConversionInfo& inforef ) const {

    cout << "Making FQ Table" << endl;

    Double                 refFreq( inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    hdulist_t              result;
    IPosition              ifshape( IPosition(1,inforef.nrIFs()) );
    ROMSColumns&           romsc( inforef.getROColumns() );
    FITSBinaryColumn       freqsel( "FREQSEL", "", "1J", "" );
    FITSBinaryColumn       iffreq( "IF FREQ", TpArrayDouble, ifshape, "HZ" );
    FITSBinaryColumn       chwidth( "CH WIDTH", TpArrayFloat, ifshape, "HZ" );
    FITSBinaryColumn       totbw( "TOTAL BANDWIDTH", TpArrayFloat, ifshape, "HZ" );
    FITSBinaryColumn       sidebandcol( "SIDEBAND", TpArrayInt, ifshape );
    MSSpectralWindow       specTable( inforef.getMS().spectralWindow() );
    FITSBinaryTableHDU*    hduptr( new FITSBinaryTableHDU("AIPS FQ", 1) );
    ROArrayColumn<Double>  inchanfreq( specTable, MSSpectralWindow::columnName(MSSpectralWindow::CHAN_FREQ) );
    ROScalarColumn<Double> intotbw( specTable, MSSpectralWindow::columnName(MSSpectralWindow::TOTAL_BANDWIDTH) );
    
    //  Add columns to the binary table object
    hduptr->addColumn( freqsel );
    hduptr->addColumn( iffreq );
    hduptr->addColumn( chwidth );
    hduptr->addColumn( totbw );
    hduptr->addColumn( sidebandcol );


    //  The number of frequency-groups. As of MS vs. 2.0 we can
    //  determine that. 
    hduptr->setNumberRows( inforef.getNrOfFreqGroups() );
    
    
    //  We now can create a record with the correct rowdescription and
    //  create RecordFieldPtrs.
    Record                         datarecord( hduptr->getRowDescription() );
    RecordFieldPtr<Int>            freqselptr( datarecord, "FREQSEL");
    RecordFieldPtr<Array<Int> >    sidebandptr( datarecord, "SIDEBAND");
    RecordFieldPtr<Array<Float> >  ifwidthptr( datarecord, "CH WIDTH");
    RecordFieldPtr<Array<Float> >  totbwptr( datarecord, "TOTAL BANDWIDTH");
    RecordFieldPtr<Array<Double> > iffreqptr( datarecord, "IF FREQ");

    // Arrays of the right shape for what's required
    IPosition                      index( IPosition(1) );
    Array<Int>                     sideband( ifshape );
    Array<Float>                   channelwidth( ifshape );
    Array<Float>                   totalbandwidth( ifshape );
    Array<Double>                  frequencyoffset( ifshape );
    
    // Do write all the frequency groups. In MS vs. 2.0, we should
    // write all the FREQ_GRPs as rows in the table. In MS vs. 1.0, we
    // assume that all rows in the SpectralWindow table make up one
    // frequency-group.
    unsigned int                   i; 
    const fqmap_type&              fqmap( inforef.getFreqGroups() );
    fqmap_type::const_iterator     iter;
    
    for( i=0, iter=fqmap.begin(); iter!=fqmap.end(); i++, iter++ ) {
        const sbset_type&          subbands( iter->second.subbands );
        sbset_type::const_iterator cursb;
        

        //  Fill in the arrays that describe the subbands/IFs in
        //  this frequency group
        for( index[0]=0, cursb=subbands.begin();
             cursb!=subbands.end(); cursb++, index[0]++ ) {
	    
            // Determine the offset w.r.t. the reference frequency
            frequencyoffset( index ) = cursb->freq - refFreq;

            //  Attempt to derive the channel bandwidth?
            channelwidth( index )    = cursb->chinc;

            // Get the NET_SIDEBAND. 0 => lower, 1 => upper
            if( romsc.spectralWindow().netSideband()(cursb->spw)==0 )
	            sideband( index ) = -1;
            else
                sideband( index ) = 1;
            //  Get the totalbandwidth
            totalbandwidth( index ) = cursb->bw;
        }

        // Fill in the datarecord
        *freqselptr   = iter->second.aipsfreqgrp;
        *sidebandptr  = sideband;
        *totbwptr     = totalbandwidth;
        *ifwidthptr   = channelwidth;
        *iffreqptr    = frequencyoffset;

        // Write the current row
        hduptr->putrow(i, datarecord);
    }

    //  add some extra keywords....
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;

    ek.mk("NO_IF", (Int)inforef.nrIFs());

    ek.first();
    ek.next();
    kwptr=ek.curr();
    while( kwptr ) {
        hduptr->addExtraKeyword( *kwptr );
        kwptr=ek.next();
    }

    //  Form return value
    result.push_back( hduptr );
    return result;
}



fqtabGenerator::~fqtabGenerator()
{}

