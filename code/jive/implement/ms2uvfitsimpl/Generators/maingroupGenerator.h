//    When exporting an MS to Classic
//    AIPS via the so called VLBA-Binary table FITS-format (see VLBA
//    Correlator memo #108 by P.J. Diamond, J. Benson etc.) the first
//    entry in the FITS file is a dummy HDU. This 'generator'-object
//    will generate just this HDU!
//
//
//  Author:
//
//            Harro Verkouter
//            05-06--97
//
//      $Id: maingroupGenerator.h,v 1.4 2006/03/02 14:21:43 verkout Exp $
#ifndef MAINGROUPGENERATOR_H_INC
#define MAINGROUPGENERATOR_H_INC


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


class maingroupGenerator :
    public hduGenerator
{
public:

    //  Static method to convert a time into a day and dayfraction!
    static void  timeToDay( casa::Double& day, casa::Double& dayfraction, casa::Double time );

    //  request to actually generate a maingroup!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "maingroup";
    }
    
    //  Delete the generator
    virtual ~maingroupGenerator();
};

// See Registrar.h
DECLARE_GENERATOR(maingroupGenerator);

#endif
