//     From a MSConversionInfo object, extract the info needed to
//     build a IDI aips IDITY (System temperature) binary table extension!
//
//
//      $Id: idityGenerator.h,v 1.1 2007/04/11 12:01:49 jive_cc Exp $
#ifndef IDITYGENERATOR_H_INC
#define IDITYGENERATOR_H_INC


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


//  The idity generator is a specific kind of HDU generator....
class idityGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a idityHDU!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "idity";
    }

    //  Delete the generator
    virtual ~idityGenerator();
};

// See Registrar.h
DECLARE_GENERATOR(idityGenerator);

#endif


