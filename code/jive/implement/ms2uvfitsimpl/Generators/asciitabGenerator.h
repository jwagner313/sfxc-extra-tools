// class:
//
//     asciitabGenerator
//
//
// Purpose:
//
//
//     From a MSConversionInfo object, extract the info needed to
//     build a ASCII TABLE extension!
//
//
//  Author:
//
//            Harro Verkouter
//
//
//      $Id: asciitabGenerator.h,v 1.4 2006/03/02 14:21:42 verkout Exp $
#ifndef ASCIITABGENERATOR_H_INC
#define ASCIITABGENERATOR_H_INC


//
//  Aips++ includes
//


//
// My includes
//
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>


//
//  The asciitab generator is a specific kind of HDU generator....
//
class asciitabGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a asciitabHDU!
    virtual hdulist_t  generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t   getID( void ) const {
        return "asciitab";
    }

    //  Delete the generator
    virtual ~asciitabGenerator();
    
};



#endif
