//  Generate a FITS-IDI header object to be placed as empty primary
//  array HDU in a FITS IDI file (such that classic aips recognizes it
//  as such)
//
//  Author: Harro Verkouter, 16-09-1999
//
//
//  $Id: idihdrGenerator.h,v 1.4 2006/03/02 14:21:43 verkout Exp $
//
//  $Log: idihdrGenerator.h,v $
//  Revision 1.4  2006/03/02 14:21:43  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.3  2006/01/13 11:35:46  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.2  2001/05/30 11:53:45  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//  HV: Imported aips++ implement-stuff
//
//
#ifndef IDIHDRGENERATOR_H
#define IDIHDRGENERATOR_H

//
// My includes
//
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

//  The IDI-Header  generator is a specific kind of HDU generator....
class idihdrGenerator :
    public hduGenerator
{
public:
    //  request to actually generate a idihdr HDU
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "idihdr";
    }

    //  Delete the generator
    virtual ~idihdrGenerator();
};

// See Registrar.h
DECLARE_GENERATOR(idihdrGenerator);

#endif
