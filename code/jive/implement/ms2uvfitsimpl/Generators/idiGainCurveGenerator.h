#ifndef IDIGAINCURVEGENERATOR_H
#define IDIGAINCURVEGENERATOR_H

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

class idiGainCurveGenerator: public virtual hduGenerator
{
 public:

  idiGainCurveGenerator();
  virtual ~idiGainCurveGenerator();

  virtual idtype_t getID(void) const { return "idiGainCurve"; }

  virtual hdulist_t generate(const MSConversionInfo& info) const;

 private:
  idiGainCurveGenerator(const idiGainCurveGenerator&);
  idiGainCurveGenerator& operator=(const idiGainCurveGenerator&);
};

DECLARE_GENERATOR(idiGainCurveGenerator);

#endif // idiGainCurveGenerator.h
