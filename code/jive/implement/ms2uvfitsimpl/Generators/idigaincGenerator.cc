// idigaincGenerator.cc
//
// Implementation of the idigaincGenerator class. See idigaincGenerator.h for details.
//
//
// Author:
//
//      Harro Verkouter   12-3-1998
//
// $Id: idigaincGenerator.cc,v 1.3 2012/07/05 07:49:57 jive_cc Exp $

#include <jive/ms2uvfitsimpl/Generators/idigaincGenerator.h>
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/Vector.h>
#include <casa/Arrays/Matrix.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/BasicSL/Constants.h>
#include <tables/Tables/TableIter.h>
// #include <tables/Tables/ExprNode.h>
#include <tables/Tables/TableParse.h>
#include <casa/Arrays/ArrayMath.h>

#include <ms/MeasurementSets/MSSysCal.h>
#include <ms/MeasurementSets/MSSysCalColumns.h>
#include <ms/MeasurementSets/MSAntennaColumns.h>

using namespace std;
using namespace casa;


DEFINE_GENERATOR(idigaincGenerator);

//  This function will do all the work of getting all the necessary
//  TSYS information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
hdulist_t idigaincGenerator::generate( const MSConversionInfo& inforef ) const {
    cout << "Making GC table" << endl;

    Table               syscal( inforef.getMS().sysCal() );
    Table               sortedcal;
    Record              rowdata;
    hdulist_t           result;
    ROMSColumns         mscol( inforef.getMS() );
    Vector<Float>       tsysval;
    ROMSSysCalColumns   sysCalColumns( inforef.getMS().sysCal() );
    ROMSAntennaColumns  anttab( inforef.getMS().antenna() );
    FITSBinaryTableHDU* idigaincptr( new FITSBinaryTableHDU("GAIN_CURVE", 1) );

    //  Counters etc.
    Int                 nrantbyif;
    Int                 npol;
    Int                 nchan,nstk;
    Double              reftime;
    Double              hourangle;
    IPosition           shp;
    IPosition           shape;
    uInt                nentries;
    const uInt          nrow( sysCalColumns.time().nrow() );
    Vector<Float>       havec;
    TableIterator       tabiter;
    
    if( !nrow ) {
        cout << "No syscal info" << endl;
        return result;
    }

    // Determine if we have to skip the first time.
    // This is needed for WSRT MS's, where the first time in the SYSCAL
    // table is the average at the middle of the observation.
    {
	 Double sttim = sysCalColumns.time()( 0 );
	 for( uInt i=0; i<nrow; i++ ) {
	      Double tim( sysCalColumns.time()(i) );
	      
	      if( tim!=sttim ) {
		   if( tim<sttim ) {
			cout << "Idigainc: First row(s) in SYSCAL table is an average"
			     << " and will be skipped" << endl;
			syscal   = syscal( syscal.nodeRownr()>=Int(i) );
		   }
	      }
	      break;
	 }
    }
    //  NOTE: *really* we should do the mainloop of this fn over the
    //  unique frequency-groups [a set of SPWIDs, linked together by
    //  identical FREQ_GROUP and/or FREQ_GROUP_NAME values in the
    //  SpectralWindow table], then selecting the spectral windows and
    //  group those together and select the data for those and stick
    //  *that* into a single row in the FITS GAIN_CURVE table For now
    //  we do not bother... (now==March 5th 2007, we==HV/DS)
    

    //  Figure out the number of unique (ANTENNA, SPECTRAL_WINDOW_ID)
    //  combinations that are present in the syscal table
    nrantbyif = Table(tableCommand("SELECT DISTINCT ANTENNA_ID, SPECTRAL_WINDOW_ID FROM $1",
				   syscal)).nrow();
    if( !nrantbyif ) {
	 cout << "taql command (SELECT DISTINCT ANTENNA_ID,SPECTRAL_WINDOW_ID)"
	      << " returned '0' (zero),\n"
	      << "indicating no unique records in the syscal table?" << endl;
	 return result;
    } else {
	 cout << "Found " << nrantbyif << " GC table entries" << endl;
    }
    
    //  Infer the number of polarizations from the shape of the
    //  tsys-column [note: this is an optional column but hey! a syscal table
    //  w/o any syscal data is good enough ground to crash, right?]
    npol = (sysCalColumns.tsys().shape(0))( 0 );
    cout << "npol=" << npol << endl;

    // From the data shape deduce number of stokes (number of polarization products)
    // and the number of channels.
    shp     = mscol.data().shape( 0 );
    nstk    = shp( 0 );
    nchan   = shp( 1 );
    
    // Find out the hour-angle at the reference time.
    // Do that by looking at the first non-auto baseline
    // [we have to divide the U-value by the distance between
    // the two stations, so it's best to ignore those where
    // distance==0 ..] 
    reftime = (inforef.observingDate()).second();

    uInt    nrfitsrows( 0 );
    // do this in a separate scope
    {
        uInt           rownr( 0 );
        Double         d( 0.0 );
        Vector<Double> uvw;

        while(rownr<mscol.nrow()) {
	     Vector<Double>  pos1( anttab.position()(mscol.antenna1()(rownr)) );
	     Vector<Double>  pos2( anttab.position()(mscol.antenna2()(rownr)) );
	     
	     Float dx = pos2(0) - pos1(0);
	     Float dy = pos2(1) - pos1(1);
	     Float dz = pos2(2) - pos1(2);
	     // d = pos2(0)-pos1(0);
	     d = sqrt(dx*dx+dy*dy+dz*dz);
	     if( !nearAbs((DComplex)d, 0.0, 1e-7) )
		  break;
	     rownr++;
        }
        if( rownr==mscol.nrow() ) {
	     cout << "Failed to find a cross-baseline "
		  << "[ie one with position(ANT1)[0] - position(ANT2)[1])!=0.0]\n"
		  << "in " << mscol.nrow() << " rows." << endl;
	     return result;
        }
        // get the accompanying UVW value
        mscol.uvw().get( rownr, uvw );
    
        //  We have uvw, now backtrace to hour-angle
	cout << "uvw(0)=" << uvw(0) << endl;
	cout << "d=" << d << endl;
	cout << "rownr=" << rownr << endl;
        hourangle = acos(uvw(0)/d)/C::degree;

        // acos returns a value between 0 and pi.
        // Find the correct quadrant by looking at the sign of v.
        // Note that v=d*sin(ha)*sin(dec), thus sign(v)==sign(sin(ha))
        // (since dec>0 for WSRT).
        if( uvw(1)<0 )
            hourangle = -hourangle;
    }
    // got hourangle for first integration (hopefully... *keeps fingers crossed*)
    cout << "Hour-angle=" << hourangle << endl;
    // We step through the sorted syscal table to find out the
    // 'chunks'.

    {
	 Block<String>       sortnames( 2 );
	 
	 sortnames[0]= "ANTENNA_ID";
	 sortnames[1]= "SPECTRAL_WINDOW_ID";
	 sortedcal = syscal.sort( sortnames );
	 tabiter = TableIterator( sortedcal, sortnames,
				  TableIterator::Ascending, TableIterator::NoSort );
    
	 // Grab the first iteration so we can dimension some of the FITS
	 // columns.  In the SYSCAL the rows are per ANTENNA,
	 // SPECTRAL_WINDOW and TIME.  In FITS they are per ANTENNA,
	 // FREQUENCY_GROUP, all data grouped together in a matrix of
	 // number-of-values * number_of_IFs.  It is the "number-of-values"
	 // that we must figure out [allegedly it is the number-of-rows for
	 // a given primary key.  Actually, the FITS defn. supports a
	 // varying number of records per primary key, as long as the
	 // arrays are pre-dimensioned to the largest value found. We do
	 // not bother with that right now].
	 Table             tablechunk( tabiter.table() );
	 MSSysCal          chunkcal( tablechunk );
	 ROMSSysCalColumns chunkcalcols( chunkcal );
	 // uInt max_tabs = 50;
	 // nentries =  tablechunk.nrow() <= max_tabs ? tablechunk.nrow() : max_tabs;
	 nentries = 2; // max and min Y
	 cout << "nentries = " << nentries << endl;
    	 // 'shape' will be the shape of the Y_VAL_[12] and GAIN_[12] and
	 // columnmatrices, ie "number-of-values" x number-of-IFs
	 shape = IPosition(2, nentries, inforef.nrIFs());
	 cout << "shape = " << shape << endl;
	 // precompute the hour-angle values. They *could* be different for each
	 // primary key but for now we do not care.
	 havec.resize( nentries );
	 for( uInt k=0; k<nentries; k++ ) {
	      havec( k )= hourangle + ((chunkcalcols.time()(k)-reftime)/C::hour * 15.0);
	 }

    }


    //  Count how many rows in the resulting table.....
    nrfitsrows = anttab.nrow();
    cout << nrfitsrows << " antennae" << endl;
      
    IPosition s2(1, inforef.nrIFs() );
    //  Now seems to be a reasonable time to start defining the table!
    idigaincptr->addColumn( FITSBinaryColumn("ANTENNA_NO", TpInt) );
    idigaincptr->addColumn( FITSBinaryColumn("ARRAY", TpInt) );
    idigaincptr->addColumn( FITSBinaryColumn("FREQID", TpInt) );
    // the rest must be dimension nrIFs() 
    idigaincptr->addColumn( FITSBinaryColumn("TYPE_1", TpArrayInt, s2) );
    idigaincptr->addColumn( FITSBinaryColumn("NTERM_1", TpArrayInt, s2) );
    idigaincptr->addColumn( FITSBinaryColumn("X_TYP_1", TpArrayInt, s2) );
    idigaincptr->addColumn( FITSBinaryColumn("Y_TYP_1", TpArrayInt, s2) );
    idigaincptr->addColumn( FITSBinaryColumn("X_VAL_1", TpArrayFloat, s2) );
    idigaincptr->addColumn( FITSBinaryColumn("Y_VAL_1", TpArrayFloat, shape, "DEGREES") );
    idigaincptr->addColumn( FITSBinaryColumn("GAIN_1", TpArrayFloat, shape) );
    idigaincptr->addColumn( FITSBinaryColumn("SENS_1", TpArrayFloat, s2, "K/JY") );

    if( npol==2 ) {
	 idigaincptr->addColumn( FITSBinaryColumn("TYPE_2", TpArrayInt, s2) );
	 idigaincptr->addColumn( FITSBinaryColumn("NTERM_2", TpArrayInt, s2) );
	 idigaincptr->addColumn( FITSBinaryColumn("X_TYP_2", TpArrayInt, s2) );
	 idigaincptr->addColumn( FITSBinaryColumn("Y_TYP_2", TpArrayInt, s2) );
	 idigaincptr->addColumn( FITSBinaryColumn("X_VAL_2", TpArrayFloat, s2) );
	 idigaincptr->addColumn( FITSBinaryColumn("Y_VAL_2", TpArrayFloat, shape, "DEGREES") );
	 idigaincptr->addColumn( FITSBinaryColumn("GAIN_2", TpArrayFloat, shape) );
	 idigaincptr->addColumn( FITSBinaryColumn("SENS_2", TpArrayFloat, s2, "K/JY") );
    }

    //  Restructure the record so it will hold all of the recordfields!
    rowdata.restructure( idigaincptr->getRowDescription() );
    
    // get pointers to the recordfields
    RecordFieldPtr<Int>           antenna( rowdata, "ANTENNA_NO");
    RecordFieldPtr<Int>           arrayId( rowdata, "ARRAY");
    RecordFieldPtr<Int>           spwId( rowdata, "FREQID");
    RecordFieldPtr<Array<Int> >   type1( rowdata, "TYPE_1");
    RecordFieldPtr<Array<Int> >   nterm1( rowdata, "NTERM_1");
    RecordFieldPtr<Array<Int> >   xtype1( rowdata, "X_TYP_1");
    RecordFieldPtr<Array<Int> >   ytype1( rowdata, "Y_TYP_1");
    RecordFieldPtr<Array<Float> > xval1( rowdata, "X_VAL_1");
    RecordFieldPtr<Array<Float> > yval1( rowdata, "Y_VAL_1");
    RecordFieldPtr<Array<Float> > gain1( rowdata, "GAIN_1");
    RecordFieldPtr<Array<Float> > sens1( rowdata, "SENS_1");

    RecordFieldPtr<Array<Int> >   type2; 
    RecordFieldPtr<Array<Int> >   nterm2; 
    RecordFieldPtr<Array<Int> >   xtype2;
    RecordFieldPtr<Array<Int> >   ytype2;
    RecordFieldPtr<Array<Float> > xval2; 
    RecordFieldPtr<Array<Float> > yval2;
    RecordFieldPtr<Array<Float> > gain2;
    RecordFieldPtr<Array<Float> > sens2;

    if( npol==2 ) {
	 type2  = RecordFieldPtr<Array<Int> >( rowdata, "TYPE_2");
	 nterm2 = RecordFieldPtr<Array<Int> >( rowdata, "NTERM_2");
	 xtype2 = RecordFieldPtr<Array<Int> >( rowdata, "X_TYP_2");
	 ytype2 = RecordFieldPtr<Array<Int> >( rowdata, "Y_TYP_2");
	 xval2  = RecordFieldPtr<Array<Float> >( rowdata, "X_VAL_2");
	 yval2  = RecordFieldPtr<Array<Float> >( rowdata, "Y_VAL_2");
	 gain2  = RecordFieldPtr<Array<Float> >( rowdata, "GAIN_2");
	 sens2  = RecordFieldPtr<Array<Float> >( rowdata, "SENS_2");
    }

    
    idigaincptr->setNumberRows( nrfitsrows );
    // Iterate over antennae
    {
	 Block<String>       antnames( 1 );
	 antnames[0]= "ANTENNA_ID";
	 TableIterator antiter = TableIterator(sortedcal, antnames,
					       TableIterator::Ascending, 
					       TableIterator::NoSort);
	 for (uInt row=0; !antiter.pastEnd(); 
	      row++, antiter.next() )
	 {
	      Table             antTableChunk( antiter.table() );
	      MSSysCal          antcal( antTableChunk );
	      ROMSSysCalColumns antCalColumns( antcal );
	      Vector<Float>     vec;
	      Matrix<Float>     gaindata;
	      //  Fill in the rowrecord:
	      *antenna = antCalColumns.antennaId()(0)+1;

	      // we could recalculate hour-angles here.

	      Block<String>     swnames( 1 );
	      swnames[0] = "SPECTRAL_WINDOW_ID";
	      TableIterator switer = TableIterator(antTableChunk, swnames, 
						   TableIterator::Ascending, 
						   TableIterator::NoSort);
	      // iterate over spectral windows
	      for (uInt j=0; !switer.pastEnd(); 
		   j++, switer.next()) {
		   Table swTableChunk( switer.table() );		
		   MSSysCal          swcal( swTableChunk );
		   ROMSSysCalColumns swCalColumns( swcal );
   
		   *spwId   = swCalColumns.spectralWindowId()(0)+1;

		   // cout << "Antenna " << *antenna << " SW: " << *spwId << endl;
		   if (swcal.tableDesc().isColumn("NFRA_GAIN")) {
			ROArrayColumn<Float> nfra_gain;
			nfra_gain.attach(swcal, "NFRA_GAIN");
			nfra_gain.getColumn(gaindata);
		   } else {
			swCalColumns.trx().getColumn( gaindata );
		   }
		   IPosition p2(1, *spwId-1);
		   (*type1)(p2) = 1;               // tabulated values
		   (*nterm1)(p2) = nentries;
		   (*xtype1)(p2) = 0;              // none
		   (*ytype1)(p2) = 3;              // hourangle
		   (*xval1)(p2) = 0;
		   (*sens1)(p2) = 0.1;

		   if( npol==2 ) {
			(*type2)(p2) = 1;               // tabulated values
			(*nterm2)(p2) = nentries;
			(*xtype2)(p2) = 0;
			(*ytype2)(p2) = 3;
			(*xval2)(p2) = 0;
			(*sens2)(p2) = 0.1;
		   }        

		   vec = gaindata.row(0);
//  		   for (uInt i=0; i<nentries; i++) {
//  		     IPosition p( 2, i, *spwId-1 );
//  		     (*gain1)(p) = vec(i);
//  		     (*yval1)(p) = havec(i);
//  		   }
//  		   if (npol==2) {
//  			vec = gaindata.row(1);
//  			for (uInt i=0; i<nentries; i++) {
//  			     IPosition p( 2, i, *spwId-1 );
//  			     (*gain2)(p) = vec(i);
//  			     (*yval2)(p) = havec(i);
//  			}
//  		   }
		   Float min_ha = havec(0);
		   Float max_ha = havec(0);
		   for (Array<Float>::iterator it=havec.begin(); 
			it != havec.end() ; it++) {
			if (*it < min_ha)
			     min_ha = *it;
			if (*it > max_ha)
			     max_ha = *it;
		   }
		   (*gain1)(IPosition( 2, 0, *spwId-1 )) = 1.0;
		   (*yval1)(IPosition( 2, 0, *spwId-1 )) = min_ha;
		   (*gain1)(IPosition( 2, 1, *spwId-1 )) = 1.0;
		   (*yval1)(IPosition( 2, 1, *spwId-1 )) = max_ha;
		   if (npol==2) {
			(*gain2)(IPosition( 2, 0, *spwId-1 )) = 1.0;
			(*yval2)(IPosition( 2, 0, *spwId-1 )) = min_ha;
			(*gain2)(IPosition( 2, 1, *spwId-1 )) = 1.0;
			(*yval2)(IPosition( 2, 1, *spwId-1 )) = max_ha;
		   }
	      }
	      *spwId = 1;
	      idigaincptr->putrow( row, rowdata );        
	 }
    }
    //  Add extra keywords....
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;

    ek.mk( "OBSCODE", "" );
    ek.mk( "NO_POL", npol );
    ek.mk( "NO_STKD", nstk );
    ek.mk( "STK_1", -5 );
    ek.mk( "NO_BAND", (Int)inforef.nrIFs() );
    ek.mk( "NO_CHAN", nchan );
    ek.mk( "REF_FREQ", inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    ek.mk( "CHAN_BW", inforef.getChannelwidth() );
    ek.mk( "REF_PIXL", (Float)1 );
    ek.mk( "NO_TABS", (Int)nentries );
    ek.mk( "TABREV", 1 );
    

    ek.first();
    ek.next();
    kwptr=ek.curr();
    while( kwptr )
    {
        idigaincptr->addExtraKeyword( *kwptr );
        kwptr=ek.next();
    }

    //  Prepare return value...
    result.push_back( idigaincptr );
    return result;
}


idigaincGenerator::~idigaincGenerator()
{
}

