//     From a MSConversionInfo object, extract the info needed to
//     build a Classic aips TYTAB (Sytem temperature) binary table extension!
//
//
//      $Id: tytabGenerator.h,v 1.4 2006/03/02 14:21:44 verkout Exp $
#ifndef TYTABGENERATOR_H_INC
#define TYTABGENERATOR_H_INC


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


//  The tytab generator is a specific kind of HDU generator....
class tytabGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a tytabHDU!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "tytab";
    }

    //  Delete the generator
    virtual ~tytabGenerator();
};

// See Registrar.h
DECLARE_GENERATOR(tytabGenerator);

#endif
