//     From a MSConversionInfo object, extract the info needed to
//     build a Classic aips FQTAB binary table extension!
//
//            Harro Verkouter
//
//
//      $Id: fqtabGenerator.h,v 1.4 2006/03/02 14:21:42 verkout Exp $
#ifndef FQTABGENERATOR_H_INC
#define FQTABGENERATOR_H_INC

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


//  The fqtab generator is a specific kind of HDU generator....
class fqtabGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a fqtabHDU!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "fqtab";
    }

    //  Delete the generator
    virtual ~fqtabGenerator();
    
};

// see Registrar.h
DECLARE_GENERATOR(fqtabGenerator);

#endif
