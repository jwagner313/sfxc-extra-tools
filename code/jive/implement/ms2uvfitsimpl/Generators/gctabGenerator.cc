// gctabGenerator.cc
//
// Implementation of the gctabGenerator class. See gctabGenerator.h for details.
//
//
// Author:
//
//      Harro Verkouter   12-3-1998
//
// $Id: gctabGenerator.cc,v 1.10 2007/05/25 14:45:28 jive_cc Exp $

#include <jive/ms2uvfitsimpl/Generators/gctabGenerator.h>
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>


//  Aips++ includes
#include <casa/Arrays/Vector.h>
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/BasicSL/Constants.h>
#include <casa/BasicMath/Math.h>

#include <tables/Tables/TableIter.h>
#include <tables/Tables/ExprNode.h>
#include <tables/Tables/TableParse.h>

#include <ms/MeasurementSets/MSColumns.h>
#include <ms/MeasurementSets/MSSysCal.h>
#include <ms/MeasurementSets/MSSysCalColumns.h>
#include <ms/MeasurementSets/MSAntennaColumns.h>

using namespace std;
using namespace casa;


DEFINE_GENERATOR(gctabGenerator);

//  This function will do all the work of getting all the necessary
//  TSYS information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
hdulist_t gctabGenerator::generate( const MSConversionInfo& inforef ) const {
    cout << "Making GC table" << endl;

    Table               syscal( inforef.getMS().sysCal() );
    Table               sortedcal;
    Record              rowdata;
    hdulist_t           result;
    ROMSColumns         mscol( inforef.getMS() );
    Vector<Float>       tsysval;
    ROMSSysCalColumns   sysCalColumns( inforef.getMS().sysCal() );
    ROMSAntennaColumns  anttab( inforef.getMS().antenna() );
    FITSBinaryTableHDU* gctabptr( new FITSBinaryTableHDU("AIPS GC", 1) );

    //  Counters etc.
    Int                 nrantbyif;
    Int                 npol;
    Int                 nchan,nstk;
    Int                 nsamplesperantenna;
    Double              reftime;
    Double              hourangle;
    IPosition           shp;
    IPosition           shape;
    uInt                n;
    const uInt          nrow( sysCalColumns.time().nrow() );
    Vector<Float>       havec;
    TableIterator       tabiter;
    
    if( !nrow ) {
        cout << "No syscal info" << endl;
        return result;
    }

    // Determine if we have to skip the first time.
    // This is needed for WSRT MS's, where the first time in the SYSCAL
    // table is the average at the middle of the observation.
    {
	 Double              sttim;

	 sttim    = sysCalColumns.time()( 0 );
	 for( uInt i=0; i<nrow; i++ ) {
	      Double tim( sysCalColumns.time()(i) );
	      
	      if( tim!=sttim ) {
		   if( tim<sttim ) {
			cout << "GCtab: First row(s) in SYSCAL table is an average"
			     << " and will be skipped" << endl;
			syscal   = syscal( syscal.nodeRownr()>=Int(i) );
		   }
	      }
	      break;
	 }
    }
    //  NOTE: *really* we should do the mainloop of this fn over the unique
    //        frequency-groups [a set of SPWIDs, linked together by identical
    //        FREQ_GROUP and/or FREQ_GROUP_NAME values in the SpectralWindow table],
    //        then selecting the spectral windows and group those together and
    //        select the data for those and stick *that* into a single row
    //        in the FITS GAIN_CURVE table
    //        For now we do not bother... (now==March 5th 2007, we==HV/DS)
    
    //  Figure out the number of unique (ANTENNA, SPECTRAL_WINDOW_ID) combinations that are
    //  present in the syscal table
    TaQLResult  rs = tableCommand("SELECT DISTINCT ANTENNA_ID, SPECTRAL_WINDOW_ID FROM $1", syscal);

    // if the result didna give a table, this will throw, according to the docs.
    // [the "rs.table()" will, allegedly]
    nrantbyif = rs.table().nrow();

    if( !nrantbyif ) {
	 cout << "taql command (SELECT DISTINCT ANTENNA_ID,SPECTRAL_WINDOW_ID)"
	      << " returned '0' (zero),\n"
	      << "indicating no unique records in the syscal table?" << endl;
	 return result;
    } else {
	 cout << "Found " << nrantbyif << " GC table entries" << endl;
    }
    
    //  Infer the number of polarizations from the shape of the
    //  tsys-column [note: this is an optional column but hey! a syscal table
    //  w/o any syscal data is good enough ground to crash, right?]
    npol = (sysCalColumns.tsys().shape(0))( 0 );
    

    // From the data shape deduce number of stokes (number of polarization products)
    // and the number of channels.
    shp     = mscol.data().shape( 0 );
    nstk    = shp( 0 );
    nchan   = shp( 1 );
    
    // Find out the hour-angle at the reference time.
    // Do that by looking at the first non-auto baseline
    // [we have to divide the U-value by the distance between
    // the two stations, so it's best to ignore those where
    // distance==0 ..] 
    reftime = (inforef.observingDate()).second();

    uInt    nrfitsrows( 0 );
    // do this in a separate scope
    {
        uInt           rownr( 0 );
        Double         d( 0.0 );
        Vector<Double> dpos;
        Vector<Double> uvw;

        while(rownr<mscol.nrow()) {
            Vector<Double>  pos1( anttab.position()(mscol.antenna1()(rownr)) );
            Vector<Double>  pos2( anttab.position()(mscol.antenna2()(rownr)) );
           
            // compute the length of the difference vector
            d = sqrt( sum( square(pos2 - pos1) ) );
            if( !nearAbs(d, 0.0, 1e-7) )
                break;
            rownr++;
        }
        if( rownr==mscol.nrow() ) {
            cout << "Failed to find a cross-baseline "
		 << "[ie one with position(ANT1)[0] - position(ANT2)[1])!=0.0]\n"
                 << "in " << mscol.nrow() << " rows." << endl;
            return result;
        }
        // get the accompanying UVW value
        mscol.uvw().get( rownr, uvw );
    
        //  We have uvw, now backtrace to hour-angle
        hourangle = acos(uvw(0)/d)/C::degree;

        // acos returns a value between 0 and pi.
        // Find the correct quadrant by looking at the sign of v.
        // Note that v=d*sin(ha)*sin(dec), thus sign(v)==sign(sin(ha))
        // (since dec>0 for WSRT).
        if( uvw(1)<0 )
            hourangle = -hourangle;
    }
    // got hourangle for first integration (hopefully... *keeps fingers crossed*)

    // We step through the sorted syscal table to find out the
    // 'chunks'.

    {
	 Block<String>       sortnames( 2 );
	 
	 sortnames[0]= "ANTENNA_ID";
	 sortnames[1]= "SPECTRAL_WINDOW_ID";
	 sortedcal = syscal.sort( sortnames );
	 tabiter = TableIterator( sortedcal, sortnames,
				  TableIterator::Ascending, TableIterator::NoSort );
    
	 // Grab the first iteration so we can dimension some of the FITS
	 // columns.  In the SYSCAL the rows are per ANTENNA,
	 // SPECTRAL_WINDOW and TIME.  In FITS they are per ANTENNA,
	 // FREQUENCY_GROUP, all data grouped together in a matrix of
	 // number-of-values * number_of_IFs.  It is the "number-of-values"
	 // that we must figure out [allegedly it is the number-of-rows for
	 // a given primary key.  Actually, the FITS defn. supports a
	 // varying number of records per primary key, as long as the
	 // arrays are pre-dimensioned to the largest value found. We do
	 // not bother with that right now].
	 Table             tablechunk( tabiter.table() );
	 MSSysCal          chunkcal( tablechunk );
	 ROMSSysCalColumns chunkcalcols( chunkcal );

	 n =  tablechunk.nrow();
	 cout << "n = " << n << endl;
    	 // 'shape' will be the shape of the Y_VAL_[12] and GAIN_[12] and
	 // columnmatrices, ie "number-of-values" x number-of-IFs
	 shape = IPosition(2, n, inforef.nrIFs());
   
	 // precompute the hour-angle values. They *could* be different for each
	 // primary key but for now we do not care.
	 havec.resize( n );
	 for( uInt k=0; k<n; k++ )
	      havec( k )= hourangle + ((chunkcalcols.time()(k)-reftime)/C::hour * 15.0);


    }


    // FIXME Wrong!
	 //  Count how many rows in the resulting table.....
    nrfitsrows = anttab.nrow();
    cout << nrfitsrows << " antennae" << endl;
      


    //  Now seems to be a reasonable time to start defining the table!
    gctabptr->addColumn( FITSBinaryColumn("ANTENNA_NO", TpInt) );
    gctabptr->addColumn( FITSBinaryColumn("SUBARRAY", TpInt) );
    gctabptr->addColumn( FITSBinaryColumn("FREQ ID", TpInt) );
    gctabptr->addColumn( FITSBinaryColumn("TYPE_1", TpInt) );
    gctabptr->addColumn( FITSBinaryColumn("NTERM_1", TpInt) );
    gctabptr->addColumn( FITSBinaryColumn("X_TYP_1", TpInt) );
    gctabptr->addColumn( FITSBinaryColumn("Y_TYP_1", TpInt) );
    gctabptr->addColumn( FITSBinaryColumn("X_VAL_1", TpFloat) );
    gctabptr->addColumn( FITSBinaryColumn("Y_VAL_1", TpArrayFloat, 
					  shape, "DEGREES") );
    gctabptr->addColumn( FITSBinaryColumn("GAIN_1", TpArrayFloat, 
					  shape) );
    gctabptr->addColumn( FITSBinaryColumn("SENS_1", TpFloat, 
					  IPosition(1,1), "K/JY") );

    if( npol==2 ) {
	 gctabptr->addColumn( FITSBinaryColumn("TYPE_2", TpInt) );
	 gctabptr->addColumn( FITSBinaryColumn("NTERM_2", TpInt) );
	 gctabptr->addColumn( FITSBinaryColumn("X_TYP_2", TpInt) );
	 gctabptr->addColumn( FITSBinaryColumn("Y_TYP_2", TpInt) );
	 gctabptr->addColumn( FITSBinaryColumn("X_VAL_2", TpFloat) );
	 gctabptr->addColumn( FITSBinaryColumn("Y_VAL_2", TpArrayFloat, 
					       shape, "DEGREES") );
	 gctabptr->addColumn( FITSBinaryColumn("GAIN_2", TpArrayFloat, 
					       shape) );
	 gctabptr->addColumn( FITSBinaryColumn("SENS_2", TpFloat, 
					       IPosition(1,1), "K/JY") );
    }

    //  Restructure the record so it will hold all of the recordfields!
    rowdata.restructure( gctabptr->getRowDescription() );
    
    // get pointers to the recordfields
    RecordFieldPtr<Int>           antenna( rowdata, "ANTENNA_NO");
    RecordFieldPtr<Int>           arrayId( rowdata, "SUBARRAY");
    RecordFieldPtr<Int>           spwId( rowdata, "FREQ ID");
    RecordFieldPtr<Int>           type1( rowdata, "TYPE_1");
    RecordFieldPtr<Int>           nterm1( rowdata, "NTERM_1");
    RecordFieldPtr<Int>           xtype1( rowdata, "X_TYP_1");
    RecordFieldPtr<Int>           ytype1( rowdata, "Y_TYP_1");
    RecordFieldPtr<Float>         xval1( rowdata, "X_VAL_1");
    RecordFieldPtr<Array<Float> > yval1( rowdata, "Y_VAL_1");
    RecordFieldPtr<Array<Float> > gain1( rowdata, "GAIN_1");
    RecordFieldPtr<Float>         sens1( rowdata, "SENS_1");
    RecordFieldPtr<Int>           type2; 
    RecordFieldPtr<Int>           nterm2; 
    RecordFieldPtr<Int>           xtype2;
    RecordFieldPtr<Int>           ytype2;
    RecordFieldPtr<Float>         xval2; 
    RecordFieldPtr<Array<Float> > yval2;
    RecordFieldPtr<Array<Float> > gain2;
    RecordFieldPtr<Float>         sens2;



    if( npol==2 ) {
	 type2  = RecordFieldPtr<Int>( rowdata, "TYPE_2");
	 nterm2 = RecordFieldPtr<Int>( rowdata, "NTERM_2");
	 xtype2 = RecordFieldPtr<Int>( rowdata, "X_TYP_2");
	 ytype2 = RecordFieldPtr<Int>( rowdata, "Y_TYP_2");
	 xval2  = RecordFieldPtr<Float>( rowdata, "X_VAL_2");
	 yval2  = RecordFieldPtr<Array<Float> >( rowdata, "Y_VAL_2");
	 gain2  = RecordFieldPtr<Array<Float> >( rowdata, "GAIN_2");
	 sens2  = RecordFieldPtr<Float>( rowdata, "SENS_2");
    }

    
    gctabptr->setNumberRows( nrfitsrows );

    // Iterate over antennae

    {
	 Block<String>       antnames( 1 );
	 antnames[0]= "ANTENNA_ID";
	 TableIterator antiter = TableIterator(sortedcal, antnames,
					       TableIterator::Ascending, 
					       TableIterator::NoSort);
	 nsamplesperantenna = -1;

	 for (uInt row=0; !antiter.pastEnd(); 
	      row++, antiter.next() )
	 {
	      Table             antTableChunk( antiter.table() );
	      MSSysCal          syscal( antTableChunk );
	      Vector<Float>     vec;
	      Matrix<Float>     gaindata;
	      ROMSSysCalColumns sysCalColumns( syscal );
	      //  Fill in the rowrecord:
	      *antenna = sysCalColumns.antennaId()(0)+1;

	      // we could recalculate hour-angles here.

	      Block<String>     swnames( 1 );
	      swnames[0] = "SPECTRAL_WINDOW_ID";
	      TableIterator switer = TableIterator(antTableChunk, swnames, 
						   TableIterator::Ascending, 
						   TableIterator::NoSort);

	      // iterate over spectral windows
	      for (uInt j=0; !switer.pastEnd(); 
		   j++, switer.next()) {
		   Table swTableChunk( switer.table() );		
		   MSSysCal          syscal( swTableChunk );
		   ROMSSysCalColumns sysCalColumns( syscal );
   
		   *spwId   = sysCalColumns.spectralWindowId()(0)+1;

		   cout << "Antenna " << *antenna << " SW: " << *spwId << endl;
		   if (syscal.tableDesc().isColumn("NFRA_GAIN")) {
			ROArrayColumn<Float> nfra_gain;
			nfra_gain.attach(syscal, "NFRA_GAIN");
			nfra_gain.getColumn(gaindata);
		   } else {
			sysCalColumns.trx().getColumn( gaindata );
		   }

		   //cout << "Still here (5)" << endl;
		   //cout << vec.shape() << p1 << p2 << endl;
		   //cout << ((*gain1)(p1, p2)).shape() << endl;

		   vec = gaindata.row(0);
		   for (uInt i=0; i<n; i++) {
		     IPosition p( 2, i, *spwId-1 );
		     (*gain1)(p) = vec(i);
		     (*yval1)(p) = havec(i);
		   }
		     
		   if( npol==2 ) {
			*type2 = 1;               // tabulated values
			*nterm2 = sysCalColumns.antennaId().nrow();
			*xtype2 = 0;
			*ytype2 = 3;
			*xval2 = 0;
			
			vec = gaindata.row(1);
			for (uInt i=0; i<n; i++) {
			  IPosition p( 2, i, *spwId-1 );
			  (*gain2)(p) = vec(i);
			  (*yval2)(p) = havec(i);
			}
			*sens2 = 1;
		   }        
	      }

	      *type1 = 1;               // tabulated values
	      *nterm1 = sysCalColumns.antennaId().nrow();
	      *xtype1 = 0;              // none
	      *ytype1 = 3;              // hourangle
	      *xval1 = 0;

	      // Get the number of samples per antenna, so we can fill in
	      // the extra keyword 'NO_TABS' later on
	      if( nsamplesperantenna<0 )
	      {
		   nsamplesperantenna = *nterm1;
	      }
	
	      *sens1 = 1;

	      gctabptr->putrow( row, rowdata );        
	 }
    }
    //  Add extra keywords....
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;

    ek.mk( "OBSCODE", "" );
    ek.mk( "NO_POL", npol );
    ek.mk( "NO_STKD", nstk );
    ek.mk( "STK_1", -5 );
    ek.mk( "NO_BAND", 1 );
    ek.mk( "NO_CHAN", nchan );
    ek.mk( "REF_FREQ", inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    ek.mk( "CHAN_BW", inforef.getChannelwidth() );
    ek.mk( "REF_PIXL", (Float)1 );
    ek.mk( "NO_TABS", nsamplesperantenna );
    ek.mk( "TABREV", 2 );
    

    ek.first();
    ek.next();
    kwptr=ek.curr();
    while( kwptr )
    {
        gctabptr->addExtraKeyword( *kwptr );
        kwptr=ek.next();
    }


    //  Prepare return value...
    result.push_back( gctabptr );
    return result;
}


gctabGenerator::~gctabGenerator()
{
}

