//     From a MSConversionInfo object, extract the info needed to
//     build a Classic aips IDIGAINC binary table extension!
//
//      $Id: idigaincGenerator.h,v 1.1 2007/04/11 12:01:48 jive_cc Exp $
#ifndef IDIGAINCGENERATOR_H_INC
#define IDIGAINCGENERATOR_H_INC


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


//  The idigainc generator is a specific kind of HDU generator....
class idigaincGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a idigaincHDU!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "idigainc";
    }

    //  Delete the generator
    virtual ~idigaincGenerator();

};

// See Registrar.h
DECLARE_GENERATOR(idigaincGenerator);

#endif
