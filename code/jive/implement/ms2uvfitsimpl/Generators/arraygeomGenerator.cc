//
//  Really generate an ARRAY_GEOMETERY binary table extension!
//
//  Author: Harro Verkouter,  20-09-1999
//
//  $Id: arraygeomGenerator.cc,v 1.13 2007/03/13 09:18:21 jive_cc Exp $
//
//  $Log: arraygeomGenerator.cc,v $
//  Revision 1.13  2007/03/13 09:18:21  jive_cc
//  HV: - cosmetic changes
//      - Reworked internals of MSConversionInfo
//        * uses less pointers
//        * less contstraints: there can be
//          different bandwidth/IF and hence
//          different channelwidth/IF.
//          Only nr-of-IFs/freqgroup and
//          nr-of-channels/IF need to be identical
//        * Caches the re-ordering of polarization products
//          as they may have to be re-ordered in order to
//          follow the FITS-ordering
//        * Spectralwindow re-ordering and mapping
//          now done via neato STL auto-sorting
//          containers. Resulting in more readable and
//          more reliable code.
//
//  Revision 1.12  2007/01/17 08:22:14  jive_cc
//  HV: Casa changed Fits.h into FITS.h
//
//  Revision 1.11  2006/03/02 14:21:42  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.10  2006/02/17 12:41:27  verkout
//  HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//
//  Revision 1.9  2006/02/10 08:53:46  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.8  2006/01/13 11:35:45  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.7  2004/08/25 06:04:35  verkout
//  HV: Fiddled with templates. They all get instantiated automatically. As a result, the source code for the templates must be visible compiletime
//
//  Revision 1.6  2004/01/05 15:28:21  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.5  2003/09/12 07:37:32  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.4  2003/02/14 15:48:01  verkout
//  HV: * trial/FITS/FITSUtil was removed.
//      * std::string does not have automatic conversion to
//        (const char*). Fits-stuff does need (const char*)
//        so had to add the odd .c_str() here and there.
//
//  Revision 1.3  2002/08/06 06:54:07  verkout
//  HV: Changed STAXOF column type from 1 int to 3 doubles...
//
//  Revision 1.2  2001/05/30 11:53:15  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//  HV: Imported aips++ implement-stuff
//
//
#include <jive/ms2uvfitsimpl/Generators/arraygeomGenerator.h>
//
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/ArrayMath.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <tables/Tables/ExprNode.h>
#include <fits/FITS.h>
#include <fits/FITS/FITSDateUtil.h>
#include <jive/Utilities/jexcept.h>
#include <ms/MeasurementSets/MSFeed.h>

#include <ms/MeasurementSets/MSAntenna.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <casa/Quanta/Euler.h>
#include <measures/Measures/MeasTable.h>

#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/Utilities/streamutil.h>

#include <sstream>

using namespace casa;
using namespace std;

DEFINE_GENERATOR(arraygeomGenerator);

//
//  This function will do all the work of getting all the necessary
//  antenna information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
//
hdulist_t arraygeomGenerator::generate( const MSConversionInfo& inforef ) const
{
    cout << "Making ARRAY_GEOMETRY table" << endl;
	
    //
    //  First, make a list off all the stuff that we may need!
    //
    Int                    nrantennas;
    String                 rdate;
    MSFeed                 feedTable( inforef.getMS().feed() );
    hdulist_t              result;
    MSAntenna              antennaTable( inforef.getMS().antenna() );
    FITSBinaryColumn       anname( "ANNAME", "The antennaname", "8A", "" );
    FITSBinaryColumn       stabxyz( "STABXYZ", "", "3D", "METERS" );
    FITSBinaryColumn       derxyz( "DERXYZ", "", "3E", "METER/S" );
    FITSBinaryColumn       orbparm( "ORBPARM", "Orbital parameters", "0D", "" );
    FITSBinaryColumn       nosta( "NOSTA", "Antenna number", "1J", "" );
    FITSBinaryColumn       mntsta( "MNTSTA", "Mount", "1J", "" );
    FITSBinaryColumn       staxof( "STAXOF", "x-offset", "3E", "METERS" );

    //
    //  All the MS columns that we need
    //
	ROMSColumns                        mscol( inforef.getMS() );
    const ROScalarMeasColumn<MEpoch>&  timemeascol( mscol.timeMeas() );
	const ROScalarQuantColumn<Double>& timequantcol( mscol.timeQuant() ); 

    //
    //  Create a new Binary Table object
    //
    FITSBinaryTableHDU*    hduptr( new FITSBinaryTableHDU("ARRAY_GEOMETRY", 1) );

    //
    // Retrieve important info
    //
    MVTime    actualobstime;
    String    timesys;
    
    try
    {
		actualobstime = inforef.observingDate();
		FITSDateUtil::toFITS( rdate, timesys, inforef.observingDate().day() );
    }
    catch( const std::exception& x )
    {
		cerr << x.what() << endl;
	
		actualobstime = MVTime( Time() );
		FITSDateUtil::toFITS( rdate, timesys, (Double)MVTime(Time()) );
		cout << "DATE-OBS can't be represented in FITS, default to "
			 << rdate << endl;
    }
    
	
    ostringstream   tmpstrm;
    
    tmpstrm << j2ms::printeffer("%04d-%02d-%02d",
			                    actualobstime.year(),
						        actualobstime.month(),
		  				        actualobstime.monthday());
    rdate = tmpstrm.str();
    
    //
    //  Configure the binary table
    //
    hduptr->addColumn( anname );
    hduptr->addColumn( stabxyz );
    hduptr->addColumn( derxyz );
    hduptr->addColumn( orbparm );
    hduptr->addColumn( nosta );
    hduptr->addColumn( mntsta );
    hduptr->addColumn( staxof );
    
    //
    // Now we create column objects that refence into the SELECTED table
    //
    ROScalarColumn<String> antname( antennaTable, MSAntenna::columnName(MSAntenna::NAME) );
    ROScalarColumn<String> antmount( antennaTable, MSAntenna::columnName(MSAntenna::MOUNT) );
    ROArrayColumn<Double>  antposition( antennaTable, MSAntenna::columnName(MSAntenna::POSITION) );
    ROArrayColumn<Double>  antoffset( antennaTable, MSAntenna::columnName(MSAntenna::OFFSET) );
    ROArrayColumn<String>  poltype( feedTable, MSFeed::columnName(MSFeed::POLARIZATION_TYPE) );
    ROScalarColumn<Int>    antid( feedTable, MSFeed::columnName(MSFeed::ANTENNA_ID) );
    
    //
    //  The number of rows
    //
    nrantennas = antennaTable.nrow();
    
    hduptr->setNumberRows( nrantennas );
    
    //
    //  Restructure the record
    //
    Record               tablerow( hduptr->getRowDescription() );
    
    //
    //  Now we can have recordfieldptrs!
    //
    RecordFieldPtr<String>          nameptr( tablerow, "ANNAME" );
    RecordFieldPtr<Array<Double> >  stabptr( tablerow, "STABXYZ" );
    RecordFieldPtr<Array<Float> >   derptr( tablerow, "DERXYZ" );
    RecordFieldPtr<Int>             nostaptr( tablerow, "NOSTA" );
    RecordFieldPtr<Int>             mntstaptr( tablerow, "MNTSTA" );
    RecordFieldPtr<Array<Float> >   staxofptr( tablerow, "STAXOF" );

    //
    //  Fill in recordfields that do not change across rows (if any)
    //
    *derptr = (Float)0.0;
        
    //
    //  Get the columns....
    //
    Int              tmpint;
    String           tmpstring;
	//
	// tmp array, used where the converted station offsets go
	// (we have to convert from Double->Float)
	//
	Array<Float>     tmpar( IPosition(1,3) );
    
    for( uInt i=0; i<(uInt)nrantennas; i++ )
    {
		//
		//  Name
		//
		*nameptr="";
            
		*nameptr  = upcase( antname(i) );
	
		//
		// Antenna position
		//
		*stabptr  = antposition(i);
				
		//
		//  Number ( in FITS, antenna nrs are 1-relative)
		//
		*nostaptr = i+1;
            
		//
		// Translate the MOUNT-string to Classic AIPS MNTSTA-number
		//
		tmpstring = downcase( antmount(i) );
	
		if( tmpstring.contains("alt-az") )
		{
			tmpint = 0;
		}
		else if( tmpstring.contains("equatorial") )
		{
			tmpint = 1;
		}
		else if( tmpstring.contains("orbiting") )
		{
			tmpint = 2;
		}
		else
		{
			tmpint = 3;
		}
            
		//
		//  Do put the value
		//
		*mntstaptr = tmpint;
            
		//
		//  The station X-offset
		//
		//*staxofptr = (Int)( antoffset(i)(IPosition(1,0)) );
		convertArray( tmpar, antoffset(i) );
		*staxofptr = tmpar;
            
		//
		//  Do write the row!
		//
		hduptr->putrow( i, tablerow );
    }
    
    //
    //  add some extra keywords....
    //
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;
    
    //
    //  Before we add them, let's calculate them!
    //
    //  Calculate GSTIA0, DEGPDY, UT1UTC, and IATUTC.
    //
    String          timeUnit = MEpoch::showType( timemeascol(0).type() );
    Quantum<Double> stime( timequantcol(0) );
    MEpoch          utctime(stime, MEpoch::UTC );
    MEpoch          iattime = MEpoch::Convert(utctime, MEpoch::IAT)();
    MEpoch          ut1time = MEpoch::Convert(utctime, MEpoch::UT1)();
    Double          utcsec  = utctime.get("s").getValue();
    Double          ut1sec  = ut1time.get("s").getValue();
    Double          iatsec  = iattime.get("s").getValue();
	
    //
    //  Use the beginning of the IAT day to calculate the GMST.
    //
    Double          utcday = floor(utctime.get("d").getValue());
    Double          iatday = floor(iattime.get("d").getValue());
    Double          gstday, gstday1;
    {
		//
		//  Our timesys==UTC, hence GSTIA0 *really* should be GSTUTC0....
		//
		Quantum<Double>   itime(iatday, "d");
		MEpoch            ia0time(itime, MEpoch::IAT);
		MEpoch            gsttime = MEpoch::Convert(ia0time, MEpoch::GMST)();
	
		gstday = gsttime.get("d").getValue();
    }

    Double           gstdeg = 360 * (gstday - floor(gstday));
    {
	//
	// #degrees/IATday is the difference between this and the next day.
	//
	Quantum<Double>   itime(iatday+1, "d");
	MEpoch            ia0time(itime, MEpoch::IAT);
	MEpoch            gsttime = MEpoch::Convert(ia0time, MEpoch::GMST)();
	
	gstday1 = gsttime.get("d").getValue();
    }

    Double degpdy = 360 * (gstday1 - gstday);
    
    //
    //  PolarMotion gives -x and -y.
    //  Need to be multiplied by earth radius to get them in meters.
    //
    const Euler&          polarMotion = MeasTable::polarMotion( utcday );
    
    //
    //  The common keywords
    //
    ek.mk( "TABREV", (Int)1 );
    ek.mk( "OBSCODE", inforef.getObsCode().c_str() );
    ek.mk( "NO_STKD", (Int)inforef.nrPolarizations() );
    ek.mk( "STK_1", inforef.getFirstStokes() );
    ek.mk( "NO_BAND", (Int)inforef.nrIFs() );
    ek.mk( "NO_CHAN", (Int)inforef.nrChannels() );
    ek.mk( "REF_FREQ", inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    ek.mk( "CHAN_BW", inforef.getChannelwidth() );
    ek.mk( "REF_PIXL", (Float)1 );
    ek.mk( "NUMORB", 0 );

    //
    //  Phew.. having done that, we can now safely fill them in...
    //
    ek.mk( "FRAME",  "GEOCENTRIC" );
    ek.mk( "ARRAYX", 0.0 );
    ek.mk( "ARRAYY", 0.0 );
    ek.mk( "ARRAYZ", 0.0 );
    ek.mk( "GSTIA0", gstdeg );
    ek.mk( "DEGPDY", degpdy );
    ek.mk( "FREQ",   inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    ek.mk( "RDATE",  rdate.c_str() );
    ek.mk( "POLARX", -polarMotion(0)*6356752.31 );
    ek.mk( "POLARY", -polarMotion(1)*6356752.31 );
    ek.mk( "UT1UTC", ut1sec-utcsec );
    ek.mk( "IATUTC", ::round2int(iatsec-utcsec) );
    ek.mk( "TIMSYS", "UTC" );
    ek.mk( "ARRNAM", "VLBA" );  // JanW: was EVN

    
    //
    //  Transfer them to the HDU
    //
    ek.first();
    ek.next();
    
    kwptr = ek.curr();
    while( kwptr )
    {
    	hduptr->addExtraKeyword( *kwptr );
    	kwptr = ek.next();
    }
    
    //  And add the HDUObject to the return value vector
    result.push_back( hduptr );
    return result;
}



arraygeomGenerator::~arraygeomGenerator()
{
}


