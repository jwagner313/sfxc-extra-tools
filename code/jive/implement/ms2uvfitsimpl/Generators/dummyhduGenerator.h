// class:
//
//     dummyhduGenerator
//
//
// Purpose:
//
//
//    The purpose of this class is: when exporting an MS to Classic
//    AIPS via the so called VLBA-Binary table FITS-format (see VLBA
//    Correlator memo #108 by P.J. Diamond, J. Benson etc.) the first
//    entry in the FITS file is a dummy HDU. This 'generator'-object
//    will generate just this HDU!
//
//
//  Author:
//
//            Harro Verkouter
//            16-3-97
//
//      $Id: dummyhduGenerator.h,v 1.4 2006/03/02 14:21:42 verkout Exp $
#ifndef DUMMYHDUGENERATOR_H_INC
#define DUMMYHDUGENERATOR_H_INC


//
//  Aips++ includes
//


//
// My includes
//
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>

//
//  The antab generator is a specific kind of HDU generator....
//
class dummyhduGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a dummyHDU!
    virtual hdulist_t  generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t   getID( void ) const {
        return "dummyhdu";
    }
    
    //  Delete the generator
    virtual ~dummyhduGenerator();
    
};



#endif
