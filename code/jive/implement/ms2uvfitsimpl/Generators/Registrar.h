// Registrar - keeps a mapping of ID -> actual generator. Also features helper 
// object for easy (autmagically) registering/deregistering
//
// $Id: Registrar.h,v 1.4 2011/10/12 12:45:35 jive_cc Exp $
//
// $Log: Registrar.h,v $
// Revision 1.4  2011/10/12 12:45:35  jive_cc
// HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//       changed into 'labels' for more genericity
//     * can now be built either as part of AIPS++/CASA [using makefile]
//       or can be built as standalone set of libs + binaries and linking
//       against casacore [using Makefile]
//       check 'code/
//
// Revision 1.3  2007-05-25 11:16:29  jive_cc
// HV: - took out debug statements
//     - redone layout to be more readable
//         (i hope)
//
// Revision 1.2  2007/04/11 12:01:48  jive_cc
// [small] Update of tConvert to include -wm2i flag, which allows conversion
// of Westerbork data (including gain-curve and system temperatures) to
// IDI FITS file format.
//
// Revision 1.1  2006/03/02 14:23:30  verkout
// HV: * Added support for automagically building a mapping of ID to actual hduGenerator; this class does the accounting and has defines to ease the class-writers task
//
//
#ifndef MS2UVFITSIMPL_GENERATORS_REGISTRAR_H
#define MS2UVFITSIMPL_GENERATORS_REGISTRAR_H

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <map>
#include <iostream>
#include <typeinfo>

class Registrar
{
    public:
        Registrar();

        // (attempt) to find the generator identified by id 'id'
        static hduGenerator*   find_ID( const hduGenerator::idtype_t& id );

        // note: the pointer is NOT taken over, the one registering
        // the generator is also responsible for deleting the storage
        static bool            registerGenerator( hduGenerator* gen );
      
        // remove this'un from the set of recognized generators
        static void            deregisterGenerator( const hduGenerator::idtype_t& id );
    private:
        //  This is the map that maps from id to pointer to generator!
        //  We follow the actual type as defined in the hduGenerator
        //  baseclass
        typedef std::map<hduGenerator::idtype_t, hduGenerator*>  generatormap_t;
        static generatormap_t*  idToGeneratorMap;
};

// trigger init of the thing
static Registrar _blurpie_thing_blah_gobbledegook;

// This is a templated helper object which, if
// you declare a static instance of it, in your source
// file, it will automagically register/unregister
// your type.
// The template parameter is your type (your
// specific hduGenerator)
template <typename _Gen>
class GeneratorRegistrar {
    public:
        GeneratorRegistrar() {
            if( (__cnt++)==0 &&
                !Registrar::registerGenerator((__ptr=new _Gen())) ) {
                std::cerr << "GeneratorRegistrar for " 
                          << typeid(_Gen()).name()
                          << " [id=" << __ptr->getID() << "] failed"
                          << std::endl;
                delete __ptr;
                throw std::exception();
            }
        } 
        ~GeneratorRegistrar() {
            if( (--__cnt)==0 ) {
                Registrar::deregisterGenerator(__ptr->getID());
                delete __ptr;
                __ptr = 0;
            }
        }

    private:
        static int             __cnt;
        static hduGenerator*   __ptr;
};


// These macros insert the code necessary to
// trigger automagic registration of your generator


// put *this* in your .h file
// (where 'Gen' is the name of your
//  class)
#define DECLARE_GENERATOR(Gen) \
    static GeneratorRegistrar<Gen>  __blurb__##Gen;
    
// put *this* in your .cc file
#define DEFINE_GENERATOR(Gen) \
    template <> int GeneratorRegistrar<Gen>::__cnt = 0;\
    template <> hduGenerator* GeneratorRegistrar<Gen>::__ptr = 0;


#endif
