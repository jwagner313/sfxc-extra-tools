//  IDI-Source Generator -> generates a binary table extension,
//  conforming to the definition of the SOURCE table, as defined in
//  AIPS Memo #102, The FITS Interferometry Data Interchange format by
//  Chris Flatters (http://www.nrao.edu/~cflatter)
//
//  Author:  Harro Verkouter,  23-09-1999
//
//  $Id: idisourceGenerator.h,v 1.4 2006/03/02 14:21:43 verkout Exp $
//
//  $Log: idisourceGenerator.h,v $
//  Revision 1.4  2006/03/02 14:21:43  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.3  2006/01/13 11:35:47  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.2  2001/05/30 11:53:48  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//  HV: Imported aips++ implement-stuff
//
//
#ifndef IDISOURCEGENERATOR_H
#define IDISOURCEGENERATOR_H


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


//  The idisource generator is a specific kind of HDU generator....
class idisourceGenerator :
    public hduGenerator
{
public:
    //  request to actually generate a idisourceHDU!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "idisource";
    }
    
    //  Delete the generator
    virtual ~idisourceGenerator();
};

// See Registrar.h
DECLARE_GENERATOR(idisourceGenerator);

#endif
