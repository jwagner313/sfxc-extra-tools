// class:
//
//     hduGenerator
//
//
// purpose:
//
//
//     To generate HDU's (+ data!) from a MS, which can then be
//     written to a fitsfile! This class merely serves as an abstract
//     base-class: the specific kind of hdu generated depends on the
//     type of hduGenerator used. As an example, a antabGenerator will
//     be created. This Generator takes a MS and retrieves from it the
//     info it needs to create a valid Classic aips ANTAB binary table
//     extension HDU!
//
//
// Author:      Harro Verkouter  12-3-1998
//
//   $Id: hduGenerator.h,v 1.4 2006/03/02 14:21:43 verkout Exp $
#ifndef HDUGENERATOR_H_INC
#define HDUGENERATOR_H_INC

#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <list>
#include <string>

//  I think we can get away with these as forward declarations: we're
//  not going to use them other than by reference!
class MSConversionInfo;


// This defines what this thang produces!
typedef std::list<HDUObject*>   hdulist_t; 


// the class (read: interface) itself
class hduGenerator
{
public:

    // this is (supposedly...) the type of the variable
    // whose value is used to identify a specific Generator.
    // Class implementors should return a unique ID for
    // their specific implementation.
    typedef std::string idtype_t;
    
    // Generate the HDU(s)
    //
    //  Upon succesful generation, returns non-empty hdulist. Otherwise
    //  hdulist with no elements is returned! 
    //
    //  Note: 
    //        +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //        The caller is responsible for deleting the HDU objects
    //        in the list!!!!!!
    //
    //        +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    virtual hdulist_t  generate( const MSConversionInfo& inforef ) const = 0;


    // Return the 'id' for your concrete generator
    virtual idtype_t   getID( void ) const = 0;

    // need virtual d'tor since we really must be derived from!
    virtual ~hduGenerator() {}
    
};


#endif
