//     From a MSConversionInfo object, extract the info needed to
//     build a Classic aips SUTAB binary table extension!
//
//
//            Harro Verkouter, 10-8-1998
//
//
//      $Id: sutabGenerator.h,v 1.4 2006/03/02 14:21:43 verkout Exp $
#ifndef SUTABGENERATOR_H_INC
#define SUTABGENERATOR_H_INC

#include <casa/aips.h>
#include <tables/Tables/TableColumn.h>

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>



//  The sutab generator is a specific kind of HDU generator....
class sutabGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a sutabHDU!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "sutab";
    }
    
    //  Delete the generator
    virtual ~sutabGenerator();
};

// See Registrar.h
DECLARE_GENERATOR(sutabGenerator);



//
//  Define a function to retrieve the Epoch from a column
//
//  If the keyword cannot be found, 0.0 is returned. Otherwise it
//  attempts to return 1950.0 or 2000.0
//
casa::Double  getEpoch( const casa::ROTableColumn& column );


#endif
