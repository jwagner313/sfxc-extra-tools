// Generate a FITS_IDI PHASE-CAL table.
//
// Written by: Mark Kettenis
//

#define RCSID(string) \
  static char rcsid[] __attribute__ ((unused)) = string

RCSID("@(#) $Id: idiPhaseCalGenerator.cc,v 1.2 2007/07/04 07:54:52 jive_cc Exp $");

// AIPS++ includes.
//
#include <casa/aips.h>
#include <casa/Arrays/Array.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/math.h>
#include <casa/Utilities/Assert.h>
#include <casa/Utilities/Sort.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <tables/Tables/ExprNode.h>
#include <tables/Tables/TableIter.h>

// JIVE includes.
//
#include <jive/ms2uvfitsimpl/FITS-IDI.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

#include <jive/ms2uvfitsimpl/Generators/idiPhaseCalGenerator.h>

using namespace std;
using namespace casa;

// Implementation of class idiPhaseCalGenerator.
//

DEFINE_GENERATOR(idiPhaseCalGenerator);

idiPhaseCalGenerator::idiPhaseCalGenerator() : hduGenerator()
{
}

hdulist_t
idiPhaseCalGenerator::generate(const MSConversionInfo& info) const
{
  hdulist_t result;
  const MeasurementSet& ms = info.getMS();
  ROMSSpWindowColumns spwc(ms.spectralWindow());
  String obscode = info.getObsCode();
  Int no_stkd = info.nrPolarizations();
  Int stk1 = info.getFirstStokes();
  Int no_band = info.nrIFs();
  Int no_chan = info.nrChannels();
  Float ref_freq = info.getReferenceFrequency(MSConversionInfo::CenterChannel);
  Float chan_bw = info.getChannelwidth();
  Float ref_pixl = 1.0;
  Int no_pol = info.getNumReceptors();
  Table pc;
  const fqmap_type& groups = info.getFreqGroups();

  if (!ms.keywordSet().isDefined("JIVE_PHASE_CAL"))
    return result;
  pc = ms.keywordSet().asTable("JIVE_PHASE_CAL");

  // Skip if the JIVE_PHASE_CAL table is missing.
  //
  if(pc.isNull())
    return result;

  // XXX
  Int no_tones = ROArrayColumn<Double>(pc, "PC_FREQ").shape(0)(0);

  IPosition stateShape(2, 4, no_band);
  IPosition toneShape(2, no_tones, no_band);

  // Create table.
  //
  FITSBinaryTableHDU *pcTab =
    new FITSBinaryTableHDU("PHASE-CAL", 1); // ??? Version?

  {
    // Create column descriptions.
    //
    FITSBinaryColumn time("TIME", "Time covered by row", "1D", "DAYS");
    FITSBinaryColumn time_interval("TIME_INTERVAL", "Duration of interval",
				   "1E", "DAYS");
    FITSBinaryColumn source_id("SOURCE_ID", "Source ID", "1J", "");
    FITSBinaryColumn antenna_no("ANTENNA_NO", "Antenna number", "1J", "");
    FITSBinaryColumn array ("ARRAY", "Array number", "1J", "");
    FITSBinaryColumn freqid("FREQID", "Frequency setup number", "1J", "");
    FITSBinaryColumn cable_cal("CABLE_CAL", "Cable calibration measurement",
			       "1D", "SECONDS");
    FITSBinaryColumn state_1("STATE_1", TpArrayFloat, stateShape,
			     "PERCENT", "State counts for polarization 1");
    FITSBinaryColumn pc_freq_1("PC_FREQ_1", TpArrayDouble, toneShape, "HZ",
			       "Phase cal tone frequencys for polarization 1");
    FITSBinaryColumn pc_real_1("PC_REAL_1", TpArrayFloat, toneShape, "",
			       "Re of phase-cal measurements for pol. 1");
    FITSBinaryColumn pc_imag_1("PC_IMAG_1", TpArrayFloat, toneShape, "",
			       "Im of phase-cal measurements for pol. 1");
    FITSBinaryColumn pc_rate_1("PC_RATE_1", TpArrayFloat, toneShape, "SEC/SEC",
			       "Phase-cal rates for polarization 1");

    // Add columns.
    //
    pcTab->addColumn(time);
    pcTab->addColumn(time_interval);
    pcTab->addColumn(source_id);
    pcTab->addColumn(antenna_no);
    pcTab->addColumn(array);
    pcTab->addColumn(freqid);
    pcTab->addColumn(cable_cal);
    pcTab->addColumn(state_1);
    pcTab->addColumn(pc_freq_1);
    pcTab->addColumn(pc_real_1);
    pcTab->addColumn(pc_imag_1);
    pcTab->addColumn(pc_rate_1);

    // Create and add optional columns.
    //
    if (no_pol > 1)
      {
	if (no_pol > 2)
	  cerr << "Warning: FITS-IDI has no support for " << no_pol
	       << " polarizations." << endl
	       << "The number of polarizations will be limited to 2."
	       << endl;

	// Create optional column descriptions.
	//
	FITSBinaryColumn state_2("STATE_2", TpArrayFloat, stateShape,
				 "PERCENT", "State counts for polarization 2");
	FITSBinaryColumn pc_freq_2("PC_FREQ_2", TpArrayDouble, toneShape, "HZ",
				   "Phase cal tone frequencies"
				   " for polarization 2");
	FITSBinaryColumn pc_real_2("PC_REAL_2", TpArrayFloat, toneShape, "",
				   "Re of phase-cal measurements for pol. 2");
	FITSBinaryColumn pc_imag_2("PC_IMAG_2", TpArrayFloat, toneShape, "",
				   "Im of phase-cal measurements for pol. 2");
	FITSBinaryColumn pc_rate_2("PC_RATE_2", TpArrayFloat, toneShape,
				   "SEC/SEC",
				   "Phase-cal rates for polarization 2");

	// Add optional columns.
	//
	pcTab->addColumn(state_2);
	pcTab->addColumn(pc_freq_2);
	pcTab->addColumn(pc_real_2);
	pcTab->addColumn(pc_imag_2);
	pcTab->addColumn(pc_rate_2);
      }

    // Add keywords.
    //
    addCommonKeywords(pcTab, 2, obscode, no_stkd, stk1, no_band, no_chan,
		      ref_freq, chan_bw, ref_pixl);
    addExtraKeyword(pcTab, "NO_POL", no_pol);
    addExtraKeyword(pcTab, "NO_TONES", no_tones);
  }

  Record data(pcTab->getRowDescription());
  RecordFieldPtr<Double> time(data, "TIME");
  RecordFieldPtr<Float> time_interval(data, "TIME_INTERVAL");
  RecordFieldPtr<Int> source_id(data, "SOURCE_ID");
  RecordFieldPtr<Int> antenna_no(data, "ANTENNA_NO");
  RecordFieldPtr<Int> array(data, "ARRAY");
  RecordFieldPtr<Int> freqid(data, "FREQID");
  RecordFieldPtr<Double> cable_cal(data, "CABLE_CAL");
  RecordFieldPtr<Array<Float> > state_1(data, "STATE_1");
  RecordFieldPtr<Array<Double> > pc_freq_1(data, "PC_FREQ_1");
  RecordFieldPtr<Array<Float> > pc_real_1(data, "PC_REAL_1");
  RecordFieldPtr<Array<Float> > pc_imag_1(data, "PC_IMAG_1");
  RecordFieldPtr<Array<Float> > pc_rate_1(data, "PC_RATE_1");
  RecordFieldPtr<Array<Float> > state_2;
  RecordFieldPtr<Array<Double> > pc_freq_2;
  RecordFieldPtr<Array<Float> > pc_real_2;
  RecordFieldPtr<Array<Float> > pc_imag_2;
  RecordFieldPtr<Array<Float> > pc_rate_2;
  if (no_pol > 1)
    {
      state_2.attachToRecord(data, "STATE_2");
      pc_freq_2.attachToRecord(data, "PC_FREQ_2");
      pc_real_2.attachToRecord(data, "PC_REAL_2");
      pc_imag_2.attachToRecord(data, "PC_IMAG_2");
      pc_rate_2.attachToRecord(data, "PC_RATE_2");
    }

  // Iterate over all antennas, feeds and time intervals that we have
  // phase cal data for.
  //
  Block<String> iterKeys(3);
  iterKeys[0] = "TIME";
  iterKeys[1] = "ANTENNA_ID";
  iterKeys[2] = "FEED_ID";
  TableIterator pcIter(pc, iterKeys);

  // Sigh.  Because we have to determine the number of rows for the
  // generated PHASE-CAL table before we can write the rows, we have
  // to iterate twice.
  //
  // Refer for comments to the second iteration.
  //
  uInt nrow = 0;
  while(!pcIter.pastEnd())
    {
      const Table& pc = pcIter.table();

      fqmap_type::const_iterator curfq = groups.begin();
      while (curfq != groups.end())
	{
	  Bool skip = True;

	  uInt band = 0;
	  const sbset_type &subbands = curfq->second.subbands;
	  sbset_type::const_iterator cursb = subbands.begin();
	  while (cursb != subbands.end())
	    {
	      AlwaysAssertExit((Int)band < no_band);

	      Int id = cursb->spw;
	      Table pcMatch = pc(pc.col("SPECTRAL_WINDOW_ID") == id);

	      if (pcMatch.nrow() == 0)
		pcMatch = pc(pc.col("SPECTRAL_WINDOW_ID") == -1);

	      if (pcMatch.nrow() > 0)
		skip = False;

	      band++;
	      cursb++;
	    }

	  if (!skip)
	    nrow++;
	  curfq++;
	}

      pcIter.next();
    }
  pcIter.reset();

  pcTab->setNumberRows(nrow);

  cout << "Writing FITS-IDI PHASE-CAL (PH) table." << endl;

  for (uInt row = 0; !pcIter.pastEnd(); pcIter.next())
    {
      const Table& pc = pcIter.table();
      ROScalarColumn<Double> timec (pc, "TIME");
      ROScalarMeasColumn<MEpoch> timeMeasc (pc, "TIME");
      ROScalarColumn<Int> antennac (pc, "ANTENNA_ID");
      ROScalarColumn<Int> feedc (pc, "FEED_ID");

      Int fieldId = info.getFieldId(antennac(0), timec(0));
      if (fieldId == -1)
	continue;

      *time = info.getIDITime(timeMeasc(0));
      *time_interval = 0.0;
      *source_id = fieldId + 1;
      *antenna_no = antennac(0) + 1;
      *array = -1;		// XXX
      *cable_cal = floatNaN();	// PCCOR doesn't like this...
      *cable_cal = 0.0;		// ...so pretend it's zero.

      // Iterate over all fequency groups/setups and bands.
      //
      // NOTE: We assume that the list is properly sorted.  All bands
      // within a frequency setup should be grouped together, and
      // should be in the right order.
      //
      fqmap_type::const_iterator curfq = groups.begin();
      while (curfq != groups.end())
	{
	  // Assume we have no phase cal tones for any band in this
	  // frequency group/setup.
	  //
	  Bool skip = True;

	  *freqid = curfq->second.aipsfreqgrp;

	  *state_1 = Array<Float>(stateShape, floatNaN());
	  *pc_freq_1 = Array<Double>(toneShape, doubleNaN());
	  *pc_real_1 = Array<Float>(toneShape, floatNaN());
	  *pc_imag_1 = Array<Float>(toneShape, floatNaN());
	  *pc_rate_1 = Array<Float>(toneShape, floatNaN());
	  if (no_pol > 1)
	    {
	      *state_2 = Array<Float>(stateShape, floatNaN());
	      *pc_freq_2 = Array<Double>(toneShape, doubleNaN());
	      *pc_real_2 = Array<Float>(toneShape, floatNaN());
	      *pc_imag_2 = Array<Float>(toneShape, floatNaN());
	      *pc_rate_2 = Array<Float>(toneShape, floatNaN());
	    }

	  uInt band = 0;
	  const sbset_type &subbands = curfq->second.subbands;
	  sbset_type::const_iterator cursb = subbands.begin();
	  while (cursb != subbands.end())
	    {
	      AlwaysAssertExit((Int)band < no_band);

	      Int id = cursb->spw;
	      Table pcMatch = pc(pc.col("SPECTRAL_WINDOW_ID") == id);

	      // In its description of the FEED table, the
	      // MeasurementSet definition (version 2.0) says that: "A
	      // value of -1 indicates that a row is valid for all
	      // spectral windows."  This phrase isn't repeated in the
	      // description of the SYSCAL table, but it doesn't hurt
	      // to deal with it here.  So if we didn't find the
	      // spectral band we're looking for, see if there is a
	      // row with SPECTRAL_WINDOW_ID set to -1, and use the
	      // data in that row.
	      //
	      if (pcMatch.nrow() == 0)
		pcMatch = pc(pc.col("SPECTRAL_WINDOW_ID") == -1);

	      if (pcMatch.nrow() > 0)
		{
		  if (pcMatch.nrow() > 1)
		    cerr << "Warning: Multiple phase cals found "
			 << "for the same antenna feed and time."
			 << endl
			 << "Only the first phase cal be used."
			 << endl
			 << "ANTENNA_ID: " << antennac(0) << endl
			 << "FEED_ID: " << feedc(0) << endl
			 << "SPECTRAL_WINDOW_ID: " << id << endl
			 << "TIME: " << timeMeasc(0) << endl;

		  ROScalarColumn<Int> spwindowc(pcMatch, "SPECTRAL_WINDOW_ID");
		  ROArrayColumn<Double> freqc(pcMatch, "PC_FREQ");
		  ROArrayColumn<Complex> datac(pcMatch, "PC_DATA");

		  // Switch polarizations if necessary.
		  //
		  uInt id1 = 0, id2 = 1;
		  uInt antennaId = antennac(0);
		  uInt feedId = feedc(0);
		  uInt spWindowId = spwindowc(0);
		  Int sb = spwc.netSideband()(spWindowId);
		  if (info.switchPolarizations (antennaId, feedId, spWindowId))
		    id1 = 1, id2 = 0;

		  // Sort tones if necessary.  PCCOR really doesn't
		  // like it if the frequencies associated with tones
		  // are not in ascending order.
		  //
		  Double freqArr[no_tones];
		  for (Int tone = 0; tone < no_tones; tone++)
		    freqArr[tone] = freqc(0)(IPosition(1, tone));

		  Sort sort;
		  sort.sortKey(freqArr, TpDouble);
		  Vector<uInt> idx;
		  sort.sort(idx, no_tones);

		  for (Int tone = 0; tone < no_tones; tone++)
		    {
		      Double freq = freqArr[idx[tone]];
		      (*pc_freq_1)(IPosition(2, tone, band)) = freq;
		      Complex data = datac(0)(IPosition(2, id1, idx[tone]));
		      if (sb == 0)
			data = conj(data);
		      (*pc_real_1)(IPosition(2, tone, band)) = real(data);
		      (*pc_imag_1)(IPosition(2, tone, band)) = imag(data);

		      if (no_pol > 1)
			{
			  (*pc_freq_2)(IPosition(2, tone, band)) = freq;
			  data = datac(0)(IPosition(2, id2, idx[tone]));
			  if (sb == 0)
			    data = conj(data);
			  (*pc_real_2)(IPosition(2, tone, band)) = real(data);
			  (*pc_imag_2)(IPosition(2, tone, band)) = imag(data);
			}
		    }

		  skip = False; // We found some data.
		}

	      band++;
	      cursb++;
	    }

	  // Skip this frequency group/setup if we didn't find any
	  // phase cal data.
	  //
	  if (!skip)
	    {
	      pcTab->putrow(row, data);
	      row++;
	    }
	  curfq++;
	}
    }

  // Store return value.
  //
  result.push_back(pcTab);
  return result;
}

idiPhaseCalGenerator::~idiPhaseCalGenerator()
{
}
