//
//  Really do generate an idiantenna HDU...
//
//  Author:  Harro Verkouter,  22-09-1999
//
//
//  $Id: idiantennaGenerator.cc,v 1.11 2008/03/19 12:47:28 jive_cc Exp $
//
//  $Log: idiantennaGenerator.cc,v $
//  Revision 1.11  2008/03/19 12:47:28  jive_cc
//  kettenis: Correctly set POLTYB in the ANTENNA table for dual pol experiments.
//
//  Revision 1.10  2007/01/17 08:22:14  jive_cc
//  HV: Casa changed Fits.h into FITS.h
//
//  Revision 1.9  2006/03/02 14:21:43  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.8  2006/02/10 08:53:46  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.7  2006/01/13 11:35:46  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.6  2004/08/25 06:04:35  verkout
//  HV: Fiddled with templates. They all get instantiated automatically. As a result, the source code for the templates must be visible compiletime
//
//  Revision 1.5  2004/01/05 15:28:35  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.4  2003/02/14 15:48:02  verkout
//  HV: * trial/FITS/FITSUtil was removed.
//      * std::string does not have automatic conversion to
//        (const char*). Fits-stuff does need (const char*)
//        so had to add the odd .c_str() here and there.
//
//  Revision 1.3  2002/08/06 06:57:43  verkout
//  HV: * Needed to specify timerange for which the antennainfo was valid
//  * Improved polarization labelling handling
//
//  Revision 1.2  2001/05/30 11:53:36  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//  HV: Imported aips++ implement-stuff
//
//
#include <jive/ms2uvfitsimpl/Generators/idiantennaGenerator.h>
//
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/ArrayMath.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/Utilities/Assert.h>
#include <tables/Tables/ExprNode.h>
#include <fits/FITS.h>
#include <fits/FITS/FITSDateUtil.h>


#include <measures/Measures/Stokes.h>
#include <ms/MeasurementSets/MSFeed.h>
#include <ms/MeasurementSets/MSAntenna.h>
#include <ms/MeasurementSets/MSFeedColumns.h>
#include <ms/MeasurementSets/MSAntennaColumns.h>

#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

using namespace std;
using namespace casa;


DEFINE_GENERATOR(idiantennaGenerator);

//  This function will do all the work of getting all the necessary
//  antenna information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
hdulist_t idiantennaGenerator::generate( const MSConversionInfo& inforef ) const
{
    cout << "Making ANTENNA table" << endl;
    //
    //  First, make a list off all the stuff that we may need!
    //
    uInt                   nopcal( 0 );
    uInt                   nRows;
    String                 rdate;
    MSFeed                 feedTable(inforef.getMS().feed() );
    hdulist_t              result;
    MSAntenna              antennaTable( inforef.getMS().antenna() );
    FITSBinaryColumn       time( "TIME", "Midpoint of period covered", "1D", "DAYS" );
    FITSBinaryColumn       interval( "TIME_INTERVAL", "Duration of period covered", "1D", "DAYS" );
    FITSBinaryColumn       anname( "ANNAME", "The antennaname", "8A", "" );
    FITSBinaryColumn       anno( "ANTENNA_NO", "The antenna nr", "1J", "" );
    FITSBinaryColumn       array( "ARRAY", "Arraynumber", "1J", "" );
    FITSBinaryColumn       freqid( "FREQID", "Frequency ID", "1J", "" );
    FITSBinaryColumn       diglev( "NO_LEVELS", "Nr of digitizer levels", "1J", "" );
    FITSBinaryColumn       poltya( "POLTYA", "Polarization type", "1A", "" );
    FITSBinaryColumn       polaa( "POLAA", TpArrayFloat, IPosition(1, inforef.nrIFs()), "DEGREES" );
    FITSBinaryColumn       poltyb( "POLTYB", "Polarization type", "1A", "" );
    FITSBinaryColumn       polab( "POLAB", TpArrayFloat, IPosition(1, inforef.nrIFs()), "DEGREES" );
    FITSBinaryColumn       polcalb( "POLCALB", TpArrayFloat, IPosition(2, nopcal, inforef.nrIFs()) );
    FITSBinaryColumn       polcala( "POLCALA", TpArrayFloat, IPosition(2, nopcal, inforef.nrIFs()) );

    //
    //  Create a new Binary Table object
    //
    FITSBinaryTableHDU*    hduptr( new FITSBinaryTableHDU("ANTENNA", 1) );

    //
    //  All the MS columns that we need
    //
    ROScalarColumn<Double> timecol( inforef.getMS(), MS::columnName(MS::TIME) );

    //
    //  Configure the binary table
    //
    hduptr->addColumn( time );
    hduptr->addColumn( interval );	
    hduptr->addColumn( anname );
    hduptr->addColumn( anno );
    hduptr->addColumn( array );
    hduptr->addColumn( freqid );
    hduptr->addColumn( diglev );
    hduptr->addColumn( poltya );
    hduptr->addColumn( polaa );

    // HV: 25/09/2003 - See comment below near decl. of 'arflt'
    if( nopcal>0 )
    {
        hduptr->addColumn( polcala );
    }
    hduptr->addColumn( poltyb );
    hduptr->addColumn( polab );
    // HV: 25/09/2003 - See comment below near decl. of 'arflt'
    if( nopcal>0 )
    {
        hduptr->addColumn( polcalb );
    }

    //
    //  Let's count how many rows the resulting table should have.....
    // 
    //  For now, since we do not know any time-dependant stuff, we
    //  just write a single row for every antenna....
    //
    nRows = antennaTable.nrow();

    
    hduptr->setNumberRows( nRows );
    
    //
    //  Restructure the row-record and make RecordFieldPtrs that point
    //  into the record....
    //
    Record                          tablerow( hduptr->getRowDescription() );
    RecordFieldPtr<Double>          timeptr( tablerow, "TIME" );
    RecordFieldPtr<Double>          intervalptr( tablerow, "TIME_INTERVAL" );    
    RecordFieldPtr<String>          nameptr( tablerow, "ANNAME" );
    RecordFieldPtr<Int>             annoptr( tablerow, "ANTENNA_NO" );
    RecordFieldPtr<Int>             arrayptr( tablerow, "ARRAY" );
    RecordFieldPtr<Int>             freqidptr( tablerow, "FREQID" );
    RecordFieldPtr<Int>             diglevptr( tablerow, "NO_LEVELS" );
    RecordFieldPtr<uChar>           poltyaptr( tablerow, "POLTYA" );
    RecordFieldPtr<Array<Float> >   polaaptr( tablerow, "POLAA" );
    RecordFieldPtr<uChar>           poltybptr( tablerow, "POLTYB" );
    RecordFieldPtr<Array<Float> >   polabptr( tablerow, "POLAB" );
    RecordFieldPtr<Array<Float> >   polcalaptr;
    RecordFieldPtr<Array<Float> >   polcalbptr;

    if( nopcal>0 )
    {
        polcalaptr.attachToRecord( tablerow, "POLCALA" );
        polcalbptr.attachToRecord( tablerow, "POLCALB" );
    }

    //
    // HV: 25/09/2003 - This array could/should/would contain the PCAL
    //                  parameters. At the moment, nopcal==0 and aips++
    //                  throws exceptions around the place when trying to
    //                  create an array with any one of the dims being <=0!?
    //                  Solution: test value of nopcal and only write the data
    //                  iff nopcal>0.
    //
    Array<Float>                    arflt;
   
    if( nopcal>0 )
    {
        arflt.resize( IPosition(2,nopcal,inforef.nrIFs()) );
        arflt = (Float)0.0;
    }
    
    //
    //  Columnstuff...
    //
	Table                           tmptab;
	Double                          midpoint;
	Double                          duration;
	Double                          tzero;
	MVTime                          obsdate( inforef.observingDate() );
	Vector<Double>                  tr;
    ROMSAntennaColumns              antcols( antennaTable );
	ROMSObservationColumns          obscols( inforef.getMS().observation() );

	//
	// Get the timerange from the Observation subtable
	//
	obscols.timeRange().get( 0, tr );

	//
	// transform the timerange into a midpoint + duration in units of days
	//
	duration = tr(1) - tr(0);

	midpoint = (tr(0) + tr(1))/2.0;
	
	//
	// According to the IDI standard, the TIME must be the time IN DAYS
	// that elapsed between 0h00 on the reference date of the experiment.
	// So let's get the refdate for the experiment and truncate that
	// to 0h00 and see how much time elapsed...
	//
	// TIME_INTERVAL must be the duration of the interval, also in days!
	//
	duration /= (60.0*60.0*24.0);

	//
	// obsdate = reference date of the experiment as a MVTime object.
	// Get the value in seconds:
	//
	tzero  = obsdate.day();

	//
	// Now round off to integer days:
	//
	tzero  = (Double)((Long)tzero);
	
	//
	// All that's left is to change midpoint to units of days (it's still in
	// seconds, as you read this....). Then subtract the midpoint and tzero
	// and tada!
	//	
	midpoint /= (60.0*60.0*24.0);
	midpoint -= tzero;
	
    //
    //  Ok. Write everything!
    //
    nRows = 0;

    for( uInt i=0; i<(uInt)antennaTable.nrow(); i++ )
    {
        //
        //  Fill in recordfields that do not change across rows (if any)
        //
        *polaaptr    = (Float)0.0;
        *polabptr    = (Float)0.0;

        if( nopcal>0 )
        {   
            *polcalaptr  = arflt;
            *polcalbptr  = arflt;
        }

		//
		//  According to doc. values are:
		//
		//  2      -> for MkII/MkIII terminals
		//  2 or 4 -> for VLBA depending on observing mode (I guess: also MkIV)
		//
        *diglevptr   = 2;

		*arrayptr    = 1;
		*freqidptr   = 1;

        //
        //  Get the name, otherwise the nxt printout don't show nothing...
        // or the wrong thing :/
        //
		*nameptr     = upcase( antcols.name()(i) );

		//
		// We should really fill this in... let's make these settings valid for 
		// the whole experiment?
		//
		cout << "---- Marking single row of data in Antenna table valid for" << endl
			 << "     the whole experiment for Antenna with id=" << i << " ("
			 << *nameptr << ")" << endl;

	
        // get the rest of the time
		*timeptr     = midpoint;
		*intervalptr = duration;

            
		//
		//  Number
		//
		*annoptr     = (Int)i + 1;
            
		//
		//  We need to fill in the polarization
		//
		//Bool          found( False );
		//const uInt    nmax( feedTable.nrow() );
	
		//
		//  First set to default value
		//
//		Stokes::StokesTypes  stktyp( Stokes::fromFITSValue(inforef.getFirstStokes()) );
//		String               stkstr( Stokes::name(stktyp) );

//		*poltyaptr = stkstr[0];
//		*poltybptr = stkstr[1];
	
		//
		// Ok, lets do this again. Locate the row(s) in the FeedTable
		// for the current antenna...
		//
		*poltyaptr = '?';
		*poltybptr = '?';
		
		tmptab = feedTable( feedTable.col("ANTENNA_ID")==(Int)i );
		if( tmptab.nrow()>0 )
		{
			//
			// Shoot.... multiple rows
			// Should deal with this more nicely... grunt...
			//
			if( tmptab.nrow()>1 )
			{
				cout << "++++ WARNING: More than one row of FEED data for ANTENNA"
					 << " with id=" << i << " (" << *nameptr << ")" << endl
					 << "     This is fine, but not (yet) supported by tConvert(c)"
					 << endl
					 << "     hence call the author and yell at him to fix this!"
					 << endl
					 << "     (+31)(0)521596516, at the time of writing this..."
					 << endl;
			}

			//
			// Just take whatever's in the first row...
			//
			ROMSFeedColumns fc(tmptab);
			Int antennaId = fc.antennaId()(0);
			Int feedId = fc.feedId()(0);
			Int spWindowId = fc.spectralWindowId()(0);

			Int no_pol = fc.numReceptors()(0);
			if (no_pol > 2)
			  {
			    cerr << "Warning: FITS_IDI has no support for "
				 << no_pol << " polarizations." << endl
				 << "The number of polarizations will be "
				 << " limited to 2." << endl;
			  }

			uInt id1 = 0, id2 = 1;
			if (inforef.switchPolarizations (antennaId, feedId,
							 spWindowId))
			  id1 = 1, id2 = 0;

			Vector<String> polType = fc.polarizationType()(0);
			DebugAssertExit((Int)polType.nelements() == no_pol);
			*poltyaptr = *polType(id1).c_str();
			if (polType.nelements() > 1)
			  *poltybptr = *polType(id2).c_str();
		}
		else
		{
			cout << "++++ WARNING: Could not find FEED data for Antenna with id="
				 << i << " (" << *nameptr << ")" << endl
				 << "     Putting in weird values....!" << endl;
		}
			
	
		//
		//  Do write the row!
		//
		hduptr->putrow( i, tablerow );
		nRows++;
    }

    //
    //  add some extra keywords....
    //
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;
    
    //
    //  The common keywords
    //
    ek.mk( "TABREV", (Int)1 );
    ek.mk( "OBSCODE", inforef.getObsCode().c_str() );
    ek.mk( "NO_STKD", (Int)inforef.nrPolarizations() );
    ek.mk( "STK_1", inforef.getFirstStokes() );
    ek.mk( "NO_BAND", (Int)inforef.nrIFs() );
    ek.mk( "NO_CHAN", (Int)inforef.nrChannels() );
    ek.mk( "REF_FREQ", inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    ek.mk( "CHAN_BW", inforef.getChannelwidth() );
    ek.mk( "REF_PIXL", (Float)1 );	
    ek.mk( "NOPCAL", (Int)nopcal );
    ek.mk( "POLTYPE", "APPROX" );
    
    ek.first();
    ek.next();
    
    kwptr=ek.curr();
    while( kwptr )
    {
    	hduptr->addExtraKeyword( *kwptr );
    	kwptr=ek.next();
    }
    
    //
    //  And add the HDUObject to the return value vector
    //
    result.push_back( hduptr );
    return result;
}



idiantennaGenerator::~idiantennaGenerator()
{
}

