#ifndef IDIFLAGGENERATOR_H
#define IDIFLAGGENERATOR_H

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

class idiFlagGenerator : public virtual hduGenerator
{
 public:

  idiFlagGenerator();
  virtual ~idiFlagGenerator();

  virtual idtype_t getID(void) const { return "idiFlag"; }

  virtual hdulist_t generate(const MSConversionInfo& info) const;

 private:
  idiFlagGenerator(const idiFlagGenerator&);
  idiFlagGenerator& operator=(const idiFlagGenerator&);
};

DECLARE_GENERATOR(idiFlagGenerator);

#endif // idiFlagGenerator.h
