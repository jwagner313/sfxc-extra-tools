#ifndef IDISYSTEMPGENERATOR_H
#define IDISYSTEMPGENERATOR_H

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

class idiSysTempGenerator : public virtual hduGenerator
{
 public:

  idiSysTempGenerator();
  virtual ~idiSysTempGenerator();

  virtual idtype_t getID(void) const { return "idiSysTemp"; }

  virtual hdulist_t generate(const MSConversionInfo& info) const;

 private:
  idiSysTempGenerator(const idiSysTempGenerator&);
  idiSysTempGenerator& operator=(const idiSysTempGenerator&);
};

DECLARE_GENERATOR(idiSysTempGenerator);

#endif // idiSysTempGenerator.h
