// Generate a FITS-IDI SYSTEM_TEMPERATURE (TS) table.
//
// Written by: Mark Kettenis
//

#define RCSID(string) \
  static char rcsid[] __attribute__ ((unused)) = string

RCSID("@(#) $Id: idiSysTempGenerator.cc,v 1.2 2007/07/04 07:54:53 jive_cc Exp $");

// Standard includes.
//
#include <set>

// AIPS++ includes.
//
#include <casa/aips.h>
#include <casa/BasicMath/Math.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/Utilities/Assert.h>
#include <casa/Utilities/GenSort.h>
#include <measures/Measures/MEpoch.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <ms/MeasurementSets/MSSysCal.h>
#include <ms/MeasurementSets/MSSysCalColumns.h>
#include <tables/Tables/ExprNode.h>
#include <tables/Tables/TableIter.h>

// JIVE includes.
//
#include <jive/ms2uvfitsimpl/FITS-IDI.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

#include <jive/ms2uvfitsimpl/Generators/idiSysTempGenerator.h>

using namespace std;
using namespace casa;

// Implementation of class idiSysTempGenerator.
//

DEFINE_GENERATOR(idiSysTempGenerator);

idiSysTempGenerator::idiSysTempGenerator() : hduGenerator()
{
}

hdulist_t
idiSysTempGenerator::generate(const MSConversionInfo& info) const
{
  hdulist_t result;
  const MeasurementSet& ms = info.getMS();
  const ROMSColumns msc(ms);
  String obscode = info.getObsCode();
  Int no_stkd = info.nrPolarizations();
  Int stk1 = info.getFirstStokes();
  Int no_band = info.nrIFs();
  Int no_chan = info.nrChannels();
  Float ref_freq = info.getReferenceFrequency(MSConversionInfo::CenterChannel);
  Float chan_bw = info.getChannelwidth();
  Float ref_pixl = 1.0;
  Int no_pol = info.getNumReceptors();
  const MSSysCal& sc = ms.sysCal();
  IPosition bandShape(1, no_band);
  const fqmap_type& groups = info.getFreqGroups();

  // Skip if the SYSCAL table is missing or empty.
  //
  if(sc.isNull() || sc.nrow() == 0)
    return result;

  // Create table.
  //
  FITSBinaryTableHDU *tsysTab =
    new FITSBinaryTableHDU("SYSTEM_TEMPERATURE", 1); // ??? Version?

  {
    // Create column descriptions.
    //
    FITSBinaryColumn time("TIME", "Central Time of interval covered",
			  "1D", "DAYS");
    FITSBinaryColumn time_interval("TIME_INTERVAL", "Duration of interval",
				   "1E", "DAYS");
    FITSBinaryColumn source_id("SOURCE_ID", "Source ID number", "1J", "");
    FITSBinaryColumn antenna_no("ANTENNA_NO", "Antenna number", "1J", "");
    FITSBinaryColumn array ("ARRAY", "Array number", "1J", "");
    FITSBinaryColumn freqid("FREQID", "Frequency setup number", "1J", "");
    FITSBinaryColumn tsys_1("TSYS_1", TpArrayFloat, bandShape, "KELVIN",
			    "System temperatures for polarization 1");
    FITSBinaryColumn tant_1("TANT_1", TpArrayFloat, bandShape, "KELVIN",
			    "System temperatures for polarization 1");

    // Add columns.
    //
    tsysTab->addColumn(time);
    tsysTab->addColumn(time_interval);
    tsysTab->addColumn(source_id);
    tsysTab->addColumn(antenna_no);
    tsysTab->addColumn(array);
    tsysTab->addColumn(freqid);
    tsysTab->addColumn(tsys_1);
    tsysTab->addColumn(tant_1);

    // Create and add optional columns.
    //
    if (no_pol > 1)
      {
	if (no_pol > 2)
	  cerr << "Warning: FITS-IDI has no support for " << no_pol
	       << " polarizations." << endl
	       << "The number of polarizations will be limited to 2."
	       << endl;

	// Create optional column descriptions.
	//
	FITSBinaryColumn tsys_2("TSYS_2", TpArrayFloat, bandShape, "KELVIN",
				"System temperatures for polarization 2");
	FITSBinaryColumn tant_2("TANT_2", TpArrayFloat, bandShape, "KELVIN",
				"System temperatures for polarization 2");

	// Add optional columns.
	//
	tsysTab->addColumn(tsys_2);
	tsysTab->addColumn(tant_2);
      }

    // Add keywords.
    //
    addCommonKeywords(tsysTab, 1, obscode, no_stkd, stk1, no_band, no_chan,
		      ref_freq, chan_bw, ref_pixl);
    addExtraKeyword(tsysTab, "NO_POL", no_pol);
  }

  Record data(tsysTab->getRowDescription());
  RecordFieldPtr<Double> time(data, "TIME");
  RecordFieldPtr<Float> time_interval(data, "TIME_INTERVAL");
  RecordFieldPtr<Int> source_id(data, "SOURCE_ID");
  RecordFieldPtr<Int> antenna_no(data, "ANTENNA_NO");
  RecordFieldPtr<Int> array(data, "ARRAY");
  RecordFieldPtr<Int> freqid(data, "FREQID");
  RecordFieldPtr<Array<Float> > tsys_1(data, "TSYS_1");
  RecordFieldPtr<Array<Float> > tant_1(data, "TANT_1");
  RecordFieldPtr<Array<Float> > tsys_2;
  RecordFieldPtr<Array<Float> > tant_2;
  if (no_pol > 1)
    {
      tsys_2.attachToRecord(data, "TSYS_2");
      tant_2.attachToRecord(data, "TANT_2");
    }

  // Iterate over all antennas, feeds and time intervals that we have
  // system calibration data for.
  //
  Block<String> iterKeys(4);
  iterKeys[0] = "ANTENNA_ID";
  iterKeys[1] = "FEED_ID";
  iterKeys[2] = "TIME";
  iterKeys[3] = "INTERVAL";
  TableIterator scIter(sc, iterKeys);

  // Sigh.  Because we have to determine the number of rows for the
  // generated SYSTEM_TEMPERATURE table before we can write the rows,
  // we have to iterate twice.
  //
  // Refer for comments to the second iteration.
  //
  uInt nrow = 0;
  for (; !scIter.pastEnd(); scIter.next())
    {
      const MSSysCal& sc = scIter.table();
      ROMSSysCalColumns scc(sc);

      if (info.getFieldId(scc.antennaId()(0), scc.time()(0)) == -1)
	continue;

      fqmap_type::const_iterator curfq = groups.begin();
      while (curfq != groups.end())
	{
	  Bool skip = True;

	  uInt band = 0;
	  const sbset_type &subbands = curfq->second.subbands;
	  sbset_type::const_iterator cursb = subbands.begin();
	  while (cursb != subbands.end())
	    {
	      AlwaysAssertExit((Int)band < no_band);

	      Int id = cursb->spw;
	      MSSysCal scMatch = sc(sc.col("SPECTRAL_WINDOW_ID") == id);

	      if (scMatch.nrow() == 0)
		scMatch = sc(sc.col("SPECTRAL_WINDOW_ID") == -1);

	      if (scMatch.nrow() > 0)
		{
		  ROMSSysCalColumns sccMatch(scMatch);
		  if (!sccMatch.tsys().isNull())
		    skip = False;
		  if (!sccMatch.tant().isNull())
		    skip = False;
		}

	      band++;
	      cursb++;
	    }

	  if (!skip)
	    nrow++;
	  curfq++;
	}
    }
  scIter.reset();

  tsysTab->setNumberRows(nrow);

  cout << "Writing FITS-IDI SYSTEM_TEMPERATURE (TS) table." << endl;

  for (uInt row = 0; !scIter.pastEnd(); scIter.next())
    {
      const MSSysCal& sc = scIter.table();
      ROMSSysCalColumns scc(sc);

      Int fieldId = info.getFieldId(scc.antennaId()(0), scc.time()(0));
      if (fieldId == -1)
	continue;

      *time = info.getIDITime(scc.timeMeas()(0));
      *time_interval = scc.intervalQuant()(0).getValue("d");
      *source_id = fieldId + 1;
      *antenna_no = scc.antennaId()(0) + 1;
      *array = -1;		// XXX

      // Iterate over all fequency groups/setups and bands.
      //
      // NOTE: We assume that the list is properly sorted.  All bands
      // within a frequency setup should be grouped together, and
      // should be in the right order.
      //
      fqmap_type::const_iterator curfq = groups.begin();
      while (curfq != groups.end())
	{
	  // Assume we have no system calibration data for any band in
	  // this frequency group/setup.
	  //
	  Bool skip = True;

	  *freqid = curfq->second.aipsfreqgrp;

	  // The FITS-IDI specification says that: "If system [or
	  // antenna] tempere information is not available for any
	  // band in either polarization then the corresponding
	  // element of the arrays shall be set to NaN."
	  //
	  *tsys_1 = Array<Float>(bandShape, floatNaN());
	  *tant_1 = Array<Float>(bandShape, floatNaN());
	  if (no_pol > 1)
	    {
	      *tsys_2 = Array<Float>(bandShape, floatNaN());
	      *tant_2 = Array<Float>(bandShape, floatNaN());
	    }

	  uInt band = 0;
	  const sbset_type &subbands = curfq->second.subbands;
	  sbset_type::const_iterator cursb = subbands.begin();
	  while (cursb != subbands.end())
	    {
	      AlwaysAssertExit((Int)band < no_band);

	      Int id = cursb->spw;
	      MSSysCal scMatch = sc(sc.col("SPECTRAL_WINDOW_ID") == id);

	      // In its description of the FEED table, the
	      // MeasurementSet definition (version 2.0) says that: "A
	      // value of -1 indicates that a row is valid for all
	      // spectral windows."  This phrase isn't repeated in the
	      // description of the SYSCAL table, but it doesn't hurt
	      // to deal with it here.  So if we didn't find the
	      // spectral band we're looking for, see if there is a
	      // row with SPECTRAL_WINDOW_ID set to -1, and use the
	      // data in that row.
	      //
	      if (scMatch.nrow() == 0)
		scMatch = sc(sc.col("SPECTRAL_WINDOW_ID") == -1);

	      if (scMatch.nrow() > 0)
		{
		  if (scMatch.nrow() > 1)
		    cerr << "Warning: Multiple calibration measurements "
			 << "found for the same antenna feed and time."
			 << endl
			 << "Only the first measurement will be used."
			 << endl
			 << "ANTENNA_ID: " << scc.antennaId()(0) << endl
			 << "FEED_ID: " << scc.feedId()(0) << endl
			 << "SPECTRAL_WINDOW_ID: " << id << endl
			 << "TIME: " << scc.timeMeas()(0) << endl;

		  ROMSSysCalColumns sccMatch(scMatch);

		  // Switch polarizations if necessary.
		  //
		  uInt id1 = 0, id2 = 1;
		  uInt antennaId = sccMatch.antennaId()(0);
		  uInt feedId = sccMatch.feedId()(0);
		  uInt spWindowId = sccMatch.spectralWindowId()(0);
		  if (info.switchPolarizations (antennaId, feedId, spWindowId))
		    id1 = 1, id2 = 0;

		  // System temperature.
		  //
		  if (!sccMatch.tsys().isNull())
		    {
		      Float tsys = sccMatch.tsys()(0)(IPosition(1, id1));
		      (*tsys_1)(IPosition(1, band)) = tsys;
		      if (no_pol > 1)
			{
			  tsys = sccMatch.tsys()(0)(IPosition(1, id2));
			  (*tsys_2)(IPosition(1, band)) = tsys;
			}
		      skip = False; // We found some data.
		    }

		  // Antenna temperature.
		  //
		  if (!sccMatch.tant().isNull())
		    {
		      Float tant = sccMatch.tant()(0)(IPosition(1, id1));
		      (*tant_1)(IPosition(1, band)) = tant;
		      if (no_pol > 1)
			{
			  Float tant = sccMatch.tant()(0)(IPosition(1, id2));
			  (*tant_2)(IPosition(1, band)) = tant;
			}
		      skip = False; // We found some data.
		    }
		}

	      band++;
	      cursb++;
	    }

	  // Skip this frequency group/setup if we didn't find any
	  // temperature data.
	  //
	  if (!skip)
	    {
	      tsysTab->putrow(row, data);
	      row++;
	    }
	  curfq++;
	}
    }

  // Store return value.
  //
  result.push_back(tsysTab);
  return result;
}

idiSysTempGenerator::~idiSysTempGenerator()
{
}
