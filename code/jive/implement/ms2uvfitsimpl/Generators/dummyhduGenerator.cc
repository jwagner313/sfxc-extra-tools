//
//  Implementation of class dummyhduGenerator
//
//
//
//   Author:   Harro Verkouter
//             16-3-1998
// 
//   $Id: dummyhduGenerator.cc,v 1.7 2006/11/22 14:14:12 jive_cc Exp $
//
//
//  My includes
//
#include <jive/ms2uvfitsimpl/Generators/dummyhduGenerator.h>
// //  AIPS++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSPrimaryArray.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <casa/Containers/Record.h>

using namespace casa;

//  This function performs all the dirty work. It creates a dummy hdu
//  object! It is the callers response to delete the object if he/she
//  no longer desires to use the object!
hdulist_t dummyhduGenerator::generate( const MSConversionInfo& ) const
{
    FITSAxis          cmplx( "COMPLEX", "The complex axis", 2 );
    FITSAxis          stokes( "STOKES", "The polarization axis", 2, -1.0, -1.0 );
    FITSAxis          ra( "RA", "Position axis (historically)" );
    FITSAxis          declination( "DEC", "Position axis (historically)" );
    hdulist_t         result;
    Array<Float>      data;
    FITSPrimaryArray* newhdu( new FITSPrimaryArray(TpFloat) );
    
    //
    // Ok. "Let's doohooohoo it!" (ref. Lord Flasheart in `BlackAdder
    // goes forth!')
    //


    newhdu->addAxis( cmplx );
    newhdu->addAxis( stokes );
    newhdu->addAxis( ra );
    newhdu->addAxis( declination );


    IPosition    hdushape( newhdu->shape() );
    Record       datarecord;

    data.resize( hdushape );    

    data=Float(0.03);

    
    datarecord.define( "0", data );
    
    newhdu->put( datarecord );

    cout << "Shape: " << hdushape << endl;
    
    //
    //  Create the HDU-object
    //
    result.push_back( newhdu );
    return result;
}


//
//  The d'tor: nothing to destruct at this moment.....
//
dummyhduGenerator::~dummyhduGenerator()
{
}











