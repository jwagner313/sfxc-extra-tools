// idityGenerator.cc
//
// Implementation of the idityGenerator class. See idityGenerator.h
// for details.
//
//
// Author:
//
//      Harro Verkouter   12-3-1998
//      Des Small         28-02-2007
//
// $Id: idityGenerator.cc,v 1.2 2011/01/06 13:54:13 jive_cc Exp $

#include <jive/ms2uvfitsimpl/Generators/idityGenerator.h>

//
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/Vector.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/BasicSL/Constants.h>
#include <tables/Tables/TableIter.h>

#include <ms/MeasurementSets/MSSysCal.h>
#include <ms/MeasurementSets/MSSysCalColumns.h>

using namespace casa;

DEFINE_GENERATOR(idityGenerator);

//  This function will do all the work of getting all the necessary
//  TSYS information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
//
hdulist_t idityGenerator::generate( const MSConversionInfo& inforef ) const
{
    cout << "Making TY table" << endl;
    //
    //  Local variables we need
    //
    uInt                nrifs( inforef.nrIFs() );
    Record              rowdata;
    MSSysCal            sysCal( inforef.getMS().sysCal() );
    hdulist_t           result;
    IPosition           ifshape( IPosition(1, inforef.nrIFs()) );
    ROMSColumns         mscol( inforef.getMS() );
    Vector<Float>       tsysval;
    Vector<Float>       tsyspol1( ifshape );
    Vector<Float>       tantpol1( ifshape );
    Vector<Float>       tsyspol2( ifshape );
    Vector<Float>       tantpol2( ifshape );
    ROMSSysCalColumns   sysCalColumns( inforef.getMS().sysCal() );
    FITSBinaryTableHDU* idityptr( new FITSBinaryTableHDU("SYSTEM_TEMPERATURE",
							 1) );

    ROScalarColumn<Int>  fieldid( inforef.getMS(), MS::columnName(MS::FIELD_ID) );
    Int knownSourceId = fieldid(0) + 1;

    //
    //  Counters etc.
    //
    uInt                i;
    uInt                strow;
    Double              sttim;
    Double              reftime;
    const Int           npol( (sysCalColumns.tsys().shape(0))(0) );
    const uInt          nrow( sysCalColumns.time().nrow() );

    
    if( !nrow )
    {
        cout << "No syscal Tsys info" << endl;
        return result;
    }

    //
    // Determine if we have to skip the first time.
    // This is needed for WSRT MS's, where the first time in the SYSCAL
    // table is the average at the middle of the observation.
    //
    strow = 0;
    sttim = sysCalColumns.time()( 0 );
    for( i=0; i<nrow; i++ )
    {
	Double tim( sysCalColumns.time()(i) );

	if( tim!=sttim )
        {
	    if( tim<sttim )
            {
		strow = i;
                cout << "First time in SYSCAL table is an average and will be skipped" << endl;
	    }
	    break;
	}
    }

    cout << "Found " << nrow-strow << " TY table entries" << endl;

    //
    // Get reference time (i.e. start time) from the main table.
    //
    reftime = floor( ((inforef.observingDate().second())/C::day) ) * C::day;

    //
    //  Now it's safe to start adding columns to the table!
    //
    idityptr->addColumn( FITSBinaryColumn("TIME", "", "1D", "DAYS") );
    idityptr->addColumn( FITSBinaryColumn("TIME_INTERVAL", "", "1E", "DAYS") );
    idityptr->addColumn( FITSBinaryColumn("SOURCE_ID", "", "1J", "") );
    idityptr->addColumn( FITSBinaryColumn("ANTENNA_NO", "", "1J", "") );
    idityptr->addColumn( FITSBinaryColumn("ARRAY", "", "1J", "") );
    idityptr->addColumn( FITSBinaryColumn("FREQID", "", "1J", "") );
    idityptr->addColumn( FITSBinaryColumn("TSYS_1", TpArrayFloat, ifshape, 
					  "KELVINS") );
    idityptr->addColumn( FITSBinaryColumn("TANT_1", TpArrayFloat, ifshape,
					  "KELVINS") );

    if( npol==2 )
    {
        idityptr->addColumn( FITSBinaryColumn("TSYS_2", TpArrayFloat, ifshape,
					      "KELVINS") );
        idityptr->addColumn( FITSBinaryColumn("TANT_2", TpArrayFloat, ifshape,
					      "KELVINS") );
    }

    //
    //  Re-shape the rowdata-record, so as it can hold all the
    //  row-data-members!
    //
    rowdata.restructure( idityptr->getRowDescription() );

    //
    //  Having done that, we can create the record-field objects!
    //
    RecordFieldPtr<Int>           sourceId( rowdata, "SOURCE_ID");
    RecordFieldPtr<Int>           antenna( rowdata, "ANTENNA_NO");
    RecordFieldPtr<Int>           arrayId( rowdata, "ARRAY");
    RecordFieldPtr<Int>           spwId( rowdata, "FREQID");
    RecordFieldPtr<Double>        time( rowdata, "TIME");
    RecordFieldPtr<Float>         interval( rowdata, "TIME_INTERVAL");
    RecordFieldPtr<Array<Float> > tsys2;
    RecordFieldPtr<Array<Float> > tant2;
    RecordFieldPtr<Array<Float> > tsys1( rowdata, "TSYS_1");
    RecordFieldPtr<Array<Float> > tant1( rowdata, "TANT_1");

    if( npol==2 )
    {
	tsys2 = RecordFieldPtr<Array<Float> >( rowdata, "TSYS_2");
	tant2 = RecordFieldPtr<Array<Float> >( rowdata, "TANT_2");
    }
    
    //
    //  Fill the table!
    //
            

    idityptr->setNumberRows( nrow / inforef.nrIFs() );
    
    //
    //  Sort the syscal-table and iterate over it...
    //
    Block<String>   sortnames( 3 );
    Block<String>   iternames( 2 );
    
    sortnames[0]="ANTENNA_ID";
    iternames[0]="ANTENNA_ID";

    sortnames[1]="TIME";
    iternames[1]="TIME";

    sortnames[2]="SPECTRAL_WINDOW_ID";
    
    //
    //  Create the sorted table....
    //
    Table          sortedsyscal( sysCal.sort(sortnames, Sort::Ascending) );
    
    //
    //  Now set up the iterator and count rows....
    //
    TableIterator  syscaliterator( sortedsyscal, iternames, 
				   TableIterator::Ascending );
    
    //
    //  We iterate through the syscal table antenna by antenna. The
    //  table was sorted by spectral_window_id, so the number of rows
    //  in the iterated table SHOULD equal the number of IFs (since 1
    //  IF = 1 SPECTRAL_WINDOW)
    //
    uInt      rowcnt( 0 );
    
    while( !syscaliterator.pastEnd() )
    {
	Table        antennacal( syscaliterator.table() );
	Vector<uInt> originalrownr( antennacal.rowNumbers() );
	
	//
	//  Verify that we have CAL info for every IF
	//
	if( antennacal.nrow()!=(uInt)ifshape[0] )
	{
	    Int    antid( -1 );
	    
	    if( antennacal.nrow() )
	    {
		antid = sysCalColumns.antennaId()( originalrownr(0) );
	    }
	    
	    cout << "+++ Not all CAL info for ANTENNA " << antid << "found. " << endl;
	    cout << "+++ Found data for " << antennacal.nrow() << " IFs. Needed " 
		 << ifshape[0] << endl;

	    //
	    //  Move on to nxt. antenna!
	    //
	    syscaliterator.next();
	    continue;
	}

	//
	//  Cache the first rownr...
	//
	uInt    row0( originalrownr(0) );
	//
	//  Ok. having verified that, get all the data
	//
	for( uInt ifcnt=0; ifcnt<nrifs; ifcnt++ )
	{
	    Array<Float>   tmp;	    
	    sysCalColumns.tsys().get( originalrownr(ifcnt), tmp );
	    tsyspol1( ifcnt ) = tmp( IPosition(1,0) );
	    if( npol==2 )
	    {
		tsyspol2( ifcnt ) = tmp( IPosition(1,1) );
	    }
	}
	tantpol1 = 0.0;
	if( npol==2 )
	{
	    tantpol2 = 0.0;
	}
	
	//
	//  Fill in the rowrecord.....
	//

	//
        // TIME
	//
	*time = (double)((sysCalColumns.time()(row0) - reftime) / C::day);
	*interval = (float)(sysCalColumns.interval()( row0 ) / C::day);

	//
	//  INDICES
	//
	*sourceId = knownSourceId;
	*antenna = sysCalColumns.antennaId()(row0) + 1;
	*spwId = sysCalColumns.spectralWindowId()(row0) + 1;

	//
	//  The data
	//
	*tsys1 = tsyspol1;
	*tant1 = tantpol1;
	
	if( npol==2 )
	{
	    *tsys2 = tsyspol2;
	    *tant2 = tantpol2;
	}
	
        idityptr->putrow( rowcnt, rowdata );
	syscaliterator.next();
	rowcnt++;
    }
    

     //
     //  add some extra keywords....
     //
     FitsKeyword*      kwptr;
     FitsKeywordList   ek;
     //
     //  The common keywords
     //
     ek.mk( "TABREV", (Int)1 );
     ek.mk( "OBSCODE", inforef.getObsCode().c_str() );
     ek.mk( "NO_STKD", (Int)inforef.nrPolarizations() );
     ek.mk( "NO_POL",  npol );
     ek.mk( "STK_1", inforef.getFirstStokes() );
     ek.mk( "NO_BAND", (Int)inforef.nrIFs() );
     ek.mk( "NO_CHAN", (Int)inforef.nrChannels() );
     ek.mk( "REF_FREQ", inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
     ek.mk( "CHAN_BW", inforef.getChannelwidth() );
     ek.mk( "REF_PIXL", (Float)1 );

     ek.first();
     ek.next();
     kwptr=ek.curr();
     while( kwptr )
     {
	  idityptr->addExtraKeyword( *kwptr );
	  kwptr=ek.next();
     }

    //  Prepare return value...
    result.push_back( idityptr );
    return result;
}


idityGenerator::~idityGenerator()
{
}

