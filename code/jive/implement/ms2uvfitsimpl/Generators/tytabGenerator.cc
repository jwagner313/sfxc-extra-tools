// tytabGenerator.cc
//
// Implementation of the tytabGenerator class. See tytabGenerator.h for details.
//
//
// Author:
//
//      Harro Verkouter   12-3-1998
//
// $Id: tytabGenerator.cc,v 1.8 2007/04/11 12:01:49 jive_cc Exp $

#include <jive/ms2uvfitsimpl/Generators/tytabGenerator.h>

//
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/Vector.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/BasicSL/Constants.h>
#include <tables/Tables/TableIter.h>

#include <ms/MeasurementSets/MSSysCal.h>
#include <ms/MeasurementSets/MSSysCalColumns.h>

using namespace casa;

DEFINE_GENERATOR(tytabGenerator);

//  This function will do all the work of getting all the necessary
//  TSYS information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
//
hdulist_t tytabGenerator::generate( const MSConversionInfo& inforef ) const
{
    cout << "Making TY table" << endl;
    //
    //  Local variables we need
    //
    uInt                nrifs( inforef.nrIFs() );
    Record              rowdata;
    MSSysCal            sysCal( inforef.getMS().sysCal() );
    hdulist_t           result;
    IPosition           ifshape( IPosition(1, inforef.nrIFs()) );
    ROMSColumns         mscol( inforef.getMS() );
    Vector<Float>       tsysval;
    Vector<Float>       tsyspol1( ifshape );
    Vector<Float>       tantpol1( ifshape );
    Vector<Float>       tsyspol2( ifshape );
    Vector<Float>       tantpol2( ifshape );
    ROMSSysCalColumns   sysCalColumns( inforef.getMS().sysCal() );
    FITSBinaryTableHDU* tytabptr( new FITSBinaryTableHDU("AIPS TY", 1) );

    //
    //  Counters etc.
    //
    uInt                i;
    uInt                strow;
    Double              sttim;
    Double              reftime;
    const Int           npol( (sysCalColumns.tsys().shape(0))(0) );
    const uInt          nrow( sysCalColumns.time().nrow() );

    
    if( !nrow )
    {
        cout << "No syscal Tsys info" << endl;
        return result;
    }

    //
    // Determine if we have to skip the first time.
    // This is needed for WSRT MS's, where the first time in the SYSCAL
    // table is the average at the middle of the observation.
    //
    strow = 0;
    sttim = sysCalColumns.time()( 0 );
    for( i=0; i<nrow; i++ )
    {
	Double tim( sysCalColumns.time()(i) );

	if( tim!=sttim )
        {
	    if( tim<sttim )
            {
		strow = i;
                cout << "First time in SYSCAL table is an average and will be skipped" << endl;
	    }
	    break;
	}
    }

    cout << "Found " << nrow-strow << " TY table entries" << endl;

    //
    // Get reference time (i.e. start time) from the main table.
    //
    reftime = floor( ((inforef.observingDate().second())/C::day) ) * C::day;

    //
    //  Now it's safe to start adding columns to the table!
    //
    tytabptr->addColumn( FITSBinaryColumn("TIME", "", "1E", "DAYS") );
    tytabptr->addColumn( FITSBinaryColumn("TIME INTERVAL", "", "1E", "DAYS") );
    tytabptr->addColumn( FITSBinaryColumn("SOURCE ID", "", "1J", "") );
    tytabptr->addColumn( FITSBinaryColumn("ANTENNA NO.", "", "1J", "") );
    tytabptr->addColumn( FITSBinaryColumn("SUBARRAY", "", "1J", "") );
    tytabptr->addColumn( FITSBinaryColumn("FREQ ID", "", "1J", "") );
    tytabptr->addColumn( FITSBinaryColumn("TSYS 1", TpArrayFloat, ifshape, 
					  "KELVINS") );
    tytabptr->addColumn( FITSBinaryColumn("TANT 1", TpArrayFloat, ifshape,
					  "KELVINS") );

    if( npol==2 )
    {
        tytabptr->addColumn( FITSBinaryColumn("TSYS 2", TpArrayFloat, ifshape,
					      "KELVINS") );
        tytabptr->addColumn( FITSBinaryColumn("TANT 2", TpArrayFloat, ifshape,
					      "KELVINS") );
    }

    //
    //  Re-shape the rowdata-record, so as it can hold all the
    //  row-data-members!
    //
    rowdata.restructure( tytabptr->getRowDescription() );

    //
    //  Having done that, we can create the record-field objects!
    //
    RecordFieldPtr<Int>           sourceId( rowdata, "SOURCE ID");
    RecordFieldPtr<Int>           antenna( rowdata, "ANTENNA NO.");
    RecordFieldPtr<Int>           arrayId( rowdata, "SUBARRAY");
    RecordFieldPtr<Int>           spwId( rowdata, "FREQ ID");
    RecordFieldPtr<Float>         time( rowdata, "TIME");
    RecordFieldPtr<Float>         interval( rowdata, "TIME INTERVAL");
    RecordFieldPtr<Array<Float> > tsys2;
    RecordFieldPtr<Array<Float> > tant2;
    RecordFieldPtr<Array<Float> > tsys1( rowdata, "TSYS 1");
    RecordFieldPtr<Array<Float> > tant1( rowdata, "TANT 1");

    if( npol==2 )
    {
	tsys2 = RecordFieldPtr<Array<Float> >( rowdata, "TSYS 2");
	tant2 = RecordFieldPtr<Array<Float> >( rowdata, "TANT 2");
    }
    
    //
    //  Fill the table!
    //
    tytabptr->setNumberRows( nrow-strow );
    
    //
    //  Sort the syscal-table and iterate over it...
    //
    Block<String>   sortnames( 3 );
    Block<String>   iternames( 2 );
    
    sortnames[0]="ANTENNA_ID";
    iternames[0]="ANTENNA_ID";

    sortnames[1]="TIME";
    iternames[1]="TIME";

    sortnames[2]="SPECTRAL_WINDOW_ID";
    
    //
    //  Create the sorted table....
    //
    Table          sortedsyscal( sysCal.sort(sortnames, Sort::Ascending) );
    
    //
    //  Now set up the iterator and count rows....
    //
    TableIterator  syscaliterator( sortedsyscal, iternames, 
				   TableIterator::Ascending );
    
    //
    //  We iterate through the syscal table antenna by antenna. The
    //  table was sorted by spectral_window_id, so the number of rows
    //  in the iterated table SHOULD equal the number of IFs (since 1
    //  IF = 1 SPECTRAL_WINDOW)
    //
    uInt      rowcnt( 0 );
    
    while( !syscaliterator.pastEnd() )
    {
	Table        antennacal( syscaliterator.table() );
	Vector<uInt> originalrownr( antennacal.rowNumbers() );
	
	//
	//  Verify that we have CAL info for every IF
	//
	if( antennacal.nrow()!=(uInt)ifshape[0] )
	{
	    Int    antid( -1 );
	    
	    if( antennacal.nrow() )
	    {
		antid = sysCalColumns.antennaId()( originalrownr(0) );
	    }
	    
	    cout << "+++ Not all CAL info for ANTENNA " << antid << "found. " << endl;
	    cout << "+++ Found data for " << antennacal.nrow() << " IFs. Needed " 
		 << ifshape[0] << endl;

	    //
	    //  Move on to nxt. antenna!
	    //
	    syscaliterator.next();
	    continue;
	}

	//
	//  Cache the first rownr...
	//
	uInt    row0( originalrownr(0) );
	//
	//  Ok. having verified that, get all the data
	//
	for( uInt ifcnt=0; ifcnt<nrifs; ifcnt++ )
	{
	    Array<Float>   tmp;	    
	    sysCalColumns.tsys().get( originalrownr(ifcnt), tmp );
	    tsyspol1( ifcnt ) = tmp( IPosition(1,0) );
	    if( npol==2 )
	    {
		tsyspol2( ifcnt ) = tmp( IPosition(1,1) );
	    }
	}
	tantpol1 = 0.0;
	if( npol==2 )
	{
	    tantpol2 = 0.0;
	}
	
	//
	//  Fill in the rowrecord.....
	//

	//
        // TIME
	//
	*time = (sysCalColumns.time()(row0) - reftime) / C::day;
	*interval = sysCalColumns.interval()( row0 ) / C::day;

	//
	//  INDICES
	//
	*sourceId = 1;
	*antenna = sysCalColumns.antennaId()(row0) + 1;
	*spwId = sysCalColumns.spectralWindowId()(row0) + 1;

	//
	//  The data
	//
	*tsys1 = tsyspol1;
	*tant1 = tantpol1;
	
	if( npol==2 )
	{
	    *tsys2 = tsyspol2;
	    *tant2 = tantpol2;
	}
	
        tytabptr->putrow( rowcnt, rowdata );
	syscaliterator.next();
	rowcnt++;
    }
    

    //
    //  Add extra keywords....
    //
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;

    ek.mk( "NO_IF", 1 );
    ek.mk( "NO_POL", npol );
    ek.mk( "REVISION", 10 );

    ek.first();
    ek.next();
    kwptr=ek.curr();
    while( kwptr )
    {
        tytabptr->addExtraKeyword( *kwptr );
        kwptr=ek.next();
    }

    //  Prepare return value...
    result.push_back( tytabptr );
    return result;
}


tytabGenerator::~tytabGenerator()
{
}

