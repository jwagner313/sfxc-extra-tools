//  Create the array_geometery binary table extension to be used when
//  writing FITS-IDI files. Ref. AIPS Memo #102, 'The FITS
//  Interferometry Data Interchange Format' by Chris Flatters
//  (www.nrao.edu/~cflatter)
//
//   $Id: arraygeomGenerator.h,v 1.4 2006/03/02 14:21:42 verkout Exp $
//
//   $Log: arraygeomGenerator.h,v $
//   Revision 1.4  2006/03/02 14:21:42  verkout
//   HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//   Revision 1.3  2006/01/13 11:35:45  verkout
//   HV: CASAfied version of the implement
//
//   Revision 1.2  2001/05/30 11:53:16  verkout
//   HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//   Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//   HV: Imported aips++ implement-stuff
//
//
#ifndef ARRAYGEOMGENERATOR_H
#define ARRAYGEOMGENERATOR_H

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


//
//  The arraygeom generator is a specific kind of HDU generator....
//
class arraygeomGenerator :
    public hduGenerator
{
public:

    // NOTE: The caller is responsible for (eventually) releasing the
    // allocated resources! (i.e. caller must delete the pointers in the list!)
    virtual hdulist_t   generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t    getID( void ) const {
        return "arraygeometry";
    }
    
    //  Delete the generator
    virtual ~arraygeomGenerator();
    
};


// See Registrar.h
DECLARE_GENERATOR(arraygeomGenerator);

#endif
