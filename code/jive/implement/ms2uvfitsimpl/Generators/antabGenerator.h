//     From a MSConversionInfo object, extract the info needed to
//     build a Classic aips ANTAB binary table extension!
//
//
//      $Id: antabGenerator.h,v 1.4 2006/03/02 14:21:42 verkout Exp $
#ifndef ANTABGENERATOR_H_INC
#define ANTABGENERATOR_H_INC


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

//  The antab generator is a specific kind of HDU generator....
class antabGenerator :
    public hduGenerator
{
public:

    // NOTE: The caller is responsible for (eventually) releasing the
    // allocated resources! (i.e. caller must delete the pointers!)
    virtual hdulist_t   generate( const MSConversionInfo& inforef ) const;
    

    virtual idtype_t    getID( void ) const {
        return "antab";
    }

    //  Delete the generator
    virtual ~antabGenerator();
};

// See Registrar.h
DECLARE_GENERATOR(antabGenerator);


#endif
