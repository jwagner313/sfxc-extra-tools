// implementation
//
// $Id: Registrar.cc,v 1.4 2007/05/25 14:14:58 jive_cc Exp $
//
// $Log: Registrar.cc,v $
// Revision 1.4  2007/05/25 14:14:58  jive_cc
// HV: - fixed stoopid typo [forgot a ';' after a cut-and-paste]
//     - trying to be a bit too smart and forgot that find_ID()
//     returned a different type than what I thought it would...
//
// Revision 1.3  2007/05/25 11:16:29  jive_cc
// HV: - took out debug statements
//     - redone layout to be more readable
//         (i hope)
//
// Revision 1.2  2007/04/11 12:01:48  jive_cc
// [small] Update of tConvert to include -wm2i flag, which allows conversion
// of Westerbork data (including gain-curve and system temperatures) to
// IDI FITS file format.
//
// Revision 1.1  2006/03/02 14:23:30  verkout
// HV: * Added support for automagically building a mapping of ID to actual hduGenerator; this class does the accounting and has defines to ease the class-writers task
//
//
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

using namespace std;

Registrar::generatormap_t*  Registrar::idToGeneratorMap = 0;


Registrar::Registrar() {
    // See if map was not yet initialized...
    if( idToGeneratorMap==0 )
        idToGeneratorMap = new std::map<hduGenerator::idtype_t, hduGenerator*>();
}

hduGenerator* Registrar::find_ID( const hduGenerator::idtype_t& id ) {
    hduGenerator*                  retval( 0 );
    generatormap_t::const_iterator curgen;

    curgen = idToGeneratorMap->find(id);
    if( curgen!=idToGeneratorMap->end() )
        retval = curgen->second;
    return retval;
}


bool Registrar::registerGenerator( hduGenerator* gen ) {
    pair<generatormap_t::iterator, bool> insres;

    insres = idToGeneratorMap->insert( make_pair(gen->getID(), gen) );
    return insres.second;
}

void Registrar::deregisterGenerator( const hduGenerator::idtype_t& id ) {
    generatormap_t::iterator curgen;

    curgen = idToGeneratorMap->find(id);
    if( curgen!=idToGeneratorMap->end() )
        idToGeneratorMap->erase( curgen );
    return;
}
