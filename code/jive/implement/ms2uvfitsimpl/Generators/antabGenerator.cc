// antabGenerator.cc
//
// Implementation of the antabGenerator class. See antabGenerator.h for details.
//
//
// Author:
//
//      Harro Verkouter   12-3-1998
//
// $Id: antabGenerator.cc,v 1.15 2012/07/05 07:49:57 jive_cc Exp $
#include <jive/ms2uvfitsimpl/Generators/antabGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

//
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <measures/Measures/MEpoch.h>
#include <ms/MeasurementSets/MSFeed.h>
#include <ms/MeasurementSets/MSAntenna.h>
#include <casa/Quanta/Euler.h>
#include <measures/Measures/MeasTable.h>

//
//  My includes
//
#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/ArrayMath.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <tables/Tables/ExprNode.h>
#include <fits/FITS.h>
#include <fits/FITS/FITSDateUtil.h>


#include <jive/Utilities/jexcept.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>


#include <jive/Utilities/streamutil.h>

using namespace std;
using namespace casa;



DEFINE_GENERATOR(antabGenerator);

//  This function will do all the work of getting all the necessary
//  antenna information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
//
hdulist_t antabGenerator::generate( const MSConversionInfo& inforef ) const
{
    cout << "Making AN table" << endl;

    //
    //  First, make a list off all the stuff that we may need!
    //
    Int                    nrantennas;
    String                 arrayname( "VLBA" );  // JanW: was EVN
    String                 rdate;
    MSFeed                 feedTable( inforef.getMS().feed() );
    hdulist_t              result;
    MSAntenna              antennaTable( inforef.getMS().antenna() );
    FITSBinaryColumn       anname( "ANNAME", "The antennaname", "8A", "" );
    FITSBinaryColumn       stabxyz( "STABXYZ", "", "3D", "METERS" );
    FITSBinaryColumn       orbparm( "ORBPARM", "Orbital parameters", "0D", "" );
    FITSBinaryColumn       nosta( "NOSTA", "Antenna number", "1J", "" );
    FITSBinaryColumn       mntsta( "MNTSTA", "Mount", "1J", "" );
    FITSBinaryColumn       staxof( "STAXOF", "x-offset", "1E", "METERS" );
    FITSBinaryColumn       poltya( "POLTYA", "Polarization type", "1A", "" );
    FITSBinaryColumn       polaa( "POLAA", "Polarization angle", "1E", "DEGREES" );
    FITSBinaryColumn       polcala( "POLCALA", "", "1E", "" );
    FITSBinaryColumn       poltyb( "POLTYB", "Polarization type", "1A", "" );
    FITSBinaryColumn       polab( "POLAB", "Polarization angle", "1E", "DEGREES" );
    FITSBinaryColumn       polcalb( "POLCALB", "", "1E", "" );


    //
    //  All the MS columns that we need
    //
	ROMSColumns                        mscol( inforef.getMS() );
    const ROScalarMeasColumn<MEpoch>&  timemeascol( mscol.timeMeas() );
	const ROScalarQuantColumn<Double>& timequantcol( mscol.timeQuant() ); 

    //
    //  Create a new Binary Table object
    //
    FITSBinaryTableHDU*    hduptr( new FITSBinaryTableHDU("AIPS AN",1) );
    
    //
    // Retrieve important info
    //
    String    timesys;
    
    try
    {
		FITSDateUtil::toFITS( rdate, timesys, inforef.observingDate().day() );
    }
    catch( const std::exception& x )
    {
		cerr << x.what() << endl;
	
		FITSDateUtil::toFITS( rdate, timesys, (Double)MVTime(Time()) );

		cout << j2ms::printeffer("Array #%3d: ", 1)
			<< "DATE-OBS can't be represented in FITS, default to "
				     << rdate << endl;
    }
    
    //
    //  Configure the binary table
    //
    hduptr->addColumn( anname );
    hduptr->addColumn( stabxyz );
    hduptr->addColumn( orbparm );
    hduptr->addColumn( nosta );
    hduptr->addColumn( mntsta );
    hduptr->addColumn( staxof );
    hduptr->addColumn( poltya );
    hduptr->addColumn( polaa );
    hduptr->addColumn( polcala );
    hduptr->addColumn( poltyb );
    hduptr->addColumn( polab );
    hduptr->addColumn( polcalb );
    

    //
    // Now we create column objects that refence into the SELECTED table
    //
    ROScalarColumn<String> antname( antennaTable, MSAntenna::columnName(MSAntenna::NAME) );
    ROScalarColumn<String> antmount( antennaTable, MSAntenna::columnName(MSAntenna::MOUNT) );
    ROArrayColumn<Double>  antposition( antennaTable, MSAntenna::columnName(MSAntenna::POSITION) );
    ROArrayColumn<Double>  antoffset( antennaTable, MSAntenna::columnName(MSAntenna::OFFSET) );
    ROArrayColumn<String>  poltype( feedTable, MSFeed::columnName(MSFeed::POLARIZATION_TYPE) );
    ROScalarColumn<Int>    antid( feedTable, MSFeed::columnName(MSFeed::ANTENNA_ID) );
        
    //
    //  The number of rows
    //
    nrantennas = antennaTable.nrow();
    
    hduptr->setNumberRows( nrantennas );
    
    
    //
    //  Restructure the record
    //
    Record               tablerow( hduptr->getRowDescription() );
        
    //
    //  Now we can have recordfieldptrs!
    //
    RecordFieldPtr<String>          nameptr( tablerow, "ANNAME" );
    RecordFieldPtr<Array<Double> >  stabptr( tablerow, "STABXYZ" );
    RecordFieldPtr<Int>             nostaptr( tablerow, "NOSTA" );
    RecordFieldPtr<Int>             mntstaptr( tablerow, "MNTSTA" );
    RecordFieldPtr<Float>           staxofptr( tablerow, "STAXOF" );
    RecordFieldPtr<uChar>           poltyaptr( tablerow, "POLTYA" );
    RecordFieldPtr<Float>           polaaptr( tablerow, "POLAA" );
    RecordFieldPtr<Float>           polcalaptr( tablerow, "POLCALA" );
    RecordFieldPtr<uChar>           poltybptr( tablerow, "POLTYB" );
    RecordFieldPtr<Float>           polabptr( tablerow, "POLAB" );    
    RecordFieldPtr<Float>           polcalbptr( tablerow, "POLCALB" );

    //
    //  Fill in recordfields that do not change across rows (if any)
    //
    *polaaptr   = (Float)0.0;
    *polabptr   = (Float)0.0;
    *polcalaptr = (Float)0.0;
    *polcalbptr = (Float)0.0;
    
    //
    //  Get the columns....
    //
    Int              tmpint;
    String           tmpstring;

    for( uInt i=0; i<(uInt)nrantennas; i++ )
    {
		//
		//  Name
		//
		*nameptr   = upcase( antname(i) );
	
		//
		// Baah! Need to flip the sign of the Y-coordinate of the
		// antennaposition because of Left-Hand coordsys in AIPS?!
		//
		Array<Double>  ap = antposition(i);
	
		ap( IPosition(1, 1) ) = -ap( IPosition(1, 1) );
		*stabptr = ap;

		//
		//  Number ( in FITS, antenna nrs are 1-relative)
		//
		*nostaptr  = i+1;
				
		//
		// Translate the MOUNT-string to Classic AIPS MNTSTA-number
		//
		tmpstring  = downcase( antmount(i) );
				
		if( tmpstring.contains("alt-az") )
		{
			tmpint=0;
		}
		else if( tmpstring.contains("equatorial") )
		{
			tmpint=1;
		}
		else if( tmpstring.contains("X-Y") )
		{
			tmpint=2;
		}
		else if( tmpstring.contains("orbiting") )
		{
			tmpint=3;
		}
		else
		{
			tmpint=0;
		}
            
		//
		//  Do put the value
		//
		*mntstaptr = tmpint;
				
		//
		//  The station X-offset
		//
		*staxofptr = (Float)( antoffset(i)(IPosition(1,0)) );
            
		//
		//  We need to fill in the polarization
		//
		const uInt    nmax( feedTable.nrow() );
	
		//
		//  First set to default value
		//
		*poltyaptr = ' ';
		*poltybptr = ' ';
	
		for( uInt j=0; j<nmax; j++ )
		{
			if( i==(uInt)antid(j) )
			{
				Vector<String>  poltypes( poltype(j) );
				
				if( poltypes.nelements()>=1 )
				{
					*poltyaptr=poltypes(0)[0];
				}
				if( poltypes.nelements()>=2 )
				{
					*poltybptr=poltypes(1)[0];
				}
				break;
			}
		}
	
		//
		//  Do write the row!
		//
		hduptr->putrow( i, tablerow );
    }
        
    //
    //  add some extra keywords....
    //
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;


    //
    //  Before we add them, let's calculate them!
    //
    //  Calculate GSTIA0, DEGPDY, UT1UTC, and IATUTC.
    //
    String          timeunit = MEpoch::showType( timemeascol(0).type() );
    Quantum<Double> stime( timequantcol(0) );
    MEpoch          utctime(stime, MEpoch::UTC );
    MEpoch          iattime = MEpoch::Convert(utctime, MEpoch::IAT)();
    MEpoch          ut1time = MEpoch::Convert(utctime, MEpoch::UT1)();
    Double          utcsec  = utctime.get("s").getValue();
    Double          ut1sec  = ut1time.get("s").getValue();
    Double          iatsec  = iattime.get("s").getValue();
    
    //
    //  Use the beginning of the IAT day to calculate the GMST.
    //
    Double          utcday = floor(utctime.get("d").getValue());
    Double          iatday = floor(iattime.get("d").getValue());
    Double          gstday, gstday1;
    {
		//
		//  Err.. our timesys=UTC, hende GSTIA0 *should* be GSTUTC0...
		//
		Quantum<Double>   itime(utcday, "d");
		MEpoch            ut0time(itime, MEpoch::UTC);
		MEpoch            gsttime = MEpoch::Convert(ut0time, MEpoch::GMST)();
	
		gstday = gsttime.get("d").getValue();
    }
    
    Double           gstdeg = 360 * (gstday - floor(gstday));
    {
		//
		// #degrees/IATday is the difference between this and the next day.
		//
		Quantum<Double>   itime(iatday+1, "d");
		MEpoch            ia0time(itime, MEpoch::IAT);
		MEpoch            gsttime = MEpoch::Convert(ia0time, MEpoch::GMST)();
	
		gstday1 = gsttime.get("d").getValue();
    }
    
    Double degpdy = 360 * (gstday1 - gstday);
    
    //
    //  PolarMotion gives -x and -y.
    //  Need to be multiplied by earth radius to get them in meters.
    //
    const Euler&          polarMotion = MeasTable::polarMotion( utcday );
    
    //
    //  Phew.. having done that, we can now safely fill them in...
    //
    //ek.mk( "ARRAYX", arraypos(0) );
    //ek.mk( "ARRAYY", arraypos(1) );
    //ek.mk( "ARRAYZ", arraypos(2) );
    ek.mk( "ARRAYX", 0.0 );
    ek.mk( "ARRAYY", 0.0 );
    ek.mk( "ARRAYZ", 0.0 );
    ek.mk( "GSTIA0", gstdeg );
    ek.mk( "DEGPDY", degpdy );
    ek.mk( "FREQ",   inforef.getReferenceFrequency( MSConversionInfo::CenterChannel ) );
    ek.mk( "RDATE",  rdate.c_str() );
    ek.mk( "POLARX", -polarMotion(0)*6356752.31 );
    ek.mk( "POLARY", -polarMotion(1)*6356752.31 );
    ek.mk( "UT1UTC", ut1sec-utcsec );
    ek.mk( "IATUTC", ::round2int(iatsec-utcsec) );
    ek.mk( "TIMSYS", "UTC" );
    ek.mk( "ARRNAM", arrayname.c_str() );
    ek.mk( "NUMORB", 0 );
    ek.mk( "NOPCAL", 0 );
    ek.mk( "POLTYPE", "        " );

    //
    //  Now transfer them to the hdu
    //
    ek.first();
    ek.next();
        
    kwptr=ek.curr();
    while( kwptr )
    {
	hduptr->addExtraKeyword( *kwptr );
	kwptr = ek.next();
    }
    
    //  And add the HDUObject to the return value vector
    result.push_back( hduptr );
    return result;
}



antabGenerator::~antabGenerator()
{
}

