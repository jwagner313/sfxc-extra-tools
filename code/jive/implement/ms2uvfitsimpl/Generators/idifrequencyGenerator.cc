//  Really do create the IDI Frequency Binary Table HDU+data object!
//
//  Author:   Harro Verkouter,  22-09-1999
//  
//  $Id: idifrequencyGenerator.cc,v 1.11 2011/10/12 12:45:35 jive_cc Exp $
//
//  $Log: idifrequencyGenerator.cc,v $
//  Revision 1.11  2011/10/12 12:45:35  jive_cc
//  HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//        changed into 'labels' for more genericity
//      * can now be built either as part of AIPS++/CASA [using makefile]
//        or can be built as standalone set of libs + binaries and linking
//        against casacore [using Makefile]
//        check 'code/
//
//  Revision 1.10  2007-03-13 09:18:21  jive_cc
//  HV: - cosmetic changes
//      - Reworked internals of MSConversionInfo
//        * uses less pointers
//        * less contstraints: there can be
//          different bandwidth/IF and hence
//          different channelwidth/IF.
//          Only nr-of-IFs/freqgroup and
//          nr-of-channels/IF need to be identical
//        * Caches the re-ordering of polarization products
//          as they may have to be re-ordered in order to
//          follow the FITS-ordering
//        * Spectralwindow re-ordering and mapping
//          now done via neato STL auto-sorting
//          containers. Resulting in more readable and
//          more reliable code.
//
//  Revision 1.9  2006/03/02 14:21:43  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.8  2006/01/13 11:35:46  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.7  2004/08/25 06:04:36  verkout
//  HV: Fiddled with templates. They all get instantiated automatically. As a result, the source code for the templates must be visible compiletime
//
//  Revision 1.6  2004/01/05 15:28:37  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.5  2003/09/12 07:37:39  verkout
//  HV: Code had to be made gcc3.* compliant.
//
//  Revision 1.4  2003/02/14 15:48:03  verkout
//  HV: * trial/FITS/FITSUtil was removed.
//      * std::string does not have automatic conversion to
//        (const char*). Fits-stuff does need (const char*)
//        so had to add the odd .c_str() here and there.
//
//  Revision 1.3  2002/08/06 09:07:01  verkout
//  HV: * Added sideband column
//  * Write relabelled freqgroup labels rather than direct AIPS++ freqgroup labels
//
//  Revision 1.2  2001/05/30 11:53:39  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//  HV: Imported aips++ implement-stuff
//
//
#include <jive/ms2uvfitsimpl/Generators/idifrequencyGenerator.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>

#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/ArrayUtil.h>
#include <casa/Arrays/ArrayMath.h>


#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/BasicMath/Math.h>

#include <jive/ms2uvfitsimpl/DumpRecord.h>

#include <ms/MeasurementSets/MSSpectralWindow.h>
#include <jive/Utilities/streamutil.h>


#include <casa/Containers/Block.h>


using namespace std;
using namespace casa;

DEFINE_GENERATOR(idifrequencyGenerator);

//  This function will do all the work of getting all the necessary
//  frequency information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
hdulist_t idifrequencyGenerator::generate( const MSConversionInfo& inforef ) const {

    cout << "Making FREQUENCY Table" << endl;

    Double                 refFreq( inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    hdulist_t              result;
    IPosition              ifshape( IPosition(1,inforef.nrIFs()) );
    ROMSColumns&           romsc( inforef.getROColumns() );
    FITSBinaryColumn       freqsel( "FREQID", "", "1J", "" );
    FITSBinaryColumn       iffreq( "BANDFREQ", TpArrayDouble, ifshape, "HZ" );
    FITSBinaryColumn       chwidth( "CH_WIDTH", TpArrayFloat, ifshape, "HZ" );
    FITSBinaryColumn       totbw( "TOTAL_BANDWIDTH", TpArrayFloat, ifshape, "HZ" );
    FITSBinaryColumn       sidebandcol( "SIDEBAND", TpArrayInt, ifshape );
    FITSBinaryTableHDU*    hduptr( new FITSBinaryTableHDU("FREQUENCY", 1) );
    

    //  Add columns to the binary table object
    hduptr->addColumn( freqsel );
    hduptr->addColumn( iffreq );
    hduptr->addColumn( chwidth );
    hduptr->addColumn( totbw );
    hduptr->addColumn( sidebandcol );


    // Each frequency group goes into one row
    hduptr->setNumberRows( inforef.getNrOfFreqGroups() );

    //  We now can create a record with the correct rowdescription and
    //  create RecordFieldPtrs.
    Record                         datarecord( hduptr->getRowDescription() );
    RecordFieldPtr<Int>            freqselptr( datarecord, "FREQID");
    RecordFieldPtr<Array<Int> >    sidebandptr( datarecord, "SIDEBAND");
    RecordFieldPtr<Array<Float> >  ifwidthptr( datarecord, "CH_WIDTH");
    RecordFieldPtr<Array<Float> >  totbwptr( datarecord, "TOTAL_BANDWIDTH");
    RecordFieldPtr<Array<Double> > iffreqptr( datarecord, "BANDFREQ");

    //  Predimension the arrays that go into a row
    //  - they hold the IF specific values
    IPosition       index( IPosition(1) );
    Array<Int>      sideband( ifshape );
    Array<Float>    channelwidth( ifshape );
    Array<Float>    totalbandwidth( ifshape );
    Array<Double>   frequencyoffset( ifshape );
    

    // Write the frequency groups!
    unsigned int                   i; 
    const fqmap_type&              fqmap( inforef.getFreqGroups() );
    fqmap_type::const_iterator     iter;

    for( i=0, iter=fqmap.begin(); iter!=fqmap.end(); iter++ ) {
        const sbset_type&          subbands( iter->second.subbands );
        sbset_type::const_iterator cursb;
        

        //  Fill in the arrays that describe the subbands/IFs in
        //  this frequency group
        for( index[0]=0, cursb=subbands.begin();
             cursb!=subbands.end(); cursb++, index[0]++ ) {
	    
            // Determine the offset w.r.t. the reference frequency
            frequencyoffset( index ) = cursb->freq - refFreq;

            //  Attempt to derive the channel bandwidth?
            channelwidth( index )    = cursb->chinc;

            // Get the NET_SIDEBAND. 0 => lower, 1 => upper
            // Actually - we relabel everything to be UPPER...
            // let's just warn the user we did it
            // HV: 29-05-2002 - Nobody @JIVE is still quite sure
            //                  how the frequency labelling in
            //                  classic AIPS is. For now, we change
            //                  the LSB labels into USB labels and
            //                  we'll find out if this works fine.
            //                  In the future, when we've *finally*
            //                  found out how to correctly label/organize
            //                  the data, we might have to do more work
            //                  (e.g. reversing LSB data...)
            sideband( index ) = 1;
            if( romsc.spectralWindow().netSideband()(cursb->spw)==0 )
				cout << "idifrequencyGenerator/WARNING!!"
					 << endl
					 << "      Detected LSB in SPW=" << cursb->spw << " FGRP=" << iter->first
					 << " DD=" << cursb->dd
					 << ", LABELLING IT AS USB IN THE FITSFILE!"
					 << endl;

            //  Get the totalbandwidth
            totalbandwidth( index ) = cursb->bw;
        }

        // Fill in the datarecord
        *freqselptr   = iter->second.aipsfreqgrp;
        *sidebandptr  = sideband;
        *totbwptr     = totalbandwidth;
        *ifwidthptr   = channelwidth;
        *iffreqptr    = frequencyoffset;

		// Write the current row
        hduptr->putrow(i, datarecord);
    }

    //  add some extra keywords....
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;

    //  The common keywords
    ek.mk( "TABREV", (Int)1 );
    ek.mk( "OBSCODE", inforef.getObsCode().c_str() );
    ek.mk( "NO_STKD", (Int)inforef.nrPolarizations() );
    ek.mk( "STK_1", inforef.getFirstStokes() );
    ek.mk( "NO_BAND", (Int)inforef.nrIFs() );
    ek.mk( "NO_CHAN", (Int)inforef.nrChannels() );
    ek.mk( "REF_FREQ", refFreq );
    ek.mk( "CHAN_BW", inforef.getChannelwidth() );
    ek.mk( "REF_PIXL", (Float)1 );

    ek.first();
    ek.next();
    kwptr=ek.curr();
    while( kwptr ) {
        hduptr->addExtraKeyword( *kwptr );
        kwptr=ek.next();
    }

    //  Prepare the retval
    result.push_back( hduptr );
    return result;
}



idifrequencyGenerator::~idifrequencyGenerator()
{}

