//     From a MSConversionInfo object, extract the info needed to
//     build a Classic aips GCTAB binary table extension!
//
//      $Id: gctabGenerator.h,v 1.4 2006/03/02 14:21:42 verkout Exp $
#ifndef GCTABGENERATOR_H_INC
#define GCTABGENERATOR_H_INC


#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>


//  The gctab generator is a specific kind of HDU generator....
class gctabGenerator :
    public hduGenerator
{
public:

    //  request to actually generate a gctabHDU!
    virtual hdulist_t generate( const MSConversionInfo& inforef ) const;

    virtual idtype_t  getID( void ) const {
        return "gctab";
    }

    //  Delete the generator
    virtual ~gctabGenerator();

};

// See Registrar.h
DECLARE_GENERATOR(gctabGenerator);

#endif
