// Generate a FITS_IDI GAIN_CURVE table.
//
// Written by: Mark Kettenis
//

#define RCSID(string) \
  static char rcsid[] __attribute__ ((unused)) = string

RCSID("@(#) $Id: idiGainCurveGenerator.cc,v 1.2 2007/07/04 07:54:52 jive_cc Exp $");

// AIPS++ includes.
//

#include <casa/aips.h>
#include <casa/Arrays/Array.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <tables/Tables/ExprNode.h>
#include <tables/Tables/TableIter.h>
#include <casa/Utilities/Assert.h>

// JIVE includes.
//
#include <jive/ms2uvfitsimpl/FITS-IDI.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

#include <jive/ms2uvfitsimpl/Generators/idiGainCurveGenerator.h>

using namespace std;
using namespace casa;

// Implementation of class idiGainCurveGenerator.
//

DEFINE_GENERATOR(idiGainCurveGenerator);

idiGainCurveGenerator::idiGainCurveGenerator() : hduGenerator()
{
}

hdulist_t
idiGainCurveGenerator::generate(const MSConversionInfo& info) const
{
  hdulist_t result;
  const MeasurementSet& ms = info.getMS();
  String obscode = info.getObsCode();
  Int no_stkd = info.nrPolarizations();
  Int stk1 = info.getFirstStokes();
  Int no_band = info.nrIFs();
  Int no_chan = info.nrChannels();
  Float ref_freq = info.getReferenceFrequency(MSConversionInfo::CenterChannel);
  Float chan_bw = info.getChannelwidth();
  Float ref_pixl = 1.0;
  Int no_pol = info.getNumReceptors();
  Int no_tabs = 0;
  Table gc;
  const fqmap_type& groups = info.getFreqGroups();

  if (!ms.keywordSet().isDefined("JIVE_GAINCURVE"))
    return result;
  gc = ms.keywordSet().asTable("JIVE_GAINCURVE");

  // Skip if the JIVE_GAINCURVE table is missing or empty.
  //
  if(gc.isNull() || gc.nrow() == 0)
    return result;

  {
    ROScalarColumn<TableRecord> curvec (gc, "CURVE");

    for (uInt row = 0; row < curvec.nrow(); row++)
      {
	TableRecord curve = curvec(row);
	RecordFieldPtr<String> type(curve, "type");
	Int num_elements;

	if (*type == "TABLE")
	  {
	    RecordFieldPtr<Array<Double> > table(curve, "table");
	    num_elements = max((*table).shape().asVector());
	  }
	else
	  {
	    RecordFieldPtr<Array<Double> > coeffs(curve, "coeffs");
	    num_elements = max((*coeffs).shape().asVector());
	  }

	no_tabs = max(no_tabs, num_elements);
      }
  }

  IPosition bandShape(1, no_band);
  IPosition tableShape(2, no_tabs, no_band);

  // Create table.
  //
  FITSBinaryTableHDU *gcTab =
    new FITSBinaryTableHDU("GAIN_CURVE", 1); // ??? Version?

  {
    // Create column descriptions.
    //
    FITSBinaryColumn antenna_no("ANTENNA_NO", "Antenna number", "1J", "");
    FITSBinaryColumn array ("ARRAY", "Array number", "1J", "");
    FITSBinaryColumn freqid("FREQID", "Frequency setup number", "1J", "");
    FITSBinaryColumn type_1("TYPE_1", TpArrayInt, bandShape, "",
			    "Gain curve types for polarization 1");
    FITSBinaryColumn nterm_1("NTERM_1", TpArrayInt, bandShape, "",
			     "Number of terms or entries for polarization 1");
    FITSBinaryColumn x_typ_1("X_TYP_1", TpArrayInt, bandShape, "",
			     "x value types for polarization 1");
    FITSBinaryColumn y_typ_1("Y_TYP_1", TpArrayInt, bandShape, "",
			     "y value types for polarization 1");
    FITSBinaryColumn x_val_1("X_VAL_1", TpArrayFloat, bandShape, "",
			     "x values for polarization 1"); 
    FITSBinaryColumn y_val_1("Y_VAL_1", TpArrayFloat, tableShape, "",
			     "y values for polarization 1");
    FITSBinaryColumn gain_1("GAIN_1", TpArrayFloat, tableShape, "",
			    "Relative gain values for polarization 1");
    FITSBinaryColumn sens_1("SENS_1", TpArrayFloat, bandShape, "K/JY",
			    "Sensitivities for polarization 1");

    // Add columns.
    //
    gcTab->addColumn(antenna_no);
    gcTab->addColumn(array);
    gcTab->addColumn(freqid);
    gcTab->addColumn(type_1);
    gcTab->addColumn(nterm_1);
    gcTab->addColumn(x_typ_1);
    gcTab->addColumn(y_typ_1);
    gcTab->addColumn(x_val_1);
    gcTab->addColumn(y_val_1);
    gcTab->addColumn(gain_1);
    gcTab->addColumn(sens_1);

    // Create and add optional columns.
    //
    if (no_pol > 1)
      {
	if (no_pol > 2)
	  cerr << "Warning: FITS-IDI has no support for " << no_pol
	       << " polarizations." << endl
	       << "The number of polarizations will be limited to 2."
	       << endl;

	// Create optional column descriptions.
	//
	FITSBinaryColumn type_2("TYPE_2", TpArrayInt, bandShape, "",
				"Gain curve types for polarization 2");
	FITSBinaryColumn nterm_2("NTERM_2", TpArrayInt, bandShape, "",
				 "Number of terms or entries "
				 "for polarization 2");
	FITSBinaryColumn x_typ_2("X_TYP_2", TpArrayInt, bandShape, "",
				 "x value types for polarization 2");
	FITSBinaryColumn y_typ_2("Y_TYP_2", TpArrayInt, bandShape, "",
				 "y value types for polarization 2");
	FITSBinaryColumn x_val_2("X_VAL_2", TpArrayFloat, bandShape, "",
				 "x values for polarization 2");
	FITSBinaryColumn y_val_2("Y_VAL_2", TpArrayFloat, tableShape, "",
				 "y values for polarization 2");
	FITSBinaryColumn gain_2("GAIN_2", TpArrayFloat, tableShape, "",
				"Relative gain values for polarization 2");
	FITSBinaryColumn sens_2("SENS_2", TpArrayFloat, bandShape, "K/JY",
				"Sensitivities for polarization 2");

	// Add optional columns.
	//
	gcTab->addColumn(type_2);
	gcTab->addColumn(nterm_2);
	gcTab->addColumn(x_typ_2);
	gcTab->addColumn(y_typ_2);
	gcTab->addColumn(x_val_2);
	gcTab->addColumn(y_val_2);
	gcTab->addColumn(gain_2);
	gcTab->addColumn(sens_2);
      }

    // Add keywords.
    //
    addCommonKeywords(gcTab, 1, obscode, no_stkd, stk1, no_band, no_chan,
		      ref_freq, chan_bw, ref_pixl);
    addExtraKeyword(gcTab, "NO_POL", no_pol);
    addExtraKeyword(gcTab, "NO_TABS", no_tabs);
  }

  Record data(gcTab->getRowDescription());
  RecordFieldPtr<Int> antenna_no(data, "ANTENNA_NO");
  RecordFieldPtr<Int> array(data, "ARRAY");
  RecordFieldPtr<Int> freqid(data, "FREQID");
  RecordFieldPtr<Array<Int> > type_1(data, "TYPE_1");
  RecordFieldPtr<Array<Int> > nterm_1(data, "NTERM_1");
  RecordFieldPtr<Array<Int> > x_typ_1(data, "X_TYP_1");
  RecordFieldPtr<Array<Int> > y_typ_1(data, "Y_TYP_1");
  RecordFieldPtr<Array<Float> > x_val_1(data, "X_VAL_1");
  RecordFieldPtr<Array<Float> > y_val_1(data, "Y_VAL_1");
  RecordFieldPtr<Array<Float> > gain_1(data, "GAIN_1");
  RecordFieldPtr<Array<Float> > sens_1(data, "SENS_1");
  RecordFieldPtr<Array<Int> > type_2;
  RecordFieldPtr<Array<Int> > nterm_2;
  RecordFieldPtr<Array<Int> > x_typ_2;
  RecordFieldPtr<Array<Int> > y_typ_2;
  RecordFieldPtr<Array<Float> > x_val_2;
  RecordFieldPtr<Array<Float> > y_val_2;
  RecordFieldPtr<Array<Float> > gain_2;
  RecordFieldPtr<Array<Float> > sens_2;
  if (no_pol > 1)
    {
      type_2.attachToRecord(data, "TYPE_2");
      nterm_2.attachToRecord(data, "NTERM_2");
      x_typ_2.attachToRecord(data, "X_TYP_2");
      y_typ_2.attachToRecord(data, "Y_TYP_2");
      x_val_2.attachToRecord(data, "X_VAL_2");
      y_val_2.attachToRecord(data, "Y_VAL_2");
      gain_2.attachToRecord(data, "GAIN_2");
      sens_2.attachToRecord(data, "SENS_2");
    }

  // Iterate over all antennas and feeds that we have gain curve data
  // for.
  //
  Block<String> iterKeys(2);
  iterKeys[0] = "ANTENNA_ID";
  iterKeys[1] = "FEED_ID";
  TableIterator gcIter(gc, iterKeys);

  // Sigh.  Because we have to determine the number of rows for the
  // generated GAIN_CURVE table before we can write the rows, we have
  // to iterate twice.
  //
  // Refer for comments to the second iteration.
  //
  uInt nrow = 0;
  while(!gcIter.pastEnd())
    {
      const Table& gc = gcIter.table();

      fqmap_type::const_iterator curfq = groups.begin();
      while (curfq != groups.end())
	{
	  Bool skip = True;

	  uInt band = 0;
	  const sbset_type &subbands = curfq->second.subbands;
	  sbset_type::const_iterator cursb = subbands.begin();
	  while (cursb != subbands.end())
	    {
	      AlwaysAssertExit((Int)band < no_band);

	      Int id = cursb->spw;
	      Table gcMatch = gc(gc.col("SPECTRAL_WINDOW_ID") == id);

	      if (gcMatch.nrow() == 0)
		gcMatch = gc(gc.col("SPECTRAL_WINDOW_ID") == -1);

	      if (gcMatch.nrow() > 0)
		skip = False;

	      band++;
	      cursb++;
	    }

	  if (!skip)
	    nrow++;
	  curfq++;
	}

      gcIter.next();
    }
  gcIter.reset();

  gcTab->setNumberRows(nrow);

  cout << "Writing FITS-IDI GAIN_CURVE (GN) table." << endl;

  for (uInt row = 0; !gcIter.pastEnd(); gcIter.next())
    {
      const Table& gc = gcIter.table();
      ROScalarColumn<Int> antennac (gc, "ANTENNA_ID");
      ROScalarColumn<Int> feedc (gc, "FEED_ID");

      *antenna_no = antennac(0) + 1;
      *array = -1;		// XXX

      // Iterate over all fequency groups/setups and bands.
      //
      // NOTE: We assume that the list is properly sorted.  All bands
      // within a frequency setup should be grouped together, and
      // should be in the right order.
      //
      fqmap_type::const_iterator curfq = groups.begin();
      while (curfq != groups.end())
	{
	  // Assume we have no system calibration data for any band in
	  // this frequency group/setup.
	  //
	  Bool skip = True;

	  *freqid = curfq->second.aipsfreqgrp;

	  *type_1 = Array<Int>(bandShape, 0);
	  *nterm_1 = Array<Int>(bandShape, 0);
	  *x_typ_1 = Array<Int>(bandShape, 0);
	  *x_val_1 = Array<Float>(bandShape, floatNaN());
	  *y_typ_1 = Array<Int>(bandShape, 0);
	  *y_val_1 = Array<Float>(tableShape, floatNaN());
	  *gain_1 = Array<Float>(tableShape, floatNaN());
	  *sens_1 = Array<Float>(bandShape, floatNaN());
	  if (no_pol > 1)
	    {
	      *type_2 = Array<Int>(bandShape, 0);
	      *nterm_2 = Array<Int>(bandShape, 0);
	      *x_typ_2 = Array<Int>(bandShape, 0);
	      *x_val_2 = Array<Float>(bandShape, floatNaN());
	      *y_typ_2 = Array<Int>(bandShape, 0);
	      *y_val_2 = Array<Float>(tableShape, floatNaN());
	      *gain_2 = Array<Float>(tableShape, floatNaN());
	      *sens_2 = Array<Float>(bandShape, floatNaN());
	    }

	  uInt band = 0;
	  const sbset_type &subbands = curfq->second.subbands;
	  sbset_type::const_iterator cursb = subbands.begin();
	  while (cursb != subbands.end())
	    {
	      AlwaysAssertExit((Int)band < no_band);

	      Int id = cursb->spw;
	      Table gcMatch = gc(gc.col("SPECTRAL_WINDOW_ID") == id);

	      // In its description of the FEED table, the
	      // MeasurementSet definition (version 2.0) says that: "A
	      // value of -1 indicates that a row is valid for all
	      // spectral windows."  This phrase isn't repeated in the
	      // description of the SYSCAL table, but it doesn't hurt
	      // to deal with it here.  So if we didn't find the
	      // spectral band we're looking for, see if there is a
	      // row with SPECTRAL_WINDOW_ID set to -1, and use the
	      // data in that row.
	      //
	      if (gcMatch.nrow() == 0)
		gcMatch = gc(gc.col("SPECTRAL_WINDOW_ID") == -1);

	      if (gcMatch.nrow() > 0)
		{
		  if (gcMatch.nrow() > 1)
		    cerr << "Warning: Multiple gain curves "
			 << "found for the same antenna feed."
			 << endl
			 << "Only the first curve will be used."
			 << endl;

		  ROScalarColumn<Int> spwindowc (gc, "SPECTRAL_WINDOW_ID");
		  ROArrayColumn<Double> gainc (gc, "GAIN");
		  ROScalarColumn<TableRecord> curvec (gc, "CURVE");

		  // Switch polarizations if necessary.
		  //
		  uInt id1 = 0, id2 = 1;
		  uInt antennaId = antennac(0);
		  uInt feedId = feedc(0);
		  uInt spWindowId = spwindowc(0);
		  if (info.switchPolarizations (antennaId, feedId, spWindowId))
		    id1 = 1, id2 = 0;

		  Int type = 0;
		  Int x_typ = 0;
		  Int y_typ = 0;
		  Float x_val = floatNaN();
		  Vector<Double> y_val;
		  Matrix<Double> gain;
		  uInt nterm;
		  Float sens;

		  TableRecord curve = curvec(0);
		  RecordFieldPtr<String> curveType(curve, "type");
		  if (*curveType == "TABLE")
		    {
		      // Tabulated gain curve.
		      type = 1;

		      RecordFieldPtr<String> tabletype(curve, "tabletype");
		      RecordFieldPtr<Array<Double> > table(curve, "table");

		      if(*tabletype == "EL")
			{
			  RecordFieldPtr<Array<Double> > el(curve, "el");
			  y_val = *el;
			  y_typ = 1;
			  gain = *table;
			}
		      else if(*tabletype == "ZA")
			{
			  RecordFieldPtr<Array<Double> > za(curve, "za");
			  y_val = *za;
			  y_typ = 2;
			  gain = *table;
			}
		      else if (*tabletype == "HADEC")
			{
			  RecordFieldPtr<Array<Double> > ha(curve, "ha");
			  RecordFieldPtr<Array<Double> > dec(curve, "dec");
			  x_val = (*dec)(IPosition(1,0));
			  y_val = *ha;
			  x_typ = 4;
			  y_typ = 3;

			  if ((*dec).nelements() > 1)
			    cerr << "Warning: Multiple declinations for "
				 << " HADEC tabulated gain curve found."
				 << endl
				 << "Only the first declination will be used."
				 << endl;

			  // Strip the axis that specifies the declination.
			  //
			  IPosition start(3, 0);
			  IPosition end((*table).endPosition());
			  start[1] = end[1] = 0;
			  IPosition ignore(2, 0, 2);
			  gain = (*table)(start, end).nonDegenerate(ignore);
			}
		      else
			AlwaysAssertExit(!"Table type not supported");
		    }
		  else if (*curveType == "POLY")
		    {
		      // Polynomial gain curve.
		      type = 2;

		      RecordFieldPtr<String> polytype(curve, "polytype");
		      RecordFieldPtr<Array<Double> > coeffs(curve, "coeffs");

		      if (*polytype == "EL")
			{
			  y_typ = 1;
			  gain = *coeffs;
			}
		      else if (*polytype == "ZA")
			{
			  type = 2;
			  y_typ = 2;
			  gain = *coeffs;
			}
		      else if (*polytype == "SPHER")
			{
			  // FITS-IDI makes a distinction between
			  // polynomial and spherical harmonic gain
			  // curves.
			  type = 3;

			  x_typ = 5;
			  y_typ = 3;
			  gain = *coeffs;
			}
		      else if (*polytype == "HA")
			{
			  RecordFieldPtr<Array<Double> > decs(curve, "decs");
			  x_val = (*decs)(IPosition(1,0));
			  x_typ = 4;
			  y_typ = 3;

			  if ((*decs).nelements() > 1)
			    cerr << "Warning: Multiple declinations for HA "
				 << "gain curve found."
				 << endl
				 << "Only the first declination will be used."
				 << endl;

			  // Strip the axis that specifies the declination.
			  //
			  IPosition start(3, 0);
			  IPosition end((*coeffs).endPosition());
			  start[1] = end[1] = 0;
			  IPosition ignore(2, 0, 2);
			  gain = (*coeffs)(start, end).nonDegenerate(ignore);
			}
		      else
			AlwaysAssertExit(!"Polynomial type not supported");
		    }
		  else
		    AlwaysAssertExit(!"Unknown gain curve type");

		  nterm = gain.column(id1).nelements();
		  sens = gainc(0)(IPosition(1, id1));
		  (*type_1)(IPosition(1, band)) = type;
		  (*nterm_1)(IPosition(1, band)) = nterm;
		  (*x_typ_1)(IPosition(1, band)) = x_typ;
		  (*x_val_1)(IPosition(1, band)) = x_val;
		  (*y_typ_1)(IPosition(1, band)) = y_typ;
		  (*sens_1)(IPosition(1, band)) = sens;
		  for (uInt i = 0; i < nterm; i++)
		    {
		      DebugAssertExit(i < (uInt)no_tabs);
		      (*gain_1)(IPosition(2, i, band)) = gain(i, id1);
		      if (type == 1)
			(*y_val_1)(IPosition(2, i, band)) = y_val[i];
		    }

		  if (no_pol > 1)
		    {
		      nterm = gain.column(id2).nelements();
		      sens = gainc(0)(IPosition(1, id2));
		      (*type_2)(IPosition(1, band)) = type;
		      (*nterm_2)(IPosition(1, band)) = nterm;
		      (*x_typ_2)(IPosition(1, band)) = x_typ;
		      (*x_val_2)(IPosition(1, band)) = x_val;
		      (*y_typ_2)(IPosition(1, band)) = y_typ;
		      (*sens_2)(IPosition(1, band)) = sens;
		      for (uInt i = 0; i < nterm; i++)
			{
			  DebugAssertExit(i < (uInt)no_tabs);
			  (*gain_2)(IPosition(2, i, band)) = gain(i, id2);
			  if (type == 1)
			    (*y_val_2)(IPosition(2, i, band)) = y_val[i];
			}
		    }

		  skip = False;	// We found some data.
		}

	      band++;
	      cursb++;
	    }

	  
	  // Skip this frequency group/setup if we didn't find any
	  // gain curve data.
	  //
	  if (!skip)
	    {
	      gcTab->putrow(row, data);
	      row++;
	    }
	  curfq++;
	}
    }

  // Store return value.
  //
  result.push_back(gcTab);
  return result;
}

idiGainCurveGenerator::~idiGainCurveGenerator()
{
}
