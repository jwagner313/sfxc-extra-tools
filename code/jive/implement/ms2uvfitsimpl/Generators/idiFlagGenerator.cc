// Generate a FITS-IDI FLAG (FL) table.
//
// Written by: Mark Kettenis
//

#define RCSID(string) \
  static char rcsid[] __attribute__ ((unused)) = string

RCSID("@(#) $Id: idiFlagGenerator.cc,v 1.2 2007/07/04 07:54:51 jive_cc Exp $");

// Standard includes.
//
#include <cstdlib>

// AIPS++ includes.
//
#include <casa/aips.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <casa/Utilities/Assert.h>
#include <measures/Measures/MEpoch.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <ms/MeasurementSets/MSFlagCmd.h>
#include <ms/MeasurementSets/MSFlagCmdColumns.h>

// JIVE includes.
//
#include <jive/ms2uvfitsimpl/FITS-IDI.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

#include <jive/ms2uvfitsimpl/Generators/idiFlagGenerator.h>

using namespace std;
using namespace casa;

// Implementation of class idiFlagGenerator.
//

DEFINE_GENERATOR(idiFlagGenerator);

idiFlagGenerator::idiFlagGenerator() : hduGenerator()
{
}

hdulist_t
idiFlagGenerator::generate(const MSConversionInfo& info) const
{
  hdulist_t result;
  const MeasurementSet& ms = info.getMS();
  const ROMSColumns msc(ms);
  String obscode = info.getObsCode();
  Int no_stkd = info.nrPolarizations();
  Int stk1 = info.getFirstStokes();
  Int no_band = info.nrIFs();
  Int no_chan = info.nrChannels();
  Float ref_freq = info.getReferenceFrequency(MSConversionInfo::CenterChannel);
  Float chan_bw = info.getChannelwidth();
  Float ref_pixl = 1.0;
  const MSFlagCmd& fc = ms.flagCmd();
  ROMSFlagCmdColumns fcc(fc);
  IPosition bandShape(1, no_band);

  // Skip if the FLAG_CMD table is missing or empty.
  //
  if(fc.isNull() || fc.nrow() == 0)
    return result;

  // Create table.
  //
  FITSBinaryTableHDU *flagTab =
    new FITSBinaryTableHDU("FLAG", 1); // ??? Version?

  {
    // Create column descriptions.
    //
    FITSBinaryColumn source_id("SOURCE_ID", "Source ID number", "1J", "");
    FITSBinaryColumn array ("ARRAY", "Array number", "1J", "");
    FITSBinaryColumn ants("ANTS", "Antenna numbers", "2J", "");
    FITSBinaryColumn freqid("FREQID", "Frequency setup number", "1J", "");
    FITSBinaryColumn timerang("TIMERANG", "Time Range", "2E", "DAYS");
    FITSBinaryColumn bands("BANDS", TpArrayInt, bandShape, "", "Band Flags");
    FITSBinaryColumn chans("CHANS", "Channel Range", "2J", "");
    FITSBinaryColumn pflags("PFLAGS", "Polarizations flags", "4J", "");
    FITSBinaryColumn reason("REASON", "Reason for flag", "40A", "");
    FITSBinaryColumn severity("SEVERITY", "Severity code", "1J", "");

    // Add columns.
    //
    flagTab->addColumn(source_id);
    flagTab->addColumn(array);
    flagTab->addColumn(ants);
    flagTab->addColumn(freqid);
    flagTab->addColumn(timerang);
    flagTab->addColumn(bands);
    flagTab->addColumn(chans);
    flagTab->addColumn(pflags);
    flagTab->addColumn(reason);
    flagTab->addColumn(severity);

    // Add keywords.
    //
    addCommonKeywords(flagTab, 2, obscode, no_stkd, stk1, no_band, no_chan,
		      ref_freq, chan_bw, ref_pixl);
  }

  flagTab->setNumberRows(fcc.nrow());

  Record data(flagTab->getRowDescription());
  RecordFieldPtr<Int> source_id(data, "SOURCE_ID");
  RecordFieldPtr<Int> array(data, "ARRAY");
  RecordFieldPtr<Array<Int> > ants(data, "ANTS");
  RecordFieldPtr<Int> freqid(data, "FREQID");
  RecordFieldPtr<Array<Float> > timerang(data, "TIMERANG");
  RecordFieldPtr<Array<Int> > bands(data, "BANDS");
  RecordFieldPtr<Array<Int> > chans(data, "CHANS");
  RecordFieldPtr<Array<Int> > pflags(data, "PFLAGS");
  RecordFieldPtr<String> reason(data, "REASON");
  RecordFieldPtr<Int> severity(data, "SEVERITY");

  // Default values.
  //
  *source_id = 0;			// All sources.
  *array = 0;				// All arrays.
  *ants = Vector<Int>(2, 0);		// All antennas.
  *freqid = -1;				// All frequency setups.
  *timerang = Vector<Float>(2, 0);
  *bands = Array<Int>(bandShape, 1); 	// All bands.
  *chans = Vector<Int>(2, 0);		// All channels.
  *pflags = Vector<Int>(4, 1);		// All polarizations.
  *severity = -1;			// No severity level assigned.

  cout << "Writing FITS-IDI FLAG (FL) table." << endl;

  for (uInt row = 0; row < fcc.nrow(); row++)
    {
      Double time = info.getIDITime(fcc.timeMeas()(row));
      Double interval = fcc.intervalQuant()(row).getValue("d");
      (*timerang)(IPosition(1, 0)) = time - interval / 2;
      (*timerang)(IPosition(1, 1)) = time + interval / 2;
      *reason = fcc.reason()(row);

      // XXX This really should be replaced with a genuine parser if
      // we're going to support more ways of flagging data than just
      // by antenna.
      //
      Regex antenna("ANTENNA1 == [0-9]+ \\|\\| ANTENNA2 == [0-9]+");
      const String& command = fcc.command()(row);
      if (command.matches(antenna))
	{
	  uInt id1 = ::atoi(command.c_str() + 11);
	  uInt id2 = ::atoi(command.c_str() + command.find("ANTENNA2") + 11);
	  
	  if (id1 == id2)
	    {
	      (*ants)(IPosition(1, 0)) = id1 + 1;
	      flagTab->putrow(row, data);
	      continue;
	    }
	}

      cerr << "Warning: Unrecognized command: " << command << endl;
    }

  // Store return value.
  //
  result.push_back(flagTab);
  return result;
}

idiFlagGenerator::~idiFlagGenerator()
{
}
