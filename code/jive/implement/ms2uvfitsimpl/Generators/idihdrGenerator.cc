//
//  Implementation of the IDI Header generator class....
//
//  Author:  Harro Verkouter,  16-09-1999
//  
//  $Id: idihdrGenerator.cc,v 1.9 2011/01/06 14:15:55 jive_cc Exp $
//
//  $Log: idihdrGenerator.cc,v $
//  Revision 1.9  2011/01/06 14:15:55  jive_cc
//  Added tConvert version to IDI history.
//
//  Revision 1.8  2006/11/22 14:14:12  jive_cc
//  HV: Removed unused parameters otherwise cannot compile with -Werror
//
//  Revision 1.7  2006/03/02 14:21:43  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.6  2006/01/13 11:35:46  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.5  2004/09/10 14:09:57  kettenis
//  Remove trailing whitespace.
//
//  Revision 1.4  2004/08/25 06:04:36  verkout
//  HV: Fiddled with templates. They all get instantiated automatically. As a result, the source code for the templates must be visible compiletime
//
//  Revision 1.3  2004/01/05 15:28:39  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.2  2001/05/30 11:53:43  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//  HV: Imported aips++ implement-stuff
//
//
//
#include <jive/ms2uvfitsimpl/Generators/idihdrGenerator.h>

//
//  AIPS++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSIDIHeader.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

using namespace casa;

DEFINE_GENERATOR(idihdrGenerator);

//
//  This function performs all the dirty work. It creates a dummy hdu
//  object! It is the callers response to delete the object if he/she
//  no longer desires to use the object!
//
hdulist_t idihdrGenerator::generate( const MSConversionInfo& m) const
{
    hdulist_t      result;
 
    cout << "Making IDI-Header" << endl;
    
    FITSIDIHeader* head = new FITSIDIHeader();
    FitsKeywordList kwlist;
    String h = m.getTConvertVersion();
    kwlist.history(h.c_str());

    kwlist.first(); 
    kwlist.next();
    FitsKeyword* kwptr = kwlist.curr();
    while (kwptr) {
	 head->addExtraKeyword(*kwptr);
	 kwptr = kwlist.next();
    }
	 
    //head->setExtraKeywords(kwlist);  // protected; doesn't work

    result.push_back( head );
    return result;
}


//
//  The d'tor: nothing to destruct at this moment.....
//
idihdrGenerator::~idihdrGenerator()
{
}
