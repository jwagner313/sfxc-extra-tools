//
//  Implementation of class maingroupGenerator
//
//
//
//   Author:   Harro Verkouter
//             05-06-1998
// 
//   $Id: maingroupGenerator.cc,v 1.13 2007/03/13 09:18:21 jive_cc Exp $
//
//   $Log: maingroupGenerator.cc,v $
//   Revision 1.13  2007/03/13 09:18:21  jive_cc
//   HV: - cosmetic changes
//       - Reworked internals of MSConversionInfo
//         * uses less pointers
//         * less contstraints: there can be
//           different bandwidth/IF and hence
//           different channelwidth/IF.
//           Only nr-of-IFs/freqgroup and
//           nr-of-channels/IF need to be identical
//         * Caches the re-ordering of polarization products
//           as they may have to be re-ordered in order to
//           follow the FITS-ordering
//         * Spectralwindow re-ordering and mapping
//           now done via neato STL auto-sorting
//           containers. Resulting in more readable and
//           more reliable code.
//
//   Revision 1.12  2006/03/02 14:21:43  verkout
//   HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//   Revision 1.11  2006/02/17 12:41:27  verkout
//   HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//
//   Revision 1.10  2006/02/10 08:53:46  verkout
//   HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//   Revision 1.9  2006/01/13 11:35:47  verkout
//   HV: CASAfied version of the implement
//
//   Revision 1.8  2004/08/25 06:04:36  verkout
//   HV: Fiddled with templates. They all get instantiated automatically. As a result, the source code for the templates must be visible compiletime
//
//   Revision 1.7  2004/01/05 15:28:44  verkout
//   HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//   Revision 1.6  2003/09/12 07:37:43  verkout
//   HV: Code had to be made gcc3.* compliant.
//
//   Revision 1.5  2003/02/14 15:48:05  verkout
//   HV: * trial/FITS/FITSUtil was removed.
//       * std::string does not have automatic conversion to
//         (const char*). Fits-stuff does need (const char*)
//         so had to add the odd .c_str() here and there.
//
//   Revision 1.4  2002/08/06 09:09:54  verkout
//   HV: Fixed bug where visibilities, flagged by data_handler (ie given negative weights) would get in again becaus of negating again. Had to ascertain that flagged visibilities *always* got a negative weight.
//
//   Revision 1.3  2001/05/30 11:53:50  verkout
//   HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//   Revision 1.2  2000/03/21 08:51:22  verkout
//   HV: By popular demand added the RECEIVER=JIVE keyword to the generators of the main data HDUs
//
//   Revision 1.1.1.1  2000/03/20 15:16:47  verkout
//   HV: Imported aips++ implement-stuff
//
//
//  My includes
//
#include <jive/ms2uvfitsimpl/Generators/maingroupGenerator.h>

//
//  AIPS++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSPrimaryGroup.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordFieldId.h>
#include <casa/Containers/RecordField.h>
#include <casa/Arrays/Vector.h>
#include <casa/Arrays/Matrix.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <ms/MeasurementSets/MSAntennaColumns.h>
#include <fits/FITS/fits.h>
#include <fits/FITS/FITSDateUtil.h>
#include <tables/Tables/TableDesc.h>
#include <tables/Tables/TableRecord.h>
#include <casa/Quanta/MVTime.h>
#include <measures/Measures/Stokes.h>
#include <casa/Utilities/GenSort.h>
#include <tables/Tables/TableIter.h>
#include <jive/Utilities/jexcept.h>

#include <ms/MeasurementSets/MSField.h>
#include <ms/MeasurementSets/MSObservation.h>
#include <ms/MeasurementSets/MSPolarization.h>
#include <ms/MeasurementSets/MSSpectralWindow.h>
#include <ms/MeasurementSets/MSDataDescription.h>
#include <casa/Arrays/ArrayMath.h>


#include <jive/ms2uvfitsimpl/MSConversionInfo.h>

#include <jive/Utilities/streamutil.h>

#include <sstream>
#include <fstream>


using namespace std;
using namespace casa;


DEFINE_GENERATOR(maingroupGenerator);


#define KOUT( a )  (cout << #a << "=" << a << endl)


// Translate time (in MJD seconds) to JD number and dayfraction
void maingroupGenerator::timeToDay( Double& day, Double& dayfraction, Double time ) {
    const Double  JDofMJD0( 2400000.5 );

	// First, go from MJD(s) to MJD(d)
	time /= C::day;

	// Now split into whole days and day fraction
	day         =  ::floor(time);
	dayfraction =  time - ::floor(time);

	//  Only thing left is to go from whole 
	//  MJD-days to whole JD days i.e. add offset
	day         += JDofMJD0;

    //time /= C::day; // now in days;
    //time += JDofMJD0; // now in JD
    
    //day = (Int)::floor(time);
    //dayfraction = time - ::floor(time);
	return;
}



//  This function performs all the dirty work. It creates a dummy hdu
//  object! It is the callers response to delete the object if he/she
//  no longer desires to use the object!
hdulist_t maingroupGenerator::generate( const MSConversionInfo& inforef ) const {
    cout << "Making Maingroup" << endl;

    //  The idea is that we first attempt to gather info, then create
    //  the axes/parameters, configure the hdu, out of speed
    //  considerations create the ROColumnObjects for the columns we
    //  need to access, set up RecordFieldPtrs for fields we want to
    //  write and then: Off we go!

    //  Some general info, like ra/dec, sourcename
    Bool                   usewavelength( False );
    Bool                   hasWeightSpectrum( False );
    Bool                   delSrcdataptr;
    Bool                   delWeightptr;
    Bool                   delFlagptr;
    Bool                   delDstdataptr;
	Double                 day;
    Double                 epoche( 2000.0 );  // NOTE: default epoch=J2000!
    Double                 dayFraction;
    Double                 wavelength( C::c/inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    String                 objectname;
    String                 bunit( "UNCALIB" );
    String                 telescope;
    String                 instrument;
    String                 uuname;
    String                 vvname;
    String                 wwname;
    MSField                fieldTable( inforef.getMS().field() );
    hdulist_t              result;
    const Double           oneOverC( 1.0 / C::c );
    Vector<Double>         radec;
    Vector<Double>         uvw( 3 );
    MSObservation          observation( inforef.getMS().observation() );
    MSPolarization         polarization( inforef.getMS().polarization() );
    MSSpectralWindow       spwinTable( inforef.getMS().spectralWindow() );
    FITSPrimaryGroup*      newhdu( new FITSPrimaryGroup(TpFloat) );
    MSDataDescription      datadescription( inforef.getMS().dataDescription() );

    //  Do create column references for all the columns that we need to access
    ROArrayColumn<Double>  indir( fieldTable, MSField::columnName(MSField::PHASE_DIR) );
    ROScalarColumn<String> inname( fieldTable, MSField::columnName(MSField::NAME) );
    ROArrayColumn<Complex> datacolumn( inforef.getMS(), inforef.getDataColumnName() );
    ROArrayColumn<Int>     stokesTypes( polarization,
					MSPolarization::columnName(MSPolarization::CORR_TYPE) );
    ROArrayColumn<Float>   weight( inforef.getMS(), MS::columnName(MS::WEIGHT) );
    ROArrayColumn<Float>   weightspectrum;
    ROScalarColumn<Bool>   rowflag( inforef.getMS(), MS::columnName(MS::FLAG_ROW) );
    ROArrayColumn<Bool>    dataflag( inforef.getMS(), MS::columnName(MS::FLAG) );
    ROArrayColumn<Double>  uvwcol( inforef.getMS(), MS::columnName(MS::UVW) );
    ROScalarColumn<Double> time( inforef.getMS(), MS::columnName(MS::TIME) );
    ROScalarColumn<Int>    ant1( inforef.getMS(), MS::columnName(MS::ANTENNA1) );
    ROScalarColumn<Int>    ant2( inforef.getMS(), MS::columnName(MS::ANTENNA2) );
    ROScalarColumn<Int>    spwinid( datadescription,
				    MSDataDescription::columnName(MSDataDescription::SPECTRAL_WINDOW_ID) );
    ROScalarColumn<Int>    polid( datadescription,
				  MSDataDescription::columnName(MSDataDescription::SPECTRAL_WINDOW_ID) );
    ROScalarColumn<Int>    frqgrp( spwinTable,
				   MSSpectralWindow::columnName(MSSpectralWindow::FREQ_GROUP) );
    ROScalarColumn<Int>    fieldid( inforef.getMS(), MS::columnName(MS::FIELD_ID) );
    ROScalarColumn<Int>    ddid( inforef.getMS(), MS::columnName(MS::DATA_DESC_ID) );
    ROScalarColumn<Double> exposure( inforef.getMS(), MS::columnName(MS::EXPOSURE) );
    ROScalarColumn<String> scopename( observation,
				      MSObservation::columnName(MSObservation::TELESCOPE_NAME) );

    
    //  Lets see if we have a weightspectrum. If so, we need to attach
    //  the ROColumn..object to the column in the MS. Otherwise we
    //  leave it un-attached.
    hasWeightSpectrum = inforef.getMS().tableDesc().isColumn(MS::columnName(MS::WEIGHT_SPECTRUM));

    if( hasWeightSpectrum )
		weightspectrum.attach( inforef.getMS(), MS::columnName(MS::WEIGHT_SPECTRUM) );

    //  Do get the RA and DEC and sourcename from the MS
	radec = Vector<Double>(2);
    if( fieldTable.nrow() && indir.isDefined(0) ) {
        radec( 0 ) = indir(0)( IPosition(2,0,0) );
		radec( 1 ) = indir(0)( IPosition(2,1,0) );
    } else {
		cout << "++++ No source pos found. Defaulting to (0,0)" << endl;
    
        radec = (Double)0.0;
    }
    
    if( fieldTable.nrow() && inname.isDefined(0) ) {
        objectname = inname(0);
    } else {
        ostringstream defname;
        
        defname << "SOURCE_" << 0;

		cout << "++++ No source name found, defaulting to `" 
			 << defname.str() << "'" << endl;
        objectname = defname.str();
    }

    //  Check if the DATE-OBS can be constructed validly.
    String            date;
    String            timesys;
    MVTime            actualobstime;

    try {
        actualobstime=inforef.observingDate();
        FITSDateUtil::toFITS( date, timesys, actualobstime );
    }
    catch( const std::exception& x ) {
        cerr << x.what() << endl;

        actualobstime = MVTime( Time() );

        FITSDateUtil::toFITS( date, timesys, actualobstime );
		cout << "++++ DATE-OBS could not be represented in FITS, defaulting to "
    		 << date << endl;
    }

    //  Attempt to determine the epoch for the coordinates
    Bool               foundEpoch( False );
    String             mref( "MEASURE_REFERENCE" );
    const TableRecord& fkeys( indir.keywordSet() );

    if( fkeys.isDefined(mref) && fkeys.dataType(mref)==TpString ) {
        //  Ok. Get the reference:
		String     dirtype;

		fkeys.get( mref, dirtype );
        
		if( dirtype.contains("2000") ) {
            epoche     = 2000.0;
		    foundEpoch = True;
		} else if( dirtype.contains("1950") ) {
            epoche     = 1950.0;
		    foundEpoch = True;
		}
    }
    //  If not found, default to 2000.0
    if( !foundEpoch ) {
		cout << "++++ Couldn't find MS epoch. Assuming J2000." << endl;
		epoche = 2000.0;
    }

    
    //  Get the telescope and instrument strings:
    if( observation.nrow()>0 ) {
        telescope  = scopename( 0 );
        instrument = scopename( 0 );
    } else {
        ostringstream  defarrname;
        
        defarrname << "ARRAY_" << 0;
        
		cout << "++++ No arrayname found, defaulting to `" 
			 << defarrname.str() << "'" << endl;
        
        telescope  = defarrname.str();
        instrument = defarrname.str();
    }
    
    //  See if we have to deviate from the default unit ("UNCALIB")
    if( datacolumn.keywordSet().isDefined("UNIT") && 
		datacolumn.keywordSet().dataType("UNIT")==TpString ) {
		datacolumn.keywordSet().get("UNIT", bunit);
		bunit = upcase(bunit);
    }

    // Get mappings + iterators into those for easy unmapping/grouping
    // stuff
    const fqmap_type&           fqmap( inforef.getFreqGroups() );
    const product_map&          prodMap( inforef.getProductMap() );
    fqmap_type::const_iterator  curfq;

    // These are the axes that describe the datastructure
    FITSAxis          ax_cmplx( "COMPLEX", "Re/Im/Wght", 3 );
    FITSAxis          ax_stokes( "STOKES", "Polarizaton axis", 
                              inforef.nrPolarizations(), // length of axis
                              1.0, // refpixl 
                              inforef.getFirstStokes()*1.0, // value at refpxl
                              inforef.stokesIncrement()*1.0 /* increment along this axis*/ );
    FITSAxis          ax_freq( "FREQ", "Frequency axis", inforef.nrChannels(), 1.0,
                            inforef.getReferenceFrequency(MSConversionInfo::CenterChannel),
                            inforef.getChannelincrement() );
    FITSAxis          ax_if( "IF", "IF axis", inforef.nrIFs(), 1.0, 1.0 );
    FITSAxis          ax_ra( "RA", "Position axis (historically)", 1, 1.0, radec(0)/C::degree );
    FITSAxis          ax_declination( "DEC", "Position axis (historically)", 1, 1.0, radec(1)/C::degree );

    
    // Ok. "Let's doohooohoo it!" (ref. Lord Flasheart in `BlackAdder
    // goes forth!')
    newhdu->addAxis( ax_cmplx );
    newhdu->addAxis( ax_stokes );
    newhdu->addAxis( ax_freq );
    newhdu->addAxis( ax_if );
    newhdu->addAxis( ax_ra );
    newhdu->addAxis( ax_declination );
    
    //  Decide on randomparameter-uvw-units
    Double    uvwscale;
    
    if( usewavelength ) {
		uuname   = "UU-L-";
		vvname   = "VV-L-";
		wwname   = "WW-L-";
		uvwscale = wavelength;
    } else {
		//  Unit is time
		uuname   = "UU--";
		vvname   = "VV--";
		wwname   = "WW--";
		uvwscale = 1.0;
    }
    
    //  The data is preceded by parameters:
    newhdu->addParameter( FITSParameter(uuname, "", uvwscale) );
    newhdu->addParameter( FITSParameter(vvname, "", uvwscale) );
    newhdu->addParameter( FITSParameter(wwname, "", uvwscale) );
    newhdu->addParameter( FITSParameter("DATE", "Day number") );
    newhdu->addParameter( FITSParameter("DATE", "Day fraction") );
    newhdu->addParameter( FITSParameter("BASELINE") );
    newhdu->addParameter( FITSParameter("FREQSEL") );
    newhdu->addParameter( FITSParameter("SOURCE") );
    newhdu->addParameter( FITSParameter("INTTIM") );

    //  This is the record we're going to use to pass the data to the
    //  FITS-stuff!
    Record       datarecord( newhdu->getGroupDescription() );

    //  Make recordfieldptrs
    RecordFieldPtr<Float>         uptr( datarecord, uuname );
    RecordFieldPtr<Float>         vptr( datarecord, vvname );
    RecordFieldPtr<Float>         wptr( datarecord, wwname );
    RecordFieldPtr<Float>         dayptr( datarecord, 3 );
    RecordFieldPtr<Float>         dayfracptr( datarecord, 4 );
    RecordFieldPtr<Float>         blptr( datarecord, "BASELINE" );
    RecordFieldPtr<Float>         freqselptr( datarecord, "FREQSEL" );
    RecordFieldPtr<Float>         sourceidptr( datarecord, "SOURCE" );
    RecordFieldPtr<Float>         inttimptr( datarecord, "INTTIM" );
    //  Tricky......
    RecordFieldPtr<Array<Float> > datamatrixptr( datarecord, 9 );

    //  Tell the HDU how many groups there are.
    //
    //  [[ASSUMPTION]] In the MS, every row contains all the data for
    //  one IF out of one freq-group. Hence one freqgroup makes up
    //  nrIF rows and one 'visibility record' will consist of
    //  one such a block.
    //
    //  NOTE: the nr of rows in the maintable isn't the number of
    //  groups; it's nrrows/nrIFs -> for every group there are nrIF
    //  rows in the maintable, each containing a matrix of nrchannels
    //  X nrpolarizations
    //
    // HV: 17-03-2000 - Removed the following section. On *large* MSs this takes
    //                  a *lot* of time (however it ensures that the correct nr
    //                  nr of groups is determined...) so let's make a dumb guess
    //                  at the nr-of-groups and add/skip them on the fly... :-(
    //                  ["following section" would be iterating over the main table,
    //                    actually counting how many groups should be written]
    // HV: 09-03-2007 - Actually, for FITS, we write all the data for all IFs for
    //                  all freq-groups into a single row. As a result, our estimate
    //                  must be "nrow()/(nIF * nFreqGroup)"
    uInt    nrgroups( inforef.getMS().nrow()/(inforef.nrIFs()*inforef.getNrOfFreqGroups()) );

    cout << "++++ Guessing that " << nrgroups << " visibilities are present" << endl;
    newhdu->setNumberOfGroups( nrgroups );

    // Do get access to the raw storage of the arrays for speed reasons
    // for source data, weight, flags and the full block for all the 
    // data for all the polarizationproducts for all channels for all
    // IFs for a given frequency-group. Claro? ;)

    //  The source data
    IPosition          srcindex( IPosition(2,0) );
    Array<Complex>     srcdata( IPosition(2, inforef.nrPolarizations(), inforef.nrChannels()) );
    const Complex*     srcdataptr( srcdata.getStorage(delSrcdataptr) );

    //  The weight data
    Array<Float>       weightdata( IPosition(2, inforef.nrPolarizations(), inforef.nrChannels()) );
    const Float*       weightdataptr( weightdata.getStorage(delWeightptr) );

    //  The flags
    Array<Bool>        flagdata( IPosition(2, inforef.nrPolarizations(), inforef.nrChannels()) );
    const Bool*        flagdataptr( flagdata.getStorage(delFlagptr) );

    //  The destination data
    IPosition          dstindex( IPosition(6,0) );
    const Float*       destdataptr( (*datamatrixptr).getStorage(delDstdataptr) );

    //  Loop and go!
    uInt               nrchannels( inforef.nrChannels() );
    uInt               nrpolarizations( inforef.nrPolarizations() );
    Bool               rowFlag;

    //  Ok. We start anew. The dest'matrix' and srcmatrix have
    //  different shapes. The source-matrix is ( (nchannel *
    //  npolcombis) Complex ), whilst the dest'matrix' is ( (nrifs *
    //  nrchannels * nrpolcombis * 3) Float )
    //
    //  What we do is the following: we resort the maintable, based on
    //  time, baseline. Then we start iterating over the maintable in
    //  steps of chunks of successive 'time,baseline'-pairs.
    Block<String>     iternames( 4 );

    iternames[0]="TIME";
    iternames[1]="ANTENNA1";
    iternames[2]="ANTENNA2";
    iternames[3]="FIELD_ID";
    
    //  Now we make a tableiterator. We iterate over equal times and
    //  baseline. In that way, we get a 'table' (reference) for every
    //  time/baseline pair. 
    uLong             groupcnt( 0 );
    TableIterator     maintableiterator(inforef.getMS(), iternames, TableIterator::Ascending);
    
    
    // Reset the table iterator to start
    maintableiterator.reset();
    
    //  Loop until either of the following conditions hold;
    //
    //  either no more groups or we've reached the number of groups
    //  that were allocated for this HDU....
    typedef map<int, int> dd2row_type;

    while( !maintableiterator.pastEnd() && groupcnt<nrgroups ) {
		//  Get the current table....
		Table        grouptable( maintableiterator.table() );
        dd2row_type  dd2row;

		// The datasize of one IF in this group (in Float's)
		uLong        IFsize( nrchannels*nrpolarizations*3 );

		//  Attach the column objects to the grouptable...
		datacolumn.attach( grouptable, inforef.getDataColumnName() );
		rowflag.attach( grouptable, MS::columnName(MS::FLAG_ROW) );
		dataflag.attach( grouptable, MS::columnName(MS::FLAG) );
		weight.attach( grouptable, MS::columnName(MS::WEIGHT) );
		ddid.attach( grouptable, MS::columnName(MS::DATA_DESC_ID) );

		if( hasWeightSpectrum )
		    weightspectrum.attach( grouptable, MS::columnName(MS::WEIGHT_SPECTRUM) );
		else
		    weightspectrum.reference( ROArrayColumn<Float>() );
	
		uvwcol.attach( grouptable, MS::columnName(MS::UVW) );
		time.attach( grouptable, MS::columnName(MS::TIME) );
		ant1.attach( grouptable, MS::columnName(MS::ANTENNA1) );
		ant2.attach( grouptable, MS::columnName(MS::ANTENNA2) );
		fieldid.attach( grouptable, MS::columnName(MS::FIELD_ID) );
		exposure.attach( grouptable, MS::columnName(MS::EXPOSURE) );

        // Build a data-description to row# mapping
        for( unsigned int i=0; i<grouptable.nrow(); i++ )
            dd2row.insert( make_pair(ddid(i), i) );

		//  Ok. Every row in the 'grouptable' contains all the data 
		//  for a specific IF/FREQGRP...
		//
		//  So what we do is: for every freq-group we get all the IFs
		//  out and stuff them in teh FITSfile...
		//
		//  for every datadescriptionid we have to look up what the
		//  freq-grp id is!
        //  NOTE: beware of not writing beyond the allocated nr-of-groups
        for( curfq=fqmap.begin(); curfq!=fqmap.end() && groupcnt<nrgroups; curfq++ ) {
			uInt                       ifcnt;
            const sbset_type&          subbands( curfq->second.subbands );
            sbset_type::const_iterator cursb;

		    srcdata          = Complex(0.0f, 0.0f);
		    weightdata       = 0.0f;
		    flagdata         = False;
		    (*datamatrixptr) = 0.0f;
	    
		    //  Loop over all IFs
		    for( cursb=subbands.begin(), ifcnt=0; cursb!=subbands.end(); cursb++, ifcnt++ ) {
				//  Attempt to locate the current DataDescription in the grouptable
                dd2row_type::const_iterator  rowptr( dd2row.find(cursb->dd) );

                //  HV: I had this in as an error, but, after falling over
                //      and re-thinking the lot, it is not *at all* necessary
                //      that *every* vibility point contains data for *all*
                //      frequency-setups... so if we encounter this condition
                //      it must mean that for the current FREQGRP/IF combi
                //      no data was found.
                //  
                //      So continue with the next IF
                if( rowptr==dd2row.end() )
                    continue;
				//  Now we can get the stuff we need, like:
				// DATA matrix
				datacolumn.get( rowptr->second, srcdata );

				//  HV: 19-10-2000 - Denise Gabuzda uncovered cross polarization
				//                   problems with our data. It appeared that we
				//                   had some minus-signs left out (i.e. we didn't do
				//                   the math correctly; Denise did...). For
				//                   mapping this was not a problem; for parallactic
				//                   angle stuff is was disastrous :/
				//                   So it was decided that in the UVF files:
				//
				//                   1) the sign of the phases should be inverted
				//                   2) the signs of all U,V and W should be inverted
				//  This is step 1)
				srcdata = conj( srcdata );

				// FLAG_ROW   
				rowFlag = rowflag( rowptr->second );
	    
				// FLAG
				dataflag.get( rowptr->second, flagdata );
	    
				//  WEIGHT_SPECTRUM takes precedence over WEIGHT...
				if( hasWeightSpectrum ) {
					//  Load the weight-spectrum in the array
				    weightspectrum.get( rowptr->second, weightdata );
				} else {
					//  Well now: in MSv2, the WEIGHT-column has dimension
					//  NPolarizations -> one weight/polarizationproduct
					//
					//  Hence we have to replicate these for the
					//  channels, since wightdata has shape (npol x nchan)
					Array<Float>    twt = weight(rowptr->second);

					for( uInt pcnt=0; pcnt<nrpolarizations; pcnt++ ) {
                        Float wt = twt( IPosition(1, pcnt) );
						for( uInt ccnt=0; ccnt<nrchannels; ccnt++ )
							weightdata(IPosition(2,pcnt,ccnt)) = wt;
                    }
				}

				// We need to re-sort the data, since in the MS the data
				// matrix is channels x polarizations, whereas in the FITS
				// file, we have polarization x channels x IFs
				uLong     count( 0 );
				uLong     srcoffset;
				uLong     dstoffset;

				for( uInt ccnt=0; ccnt<nrchannels; ccnt++ ) {
				    for( uInt pcnt=0; pcnt<nrpolarizations; pcnt++, count++ ) {
						//  Calc source offset (we're adressing in a
						//  matrix of dimension
						//  nrchannels*nrpolarizations), so the
						//  current channel/pol combi can be found at:
						//  (current. pol.)*nrchannels + (current
						//  channelnr), that is, if the matrix would be in
						//  memory in C-order!
						//  
						//    but....!
						//
						//  Matrix is in FORTRAN order in memory... (*sigh*)
						srcoffset = ccnt*nrpolarizations + prodMap[pcnt];
		    
						//  Reshuffle data and set weight
						//
						//  The destinationoffset is somewhat more
						//  difficult: the organisation within a random
						//  group is:
						//
						//   [RR ..LL] [RR..LL] [RR..LL] [RR..LL] [RR..LL].........
						//    |                     |       |
						//    -----------------------       --------------->
						//           nrchannel                   nrchannel
						//               |                           |
						//               -----------------------------
						//                           nrif
						//
						//   where [RR..LL] _really_ is:
						//
						//   [Real Imag Weight] .... [Real Imag Weight]
						//           |                       |
						//           -------------------------
						//                     nrstokes
						//
						//  So the destination calculation becomes:
						//
						//  (current IF)*IFdatasize+(current channel)*(nrstokes*3)+(current stokesindex)
						//    + (0,1 or 2, depending on writing Real,Imag or weight)
						dstoffset = ifcnt*IFsize + ccnt*(nrpolarizations*3) + pcnt*3;

						((Float*)destdataptr)[ dstoffset ]   = srcdataptr[srcoffset].real();
						((Float*)destdataptr)[ dstoffset+1 ] = srcdataptr[srcoffset].imag();

						//  The weights are a matrix of shape (npol x nchan), 
						//  stored in memory in FORTRAN order (AAARG!! :) )
						//
						//  So it looks a bit like this:
						//
						//  [WT pol1][WT pol2]..[WT poln][WT pol1][WT pol2]..[WT poln]...
						//     |                    |       |                    |
						//     ----------------------       ----------------------
						//        These are the                Weights for
						//        weights for                  channel 2
						//        channel 1
						//
						//                 |                       |
						//                 ------------------------------> 
						//                       repeated nchan times
						//
						//  Hence we will find the weight for channel c and pol p
						//  at the following offset:
						//
						//      weightoff(c,p) = c*npol + p

                        // HV: 09-03-2007 - In case of shuffling polarization products
                        //                  around, the associated weights must be
                        //                  shuffled too. Did not do that yet...
                        //                  [in uvtableGenerator this got fixed back in
                        //                   2002... took only 5 years before this'un
                        //                   got fix0red ;)]
						if( rowFlag || flagdataptr[srcoffset] ) {
						    // FLAGged
							// HV 18-04-2002 - Drat! Need to make sure that
							//                 flagged visibilities really
							//                 do have negative weights!
							//                 (just negating is not enough...)
						    ((Float*)destdataptr)[ dstoffset+2 ] =
							  -fabs( weightdataptr[ccnt*nrpolarizations + prodMap[pcnt]] );
						} else {
						    // NOT FLAGged
						    ((Float*)destdataptr)[ dstoffset+2 ] =
									  weightdataptr[ ccnt*nrpolarizations + prodMap[pcnt] ];
						}
				    }
				    // Ok. Done all pols..
				}
				// Phew.. 'transposed' matrix.
			}
            // Processed all IFs
            // Since we skip IFs when we do not find a row in the "grouptable"
            // (rather than "break"eing) we always get the maximum amount of
            // data present in the MS [present as in 'present of what we expect']
            // If nothing found, everything will be set to default 0.0 values
            // so we can just write the group, period

            // UVW/TIME etc are not expected to vary so we just take them from
            // row 0. Actually, even if they _would_, there's just no way we can
            // put it in this FITS format.

            // Deal properly with UVW
            uvwcol.get( 0, uvw );
            //  This is part 2) of the explanation above: "HV: 19-10-2000 - ..." 
            uvw *= -1.0;

            //  We want to get the uvw in units of wavelengths. From the
            //  MS we get it in meters. So we divide through by the
            //  wavelength to get it in units of wavelength
            if( usewavelength ) 
                uvw /= wavelength;
            else
                uvw *= oneOverC;
            *uptr = uvw(0);
            *vptr = uvw(1);
            *wptr = uvw(2);

            // time	
            timeToDay( day, dayFraction, time(0) );
			
            *dayptr     = day;
            *dayfracptr = dayFraction;
			
            // BASELINE
            *blptr  = (ant1(0)+1)*256 + ant2(0) + 1;
    				
            // FREQSEL ( in the future this will be FREQ_GRP+1, for now, it's 1 )
            *freqselptr = curfq->second.aipsfreqgrp;
			
            // SOURCE ID
            *sourceidptr = (fieldid(0) + 1);
			
            // INTTIM
            *inttimptr = exposure(0);
			
            //  Put the group!
            newhdu->put(groupcnt, datarecord);
			
            if( !(groupcnt%100) ) {
                Double pct( ((Double)groupcnt/(Double)nrgroups)*100.0 );
                cout << j2ms::printeffer("Done: %4.1lf%%", pct) << "    \r" << flush;
            }
            groupcnt++;
            //  Done processing this particular FREQGRP
        }
        // Done all freq-groups
        // DON't forget to advance the tableiterator!!!!!!
		maintableiterator.next();
    }
    cout << endl;

    cout << "Wrote " << groupcnt << " groups out of anticipated " << nrgroups << endl;
    
    //  Check if we need to add some groups...
    if( groupcnt<nrgroups ) {
        cout << "Drat! Not enough groups were written. Adding " << (nrgroups-groupcnt) 
             << " empty groups to validate fits-file...." << flush;
	
        //  Set up empty group
        (*datamatrixptr) = 0.0f;//Complex( 0.0f, 0.0f );

        *uptr = 0.0;
        *vptr = 0.0;
        *wptr = 0.0;
	
        *dayptr      = 0.0;
        *dayfracptr  = 0.0;
	
        *blptr       = 0.0;
        *freqselptr  = 1;
        *sourceidptr = 1;
        *inttimptr   = 1.0;
        
        while( groupcnt++<nrgroups ) 
            newhdu->put( groupcnt, datarecord );
	    cout << "ok." << endl;
    }
    //  Don't forget to delete (if necessary) the storage for the
    //  arrays!
    srcdata.freeStorage( srcdataptr, delSrcdataptr );
    weightdata.freeStorage( weightdataptr, delWeightptr );
    flagdata.freeStorage( flagdataptr, delFlagptr );
    (*datamatrixptr).freeStorage( destdataptr, delDstdataptr );

    //  Add some extra-keywords.....
    FitsKeyword*      ptr2kw;
    ostringstream     strm;
    FitsKeywordList   tmp;
    

    tmp.mk( FITS::OBJECT, objectname.c_str(), "Multisource" );
    tmp.mk( FITS::EPOCH, epoche, "Epoch" );
    tmp.mk( FITS::DATE_OBS, date.c_str() );
    tmp.mk( FITS::TELESCOP, telescope.c_str() );
    tmp.mk( FITS::OBSERVER, inforef.getObsCode().c_str() );
    tmp.mk( FITS::INSTRUME, instrument.c_str() );
    tmp.mk( "RECEIVER", "JIVE" );
    tmp.mk( FITS::BSCALE, 1.0 );
    tmp.mk( FITS::BZERO, 0.0 );
    tmp.mk( FITS::BUNIT, bunit.c_str() );

    //  Check for multiple pieces....
    if( inforef.maxPieceNr()>1 ) {
    	strm.str( string() );
    	strm << "AIPS  IPIECE= " << inforef.itsPieceNr()+1 << "  NPIECE=" 
	         << inforef.maxPieceNr();
	
    	tmp.history( strm.str().c_str() );

    	//  We need to write first/last visibility....
    	strm.str( string() );
    	strm << "AIPS  FIRSTVIS= " << MSConversionInfo::lastVisibility+1;
    	tmp.history( strm.str().c_str() );

    	MSConversionInfo::lastVisibility += groupcnt;

    	strm.str( string() );
    	strm << "AIPS  LASTVIS = " << MSConversionInfo::lastVisibility+1;
    	tmp.history( strm.str().c_str() );	
    }

    tmp.first();
    tmp.next();
    ptr2kw = tmp.curr();
    while( ptr2kw ) {
        newhdu->addExtraKeyword( *ptr2kw );
        ptr2kw = tmp.next();
    }

    // Put HDU in the return value
    result.push_back( newhdu );
    return result;
}



//  The d'tor: nothing to destruct at this moment.....
maingroupGenerator::~maingroupGenerator()
{}











