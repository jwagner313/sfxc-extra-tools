// asciitabGenerator.cc
//
// Implementation of the asciitabGenerator class. See asciitabGenerator.h for details.
//
//
// Author:
//
//      Harro Verkouter   12-3-1998
//
// $Id: asciitabGenerator.cc,v 1.8 2006/11/22 14:14:12 jive_cc Exp $
#include <jive/ms2uvfitsimpl/Generators/asciitabGenerator.h>

//
//  Aips++ includes
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <ms/MeasurementSets/MSColumns.h>
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAsciiTableHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAsciiColumn.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordField.h>
#include <jive/ms2uvfitsimpl/DumpRecord.h>
#include <jive/Utilities/streamutil.h>


using namespace std;
using namespace casa;

//  This function will do all the work of getting all the necessary
//  antenna information out of the MSConversionInfo object. It will
//  then create a Ascii table extension HDU and pass that back to the
//  caller!
hdulist_t asciitabGenerator::generate( const MSConversionInfo& ) const
{
    //
    // Do it!
    //
    Record              tablerow;
    hdulist_t           result;
    FITSAsciiColumn     anname( "ANNAME", "The antennaname", "A8", "" );
    FITSAsciiColumn     stabxyz( "STABXYZ", "", "F12.3", "METERS" );
    FITSAsciiColumn     derxyz( "DERXYZ", "", "D7.2E3", "METERS/SEC" );
    FITSAsciiTableHDU*  hduptr( 0 );


    //
    //  Now do create the object and return the pointer to the caller!
    //
    hduptr=new FITSAsciiTableHDU( "ANTAB", 1 );

    hduptr->addColumn( anname );
    hduptr->addColumn( stabxyz );
    hduptr->addColumn( derxyz );

    hduptr->setNumberRows( 10 );
    
    //
    //  Restructure the record
    //
    tablerow.restructure( hduptr->getRowDescription() );

    //
    //  Now we can have recordfieldptrs!
    //
    RecordFieldPtr<String>   nameptr( tablerow, "ANNAME" );
    RecordFieldPtr<Float>    stabptr( tablerow, "STABXYZ" );
    RecordFieldPtr<Double>   derxyzptr( tablerow, "DERXYZ" );


    *nameptr="Blah";
    //FITSColumn&    col0( hduptr->getColumn("STABXYZ") );
    //FITSColumn&    col1( hduptr->getColumn("ANNAME") );
    
    for( Int k=0; k<5; k++ )
    {
        *stabptr=(Float)k*3.14;
        (*derxyzptr)=-1.0*k*3.14;
        hduptr->putrow( k, tablerow );
    }
    
    result.push_back( hduptr );
    return result;
}



asciitabGenerator::~asciitabGenerator()
{
}

