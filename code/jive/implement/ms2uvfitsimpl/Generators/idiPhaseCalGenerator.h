#ifndef IDIPHASECALGENERATOR_H
#define IDIPHASECALGENERATOR_H

#include <jive/ms2uvfitsimpl/Generators/hduGenerator.h>
#include <jive/ms2uvfitsimpl/Generators/Registrar.h>

class idiPhaseCalGenerator: public virtual hduGenerator
{
 public:

  idiPhaseCalGenerator();
  virtual ~idiPhaseCalGenerator();

  virtual idtype_t getID(void) const { return "idiPhaseCal"; }

  virtual hdulist_t generate(const MSConversionInfo& info) const;

 private:
  idiPhaseCalGenerator(const idiPhaseCalGenerator&);
  idiPhaseCalGenerator& operator=(const idiPhaseCalGenerator&);
};

DECLARE_GENERATOR(idiPhaseCalGenerator);

#endif // idiPhaseCalGenerator.h
