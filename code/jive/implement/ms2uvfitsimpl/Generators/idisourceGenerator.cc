//
//  Do generate the source table!
//
//
//
//
#include <jive/ms2uvfitsimpl/Generators/idisourceGenerator.h>

#include <ms/MeasurementSets/MSColumns.h>

//
//  My includes
//
#include <jive/ms2uvfitsimpl/MSConversionInfo.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/Vector.h>
#include <casa/Arrays/Matrix.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <casa/Containers/Record.h>
#include <tables/Tables/TableRecord.h>
#include <casa/Containers/RecordField.h>
#include <casa/BasicSL/Constants.h>
#include <tables/Tables/TableIter.h>
#include <tables/Tables/ExprNode.h>
#include <tables/Tables/TableDesc.h>
#include <measures/Measures.h>
#include <measures/Measures/MDirection.h>
#include <casa/Quanta/MVAngle.h>
#include <ms/MeasurementSets/MeasurementSet.h>

//  Include this for the getEpoch-function
#include <jive/ms2uvfitsimpl/Generators/sutabGenerator.h>

#include <ms/MeasurementSets/MSField.h>
#include <ms/MeasurementSets/MSSource.h>
#include <ms/MeasurementSets/MSFieldColumns.h>
#include <ms/MeasurementSets/MSSourceColumns.h>

#include <jive/Utilities/streamutil.h>
#include <jive/Utilities/texcept.h>

#include <sstream>

using namespace casa;
using namespace std;

DEFINE_GENERATOR(idisourceGenerator);


#define TESTGET(a,b) \
    do {\
        cout << (a) << "..." << flush;\
        (b);\
        cout << "ok" << endl;\
    } while(0);


//
//  This function will do all the work of getting all the necessary
//  TSYS information out of the MSConversionInfo object. It will
//  then create a binary table extension HDU and pass that back to the
//  caller!
//

//
//
//  The strategy for the SOURCE NAME is as follows:
//
//  * If the field has a source attached ( source_id!=-1 ), and it's
//  the only field with this source_id, we write the name from the
//  source table.
//
//  * If the field has a source attached AND there is more than one
//  field, we first look if the 'NAME' column is present in the
//  fieldtable. If so, the value in this field will be used. If the
//  'NAME' column is absent (MS vs. < 2.0) we use the string in the
//  'CODE' column as sourcename for the current field.
//
//     In either case the following holds: if the value in the field
//     is empty or not defined, we make up a sourcename ourselves and
//     tell the user we did it.
//
//
//  NOTE: On SOURCE NUMBERING.
//
//  Since Classic AIPS cannot deal with multiple pointings per source
//  and the users still want their data in Classic AIPS, we do the
//  following: every field in the field-table is regarded as a single
//  source (as seen from Classic AIPS point of view). The RA/DEC are
//  taken from the field-table. If there happens to be a physical
//  source of radiation present in the field (indicated by
//  source_id!=-1), the velocity/ IQUV-fluxes etc. are taken from the
//  SOURCE table, otherwise they will be filled in with defaults.
//
//  Errr. I'm drifting from the subject. The source numbering. In the
//  main-table generator, we have to take into account too the fact
//  that we're dealing with multi-source: we add the SOURCE ID-random
//  parameter to the list of parameters. The value we fill in here is
//  the FIELD_ID from the main table. So we do NOT use the SOURCE_ID
//  from the SOURCE-table in aips++.
//
hdulist_t idisourceGenerator::generate( const MSConversionInfo& inforef ) const
{
    cout << "Making SOURCE table" << endl;

    // automatic variables
    Int                    unknownsource( 0 );
    Int                    currentsourceid;
    Int                    rownrinsourcetab;
    uInt                   nrifs( inforef.nrIFs() );
    uInt                   nrrows; // The number of rows in the destination table!
    uInt                   pointingspersrc;
    Double                 currentepoch;
    String                 sourcename;
    MSField                fieldtable( inforef.getMS().field() );
    hdulist_t              result;
	ROMSColumns            mscol( inforef.getMS() );
    const MSSource&        sourcetable( inforef.getMS().source() );
    ROMSFieldColumns       fieldcolumns( fieldtable );
    ROMSSourceColumns      sourcecolumns( sourcetable );
    FITSBinaryTableHDU*    sutabptr( new FITSBinaryTableHDU("SOURCE", 1) );

    
    //  Let's try to find out the total number of rows in the
    //  destination SU-table. This number is necessarily equal to the
    //  number of pointings. We will treat (in principle) every
    //  pointing as a different source.
    nrrows = fieldtable.nrow();
    
    //  If, after all checking etc. nrrows==0, it means that there's
    //  nothing here for us!
    if( !nrrows ) {
    	cout << "No source/field information found." << endl;
    	return result;
    }

    //  Having verified that we have to write some stuff, we need to
    //  setup the table!
    IPosition     ifshape( 1, nrifs );
    Array<Float>  nullswithifshape( ifshape );
    Array<uChar>  charswithifshape( ifshape );
    Array<uChar>  fourchars( IPosition(1,4) );
    Array<Double> dnullswithifshape( ifshape );
    
    nullswithifshape  = (Float)0.0f;
    charswithifshape  = ((uChar)' ');
    fourchars         = ((uChar)' ');
    dnullswithifshape = (Double)0.0;

    sutabptr->addColumn( FITSBinaryColumn("SOURCE_ID", "Source ID", "1J", "") );
    sutabptr->addColumn( FITSBinaryColumn("SOURCE", "Source name", "16A", "") );
    sutabptr->addColumn( FITSBinaryColumn("QUAL", "", "1J", "") );
	sutabptr->addColumn( FITSBinaryColumn("CALCODE", TpArrayUChar, IPosition(1,4) ) );
    sutabptr->addColumn( FITSBinaryColumn("FREQID", "", "1J", "") );
    //sutabptr->addColumn( FITSBinaryColumn("CALCODE", "Calibrator code", "4A", "" ) );
    sutabptr->addColumn( FITSBinaryColumn("IFLUX", TpArrayFloat, ifshape, "JY") );
    sutabptr->addColumn( FITSBinaryColumn("QFLUX", TpArrayFloat, ifshape, "JY") );
    sutabptr->addColumn( FITSBinaryColumn("UFLUX", TpArrayFloat, ifshape, "JY") );
    sutabptr->addColumn( FITSBinaryColumn("VFLUX", TpArrayFloat, ifshape, "JY") );
    sutabptr->addColumn( FITSBinaryColumn("ALPHA", TpArrayFloat, ifshape, "") );
    sutabptr->addColumn( FITSBinaryColumn("FREQOFF", TpArrayFloat, ifshape, "HZ") );
    sutabptr->addColumn( FITSBinaryColumn("RAEPO", "RA of epoch", "1D", "DEGREES") );
    sutabptr->addColumn( FITSBinaryColumn("DECEPO", "DEC of epoch", "1D", "DEGREES") );
    sutabptr->addColumn( FITSBinaryColumn("EQUINOX", "Mean equinox", "8A", "") );    
    sutabptr->addColumn( FITSBinaryColumn("RAAPP", "Apparent RA", "1D", "DEGREES") );
    sutabptr->addColumn( FITSBinaryColumn("DECAPP", "Apparent DEC", "1D", "DEGREES") );
    sutabptr->addColumn( FITSBinaryColumn("SYSVEL", TpArrayDouble, ifshape, "M/SEC") );    
    sutabptr->addColumn( FITSBinaryColumn("VELTYP", "Velocity type", "8A", "") );    
    sutabptr->addColumn( FITSBinaryColumn("VELDEF", "Velocity definition", "8A", "") );    
    sutabptr->addColumn( FITSBinaryColumn("RESTFREQ", TpArrayDouble, ifshape, "M/SEC") );
    sutabptr->addColumn( FITSBinaryColumn("PMRA", "", "1D", "DEG/DAY") );
    sutabptr->addColumn( FITSBinaryColumn("PMDEC", "", "1D", "DEG/DAY") );
    sutabptr->addColumn( FITSBinaryColumn("PARALLAX", "Parallax of source", "1E", "ARCSEC") );

    //  Tell the table how many rows it should hold!
    //
    //  NOTE: For every field (or SOURCE, to speak in Classic AIPS
    //  terms) we need to write 'nrfreqids' rows, one for every
    //  frequency setup... At the moment, that's one, may change later
    //  on...
    sutabptr->setNumberRows( nrrows );
    

    //  Having done that, we can now start to use the datarecord and
    //  make recordfieldpointers!
    Record     rowdata( sutabptr->getRowDescription() );

    //  The recordfieldpointers
    RecordFieldPtr<Int>            sourceidptr( rowdata, "SOURCE_ID" );
    RecordFieldPtr<String>         nameptr( rowdata, "SOURCE" );
    RecordFieldPtr<Int>            qualptr( rowdata, "QUAL" );
    //RecordFieldPtr<String>         calcodeptr( rowdata, "CALCODE" );
    //
    //  HV: 05-04-2000
    //  Changed template type of RecordFieldPtr for CALCODE from 
    //  String to Array<uChar> since we must make sure that we
    //  get 4 spaces there!
    RecordFieldPtr<Array<uChar> >  calcodeptr( rowdata, "CALCODE" );
    RecordFieldPtr<Int>            freqidptr( rowdata, "FREQID" );
    RecordFieldPtr<Array<Float> >  ifluxptr( rowdata, "IFLUX" );
    RecordFieldPtr<Array<Float> >  qfluxptr( rowdata, "QFLUX" );
    RecordFieldPtr<Array<Float> >  ufluxptr( rowdata, "UFLUX" );
    RecordFieldPtr<Array<Float> >  vfluxptr( rowdata, "VFLUX" );
    RecordFieldPtr<Array<Float> >  alphaptr( rowdata, "ALPHA" );
    RecordFieldPtr<Array<Float> >  freqoffptr( rowdata, "FREQOFF" );
    RecordFieldPtr<Double>         raepoptr( rowdata, "RAEPO" );
    RecordFieldPtr<Double>         decepoptr( rowdata, "DECEPO" );
    RecordFieldPtr<String>         equinoxptr( rowdata, "EQUINOX" );
    RecordFieldPtr<Double>         raappptr( rowdata, "RAAPP" );
    RecordFieldPtr<Double>         decappptr( rowdata, "DECAPP" );
    RecordFieldPtr<Array<Double> > sysvelptr( rowdata, "SYSVEL" );
    RecordFieldPtr<String>         veltypptr( rowdata, "VELTYP" );
    RecordFieldPtr<String>         veldefptr( rowdata, "VELDEF" );
    RecordFieldPtr<Array<Double> > restfreqptr( rowdata, "RESTFREQ" );
    RecordFieldPtr<Double>         pmraptr( rowdata, "PMRA" );
    RecordFieldPtr<Double>         pmdecptr( rowdata, "PMDEC" );
    RecordFieldPtr<Float>          parallaxptr( rowdata, "PARALLAX" );
    
    //  Now loop over all fields!
    for( uInt fieldnr=0; fieldnr<fieldtable.nrow(); fieldnr++ ) {
        //  Fill in default stuff
        *qualptr     = 1;
    	*calcodeptr  = fourchars;

    	*ifluxptr    = nullswithifshape;
    	*qfluxptr    = nullswithifshape;
    	*ufluxptr    = nullswithifshape;
    	*vfluxptr    = nullswithifshape;

    	*freqoffptr  = nullswithifshape;
    	*freqidptr   = 1;

    	*alphaptr    = nullswithifshape;
    	*restfreqptr = dnullswithifshape;
	
    	*raepoptr    = 0.0;
    	*decepoptr   = 0.0;
    
    	*pmraptr     = 0.0;
    	*pmdecptr    = 0.0;
    
    	*raappptr    = 0.0;
    	*decappptr   = 0.0;
    
    	*sysvelptr   = dnullswithifshape;

    	// HV: 28-03-2002 - After discussion with Chris/Huib, it was
    	//                  decided to blank the veltyp/veldef keywords
    	//                  as no velocity frame could have been set (yet).
    //	*veltypptr   = "LSR";
    //	*veldefptr   = "RADIO";
    	*veltypptr   = "";
    	*veldefptr   = "";
    
    	*parallaxptr = 0.0f;
    	
    	*equinoxptr  = "";
    
    	// Stuff into source id the current fieldid. FIELD_IDs in main
    	// are direct indices and hence the rownumber is the source
    	// index. Numbering in Classic AIPS start at 1, we start at 0
    	// so add 1
    	*sourceidptr = fieldnr+1;
    	
    	//  How about the name of the source? Let's first find out if
    	//  there's a source 'attached' to this field and if so, if
    	//  this is the only field 'attached' to that source
    	sourcename       = "";
    	currentsourceid  = -1;
    	currentepoch     = 0.0;
    	rownrinsourcetab = -1;
    	pointingspersrc  = 0;
    
    	currentsourceid = fieldcolumns.sourceId()( fieldnr );

    	if( currentsourceid!=-1 ) {
            if( sourcetable.isNull() ) {
                cout << "idisourceGenerator.cc: Found a field (#" << fieldnr << ") with "
                     << "a SOURCE_ID!=-1 (actually, it is " << currentsourceid << ") BUT "
                     << "THERE IS NO SOURCE TABLE IN THIS MEASUREMENT SET!!!!!"
                     << endl;
                THROW_TEXCEPT("Internal inconsistent MS");
            }
    	    //  Select.... i.e. try to find the row in the source
    	    //  table that describes the source.
    	    Table   pointings( fieldtable(fieldtable.col("SOURCE_ID")==currentsourceid) );
    	    Table   src( sourcetable(sourcetable.col("SOURCE_ID")==currentsourceid) );	    
    
    	    pointingspersrc = pointings.nrow();
    	    
    	    //  Theoretically there should be only one entry in the
    	    //  sourcetable with SOURCE_ID==currentsourceid
    	    if( src.nrow()==1 )
        		rownrinsourcetab = src.rowNumbers()( 0 );

    	    //  If an entry was found in the sourcetable, we take source specific info
    	    //  from the sourcetable....
    	    if( rownrinsourcetab>=0 ) {
        		//  SYSVEL
        		if( sourcetable.tableDesc().isColumn("SYSVEL") && 
        		    sourcecolumns.sysvel().isDefined(rownrinsourcetab) ) {
        		    //  <NOTE> Maybe we need to check the velocity
        		    //  reference to (when needed) convert the
        		    //  radial velocity to LSR velocity!
        		    *sysvelptr = sourcecolumns.sysvel()( rownrinsourcetab );
        		}
        		
        		//  Proper motion
        		Vector<Double>   pm;
        		
        		sourcecolumns.properMotion().get( rownrinsourcetab, pm );
        		    
        		*pmraptr  = pm(0);
        		*pmdecptr = pm(1);
        		
        		//  Calibration group
        		if( sourcecolumns.calibrationGroup().isDefined(rownrinsourcetab) ) {
        		    *qualptr = sourcecolumns.calibrationGroup()( rownrinsourcetab );
        		}
        
        		//  Source code:
        		//
        		//  NOTE: in the MS this is a string; the length of the string should be
        		//  equal to the number of IFs, one character/IF!
        		if( sourcecolumns.code().isDefined(rownrinsourcetab) ) {
        		    const String&  tmpdata( sourcecolumns.code()(rownrinsourcetab) );
        		    
        		    //  Copy at most 4 characters...
        		    *calcodeptr = fourchars;
        		    for( unsigned int ccnt=0; ccnt<4 && ccnt<tmpdata.length(); ccnt++ )
            			(*calcodeptr)( IPosition(1,ccnt) ) = tmpdata[ccnt];
        		}
      	    }
       	}
       	else
      	{
    	    cout << j2ms::printeffer("field %03d: ", fieldnr) << "field has no associated source!\n";
    	    cout << "           " << "Some source-specific info may be not present...\n" << flush;
    	}
    
    	//  Now let's see. We first check if there's a fieldname
    	//  column. If not present or the field is empty, we check the
    	//  fieldcode-column. If that field is empty, we can fall back
    	//  on the source table (if a source was attached). If, after
    	//  that, the sourcename is still empty, we make up something.
    	sourcename = upcase( fieldcolumns.name()(fieldnr) );
   
    	if( sourcename=="" ) {
    	    cout << j2ms::printeffer("field %03d: ", fieldnr) 
                 << "No SOURCENAME found.. trying FIELD::CODE..." << endl;
    	    sourcename = upcase( fieldcolumns.code()(fieldnr) );
    	}
    
    	if( sourcename=="" && pointingspersrc==1 ) {
    	    cout << j2ms::printeffer("field %03d: ", fieldnr) 
                 << "No SOURCENAME found.. trying SOURCE::NAME (1 pointing, thus SOURCE=unique)..." 
                 << endl;
    	    //  One pointing per source: we can use the sourcename as
    	    //  a unique name. But tell the user we did it.
    	    if( rownrinsourcetab>=0 && sourcecolumns.name().isDefined(rownrinsourcetab) ) {
        		sourcename = upcase( sourcecolumns.name()(rownrinsourcetab) );
        		
        		if( sourcename!="" ) {
        		    cout << j2ms::printeffer("field %03d: ",fieldnr) 
        			<< "No fieldname found. Using unique name from SOURCE table: "
        			<< sourcename << endl;
        		}
    	    }
    	}
    	
    	//  If the sourcename is STILL empty, we have to make
    	//  something of our own!
    	if( sourcename=="" ) {
    	    ostringstream  namestr;
    	    
    	    namestr << "FIELD" << unknownsource++;
    	    
    	    sourcename = namestr.str();
    	    
    	    cout << j2ms::printeffer("field %03d: ",fieldnr) 
    		     << "No fieldname/fieldcode/sourcename found, defaulting to " << sourcename << endl;
    	}
    
    	//  Ok. Over to the field position now!
    	{
            MEpoch             rdate( MVEpoch(inforef.observingDate().get("s")), MEpoch::UTC );
    		MeasFrame          appframe( rdate );
    	    MVAngle            fieldra;
    	    MVAngle            fielddec;
    	    MDirection         deldir;
    	    MDirection         appdir;
            Vector<Double>     fldpos;
    	    Vector<Double>     apppos;
            Vector<MDirection> dvec;

    	    dvec   = fieldcolumns.delayDirMeasCol()( fieldnr );
            deldir = dvec(0);
    	    appdir = MDirection::Convert( deldir,
    										MDirection::Ref(MDirection::APP, appframe) )();
    	    
            fldpos     = (deldir.getValue()).get();
            fieldra    = MVAngle( fldpos(0) );
            fielddec   = MVAngle( fldpos(1) );
    	    *raepoptr  = fieldra.degree();
    	    *decepoptr = fielddec.degree();
    	    
    	    apppos     = (appdir.getValue()).get();
    	    fieldra    = MVAngle( apppos(0) );
    	    fielddec   = MVAngle( apppos(1) );
    	    *raappptr  = fieldra.degree();
    	    *decappptr = fielddec.degree();
    	}
    
    	//  The epoch
    	String    equinoxstring;
    	
    	currentepoch = ::getEpoch( fieldcolumns.delayDir() );
    	
    	switch( (Int)currentepoch ) {
    	    case 1950:
    		equinoxstring = "1950.0B";
    		break;
    		
    	    case 2000:
    		equinoxstring = "J2000";
    		break;
    		
    	    default:
    		equinoxstring = "J2000";		
    		cout << j2ms::printeffer("field %03d: ",fieldnr) << "No EPOCH found, defaulting to "
    						  << equinoxstring << endl;
    		break;
    	}
    	
    	//  Do write the name in the record!
    	*nameptr    = sourcename;
    	*equinoxptr = equinoxstring;
    
    	//  Do not forget to write the row....
    	sutabptr->putrow( fieldnr, rowdata );
    }


    //  Add extra keywords....
    FitsKeyword*      kwptr;
    FitsKeywordList   ek;

    //
    //  The common keywords
    //
    ek.mk( "TABREV", (Int)1 );
    ek.mk( "OBSCODE", inforef.getObsCode().c_str() );
    ek.mk( "NO_STKD", (Int)inforef.nrPolarizations() );
    ek.mk( "STK_1", inforef.getFirstStokes() );
    ek.mk( "NO_BAND", (Int)inforef.nrIFs() );
    ek.mk( "NO_CHAN", (Int)inforef.nrChannels() );
    ek.mk( "REF_FREQ", inforef.getReferenceFrequency(MSConversionInfo::CenterChannel) );
    ek.mk( "CHAN_BW", inforef.getChannelwidth() );
    ek.mk( "REF_PIXL", (Float)1 );
    
    //  Add the keywords to the HDU!
    ek.first();
    ek.next();
    kwptr=ek.curr();
    while( kwptr ) {
        sutabptr->addExtraKeyword( *kwptr );
        kwptr=ek.next();
    }

    //  Prepare return value...
    result.push_back( sutabptr );
    return result;
}


idisourceGenerator::~idisourceGenerator()
{
}

