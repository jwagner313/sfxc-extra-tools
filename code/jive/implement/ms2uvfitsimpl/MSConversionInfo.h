// analyses MS and verifies if it can be represented in FITS and caches the most important results
//
//  Set up conversion parameters for the transformation from MS to
//  UVFITS file. The UVFITS-'standard' tells us that there are a
//  number of 'global' FITS-keywords in the UVFITS file (like
//  e.g. OBSCODE, NO_STKD, NO_BAND...). This class will, upon
//  creation attempt to get this info from the MS. Other
//  information may also be nice to know, like time, units etc. All
//  this info has to be located once.
//
//  The MSConversionInfo object will attempt to open the MS for
//  you: you won't get a pointer to the MS! The object will delete
//  any resources itself! You will have access to the MS via a
//  reference: you can't delete a reference!
//
//      Author:   Harro Verkouter,   12-3-1998
//
//       $Id: MSConversionInfo.h,v 1.10 2011/05/04 09:11:52 jive_cc Exp $
#ifndef MSCONVERSIONINFO_H_INC
#define MSCONVERSIONINFO_H_INC

//  Includes
#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <casa/Quanta/MVTime.h>

#include <measures/Measures.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSColumns.h>

#include <map>
#include <set>
#include <iostream>



//  Not exactly sure why I put this function here, but this header file is
//  included in all generator files so maybe that's the easiest way to get
//  this function everyewhere available
//
//  It returns the double value for the *nearest* integer
//      -6.9999 will be rounded to -7.0
//      34.78   will be rounded to 35.0
//      -4.45   will be rounded to -4.0
casa::Double round2int( casa::Double r );

// Classic aips requires frequency-group-numbers to 
// nicely increment from 1..n. The MS gives no warranty
// that the freqgroup column in the SpectralWindow table
// actually adheres to this convention.
//
// Also, as a convenience for the user as well as the fact
// that we must decide on an order in which to write the spectralwindows
// (aka IFs, aka Subbands) [in the MS the order in which the spectral windows
// appear in the SpectralWindow table is arbitrary], so as we can
// at least guarantee internal consistency between different pieces of code
// that do the MS -> FITS translation,
// we reorder the spectral windows in increasing frequency order.
//
// The following map/set stuff will help in keeping it all together -
// mapping AIPS++ MS based indices (DataDescriptionId, SpectralWindowId
// and PolarizationId) to (potentially) reassigned Classic AIPS/FITS acceptable
// indices


// this is one piece of information we map to ->
// the per-spectral-window properties we need (up to
// now, Feb 2007)
struct msid_type {
    casa::Int     spw;
    casa::Int     dd;
    casa::Int     pol;
    casa::Double  freq;
    casa::Double  chinc;
    casa::Double  bw;

    msid_type( casa::Int sid, casa::Int did, casa::Int pid,
               casa::Double f, casa::Double channelinc, casa::Double bandwidth );
};
// Implement a strict-weak-ordering for msid_type thingys.
// We order them by frequency only. Assumption is that
// "the frequency" is unique enough a property...
// Then again - if a duplicate is tried to insert the code will
// barf on that anyhoo!
struct msidtype_compare {
    bool operator()(const msid_type& l, const msid_type& r ) const;
};

// print in HRF
std::ostream& operator<<( std::ostream& os, const msid_type& mid );

// A set of these things is taken to be the collection of stuff
// making up a specifid freq-group
typedef std::set<msid_type, msidtype_compare>  sbset_type;

// We map an aips++ freq-group-id to
// the following compound structure containing:
//  - the associated assigned classic-aips-freqgroup-nr
//  - the set of spectral window definitions for this
//    freq-group
struct freqgroup_type {
    casa::Int  aipsfreqgrp;
    sbset_type subbands;

    // default c'tor sets the aipsfreqgrp member to (-1)
    // to indicate that it's not been set.
    freqgroup_type();
    freqgroup_type( casa::Int afqgrp );
};

// And finally, we keep a map of those,
// mapping the MS freq-group-number to
// the collection of stuff
typedef std::map<casa::Int,freqgroup_type>  fqmap_type;


// Convenience mappings

// From aips++ freq-group-id to associated spectralwindowids
// Note that in MSConversionInfo the spectralwindows are re-ordered
// in increasing frequency order. The order in the list of SPWIds 
// here is AFTER reordering.
typedef std::vector<casa::Int>           spwid_list;
typedef std::map<casa::Int, spwid_list>  spwbyfq_type;


// This vector tells the order of the products.
// In the MS the visibility data is in matrices where
// the 'corrType' column in the Polarization table
// table tells us which row in the matrix associates
// with which polarization product. There is no explicit
// order.
// In FITS, though, there is. The code checks that
// the products that are present are representable in
// FITS [in fits it is a start, #-of-products and an increment
// for tracking which products are present].
// Also, it may mean that the rows from the MS may end up
// in a different row in the FITS file.
// This vector lists, for every source-row 0..n [length of
// vector] the destination row of that product.
typedef std::vector<casa::uInt>           product_map;


// Infrastructure to create maps keyed by antenna feed.
//

// Key type.
//
struct FeedKey
{
  casa::Int antennaId;
  casa::Int feedId;
  casa::Int spWindowId;
};

// Key comparision functor.
//
struct FeedKeyCompare
{
  // Return true if L is less than L.
  //
  bool operator()(FeedKey k, FeedKey l) const
    {
      return ((k.antennaId == l.antennaId)
	      ? ((k.feedId == l.feedId)
		 ? k.spWindowId < l.spWindowId
		 : k.feedId < l.feedId)
	      : k.antennaId < l.antennaId);
    }
};


// Infrastructure to create index of scans.
//

// Key type.
//
struct ScanKey
{
  std::set<casa::Int> antennaIds;
  casa::Int arrayId;
  casa::Double startTime, endTime;
  casa::Int fieldId;
};

// Key comparison function.
//
struct ScanKeyCompare
{
  // Return true if K is less than L.
  //
  bool operator()(ScanKey k, ScanKey l) const
    {
      return ((k.startTime == l.startTime)
	      ? k.arrayId < l.arrayId
	      : k.startTime < l.startTime);
    }
};

typedef std::set<ScanKey, ScanKeyCompare> ScanSet;


//  The class declaration
class MSConversionInfo {
    public:

        // bodge to support splitting up a single MS
        // into various pieces. Sometimes it is necessary
        // to retain the number of the last written visibility
        static casa::uInt   lastVisibility;

        //  Need to be able to specify *what* frequency we want:
        //  center-of-band, center-of-first-channel, band-edge...
        typedef enum _reference {
            CenterBand, CenterChannel, Edge
        } reference;

        // Allow construction from String (supposedly being the name of a MS)
        MSConversionInfo( const casa::String& msname );

        // and from a pre-opened MS
        MSConversionInfo( const casa::MeasurementSet& msname );

        //  Attempt to attach to an already opened MS (used when
        //  splitting a MS -> usr. opens the MS, creates subtables, for
        //  each of which a MSConversionInfo object has to be created)
        MSConversionInfo( casa::MeasurementSet& msref, casa::uInt thispiece, casa::uInt maxpiece );
        // Do not call "attachMS()" [which analyzes the MS and tests FITS representability]
        // but take over analyzation results from other
    	MSConversionInfo( const MSConversionInfo& other, casa::MeasurementSet& msref,
    					  casa::uInt thispiece, casa::uInt maxpiece );

        //  retrieve the original name of the MS
        const casa::String& getMSname( void ) const;

        //  Grant access to the MS, so others can access the tables and
        //  whatsoever.
        const casa::MeasurementSet&  getMS( void ) const;

        // Grant RO-access to the columns.
        casa::ROMSColumns&  getROColumns( void ) const;

        //  Provide acces to the cached items!
        casa::Int           getFirstStokes( void ) const;
        casa::Int           stokesIncrement( void ) const; 
        casa::uInt          nrPolarizations( void ) const;
        casa::uInt          nrChannels( void ) const;

        // This vector tells the order of the products.
        // In the MS the visibility data is in matrices where
        // the 'corrType' column in the Polarization table
        // table tells us which row in the matrix associates
        // with which polarization product. There is no explicit
        // order.
        // In FITS, though, there is. The code checks that
        // the products that are present are representable in
        // FITS [in fits it is a start, #-of-products and an increment
        // for tracking which products are present].
        // Also, it may mean that the rows from the MS may end up
        // in a different row in the FITS file.
        // This vector lists, for every source-row 0..n [length of
        // vector] the destination row of that product.
        const product_map&  getProductMap( void ) const;

        // Correlator/VEX speak:         subbands
        // Classic AIPS/UVFITS speak:    IFs (eye-efs)
        // FITS-IDI format speak:        BANDs
        casa::uInt          nrIFs( void ) const;
    
        // Frequency info
        casa::Double        getReferenceFrequency( MSConversionInfo::reference r ) const;
        casa::Double        getTotalBandwidth( void ) const;
        casa::Double        getChannelwidth( void ) const;
        casa::Double        getChannelincrement( void ) const;

        //  The name of the DATA-column?
        const casa::String& getDataColumnName( void ) const;

        //  Derived from value in first cell of time-column!
        const casa::MVTime& observingDate( void ) const;

        //  The obscode=keyword values
        const casa::String& getObsCode( void ) const;

	const casa::String getTConvertVersion( void ) const;

        //  Multi-part fits file info
        casa::uInt          itsPieceNr( void ) const;
        casa::uInt          maxPieceNr( void ) const;

        //  The frequencygroups (as decoded in attachMS)
        casa::uInt          getNrOfFreqGroups( void ) const;
        const fqmap_type&   getFreqGroups( void ) const;

        // get the map of aips++ freq-group-id to
        // associated-ordered-by-frequency spectral-window-ids
        const spwbyfq_type& getSpwMap( void ) const;

	// Return the maximum number of receptors per antenna feed for
	// MeasurementSet MS.  If MS does not contain any antenna feeds,
	// return -1.
	//
	casa::Int getNumReceptors() const;

	// Return true if the polarizations for the antenne feed specified
	// by ANTENNAID, FEEDID and SPECTRALWINDOWID should be switched.
	//
	casa::Bool switchPolarizations(casa::Int antennaId, casa::Int feedId,
				       casa::Int spWindowId) const;

	// Return the field ID for the specified antenna and time.
	//
	casa::Int getFieldId(casa::Int antennaId, casa::Double time) const;

	// Return TIME in days relative to the reference date.
	//
	casa::Double getIDITime(const casa::MEpoch& time) const;

	// Set scan fuzziness.
	//
	static void setFuzz(casa::Double interval) { fuzz = interval; };

        //  Destruct the info object
        ~MSConversionInfo();
    private:
        //  The measurementset to which the info object applies
        casa::MeasurementSet    itsMS;

        //  The pointer to the RO Column object: the translators can only
        //  read from the columns!
        //  NOTE: this is a pointer since the ROMSColumns object does
        //  not support assignment
        casa::ROMSColumns*       itsROColumnsptr;
    
        // Number of polarizations/channels/BANDs|IFs
        casa::uInt              itsNumberOfPolarizations;
        casa::uInt              itsNumberOfChannels;
        casa::uInt              itsNumberOfIFs;
        //  Cached frequency info
        casa::Double            itsReferenceFrequency;
        casa::Double            itsTotalBandwidth;
        casa::Double            itsChannelincrement;

        //  The observing date
        casa::MVTime            itsObservingDate;
        //  The deduced columnname
        casa::String            itsDataColumnName;

        //  Possibility to indicate which piece out of max nr of pieces
        //  this set represents...
        casa::uInt              myPieceNr;
        casa::uInt              nrPieces;

        casa::String            myObsCode;
        casa::Int               myFirstStokes;
        casa::Int               myStokesInc;

        //  Freq-group stuff: we need to build/keep the list ourselves
        fqmap_type              myList;
        spwbyfq_type            spwMap;
        product_map             productMap;
    
	// Caches for various (expensive) lookups.
	//
	mutable std::map<FeedKey, casa::Bool, FeedKeyCompare> switchCache;
	ScanSet              scans;

	// Reference date.
	//
	casa::MEpoch         refTime;

	// Scan fuzziness.
	//
	static casa::Double  fuzz;

        //  The private method section

        //  Attempt to attach a MS. Throws exception if it fails...
        void                    attachMS(const casa::MeasurementSet& msref);

	// Build an index of scans.
	//
	void              buildScanSet();

        //  Forbid copying, assignment and default ctor!
        MSConversionInfo();
        MSConversionInfo( const MSConversionInfo& );
        const MSConversionInfo& operator=( const MSConversionInfo& );
};


#endif
