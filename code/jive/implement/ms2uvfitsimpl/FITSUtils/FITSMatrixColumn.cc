//
//  implementation of the matrix column class
//
//
//  Author:  Harro Verkouter, 13-09-1999
//
//  $Id: FITSMatrixColumn.cc,v 1.8 2006/03/02 14:21:41 verkout Exp $
//
//  $Log: FITSMatrixColumn.cc,v $
//  Revision 1.8  2006/03/02 14:21:41  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.7  2006/02/10 08:53:46  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.6  2006/01/13 11:35:44  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.5  2004/08/25 06:02:23  verkout
//  HV: Fiddled with template shit. All templates now done automatically.
//      As a result, the source-code for the templates has to be visible
//      compiletime.
//
//  Revision 1.4  2004/01/05 15:24:25  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.3  2003/09/12 07:31:49  verkout
//  HV: Code had to be made gcc3.* compliant. Some changes in AIPS++ had to be incorparated as well.
//
//  Revision 1.2  2001/05/30 11:50:39  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:48  verkout
//  HV: Imported aips++ implement-stuff
//
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSMatrixColumn.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSMatrixAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <casa/Utilities/DataType.h>
#include <jive/Utilities/jexcept.h>

#include <sstream>

using namespace std;
using namespace casa;


FITSMatrixColumn::FITSMatrixColumn( const String& type, DataType tp,
                                    const String& unit, const String& comment ) :
    FITSBinaryColumn( type, tp, IPosition(1,0), unit, comment ),
    myDataType( asScalar(tp) )
{
    this->setMatrixFlag();
}



FITSMatrixColumn::FITSMatrixColumn( Int number, uLong offset, FitsKeywordList& kwsource, 
                                    FITSBinaryTableHDU* ptr2parent ) :
    FITSBinaryColumn( number, offset, kwsource, ptr2parent ),
    myDataType( TpOther )
{
    //  Check format and init acts upon the tform-keyword!
    //  (which has already been read and filled in
    //   by the FITSBinaryColumn() c'tor)
    CheckFormatAndInit();

    //  Tell the binary colum that it really is a matrix column
    this->setMatrixFlag();

    //  Since this is the matrix inary colum c'tor, we may have to filter
    //  out the TDIMn keyword!
    FitsKeyword*   kwptr;
    
    kwptr = kwsource( FITS::TDIM, this->columnnr()+1 );
    if( kwptr )
    {
        //  Read the IPosition from the keyword!
        IPosition    shape;
        RecordDesc   coldesc;
        stringstream strm;
        
        strm << kwptr->asString();
        strm >> shape;
        
        coldesc.addField( this->ttype(), this->getColumnDataType(), shape );
        
        setColumnDescription( coldesc );

        kwsource.del();
    }
    
    //  And... read all the axes descriptions!
    if( (kwptr=kwsource("MAXIS"))==0 )
    {
	    THROW_JEXCEPT("Attempt to create MATRIX-column but the MAXIS "
                << "keyword was not found!");
    }
    
    Int        nraxes;

    nraxes = kwptr->asInt();
    
    kwsource.del();
    
    //  Ok. Loop over all axes!
    for( Int i=0; i<nraxes; i++ )
    {
    	myAxes.push_back( new FITSMatrixAxis(i, kwsource, this) );
    }

    // Ok. Done!
}



FITSMatrixColumn::FITSMatrixColumn( Int number, uLong offset, const FITSMatrixColumn& other,
				    FITSBinaryTableHDU* ptr2parent ) :
    FITSBinaryColumn( number, offset, (const FITSBinaryColumn&)other, ptr2parent ),
    myDataType( other.myDataType )
{
    int                          n;
    axislist_t::const_iterator   curaxis;
    
    //  Tell the BinaryColumn object within ourselves that we're a
    //  matrix column
    this->setMatrixFlag();

    //  Copy the axes across...
    for( curaxis=other.myAxes.begin(), n=0;
         curaxis!=other.myAxes.end();
         curaxis++, n++ )
    {
    	myAxes.push_back( new FITSMatrixAxis(n, *(*curaxis), this) );
    }

    //  Ok.. done!
}


uInt FITSMatrixColumn::nrAxes( void ) const
{
    return myAxes.size();
}

const FITSMatrixAxis* FITSMatrixColumn::getAxis( uInt axnr ) const
{
    const FITSMatrixAxis*   retval( 0 );
    
    if( axnr<myAxes.size() )
    {
    	retval = myAxes[axnr];
    }
    return retval;
}





Bool FITSMatrixColumn::addAxis( const FITSMatrixAxis& axis )
{
    if( this->hasParentHDU() )
    {
    	return False;
    }
    
    //
    //  Add the axis into ourselves, re-generate the tform string and
    //  re-set the stuff!
    //
    Int               curaxisnr;
    FITSMatrixAxis*   newaxisptr( 0 );
    
    //
    //  Step one: in order to determine the correct number for this
    //  axis, we first find out how many axes there already are.
    //
    curaxisnr = (Int)myAxes.size();
    
    //
    //  Now we know that, we can safely create the new axisobject with
    //  the proper settings
    //
    newaxisptr= new FITSMatrixAxis( curaxisnr, axis, this );

    //
    //  Make room for this one...
    //
    myAxes.push_back( newaxisptr );
    
    //
    //  Update our internals...
    //
    IPosition     curshape( this->shape() );
    ostringstream formstrm;
    
    formstrm << curshape.product();

    switch( myDataType )
    {
	case TpChar:
	case TpUChar:
	    formstrm << "A";
	    break;
	    
        case TpBool:
            formstrm << "L";
            break;

        case TpShort:
        case TpUShort:
            formstrm << "I";
            break;
            
        case TpInt:
        case TpUInt:
            formstrm << "J";
            break;
            
        case TpFloat:
            formstrm << "E";
            break;
            
        case TpDouble:
            formstrm << "D";
            break;
            
        case TpComplex:
            formstrm << "C";
            break;
            
        case TpDComplex:
            formstrm << "M";
            break;
            
        default:
            THROW_JEXCEPT("FITSMatrixColumn: datatype not xpressible in FITS");
            break;
    }

    //
    //  Update internal structure of the column object
    //  will throw if fishy
    //
    this->setForm( formstrm.str() );    
    this->CheckFormatAndInit();

    //
    //  Return success...
    //
    return True;
}



//
//  Return the shape of the matrix in this column
//
IPosition FITSMatrixColumn::shape( void ) const
{
    int                        i;
    IPosition                  retval( myAxes.size() );
    axislist_t::const_iterator curaxis;

    for( curaxis=myAxes.begin(), i=0;
         curaxis!=myAxes.end();
         curaxis++, i++ )
    {
    	retval(i) = (*curaxis)->getAxisLength();
    }
    return retval;
}



//
//  Ok. Translate ourselves into keywords....
//
FitsKeywordList FITSMatrixColumn::asFITSKeys( void ) const
{
    FitsKeywordList     retval( this->FITSBinaryColumn::asFITSKeys() );
    
    //
    //  First, the ordinary column keywords, let the BinaryColumn
    //  object do that for us (already done by the time we end up
    //  here...)
    //
    //  Ok. Now add the TDIM keyword to indicate that this is a matrix
    //  column...
    //
    IPosition                  ip( this->shape() );
    axislist_t::const_iterator curaxis;
    
    if( ip.nelements()>0 )
    {
        //
        //  Array with at least one domension
        //
		uInt          nel( ip.nelements() );
        ostringstream strm;

        strm << "(";

		for( uInt i=0; i<nel; i++ )
		{
		    strm << ip(i);
		    if( i<(nel-1) )
		    {
    			strm << ",";
		    }
		}
		strm << ")";
        
		//cout << strm.str() << endl;
	
        retval.mk( this->columnnr()+1, FITS::TDIM, strm.str().c_str() );
    }

    //  Add our own stuff: the axes descriptions...
    for( curaxis=myAxes.begin();
         curaxis!=myAxes.end();
         curaxis++ )
    {
    	FitsKeyword*        kwptr;
    	FitsKeyword*        newkw;
    	FitsKeywordList     tmp( (*curaxis)->asFITSKeys() );
	
    	tmp.first();
    	tmp.next();
    	kwptr = tmp.curr();
	
    	while( kwptr!=0 )
    	{
    	    newkw = new FitsKeyword( *kwptr );
    	    retval.insert( *newkw );
	    
    	    kwptr = tmp.next();
    	}	
    }
    return retval;
}
					      

FITSMatrixColumn::~FITSMatrixColumn()
{
    axislist_t::iterator curaxis;

    //  Delete all the axes...
    for( curaxis=myAxes.begin();
         curaxis!=myAxes.end();
         curaxis++ )
    {
    	delete (*curaxis);
    }
}

