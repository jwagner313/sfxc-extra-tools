//
//  Implementation of the FITSBinaryColumn class
//
//
//  Author:
// 
//        Harro Verkouter,  18-05-1998
//
//  $Id: FITSBinaryColumn.cc,v 1.9 2012/07/05 07:49:57 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/BasicMath/Math.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSTableHDU.h>
#include <fits/FITS/fits.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/Utilities/jexcept.h>

#include <sstream>

//#include <jive/ms2uvfitsimpl/DumpRecord.h>

using namespace casa;
using namespace std;

FITSBinaryColumn::FITSBinaryColumn( const String& type, const String& comment, 
                                    const String& form, const String& unit ) :
    FITSColumn( type, comment, form, unit ),
    itsTotalWidth( 0L ),
    itsNumberElements( 0 ),
    boolIsBitArray( False ),
    myMatrixFlag( False )
{
    //
    //  Tell the fits-column object that we're a binary column!
    //
    this->setBinaryColumnFlag();

    //
    //  Do our own 'Check form and init!'
    //
    CheckFormatAndInit();
}

FITSBinaryColumn::FITSBinaryColumn( const String& type, DataType form,
                                    const IPosition& shape,
                                    const String& unit, const String& comment ) :
    FITSColumn( type, comment, "", unit ),
    itsTotalWidth( 0L ),
    itsNumberElements( shape.product() ),
    boolIsBitArray( False ),
    myMatrixFlag( False )
{
    String        curform;
    RecordDesc    columndescription;
    ostringstream strm;
    
    //
    //  Tell the fits-column object that we're a binary column!
    //
    this->setBinaryColumnFlag();
    

    //
    //  First check if the form is supported
    //
    switch( asScalar(form) )
    {
        case TpChar:
        case TpUChar:
            //
            //  Chars are only one byte/element
            //
            itsTotalWidth=itsNumberElements;
            strm << itsNumberElements << "A";
            break;
            
        case TpBool:
            //
            //  a bool is a char, therefore 1byte/element
            //
            itsTotalWidth=itsNumberElements;
            strm << itsNumberElements << "L";
            break;

        case TpShort:
        case TpUShort:
            //
            //  Shorts are 2bytes/element
            //
            itsTotalWidth=itsNumberElements*2;
            strm << itsNumberElements << "I";
            break;
            
        case TpInt:
        case TpUInt:
            //
            //  Ints are 4-byte/ element
            //
            itsTotalWidth=itsNumberElements*4;
            strm << itsNumberElements << "J";
            break;
            
        case TpFloat:
            //
            //  Single precision float , 4bytes/element
            //
            itsTotalWidth=itsNumberElements*4;
            strm << itsNumberElements << "E";
            break;
            
        case TpDouble:
            //
            //  Doubles, 8bytes/element
            //
            itsTotalWidth=itsNumberElements*8;
            strm << itsNumberElements << "D";
            break;
            
        case TpComplex:
            //
            //  1 complex = 2 Floats = 8 bytes/ element
            //
            itsTotalWidth=itsNumberElements*8;
            strm << itsNumberElements << "C";
            break;
            
        case TpDComplex:
            //
            //  1 DComplex = 2 Double = 16bytes/element
            //
            itsTotalWidth=itsNumberElements*16;
            strm << itsNumberElements << "M";
            break;
            
        default:
            THROW_JEXCEPT("FITSBinaryColumn: datatype not xpressible in FITS");
            break;
    }

    //
    //  get the form
    //
    curform = String( strm.str() );

    if( isArray(form) )
    {
        columndescription.addField( this->ttype(), form, shape );
    }
    else
    {
        columndescription.addField( this->ttype(), form );
    }
    
    //
    //  Set internals
    //
    setForm( curform );
    setDataType( form );
    setColumnDescription( columndescription );
}



    
//
//  The creator from a FITSfile...
//
FITSBinaryColumn::FITSBinaryColumn( Int number, uLong offset, FitsKeywordList& kwsource, 
                                    FITSBinaryTableHDU* ptr2parent ) :
    FITSColumn( number, offset, kwsource, (FITSTableHDU*)ptr2parent ),
    itsTotalWidth( 0L ),
    itsNumberElements( 0 ),
    boolIsBitArray( False ),
    myMatrixFlag( False )
{
    //
    //  Tell the fits-column object that we're a binary column!
    //
    this->setBinaryColumnFlag();

    //
    //  Check format and init acts upon the tform-keyword (that has
    //  been filled in by the c'tor of FITSColumn!)!
    //
    CheckFormatAndInit();
}

FITSBinaryColumn::FITSBinaryColumn( Int number, uLong offset, 
                                    const FITSBinaryColumn& other, FITSBinaryTableHDU* ptr2parent ) :
    FITSColumn( number, offset, (FITSColumn&)other, (FITSTableHDU*)ptr2parent ),
    itsTotalWidth( other.itsTotalWidth ),
    itsNumberElements( other.itsNumberElements ),
    boolIsBitArray( other.boolIsBitArray ),
    myMatrixFlag( other.myMatrixFlag )
{
    //
    //  Tell the fits-column object that we're a binary column!
    //
    this->setBinaryColumnFlag();
}

FITSBinaryColumn::FITSBinaryColumn( const FITSBinaryColumn& other ) :
    FITSColumn( (const FITSColumn&)other ),
    itsTotalWidth( other.itsTotalWidth ),
    itsNumberElements( other.itsNumberElements ),
    boolIsBitArray( other.boolIsBitArray ),
    myMatrixFlag( other.myMatrixFlag )
{
    //
    //  Tell the fits-column object that we're a binary column!
    //
    this->setBinaryColumnFlag();
}

FitsKeywordList FITSBinaryColumn::asFITSKeys( void ) const
{
    FitsKeywordList    retval( this->FITSColumn::asFITSKeys() );
    
    //
    //  Add any binary column specific stuff here...
    //
    return retval;
}
    
    

//
//  Put the value that is in field fieldidx of the record into the cell
//  'rownr' of this column
//
Bool FITSBinaryColumn::put( uInt rownr, const RecordInterface& recordref, const RecordFieldId& fieldidx )
{
    Bool         retval( False );

    //
    //  Check if we have a parent!
    //
    if( !this->hasParentHDU() )
    {
        return retval;
    }

    //
    //  Check if the type of the record passed matches that of the
    //  column!
    //
    

    //
    //  If our parent has no datasegment associated yet, he/she should
    //  make one!
    //
    FITSBinaryTableHDU&  ref2parent( (FITSBinaryTableHDU&)(this->getParentHDU()) );
    

    if( !(ref2parent.hasDataSegment()) )
    {
        //
        //  Hell! Parent HDU has no data yet!
        //
        ref2parent.makeDataSegment();
        
        if( !ref2parent.hasDataSegment() )
        {
            return retval;
        }
    }
    
    //
    //  Ok. The parent now has a datasegment available!
    //

    //
    //  Find out if the datatypes of the indicated recordfield and the
    //  current column datatype match!
    //
    DataType     fieldtype( recordref.dataType(fieldidx) );
    DataType     columndatatype( this->getColumnDataType() );
    RecordDesc   fielddesc;
    RecordDesc   columndesc( this->getColumnDescription() );

//     if( !rownr )
//     {
// 	cout << "fieldidx=" << fieldidx.fieldName() << " (" << fieldidx.fieldNumber() << ")" << endl;
// 	cout << "fieldtype=" << fieldtype << endl;
// 	cout << "columndatatype=" << columndatatype << endl;
// 	cout << "columndesc.shape(0)=" << columndesc.shape(0) << endl;
// 	cout << "recordref.shape(fieldidx)=" << recordref.shape(fieldidx) << endl;
//     }
    

    //
    //  Create a recorddesc for the recordfield
    //
    if( isArray(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype,
                            recordref.shape(fieldidx) );
    }
    else if( isScalar(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype );
    }
    else
    {
        //
        //  Field is a table or another ecord or something we don't
        //  want tp bother with!
        //
        return retval;
    }
    

    if( fielddesc!=columndesc )
    {
	    THROW_JEXCEPT("No put on column " << this->ttype() << ", row="
                << rownr << endl << "Expected " << columndesc << " got "
                << fielddesc << endl);
    }

    //
    //  Ok. The field the fieldidx points at is compatible with what
    //  this column contains. So let's go on!
    //


    //
    //  That's it boys and girls: the checking's over: now do the
    //  putting!
    //
    //
    //  Create a pointer to the actual data-location!
    //
    uLong         totaloffset( 0L );

    totaloffset=((uLong)rownr*(uLong)ref2parent.getCurrentRowWidthInBytes())+((uLong)this->columnoffset());
    
    //
    //  From now on, we'll assume the default return value is True,
    //  only in exceptional cases, the value might evaluate to False.
    //
    retval=True;

    //
    //  NOTE: only the cases Array<Bool> and TpString are different
    //  from normal procedure.
    //

    switch( columndatatype )
    {
        case TpString:
            {
                void*         dataptr( (ref2parent.getDataSegment())[totaloffset] );
                Char*         dest( (Char*)dataptr );
                const String& ref( recordref.asString(fieldidx) );

                if( !dataptr )
                {
                    retval=False;
                    break;
                }
                
                //
                //  Copy at most itsNumberElements of characters from
                //  the string
                //
                //memcpy( dest, (recordref.asString(fieldidx)).chars(), itsNumberElements );
                memcpy( dest, ref.chars(), min((uInt)itsNumberElements, (uInt)ref.length()) );
            }
            break;
            
        case TpBool:
        case TpArrayBool:
            {
                void*   dataptr( (ref2parent.getDataSegment())[totaloffset] );
                
                if( !dataptr )
                {
                    retval=False;
                    break;
                }
                
                //
                //  Array is array of fitslogicals (is character
                //  'T' or 'F'... or array of bits.
                //

                //
                //  This ptr is used when bool is fitslogical
                //
                Char*  dest( (Char*)dataptr );
                //
                //  And this one is used when it's a bit array!
                //
                uChar* udest( (uChar*)dataptr );
                
                if( itsNumberElements>1 )
                {
                    //
                    //  Array of fitsbools
                    //
                    Bool               deleteit;
                    const Bool*        ptr2storage;
                    const Array<Bool>& tmp( recordref.asArrayBool(fieldidx) );
                    
                    //tmp.reference( recordref.asArrayBool(fieldidx) );
                    
                    ptr2storage = tmp.getStorage( deleteit );
                    
                    if( ptr2storage )
                    {
                        if( boolIsBitArray )
                        {
                            for( Int i=0; i<itsNumberElements; i++ )
                            {
                                if( ptr2storage[i]==True )
                                {
                                    udest[i/8]|=( uChar(0x1)<<(i%8) );
                                }
                                else
                                {
                                    udest[i/8]&=( ~(uChar(0x1)<<(i%8)) );
                                }
                            }
                        }
                        else
                        {
                            for( Int i=0; i<itsNumberElements; i++ )
                            {
                                dest[i]= ((ptr2storage[i]==True)?('T'):('F'));
                            }
                        }
                        
                    }
                    
                    tmp.freeStorage( ptr2storage, deleteit );
                }
                else
                {
                    //
                    //  A single bool:
                    //
                    if( boolIsBitArray )
                    {
                        if( recordref.asBool(fieldidx)==True )
                        {
                            *udest |= ( uChar(0x1) );
                        }
                        else
                        {
                            *udest &= ~( uChar(0x1) );
                        }
                    }
                    else
                    {
                        *dest= (((recordref.asBool(fieldidx))==True)?('T'):('F'));
                    }
                }
            }
            break;
            
        default:
            //
            //  Default is to route it through to the global function
            //  flushRecordField!
            //
            if( itsNumberElements )
            {
                retval = flushRecordField( recordref, fieldidx, ref2parent.getDataSegment(), totaloffset );
            }
            break;
    }
    return retval;
}

Bool FITSBinaryColumn::get( uInt rownr, RecordInterface& recordref, const RecordFieldId& fieldidx ) const
{
    //
    //  Now we know that we have a parent!
    //
    Bool                 except( False );
    Bool                 retval( False );

    //
    //  Check if the indicated field indeed does exist
    //
    try
    {
        recordref.idToNumber(fieldidx);
    }
    catch( const std::exception& x )
    {
        cerr << x.what() << endl;
        except=True;
    }
    
    if( except )
    {
        cout << "indicated field does not exist!" << endl;
        return retval;
    }
    
    //
    //  Check if the column-object has a parent HDU
    //
    if( !this->hasParentHDU() )
    {
        return retval;
    }
    
    //
    //  Create a pointer to the parent object
    //
    FITSBinaryTableHDU&  ref2parent( (FITSBinaryTableHDU&)(this->getParentHDU()) );
    

    if( !(ref2parent.hasDataSegment()) )
    {
        //
        //  Hell! Parent HDU has no data yet!
        //
        return retval;
    }
    
    //
    //  Very well: parent has data associated, now let's get the
    //  requested info!
    //

    //
    //  Make a recorddesc from the field the index points at. Compare
    //  it to the desc that describes this column. Ifg match OK,
    //  otherwise, get out!
    //
    DataType     fieldtype( recordref.dataType(fieldidx) );
    DataType     columndatatype( this->getColumnDataType() );
    RecordDesc   fielddesc;
    RecordDesc   columndesc( this->getColumnDescription() );

    //
    //  Create a recorddesc for the recordfield
    //
    if( isArray(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype,
                            recordref.shape(fieldidx) );
    }
    else if( isScalar(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype );
    }
    else
    {
        //
        //  Field is a table or another ecord or something we don't
        //  want tp bother with!
        //
        return retval;
    }


    if( fielddesc!=columndesc )
    {
        cout << "Descriptions don't match. No get!" << endl;
        return retval;
    }
    

    //
    //  Now do get the datapointer: calculate the total offset of this
    //  datum into the block of fitsdata (i.e. the FITSDataSegment).
    //
    uLong         totaloffset( 0L );

    totaloffset=((uLong)rownr*(uLong)ref2parent.getCurrentRowWidthInBytes())+(uLong)(this->columnoffset());
    
    //
    //  Now then, depending on what the column is, fill the record!
    //

    //
    //  From now on, the default return value changes to True
    //
    retval=True;
    
    //
    //  Note: only the specific cases String and Array<Bool> need to
    //  be dealt with, the standard types can be done via the existing
    //  flush/fillRecordField functions.
    //
    switch( columndatatype )
    {
        case TpString:
            {
                //
                //  We can't be sure the string's zero
                //  terminated. Therefore we copy the maximum amount of
                //  chars. across and make sure the string is properly
                //  terminated!
                //
                Char*       buffer=new Char[itsNumberElements+1];
                const void* dataptr( (ref2parent.getDataSegment())[totaloffset] );
                
                if( !dataptr )
                {
                    retval=False;
                    break;
                }
                

                memcpy( buffer, dataptr, itsNumberElements );
                buffer[itsNumberElements]='\0';
                
                recordref.define( fieldidx, String(buffer) );
            }
            break;

        case TpBool:
        case TpArrayBool:
            {
                const void*  dataptr( (ref2parent.getDataSegment())[totaloffset] );

                if( !dataptr )
                {
                    retval=False;
                    break;
                }
                

                if( boolIsBitArray )
                {
                    if( itsNumberElements>1 )
                    {
                        //
                        //  For bit-arrays, the itsTotalWidth is the number of
                        //  bytes needed to accomodate the bit-array
                        //
                        Bool*        buffer( new Bool[itsNumberElements] );
                        const uChar* byteptr( (const uChar*)dataptr );
                        
                        for( Int i=0; i<itsNumberElements; i++ )
                        {
                            if( byteptr[i/8]&(uChar(0x1)<<(i%8)) )
                            {
                                buffer[i]=True;
                            }
                            else
                            {
                                buffer[i]=False;
                            }
                        }
                
                        Array<Bool>  tmp( IPosition(1,itsNumberElements), buffer, SHARE );
                        
                        recordref.define( fieldidx, tmp );
                        
                        //
                        //  We don't need the buffer anymore!
                        //
                        delete [] buffer;
                    }
                    else if( itsNumberElements )
                    {
                        Bool         tmp;
                        const uChar* byteptr( (const uChar*)dataptr );
                        
                        tmp=( ((*byteptr)&0x01)==uChar(0x01) );
                        
                        recordref.define( fieldidx, tmp );
                    }
                }
                else
                {
                    //
                    //  Cast the datapointer to a ptr to FITS booleans (a
                    //  char: 'T'==true and 'F'==false and ' '==undefined!
                    //
                    Char*  ptr2fitsbool( (Char*)dataptr );
                    
                    if( itsNumberElements>1 )
                    {
                        //
                        //  Array of Bool!
                        //
                        Vector<Bool>  tmp( itsNumberElements );
                        
                        for( Int i=0; i<itsNumberElements; i++ )
                        {
                            tmp(i)=(ptr2fitsbool[i]=='T');
                        }
                        
                        recordref.define( fieldidx, tmp );
                    }
                    else if( itsNumberElements )
                    {
                        recordref.define( fieldidx, (*ptr2fitsbool=='T') );
                    }
                }
            }
            break;
            
        default:
            //
            //  Pass on to the fill() function....
            //
            if( itsNumberElements )
            {
                retval = fillRecordField( recordref, fieldidx, ref2parent.getDataSegment(), totaloffset );
            }
            break;
    }
    
    return retval;
}



uLong FITSBinaryColumn::getWidthInBytes( void ) const
{
    return itsTotalWidth;
}


Bool FITSBinaryColumn::isMatrixColumn( void ) const
{
    return myMatrixFlag;
}




FITSBinaryColumn::~FITSBinaryColumn()
{    
}

//
//
//  The protected section: allow people only to set the matrix flag ->
//  once it has been set it cannot be unset!
//
//
void FITSBinaryColumn::setMatrixFlag( void ) 
{
    myMatrixFlag = True;
    return;
}




//  Private methods:
//
//
//  Check the string 'itsForm' and try to deduce what's in
//  there. According to that, fill in the affected private
//  datamembers: columnDataType and itsTotalWidth!
void FITSBinaryColumn::CheckFormatAndInit( void ) {
    Char        fitstype;
    DataType    columnDataType;
    RecordDesc  columnDescription;
    const Regex allowedForm( "[0-9]+[DEAJLIXCM][ \t]*" );
    
    //  If the form does not conform to the allowed form, we throw an
    //  exception!
    if( !(this->tform().matches(allowedForm)) )
        THROW_JEXCEPT("FITSBinaryColumn: the form specifier `" << this->tform()
                << "' is invalid. No valid column could be created");
    
    //  Now break apart....
    if( ::sscanf(this->tform().chars(), "%d%c", &itsNumberElements, &fitstype)!=2 ) 
        THROW_JEXCEPT("FITSBinaryColumn: the form specifier `" << this->tform()
                << "' is invalid. No valid column could be created");

    //  Ok. Determine the type of this column.....
    
    switch( fitstype )
    {
        case 'D':
            //
            //  Double precision IEEE floating point, 8bytes/element
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayDouble
                //
                columnDataType=TpArrayDouble;
            }
            else
            {
                columnDataType=TpDouble;
            }

            //
            //  Compute width of column
            //
            itsTotalWidth=itsNumberElements*8;
            
            break;

        case 'E':
            //
            //  Single precision IEEE floating point. 4bytes/element
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayFloat
                //
                columnDataType=TpArrayFloat;
            }
            else
            {
                columnDataType=TpFloat;
            }

            //
            //  Compute columnwidth
            //
            itsTotalWidth=itsNumberElements*4;
            
            break;

        case 'A':
            //
            //  Characters, 1 byte/element
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayChar = string?!
                //
                //columnDataType=TpArrayChar;
                columnDataType=TpString;
            }
            else
            {
                columnDataType=TpUChar;
            }

            //
            //  Coluumnwidth
            //
            itsTotalWidth=itsNumberElements;
            
            break;


        case 'J':
            //
            //  Long integer, 4 bytes/element. In 'DataType.h' they
            //  state that the Int (used in AIPS++) is always 4 bytes,
            //  so we use that one (the enum does not feature lon/long
            //  int)
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayDouble
                //
                columnDataType=TpArrayInt;
            }
            else
            {
                columnDataType=TpInt;
            }

            //
            //  Columnwidth
            //
            itsTotalWidth=itsNumberElements*4;
            
            break;

        case 'L':
            //
            //  FitsLogical. An ASCII 'T' for True, 'F' for False and
            //  '\0' for undefined. In any case, it takes up one
            //  character per element!
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayDouble
                //
                columnDataType=TpArrayBool;
            }
            else
            {
                columnDataType=TpBool;
            }

            //
            //  Columnwidth
            //
            itsTotalWidth=itsNumberElements;
            
            break;


        case 'I':
            //
            //  Short integer, 2 bytes/element.
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayDouble
                //
                columnDataType=TpArrayShort;
            }
            else
            {
                columnDataType=TpShort;
            }

            //
            //  Columnwidth
            //
            itsTotalWidth=itsNumberElements*2;
            
            break;


        case 'X':
            //
            //  Bit array. The bit array is divided over bytes. So we
            //  have to find the smallest multiple of 8 that is
            //  greater than or equal to nrelements. The type is
            //  ArrayChar, the width (well, we'll calculate that!)
            //
            boolIsBitArray=True;

            //
            //  Columnwidth
            //
            if( itsNumberElements )
            {
                itsTotalWidth=(itsNumberElements/8)+1;

                if( itsNumberElements>1 )
                {
                    columnDataType=TpArrayBool;
                }
                else
                {
                    columnDataType=TpBool;
                }
            }
            else
            {
                itsTotalWidth=0;
            }
            break;

        case 'C':
            //
            //  Complex number, consisting of two IEEE 4-byte single
            //  precision floating points: 8 bytes/element
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayDouble
                //
                columnDataType=TpArrayComplex;
            }
            else
            {
                columnDataType=TpComplex;
            }

            //
            //  Columnwidth
            //
            itsTotalWidth=itsNumberElements*8;
            
            break;

        case 'M':
            //
            //  Complex number consisting of two IEEE 8-byte double
            //  precision floating point numbers, 16 bytes/element
            //
            if( itsNumberElements>1 )
            {
                //
                //  ArrayDouble
                //
                columnDataType=TpArrayDComplex;
            }
            else
            {
                columnDataType=TpDComplex;
            }

            //
            //  Columnwidth
            //
            itsTotalWidth=itsNumberElements*16;
            
            break;

        default:
            //
            //  We didn't recognize/support the datatype?!
            //
            THROW_JEXCEPT("FITSBinaryColumn: Unsupported column datatype `"
                    << this->tform() << "'");
            break;
    }

    //
    //  Ok. Everything is filled in by now: we have the datatype and
    //  the column width right? Only thing left is to make the column
    //  description!
    //

    //
    //  Lets construct a unique name for this column:
    //
    if( itsNumberElements>1 && columnDataType!=TpString )
    {
        columnDescription.addField( this->ttype(), columnDataType, IPosition(1,itsNumberElements) );
    }
    else
    {
        columnDescription.addField( this->ttype(), columnDataType );
    }

    //
    // Set the correct values in the base class
    //
    setDataType( columnDataType );
    setColumnDescription( columnDescription );
}

