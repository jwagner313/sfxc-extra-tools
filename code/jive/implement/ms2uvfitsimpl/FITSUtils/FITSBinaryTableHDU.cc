//
//  Implementation of the FITSBinaryTableHDU class
//
//
//  Author:
//
//   Harro Verkouter,   18-05-1998
//
//
//  $Id: FITSBinaryTableHDU.cc,v 1.8 2007/05/25 10:22:50 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSMatrixColumn.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSMatrixAxis.h>

#include <sstream>

using namespace std;
using namespace casa;

//  Create a BinaryTableHDU with a name and a versionnr
FITSBinaryTableHDU::FITSBinaryTableHDU( const String& name, uInt version ) :
    FITSTableHDU( name, version )
{}

FITSBinaryTableHDU::FITSBinaryTableHDU( FitsKeywordList& kwlist, ByteIO* ptr2stream ) :
    FITSTableHDU( kwlist, ptr2stream )
{
    FitsKeyword*    keywordptr;
    
    //  The c'tor of the FITSTableHDU object has taken out the values
    //  it needs. We only have to be concerned about our columns!
    //
    //  Verify that the HDU is a binary table extension!
    if( (keywordptr=kwlist(FITS::XTENSION))==0 )
        throw( AipsError("Attempt to create BINTABLEextension from keywordlist,"
                         "however, no `XTENSION'-keyword found!") );

    String   tmp( keywordptr->asString() );
    
    if( !tmp.matches(FITSBinaryTableHDU::fitsname()) )
        throw( AipsError("Attempt to create BINTABLE hdu with incorrect XTENSION type, "
                         +String(keywordptr->asString())+" was found.") );
    //  Remove this keyword from the list!
    kwlist.del();


    // Extract all the columns
    if( (keywordptr=kwlist(FITS::TFIELDS))==0 )
        throw( AipsError("Attempt to create a table: number of columns is missing (TFIELDS)") );

    Int    nrofcolumns( keywordptr->asInt() );
    
    if( nrofcolumns<=0 )
        throw( AipsError("Invalid value for TFIELDS (=number of columns) encountered. Value should be >0") );
    
    //  Delete the keyword
    kwlist.del();

    //  Get out all the columns:
    uLong             offset( 0L );
    FITSBinaryColumn* ptr2column;
    
    for( Int j=0; j<nrofcolumns; j++ ) {
        //  Create a new column.
        ptr2column = new FITSBinaryColumn(j, offset, kwlist, this);

        FITSTableHDU::addColumn( (FITSColumn*)ptr2column );
        
        //  And the offset
        offset += ptr2column->getWidthInBytes();
    }
    
    //  We're left with all the other keywords!
    this->setExtraKeywords( kwlist );

    //  Make the datasegment!
    this->makeDataSegment( ptr2stream );
}

Int FITSBinaryTableHDU::addColumn( const FITSBinaryColumn& column2add ) {
    //  If a datasegment is present: we may not add any columns any
    //  more! Its layout is fixed under that condition
    if( this->hasDataSegment() )
        return -1;

    Int          newcolumnnr;
    uLong        newcolumnoffset( 0L );
    FITSColumn*  newcolumn( 0 );

    //  Find out the column number for this new column
    newcolumnnr = this->numberColumns();
    
    //  Decide on the current offset for the newly allocated column:
    //
    //  Only have to do calculations if this is NOT the first column
    //  (i.e. newcolumnnr!=0)
    if( newcolumnnr ) {
        //  Add the offset+width of the last column: this will give
        //  the new offset!
        FITSColumn*   lastcolumn( FITSTableHDU::getColumn(newcolumnnr-1) );
        
        if( !lastcolumn )
            return -1;
        
        newcolumnoffset=lastcolumn->columnoffset()+(uLong)(lastcolumn->getWidthInBytes());
    }
    
    //  Create the new column object
    if( column2add.isMatrixColumn() )
	    newcolumn = new FITSMatrixColumn( newcolumnnr, newcolumnoffset,
					                      (const FITSMatrixColumn&)column2add, this );
    else
	    newcolumn = new FITSBinaryColumn( newcolumnnr, newcolumnoffset, column2add, this );

    //  Make space for it
    return FITSTableHDU::addColumn( (FITSColumn*)newcolumn );
}

HDUObject::hdu_t FITSBinaryTableHDU::getHDUtype( void ) const {
    return HDUObject::binarytable;
}

FITSBinaryColumn& FITSBinaryTableHDU::getColumn( const String& name, uInt occurrence ) const {
    FITSColumn*  retval( FITSTableHDU::getColumn(name,occurrence) );
    
    if( !retval )
        throw( AipsError("Column `"+name+"' not found") );
    return *((FITSBinaryColumn*)retval);
}

FITSBinaryColumn& FITSBinaryTableHDU::getColumn( uInt columnnr ) const {
    FITSColumn* retval( FITSTableHDU::getColumn(columnnr) );
    
    if( !retval )
        throw( AipsError("Column not found - index out of range") );
    return *((FITSBinaryColumn*)retval);
}

String FITSBinaryTableHDU::xtension( void ) const {
    return "BINTABLE";
}

Regex FITSBinaryTableHDU::fitsname( void ) {
    return Regex( "BINTABLE[ \t]*" );
}


//  Destruct the object!
FITSBinaryTableHDU::~FITSBinaryTableHDU()
{}


//  The protected method(s)


//  Generate the datadecsription keywords for this binary table HDU
//  object. We only need to add stuff if we have a matrix column...
FitsKeywordList FITSBinaryTableHDU::getDataDescriptionKeywords( void ) const {
    FitsKeywordList    retval( this->FITSTableHDU::getDataDescriptionKeywords() );

    //  Check all columns....
    for( uInt i=0; i<this->numberColumns(); i++ ) {
    	const FITSBinaryColumn&  ref( this->getColumn(i) );

    	if( ref.isMatrixColumn() ) {
    	    //  Ok... do all the MATRIX stuff...
    	    ostringstream            kwstrm;
    	    const FITSMatrixColumn&  mcref( (const FITSMatrixColumn&)ref );
	    
    	    retval.last();

    	    kwstrm.str( string() );
    	    kwstrm << "TMATX" << i+1;
    	    retval.mk( kwstrm.str().c_str(),
                       (Bool)True,
                       "This is the matrix column" );

    	    retval.mk( "NMATRIX", (Int)1, "One matrix per row" );
    	    retval.mk( "MAXIS", (Int)mcref.nrAxes(), "Number of axes in the matrix" );

    	    for( uInt j=0; j<mcref.nrAxes(); j++ ) {
        		const FITSMatrixAxis*   maptr( mcref.getAxis(j) );
		
        		if( maptr ) {
        		    kwstrm.str( string() );
        		    kwstrm << "MAXIS" << j+1;
    		    
        		    retval.mk( kwstrm.str().c_str(),
                               (Int)maptr->getAxisLength(),
                               "Length of this axis" );
        		}
    	    }
    	    //  BREAK! 
    	    //
    	    //  NOTE: Classic AIPS only supports one matrix column??
    	    break;
    	}
    }
    
    //
    //  Now.. append the Ordinary keywords...
    //
    //FitsKeyword*       kwptr;
    //FitsKeyword*       newkwptr;
    //FitsKeywordList    tmp( this->FITSTableHDU::getDataDescriptionKeywords() );

    //retval.last();
    //tmp.first();
    //tmp.next();
    //kwptr = tmp.curr();
    //while( kwptr )
    //{
    //newkwptr = new FitsKeyword( *kwptr );
    //retval.insert( *newkwptr );
    //kwptr = tmp.next();
    //}
    return retval;
}
