//
//  Implementation of the FITSIDIDHeader class!
//
//
//  Author: Harro Verkouter, 13-09-1999
//
//
//   $Id: FITSIDIHeader.cc,v 1.5 2006/01/13 11:35:43 verkout Exp $
//
//  $Log: FITSIDIHeader.cc,v $
//  Revision 1.5  2006/01/13 11:35:43  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.4  2004/08/25 06:02:16  verkout
//  HV: Fiddled with template shit. All templates now done automatically.
//      As a result, the source-code for the templates has to be visible
//      compiletime.
//
//  Revision 1.3  2004/01/05 15:24:18  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.2  2001/05/30 11:50:26  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:48  verkout
//  HV: Imported aips++ implement-stuff
//
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSIDIHeader.h>

using namespace std;
using namespace casa;

FITSIDIHeader::FITSIDIHeader() :
    HDUObject( TpUChar )
{
}

FITSIDIHeader::FITSIDIHeader( FitsKeywordList& kwlist ) :
    HDUObject( kwlist )
{
    FitsKeyword*   keywordptr;
    
    //
    //  Ok. Check if this is an IDI header...
    //
    if( (keywordptr=kwlist(FITS::NAXIS))==0 )
    {
        //
        //  Huh? An array HDU without NAXIS keyword!?!
        //
        throw( AipsError("No NAXIS keyword present in IDI-Header keywordlist!") );
    }
    
    //
    //  Ok. delete this keyword from the kwlist!
    //
    kwlist.del();

    //
    //  Retrieve the axis info!
    //
    Int       i;
    Int       nraxes( 2 );
    Int       valuesneeded[2] = { 777777701, 0 };
    
    for( i=0; i<nraxes; i++ )
    {
        //
        //  Let all the axes get their info from the keywordlist!
        //
	if( (keywordptr=kwlist(FITS::NAXIS, i+1))==0 )
	{
	    break;
	}
	if( keywordptr->asInt()!=valuesneeded[i] )
	{
	    break;
	}
	kwlist.del();
    }

    if( i<nraxes )
    {
	//
	//  Something wrong!!!
	//
	throw( AipsError("FITSIDIHeader: signature in keywordlist not NOT a FITS-IDI one!") );
    }

    //
    //  We're left with all the other keywords!
    //
    this->setExtraKeywords( kwlist );
    
    //
    //  Done!
    //    
}


uLong FITSIDIHeader::getCurrentDataVolumeInBytes( void ) const
{
    return 0;
}

HDUObject::hdu_t FITSIDIHeader::getHDUtype( void ) const
{
    return HDUObject::defaulthdu;
}


FITSIDIHeader::~FITSIDIHeader()
{    
}


//
//
//  The protected section...
//
//
FitsKeywordList FITSIDIHeader::getHeaderKeywords( void ) const
{
    FitsKeywordList   retval;
    
    retval.mk( FITS::SIMPLE, True, "Yep! It's a FITS file..." );
    retval.mk( FITS::BITPIX,    8 );
    return retval;
}

FitsKeywordList FITSIDIHeader::getDataDescriptionKeywords( void ) const
{
    FitsKeywordList   retval;

    retval.mk( FITS::NAXIS,     0 );
    retval.mk( FITS::EXTEND,    True );
    retval.mk( FITS::GROUPS,    True );
    retval.mk( FITS::GCOUNT,    0 );
    retval.mk( FITS::PCOUNT,    0 );
    
    //retval.mk( 1, FITS::NAXIS,  777777701, "Signature code for UV data in table" );
    //retval.mk( 2, FITS::NAXIS,          0, "No data in primary array" );

    return retval;
}
