//
//  Implementation of the FITSImageExtension class
//
//  Author:
//
//         Harro Verkouter,   26-05-1998
//
//
//     $Id: FITSImageExtension.cc,v 1.10 2011/10/12 12:45:35 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSImageExtension.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <casa/Containers/RecordDesc.h>
#include <jive/Utilities/jexcept.h>

#include <casa/Utilities/Regex.h>

using namespace casa;

FITSImageExtension::FITSImageExtension( DataType basicfitstype ) :
    FITSArrayHDU( basicfitstype ),
    itsExtensionName( "BLAH" ),
    itsExtensionVersion( 1 )
{
}


FITSImageExtension::FITSImageExtension( FitsKeywordList& kwlist, ByteIO* ptr2stream ) :
    FITSArrayHDU( kwlist, ptr2stream ),
    itsExtensionName( "BLAH" ),
    itsExtensionVersion( 1 )
{
    FitsKeyword*   keywordptr;
    
    //  Take out the extname/extver keywords!
    if( (keywordptr=kwlist(FITS::EXTNAME))!=0 ) {
        itsExtensionName=keywordptr->asString();
        kwlist.del();
    }
    
    if( (keywordptr=kwlist(FITS::EXTVER))!=0 ) {
        itsExtensionVersion=keywordptr->asInt();
        kwlist.del();
    }

    //  After taking out all the keywords for axes etc. we're left
    //  with notjing but the 'extra' keywords. Therefore we copy
    //  what's left in the keywordlist into our own list of extra
    //  keywords.
    this->setExtraKeywords( kwlist );

    //  Now's the time to make the datasegment
    this->makeDataSegment( ptr2stream );
}

HDUObject::hdu_t FITSImageExtension::getHDUtype( void ) const {
    return HDUObject::imageextension;
}

//  We use the recordinterface to pass the array. Assume the array is
//  in field 0 of the recordinterface
Bool FITSImageExtension::put( const RecordInterface& recordref )
{
    Bool             retval( False );
    
    //  datatype not compatible....
    if( recordref.type(0)!=MakeArrayType(this->getDataType()) )
        return retval;


    //  Put the complete array!
    if( !this->hasDataSegment() ) {
        //  No data allocated yet....
        this->makeDataSegment();
        
        if( !this->hasDataSegment() )
            THROW_JEXCEPT("Aargh. Failed to create datasegment for PrimaryArray!");
    }

    //  Flush the recordfield to the datasegment
    retval = flushRecordField( recordref, 0, this->getDataSegment(), 0L );
    
    return retval;
}

Bool FITSImageExtension::get( RecordInterface& recordref ) const {

    //  If no datasegment, return False
    if( !this->hasDataSegment() )
        return False;
    
    //  Make a record desc so we can restructure the user's record to
    //  match the description of this array!
    RecordDesc   thisarray;
    
    thisarray.addField( "arraydata",
		        MakeArrayType(this->getDataType()), this->shape() );
    
    try {
        recordref.restructure( thisarray );
    }
    catch( const std::exception& x ) {
        cerr << x.what() << endl;
        return False;
    }

    //  Fill the recordfield from the datasegment
    return fillRecordField( recordref, 0, this->getDataSegment(), 0L );
}


String FITSImageExtension::xtension( void ) const {
    return "IMAGE";
}

String FITSImageExtension::extname( void ) const {
    return itsExtensionName;
}

Int FITSImageExtension::extver( void ) const {
    return itsExtensionVersion;
}

Regex FITSImageExtension::fitsname( void ) {
    return Regex( "IMAGE[ \t]*" );
}



FITSImageExtension::~FITSImageExtension()
{}

FitsKeywordList FITSImageExtension::getHeaderKeywords( void ) const {
    FitsKeywordList  retval;
    
    retval.mk( FITS::XTENSION, this->xtension().c_str(), "This is an IMAGE extension" );
    retval.mk( FITS::EXTNAME,  this->extname().c_str() );
    retval.mk( FITS::EXTVER,   this->extver() );

    return retval;
}

    
