//
//  Implementation of the FITSAsciiTableHDU class
//
//
//  Author:
//
//   Harro Verkouter,   18-05-1998
//
//
//  $Id: FITSAsciiTableHDU.cc,v 1.6 2007/05/25 10:22:50 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAsciiTableHDU.h>

using namespace casa;

//  Create an AsciiTableHDU with a name and a versionnr
FITSAsciiTableHDU::FITSAsciiTableHDU( const String& name, uInt version ) :
    FITSTableHDU( name, version )
{}

FITSAsciiTableHDU::FITSAsciiTableHDU( FitsKeywordList& kwlist, ByteIO* ptr2stream ) :
    FITSTableHDU( kwlist, ptr2stream )
{
    FitsKeyword*    keywordptr;
    
    //  The c'tor of the FITSTableHDU object has taken out the values
    //  it needs. We only have to be concerned about our columns!

    //  Ok. Verify that the HDU is a binary table extension!
    if( (keywordptr=kwlist(FITS::XTENSION))==0 )
        throw( AipsError("Attempt to create TABLEextension from keywordlist,"
                         "however, no `XTENSION'-keyword found!") );

    String   tmp( keywordptr->asString() );
    
    if( !tmp.matches(FITSAsciiTableHDU::fitsname()) )
        throw( AipsError("Attempt to create TABLE hdu with incorrect XTENSION type, "
                         +String(keywordptr->asString())+" was found.") );
    
    //  Remove this keyword from the list!
    kwlist.del();

    // Extract all the columns
    if( (keywordptr=kwlist(FITS::TFIELDS))==0 )
        throw( AipsError("Attempt to create a table: number of columns is missing (TFIELDS)") );

    Int    nrofcolumns( keywordptr->asInt() );
    
    if( nrofcolumns<=0 )
        throw( AipsError("Invalid value for TFIELDS (=number of columns) encountered. Value should be >0") );
    
    //  Delete the keyword
    kwlist.del();

    //  Get out all the columns:
    uLong            offset( 0L );
    FITSAsciiColumn* ptr2column;
    
    for( Int j=0; j<nrofcolumns; j++ ) {
        //  Create a new column.
        ptr2column = new FITSAsciiColumn( j, offset, kwlist, this );

        //  Add it
        FITSTableHDU::addColumn( (FITSColumn*)ptr2column );
        
        //  do not forget to increase the offset...
        offset += ptr2column->getWidthInBytes();
    }
    
    //  We're left with all the other keywords!
    this->setExtraKeywords( kwlist );

    //  Make the datasegment!
    this->makeDataSegment( ptr2stream );
}

Int FITSAsciiTableHDU::addColumn( const FITSAsciiColumn& column2add ) {
    //  If a datasegment is present: we may not add any columns any
    //  more! (the HDU layout is "fixed")
    if( this->hasDataSegment() )
        return -1;
    
    Int          newcolumnnr;
    uLong        newcolumnoffset( 0L );
    FITSColumn*  newcolumn( 0 );

    //  Find out the column number for this new column
    newcolumnnr=this->numberColumns();
    
    //  Decide on the current offset for the newly allocated column:
    //
    //  Only have to do calculations if this is NOT the first column
    //  (i.e. newcolumnnr!=0)
    if( newcolumnnr ) {
        //  Add the offset+width of the last column: this will give
        //  the new offset!
        FITSColumn*   lastcolumn( FITSTableHDU::getColumn(newcolumnnr-1) );
        
        if( !lastcolumn )
            return -1;
        
        newcolumnoffset = lastcolumn->columnoffset()+(uLong)(lastcolumn->getWidthInBytes());
    }
    
    //  Create the new column object
    newcolumn = new FITSAsciiColumn( newcolumnnr, newcolumnoffset, column2add, this );
    
    //  Make space for it
    return FITSTableHDU::addColumn( (FITSColumn*)newcolumn );
}



HDUObject::hdu_t FITSAsciiTableHDU::getHDUtype( void ) const {
    return HDUObject::asciitable;
}

FITSAsciiColumn& FITSAsciiTableHDU::getColumn( const String& name, uInt occurrence ) const {
    FITSColumn*  retval( FITSTableHDU::getColumn(name,occurrence) );
    
    if( !retval )
        throw( AipsError("Column `"+name+"' not found") );
    return *((FITSAsciiColumn*)retval);
}

FITSAsciiColumn& FITSAsciiTableHDU::getColumn( uInt columnnr ) const {
    FITSColumn* retval( FITSTableHDU::getColumn(columnnr) );
    
    if( !retval )
        throw( AipsError("Column not found - index out of range") );
    return *((FITSAsciiColumn*)retval);
}


String FITSAsciiTableHDU::xtension( void ) const {
    return "TABLE";
}

Regex FITSAsciiTableHDU::fitsname( void ) {
    return Regex( "TABLE[ \t]*" );
}


//  Destruct the object!
FITSAsciiTableHDU::~FITSAsciiTableHDU()
{}
