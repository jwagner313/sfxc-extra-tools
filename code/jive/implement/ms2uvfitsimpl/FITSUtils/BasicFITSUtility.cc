//
//  Implementation of the abstract BasicFITSUtility class.
//
//
//  Author:
//
//        Harro Verkouter      24-3-1998
//
//  $Id: BasicFITSUtility.cc,v 1.6 2006/02/10 08:53:45 verkout Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/BasicFITSUtility.h>
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <casa/Exceptions.h>
#include <casa/BasicSL/String.h>

using namespace casa;
//
//  Empty default c'tor
//
BasicFITSUtility::BasicFITSUtility() :
    itsParentHDUptr( 0 ),
    itsActualType( BasicFITSUtility::none )
{
}

//
//  Create a BasicFITSUtility with a parent!
//
BasicFITSUtility::BasicFITSUtility( HDUObject* const ptr2parent ) :
    itsParentHDUptr( ptr2parent ),
    itsActualType( BasicFITSUtility::hduObjectPtr )
{
    //
    //  Nothing more to do!
    //
}

BasicFITSUtility::BasicFITSUtility( FITSBinaryColumn* const ptr2parent ) :
    itsParentHDUptr( (BasicFITSUtility* const)ptr2parent ),
    itsActualType( BasicFITSUtility::binaryColumnPtr )
{
}

BasicFITSUtility::BasicFITSUtility( const BasicFITSUtility& other ):
    itsParentHDUptr( other.itsParentHDUptr ),
    itsActualType( other.itsActualType )
{
}

//
//  Attempt to get the requested parent (if possible). Otherwise an
//  exception is thrown.
//
//  The return value is there to keep the compiler happy: it failed to
//  recognize the throw as a keyword and hence complained that the
//  function did not return a value... :-(, so we give it one!
//
HDUObject& BasicFITSUtility::getParentHDU( void )
{
    if( itsParentHDUptr && itsActualType==BasicFITSUtility::hduObjectPtr )
    {
        return *((HDUObject*)itsParentHDUptr);
    }
    throw( AipsError("FITSUtility has no parent HDU associated. U tried to get it anyway!") );
    return *( (HDUObject*)0 );
}

FITSBinaryColumn& BasicFITSUtility::getParentBinaryColumn( void )
{
    if( itsParentHDUptr && itsActualType==BasicFITSUtility::binaryColumnPtr )
    {
        return *((FITSBinaryColumn*)itsParentHDUptr);
    }
    throw( AipsError(String("FITSUtility has no parent BinaryColumn associated.") + 
		     String(" U tried to get it anyway!")) );
    return *( (FITSBinaryColumn*)0 );
}

//
//  the const versions
//
const HDUObject& BasicFITSUtility::getParentHDU( void ) const
{
    if( itsParentHDUptr && itsActualType==BasicFITSUtility::hduObjectPtr )
    {
        return *((const HDUObject*)itsParentHDUptr);
    }
    throw( AipsError("FITSUtility has no parent HDU associated. U tried to get it anyway!") );
    return *( (const HDUObject*)0 );
}

const FITSBinaryColumn& BasicFITSUtility::getParentBinaryColumn( void ) const
{
    if( itsParentHDUptr && itsActualType==BasicFITSUtility::binaryColumnPtr )
    {
        return *((const FITSBinaryColumn*)itsParentHDUptr);
    }
    throw( AipsError(String("FITSUtility has no parent BinaryColumn associated.") +
		     String(" U tried to get it anyway!")) );
    return *( (const FITSBinaryColumn*)0 );
}


//
//  Question if the object has a parent of the requested type...
//
Bool BasicFITSUtility::hasParentHDU( void ) const
{
    return ( itsParentHDUptr!=0 && itsActualType==BasicFITSUtility::hduObjectPtr );
}

Bool BasicFITSUtility::hasParentBinaryColumn( void ) const
{
    return ( itsParentHDUptr!=0 && itsActualType==BasicFITSUtility::binaryColumnPtr );
}

//
//  The destructor!
//
BasicFITSUtility::~BasicFITSUtility()
{
}
