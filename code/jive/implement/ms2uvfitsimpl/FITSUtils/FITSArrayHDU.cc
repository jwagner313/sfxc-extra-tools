//
//  Implementation of the FITSArrayHDU class
//
//  Author:
//
//        Harro Verkouter    06-04-1998
//
//        $Id: FITSArrayHDU.cc,v 1.5 2006/01/13 11:35:42 verkout Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSArrayHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <casa/Containers/RecordInterface.h>
#include <casa/Containers/RecordDesc.h>

using namespace std;
using namespace casa;

//
//  Default FITSArrayHDU
//
FITSArrayHDU::FITSArrayHDU( const DataType basicfitstype ) :
    HDUObject( basicfitstype )
{
}

//
//  Construct the FITSArrayHDU object from a keywordlist that
//  (supposedly) has just been read from a file!
//
FITSArrayHDU::FITSArrayHDU( FitsKeywordList& kwlist, ByteIO* ) :
    HDUObject( kwlist )
{
    FitsKeyword*   keywordptr;
    
    //
    //  Ok. Reconstruct the ArrayHDU object from the keywordlist: find
    //  out all the axis!
    //
    if( (keywordptr=kwlist(FITS::NAXIS))==0 )
    {
        //
        //  Huh? An array HDU without NAXIS keyword!?!
        //
        throw( AipsError("No NAXIS keyword present in ArrayHDU keywordlist!") );
    }
    
    //
    //  Allocate spaces for the axisptrs!
    //
    Int   nraxes( keywordptr->asInt() );
    
    itsAxisptrs.resize( nraxes );

    //
    //  Ok. delete this keyword from the kwlist!
    //
    kwlist.del();

    //
    //  Retrieve the axis info!
    //
    for( Int i=0; i<nraxes; i++ )
    {
        //
        //  Let all the axes get their info from the keywordlist!
        //
        itsAxisptrs(i)=new FITSAxis( i, kwlist, this );
    }

    //
    //  Done!
    //
}



//
//  Add the axis to this HDU. -1 is returned in case an error occurrs,
//  the assigned axis number otherwise.
//
Int FITSArrayHDU::addAxis( const FITSAxis& axis )
{
    //
    //  If datasegment is non-null, it has already been allocated ->
    //  therefore we cannot add any more axes.
    //
    if( this->hasDataSegment() )
    {
        return -1;
    }
    

    Int         curaxisnr;
    FITSAxis*   newaxisptr( 0 );
    
    //
    //  Step one: in order to determine the correct number for this
    //  axis, we first find out how many axes there already are.
    //
    itsAxisptrs.shape( curaxisnr );
    
    //
    //  Now we know that, we can safely create the new axisobject with
    //  the proper settings
    //
    newaxisptr= new FITSAxis( curaxisnr, axis, this );
    
    //
    //  Make room for this one...
    //
    itsAxisptrs.resize( curaxisnr+1, True );
    
    //
    //  Copy it in
    //
    itsAxisptrs( curaxisnr )=newaxisptr;
    
    //
    //  Return success...
    //
    return curaxisnr;
}


uLong FITSArrayHDU::getCurrentDataVolumeInBytes( void ) const
{
    Int        nraxis;
    uLong      datavolume( (uLong)this->getDataSizeInBytes() );
    
    itsAxisptrs.shape( nraxis );
    
    for( Int i=0; i<nraxis; i++ )
    {
        datavolume*=( (uLong)(itsAxisptrs(i)->getAxisLength()) );
    }
    
    return datavolume;
}


//
//  The ArrayHDU differs from the basic HDUObject in that that it
//  describes some data!
//
FitsKeywordList FITSArrayHDU::getDataDescriptionKeywords( void ) const
{
    Int             nraxis;
    FitsKeywordList retval;

    //
    //  Loop over all axes....
    //
    itsAxisptrs.shape( nraxis );
    
    retval.mk( FITS::NAXIS, nraxis );

    //
    //  Fill in all NAXIS-keywords
    //
    for( Int j=0; j<nraxis; j++ )
    {
        retval.mk( j+1, FITS::NAXIS, itsAxisptrs(j)->getAxisLength() );
    }


    for( Int i=0; i<nraxis; i++ )
    {
        FitsKeyword*       keywordptr;
        FitsKeyword*       newkeywordptr;
        FitsKeywordList    axiskw( itsAxisptrs(i)->asFITSKeys() );
        
        axiskw.first();
        axiskw.next();
        keywordptr=axiskw.curr();
        
        while( keywordptr!=0 )
        {
            newkeywordptr=new FitsKeyword( *keywordptr );
            
            retval.insert( *newkeywordptr );
            keywordptr=axiskw.next();
        }
    }

    return retval;
}


//
//  Return the shape of the array!
//
IPosition FITSArrayHDU::shape( void ) const
{
    Int        nraxes;

    itsAxisptrs.shape( nraxes );

    IPosition  retval( nraxes, 0 );
    
    for( Int i=0; i<nraxes; i++ )
    {
        retval(i)=itsAxisptrs(i)->getAxisLength();
    }
    return retval;
}

Bool FITSArrayHDU::axisExists( const String& axistype ) const
{
    Int    nraxes;
    Bool   retval( True );

    itsAxisptrs.shape( nraxes );
    
    for( Int i=0; i<nraxes; i++ )
    {
        if( itsAxisptrs(i)->ctype()==axistype )
        {
            retval=True;
            break;
        }
    }
    return retval;
}


FITSAxis& FITSArrayHDU::getAxis( const String& axistype ) const
{
    Int        nraxes;
    FITSAxis*  ptr2retval( 0 );


    itsAxisptrs.shape( nraxes );
    
    for( Int i=0; i<nraxes; i++ )
    {
        if( itsAxisptrs(i)->ctype()==axistype )
        {
            ptr2retval=itsAxisptrs(i);
        }
    }

    if( !ptr2retval )
    {
        throw( AipsError("Sorry, the axis `"+axistype+"' does not exist!") );
    }
    return *ptr2retval;
}

FITSAxis& FITSArrayHDU::getAxis( uInt index ) const
{
    if( index>=itsAxisptrs.nelements() )
    {
	throw( AipsError("Sorry, axis index out of range!") );
    }
    return *(itsAxisptrs(index));
}


//
//  Delete the array's resources
//
FITSArrayHDU::~FITSArrayHDU()
{
    Int    counter;
    
    //
    //  Delete all FITSAxis'
    //
    itsAxisptrs.shape( counter );
    
    for( Int i=0; i<counter; i++ )
    {
        delete itsAxisptrs( i );
    }

    //
    //  And we're done!
    //
}

