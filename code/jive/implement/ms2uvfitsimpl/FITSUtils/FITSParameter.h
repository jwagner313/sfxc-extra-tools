//
//  Class:
//
//        FITSParameter
//
//
//  Purpose:
//
//        This class wil describe the basic properties of a parameter
//        in the description of a random group structure in a
//        FITS-file. The class is not templated, but when adding it to
//        a random group hdu, the datatypes must match.
//
//        
//
//
//  Author:
//
//        Harro Verkouter     24-3-1998
//
//
//      $Id: FITSParameter.h,v 1.5 2007/01/17 08:22:01 jive_cc Exp $
//
#ifndef FITSPARAMETER_H_INC
#define FITSPARAMETER_H_INC


#include <jive/ms2uvfitsimpl/FITSUtils/BasicFITSUtility.h>
#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <fits/FITS.h>
#include <casa/Containers/RecordInterface.h>
#include <casa/Containers/RecordFieldId.h>


class FITSPrimaryGroup;

class FITSParameter :
    public BasicFITSUtility
{
public:
    //
    //  Three constructors.
    //
    //  1) Create a complete parameter -> user has control over all
    //  parameter properties (but the parameter number) AND THE
    //  PARAMETER TYPE. So if you've just added a parameter to a
    //  PrimaryGroup and you want to get/put a/some values, get a
    //  reference to the parameter from the PrimaryGroup HDU rather
    //  than the object you used to add to the HDU! This is done to
    //  ensure that the parameter type of the parameters in the
    //  PrimaryGroup HDU match with the type of the HDU.
    //
    //  2) Create a parameter from a FitsKeywordlist -> intended use:
    //  attempt to read the parameters settings from the list of
    //  keywords just read from file!
    //
    //  3) A kind of copy-constructor -> the parameter settings of
    //  'other' are copied across apart from the parameternumber. This
    //  constructor finds its use when a HeaderDataUnit needs to add a
    //  parameter. The user specifies a parameter that needs to be
    //  added to the HeaderdataUnit. The user does not know the actual
    //  number of the parameter. The HeaderdataUnit does know and uses
    //  this c'tor to create the correct parameter.
    //
    FITSParameter( const casa::String& type, const casa::String& comment="", 
                   casa::Double pscale=1.0, casa::Double pzero=0.0 );
    
    //
    //  In the following two c'tors, the parameter data type is taken
    //  from the parent!
    //
    FITSParameter( casa::Int number, casa::FitsKeywordList& kwsource, FITSPrimaryGroup* const ptr2parent );
    
    FITSParameter( casa::Int number, const FITSParameter& other, FITSPrimaryGroup* const ptr2parent );
   

    FITSParameter( const FITSParameter& other );
    
    //
    //  Translate this parameter to Fitskeywords
    //
    virtual casa::FitsKeywordList asFITSKeys( void ) const;

    //
    //  Get the parameter value for this parameter for random group
    //  #idx. 
    //
    //  NOTE: the result of the get-action will be stored in the first
    //  field of the record. In order to achieve that, the record will
    //  be first restructured to hold axactly one field of the
    //  internal datatype!
    //
    //  NOTE: when the record re-structuring does not succeed, the
    //  exception is caught within the function. A value of False is
    //  returned to the user.
    //
    casa::Bool         get( casa::uLong idx, casa::RecordInterface& riref, const casa::RecordFieldId& fieldidx=0 ) const;

    casa::Bool         put( casa::uLong idx, const casa::RecordInterface& riref, const casa::RecordFieldId& fieldidx=0 );


    //
    //  Get access to some of the properties of the parameter, READ
    //  ONLY and fits nomenclature
    //
    casa::String       ptype( void ) const;
    casa::Double       pscal( void ) const;
    casa::Double       pzero( void ) const;
    

    //
    //  Destruct this parameter
    //
    virtual ~FITSParameter();

private:
    //
    //  These are the variables that describe the parameter
    //

    //
    //  The parameter type
    //
    casa::String         itsParametertype;
    
    //
    //  The comment
    //
    casa::String         itsComment;
    
    //
    //  If appropriate: which parameternumber is this?
    //
    casa::Int            itsParameternumber;

    casa::Double         itsScale;
    
    casa::Double         itsZeropoint;

    //
    // Save the current basic fits type in here..., describes what
    // format the parameter should take.
    //
    casa::DataType       itsDataType;


    //
    //  The parameter's private methods
    //

    //
    //  We do not allow copying and assigning to parameters, nor do we
    //  allow default parameters!
    //
    FITSParameter();
    FITSParameter& operator=( const FITSParameter& );
    
};


#endif
