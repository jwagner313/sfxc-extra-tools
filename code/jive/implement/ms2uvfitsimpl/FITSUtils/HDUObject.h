//
// Class:
// 
//        HDUObject
//
// Purpose:
//
//        This class serves as a placeholder for the real
//        HeaderDataUnit object. For now, it may be just a
//        typedef. All the code under 'ms2uvfitsimpl' uses this class
//        to refer to any type of HDU object. Since everything is done
//        by reference (or pointer sometimes) the virtual function
//        mechanism will take care of the fact that everything will be
//        written as it should.
//
// Note:
//
//        It is possible to create a default HDUObject (it's not an
//        abstract base-class) in this case, the HDUObject describes a
//        null-hdu containing no data. This can be used to start a
//        valid FITS-file with extensions.
//
// Author:
//
//        Harro Verkouter     02-04-1998
//
//
//        $Id: HDUObject.h,v 1.5 2007/04/11 12:01:47 jive_cc Exp $
//
#ifndef HDUOBJECT_H_INC
#define HDUOBJECT_H_INC

#include <jive/ms2uvfitsimpl/FITSUtils/BasicFITSUtility.h>
#include <fits/FITS/fitsio.h>
#include <fits/FITS.h>
#include <casa/Utilities/DataType.h>
#include <casa/IO/ByteIO.h>
#include <casa/Exceptions.h>

#include <iostream>

//
//  We can get away with these forward declarations, since we don't
//  use the objects in the member functions.
//
class FITSDataSegment;

class HDUObject :
public BasicFITSUtility
{
     //
     //  The output operator/insertion operator is a friend of ours. We
     //  would rather have it that it was an acqaintance but then
     //  again....
     //
     friend casa::ByteIO& operator<<( casa::ByteIO& bioref, const HDUObject& hduref );

 public:

     //
     //  The type of the hdu: derived classes are expected to implement
     //  the 'getHDUtype()' function, so users can succesfully upcast
     //  pointers!
     //
     enum hdu_t 
     {
	  defaulthdu, primaryarray, primarygroup, asciitable, binarytable, imageextension 
     };
    
    

     //
     //  A default HDUObject!
     //
     //  The 'basicfitstype' is passed on to the
     //  BasicFITSUtility-class, which knows which datatype it
     //  represents!
     //
     //  NOTE: allowed DataTypes are (between parenthesis, the
     //  associated value of the BITPIX keyword):
     //
     //  TpUChar  (BITPIX=8)
     //  TpShort  (BITPIX=16)
     //  TpInt    (BITPIX=32)
     //  TpFloat  (BITPIX=-16)
     //  TpDouble (BITPIX=-32)
     //
     //  If the type is other than these, an exception will be thrown!
     //
     HDUObject( const casa::DataType basicfitstype );
    
    //
    //  Create the HDUObject from a keywordlist (presumably just read
    //  from a file). The c'tor doesn't do much, apart from trying to
    //  figure out the bitpix datatype and set the internal value
    //  accordingly!
    //
     HDUObject( casa::FitsKeywordList& kwlist );
    

     //
     //  Get a description of this HDUObject in FitsKeywords!...well
     //  actually, let's split it up into different sections.
     //
     //  I.m.h.o there are three categories of FITSKeywords in a HDU:
     //
     //  1) The header keywords (like 'SIMPLE=' or
     //  'EXTENSION='+'EXTNAME=' ),
     //
     //  2) The data descriptor keywords; for Array HDU's the
     //  'NAXIS='+'NAXISn=' keywords, for table HDU's the 2 axes of the
     //  table + all column descriptions,
     //
     //  3) The extra keywords, including comment etc.
     //
     //  So if a particular HDUObject is requested to translate itself
     //  into FITSKeys (via the non-virtual 'asFITSKeys()' method), it
     //  will call these three virtual methods. Derived objects can
     //  fill in the requested fitskeywords at their own will/at their
     //  own responsibility.
     //
     //  Note: derived HDU objects should not end the keywordlist, the
     //  base-class 'asFITSKeys()' method does that already.
     //
     //  Note: the virtual methods are protected in the base-class. It
     //  is advised for derived classes to keep them protected as
     //  well. Unless you want to be able to check the result of the
     //  individual function calls of course.
     //
     casa::FitsKeywordList asFITSKeys( void ) const;
    
     //
     //  Return the data-size; basically this should return the
     //  absolute value of BITPIX divided through by 8.
     //
     casa::uInt            getDataSizeInBytes( void ) const;

    //
    //  HDU's may describe some amount of data: let them thell how
    //  many bytes they currently describe!
    //
     virtual casa::uLong   getCurrentDataVolumeInBytes( void ) const;

     //
     // Return the basic fitstype of this HDU!
     //
     casa::DataType        getDataType( void ) const;


     //
     // Add the keyword to the list of extrakeywords.
     //
     // NOTE: the keyword is copied (space is allocated) since the
     // FitsKeywordList adds a reference (or pointer) to itself rather
     // than copying it itself, so we do it for it!
     //
     void            addExtraKeyword( casa::FitsKeyword& newkeyword );
     void            removeExtraKeyword( casa::FitsKeyword& keyword2del );
    
    //
    //  Return the type of the HDU
    //
     virtual HDUObject::hdu_t  getHDUtype( void ) const;
    
     //
     //  In order to do a proper cleanup after an exception is thrown,
     //  we implement this function
     //
     void             cleanup();

    //
    //  Destruct this object...
    //
     virtual ~HDUObject();


 protected:
     //********************************************************************************/
     //
     //                         The protected methods
     //
     //********************************************************************************/

     virtual casa::FitsKeywordList getHeaderKeywords( void ) const;
    
     virtual casa::FitsKeywordList getDataDescriptionKeywords( void ) const;
    
     //
     //  We must provide for an interface to the list of extra
     //  keywords. Since every HDU object is capable of holding a list
     //  of extra keywords, it is unnecessary to let every derived HDU
     //  have their own list of keywords, so we keep it here and
     //  provide them with an interface to modify/inspect the list!
     //

    //
    //  Overwrite the existing list
    //
     void                    setExtraKeywords( casa::FitsKeywordList& newlist );

     //
     //  If the user wants access: only read-only access is granted!
     //
     casa::ConstFitsKeywordList    getExtraKeywords( void ) const;
    

     //
     //  We also need an interface to the FITSDataSegment, since every
     //  HDUObject is entitled to have their own DataSegment.
     //
     //  NOTE: The derived class is responsible for telling the
     //  base-class (this class) to make the datasegment. The
     //  base-class does not know when the time has come to create the
     //  datasegment. So first let the derived classes determine how
     //  big the datasegment is; hence they can call the
     //  makeDataSegment-methods.
     //
     //  NOTE: The makeDataSegment - functions call the
     //  'getCurrentDataVolumeInBytes()' method in order to determine
     //  the size of the segment needed. So the derived classes should
     //  return the size they represent.
     //
     //  NOTE: This method also calls the 'getDataType()'
     //  method. Usually that should not be any problem since the
     //  creator of HDUObject checks for validity of the DataType that
     //  is requested.
     //
     void                     makeDataSegment( void );
     void                     makeDataSegment( casa::ByteIO* ptr2stream );
    
    //
    //  This method tells us if this HDU has a DataSegment attached or
    //  not!
    //
     casa::Bool                     hasDataSegment( void ) const;
    
     //
     //  This returns the datasegment. Throws an exception if there's
     //  no datasegment yet!
     //
     FITSDataSegment&         getDataSegment( void );
     const FITSDataSegment&   getDataSegment( void ) const;
    
     //
     //  Dump it's data on the requested ByteIO stream......
     //
     casa::Bool                     writeDataSegment( casa::ByteIO& bioref ) const;



 private:
     //
     //  No private datamembers (yet)
     //
    
     //
     //  Yes: the basic fits type. HDUObjects describe arrays or
     //  matrices of a certain datatype. Which data type it is, this
     //  variable describes it:
     //
     //  This DataType should control the BITPIX keyword value. See the
     //  comment with the constructor of this class for details.
     //
     casa::DataType         itsBasicDataType;

    //
    //  The lits of extra keywords
    //
     casa::FitsKeywordList  itsExtraKeywords;
    
     //
     //  And the pointer to the datasegment (if any!)
     //
     FITSDataSegment* itsDataSegmentptr;
    

     //
     //  Private methods
     //

    //
    //  Disable assigning/copying!
    //
     HDUObject( const HDUObject& );
     HDUObject& operator=( const HDUObject& );
};


//
//  The global 'output' functions
//
casa::FitsOutput& operator<<( casa::FitsOutput& fitsoutputref,
			      const HDUObject& hduref );
casa::ByteIO& operator<<( casa::ByteIO& bioref,
			  const HDUObject& hduref );
std::ostream& operator<<( std::ostream& osref,
			  const HDUObject& hduref );

std::ostream& operator<<( std::ostream& osref,
			  const HDUObject::hdu_t& hduref );


//
//  And the global 'input' functions
//
//  NOTE: these functions take a reference to a pointer to a
//  HDUObject. The idea is that this gloabl function attempts to read
//  a keywordlist from the inputobject. The function 'parses' the
//  keywordlist and tries to decide what kind of HDUObject is
//  described. If that is succesfull, a correct HDUObject can be
//  created from the keywordlist. The HDUObject parses the list
//  further to extract all the info it needs!
//
//  The pointer to the newly allocated HDUObject is returned in the
//  functionparameter. Upcasts from HDUObject* can be done by checking
//  the return value of the virtual 'getHDUtype()' method. Derived
//  classes should implement this method in order to identify
//  themselves...
//
casa::FitsInput& operator>>( casa::FitsInput& fitsinputref,
			     HDUObject*& ref2hduptr );
casa::ByteIO& operator>>( casa::ByteIO& bioref,
			  HDUObject*& ref2hduptr );
std::istream& operator>>( std::istream& isref,
			  HDUObject*& ref2hduptr );



#endif
