//
//   Implementation of the FITSParameter class
//
//
//   Author:
//
//         Harro Verkouter     24-3-1998
//
//
//    $Id: FITSParameter.cc,v 1.8 2006/03/02 14:21:41 verkout Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <casa/Containers/RecordDesc.h>
#include <casa/Containers/RecordFieldId.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSPrimaryGroup.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>

#include <sstream>

using namespace std;
using namespace casa;

//
//  Do construct a complete parameter!
//
FITSParameter::FITSParameter( const String& type, const String& comment, 
                              Double pscale, Double pzero ) :
    BasicFITSUtility(),
    itsParametertype( type ),
    itsComment( comment ),
    itsParameternumber( -1 ),
    itsScale( pscale ),
    itsZeropoint( pzero ),
    itsDataType( TpOther )
{
}

FITSParameter::FITSParameter( Int number, FitsKeywordList& kwsource, FITSPrimaryGroup* const ptr2parent ) :
    BasicFITSUtility( ptr2parent ),
    itsParametertype( "" ),
    itsComment( "" ),
    itsParameternumber( number ),
    itsScale( 1.0 ),
    itsZeropoint( 0.0 ),
    itsDataType( ((ptr2parent!=0)?(ptr2parent->getDataType()):(TpOther)) )
{
    //
    //  Check if a valid datatype could be gotten....
    //
    if( !::isBasicFitsType(itsDataType) )
    {
        ostringstream    tmp;
        
        tmp << "Sorry, but datatype " << itsDataType << " is not allowed "
             << "for FITSParameters";
        
        throw( AipsError(tmp.str()) );
    }


    //
    //  Attempt to extract the values for parameter nr
    //  'itsParameternumber' from the FitsKeywordList!
    //
    FitsKeyword*   keywordptr;
    
    //
    //  Parse the keywordlist and attempt to extract all the info we
    //  need
    //
    if( (keywordptr=kwsource(FITS::PTYPE, itsParameternumber+1))!=0 )
    {
        itsParametertype=keywordptr->asString();
        itsComment=keywordptr->comm();
        
        //
        //  Delete this keyword from the list!
        //
        kwsource.del();
    }

    //
    //  The scale of the parameter
    //
    if( (keywordptr=kwsource(FITS::PSCAL, itsParameternumber+1))!=0 )
    {
        itsScale=keywordptr->asDouble();
        
        //
        //  Delete this keyword from the list!
        //
        kwsource.del();
    }

    //
    //  The zero-point
    //
    if( (keywordptr=kwsource(FITS::PZERO_FITS, itsParameternumber+1))!=0 )
    {
        itsZeropoint=keywordptr->asDouble();
        
        //
        //  Delete this keyword from the list!
        //
        kwsource.del();
    }
}


FITSParameter::FITSParameter( Int number, const FITSParameter& other, FITSPrimaryGroup* const ptr2parent ) :
    BasicFITSUtility( ptr2parent ),
    itsParametertype( other.itsParametertype ),
    itsComment( other.itsComment ),
    itsParameternumber( number ),
    itsScale( other.itsScale ),
    itsZeropoint( other.itsZeropoint ),
    itsDataType( ((ptr2parent!=0)?(ptr2parent->getDataType()):(TpOther)) )
{
    //
    //  Done: nothing left to do! Well.....
    //

    //
    //  Check if a valid datatype could be gotten....
    //
    if( !::isBasicFitsType(itsDataType) )
    {
        ostringstream    tmp;
        
        tmp << "Sorry, but datatype " << itsDataType << " is not allowed "
             << "for FITSParameters";
        
        throw( AipsError(tmp.str()) );
    }

}

FITSParameter::FITSParameter( const FITSParameter& other ):
    BasicFITSUtility( (const BasicFITSUtility&)other ),
    itsParametertype( other.itsParametertype ),
    itsComment( other.itsComment ),
    itsParameternumber( other.itsParameternumber ),
    itsScale( other.itsScale ),
    itsZeropoint( other.itsZeropoint ),
    itsDataType( other.itsDataType )
{
    //
    //  Done: nothing left to do! Well.....
    //
}

FitsKeywordList FITSParameter::asFITSKeys( void ) const
{
    FitsKeywordList    retval;

    //
    // Ok. Translate this parameter into fitskeys!
    //
    retval.mk( itsParameternumber+1, FITS::PTYPE,      itsParametertype.c_str(),
													   itsComment.c_str() );
    retval.mk( itsParameternumber+1, FITS::PSCAL,      itsScale );
    retval.mk( itsParameternumber+1, FITS::PZERO_FITS, itsZeropoint );

    return retval;
}

Bool FITSParameter::get( uLong idx, RecordInterface& riref, const RecordFieldId& fieldidx ) const
{
    Bool        retval( False );

    //
    //  Let's check if we have a parent 
    //
    if( !this->hasParentHDU() )
    {
        return retval;
    }

    FITSPrimaryGroup&   ref2parent( (FITSPrimaryGroup&)(this->getParentHDU()) );
    
    //
    //  Check if the parent has a datasegment associated...?
    //
    if( !ref2parent.hasDataSegment() )
    {
        return retval;
    }
    

    //
    //  Check if the indicated recordfield indeed contains a field of
    //  the type of this parametertype
    //
    if( riref.dataType(fieldidx)!=itsDataType )
    {
        return retval;
    }
    
    //
    //  Now determine the offset in the datablock where we can find
    //  the parameter
    //
    uLong       totaloffset;
    
    totaloffset= (idx*(ref2parent.getGroupSizeInBytes()))+
        ((uLong)itsParameternumber*((uLong)ref2parent.getDataSizeInBytes()));
    

    //
    // Fill the recordfield from the datasegment
    //
    retval=fillRecordField( riref, fieldidx, ref2parent.getDataSegment(), totaloffset );
    
    return retval;
}


Bool FITSParameter::put( uLong idx, const RecordInterface& riref, const RecordFieldId& fieldidx )
{
    Bool    retval( False );


    //
    //  Let's check if we have a parent 
    //
    if( !this->hasParentHDU() )
    {
        return retval;
    }

    FITSPrimaryGroup&   ref2parent( (FITSPrimaryGroup&)(this->getParentHDU()) );
    
    //
    //  Check if the parent has a datasegment associated...?
    //
    if( !ref2parent.hasDataSegment() )
    {
        ref2parent.makeDataSegment();
        
        if( !ref2parent.hasDataSegment() )
        {
            return retval;
        }
    }
    

    //
    //  Check if the indicated recordfield indeed contains a field of
    //  the type of this parametertype
    //
    if( riref.dataType(fieldidx)!=itsDataType )
    {
        return retval;
    }
    
    //
    //  Now determine the offset in the datablock where we can find
    //  the parameter
    //
    uLong       totaloffset;
    
    totaloffset= (idx*(ref2parent.getGroupSizeInBytes()))+
        ((uLong)itsParameternumber*((uLong)ref2parent.getDataSizeInBytes()));
    
    //
    //  Flush the recordfield to the datasegment
    //
    retval=flushRecordField( riref, fieldidx, ref2parent.getDataSegment(), totaloffset );

    //
    // We can return!
    //
    return retval;
}

String FITSParameter::ptype( void ) const
{
    return itsParametertype;
}

Double FITSParameter::pscal( void ) const
{
    return itsScale;
}

Double FITSParameter::pzero( void ) const
{
    return itsZeropoint;
}


//
//  Destruct the parameter (nothing to do yet)
//
FITSParameter::~FITSParameter()
{
}


//********************************************************************************/
//
//
//                      The FITSParameter private methods
//
//
//********************************************************************************/


