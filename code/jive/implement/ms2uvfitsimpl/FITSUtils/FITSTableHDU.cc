//
//  Implementation of the FITSTableHDU class
//
//
//  Author:
//
//        Harro Verkouter,  06-04-1998
//
//
//        $Id: FITSTableHDU.cc,v 1.10 2011/10/12 12:45:35 jive_cc Exp $
//
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSTableHDU.h>
#include <casa/Utilities/DataType.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <casa/Containers/Record.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/Utilities/jexcept.h>

#include <casa/Containers/Block.h>

using namespace casa;

//
//  HDU's have no parents! and they Table's are constituted of
//  byte-sized data!
//
FITSTableHDU::FITSTableHDU( const String& name, uInt version ) :
    HDUObject( TpUChar ),
    itsTableName( name ),
    itsTableVersion( version )
{
    //
    //  Since this is the table-HDU-base class, we can make sure that
    //  the table has two axes; one is the width of the table (the sum
    //  of all the individual columns + the product of the matrixaxes
    //  of any matrices following the columns) and the other the
    //  number of entries in the table. We already create both
    //  axes. The lengths will be updated when columns/rows are added
    //
    itsAxisptrs.resize( 2 );
    
    itsAxisptrs(0)=new FITSAxis( "rowwidth", "Width of table in bytes", 0 );
    itsAxisptrs(1)=new FITSAxis( "nrrows", "Number of raows in the table", 0 );    
}

FITSTableHDU::FITSTableHDU( FitsKeywordList& kwlist, ByteIO* ) :
    HDUObject( TpUChar ),
    itsTableName( "" ),
    itsTableVersion( 0 )
{
    FitsKeyword*   keywordptr;
    
    //
    //  Get out the extname and extver-values
    //
    if( (keywordptr=kwlist(FITS::EXTNAME))!=0 )
    {
        itsTableName=keywordptr->asString();
        kwlist.del();
    }

    if( (keywordptr=kwlist(FITS::EXTVER))!=0 )
    {
        itsTableVersion=keywordptr->asInt();
        kwlist.del();
    }
    

    //
    //  Get the current rowwidth/nrrows
    //
    if( (keywordptr=kwlist(FITS::NAXIS))==0 )
    {
        THROW_JEXCEPT("TABLE hdu creation: no NAXIS keyword found");
    }
    
    if( keywordptr->asInt()!=2 )
    {
        //
        //  NAXIS != 2 ?!!
        //
        THROW_JEXCEPT("Keyword NAXIS has incorrect value "
                << keywordptr->asInt() << " (should be 2)");
    }
    
    //
    //  Delete the NAXIS keyword, it's been checked!
    //
    kwlist.del();
    
    //
    //  Get the axis private parts
    //
    itsAxisptrs.resize( 2 );    

    //
    //  This will already will in the table row width (therefore, from
    //  this moment on, getCurrentDataVolumeInBytes() will give the
    //  correct return value!
    //
    for( Int i=0; i<2; i++ )
    {
        itsAxisptrs(i)=new FITSAxis( i, kwlist, (FITSArrayHDU*)0 );
    }
    

    //
    //  Done!
    //
}



Int FITSTableHDU::addColumn( FITSColumn* columnptr )
{
    Int     nrcolumns;

    //
    //  If a datasegment is present: we may not add any columns any
    //  more!
    //
    if( this->hasDataSegment() )
    {
        return -1;
    }
    
    //
    //  Make space for it
    //
    itsColumnptrs.shape( nrcolumns );
    
    itsColumnptrs.resize( nrcolumns+1, True );
    
    //
    //  Copy the pointer into the vector
    //
    itsColumnptrs( nrcolumns )=columnptr;
    
    //
    //  We need to update the total table width?
    //
    itsAxisptrs(0)->setAxisLength( this->getCurrentRowWidthInBytes() );

    //
    // And of course the table row description needs to be updated!
    //
    itsRowDescription.merge( columnptr->getColumnDescription(), RecordInterface::RenameDuplicates );

    //
    //  And we're done
    //
    return (nrcolumns+1);
}

//
//  How big is the table?
//
uLong FITSTableHDU::getCurrentDataVolumeInBytes( void ) const
{
    return (((uLong)getCurrentRowWidthInBytes())*((uLong)itsAxisptrs(1)->getAxisLength()));
}



//
//  Return the total width of the column
//
uLong FITSTableHDU::getCurrentRowWidthInBytes( void ) const
{
    Int         nrcolumns;
    uLong       totalwidth = 0L;

    //
    //  Find out the last column, sum up the offset+width, and TADA!
    //  we have the answer!
    //
    itsColumnptrs.shape( nrcolumns );
    
    if( nrcolumns )
    {
        FITSColumn*  ptr2lastcolumn( itsColumnptrs(nrcolumns-1)  );

        if( ptr2lastcolumn )
        {
            totalwidth=ptr2lastcolumn->columnoffset()+(uLong)ptr2lastcolumn->getWidthInBytes();
        }
    }
    return totalwidth;
}

FitsKeywordList FITSTableHDU::getHeaderKeywords( void ) const
{
    FitsKeywordList    retval;
    
    retval.mk( FITS::XTENSION, this->xtension().c_str() );
    retval.mk( FITS::BITPIX, DataTypeToBitpix(this->getDataType()) );
    return retval;
}

FitsKeywordList FITSTableHDU::getDataDescriptionKeywords( void ) const
{
    FitsKeywordList    retval;
    
    //
    //  Create the data description for the table....
    //
    retval.mk( FITS::NAXIS,    2, "A table is a matrix isn't it?" );

    for( int i=0; i<2; i++ )
    {
        retval.mk( i+1, FITS::NAXIS, itsAxisptrs(i)->getAxisLength() );
    }

    retval.mk( FITS::PCOUNT, 0 );
    retval.mk( FITS::GCOUNT, 1 );

    //
    //  Describe the columns....
    //
    Int    nrcolumns;
    
    itsColumnptrs.shape( nrcolumns );
    
    //
    //  Set the TFIELDS keyword:
    //
    retval.mk( FITS::TFIELDS, nrcolumns, "Number of columns in each row" );

    //
    // Tell the tablename/version
    //
    retval.mk( FITS::EXTNAME, itsTableName.c_str() );
    retval.mk( FITS::EXTVER, Int(itsTableVersion) );


    for( Int j=0; j<nrcolumns; j++ )
    {
        FitsKeyword*    keywordptr;
        FitsKeyword*    newkeywordptr;
        FitsKeywordList columnkeywords( itsColumnptrs(j)->asFITSKeys() );
        

        columnkeywords.first();
        columnkeywords.next();
        keywordptr=columnkeywords.curr();
        
        while( keywordptr!=0 )
        {
            newkeywordptr=new FitsKeyword( *keywordptr );

            retval.insert( *newkeywordptr );
            keywordptr=columnkeywords.next();
        }
    }


    return retval;
}

uInt FITSTableHDU::numberColumns( void ) const
{
    Int     nrcolumns;
    
    itsColumnptrs.shape( nrcolumns );

    return (uInt)nrcolumns;
}

Bool FITSTableHDU::setNumberRows( uInt nrow )
{
    if( this->hasDataSegment() )
    {
        return False;
    }
    
    //
    //  No datasegment yet: it is allowed to resize the table!
    //
    itsAxisptrs(1)->setAxisLength( nrow );
    return True;
}

uInt FITSTableHDU::getNumberRows( void ) const
{
    return (uInt)(itsAxisptrs(1)->getAxisLength());
}



uInt FITSTableHDU::columnExists( const String& type ) const
{
    Int   retval( 0 );
    Int   nrcolumns;
    
    itsColumnptrs.shape( nrcolumns );
    
    for( Int i=0; i<nrcolumns; i++ )
    {
        if( itsColumnptrs(i)->ttype()==type )
        {
            retval++;
        }
    }
    return (uInt)retval;
}

FITSColumn* FITSTableHDU::getColumn( const String& type, Int occurrence ) const
{
    Int         i;
    Int         currentoccurrence( 0 );
    Int         nrcolumns;
    FITSColumn* retval( 0 );
    
    itsColumnptrs.shape( nrcolumns );
    
    for( i=0; i<nrcolumns; i++ )
    {
        if( itsColumnptrs(i)->ttype()==type )
        {
            if( occurrence==currentoccurrence )
            {
                break;
            }
            else
            {
                currentoccurrence++;
            }
        }
    }
    
    if( i<nrcolumns )
    {
        //
        //  Loop has been prematurely ended: column is found!
        //
        retval=itsColumnptrs(i);
    }
    return retval;

}

FITSColumn* FITSTableHDU::getColumn( Int columnnr ) const
{
    Int         maxcolumnnr;
    FITSColumn* retval( 0 );
    
    itsColumnptrs.shape( maxcolumnnr );
    
    if( columnnr>=0 && columnnr<maxcolumnnr )
    {
        retval=itsColumnptrs( columnnr );
    }
    return retval;
}


//
//  The get/put row functions!
//
Bool FITSTableHDU::getrow( uInt rownr, RecordInterface& riref ) const
{
    Bool    retval( False );
    Bool    except;

    //
    //  If no datasegment: bummer!
    //
    if( !this->hasDataSegment() )
    {
        return retval;
    }
    
    //
    // Attempt to re-structure the user's record!
    //
    except=False;
    
    try
    {
        riref.restructure( this->getRowDescription() );
    }
    catch( const std::exception& x )
    {
        cerr << x.what() << endl;
        except=True;
    }
    
    if( except )
    {
        return retval;
    }
    
    //
    //  Get all the datavalues!
    //
    Int   nrcolumns;
    
    itsColumnptrs.shape( nrcolumns );
    
    for( uInt i=0; i<(uInt)nrcolumns; i++ )
    {
        if( !(itsColumnptrs(i)->get( rownr, riref, i )) )
        {
            break;
        }
    }
    return retval;
}

Bool FITSTableHDU::putrow( uInt rownr, const RecordInterface& riref )
{
    Bool    retval( False );
    
    //
    //  Ok. If no datasegment: make it!
    //
    if( !this->hasDataSegment() )
    {
        this->makeDataSegment();
    }

    //
    //  Ok. Do copy all fields across!
    //

    //
    //  First check if the record the user is passing has the right
    //  structure for us!
    //
    if( riref.description()!=itsRowDescription )
    {
        return retval;
    }
    
    //
    // Loop over all columns and put the correct recordfield!
    //

    //
    //  Put all the datavalues!
    //
    Int  nrcolumns;
    Int  i;
    
    itsColumnptrs.shape( nrcolumns );
    
    for( i=0; i<nrcolumns; i++ )
    {
        if( !(itsColumnptrs(i)->put( rownr, riref, i )) )
        {
            break;
        }
    }

    retval = ( i==nrcolumns );

    return retval;
}

RecordDesc FITSTableHDU::getRowDescription( void ) const
{
    return itsRowDescription;
}

String FITSTableHDU::extname( void ) const
{
    return itsTableName;
}

Int FITSTableHDU::extver( void ) const
{
    return itsTableVersion;
}


//
//  Destruct the table!
//
FITSTableHDU::~FITSTableHDU()
{
    Int   counter;
    Int   nrcolumns;
    Int   nraxes;
    
    //
    //  Theoreticaaly this should not be neccessary (in principle
    //  there should be just two axes in the table) but if we do it
    //  like this than wee never have a memory leak....
    //
    itsAxisptrs.shape( nraxes );
    
    for( counter=0; counter<nraxes; counter++ )
    {
        delete itsAxisptrs(counter);
    }
    
    //
    //  Delete all the columns
    //
    itsColumnptrs.shape( nrcolumns );
    
    for( counter=0; counter<nrcolumns; counter++ )
    {
        delete itsColumnptrs( counter );
    }

    //
    //  And we're done
    //
}


//
//
//
//   The private methods
//
//
//

