//
//  Some global functions to test if DataTypes match etc. See
//  descriptions that go with the functions for detals.
//
//  Also conversion from DataType -> BITPIX and vice-versa
//
//  And test for equality of two FitsKeywords (match on name+(index))
//
//
//
//  Author:      
//
//               Harro Verkouter,     20-04-1998
//
//   $Id: freefunctions.h,v 1.3 2007/01/17 08:22:02 jive_cc Exp $
//
#ifndef FREEFUNCTIONS_H_INC
#define FREEFUNCTIONS_H_INC


#include <casa/aips.h>
#include <casa/Utilities/DataType.h>
#include <casa/Arrays/IPosition.h>
#include <fits/FITS.h>

#include <iostream>


//
//  This is a local free function, specially suited for the
//  FITSParameter class. it takes two DataTypes and decides whether or
//  not the source type can be changed in the destination type, by
//  either casting or indexing (especially for arrays, indexed by
//  IPositions) or taking the real/imaginary part or any combination
//  of the actions.
//
//  It basically checks if the BASIC types are equal/compatible, and
//  only for those basic types that are supported in FITS!
//
casa::Bool dataTypesMatch( casa::DataType destinationtype,
						   casa::DataType sourcetype );


//
//  Test if the given datatype is a basic fits-compatible datatype
//
casa::Bool isBasicFitsType( casa::DataType sometype );

//
// Make an array type of a basic type (if sometype is a valid basic
// fitstype!)
//
casa::DataType MakeArrayType( casa::DataType sometype );

//
//  Convert DataTypes to bitpix value (and the reverse)
//

//
//  Return value is TpOther if the 'bitpixvalue' is not recognized as
//  a valid value for the 'BITPIX' keyword.
//
casa::DataType  BitpixToDataType( casa::Int bitpixvalue );

//
//  Return value is 0 if 'sometype' is not a basic fitstype!
//
casa::Int       DataTypeToBitpix( casa::DataType sometype );

//
//  Return True if the names of the two keywords (and the indices, if
//  applicable) match!
//
casa::Bool operator==( const casa::FitsKeyword& lval,
		               const casa::FitsKeyword& rval );

//
//  Read an IPosition from a stream, in ASCII format; the inverse from
//  operator<<(IPosition)
//
std::istream& operator>>( std::istream& is, casa::IPosition& rval );

#endif
