//
//  Class: 
//
//       FITSAsciiColumn
//
//  Purpose:
//
//       Object describes a column in a FITSBinaryTable Object. Does
//       correct formatting on get/put optins.
//
//
//
//  Author:
//
//       Harrro Verkoutrer,    18-05-1998
//
//
//  $Id: FITSAsciiColumn.h,v 1.4 2007/01/17 08:22:01 jive_cc Exp $
//
#ifndef FITSASCIICOLUMN_H_INC
#define FITSASCIICOLUMN_H_INC

#include <casa/aips.h>
#include <fits/FITS.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSColumn.h>

//
//  Forward declarations
//
class FITSAsciiTableHDU;


class FITSAsciiColumn : 
    public FITSColumn
{
public:
    //
    // The public interface of the FITSAsciiColumn. For explanation
    // of the three different creators, see FITSColumn.h
    //
    FITSAsciiColumn( const casa::String& type, const casa::String& comment, const casa::String& form, const casa::String& unit );
    
    FITSAsciiColumn( casa::Int number, casa::uLong offset, casa::FitsKeywordList& kwsource, FITSAsciiTableHDU* ptr2parent );
    
    FITSAsciiColumn( casa::Int number, casa::uLong offset, const FITSAsciiColumn& other, FITSAsciiTableHDU* ptr2parent );
    
    //
    //  Via recordfieldid! Default to the first field in the record
    //
    virtual casa::Bool            get( casa::uInt rownr, casa::RecordInterface& recordref, 
                                 const casa::RecordFieldId& fieldidx=0 ) const;
    virtual casa::Bool            put( casa::uInt rownr, const casa::RecordInterface& recordref, 
                                 const casa::RecordFieldId& fieldidx=0 );

    //
    //  We have our own method of translating this into FITSkeywords!
    //
    virtual casa::FitsKeywordList asFITSKeys( void ) const;

    //
    //  Width of this column
    //
    virtual casa::uLong           getWidthInBytes( void ) const;

    //
    //  Destruct the column object!
    //
    ~FITSAsciiColumn();

protected:
    //
    // Any protected functions go here.
    //


private:
    //
    //  The BinaryColumn's specific data
    //

    //
    //  We keep track of the width of this column ourselves
    //
    casa::uLong                 itsTotalWidth;

    //
    //  The width of the field
    //
    casa::uInt                  itsFieldWidth;
    
    //
    //  And the precision (nr of digits after the decimal point)
    //
    casa::uInt                  itsPrecision;
    
    //
    //  Number of digits in exponent
    //
    casa::uInt                  itsExponentPrecision;
    
    //
    //  AsciiColumn private methods
    //

    //
    //  This function checks the 'form' string and from there tries
    //  to determine the datatype of the column and the total column
    //  width. If a nonsupported datatype or invalid form-description
    //  was entered, an exception will be thrown!
    //
    void           CheckFormatAndInit( void );

    //  No default ascii columns possible
    FITSAsciiColumn( );
    FITSAsciiColumn( const FITSAsciiColumn& );
    FITSAsciiColumn& operator=( const FITSAsciiColumn& );
};



#endif
