//
//  Class:
//
//          FITSImageExtension
//
//  Purpose:
//
//          This class describes an ImageExtension in a FITS file!
//
//
//  Author:
//
//          Harro Verkouter,   26-05-1998
//
//
//     $Id: FITSImageExtension.h,v 1.4 2006/01/13 11:35:43 verkout Exp $
//
#ifndef FITSIMAGEEXTENSION_H_INC
#define FITSIMAGEEXTENSION_H_INC


//
//  We need this include since it is our baseclass!
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSArrayHDU.h>
#include <casa/Utilities/Regex.h>
#include <casa/Containers/RecordInterface.h>


//
//  The primary array hdu class. It isA FITSArrayHDU, therefore we
//  derive it from that class
//
class FITSImageExtension :
    public FITSArrayHDU
{
public:
    //
    //  The public interface
    //

    //
    //  Create a primary array of a given type (basic fits type: the
    //  type of data items),
    //
    FITSImageExtension( casa::DataType basicfitstype );

    //
    //  Create the primary array from a fitskeywordlist, that's just
    //  been read from an existing fitsfile represented by
    //  'ptr2stream'
    //
    FITSImageExtension( casa::FitsKeywordList& kwlist, casa::ByteIO* ptr2stream );

    //
    //  The type of this hdu
    //
    virtual HDUObject::hdu_t getHDUtype( void ) const;

    //
    // Define get/put actions on the array!
    //
    // Note: these functions are NOT defined on the baseclass, and
    // they are virtual in THIS class -> derived classes from this
    // object may overload the get/put actions.
    //
    virtual casa::Bool      put( const casa::RecordInterface& recordref );
    
    virtual casa::Bool      get( casa::RecordInterface& recordref ) const;

    //
    //  Implement the 'extension HDU' specific access function
    //

    //
    // Returns 'IMAGE'
    //
    casa::String            xtension( void ) const;

    //
    //  Do images have an extname/extver? 
    //
    casa::String            extname( void ) const;
    casa::Int               extver( void ) const;
    
    //
    //  Return the regex that matches the description of this extname
    //
    static casa::Regex      fitsname( void );
    

    //
    //  Destruct the primary array
    //
    virtual ~FITSImageExtension();
    
protected:
    //
    //  Protected functions
    //
    virtual casa::FitsKeywordList getHeaderKeywords( void ) const;

private:
    //
    //  The primary arrays private parts
    //
    casa::String            itsExtensionName;
    casa::Int               itsExtensionVersion;
    

    //
    //  The private methods
    //

    //
    //  These are undefined and inaccessible
    //
    FITSImageExtension();
    FITSImageExtension( const FITSImageExtension& );
    FITSImageExtension& operator=( const FITSImageExtension& );
};



#endif
