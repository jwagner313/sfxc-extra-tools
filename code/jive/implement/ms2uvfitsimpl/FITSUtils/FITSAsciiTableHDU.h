//
//  Class:
//
//        FITSAsciiTableHDU
//
//
//  Purpose:
//
//        Specialization of the FITSTableHDU class, representing a
//        binary table in FITS format.
//
//
//  Author:
//
//        Harro Verkouter,   18-05-1998
//
//
//      $Id: FITSAsciiTableHDU.h,v 1.3 2006/01/13 11:35:42 verkout Exp $  
//
#ifndef FITSASCIITABLEHDU_H_INC
#define FITSASCIITABLEHDU_H_INC

//
//  Need this: it's our baseclass
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSTableHDU.h>
#include <casa/Utilities/Regex.h>


//
//  Forward declarations
//
class FITSAsciiColumn;


//
//  The class itself
//
class FITSAsciiTableHDU :
    public FITSTableHDU
{
    //
    //  The public interface
    //
public:
    
    //
    //  A Binary table! Create in memory
    //
    FITSAsciiTableHDU( const casa::String& name, casa::uInt version );
    
    //
    //  Supposedly: create it from a disk/tape (a stream, rather)
    //
    FITSAsciiTableHDU( casa::FitsKeywordList& kwlist, casa::ByteIO* ptr2stream );
    

    //
    //  For binary table HDU's we allow adding of binary columns!
    //
    //
    //  Tables do have the possibility of adding columns. FITSTable
    //  objects should implement this function, in order for them to
    //  get the pointer to the parent right!
    //
    //  IMPORTANT NOTE:
    //
    //  The column object that is passed as argument is COPIED before
    //  it is added internally. So any changes one performs on the
    //  local variable are NOT reflected in the internal column
    //  object. The 'getColumn()' methods should be used to get a
    //  reference to the actual column object. Binding information (if
    //  the column was bound to something) is copied. So if you
    //  already did that, there's no need to redo that after the
    //  column object is added to the HDU.
    //
    //  The return value is the actual column number that was assigned
    //  to this column. If less than zero: an error occurred and the
    //  column could not be added!
    //
    casa::Int           addColumn( const FITSAsciiColumn& column2add );

    //
    //  What type of hdu are we?
    //
    virtual HDUObject::hdu_t getHDUtype( void ) const;

    //
    // Retrieve columns. If the columns are not found, an exception
    // will be thrown!
    //
    FITSAsciiColumn&   getColumn( const casa::String& name, casa::uInt occurrence=0 ) const;
    FITSAsciiColumn&   getColumn( casa::uInt columnnr ) const;

    //
    //  Return the extension type for this table!
    //
    casa::String             xtension( void ) const;

    //
    //  Return the regex that describes the description of this tablekeyword
    //  It is something like:
    //
    //    ^TABLE[ \t]*$
    //
    static casa::Regex       fitsname( void );

    //
    //  Destruct the HDU!
    //
    ~FITSAsciiTableHDU();

protected:
    //
    //  The protected functions go in here. Like the header keywords!
    //
    
private:
    //
    //  The AsciiTable's private parts go here!
    //

    //
    //  Undefined and inaccessible
    //
    FITSAsciiTableHDU();
    FITSAsciiTableHDU( const FITSAsciiTableHDU& );
    FITSAsciiTableHDU& operator=( const FITSAsciiTableHDU& );
};

#endif
