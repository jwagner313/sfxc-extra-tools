//
// Specialization of the HDUObject class : this one is an empty
// header, supposedly to be used to open a FITS-IDI file (see AIPS
// memo #102: The FITS Interferometry Data Interchange format, by
// Chris Flatters, http://www.aoc.nrao.edu/~cflatter/FITS-IDI.html)
//
// Author:  Harro Verkouter, 13-09-1999
//
//
//  $Id: FITSIDIHeader.h,v 1.3 2006/01/13 11:35:43 verkout Exp $
//
//  $Log: FITSIDIHeader.h,v $
//  Revision 1.3  2006/01/13 11:35:43  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.2  2001/05/30 11:50:29  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:48  verkout
//  HV: Imported aips++ implement-stuff
//
//
//
//  Purpose: to create an empty HDU object in such a way that it will
//  be recognized by classic AIPS as being a FITS IDI file. This is
//  really necessary to make sure we isolate this part since classic
//  AIPS is checking in a very dumb way, it reads the header and
//  checks at (e.g.) *character* positions 108-109 to see if those
//  characters are ' ' and '2' (and two more of such conditions) to
//  decide what the file format is... Hence we must make sure that
//  *indeed* at positions 108/109 we write the characters ' ' and '2'
//  etc...
//
//
//
#ifndef FITSIDIHEADER_H
#define FITSIDIHEADER_H


//
//  This is our baseclass, we'll need it...
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>


class FITSIDIHeader :
    public HDUObject
{
public:

    //
    //  FITS IDI Headers don't take *any* arguments, hence the onlt
    //  c'tor allowed is the default one
    //
    FITSIDIHeader();
    
    //
    //  Create it from a keywordlist, supposedly just read from file?!
    //
    FITSIDIHeader( casa::FitsKeywordList& kwlist );

    //
    //  Return the type of this HDU (=defaulthdu?)
    //
    virtual HDUObject::hdu_t getHDUtype( void ) const;
    
    //
    // we have NO data attached whatsoever...
    //
    virtual casa::uLong            getCurrentDataVolumeInBytes( void ) const;

    //
    //  de-allocate allocated resources (if any)
    //
    virtual ~FITSIDIHeader();
    

protected:
    //
    //  Ok. Implement our own methods...
    //
    virtual casa::FitsKeywordList getHeaderKeywords( void ) const;
    
    virtual casa::FitsKeywordList getDataDescriptionKeywords( void ) const;
    
private:
    //
    //  The private parts
    //

    //
    //  These are undefined hence we make'm inaccessible
    //
    FITSIDIHeader( const FITSIDIHeader& );
    FITSIDIHeader& operator=( const FITSIDIHeader& );
};






#endif
