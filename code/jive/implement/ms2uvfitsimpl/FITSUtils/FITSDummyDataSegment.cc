//
//  Implementation of the FITSDummyDataSegment class!
//
//
//  Author:
//
//         Harro Verkouter,   06-08-1998
//
//
//  $Id: FITSDummyDataSegment.cc,v 1.5 2006/01/13 11:35:43 verkout Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDummyDataSegment.h>

using namespace casa;

FITSDummyDataSegment::FITSDummyDataSegment( uLong size, DataType basicfitstype ) :
    FITSDataSegment( 0L, basicfitstype ),
    itsNumberOfBlocks( 0L ),
    itsDataptr( 0 )
{
    //
    //  Compute the size in blocks
    //
    if( size )
    {
        itsNumberOfBlocks=( (size/(uLong)2880)+1 );
    }
    
    //
    //  Allocate the block...
    //
    itsDataptr=new uChar[2880];
    
    //
    //  And initialize it!
    //
    this->initDataSegment( itsDataptr, basicfitstype, 2880 );
}



uLong FITSDummyDataSegment::getSegmentSize( void ) const
{
    return (itsNumberOfBlocks*(uLong)2880);
}

FITSDataSegment::source_t FITSDummyDataSegment::dataSegmentOrigin( void ) const
{
    return FITSDataSegment::memory;
}

FITSDummyDataSegment::~FITSDummyDataSegment()
{
    delete [] itsDataptr;
}

//
//  The protected methods
//
uLong FITSDummyDataSegment::nrFitsBlocks( void ) const
{
    return itsNumberOfBlocks;
}

void* FITSDummyDataSegment::operator[]( uLong offset )
{
    void*   retval( 0 );
    
    if( offset<this->getSegmentSize() )
    {
        retval=itsDataptr;
    }
    
    return retval;
}

const void* FITSDummyDataSegment::operator[]( uLong offset ) const
{
    void*   retval( 0 );
    
    if( offset<this->getSegmentSize() )
    {
        retval=itsDataptr;
    }
    
    return retval;
}


