//
//  Class:
//
//          FITSPrimaryGroup
//
//  Purpose:
//
//          This class describes a PrimaryGroup in a FITS file!
//
//
//  Author:
//
//          Harro Verkouter,   26-05-1998
//
//
//     $Id: FITSPrimaryGroup.h,v 1.3 2006/01/13 11:35:44 verkout Exp $
//
#ifndef FITSPRIMARYGROUP_H_INC
#define FITSPRIMARYGROUP_H_INC


//
//  We need this include since it is our baseclass!
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSArrayHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>

#include <casa/Arrays/Vector.h>
#include <casa/Containers/RecordInterface.h>


//
//  The primary group hdu class. It isA FITSArrayHDU, therefore we
//  derive it from that class
//
class FITSPrimaryGroup :
    public FITSArrayHDU
{
    //
    //  The get/put functions of the FITSParameter class are friends
    //  of ours!
    //
    friend casa::Bool FITSParameter::put( casa::uLong idx, const casa::RecordInterface& riref,
                                    const casa::RecordFieldId& fieldidx );
    friend casa::Bool FITSParameter::get( casa::uLong idx, casa::RecordInterface& riref,
                                    const casa::RecordFieldId& fieldidx ) const;

public:
    //
    //  The public interface
    //

    //
    //  Create a primary group of a given type (basic fits type: the
    //  type of data items),
    //
    FITSPrimaryGroup( casa::DataType basicfitstype );

    //
    //  Create the primary array from a fitskeywordlist, that's just
    //  been read from an existing fitsfile represented by
    //  'ptr2stream'
    //
    FITSPrimaryGroup( casa::FitsKeywordList& kwlist, casa::ByteIO* ptr2stream );

    //
    //  Primary groups have the possibility of adding parameters! The
    //  return value is the parameter number that was assigned to the
    //  parameter. If the return value is <0, the parameter was NOT
    //  added.
    //
    virtual casa::Int       addParameter( const FITSParameter& parameter );

    //
    //  Set the number of groups (if possible)
    //
    casa::Bool              setNumberOfGroups( casa::uLong nrofgroups );

    //
    //  The type of this hdu
    //
    virtual HDUObject::hdu_t getHDUtype( void ) const;

    //
    // Define get/put actions on the primary group!
    //
    // Note: these functions are NOT defined on the baseclass, and
    // they are virtual in THIS class -> derived classes from this
    // object may overload the get/put actions.
    //
    virtual casa::Bool      put( casa::uLong group, const casa::RecordInterface& recordref );
    
    virtual casa::Bool      get( casa::uLong group, casa::RecordInterface& recordref ) const;

    //
    //  We have to implement our own version of the
    //  'getCurrentDataVolumeInBytes()' method, since there are two
    //  points we have to take into account:
    //
    //  1) axis 0 has length 0 -> product of all axes=0 which is
    //  usually quite wrong...
    //
    //  2) We have a number of groups, preceded by a number of
    //  parameters -> we have to take the parameters in account too.
    //
    virtual casa::uLong     getCurrentDataVolumeInBytes( void ) const;

    //
    //  Users might be interested in the following info
    //

    //
    //  The shape of a group in the random groups structure
    //
    casa::IPosition         groupShape( void ) const;
    casa::uLong             getGroupSizeInBytes( void ) const;
    casa::uLong             getNumberOfGroups( void ) const;
    casa::RecordDesc        getGroupDescription( void ) const;

    //
    //  Check if the given parametr exists in this HDU. The number of
    //  occurrences is returned.
    //
    casa::uInt              parameterExists( const casa::String& parametertype ) const;
    casa::uInt              getNumberOfParameters( void ) const;
    
    //
    //  Access parameters (by name or by number). Note: if the
    //  adressed parameter cannot be found, an aipsError exception is
    //  thrown!
    //
    FITSParameter&    getParameter( const casa::String& parametername, casa::uInt occurrence=0 ) const;
    FITSParameter&    getParameter( casa::uInt parameternumber ) const;
    

    //
    //  Destruct the primary array
    //
    virtual ~FITSPrimaryGroup();
    
protected:
    //
    //  Protected functions
    //
    virtual casa::FitsKeywordList   getDataDescriptionKeywords( void ) const;
    
private:
    //
    //  The primary groups private parts
    //
    
    //
    //  The number of groups represented by this HDU
    //
    casa::uLong                   itsNumberOfGroups;
    
    //
    //  Primary groups have parameters! Keep a vector of pointers to
    //  the parameters
    //
    casa::Vector<FITSParameter*>  itsParameterptrs;
    
    //
    //  The private methods
    //

    //
    //  These are undefined and inaccessible
    //
    FITSPrimaryGroup();
    FITSPrimaryGroup( const FITSPrimaryGroup& );
    FITSPrimaryGroup& operator=( const FITSPrimaryGroup& );
};



#endif
