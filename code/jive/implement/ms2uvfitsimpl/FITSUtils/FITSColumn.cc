//
//   Implementation of the FITSColumn class
//
//
//   Author:
//
//         Harro Verkouter     24-3-1998
//
//
//    $Id: FITSColumn.cc,v 1.8 2006/03/02 14:21:41 verkout Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSColumn.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSTableHDU.h>
#include <casa/Containers/RecordDesc.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Containers/RecordField.h>


#include <iomanip>
#include <sstream>

using namespace std;
using namespace casa;


ostream& operator<<( ostream& osref, const FITSColumn& columnref )
{
    osref << "Column #" << setw(2) << columnref.columnnr() << " " << columnref.ttype() 
          << ": " << columnref.tform() << "\n"
          << columnref.tunit() << " found at byte offset " << columnref.columnoffset()
          << endl;
    
    return osref;
}



//
//
//  Let the user construct a FITSColumn. The column is merely a
//  description and does not contain anything. It will be possible to
//  assign a column so the data will be available through the put/get
//  functions.
//
//
FITSColumn::FITSColumn( const String& type, const String& comment, const String& form, const String& unit ) :
    itsColumnType( type ),
    itsComment( comment ),
    itsForm( form ),
    itsUnit( unit ),
    itsColumnnumber( -1 ),
    columnDataType( TpOther ),
    itsNumberElements( 0 ),
    itsTotalWidth( 0 ),
    itsOffsetInRow( 0L ),
    myBinaryColumnFlag( False )
{
}


//
//  Attempt to construct a column from a FitsKeywordList
//
FITSColumn::FITSColumn( Int columnnr, uLong offset, FitsKeywordList& kwsource, FITSTableHDU* ptr2parent ) :
    BasicFITSUtility( ptr2parent ),
    itsColumnType( "" ),
    itsComment( "" ),
    itsForm( "" ),
    itsUnit( "" ),
    itsColumnnumber( columnnr ),
    columnDataType( TpOther ),
    itsNumberElements( 0 ),
    itsTotalWidth( 0 ),
    itsOffsetInRow( offset ),
    myBinaryColumnFlag( False )
{
    //
    //  Now we parse the keywordlist and attempt to locate the info we
    //  so desperately need....
    //
    FitsKeyword*  keywordptr( 0 );
    
    if( (keywordptr=kwsource(FITS::TFORM, itsColumnnumber+1))==0 )
    {
        ostringstream   error;
        
        error << "Column keyword TFORM" << itsColumnnumber+1 << " not present.";
        
        throw( AipsError(error.str()) );
    }

    //
    //  Ok. Go ahead
    //
    itsForm=keywordptr->asString();
    itsComment=keywordptr->comm();
    
    kwsource.del();
    
    //
    //  TTYPE:
    //
    if( (keywordptr=kwsource(FITS::TTYPE, itsColumnnumber+1))!=0 )
    {
        itsColumnType=keywordptr->asString();
        kwsource.del();
    }

    //
    //  TUNIT:
    //
    if( (keywordptr=kwsource(FITS::TUNIT, itsColumnnumber+1))!=0 )
    {
        itsUnit=keywordptr->asString();
        kwsource.del();
    }
}

//
//
//  The 'copy' constructor: especially designed for
//  HeaderDataUnits. The HeaderDataUnits know the current column
//  index and are capable of filling the variable with a sensible
//  value!
//
//  NOTE: when determining the offset of this column with respect to
//  the beginning of a row in the parent table, the object ASSUMES IT
//  WILL BE ADDED AT THE END OF THE CURRENT ROW. Therefore it ASSUMES
//  that it's offset is the current length of a row in the table
//  (without having itself added!)
//
//
FITSColumn::FITSColumn( Int columnnr, uLong offset, const FITSColumn& other, FITSTableHDU* ptr2parent ) :
    BasicFITSUtility( ptr2parent ),
    itsColumnType( other.itsColumnType ),
    itsComment( other.itsComment ),
    itsForm( other.itsForm ),
    itsUnit( other.itsUnit ),
    itsColumnnumber( columnnr ),
    columnDataType( other.columnDataType ),
    itsNumberElements( other.itsNumberElements ),
    itsTotalWidth( other.itsTotalWidth ),
    itsOffsetInRow( offset ),
    itsColumnDescription( other.itsColumnDescription ),
    myBinaryColumnFlag( other.myBinaryColumnFlag )
{
    //
    //  Nothing more to do I suspect?! We don't have to check the
    //  format etc. since the "other"-object must already have been
    //  constructed. If the format was wrong, the object would not
    //  have been created; an exception would be thrown.
    //
}

FITSColumn::FITSColumn( const FITSColumn& other ):
    BasicFITSUtility( (const BasicFITSUtility&)other ),
    itsColumnType( other.itsColumnType ),
    itsComment( other.itsComment ),
    itsForm( other.itsForm ),
    itsUnit( other.itsUnit ),
    itsColumnnumber( other.itsColumnnumber ),
    columnDataType( other.columnDataType ),
    itsNumberElements( other.itsNumberElements ),
    itsTotalWidth( other.itsTotalWidth ),
    itsOffsetInRow( other.itsOffsetInRow ),
    itsColumnDescription( other.itsColumnDescription ),
    myBinaryColumnFlag( other.myBinaryColumnFlag )
{
    //
    //  Nothing more to do I suspect?! We don't have to check the
    //  format etc. since the "other"-object must already have been
    //  constructed. If the format was wrong, the object would not
    //  have been created; an exception would be thrown.
    //
}

Bool FITSColumn::setDataType( const DataType& newdatatype )
{
    columnDataType=newdatatype;
    return True;
}

Bool FITSColumn::setColumnDescription( const RecordDesc& newdescription )
{
    itsColumnDescription=newdescription;
    return True;
}

Bool FITSColumn::setForm( const String& newform )
{
    itsForm=newform;
    return True;
}


void FITSColumn::setColumnOffset( uLong newoffset )
{
    itsOffsetInRow=newoffset;
    return;
}


//
//  Return the aips++ datatyoe of this column
//
DataType FITSColumn::getColumnDataType( void ) const
{
    return columnDataType;
}

RecordDesc FITSColumn::getColumnDescription( void ) const
{
    return itsColumnDescription;
}



//
// Ok. Translate the column into FITSKeys....
//
FitsKeywordList FITSColumn::asFITSKeys( void ) const
{
    FitsKeywordList    retval;

    //
    //  Ok. Lets do it!
    //
    retval.mk( itsColumnnumber+1, FITS::TTYPE, itsColumnType.c_str(),
			itsComment.c_str() );
    retval.mk( itsColumnnumber+1, FITS::TFORM, itsForm.c_str() );

    //
    //  Only if there are any characters at all in the unit, there's
    //  use in translating it to a FITSKeyword
    //
    if( itsUnit.length() )
    {
        retval.mk( itsColumnnumber+1, FITS::TUNIT, itsUnit.c_str() );
    }
    
    return retval;
}


//********************************************************************************/
//
//
//                          General functions
//
//
//********************************************************************************/


uLong FITSColumn::getWidthInBytes( void ) const
{
    return uLong(itsTotalWidth);
}

Int FITSColumn::columnnr( void ) const
{
    return itsColumnnumber;
}

uLong FITSColumn::columnoffset( void ) const
{
    return itsOffsetInRow;
}


const String& FITSColumn::ttype( void ) const
{
    return itsColumnType;
}

const String& FITSColumn::tform( void ) const
{
    return itsForm;
}

const String& FITSColumn::tunit( void ) const
{
    return itsUnit;
}


Bool FITSColumn::isBinaryColumn( void ) const
{
    return myBinaryColumnFlag;
}



//
//  Ok. Destruct this column!
//
FITSColumn::~FITSColumn()
{
    //
    //  Nothing to ddo yet!
    //
}




//********************************************************************************/
//
//              The FITSColumn private methods.....
//
//********************************************************************************/

void FITSColumn::setBinaryColumnFlag( void )
{
    myBinaryColumnFlag = True;
}
