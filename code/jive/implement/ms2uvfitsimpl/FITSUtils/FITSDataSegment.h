//
//  Class:
//
//         FITSDataSegment
//
//  Purpose:
//
//         The FITSDataSegment class can be created in some ways. the
//         datasegment object knows where it was created and
//         therefore, read and write actions from the HDU objects can
//         be coordinated to make sure the get/put actions affect the
//         correct pieces of memory (or file). Whenever possible, the
//         FITSdatasegment will try to map a piece of a file to a
//         pointer to data, so access speeds up!
//
//
//
//  Author:
//
//         Harro Verkouter,    27-04-1998
//
//
//        $Id: FITSDataSegment.h,v 1.2 2006/01/13 11:35:43 verkout Exp $
//
#ifndef FITSDATASEGMENT_H_INC
#define FITSDATASEGMENT_H_INC

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <casa/Utilities/DataType.h>
#include <casa/IO/ByteIO.h>
#include <casa/Containers/RecordFieldId.h>
#include <casa/Containers/RecordInterface.h>



class FITSDataSegment
{
    //
    //  The output-function is a friend of this object....
    //
    friend casa::ByteIO& operator<<( casa::ByteIO& bioref,
									 const FITSDataSegment& dsref );

public:
    //
    //  An enum, telling where the object originated
    //
    enum source_t 
    {
        none, disk, memory
    };
    

    //
    //  Create the object
    //

    //
    //  This will create a datasegment of 'size' bytes long; a memory
    //  object. Pass the basic fitstype (either: TpChar, TpShort,
    //  TpInt, TpFloat or TpDouble), this parameter is used to pad the
    //  block with the zero representation of the correct type.
    //  
    //  NOTE: it is sufficient to pass the requested size, the object
    //  (since it's a FITSDataSegment, will calculate how many
    //  2880-byte block are necessary to grant the request.
    //
    //
    FITSDataSegment( casa::uLong size, casa::DataType basicfitstype );
    
    //
    //  Create the FITSDataSegment from a section in a ByteIO stream,
    //  starting at the current filepointer (will be determined by the
    //  FITSDataSegment class) and being 'size' bytes long
    //
    //  NOTE: The pointer is kept, but not taken over!
    //
    FITSDataSegment( casa::ByteIO* bytestream,
					 casa::uLong size,
					 casa::DataType basicfitstype );
    

    //
    //  Return the actual size of the datasegment
    //
    virtual casa::uLong        getSegmentSize( void ) const;

    //
    //  Allow the user access to the data......
    //
    //  NOTE: don't delete this pointer our from under us!
    //
    //
    //  These functions check that the index is valid and within the
    //  datasegment!
    //
    //  NOTE: if the index is outside the datasegment, a NULL-pointer
    //  is returned!
    //
    virtual void*        operator[]( casa::uLong offset );
    virtual const void*  operator[]( casa::uLong offset ) const;    
    
    //
    // Where did we originate from?
    //
    virtual FITSDataSegment::source_t dataSegmentOrigin( void ) const;

    //
    //  Destruct it
    //
    virtual ~FITSDataSegment();

protected:
    //
    //  Protected methods: a FITSDataSegment consists of a number of
    //  blocks of data. For the read/write-functions it may be useful
    //  to get access to whole blocks of data rather than having to
    //  compute that themselves!
    //
    //  NOTE: these functions are made virtual, so we can introduce a
    //  dummy FITSDataSegment with minimal overhead!
    //
    virtual casa::uLong        nrFitsBlocks( void ) const;

    //
    //  Initialize the datasegment with the 0-representation of the
    //  data type, as indicated by the functionparameter
    //
    static void          initDataSegment( void* ptr2block,
			 							  casa::DataType whichtype,
										  casa::uLong blocksize );

private:
    //
    //  The data segment's private parts
    //


    //
    //  The origin of the datasegment .....
    //
    source_t       itsOrigin;
    
    //
    //  The size of the segment
    //
    casa::uLong          itsFitsSize;
    casa::uLong          itsOriginalSize;
    
    //
    //  If appropriate: the offset within the stream it was created
    //  from
    //
    casa::uLong          itsOffset;
    
    //
    //  And again (if appropriate) the stream whence it'd came
    //
    casa::ByteIO*        itsStreamptr;
    
    //
    //  The pointer to the actual array of bytes!!!
    //
    void*          itsDataptr;

    //
    //  Filedescriptor of the (eventual) tmpfile!
    //
    casa::Int            itsTempfileDescriptor;
    casa::String         itsTempfileName;

    //
    //  Private methods
    //

    //
    //  This static function can be used to generate a temp-file. The
    //  assigned filedescriptor and generated filename will be
    //  returned in the function parameters. The unsigned char* return
    //  value indicates whether or not the function was succesful: if
    //  the pointer is non-null: everything OK and memory succesfully
    //  mapped to return value. ptr=0 -> something went wrong!
    //
    static void*   attachTempFile( casa::String& allocatedname,
			                       casa::Int& allocatedfd, casa::uLong size );
    

    //
    //  No defaults, copying or assignment!
    //
    FITSDataSegment();
    FITSDataSegment( const FITSDataSegment& );
    FITSDataSegment& operator=( const FITSDataSegment& );
};


//
//  Easy access functions, read or write the indicated recordfield
//  at the offset 'offset' in the datasegment. These functions
//  take care of the conversion LOCAL -> FITS and vice
//  versa. Also, they check for a valid offset.
//
//  NOTE: these functions expect that the type of the field is set
//  correctly. It will take actions accordingly.
//
//  NOTE: No exceptions are thrown if anything goes wrong; if we
//  would, then the return value Bool would be not necessary....
//
casa::Bool fillRecordField( casa::RecordInterface& recordref,
                            const casa::RecordFieldId& fieldidx,
                            const FITSDataSegment& datasegmentref,
                            casa::uLong offset );

casa::Bool flushRecordField( const casa::RecordInterface& recordref,
                             const casa::RecordFieldId& fieldidx,
                             FITSDataSegment& datasegmentref,
                             casa::uLong offset );

#endif
