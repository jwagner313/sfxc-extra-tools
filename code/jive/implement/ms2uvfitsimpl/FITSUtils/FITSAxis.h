//
//  Class:
//
//        FITSAxis
//
//
//  Purpose:
//
//        This class wil describe the basic properties of an Axis in
//        the description of an array of data. This class does not
//        have to be templated, since the datatype is kept in the
//        HeaderDataUnit-object. If we add axes to that
//        HeaderDataUnit, they will automatically describe data of the
//        format used in the HeaderDataUnit.
//
//
//
//
//  Author:
//
//        Harro Verkouter     24-3-1998
//
//
//      $Id: FITSAxis.h,v 1.5 2007/01/17 08:22:01 jive_cc Exp $
//
#ifndef FITSAXIS_H_INC
#define FITSAXIS_H_INC



#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <fits/FITS.h>

#include <jive/ms2uvfitsimpl/FITSUtils/BasicFITSUtility.h>

#include <iostream>

class FITSArrayHDU;

//
//  The FITSAxis class...
//
class FITSAxis :
    public BasicFITSUtility
{
public:
    //
    //  The only allowed c'tors are those two:
    //
    //   1) Axis is created from known values
    //
    //   2) Axis is created out of a list of FITSKeywords (intended
    //   use: when a hdu has been read from file, we must extract axes
    //   from the FitsKeywordList
    //

    //
    //  This c'tor enables us to create an axis without really having
    //  to specify all it's details! Intended use: if an axis has to
    //  be added to a HDU, the user doesn't know about the axis number
    //  (and shouldn't as well!!). The HeaderDataUnit keeps track of
    //  its own number of axis!
    //
    FITSAxis( const casa::String& type, const casa::String& comment="",
			  casa::Int length=1, casa::Double refpixel=1.0, 
              casa::Double refvalue=0.0, casa::Double increment=1.0,
			  casa::Double rotangle=0.0 );
    

    FITSAxis( casa::Int axisnr,
			  casa::FitsKeywordList& kwsource,
			  FITSArrayHDU* const parent );

    //
    //  This constructor is a special one: it duplicates the contents
    //  of 'other' into the new object, but sets the axisnumber to
    //  'axisnumber'. This constructor is used by the HeaderDataUnit
    //  objects when axes are being added to them. The HeaderDataUnit
    //  object knows what the current axisnumber is. The user
    //  doesn't. So the user creates an axis (from ctor 1) and tells
    //  the HeaderDataUnit to add this one. The HeaderDataUnit calls
    //  this c'tor because it knows all the details.
    //
    //  Note: one of the parameters is the FITSArrayHDU class. We can
    //  make that assumption since arrayHDUs are the only HDU's who
    //  support axis.
    //
    FITSAxis( casa::Int axisnr, const FITSAxis& other, FITSArrayHDU* parent );


    // 
    // Because of gcc3.4 (and higher) ISO C++ standard compliance,
    // this copy c'tor must exists (bummer).
    // "temporaries, even those passed by const-reference *MUST*
    //  be copy constructable" <-- b*stards! ;)
    // And this construct we use in the 'addAxis()' method...
    // (passing a temporary by const ref)
    //
    FITSAxis( const FITSAxis& other );

    //
    //  Translate the contents of this axis to FitsKeywords!
    //
    virtual casa::FitsKeywordList asFITSKeys( void ) const;

    //
    //  Return the length of this axis
    //
    casa::Int                     getAxisLength( void ) const;
    void                    setAxisLength( casa::Int newlength );

    //
    //  Return the axis-parameters, naming convention resembles FITS
    //  nomenclature.
    //
    casa::Int                     axisnr( void ) const;
    casa::Int                     naxis( void ) const;
    casa::Double                  crpix( void ) const;
    casa::Double                  crval( void ) const;
    casa::Double                  cdelt( void ) const;
    casa::Double                  crota( void ) const;
    const casa::String&           ctype( void ) const;

    //
    //  Destruct the axis
    //
    virtual ~FITSAxis();
    
private:
    //
    //  These values describe the FITS-axis
    //
    
    //
    // The type of this axis
    //
    casa::String    itsAxistype;
    
    //
    //  Comment associated with this axis...
    //
    casa::String    itsComment;

    //
    //  The length of this axis
    //
    casa::Int       itsAxislength;

    //
    //  The reference pixel of this axis
    //
    casa::Double    itsReferencepixel;
    
    //
    //  The reference value for this axis
    //
    casa::Double    itsReferencevalue;
    
    //
    //  Axis increment (per pixel)
    //
    casa::Double    itsIncrement;
    
    //
    //  The rotation  angle
    //
    casa::Double    itsRotationangle;

    //
    //  The number of this axis in the current HDU
    //
    casa::Int       itsAxisnumber;

    //
    //  Private methods
    //

    //
    //  We forbid copying/assigning of axes!
    //
    FITSAxis& operator=( const FITSAxis& );
};

//
//  The global output operator....
//
std::ostream& operator<<( std::ostream& osref, const FITSAxis& axisref );

#endif
