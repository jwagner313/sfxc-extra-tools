//
//  Implementation of the HDUObject class!
//
//
//
//
//
#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSPrimaryArray.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSPrimaryGroup.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAsciiTableHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSImageExtension.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDummyDataSegment.h>

#include <casa/Exceptions.h>

#include <cstdlib>
#include <iostream>

using namespace casa;
using namespace std;

//
//  Output to FitsOutput
//
FitsOutput& operator<<( FitsOutput& fitsoutputref, const HDUObject& /*hduref*/ )
{
	throw( AipsError("FitsOutput& operator<<(const HDUObject&) not "
				     "implemented!") );
#if 0
    FitsKeywordList       thelist( hduref.asFITSKeys() );
    FitsKeyCardTranslator translator;
#endif    
    //cout << "Putting HDUObject into FitsOutput..." << endl;

#if 0
    thelist.first();
    
    while( 1 )
    {
        int   retval;
        
        retval=translator.build(buffer, thelist);
        
        buffer[ 2880-1 ]='\0';
        cout << buffer << endl;

        if( retval==0 )
        {
            break;
        }
    }
#endif
    return fitsoutputref;
}

ByteIO& operator<<( ByteIO& bioref, const HDUObject& hduref )
{
    char                  buffer[ 2880 ];
    FitsKeywordList       thelist( hduref.asFITSKeys() );
    FitsKeyCardTranslator translator;
    
    thelist.first();
    thelist.next();
    
    while( 1 )
    {
        int   retval;
        
        retval=translator.build(buffer, thelist);
        
        bioref.write( 2880, buffer );
        
        if( retval==0 )
        {
            break;
        }
    }

    hduref.writeDataSegment( bioref );
    
    return bioref;
}

//
//  Dump it to a stream (either console or file or whatever!)
//
ostream& operator<<( ostream& osref, const HDUObject& hduref )
{
    char                  buffer[ 2880 ];
    FitsKeywordList       thelist( hduref.asFITSKeys() );
    FitsKeyCardTranslator translator;
    
    thelist.first();
    thelist.next();
    
    while( 1 )
    {
        int   retval;
        
        retval=translator.build(buffer, thelist);
        
        osref.write( buffer, 2880 );
        
        if( retval==0 )
        {
            break;
        }
    }
    return osref;
}

ostream& operator<<( ostream& osref, const HDUObject::hdu_t& hdutref )
{
    switch( hdutref )
    {
	case HDUObject::defaulthdu:
	    osref << "<default hdu>";
	    break;
	    
	case HDUObject::primaryarray:
	    osref << "PrimaryArray";
	    break;
	    
	case HDUObject::primarygroup:
	    osref << "PrimaryGroup";
	    break;
	    
	case HDUObject::asciitable:
	    osref << "AsciiTable";
	    break;
	    
	case HDUObject::binarytable:
	    osref << "BinaryTable";
	    break;
	    
	case HDUObject::imageextension:
	    osref << "ImageExtension";
	    break;
	    
	default:
	    osref << "<INVALID>";
	    break;
	    
    }
    return osref;
}



//
// Read the hdu from an input....
//
FitsInput& operator>>( FitsInput& fitsinputref, HDUObject*& ref2hduptr )
{
    ref2hduptr=0;
    
    return fitsinputref;
}

ByteIO& operator>>( ByteIO& bioref, HDUObject*& ref2hduptr )
{
    Int                   blocksread;
    Char                  block[2880];
    Bool                  endfound( False );
    Long                  startfilepointer;
    FitsKeyword*          keywordptr;
    FitsKeywordList       thelist;
    FitsKeyCardTranslator translator;
    
    //
    //  Init retval....
    //
    ref2hduptr=(HDUObject*)0;

    
    //
    //  Get the current filepointer:
    //
    startfilepointer=bioref.seek( (long long int)0L, ByteIO::Current );

    //cout << "Currentfilepointer=" << startfilepointer << endl;
    
    //    << ", length of stream=" << bioref.length() << endl;
    

    if( bioref.length()==startfilepointer )
    {
        //
        //  we're at the end of the stream! Nothing more to do!
        //
        ref2hduptr=0;
        return bioref;
    }
    
    //
    //  Ok read blocks until end of textheader has been found
    //
    blocksread=0;

    while( !endfound )
    {
        //
        //  Read a block....
        //
        bioref.read( 2880, block );

        translator.parse( block, thelist, blocksread, FITSError::defaultHandler, False );
        
        //
        //  Check if end is found....
        //
        endfound = ( thelist(FITS::END)!=0 );
        

        //
        //  Update variables who need updating after completing a loop
        //  iteration.....
        //
        blocksread++;
    }

    //cout << "It took us " << blocksread << " blocks to find the END keyword..." << endl;
    
    //
    //  Now it's time to find out what kind of HDU we're dealing
    //  with.....
    //
    if( thelist(FITS::SIMPLE)!=0 )
    {
        //
        //  The SIMPLE-keyword is present, now depending on the
        //  filepointer this is valid or not!
        //
        if( startfilepointer!=0 )
        {
            throw( AipsError("Primary HDU found at unexpected location!") );
        }

        //
        //  Either primary array or primary group. If the
        //  GCOUNT-keyword value >0, we may assume it is a primary
        //  group HDU, otherwise it is a primary array!
        //
        if( ((keywordptr=thelist(FITS::GROUPS))!=0) &&
            keywordptr->asBool()==True )
        {
            //cout << "HDU is a primary group HDU. attempt to create!" << endl;
            FITSPrimaryGroup*     newprimarygroup;
            
            newprimarygroup=new FITSPrimaryGroup( thelist, &bioref );
            ref2hduptr=newprimarygroup;
        }
        else
        {
            //cout << "HDU is a primary array HDU. We'll try to construct it 4U" << endl;
            FITSPrimaryArray*   newarrayhdu( 0 );
            
            newarrayhdu=new FITSPrimaryArray( thelist, &bioref );
            ref2hduptr=newarrayhdu;
        }
    }
    else
    {
        if( (keywordptr=thelist(FITS::XTENSION))!=0 )
        {
            String   extension( keywordptr->asString() );
            
            //
            //  HDU may be an ASCII table, Binary table or image extension!
            //
            if( extension.matches(FITSAsciiTableHDU::fitsname()) )
            {
                //cout << "HDU is an ASCII Table extension HDU." << endl;
                FITSAsciiTableHDU*  newtable( 0 );
                
                newtable= new FITSAsciiTableHDU( thelist, &bioref );
                ref2hduptr=newtable;

            }
            else if( extension.matches(FITSBinaryTableHDU::fitsname()) )
            {
                //cout << "HDU is a bintable extension HDU" << endl;
                //cout << "Attempt to create..." << endl;

                FITSBinaryTableHDU*  newtable( 0 );
                
                newtable= new FITSBinaryTableHDU( thelist, &bioref );
                ref2hduptr=newtable;
            }
            else if( extension.matches(FITSImageExtension::fitsname()) )
            {
                //cout << "HDU is an image extension!" << endl;
                FITSImageExtension*  newimag( new FITSImageExtension(thelist,&bioref) );
                
                ref2hduptr=newimag;
            }
            else
            {
                //cout << "Extension= `" << extension
                //     << "' is not a standard extension." << endl;
            }
        }
        else
        {
            //cout << "HDU is not a recognizable HDU (not a primary data array/"
            //     << "group nor valid extension!" << endl;
        }
    }
    
    return bioref;
}



istream& operator>>( istream& isref, HDUObject*& ref2hduptr )
{
    ref2hduptr=0;
    
    return isref;
}




//
//
//  Create a default HDU object with a defined type
//
//
HDUObject::HDUObject( const DataType basicfitstype ) :
    BasicFITSUtility(),
    itsBasicDataType( basicfitstype ),
    itsDataSegmentptr( 0 )
{
    if( !isBasicFitsType(itsBasicDataType) )
    {
        ostringstream    tmp;
                
        tmp << "Sorry, but datatype " << itsBasicDataType
            << " is not allowed for HDUobjects";
        
        throw( AipsError(tmp.str()) );
    }
}

//
//  Create it from a list!
//
HDUObject::HDUObject( FitsKeywordList& kwlist ) :
    BasicFITSUtility(),
    itsBasicDataType( TpOther ),
    itsDataSegmentptr( 0 )
{
    FitsKeyword*   bitpixptr;
    
    //
    //  Locate the 'BITPIX'-keyword
    //
    if( (bitpixptr=kwlist(FITS::BITPIX))!=0 )
    {
        itsBasicDataType=BitpixToDataType( bitpixptr->asInt() );

        //
        //  Delete the keyword from the list!
        //
        kwlist.del();
    }

    //
    //  If itsBasicDataType is still not valid: we didn't recognize it
    //  or it was not present!
    //
    if( itsBasicDataType==TpOther )
    {
        throw( AipsError("No valid value for BITPIX encountered whilst creating HDU") );
    }
}



//
//  Ok, an arbitrary HDU needs to be translated into
//  fitskeywords. Call the virtual methods that (in total) will make
//  up the fitskeywordlist that describes this HDUObject
//
FitsKeywordList HDUObject::asFITSKeys( void ) const
{
    FitsKeyword*        keywordptr( 0 );
    FitsKeywordList     retval( this->getHeaderKeywords() );
    FitsKeywordList     ddkeywords( this->getDataDescriptionKeywords() );
    
    //
    //  Ok we start off wit the headerkeywords
    //
    
    //
    //  Skip to end of list....
    //
    retval.last();


    //
    //  The loop as it is shown here is (one of the) correct way(s) to
    //  loop through a linked list, however, due to a bug in the
    //  FitsKeywordList class, the first element will be copied twice!
    //
    ddkeywords.first();
    ddkeywords.next();
    keywordptr=ddkeywords.curr();
    
    while( keywordptr!=0 )
    {
        FitsKeyword*  newkeywordptr( new FitsKeyword(*keywordptr) );
        
        retval.insert( *newkeywordptr );
        keywordptr=ddkeywords.next();
    }

    //
    //  Append the xtra keywords
    //
    retval.last();
    
    //
    // Add these after the datadescription keywords?
    //
    //retval.mk( FITS::BLOCKED,   True, "Ok. It may be blocked" );
    //retval.mk( FITS::EXTEND,    True, "Extensions may be present" );

    //
    //  end of list:
    //
    retval.last();

    //
    //  Due to stupidities in the FitsKeywordList-class, we MUST copy
    //  our own private datamember 'itsExtraKeywords' before we're
    //  allowed to access methods to inspect the list.......
    //
    FitsKeywordList    kut( itsExtraKeywords );
    
    kut.first();
    kut.next();
    keywordptr=kut.curr();
    while( keywordptr!=0 )
    {
        retval.insert( *(new FitsKeyword(*keywordptr)) );
        keywordptr=kut.next();
    }
    
    //
    // End the list...
    //
    retval.end();
    
    return retval;
}


//
//  Default implementation of the writeDataSegment: don't do anything!
//  Return success.
//
Bool HDUObject::writeDataSegment( ByteIO& bioref ) const
{
    FITSDataSegment*  ptr2segment( itsDataSegmentptr );
    
    //
    //  If this HDU represents an amount of data AND there's no
    //  datasegment allocated yet, we create a temp. segment because
    //  we MUST write the described amount of bytes!
    //
    if( !ptr2segment && this->getCurrentDataVolumeInBytes()>0 )
    {
        ptr2segment=new FITSDummyDataSegment( this->getCurrentDataVolumeInBytes(), this->getDataType() );
    }
    
    //
    //  The dummy segment is only created when the segment actually
    //  describes data, but the user has failed to fill in any
    //  data. If the HDU describes nothing (no data), no need to
    //  create a dummy segment, and no need to write anything to file!
    //
    if( ptr2segment )
    {
	bioref << (*ptr2segment);
    }
    
    
    if( ptr2segment!=itsDataSegmentptr )
    {
        delete ptr2segment;
    }
    return True;
}


//
//  Default HDU does not describe any data!
//
uLong  HDUObject::getCurrentDataVolumeInBytes( void ) const
{
    return 0L;
}



uInt HDUObject::getDataSizeInBytes( void ) const
{
    //
    //  datasize in bytes = (abs(bitpixvalue))/8
    //
    Int     bitpixvalue( DataTypeToBitpix(itsBasicDataType) );
    
    return ((uInt)(abs(bitpixvalue)))/8;
}

DataType HDUObject::getDataType( void ) const
{
    return itsBasicDataType;
}


HDUObject::hdu_t HDUObject::getHDUtype( void ) const
{
    return HDUObject::defaulthdu;
}

void HDUObject::cleanup()
{
    delete this;
    //HDUObject::~HDUObject();
}


//
//  Delete the HDUObject
//
HDUObject::~HDUObject()
{
    //
    //  We need to delete the DataSegment!
    //
    delete itsDataSegmentptr;
    itsDataSegmentptr=0;
}


/********************************************************************************/
//
//
//        The HDUObject's protected and private methods
//
//
/********************************************************************************/


//
//  Provide default implementations of the following functions
//
FitsKeywordList HDUObject::getHeaderKeywords( void ) const
{
    Int             bpvalue( DataTypeToBitpix(itsBasicDataType) );
    FitsKeywordList retval;
    
    //
    // The default implementation return a dummy HDU that describes no
    // data, and has no parameters.
    //
    retval.mk( FITS::SIMPLE,    True, "Yes, this is a FITS file..." );
    retval.mk( FITS::BITPIX, bpvalue, "Over here, we use 8bits/pixel" );
    
    return retval;
}

FitsKeywordList  HDUObject::getDataDescriptionKeywords( void ) const
{
    FitsKeywordList    retval;
    
    //
    //  Description of no data:
    //
    retval.mk( FITS::NAXIS,  0, "Dummy header describes no data" );
    retval.mk( FITS::GCOUNT, 0, "Dummy header has no groups either" );
    retval.mk( FITS::PCOUNT, 0, "Dummy header has no parameters also" );
    
    return retval;
}

void HDUObject::setExtraKeywords( FitsKeywordList& newlist )
{
    //
    // Make sure the list is empty
    //
    itsExtraKeywords.delete_all();
    
    //
    //  And fill it up with the new list!
    //
    itsExtraKeywords=newlist;
}

void HDUObject::addExtraKeyword( FitsKeyword& newkeyword )
{
    FitsKeyword*  newkeywordptr( new FitsKeyword(newkeyword) );
    
    itsExtraKeywords.insert( *newkeywordptr );
    
    return;
}

void HDUObject::removeExtraKeyword( FitsKeyword& keyword2del )
{
    Int           kwcounter( 0 );
    FitsKeyword*  ptr2keyword2del;
    
    while( (ptr2keyword2del=itsExtraKeywords(kwcounter))!=0 )
    {
        if( (*ptr2keyword2del)==keyword2del )
        {
            itsExtraKeywords.del();
            break;
        }
        kwcounter++;
    }
    
    return;
}

ConstFitsKeywordList HDUObject::getExtraKeywords( void ) const
{
    return ConstFitsKeywordList( (FitsKeywordList&)itsExtraKeywords );
}

void HDUObject::makeDataSegment( void )
{
    if( !itsDataSegmentptr )
    {
        itsDataSegmentptr = new FITSDataSegment( this->getCurrentDataVolumeInBytes(),
                                                 this->getDataType() );
    }
}

void HDUObject::makeDataSegment( ByteIO* ptr2stream )
{
    if( !itsDataSegmentptr )
    {
        itsDataSegmentptr = new FITSDataSegment( ptr2stream,
                                                 this->getCurrentDataVolumeInBytes(),
                                                 this->getDataType() );
    }
}

Bool HDUObject::hasDataSegment( void ) const
{
    return ( itsDataSegmentptr!=0 );
}

FITSDataSegment& HDUObject::getDataSegment( void )
{
    if( itsDataSegmentptr )
    {
        return *itsDataSegmentptr;
    }
    throw( AipsError("HDUObject: Request for non-existant datasegment!") );
}

const FITSDataSegment& HDUObject::getDataSegment( void ) const
{
    if( itsDataSegmentptr )
    {
        return *itsDataSegmentptr;
    }
    throw( AipsError("HDUObject: Request for non-existant datasegment!") );
}
