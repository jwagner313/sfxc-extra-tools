//
//  Implementation of the FITSPrimaryArray class
//
//  Author:
//
//         Harro Verkouter,   26-05-1998
//
//
//     $Id: FITSPrimaryArray.cc,v 1.6 2006/02/10 08:53:46 verkout Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSPrimaryArray.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <casa/Containers/RecordInterface.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <casa/Containers/RecordDesc.h>
#include <jive/Utilities/jexcept.h>

using namespace std;
using namespace casa;

FITSPrimaryArray::FITSPrimaryArray( DataType basicfitstype ) :
    FITSArrayHDU( basicfitstype )
{
}


FITSPrimaryArray::FITSPrimaryArray( FitsKeywordList& kwlist, ByteIO* ptr2stream ) :
    FITSArrayHDU( kwlist, ptr2stream )
{
    //
    //  After taking out all the keywords for axes etc. we're left
    //  with notjing but the 'extra' keywords. Therefore we copy
    //  what's left in the keywordlist into our own list of extra
    //  keywords.
    //
    this->setExtraKeywords( kwlist );

    //
    //  Now's the time to make the datasegment
    //
    this->makeDataSegment( ptr2stream );
}

HDUObject::hdu_t FITSPrimaryArray::getHDUtype( void ) const
{
    return HDUObject::primaryarray;
}

//
//  We use the recordinterface to pass the array. Assume the array is
//  in field 0 of the recordinterface
//
Bool FITSPrimaryArray::put( const RecordInterface& recordref )
{
    Bool             retval( False );
    
    //
    //  datatype not compatible....
    //
    if( recordref.type(0)!=MakeArrayType(this->getDataType()) )
    {
        return retval;
    }


    //
    //  Put the complete array!
    //
    if( !this->hasDataSegment() )
    {
        this->makeDataSegment();
        if( !this->hasDataSegment() )
        {
            return retval;
        }
    }
    
    //
    //  Ok. Dump the contents into the datasegment
    //
    retval= flushRecordField( recordref, 0, this->getDataSegment(), 0L );
    
    return retval;
}

Bool FITSPrimaryArray::get( RecordInterface& recordref ) const
{
    Bool             retval( False );

    //
    //  If no datasegment, return False
    //
    if( !this->hasDataSegment() )
    {
        return retval;
    }
    
    //
    //  Make a record desc so we can restructure the user's record to
    //  match the description of this array!
    //
    RecordDesc   thisarray;
    
    thisarray.addField( "arraydata", MakeArrayType(this->getDataType()), this->shape() );
    

    Bool   except( False );
    
    try
    {
        recordref.restructure( thisarray );
    }
    catch( const std::exception& x )
    {
        cerr << x.what() << endl;
        except=True;
    }

    //
    //  If we failed to reshape the Record -> get out!
    //
    if( except )
    {
        return retval;
    }

    //
    //  Fill the recordfield from the datasegment
    //
    retval= fillRecordField( recordref, 0, this->getDataSegment(), 0L );
    
    return retval;
}



FITSPrimaryArray::~FITSPrimaryArray()
{
    //
    //  Nothing to do yet!
    //
}

