//
//  Implemenmtation of the FITSDataSegment-class
//
//
//  Author:
//
//        Harro Verkouter,  27-04-1998
//
//
//     $Id: FITSDataSegment.cc,v 1.9 2012/07/05 07:49:57 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <casa/IO/ByteIO.h>
#include <casa/Exceptions.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <fits/FITS/fits.h>
#include <casa/Containers/Record.h>
#include <casa/Containers/RecordFieldId.h>

#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <cerrno>
#include <cstring>

using namespace std;
using namespace casa;

//
//  Do the binary output into the stream!
//
ByteIO& operator<<( ByteIO& bioref, const FITSDataSegment& dsref )
{
    uLong    blocks2do( dsref.nrFitsBlocks() );
    
    for( uLong i=0; i<blocks2do; i++ )
    {
        bioref.write( 2880, dsref[i*2880] );
    }
    return bioref;
}




//
//  Create a DataSegment of 'size' bytes. Build some smartness in it!
//
FITSDataSegment::FITSDataSegment( uLong size, DataType basicfitstype ) :
    itsOrigin( FITSDataSegment::memory ),
    itsFitsSize( 0L ),
    itsOriginalSize( size ),
    itsOffset( 0L ),
    itsStreamptr( 0 ),
    itsDataptr( 0 ),
    itsTempfileDescriptor( -1 ),
    itsTempfileName( "" )
{
    //
    //  Check the basic fitstype....
    //
    if( !isBasicFitsType(basicfitstype) )
    {
        throw( AipsError("Invalid basic fits-type whilst trying to create a FITSDataSegment") );
    }
    
    //
    //  Transform itsSize into an integral number of FITS 2880-byte
    //  blocks...(if size != 0)
    //
    if( itsOriginalSize!=0 )
    {
	//
	//  If the size is an integral number of times 2880, we don't
	//  have to add an extra block, do we?
	//
	if( itsOriginalSize%2880L )
	{
	    itsFitsSize= (((itsOriginalSize/2880L)+1L)*2880L);	    
	}
	else
	{
	    itsFitsSize= itsOriginalSize;
	}
	
    }
    
    //
    //  Check if we're not crossing the 2**31-1 byte boundary
    //
    if( itsFitsSize>=((1LL<<31)-1) )
    {
        throw( AipsError("FITSDataSegment: Attempt to create segment with size >= 2**31-1 bytes. Unsupported by the OS") );
    }
    

    //
    //  Ok. The user wants some memory huh?
    //
    //  Build some smartness in it. If the block is too large, we may
    //  as well start using a tempfile rightaway. Otherwise, we bang
    //  it in memory just like that!
    //
    //
    if( itsFitsSize>(30L*1024L*1024L) )
    {
        //
        //  Size > 30Mb, maybe worthfile to create a tmpfile and use
        //  that!
        //
        itsDataptr = this->attachTempFile( itsTempfileName, itsTempfileDescriptor, itsFitsSize );
    }
    
    //
    //  If we end up here and itsDataptr is still NULL, we may assume
    //  that our 'intelligent' scheme has failed somewhere. Therfore
    //  we will just create a memory block of the requested size!
    //
    //  I assume that 'new' throws an exception when it cannot
    //  allocate the memory (it just closes the App. if I'm
    //  correct...)
    //
    if( itsDataptr==0 )
    {
        itsDataptr=new uChar[ itsFitsSize ];
    }

    //
    //  Correctly initialize the block!
    //
    initDataSegment( itsDataptr, basicfitstype, itsFitsSize );
    
    //
    //  That's it!
    //
}


//
//  Create a DataSegment of 'size' bytes out of a bytestream!
//
FITSDataSegment::FITSDataSegment( ByteIO* bytestream, uLong size, DataType basicfitstype ) :
    itsOrigin( FITSDataSegment::disk ),
    itsFitsSize( 0L ),
    itsOriginalSize( size ),
    itsOffset( 0L ),
    itsStreamptr( bytestream ),
    itsDataptr( 0 ),
    itsTempfileDescriptor( -1 ),
    itsTempfileName( "" )
{
    //
    //  Incorrect basic fits type? Throw an exception!
    //
    if( !isBasicFitsType(basicfitstype) )
    {
        throw( AipsError("Invalid basic fitstype whilst trying to create FITSDataSegment") );
    }
    

    //
    //  Well then: do some checking....
    //
    if( !itsStreamptr )
    {
        throw( AipsError("No streamptr whilst trying to attach to a stream?!") );
    }
    
    //
    //  Transform itsSize into an integral number of FITS 2880-byte
    //  blocks...
    //
    if( itsOriginalSize!=0 )
    {
	if( itsOriginalSize%2880L )
	{
	    itsFitsSize= (((itsOriginalSize/2880L)+1L)*2880L);
	}
	else
	{
	    itsFitsSize= itsOriginalSize;
	}
    }

    //
    //  Check if we're not crossing the 2**31-1 byte boundary
    //
    if( itsFitsSize>=((1LL<<31)-1) )
    {
        throw( AipsError("FITSDataSegment: Attempt to create segment with size >= 2**31-1 bytes. Unsupported by the OS") );
    }
    
    //
    //  If the stream is not seekable, we might just want to copy
    //  this to a tempfile on disk (which is seekable!)
    //
    //if( !itsStreamptr->isSeekable() )
    if( 1 && itsFitsSize!=0 )
    {
        //
        //  The current filepointer in the original stream!
        //
        itsOffset= itsStreamptr->seek( (long long int)0L, ByteIO::Current );
        
        //
        //  Check if the segment does not extend beyond the size of
        //  the stream
        //
        if( (itsOffset+itsFitsSize)>(uLong)itsStreamptr->length() )
        {
            throw( AipsError("FITSDataSegment: segment extends beyond end-of-stream!") );
        }

        //
        //  Create a tempfile!
        //
        itsDataptr= attachTempFile( itsTempfileName, itsTempfileDescriptor, itsFitsSize );

        //
        //  Well then, if itsDataptr==0, something went
        //  wrong during creation. Now try to allocate memory in
        //  stead!
        //
        if( itsDataptr==0 )
        {
            itsDataptr= new uChar[ itsFitsSize ];
        }

        //
        //  Init the block!
        //
        //initDataSegment( itsDataptr, basicfitstype, itsFitsSize );
        
        //
        //  Copy everything across....
        //
        uLong         done( 0L );
        uLong         remain( itsFitsSize );
        uLong         maxcopy;

        while( remain )
        {
            maxcopy=( (remain<2880)?(remain):(2880) );

            itsStreamptr->read( maxcopy, (void *)((uChar *)itsDataptr+done) );
            
            //
            //  Update
            //
            remain -= maxcopy;
            done   += maxcopy;
        }

        itsStreamptr->seek( (long long int)(itsOffset+itsFitsSize), ByteIO::Begin );
        /*
        cout << "Filepointer now is: " << itsStreamptr->seek(0L, ByteIO::Current) << endl;
        cout << "Whilst itsOffset=" << itsOffset << " and itsFitsSize=" 
             << itsFitsSize << " therefore, pos SHOULD be: " << itsOffset+itsFitsSize << endl;
             */
    }
    else
    {
        //
        // Stream is seekable. Now attach it!
        //
        
        //
        //  Alas, no getFileNo() - function is available!
        //
        
        //
        //  We need the fileno in order to be able to to use the mmap
        //  function!
        //
    }

    
    //
    //  That's it!
    //
}

//
//  Tell the user the size of this datasegment
//
uLong FITSDataSegment::getSegmentSize( void ) const
{
    return itsFitsSize;
}


FITSDataSegment::source_t FITSDataSegment::dataSegmentOrigin( void ) const
{
    return itsOrigin;
}



//
//  Destruct the object....
//
FITSDataSegment::~FITSDataSegment()
{
    //
    // Destruct the object
    //
    if( itsDataptr )
    {
        //
        // Unmap the datapointer!
        //

        //
        //  Try to figure out where the ptr came from (either use munmap or delete!)
        //
        
        if( itsTempfileDescriptor>=0 )
        {
            //
            //  We may safely assume that the pointer was mapped to a
            //  tempfile! Now use munmap!
            //
            munmap( (char*)itsDataptr, itsFitsSize );
        }
        else
        {
            //
            //  ptr came from call to new[], use delete!
            //
            delete [] (uChar *)itsDataptr;
        }
        
        itsDataptr=0;
        itsFitsSize=0L;
        itsOriginalSize=0L;
    }
    
    //
    // If a tempfile was associated, delete it!
    //
    if( itsTempfileDescriptor>=0 )
    {
        ::close( itsTempfileDescriptor );

        ::unlink( itsTempfileName.c_str() );
    }
    
    //
    //  Done!
    //
}

/********************************************************************************

                           The protected section

 ********************************************************************************/
uLong  FITSDataSegment::nrFitsBlocks( void ) const
{
    return itsFitsSize/(uLong)2880;
}

void* FITSDataSegment::operator[]( uLong offset )
{
    void*   retval( 0 );
    
    if( offset<itsFitsSize )
    {
        retval = (void*)((uChar *)itsDataptr+offset);
    }
    return retval;
}


const void* FITSDataSegment::operator[]( uLong offset ) const
{
    void*   retval( 0 );
    
    if( offset<itsFitsSize )
    {
        retval = (void*)((uChar *)itsDataptr+offset);
    }
    return retval;
}



/********************************************************************************

                           The private section

 ********************************************************************************/
void FITSDataSegment::initDataSegment( void* ptr2block, DataType whichtype, uLong blocksize )
{
    //
    //  If no datablock: nothing to do!
    //
    if( !ptr2block || !blocksize )
    {
        return;
    }
    
    //
    //  'Klad'block
    //
    uChar   initblock[2880];
    uLong   nrblocks( blocksize/(uLong)2880 );

    switch( whichtype )
    {
        case TpUChar:
        case TpChar:
            {
                memset( initblock, 0x20, 2880 );
                for( uLong i=0; i<nrblocks; i++ )
                {
                    ::memcpy( (void *)((uChar *)ptr2block+i*2880), initblock, 2880 );
                }
            }
            break;
            
        case TpShort:
            {
                uLong  i;
                uLong  itemsinfitsblock( 2880/sizeof(Short) );
                Short  localnul( 0 );
                Short  fitsnul;
                
                FITS::l2f( (void*)&fitsnul, &localnul, 1 );
                
                for( i=0; i<itemsinfitsblock; i++ )
                {
                    ::memcpy( initblock+i*sizeof(Short), &fitsnul, sizeof(Short) );
                }
                

                for( i=0; i<nrblocks; i++ )
                {
                    ::memcpy( (void *)((uChar *)ptr2block+i*2880), initblock, 2880 );
                }
            }
            break;

        case TpInt:
            {
                uLong  i;
                uLong  itemsinfitsblock( 2880/sizeof(Int) );
                Int    localnul( 0 );
                Int    fitsnul;
                
                FITS::l2f( (void*)&fitsnul, &localnul, 1 );
                
                for( i=0; i<itemsinfitsblock; i++ )
                {
                    ::memcpy( initblock+i*sizeof(Short), &fitsnul, sizeof(Int) );
                }
                

                for( i=0; i<nrblocks; i++ )
                {
                    ::memcpy( (void *)((uChar *)ptr2block+i*2880), initblock, 2880 );
                }
            }
            break;

        case TpFloat:
            {
                uLong  i;
                uLong  itemsinfitsblock( 2880/sizeof(Float) );
                Float  localnul( 0 );
                Float  fitsnul;
                
                FITS::l2f( (void*)&fitsnul, &localnul, 1 );
                
                for( i=0; i<itemsinfitsblock; i++ )
                {
                    ::memcpy( initblock+i*sizeof(Short), &fitsnul, sizeof(Float) );
                }
                

                for( i=0; i<nrblocks; i++ )
                {
                    ::memcpy( (void *)((uChar *)ptr2block+i*2880), initblock, 2880 );
                }
            }
            break;
        
        case TpDouble:
            {
                uLong  i;
                uLong  itemsinfitsblock( 2880/sizeof(Double) );
                Double localnul( 0 );
                Double fitsnul;
                
                FITS::l2f( (void*)&fitsnul, &localnul, 1 );
                
                for( i=0; i<itemsinfitsblock; i++ )
                {
                    ::memcpy( initblock+i*sizeof(Short), &fitsnul, sizeof(Short) );
                }
                

                for( i=0; i<nrblocks; i++ )
                {
                    ::memcpy( (void *)((uChar *)ptr2block+i*2880), initblock, 2880 );
                }
            }
            break;
            
        default:
            //
            //  Nothing to do!
            //
            break;
    }
    
    return;
}


//
//  Attempt to create a tempfile and map the file to memory!
//
void* FITSDataSegment::attachTempFile( String& allocatedname, Int& allocatedfd, uLong size )
{
    //
    //  Attempt to make a tempfile and map it's contents to a pointer
    //
    char    templatename[]="datasegment.XXXXXX";
    void*   retval( 0 );
    
    //
    //  Step 0) Initialize the return values to unsuccesful indicators
    //
    allocatedname="";
    allocatedfd=-1;
    
    
    //
    //  Step 1) attempt to make a tempfilename 
    //
	allocatedfd = ::mkstemp(templatename);
   
    //
    //  Check if mktemp succeeded in creating a filename
    //
    if( allocatedfd==-1 )
    {
        cout << "Failed to make+open tempfile - " 
			 << ::strerror(errno)
			 << endl;
        return retval;
    }
	allocatedname = String(templatename);
    //
    //  Set the filepointer to the filesize and write a char, so the
    //  OS thinks the file is at least the amount of bytes we want to
    //  map!
    //
    Char    t( ' ' );
    
    ::lseek( allocatedfd, size, SEEK_SET );
    ::write( allocatedfd, &t, 1 );

    //
    //  Succesfully created the file!  Now attach the file
    //  to the program as 'virtual' memory!
    //
    retval = ::mmap( 0, size,
					 PROT_READ | PROT_WRITE, MAP_SHARED,
					 allocatedfd, 0 );

    if( !retval )
    {
        cout << "Failed to mmap - "
			 << ::strerror(errno)
			 << endl;
        
        //
        //  Close and clean up!
        //
        ::close( allocatedfd );
        allocatedfd=-1;

        ::unlink( allocatedname.chars() );
        allocatedname="";
    }
    return retval;
}


/********************************************************************************/
//
//
//               Some global functions here
//
//
/********************************************************************************/
//
//  Fill a recordfield from the datasegment (if possible!)
//
Bool fillRecordField( RecordInterface& recordref,
                      const RecordFieldId& fieldidx,
                      const FITSDataSegment& datasegmentref,
                      uLong offset )
{
    //
    //  The return value of the function
    //
    Bool         retval( False );
    DataType     fieldtype( recordref.dataType(fieldidx) );
    IPosition    fieldshape( recordref.shape(fieldidx) );

    //
    //  First: attempt to get the dataptr
    //
    const void*  dataptr( datasegmentref[offset] );
    
    if( !dataptr )
    {
        return retval;
    }
    
    //
    //  Ok. pointer is validated (though only the starting point is
    //  validated, the end point we might want to check later on, if
    //  we know how many bytes we really need...)
    //

    //
    //  Depending on the type of the recordfield, we do something
    //
    switch( fieldtype )
    {
        case TpChar:
        case TpUChar:
        case TpArrayChar:
        case TpArrayUChar:
            {
                if( !isArray(fieldtype) )
                {
                    const uChar*  ptr2uchar( (const uChar*)dataptr );

                    recordref.define( fieldidx, *ptr2uchar );
                    retval=True;
                }
                else
                {
                    //
                    //  We need to construct an array!
                    //
                    uLong         bytesneeded;
                    Array<uChar>  tmp( fieldshape );
                    
                    //
                    //  Verify that the datasegment we're trying to
                    //  read from is large enough....
                    //
                    bytesneeded=tmp.nelements();
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        uChar*   buffer( new uChar[tmp.nelements()] );
                        
                        //
                        //  FITS -> LOCAL conversion
                        //
                        FITS::f2l( buffer, (void *)dataptr, tmp.nelements() );

                        //
                        //  Now that's verified, we can safely take
                        //  the storage!
                        //
                        tmp.takeStorage( fieldshape, buffer, SHARE );
                        recordref.define( fieldidx, tmp );

                        //
                        //  Delete the buffer
                        //
                        delete [] buffer;

                        //
                        // And indicate success.
                        //
                        retval=True;
                    }
                }
            }
            break;
            
        case TpShort:
        case TpArrayShort:
            {
                if( !isArray(fieldtype) )
                {
                    Short  tmp;
                    
                    //
                    //  Do FITS -> LOCAL conversion
                    //
                    FITS::f2l( &tmp, (void *)dataptr, 1 );
                    
                    //
                    //  And stuff the value into the recordfield
                    //
                    recordref.define( fieldidx, tmp );
                    
                    retval=True;
                }
                else
                {
                    //
                    //  Array of short!
                    //
                    uLong        bytesneeded;
                    Array<Short> tmp( fieldshape );
                    
                    bytesneeded=tmp.nelements()*sizeof(Short);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        Short*  buffer( new Short[tmp.nelements()] );
                        
                        //
                        //  FITS -> LOCAL
                        //
                        FITS::f2l( buffer, (void *)dataptr, tmp.nelements() );
                        
                        //
                        //  Let the array have the storage (for now)
                        //
                        tmp.takeStorage( fieldshape, buffer, SHARE );
                        
                        recordref.define( fieldidx, tmp );
                        
                        //
                        //  Buffer can go now!
                        //
                        delete [] buffer;

                        retval=True;
                    }
                }
                
            }
            break;

        case TpInt:
        case TpArrayInt:
            {
                if( !isArray(fieldtype) )
                {
                    Int  tmp;
                    
                    //
                    //  Do FITS -> LOCAL conversion
                    //
                    FITS::f2l( &tmp, (void *)dataptr, 1 );
                    
                    //
                    //  And stuff the value into the recordfield
                    //
                    recordref.define( fieldidx, tmp );
                    
                    retval=True;
                }
                else
                {
                    //
                    //  Array of ints!
                    //
                    uLong      bytesneeded;
                    Array<Int> tmp( fieldshape );
                    
                    bytesneeded=tmp.nelements()*sizeof(Int);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        Int*  buffer( new Int[tmp.nelements()] );
                        
                        //
                        //  FITS -> LOCAL
                        //
                        FITS::f2l( buffer, (void *)dataptr, tmp.nelements() );
                        
                        //
                        //  Let the array have the storage (for now)
                        //
                        tmp.takeStorage( fieldshape, buffer, SHARE );
                        
                        recordref.define( fieldidx, tmp );
                        
                        //
                        //  Buffer can go now!
                        //
                        delete [] buffer;

                        retval=True;
                    }
                }
            }
            break;

        case TpFloat:
        case TpArrayFloat:
            {
                if( !isArray(fieldtype) )
                {
                    Float  tmp;
                    
                    //
                    //  Do FITS -> LOCAL conversion
                    //
                    FITS::f2l( &tmp, (void *)dataptr, 1 );
                    
                    //
                    //  And stuff the value into the recordfield
                    //
                    recordref.define( fieldidx, tmp );
                    
                    retval=True;
                }
                else
                {
                    //
                    //  Array of floats!
                    //
                    uLong        bytesneeded;
                    Array<Float> tmp( fieldshape );
                    
                    bytesneeded=tmp.nelements()*sizeof(Float);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        Float*  buffer( new Float[tmp.nelements()] );
                        
                        //
                        //  FITS -> LOCAL
                        //
                        FITS::f2l( buffer, (void *)dataptr, tmp.nelements() );
                        
                        //
                        //  Let the array have the storage (for now)
                        //
                        tmp.takeStorage( fieldshape, buffer, SHARE );
                        
                        recordref.define( fieldidx, tmp );
                        
                        //
                        //  Buffer can go now!
                        //
                        delete [] buffer;
                        
                        retval=True;
                    }
                }
            }
            break;

        case TpDouble:
        case TpArrayDouble:
            {
                if( !isArray(fieldtype) )
                {
                    Double  tmp;
                    
                    //
                    //  Do FITS -> LOCAL conversion
                    //
                    FITS::f2l( &tmp, (void *)dataptr, 1 );
                    
                    //
                    //  And stuff the value into the recordfield
                    //
                    recordref.define( fieldidx, tmp );
                    
                    retval=True;
                }
                else
                {
                    //
                    //  Array of doubles!
                    //
                    uLong         bytesneeded;
                    Array<Double> tmp( fieldshape );
                    
                    bytesneeded=tmp.nelements()*sizeof(Double);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        Double*  buffer( new Double[tmp.nelements()] );
                        
                        //
                        //  FITS -> LOCAL
                        //
                        FITS::f2l( buffer, (void *)dataptr, tmp.nelements() );
                        
                        //
                        //  Let the array have the storage (for now)
                        //
                        tmp.takeStorage( fieldshape, buffer, SHARE );
                        
                        recordref.define( fieldidx, tmp );
                        
                        //
                        //  Buffer can go now!
                        //
                        delete [] buffer;

                        retval=True;
                    }
                }
            }
            break;

        case TpComplex:
        case TpArrayComplex:
            {
                if( !isArray(fieldtype) )
                {
                    Complex  tmp;
                    
                    //
                    //  Do FITS -> LOCAL conversion
                    //
                    FITS::f2l( &tmp, (void *)dataptr, 1 );
                    
                    //
                    //  And stuff the value into the recordfield
                    //
                    recordref.define( fieldidx, tmp );
                    
                    retval=True;
                }
                else
                {
                    //
                    //  Array of complex!
                    //
                    uLong          bytesneeded;
                    Array<Complex> tmp( fieldshape );
                    
                    bytesneeded=tmp.nelements()*sizeof(Complex);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        Complex*  buffer( new Complex[tmp.nelements()] );
                        
                        //
                        //  FITS -> LOCAL
                        //
                        FITS::f2l( buffer, (void *)dataptr, tmp.nelements() );
                        
                        //
                        //  Let the array have the storage (for now)
                        //
                        tmp.takeStorage( fieldshape, buffer, SHARE );
                        
                        recordref.define( fieldidx, tmp );
                        
                        //
                        //  Buffer can go now!
                        //
                        delete [] buffer;

                        retval=True;
                    }
                }
            }
            break;

        case TpDComplex:
        case TpArrayDComplex:
            {
                if( !isArray(fieldtype) )
                {
                    DComplex  tmp;
                    
                    //
                    //  Do FITS -> LOCAL conversion
                    //
                    FITS::f2l( &tmp, (void *)dataptr, 1 );
                    
                    //
                    //  And stuff the value into the recordfield
                    //
                    recordref.define( fieldidx, tmp );
                    
                    retval=True;
                }
                else
                {
                    //
                    //  Array of dcomplex!
                    //
                    uLong           bytesneeded;
                    Array<DComplex> tmp( fieldshape );
                    
                    bytesneeded=tmp.nelements()*sizeof(DComplex);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        DComplex*  buffer( new DComplex[tmp.nelements()] );
                        
                        //
                        //  FITS -> LOCAL
                        //
                        FITS::f2l( buffer, (void *)dataptr, tmp.nelements() );
                        
                        //
                        //  Let the array have the storage (for now)
                        //
                        tmp.takeStorage( fieldshape, buffer, SHARE );
                        
                        recordref.define( fieldidx, tmp );
                        
                        //
                        //  Buffer can go now!
                        //
                        delete [] buffer;

                        retval=True;
                    }
                }
            }
            break;


        default:
            //
            //  Unhandled....
            //
            break;
    }
    return retval;
}

//
//  Flush the contents of the indicated recordfield to the datasegment
//  (if possible)
//
Bool flushRecordField( const RecordInterface& recordref,
                       const RecordFieldId& fieldidx,
                       FITSDataSegment& datasegmentref,
                       uLong offset )
{
    //
    //  The return value for this function
    //
    Bool       retval( False );

    //
    //  Elementary knowledge about the field we're adressing
    //
    DataType   fieldtype( recordref.dataType(fieldidx) );
    IPosition  fieldshape( recordref.shape(fieldidx) );

    //
    //  First: attempt to get the dataptr
    //
    void*      dataptr( datasegmentref[offset] );
    
    if( !dataptr )
    {
        return retval;
    }
    
    //
    // Ok. Starting pont validated. End point validation will be done
    // later
    //

    //
    //  Take action depending on the type of the field...
    //
    switch( fieldtype )
    {
        case TpChar:
        case TpUChar:
        case TpArrayChar:
        case TpArrayUChar:
            {
                if( !isArray(fieldtype) )
                {
                    *((uChar*)dataptr)=recordref.asuChar( fieldidx );
                    retval=True;
                }
                else
                {
                    //
                    //  It's an array. We havr to do some more work
                    //  now....
                    //
                    Bool                deleteit;
                    uLong               bytesneeded;
                    const uChar*        ptr2storage;
                    const Array<uChar>& tmp( recordref.asArrayuChar(fieldidx) );
                    
                    //
                    //  Check endpoint of the array....
                    //
                    bytesneeded=tmp.nelements();
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        //
                        //  Ok. We have enough space to write all the
                        //  databytes...
                        //
                        if( (ptr2storage=tmp.getStorage(deleteit))!=0 )
                        {
                            //
                            //  Do local -> fits conversion
                            //
                            FITS::l2f( dataptr, (uChar*)ptr2storage, tmp.nelements() );
                            retval=True;
                        }
                        
                        //
                        //  Release the storage
                        //
                        tmp.freeStorage( ptr2storage, deleteit );
                    }
                }
            }
            break;

        case TpShort:
        case TpArrayShort:
            {
                if( !isArray(fieldtype) )
                {
                    Short tmp( recordref.asShort(fieldidx) );
                    
                    //
                    //  Do local -> fits conversion
                    //
                    FITS::l2f( dataptr, &tmp, 1 );

                    retval=True;
                }
                else
                {
                    //
                    //  It's an array. We havr to do some more work
                    //  now....
                    //
                    Bool                deleteit;
                    uLong               bytesneeded;
                    const Short*        ptr2storage;
                    const Array<Short>& tmp( recordref.asArrayShort(fieldidx) );
                    
                    //
                    //  Check endpoint of the array....
                    //
                    bytesneeded=(uLong)tmp.nelements()*(uLong)sizeof(Short);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        //
                        //  Ok. We have enough space to write all the
                        //  databytes...
                        //
                        if( (ptr2storage=tmp.getStorage(deleteit))!=0 )
                        {
                            //
                            //  Do local -> fits conversion
                            //
                            FITS::l2f( dataptr, (Short*)ptr2storage, tmp.nelements() );
                            retval=True;
                        }
                        
                        //
                        //  Release the storage
                        //
                        tmp.freeStorage( ptr2storage, deleteit );
                    }
                }
            }
            break;

        case TpInt:
        case TpArrayInt:
            {
                if( !isArray(fieldtype) )
                {
                    Int tmp( recordref.asInt(fieldidx) );
                    
                    //
                    //  Do local -> fits conversion
                    //
                    FITS::l2f( dataptr, &tmp, 1 );

                    retval=True;
                }
                else
                {
                    //
                    //  It's an array. We havr to do some more work
                    //  now....
                    //
                    Bool              deleteit;
                    uLong             bytesneeded;
                    const Int*        ptr2storage;
                    const Array<Int>& tmp( recordref.asArrayInt(fieldidx) );
                    
                    //
                    //  Check endpoint of the array....
                    //
                    bytesneeded=(uLong)tmp.nelements()*(uLong)sizeof(Int);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        //
                        //  Ok. We have enough space to write all the
                        //  databytes...
                        //
                        if( (ptr2storage=tmp.getStorage(deleteit))!=0 )
                        {
                            //
                            //  Do local -> fits conversion
                            //
                            FITS::l2f( dataptr, (Int*)ptr2storage, tmp.nelements() );
                            retval=True;
                        }
                        
                        //
                        //  Release the storage
                        //
                        tmp.freeStorage( ptr2storage, deleteit );
                    }
                }
            }
            break;
            
        case TpFloat:
        case TpArrayFloat:
            {
                if( !isArray(fieldtype) )
                {
                    Float tmp( recordref.asFloat(fieldidx) );
                    
                    //
                    //  Do local -> fits conversion
                    //
                    FITS::l2f( dataptr, &tmp, 1 );

                    retval=True;
                }
                else
                {
                    //
                    //  It's an array. We havr to do some more work
                    //  now....
                    //
                    Bool                deleteit;
                    uLong               bytesneeded;
                    const Float*        ptr2storage;
                    const Array<Float>& tmp( recordref.asArrayFloat(fieldidx) );
                    
                    //
                    //  Check endpoint of the array....
                    //
                    bytesneeded=(uLong)tmp.nelements()*(uLong)sizeof(Float);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        //
                        //  Ok. We have enough space to write all the
                        //  databytes...
                        //
                        if( (ptr2storage=tmp.getStorage(deleteit))!=0 )
                        {
                            //
                            //  Do local -> fits conversion
                            //
                            FITS::l2f( dataptr, (Float*)ptr2storage, tmp.nelements() );
                            retval=True;
                        }
                        
                        //
                        //  Release the storage
                        //
                        tmp.freeStorage( ptr2storage, deleteit );
                    }
                }
            }
            break;

        case TpDouble:
        case TpArrayDouble:
            {
                if( !isArray(fieldtype) )
                {
                    Double tmp( recordref.asDouble(fieldidx) );
                    
                    //
                    //  Do local -> fits conversion
                    //
                    FITS::l2f( dataptr, &tmp, 1 );

                    retval=True;
                }
                else
                {
                    //
                    //  It's an array. We havr to do some more work
                    //  now....
                    //
                    Bool                 deleteit;
                    uLong                bytesneeded;
                    const Double*        ptr2storage;
                    const Array<Double>& tmp( recordref.asArrayDouble(fieldidx) );
                    
                    //
                    //  Check endpoint of the array....
                    //
                    bytesneeded=(uLong)tmp.nelements()*(uLong)sizeof(Double);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        //
                        //  Ok. We have enough space to write all the
                        //  databytes...
                        //
                        if( (ptr2storage=tmp.getStorage(deleteit))!=0 )
                        {
                            //
                            //  Do local -> fits conversion
                            //
                            FITS::l2f( dataptr, (Double*)ptr2storage, tmp.nelements() );
                            retval=True;
                        }
                        
                        //
                        //  Release the storage
                        //
                        tmp.freeStorage( ptr2storage, deleteit );
                    }
                }
            }
            break;

        case TpComplex:
        case TpArrayComplex:
            {
                if( !isArray(fieldtype) )
                {
                    Complex tmp( recordref.asComplex(fieldidx) );
                    
                    //
                    //  Do local -> fits conversion
                    //
                    FITS::l2f( dataptr, &tmp, 1 );

                    retval=True;
                }
                else
                {
                    //
                    //  It's an array. We havr to do some more work
                    //  now....
                    //
                    Bool                  deleteit;
                    uLong                 bytesneeded;
                    const Complex*        ptr2storage;
                    const Array<Complex>& tmp( recordref.asArrayComplex(fieldidx) );
                    
                    //
                    //  Check endpoint of the array....
                    //
                    bytesneeded=(uLong)tmp.nelements()*(uLong)sizeof(Complex);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        //
                        //  Ok. We have enough space to write all the
                        //  databytes...
                        //
                        if( (ptr2storage=tmp.getStorage(deleteit))!=0 )
                        {
                            //
                            //  Do local -> fits conversion
                            //
                            FITS::l2f( dataptr, (Complex*)ptr2storage, tmp.nelements() );
                            retval=True;
                        }
                        
                        //
                        //  Release the storage
                        //
                        tmp.freeStorage( ptr2storage, deleteit );
                    }
                }
            }
            break;

        case TpDComplex:
        case TpArrayDComplex:
            {
                if( !isArray(fieldtype) )
                {
                    DComplex tmp( recordref.asDComplex(fieldidx) );
                    
                    //
                    //  Do local -> fits conversion
                    //
                    FITS::l2f( dataptr, &tmp, 1 );

                    retval=True;
                }
                else
                {
                    //
                    //  It's an array. We havr to do some more work
                    //  now....
                    //
                    Bool                   deleteit;
                    uLong                  bytesneeded;
                    const DComplex*        ptr2storage;
                    const Array<DComplex>& tmp( recordref.asArrayDComplex(fieldidx) );
                    
                    //
                    //  Check endpoint of the array....
                    //
                    bytesneeded=(uLong)tmp.nelements()*(uLong)sizeof(DComplex);
                    
                    if( (offset+bytesneeded)<datasegmentref.getSegmentSize() )
                    {
                        //
                        //  Ok. We have enough space to write all the
                        //  databytes...
                        //
                        if( (ptr2storage=tmp.getStorage(deleteit))!=0 )
                        {
                            //
                            //  Do local -> fits conversion
                            //
                            FITS::l2f( dataptr, (DComplex*)ptr2storage, tmp.nelements() );
                            retval=True;
                        }
                        
                        //
                        //  Release the storage
                        //
                        tmp.freeStorage( ptr2storage, deleteit );
                    }
                }
            }
            break;

        default:
            //
            //  Unhandled datatype
            //
            break;
    }

    return retval;
}








