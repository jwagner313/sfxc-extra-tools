//
//  Class:
//
//          FITSPrimaryArray
//
//  Purpose:
//
//          This class describes a PrimaryArray in a FITS file!
//
//
//  Author:
//
//          Harro Verkouter,   26-05-1998
//
//
//     $Id: FITSPrimaryArray.h,v 1.3 2006/01/13 11:35:44 verkout Exp $
//
#ifndef FITSPRIMARYARRAY_H_INC
#define FITSPRIMARYARRAY_H_INC


//
//  We need this include since it is our baseclass!
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSArrayHDU.h>
#include <casa/Containers/RecordInterface.h>


//
//  The primary array hdu class. It isA FITSArrayHDU, therefore we
//  derive it from that class
//
class FITSPrimaryArray :
    public FITSArrayHDU
{
public:
    //
    //  The public interface
    //

    //
    //  Create a primary array of a given type (basic fits type: the
    //  type of data items),
    //
    FITSPrimaryArray( casa::DataType basicfitstype );

    //
    //  Create the primary array from a fitskeywordlist, that's just
    //  been read from an existing fitsfile represented by
    //  'ptr2stream'
    //
    FITSPrimaryArray( casa::FitsKeywordList& kwlist, casa::ByteIO* ptr2stream );

    //
    //  The type of this hdu
    //
    virtual HDUObject::hdu_t getHDUtype( void ) const;

    //
    // Define get/put actions on the array!
    //
    // Note: these functions are NOT defined on the baseclass, and
    // they are virtual in THIS class -> derived classes from this
    // object may overload the get/put actions.
    //
    virtual casa::Bool      put( const casa::RecordInterface& recordref );
    
    virtual casa::Bool      get( casa::RecordInterface& recordref ) const;

    //
    //  Destruct the primary array
    //
    virtual ~FITSPrimaryArray();
    
protected:
    //
    //  Protected functions
    //
    

private:
    //
    //  The primary arrays private parts
    //

    //
    //  The private methods
    //

    //
    //  These are undefined and inaccessible
    //
    FITSPrimaryArray();
    FITSPrimaryArray( const FITSPrimaryArray& );
    FITSPrimaryArray& operator=( const FITSPrimaryArray& );
};



#endif
