//
// Class:
//
//       FITSArrayHDU
//
// Purpose:
//
//       Yet another layer of abstraction. A FITSArrayHDU object is a
//       HDUObject, bu it does not have to feauture everything that's
//       possible with a tableHDUObject (and vice versa). ArrayHDU's
//       should feature adding of axes and parameters (for primary
//       group hdu's). Furthermore, dataarray-specific functions (that
//       do not resemble anything like a table) may be implemented in
//       this kind of HDUs
//
// Author:
//
//       Harro Verkouter,     06-04-1998
//
//
//        $Id: FITSArrayHDU.h,v 1.3 2006/01/13 11:35:42 verkout Exp $
// 
#ifndef FITSARRAYHDU_H
#define FITSARRAYHDU_H

#include <casa/aips.h>
#include <casa/Arrays/Vector.h>
#include <casa/Utilities/DataType.h>
#include <casa/Arrays/IPosition.h>

#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>


//
//  Forward declarations
//
//class RecordInterface;


class FITSArrayHDU :
    public HDUObject
{
public:
    //
    //  Add an axis to this Array. The return value is the (0-based)
    //  axisnumber assigned to this axis.
    //
    //  IMPORTANT NOTE:
    //
    //  The axis-object that is passed, will be COPIED before it is
    //  added internally. If you make changes to your local copy of
    //  the object (i.e. the function parameter) they will not be
    //  reflected in the FITSArrayHDU-object. Use one of the
    //  'getAxis()' methods in order to get access to the axes
    //  themselves.
    //
    //  NOTE:
    //  
    //  If somebody feels the need to keep track of the axes
    //  him/herself, one can derive a new HDU from this one and musrt
    //  overload the 'addAxis()', 'getAxis()',
    //  'getDataDescriptionKeywords()' methods.
    // 
    //
    virtual casa::Int       addAxis( const FITSAxis& axis );    

    //
    // HeaderdataUnits representing an array (rather than a table) may
    // be asked to return the size of the array they currently
    // represent. Basically it is (the product of all the axislengths
    // (+ any parameters)) times the datasize. It can be used when
    // asked to skip a hdu, or to read/write the whole array?
    //
    virtual casa::uLong     getCurrentDataVolumeInBytes( void ) const;

    //
    // Return the shape of the array represented by this ArrayHDU!
    //
    // NOTE: this function is NOT virtual, since the ArrayHDU class
    // does all the bookkeeping w.r.t. axes. Therfore, the ArrayHDU
    // does exactly know the shape of the array it represents.
    //
    casa::IPosition         shape( void ) const;

    //
    //  Get access to axes?
    //
    //  NOTE: If the requested axis does not exist, an xception is
    //  thrown! Use the 'axisExists' function first. That will
    //  indicate whether or not the requested axis is present.
    //
    casa::Bool              axisExists( const casa::String& axistype ) const;
    FITSAxis&         getAxis( const casa::String& axistype ) const;
    FITSAxis&         getAxis( casa::uInt index ) const;
    
    //
    //  Destruct the array HDU
    //
    virtual ~FITSArrayHDU();

protected:
    //
    //  We make the cretaors protected, since only derived classes may
    //  create an arrayHDU: default ArrayHDU's do not exist!
    //

    //
    //  Create a FITSArrayHDU, representing an array of values of type
    //  'basicfitstype'
    //
    FITSArrayHDU( const casa::DataType basicfitstype );
    
    //
    //  Create the arrayhdu from a keywordlist that has just been read
    //  from a file! Pass the stream as well, so the datasegment can
    //  be filled in automatically
    //
    FITSArrayHDU( casa::FitsKeywordList& kwlist, casa::ByteIO* ptr2stream );

    //
    //  Implement functions to translate the array hdu into several kinds of keywords
    //
    virtual casa::FitsKeywordList getDataDescriptionKeywords( void ) const;

    //
    //  Protected functions: they are accessible by the derived
    //  classes. To be used when the derived class thinks it's time
    //  the arrayhdu should really create the datasegment
    //

private:
    //
    //  The private parts:
    //

    //
    //  An arrayHDU object keeps a list of axes that describe the
    //  data!
    //
    casa::Vector<FITSAxis*>       itsAxisptrs;

    //
    //  The private methods
    //

    //
    //  Prevent copying and assignment
    //
    FITSArrayHDU();
    FITSArrayHDU( const FITSArrayHDU& );
    FITSArrayHDU& operator=( const FITSArrayHDU& );
};


#endif
