//  Implementation of the FITSPrimaryGroup class
//
//  Author:
//
//         Harro Verkouter,   26-05-1998
//
//
//     $Id: FITSPrimaryGroup.cc,v 1.8 2011/10/12 12:45:35 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSPrimaryGroup.h>

#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSParameter.h>
#include <casa/Containers/RecordInterface.h>
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <casa/Containers/RecordDesc.h>
#include <jive/Utilities/jexcept.h>

#include <casa/Containers/Block.h>

using namespace casa;

FITSPrimaryGroup::FITSPrimaryGroup( DataType basicfitstype ) :
    FITSArrayHDU( basicfitstype ),
    itsNumberOfGroups( 0L )
{
    //
    //  Primary groups always have the first axis set to length '0'
    //
    this->addAxis( FITSAxis("degenerate", "", 0) );
}


FITSPrimaryGroup::FITSPrimaryGroup( FitsKeywordList& kwlist, ByteIO* ptr2stream ) :
    FITSArrayHDU( kwlist, ptr2stream ),
    itsNumberOfGroups( 0L )
{
    FitsKeyword*  keywordptr;
    
    if( (keywordptr=kwlist(FITS::GCOUNT))==0 )
    {
        THROW_JEXCEPT("Attempt to create a primary group but no GCOUNT "
                << "keyword was found!");
    }
    
    itsNumberOfGroups=keywordptr->asInt();
    kwlist.del();
    

    if( (keywordptr=kwlist(FITS::HISTORY))==0 )
    {
        THROW_JEXCEPT("Attempt to create a primary group but no HISTORY "
		      << "keyword was found!");
    }    
    //
    //  We need to take out all the parameters
    //
    if( (keywordptr=kwlist(FITS::PCOUNT))==0 )
    {
        THROW_JEXCEPT("Attempt to create primary group, but no PCOUNT "
                << "keyword was found!");
    }

    Int   nrparameters( keywordptr->asInt() ); 

    //
    //  Remove the processed keyword from the list
    //
    kwlist.del();
    
    //
    //  Resize the vector of parameters
    //
    itsParameterptrs.resize( nrparameters );
    
    for( Int pcount=0; pcount<nrparameters; pcount++ )
    {
        itsParameterptrs(pcount) = new FITSParameter(pcount, kwlist, this);
    }

    //
    //  After taking out all the keywords for axes etc. we're left
    //  with notjing but the 'extra' keywords. Therefore we copy
    //  what's left in the keywordlist into our own list of extra
    //  keywords.
    //
    this->setExtraKeywords( kwlist );

    //
    //  Now's the time to make the DataSegment!
    //
    this->makeDataSegment( ptr2stream );
}

Int FITSPrimaryGroup::addParameter( const FITSParameter& parameter )
{
    Int    retval( -1 );
    
    if( this->hasDataSegment() )
    {
        return retval;
    }
    
    Int            curparameternr;
    FITSParameter* newparameterptr( 0 );
    
    //
    //  Step one: in order to determine the correct number for this
    //  parameter, we first find out how many parameters there already
    //  are.
    //
    itsParameterptrs.shape( curparameternr );
    
    //
    //  Now we know that, we can safely create the new parameterobject with
    //  the proper settings
    //
    newparameterptr= new FITSParameter( curparameternr, parameter, this );
    
    //
    //  Make room for this one...
    //
    itsParameterptrs.resize( curparameternr+1, True );
    
    //
    //  Copy it in
    //
    itsParameterptrs( curparameternr )=newparameterptr;
    
    //
    //  Return success...
    //
    return curparameternr;
}

Bool FITSPrimaryGroup::setNumberOfGroups( uLong numberofgroups )
{
    Bool   retval( False );
    
    //
    //  Only if no datasegment present yet it is allowed to change the
    //  number of groups!
    //
    if( !this->hasDataSegment() )
    {
        itsNumberOfGroups=numberofgroups;
        retval=True;
    }
    return retval;
}



HDUObject::hdu_t FITSPrimaryGroup::getHDUtype( void ) const
{
    return HDUObject::primarygroup;
}

//
//  We use the recordinterface to pass the array. Assume the array is
//  in field 0 of the recordinterface
//
Bool FITSPrimaryGroup::put( uLong group, const RecordInterface& recordref )
{
    Bool             retval( False );
    
    //
    //  Step one: verify that the recorddescription of the record
    //  passed to us by the user, is compatible with the description
    //  of a group! NOTE: we are (in principle) not interested in the
    //  fieldnames. Bute the number of fields, the type an shape do
    //  matter. therefore we use the != (or ==) operators rather than
    //  the 'isEqual()' ember functions.
    //
    if( recordref.description()!=this->getGroupDescription() )
    {
        return retval;
    }
    
    //
    //  Ok. the record the user passed is compatible for what we
    //  expect for a group. The idea now is to first put all
    //  parameters and last but not least the array!
    //
    

    //
    //  Make sure we have a datasegment available
    //
    if( !this->hasDataSegment() )
    {
        //
        //  No data allocated yet....
        //
        this->makeDataSegment();
        
        if( !this->hasDataSegment() )
        {
            THROW_JEXCEPT("Aargh. Failed to create datasegment for PrimaryGroup!");
        }
    }

    //
    //  Do write all the parameters
    //
    Int          nrparameters;
    
    itsParameterptrs.shape( nrparameters );
    for( Int i=0; i<nrparameters; i++ )
    {
        itsParameterptrs(i)->put( group, recordref, i );
    }
    
    //
    //  Calculate the offset where the array of this group should end
    //  up!
    //
    uLong        totaloffset( 0L );
    
    totaloffset=(group*this->getGroupSizeInBytes())+((uLong)nrparameters*(uLong)this->getDataSizeInBytes());

    //
    //  Flush the contents of the recordfield into the datasegment
    //
    retval=flushRecordField( recordref, nrparameters, this->getDataSegment(), totaloffset );

    return retval;
}

Bool FITSPrimaryGroup::get( uLong group, RecordInterface& recordref ) const
{
    Bool             retval( False );
    RecordDesc       thisgroupdesc( this->getGroupDescription() );
    

    //
    //  If no datasegment, return False
    //
    if( !this->hasDataSegment() )
    {
        return retval;
    }
    
    //
    //  Attempt to restructure the user's record to be compatible to
    //  what the group represents.
    //
    Bool   except( False );
    
    try
    {        
        if( recordref.description()!=thisgroupdesc )
        {
            recordref.restructure( thisgroupdesc );
        }
    }
    catch( const std::exception& x )
    {
        cerr << x.what() << endl;
        except=True;
    }

    //
    //  If we failed to reshape the Record -> get out!
    //
    if( except )
    {
        return retval;
    }
    
    //
    //  Get all the parameters!
    //
    Int   nrparameters;
    
    itsParameterptrs.shape( nrparameters );
    for( Int i=0; i<nrparameters; i++ )
    {
        itsParameterptrs(i)->get( group, recordref, i );
    }
    
    //
    //  Only thing left is to get the array!
    //
    uLong     totaloffset( 0L );
    
    //
    //  Determine the total offset at which the array should be found
    //
    totaloffset=(group*this->getGroupSizeInBytes())+((uLong)nrparameters*(uLong)this->getDataSizeInBytes());
    
    //
    //  Fill the recordfield from the datasegment
    //
    retval= fillRecordField( recordref, nrparameters, this->getDataSegment(), totaloffset );

    return retval;
}

//
//  Determine the data volume represented by this object
//
uLong FITSPrimaryGroup::getCurrentDataVolumeInBytes( void ) const
{
    uLong    retval( 1L );

    //
    //  The formula to calculate the datasize for this HDU is:
    //
    //   (abs(BITPIX)/8)*(GCOUNT * (PCOUNT+shape(GROUP))
    //
    
    //
    //  Let's first calculate the volume of the array for very group
    //  (the product of NAXISn where n>1 && naxis<=NAXIS)
    //
    IPosition  groupshape( this->groupShape() );
    
    //
    //  If no shape -> empty -> datasize=0
    //
    if( groupshape.nelements()==0 )
    {
	return 0L;
    }
    

    retval*=(uLong)(groupshape.product());
    
    //
    //  'tmp' holds the volume (in dataitems) of a group
    //
    Int   nrparameters;
    
    itsParameterptrs.shape( nrparameters );
    
    //
    //  We add the pcount (=nrparameters) to 'tmp'
    //
    retval+=(uLong)nrparameters;
    
    //
    //  We multiply by the nr of groups...
    //
    retval*=itsNumberOfGroups;
    
    //
    // Finally, multiply by the number of bytes/dataitem
    //
    retval*=(uLong)(this->getDataSizeInBytes());
    
    return retval;
}

IPosition FITSPrimaryGroup::groupShape( void ) const
{
    IPosition   totalshape( this->shape() );
    IPosition   retval;

    if( totalshape.nelements() )
    {
	retval.resize( totalshape.nelements()-1 );
    
	for( uInt i=0; i<retval.nelements(); i++ )
	{
	    retval(i)=totalshape(i+1);
	}
    }
    return retval;
}

uLong FITSPrimaryGroup::getGroupSizeInBytes( void ) const
{
    uInt   nrparameters( this->getNumberOfParameters() );
    uInt   nrarrayelements( this->groupShape().product() );
    
    return ((uLong)nrparameters+(uLong)nrarrayelements)*(uLong)(this->getDataSizeInBytes());
}


uLong FITSPrimaryGroup::getNumberOfGroups( void ) const
{
    return itsNumberOfGroups;
}

//
//  Let's create a description of the group
//
RecordDesc FITSPrimaryGroup::getGroupDescription( void ) const
{
    Int            nrparameters;
    DataType       thistype( this->getDataType() );
    RecordDesc     retval;
    
    //
    //  Ok: the structure of a group record is as follows:
    //
    //  <n-parameters of type T><Array of type T with Shape S>
    //
    itsParameterptrs.shape( nrparameters );
    for( Int i=0; i<nrparameters; i++ )
    {
        RecordDesc   newfield;
        
        newfield.addField( itsParameterptrs(i)->ptype(), thistype );
        
        retval.mergeField( newfield, 0, RecordInterface::RenameDuplicates );
    }
    
    //
    //  Add an array of 'thistype' with shape 'groupShape()'
    //
    retval.addField( "group", asArray(thistype), this->groupShape() );

    return retval;
}


uInt FITSPrimaryGroup::parameterExists( const String& parametertype ) const
{
    Int    nrparameters;
    uInt   retval( 0 );
    
    itsParameterptrs.shape( nrparameters );
    
    for( Int i=0; i<nrparameters; i++ )
    {
        if( itsParameterptrs(i)->ptype()==parametertype )
        {
            retval++;
        }
    }
    return retval;
}

uInt FITSPrimaryGroup::getNumberOfParameters( void ) const
{
    Int   nrofparameters;
    
    itsParameterptrs.shape( nrofparameters );
    
    return (uInt)nrofparameters;
}

FITSParameter& FITSPrimaryGroup::getParameter( const String& parametername, uInt occurrence ) const
{
    Int            i;
    Int            currentoccurrence( 0 );
    Int            nrparameters;
    FITSParameter* retvalptr( 0 );
    
    itsParameterptrs.shape( nrparameters );
    
    for( i=0; i<nrparameters; i++ )
    {
        if( itsParameterptrs(i)->ptype()==parametername )
        {
            if( occurrence==(uInt)currentoccurrence )
            {
                break;
            }
            else
            {
                currentoccurrence++;
            }
        }
    }
    
    if( i<nrparameters )
    {
        //
        //  Loop has been prematurely ended: column is found!
        //
        retvalptr=itsParameterptrs(i);
    }

    if( !retvalptr )
    {
        THROW_JEXCEPT("Parameter `" << parametername << "' not found!");
    }
    return (*retvalptr);
}

FITSParameter& FITSPrimaryGroup::getParameter( uInt parameternumber ) const
{
    if( parameternumber>=this->getNumberOfParameters() )
    {
        THROW_JEXCEPT("Request for parameter number out of range!"
                << "req: " << parameternumber << ", "
                << "avail: " << this->getNumberOfParameters());
    }
    return *(itsParameterptrs(parameternumber));
}



FITSPrimaryGroup::~FITSPrimaryGroup()
{
    Int    counter;
    
    //
    //  Delete all FITSParameters
    //
    itsParameterptrs.shape( counter );
    
    for( Int i=0; i<counter; i++ )
    {
        delete itsParameterptrs( i );
    }
    
    //
    //  And we're done!
    //
}

/********************************************************************************/
//
//
//               The protected methods
//
//
/********************************************************************************/
FitsKeywordList FITSPrimaryGroup::getDataDescriptionKeywords( void ) const
{
    FitsKeyword*     keywordptr;
    FitsKeywordList  retval( FITSArrayHDU::getDataDescriptionKeywords() );

    //
    //  Skip to end of list
    //
    retval.last();
    
    //
    //  And add the keywords we need!
    //
    Int    nrparameters;
    
    itsParameterptrs.shape( nrparameters );
    
    retval.mk( FITS::GROUPS,       True            , "This is a random group fitsfile!" );
    retval.mk( FITS::GCOUNT, (Int)itsNumberOfGroups, "The number of groups" );
    retval.mk( FITS::PCOUNT,      nrparameters     , "The number of random parameters preceding each group" );    
    //
    //  Now loop over all parameters and add the
    //  keyword-representations of those to the return value.
    //
    for( Int i=0; i<nrparameters; i++ )
    {
        FitsKeywordList   parameter( itsParameterptrs(i)->asFITSKeys() );
        
        //
        //  Position the 'cursors'
        //
        retval.last();
        parameter.first();
        parameter.next();
        keywordptr=parameter.curr();
        while( keywordptr )
        {
            FitsKeyword*  ptr2newkw( new FitsKeyword(*keywordptr) );
            
            retval.insert( *ptr2newkw );
            keywordptr=parameter.next();
        }
    }
    
    //
    //  And we're done
    //
    return retval;
}

