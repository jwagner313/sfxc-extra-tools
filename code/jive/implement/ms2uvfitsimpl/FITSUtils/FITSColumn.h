//
//  Class:
//
//        FITSColumn
//
//
//  Purpose:
//
//        This class will describe the properties of a
//        column in a FITS table. These properties encompass the
//        datatype ( ..FORM fields in the FITSdescription), the
//        description of the column and the width and the offset of
//        the column within a row in the FITS table.
//
//        I choose not to have this column templated bute use the
//        AIPS++ DataType enumeration instead, to prevent us from
//        having to upcast to templated type.
//
//
//
//
//  Author:
//
//        Harro Verkouter     27-3-1998
//
//
//      $Id: FITSColumn.h,v 1.6 2007/01/17 08:22:01 jive_cc Exp $
//
#ifndef FITSCOLUMN_H_INC
#define FITSCOLUMN_H_INC


#include <jive/ms2uvfitsimpl/FITSUtils/BasicFITSUtility.h>
//#include <jive/ms2uvfitsimpl/FITSUtils/FITSTableHDU.h>
#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Arrays/Slicer.h>
#include <casa/Utilities/DataType.h>
#include <casa/Arrays/Array.h>
#include <casa/Containers/RecordInterface.h>
#include <casa/Containers/RecordDesc.h>
#include <casa/Containers/RecordFieldId.h>

#include <fits/FITS.h>

#include <tables/Tables/ScalarColumn.h>
#include <tables/Tables/ArrayColumn.h>

#include <iostream>


class FITSTableHDU;

class FITSColumn :
    public BasicFITSUtility
{
public:
    //
    // Return the AIPS++ datatype that describes the nature of this
    // column
    //
    casa::DataType                getColumnDataType( void ) const;
    casa::RecordDesc              getColumnDescription( void ) const;

    //
    //  Translate this Column to Fitskeywords
    //
    virtual casa::FitsKeywordList asFITSKeys( void ) const;

    //
    //  Get or put the value in a cell. Derived columns MUST implement these methods!
    //
    virtual casa::Bool            get( casa::uInt rownr, casa::RecordInterface& recordref, const casa::RecordFieldId& fieldidx ) const =0;
    virtual casa::Bool            put( casa::uInt rownr, const casa::RecordInterface& recordref, const casa::RecordFieldId& fieldidx ) =0;

    
    //
    //  Let the user have access (mostly read-only) to some of the
    //  private parts!
    //

    //
    // The width of this particular column (in bytes)
    //
    virtual casa::uLong           getWidthInBytes( void ) const;
    

    //
    //  Accessors, use FITS nomenclature:
    //
    const casa::String&           ttype( void ) const;
    const casa::String&           tform( void ) const;
    const casa::String&           tunit( void ) const;

    //
    //  Some administrative functions, I don't think users will be
    //  much using these, but anyway.
    //
    casa::Int                     columnnr( void ) const;
    casa::uLong                   columnoffset( void ) const;

    //
    //  Do we happen to be a binary column, by any chance??
    //
    casa::Bool                    isBinaryColumn( void ) const;

    //
    //  Destruct this Column
    //
    virtual ~FITSColumn();

protected:
    //
    //  Three constructors. We make all of them protected, so only
    //  derived classes (actual specialization of the column class)
    //  are able to construct a column object.
    //
    //  1) Create a complete column -> user has control over all
    //  Column properties (but the Column number and the offset)
    //
    //  2) Create a column from a FitsKeywordlist -> intended use:
    //  attempt to read the column's settings from the list of
    //  keywords just read from file! The parent HDU tells us what our
    //  ordinal number and offset are.
    //
    //  3) A kind of copy-constructor -> the column settings of
    //  'other' are copied across apart from the columnnumber and
    //  offset. This constructor finds its use when a HeaderDataUnit
    //  needs to add a column. The user specifies a column that needs
    //  to be added to the HeaderdataUnit. The user does not know the
    //  actual number of the column. The HeaderdataUnit does know and
    //  uses this c'tor to create the correct column.
    //
    //
    //  NOTE: the number parameter (as well as the private datamember
    //  'itsColumnnumber' are Ints, because the value '-1' is used to
    //  indicate that the column has not been assigned to a valid HDU!
    //
    FITSColumn( const casa::String& type, const casa::String& comment,
                const casa::String& form, const casa::String& unit );
    
    FITSColumn( casa::Int number, casa::uLong offset,
                casa::FitsKeywordList& kwsource, FITSTableHDU* ptr2parent );
    
    FITSColumn( casa::Int number, casa::uLong offset,
                const FITSColumn& other, FITSTableHDU* ptr2parent );
   

    // 
    // Because of gcc3.4 (and higher) ISO C++ standard compliance,
    // this copy c'tor must exists (bummer).
    // "temporaries, even those passed by const-reference *MUST*
    //  be copy constructable" <-- b*stards! ;)
    // And this construct we use in the 'addColumn()' methods...
    // (passing a temporary by const ref)
    //
    // NOTE NOTE NOTE NOTE
    //    IF the 'other' object has a Parent, it doesn't get copied
    //    across!
    //
    FITSColumn( const FITSColumn& other );

    //
    //  The derived classes must have acces to these members: they
    //  first have to parse the TFORM field and hence they can decide
    //  what column type/shape datatype etc. it is!
    //
    casa::Bool      setDataType( const casa::DataType& newdatatype );
    casa::Bool      setColumnDescription( const casa::RecordDesc& newdescription );
    casa::Bool      setForm( const casa::String& newform );

    //
    //  Sometimes (e.g. for an ASCII-column) we may have to change the
    //  offset for this column...
    //
    void      setColumnOffset( casa::uLong newoffset );
    

protected:

    //
    //  Yuck! Do I not like this.... derived classes (and friends for
    //  that matter) can use this method to tell this Column
    //  Object that is really is a BinaryColumn... but.. since we must
    //  make decisions for keywords in another object (the
    //  BinaryTable) we must be able to find out if there are Matrix
    //  columns present and if so, we must even know which one it is!
    //
    //  Wait... you haven't heard the best part yet... it's not even
    //  part of the standard, these so-called matrix columns! They're
    //  an invention of the the Classic AIPS team... and since we
    //  *really* need classic AIPS we'll need to find a way to get our
    //  data through into classic AIPS...
    // 
    void        setBinaryColumnFlag( void );

private:
    //
    //  These are the variables that describe the Column
    //

    //
    //  The Column type
    //
    casa::String         itsColumnType;
    
    //
    //  The comment
    //
    casa::String         itsComment;
    
    //
    // This string describes the form of the column, according to
    // form= element_count+BASCIC_TYPE_CHARACTER,
    //
    //  e.g.  a column of single precision floats would have the
    //  following form: "1E"
    //
    //  a column with an array of 10 characters would have the form:
    //  "10A"
    //
    casa::String         itsForm;
    
    //
    //  The unit of the data in the column
    //
    casa::String         itsUnit;
    

    //
    //  If appropriate: which Columnnumber is this?
    //
    casa::Int            itsColumnnumber;

    //
    //  For that matter, we keep the current datatype in this variable
    //  (this is the AIPS++ DataType enum). This enum also holds
    //  arraytypes etc. just what we need!
    //
    casa::DataType       columnDataType;

    //
    //  How many data-type elements does this column take (form=
    //  <nrelements><datatype>)
    //
    casa::Int            itsNumberElements;
    
    //
    //  We also need to have the total column width and the column
    //  offset in the row of the table (if known of course: this is
    //  only valid if the column is attached to a HeaderDataUnit)!
    //

    //
    //  This is the column width in bytes
    //
    casa::uInt           itsTotalWidth;
    
    //
    //  The offset of this column in the row (also in bytes)
    //
    casa::uLong          itsOffsetInRow;

    //
    //  The record description that describes this column in AIPS++
    //  terminology (like a combination of DataType and IPosition). It
    //  will be filled in by the 'CheckFormatAndInit()' private member
    //  function.
    //
    casa::RecordDesc     itsColumnDescription;

    //
    //  The flag that tells us if we are a matrix column or not..
    //
    casa::Bool           myBinaryColumnFlag;


    //
    //  The Column's private methods
    //

    
    //
    //  We do not allow copying and assigning to Columns, nor do we
    //  allow default Columns!
    //
    FITSColumn();
    FITSColumn& operator=( const FITSColumn& );
    
};

//
//  Display the columns properties on a stream....
//
std::ostream& operator<<( std::ostream& osref, const FITSColumn& columnref );

#endif
