//
//  Class:
//
//        BasicFITSUtility
//
//  Purpose:
//
//        It is the abstract base class for FITS-utilities like
//        parameters, axes and columns. They all derive from this
//        class. This class has the virtual method 'asFITSkeys', which
//        (is supposed to) return a description of that FITSutility in
//        FitsKeywords!
//
//        For an axis, it would generate something like:
//
//        CRVAL<axisnumber>=<reference value of this axis>
//        CRPIX<axisnumber>=<reference pixel of this axis>
//          .
//          .
//        CTYPE<axisnumber>=<type of this axis>
//        
//
//
//  Author:
//
//        Harro Verkouter,    24-3-1998
//
//
//    $Id: BasicFITSUtility.h,v 1.4 2007/01/17 08:22:01 jive_cc Exp $
//
#ifndef BASICFITSUTILITY_H_INC
#define BASICFITSUTILITY_H_INC

//#include <fits/Fits.h>
#include <fits/FITS.h>


//
//  Forward declarations
//
class HDUObject;
class FITSBinaryColumn;


class BasicFITSUtility
{
public:
    //
    //  A default BasicFITSUtility (is the only way to go; we don't
    //  know what kind of utility this will be!)
    //
    BasicFITSUtility();

    //
    //  Create the FITS utility with a parent. Mostly used by
    //  HeaderDataUnits, they can supply the this-pointer as
    //  argument. The 'user' usually doesn't deal with the internals
    //  of the FITSUtilities, just wants to set/get their properties.
    //
    //  Note: the pointer is copied, but it is not deleted upon
    //  destruction of this object. The user is responsible that the
    //  object which this FITSUtility is connected to, lives longer
    //  than or equal to the time of the FITSUtility!
    //
    BasicFITSUtility( HDUObject* const ptr2parent );

    //
    //  BasicFITSUtilities can also have a BinaryColumn as
    //  parent (like matrix axes...)?
    //
    BasicFITSUtility( FITSBinaryColumn* const ptr2parent );


    // 
    // Because of gcc3.4 (and higher) ISO C++ standard compliance,
    // this copy c'tor must exists (bummer).
    // "temporaries, even those passed by const-reference *MUST*
    //  be copy constructable" <-- b*stards! ;)
    // And this construct we use in the 'addColumn()',
    // 'addAxis()' method...
    // (passing a temporary by const ref)
    //
    BasicFITSUtility( const BasicFITSUtility& other );

    //
    //  If this function is called, the FITSUtility in question is
    //  asked to translate itself into a linked list of FITSkeyword
    //  (the FitsKeywordList). This is usually done before the utility
    //  is to be flushed to file or terminal or whatever!
    //
    virtual casa::FitsKeywordList asFITSKeys( void ) const=0;

    //
    //  Retrieve the parent HDU (reference to). We don't give access
    //  to the pointer, otherwise people may delete it out from under
    //  us!!!! 
    //
    //  If the itsParentHDUptr datamember is null (not set) or it was
    //  not a HDUObject, an exception is thrown! Use the
    //  'hasParentHDU()' member function first.
    //
    HDUObject&        getParentHDU( void );
    FITSBinaryColumn& getParentBinaryColumn( void );

    //
    //  The const version
    //
    const HDUObject&        getParentHDU( void ) const;
    const FITSBinaryColumn& getParentBinaryColumn( void ) const;
    
    //
    //  Does this FITSUtility has the parent HeaderDataUnit set or
    //  not?
    //
	casa::Bool             hasParentHDU( void ) const;
	casa::Bool             hasParentBinaryColumn( void ) const;

    //
    //  Destruct the Utility!
    //
    virtual ~BasicFITSUtility();

private:

    //
    //  Keep track of what the private basicfitsutilty pointer
    //  actually pointed to...
    //
    enum _ptrType
    {
	none, hduObjectPtr, binaryColumnPtr
    };
    typedef _ptrType     ptrType;
    

    //
    //  Every FITSUtility can have a parent HeaderDataUnit. Note: the
    //  pointer is not deleted upon destruction of the object. The
    //  pointer is only kept so we can always get back at a
    //  FITSUtility's parent HDU.
    //
    //  Note: we make the pointer const, rather than the object
    //  pointed to. For us, it is more important to NOT mess with the
    //  pointer; it is less harmful if we call memberfunctions of the
    //  HDUObject pointed to....
    //
    //HDUObject* const    itsParentHDUptr;
    BasicFITSUtility* const    itsParentHDUptr;

    //
    //  Keep track of what the pointer actually pointed to...
    //
    BasicFITSUtility::ptrType  itsActualType;

    //
    //  The private methods:
    //

    //
    //  Undefined and inaccessible:
    //
    BasicFITSUtility& operator=( const BasicFITSUtility& );
};


#endif
