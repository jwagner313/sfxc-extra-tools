//
//  Implementation of the FITSAxis class
//
// 
//  Author:
//
//        Harro Verkouter    24-3-1998
//
//
//   $Id: FITSAxis.cc,v 1.8 2006/03/02 14:21:41 verkout Exp $  
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSArrayHDU.h>

#include <iomanip>
#include <sstream>

using namespace std;
using namespace casa;

ostream& operator<<( ostream& osref, const FITSAxis& axisref )
{
    osref << "Axis #" << setw(2) << axisref.axisnr() << " " 
          << axisref.ctype() << ": " 
          << axisref.naxis() << "\n"
          << "CRPIX=" << axisref.crpix() << "\n"
          << "CRVAL=" << axisref.crval() << "\n"
          << "CDELT=" << axisref.cdelt() << "\n"
          << "CROTA=" << axisref.crota() << endl;
    
    return osref;
}


//
//  First: the constructor with all the stuff in it!
//
//  Typically used by user's that want to add axes to a HDU.
//
FITSAxis::FITSAxis( const String& type, const String& comment, Int length, Double refpixel, 
                    Double refvalue, Double increment, Double rotangle ) :
    BasicFITSUtility(),
    itsAxistype( type ),
    itsComment( comment ),
    itsAxislength( length ),
    itsReferencepixel( refpixel ),
    itsReferencevalue( refvalue ),
    itsIncrement( increment ),
    itsRotationangle( rotangle ),
    itsAxisnumber( -1 )
{
    //
    //  Nothing left to do...
    //
}


FITSAxis::FITSAxis( Int axisnr, FitsKeywordList& kwsource, FITSArrayHDU* const parent ) :
    BasicFITSUtility( parent ),
    itsAxistype( "" ),
    itsComment( "" ),
    itsAxislength( 1 ),
    itsReferencepixel( 1.0 ),
    itsReferencevalue( 0.0 ),
    itsIncrement( 1.0 ),
    itsRotationangle( 0.0 ),
    itsAxisnumber( axisnr )
{
    FitsKeyword*   keywordptr( 0 );
    
    //
    //  Parse the keywordlist and attempt to extract all the info we
    //  need. Note: we assume the 'axisnr' that's being passed as an
    //  argument is zero based. In the FITSfile, it is one-based!
    //

    //
    //  Locate the NAXISn keyword!
    //
    if( (keywordptr=kwsource(FITS::NAXIS, itsAxisnumber+1))==0 )
    {
        //
        //  No NAXISn keyword!!!
        //
        ostringstream  error;
        
        error << "Rats! No NAXIS" << itsAxisnumber+1 << " keyword found. Aborting!";
        
        throw( AipsError(error.str()) );
    }

    //
    //  Fill in the length of the axis and the comment!
    //
    itsAxislength=keywordptr->asInt();
    itsComment=keywordptr->comm();

    //
    //  Delete the keyword!
    //
    kwsource.del();

    //
    //  Locate the axistype....
    //
    if( (keywordptr=kwsource(FITS::CTYPE, itsAxisnumber+1))!=0 )
    {
        itsAxistype=keywordptr->asString();

        //
        //  Delete!
        //
        kwsource.del();
    }

    
    //
    //  The axis-increment
    //
    if( (keywordptr=kwsource(FITS::CDELT, itsAxisnumber+1))!=0 )
    {
        itsIncrement=keywordptr->asDouble();
        
        kwsource.del();
    }

    //
    //  The reference pixel!
    //
    if( (keywordptr=kwsource(FITS::CRPIX, itsAxisnumber+1))!=0 )
    {
        itsReferencepixel=keywordptr->asDouble();
        
        kwsource.del();
    }
    
    //
    // The refernce value
    //
    if( (keywordptr=kwsource(FITS::CRVAL, itsAxisnumber+1))!=0 )
    {
        itsReferencevalue=keywordptr->asDouble();
        
        kwsource.del();
    }

    //
    // Rotation angle
    //
    if( (keywordptr=kwsource(FITS::CROTA, itsAxisnumber+1))!=0 )
    {
        itsRotationangle=keywordptr->asDouble();
        
        kwsource.del();
    }

    //
    //  e-thi.. e-thi.. e-that's all folks!
    //
}

//
//
//  The "copy"-c'tor
//
//
FITSAxis::FITSAxis( Int axisnr, const FITSAxis& other, FITSArrayHDU* ptr2parent ) :
    BasicFITSUtility( ptr2parent ),
    itsAxistype( other.itsAxistype ),
    itsComment( other.itsComment ),
    itsAxislength( other.itsAxislength ),
    itsReferencepixel( other.itsReferencepixel ),
    itsReferencevalue( other.itsReferencevalue ),
    itsIncrement( other.itsIncrement ),
    itsRotationangle( other.itsRotationangle ),
    itsAxisnumber( axisnr )    
{
    //
    //  Nothing to do, all's done that needs to be done
    //
}

//
// The *real* copy c'tor...
//
FITSAxis::FITSAxis( const FITSAxis& other ):
    BasicFITSUtility( (HDUObject*)0 ),
    itsAxistype( other.itsAxistype ),
    itsComment( other.itsComment ),
    itsAxislength( other.itsAxislength ),
    itsReferencepixel( other.itsReferencepixel ),
    itsReferencevalue( other.itsReferencevalue ),
    itsIncrement( other.itsIncrement ),
    itsRotationangle( other.itsRotationangle ),
    itsAxisnumber( other.itsAxisnumber )    
{
    //
    //  Nothing to do, all's done that needs to be done
    //
}


//
//  Translate this axis into a FitsKeywordList!
//
FitsKeywordList FITSAxis::asFITSKeys( void ) const
{
    FitsKeywordList   retval;
    
    //
    //  Ok. here we go....
    //
    //retval.mk( itsAxisnumber+1, FITS::NAXIS, itsAxislength, itsComment );

    //
    //  Only if the axis has a length other than 0 (which means a real
    //  degenerate), we need to add the description to the list of
    //  keywords
    //
    if( itsAxislength!=0 )
    {
        retval.mk( itsAxisnumber+1, FITS::CTYPE, itsAxistype.c_str() );
        retval.mk( itsAxisnumber+1, FITS::CDELT, itsIncrement );
        retval.mk( itsAxisnumber+1, FITS::CRPIX, itsReferencepixel );
        retval.mk( itsAxisnumber+1, FITS::CRVAL, itsReferencevalue );
        retval.mk( itsAxisnumber+1, FITS::CROTA, itsRotationangle );
    }
    return retval;
}

Int FITSAxis::getAxisLength( void ) const
{
    return itsAxislength;
}

void FITSAxis::setAxisLength( Int newlength )
{
    itsAxislength=newlength;
}

//
//  Return internal values!
//
Int FITSAxis::axisnr( void ) const
{
    return itsAxisnumber;
}


Int FITSAxis::naxis( void ) const
{
    return itsAxislength;
}

Double FITSAxis::crpix( void ) const
{
    return itsReferencepixel;
}

Double FITSAxis::crval( void ) const
{
    return itsReferencevalue;
}

Double FITSAxis::cdelt( void ) const
{
    return itsIncrement;
}

Double FITSAxis::crota( void ) const
{
    return itsRotationangle;
}

const String& FITSAxis::ctype( void ) const
{
    return itsAxistype;
}


//
//  Destruct this axis!
//
FITSAxis::~FITSAxis()
{
}

