//
//  Derived class FITSMatrixAxis -> describes an axis in a matrix in a
//  row in binary table... (doesn't that tell you what you always
//  wanted to know but where afraid to ask?)
//
//  Author:  Harro Verkouter,  13-09-1999
//
//  $Id: FITSMatrixAxis.h,v 1.4 2007/01/17 08:22:01 jive_cc Exp $
//
//  $Log: FITSMatrixAxis.h,v $
//  Revision 1.4  2007/01/17 08:22:01  jive_cc
//  HV: Casa changed Fits.h into FITS.h
//
//  Revision 1.3  2006/01/13 11:35:44  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.2  2001/05/30 11:50:37  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:48  verkout
//  HV: Imported aips++ implement-stuff
//
//
#ifndef FITSMATRIXAXIS_H
#define FITSMATRIXAXIS_H


#include <jive/ms2uvfitsimpl/FITSUtils/BasicFITSUtility.h>
#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <fits/FITS.h>

#include <iostream>


//
//  Forward declaration!
//
class FITSMatrixColumn;


class FITSMatrixAxis :
    public BasicFITSUtility
{
public:
    //
    //  Create a fully defined matrix axis...
    //
    FITSMatrixAxis( const casa::String& type, const casa::String& comment="", casa::Int length=1,
		    casa::Double refpixel=1.0, casa::Double refvalue=0.0, casa::Double increment=1.0,
		    casa::Double rotangle=0.0 );
    

    FITSMatrixAxis( casa::Int axisnr, casa::FitsKeywordList& kwsource, FITSMatrixColumn* const parent );

    //
    //  This constructor is a special one: it duplicates the contents
    //  of 'other' into the new object, but sets the axisnumber to
    //  'axisnumber'. This constructor is used by the MatrixColumn
    //  objects when axes are being added to them. The MatrixColumn
    //  object knows what the current axisnumber is. The user
    //  doesn't. So the user creates an axis (from ctor 1) and tells
    //  the MatrixColumn to add this one. The MatrixColumn calls this
    //  c'tor because it knows all the details.
    //
    //  Note: one of the parameters is the MatrixColumn class. We can
    //  make that assumption since MatrixColumns are the only columns's who
    //  support axis.
    //
    FITSMatrixAxis( casa::Int axisnr, const FITSMatrixAxis& other, FITSMatrixColumn* parent );

    //
    //  Translate the contents of this axis to FitsKeywords!
    //
    virtual casa::FitsKeywordList asFITSKeys( void ) const;

    //
    //  Return the length of this axis
    //
    casa::Int                     getAxisLength( void ) const;
    void                    setAxisLength( casa::Int newlength );

    //
    //  Return the axis-parameters, naming convention reselmbles FITS
    //  nomenclature.
    //
    casa::Int                     axisnr( void ) const;
    casa::Int                     naxis( void ) const;
    casa::Double                  crpix( void ) const;
    casa::Double                  crval( void ) const;
    casa::Double                  cdelt( void ) const;
    casa::Double                  crota( void ) const;
    const casa::String&           ctype( void ) const;

    //
    //  De-allocate resources (if any)
    //
    virtual ~FITSMatrixAxis();
    
    
private:
    //
    //  The private parts
    //

    //
    // The type of this axis
    //
    casa::String    itsAxistype;
    
    //
    //  Comment associated with this axis...
    //
    casa::String    itsComment;

    //
    //  The length of this axis
    //
    casa::Int       itsAxislength;

    //
    //  The reference pixel of this axis
    //
    casa::Double    itsReferencepixel;
    
    //
    //  The reference value for this axis
    //
    casa::Double    itsReferencevalue;
    
    //
    //  Axis increment (per pixel)
    //
    casa::Double    itsIncrement;
    
    //
    //  The rotation  angle
    //
    casa::Double    itsRotationangle;

    //
    //  The number of this axis in the current HDU
    //
    casa::Int       itsAxisnumber;

    //
    //  These are undefined hence we make'm inaccessible
    //
    FITSMatrixAxis();
    FITSMatrixAxis( const FITSMatrixAxis& );
    FITSMatrixAxis& operator=( const FITSMatrixAxis& );
};


//
//  global output operator
//
std::ostream& operator<<( std::ostream& os, const FITSMatrixAxis& axisref );

#endif
