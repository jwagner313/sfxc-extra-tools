//
//  The actual code for the free functions
//
//  Author:
//
//           Harro Verkouter     20-04-1998
//
//
//   $Id: freefunctions.cc,v 1.8 2007/05/25 10:22:50 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/freefunctions.h>
#include <fits/FITS.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Utilities/Regex.h>
#include <jive/Utilities/stringutil.h>

using namespace casa;
using std::istream;

Bool dataTypesMatch( DataType destinationtype, DataType sourcetype )
{
    Bool retval( False );

    switch( destinationtype )
    {
        //
        //  Handle the one-byte types first
        //
        case TpUChar:
            
            switch( sourcetype )
            {
                case TpChar:
                case TpUChar:
                case TpArrayChar:
                case TpArrayUChar:
                    retval=True;
                    
                default:
                    retval=False;
            }
            break;

        //
        //  Shorts are two bytes....
        //
        case TpShort:
            switch( sourcetype )
            {
                case TpChar:
                case TpUChar:
                case TpShort:
                case TpArrayChar:
                case TpArrayUChar:
                case TpArrayShort:
                    retval=True;
                    
                default:
                    retval=False;
            }
            break;
            
        //
        // The int type
        //
        case TpInt:
            switch( sourcetype )
            {
                case TpChar:
                case TpUChar:
                case TpShort:
                case TpInt:
                case TpArrayChar:
                case TpArrayUChar:
                case TpArrayShort:
                case TpArrayInt:
                    retval=True;
                    
                default:
                    retval=False;
            }
            break;

        //
        //  The float-case
        //
        case TpFloat:
            switch( sourcetype )
            {
                case TpFloat:
                case TpComplex:
                case TpArrayFloat:
                case TpArrayComplex:
                    retval=True;
                    
                default:
                    retval=False;
            }
            break;
        //
        // The double case
        //
        case TpDouble:
            switch( sourcetype )
            {
                case TpFloat:
                case TpDouble:
                case TpComplex:
                case TpDComplex:
                case TpArrayFloat:
                case TpArrayDouble:
                case TpArrayComplex:
                case TpArrayDComplex:
                    retval=True;
                    
                default:
                    retval=False;
            }
            break;

        default:
            //
            //  Non-basic fitstype!
            //
            retval=False;
            break;
    }
    return retval;
}


//
//  Is it a basic fits type?
//
Bool isBasicFitsType( DataType sometype )
{
    //
    //  Test!
    //
    switch( sometype )
    {
        case TpUChar:
        case TpShort:
        case TpInt:
        case TpFloat:
        case TpDouble:
            //
            //  No problem!
            //
            return True;
            
        default:
            //
            //  All other types cannot be directly represnted oin FITS
            //  (i.e. there is no valid BITPIX value associated with
            //  them)
            //
            return False;
    }
    return False;
}


//
//  Transform the datatype into an arraytype (if possible)
//
DataType MakeArrayType( DataType sometype )
{
    switch( sometype )
    {
        case TpChar:
            return TpArrayChar;
            
        case TpUChar:
            return TpArrayUChar;
            
        case TpShort:
            return TpArrayShort;
            
        case TpUShort:
            return TpArrayUShort;

        case TpInt:
            return TpArrayInt;
            
        case TpUInt:
            return TpArrayUInt;
            
        case TpFloat:
            return TpArrayFloat;
            
        case TpDouble:
            return TpArrayDouble;
            
        case TpComplex:
            return TpArrayComplex;
            
        case TpDComplex:
            return TpArrayDComplex;
            
        default:
            return sometype;
    }
    return sometype;
}

//
//  Convert bitpixvalue to datatype (and reverse)
//            
DataType BitpixToDataType( Int bitpixvalue )
{
    DataType   retval( TpOther );
    
    switch( bitpixvalue )
    {
        case 8:
            retval= TpUChar;
            break;
            
        case 16:
            retval=TpShort;
            break;
            
        case 32:
            retval=TpInt;
            break;
            
        case -32:
            retval=TpFloat;
            break;
            
        case -64:
            retval=TpDouble;
            break;
            
        default:
            break;
    }
    return retval;
}

Int  DataTypeToBitpix( DataType sometype )
{
    Int   retval( 0 );
    
    switch( sometype )
    {
        case TpUChar:
            retval=8;
            break;
            
        case TpShort:
            retval=16;
            break;
            
        case TpInt:
            retval=32;
            break;
            
        case TpFloat:
            retval=-32;
            break;
            
        case TpDouble:
            retval=-64;
            break;
            
        default:
            break;
    }
    return retval;
}


//
//  Test for equality of two keywords, based on name (and index, if applicable)
//
Bool operator==( const FitsKeyword& lval, const FitsKeyword& rval )
{
    Bool  retval( False );
    Bool  lvalisindexed( lval.isindexed() );


    if( lvalisindexed!=rval.isindexed() )
    {
        //
        //  One is indexed and the other is not -> they can never be
        //  the same!
        //
        return retval;
    }
    
    //
    //  Introduce a help-var
    //
    retval=( String(lval.name())==String(rval.name()) );

    if( lvalisindexed )
    {
        //
        //  Both names and indices must match!
        //
        retval= ( ((lval.index()==rval.index()) && retval) );
    }
    return retval;
}

//  Parse the stream and get the IPosition out
istream& operator>>( istream& is, IPosition& rval ) {
    char        separators[]={"\n"};
    string      iposstr;
    const Regex rx_iposition( "\\([0-9]+(,[0-9]+)*\\)" );
    
    //  First we clear the return value, in case an error occurs
    rval.resize( 0 );
    
    //  Until either an error occurs or we find a valid IPosition of
    //  the form:
    //  (<num>[,<num>,...])

    //  Well, let's try to read characters from the stream until we
    //  find a separator or EOF
    while( 1 ) {
        char    c;

        // Check if we reached end of stream
        if( is.eof() )
            break;
        
        // Ok. get the character!
        if( (c=is.get())==EOF )
            break;
        
        // is it one of the separators?
        if( ::strchr(separators,c) )
            break;

        // no: just copy it into our buffer!
        iposstr.push_back(c);
    }
    
    //  Check if there's anything in the buffer (it must be >1, since
    //  there's always the '\0' character!
    if( String(iposstr).matches(rx_iposition) ) {
        //  Parse the string, but first strip the leading '('
        //  and the terminating ')'
        //  Note: we can do this since we know that iposstr
        //  matches the Regex, which enforces that the string
        //  starts with '(' and termintes with ')'
        int                            indcount;
        string                         stripped( iposstr.begin()+1, iposstr.end()-1 );
        vector<string>                 indices;
        vector<string>::const_iterator curind;

        // split whatever remained after stripping
        // into parts, separating at ','
        indices = ::split( stripped, ',' );
           
        // resize the return value
        rval.resize( indices.size() );

        // and fill the return value
        for( curind=indices.begin(), indcount=0;
             curind!=indices.end();
             curind++, indcount++ ) {
            // let ::strtol do the dirty work :)
            //  (so we can give indices in HEX or OCTAL too :))
            rval( indcount ) = ::strtol(curind->c_str(), 0, 0);
        }
    }
    return is;
}
