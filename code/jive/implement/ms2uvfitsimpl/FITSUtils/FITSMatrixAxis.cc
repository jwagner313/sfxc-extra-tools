//
//  Implementation of the FITSMatrixAxis class...
// 
//  Author: Harro Verkouter,  13-09-1999
//
//  $Id: FITSMatrixAxis.cc,v 1.8 2006/02/10 08:53:46 verkout Exp $
//
//  $Log: FITSMatrixAxis.cc,v $
//  Revision 1.8  2006/02/10 08:53:46  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles
//
//  Revision 1.7  2006/01/13 11:35:44  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.6  2004/08/25 06:02:21  verkout
//  HV: Fiddled with template shit. All templates now done automatically.
//      As a result, the source-code for the templates has to be visible
//      compiletime.
//
//  Revision 1.5  2004/01/05 15:24:23  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.4  2003/09/12 07:31:47  verkout
//  HV: Code had to be made gcc3.* compliant. Some changes in AIPS++ had to be incorparated as well.
//
//  Revision 1.3  2003/02/14 15:45:29  verkout
//  HV: * trial/FITS/FITSUtil was removed.
//      * std::string does not have automatic conversion to
//        (const char*). Fits-stuff does need (const char*)
//        so had to add the odd .c_str() here and there.
//
//  Revision 1.2  2001/05/30 11:50:35  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:48  verkout
//  HV: Imported aips++ implement-stuff
//
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSMatrixAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSMatrixColumn.h>

#include <jive/Utilities/jexcept.h>

#include <iomanip>
#include <sstream>

using namespace std;
using namespace casa;

ostream& operator<<( ostream& osref, const FITSMatrixAxis& axisref )
{
    osref << "MatrixAxis #" << setw(2) << axisref.axisnr() << " " 
          << axisref.ctype() << ": " 
          << axisref.naxis() << "\n"
          << "CRPIX=" << axisref.crpix() << "\n"
          << "CRVAL=" << axisref.crval() << "\n"
          << "CDELT=" << axisref.cdelt() << "\n"
          << "CROTA=" << axisref.crota() << endl;
    
    return osref;
}


//
//  First: the constructor with all the stuff in it!
//
//  Typically used by user's that want to add axes to a HDU.
//
FITSMatrixAxis::FITSMatrixAxis( const String& type, const String& comment, Int length,
				Double refpixel, Double refvalue, Double increment,
				Double rotangle ) :
    BasicFITSUtility(),
    itsAxistype( type ),
    itsComment( comment ),
    itsAxislength( length ),
    itsReferencepixel( refpixel ),
    itsReferencevalue( refvalue ),
    itsIncrement( increment ),
    itsRotationangle( rotangle ),
    itsAxisnumber( -1 )
{
    //
    //  Nothing left to do...
    //
}


FITSMatrixAxis::FITSMatrixAxis( Int axisnr, FitsKeywordList& kwsource,
				FITSMatrixColumn* const parent ) :
    BasicFITSUtility( (FITSBinaryColumn* const)parent ),
    itsAxistype( "" ),
    itsComment( "" ),
    itsAxislength( 1 ),
    itsReferencepixel( 1.0 ),
    itsReferencevalue( 0.0 ),
    itsIncrement( 1.0 ),
    itsRotationangle( 0.0 ),
    itsAxisnumber( axisnr )
{
    FitsKeyword*   keywordptr( 0 );
    ostringstream  strm;
    
    //
    //  Parse the keywordlist and attempt to extract all the info we
    //  need. Note: we assume the 'axisnr' that's being passed as an
    //  argument is zero based. In the FITSfile, it is one-based!
    //


    //
    //  Locate the NAXISn keyword!
    //
    strm << "MAXIS" << itsAxisnumber+1;
    
    if( (keywordptr=kwsource(strm.str().c_str()))==0 )
    {
        THROW_JEXCEPT("Rats! No MAXIS" << itsAxisnumber+1
                << " keyword found. Aborting!");
    }

    //
    //  Fill in the length of the axis and the comment!
    //
    itsAxislength=keywordptr->asInt();
    itsComment=keywordptr->comm();

    //
    //  Delete the keyword!
    //
    kwsource.del();

    //
    //  Locate the axistype....
    //
    if( (keywordptr=kwsource(FITS::CTYPE, itsAxisnumber+1))!=0 )
    {
        itsAxistype=keywordptr->asString();

        //
        //  Delete!
        //
        kwsource.del();
    }

    
    //
    //  The axis-increment
    //
    if( (keywordptr=kwsource(FITS::CDELT, itsAxisnumber+1))!=0 )
    {
        itsIncrement=keywordptr->asDouble();
        
        kwsource.del();
    }

    //
    //  The reference pixel!
    //
    if( (keywordptr=kwsource(FITS::CRPIX, itsAxisnumber+1))!=0 )
    {
        itsReferencepixel=keywordptr->asDouble();
        
        kwsource.del();
    }
    
    //
    // The refernce value
    //
    if( (keywordptr=kwsource(FITS::CRVAL, itsAxisnumber+1))!=0 )
    {
        itsReferencevalue=keywordptr->asDouble();
        
        kwsource.del();
    }

    //
    // Rotation angle
    //
    if( (keywordptr=kwsource(FITS::CROTA, itsAxisnumber+1))!=0 )
    {
        itsRotationangle=keywordptr->asDouble();
        
        kwsource.del();
    }

    //
    //  e-thi.. e-thi.. e-that's all folks!
    //
}

//
//
//  The "copy"-c'tor
//
//
FITSMatrixAxis::FITSMatrixAxis( Int axisnr, const FITSMatrixAxis& other,
				FITSMatrixColumn* ptr2parent ) :
    BasicFITSUtility( (FITSBinaryColumn* const)ptr2parent ),
    itsAxistype( other.itsAxistype ),
    itsComment( other.itsComment ),
    itsAxislength( other.itsAxislength ),
    itsReferencepixel( other.itsReferencepixel ),
    itsReferencevalue( other.itsReferencevalue ),
    itsIncrement( other.itsIncrement ),
    itsRotationangle( other.itsRotationangle ),
    itsAxisnumber( axisnr )    
{
    //
    //  Nothing to do, all's done that needs to be done
    //
}


//
//  Translate this axis into a FitsKeywordList!
//
FitsKeywordList FITSMatrixAxis::asFITSKeys( void ) const
{
    FitsKeywordList   retval;
    
    //
    //  Ok. here we go....
    //

    //
    //  Only if the axis has a length other than 0 (which means a real
    //  degenerate), we need to add the description to the list of
    //  keywords
    //
    if( itsAxislength!=0 )
    {
        retval.mk( itsAxisnumber+1, FITS::CTYPE, itsAxistype.c_str() );
        retval.mk( itsAxisnumber+1, FITS::CDELT, itsIncrement );
        retval.mk( itsAxisnumber+1, FITS::CRPIX, itsReferencepixel );
        retval.mk( itsAxisnumber+1, FITS::CRVAL, itsReferencevalue );
        retval.mk( itsAxisnumber+1, FITS::CROTA, itsRotationangle );
    }
    return retval;
}

Int FITSMatrixAxis::getAxisLength( void ) const
{
    return itsAxislength;
}

void FITSMatrixAxis::setAxisLength( Int newlength )
{
    itsAxislength=newlength;
}

//
//  Return internal values!
//
Int FITSMatrixAxis::axisnr( void ) const
{
    return itsAxisnumber;
}


Int FITSMatrixAxis::naxis( void ) const
{
    return itsAxislength;
}

Double FITSMatrixAxis::crpix( void ) const
{
    return itsReferencepixel;
}

Double FITSMatrixAxis::crval( void ) const
{
    return itsReferencevalue;
}

Double FITSMatrixAxis::cdelt( void ) const
{
    return itsIncrement;
}

Double FITSMatrixAxis::crota( void ) const
{
    return itsRotationangle;
}

const String& FITSMatrixAxis::ctype( void ) const
{
    return itsAxistype;
}


//
//  Destruct this axis!
//
FITSMatrixAxis::~FITSMatrixAxis()
{
}

