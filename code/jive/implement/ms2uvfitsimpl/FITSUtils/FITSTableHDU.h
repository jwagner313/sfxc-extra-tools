//
// Class:
//
//       FITSTableHDU
//
// Purpose:
//
//       Another level of abstraction. All kinds of fitstable should
//       derive from this class. This class provides for all of the
//       functions that can be executed upon a table.
//
//
//
//
//
//
#ifndef FITSTABLEHDU_H_INC
#define FITSTABLEHDU_H_INC

#include <jive/ms2uvfitsimpl/FITSUtils/HDUObject.h>
#include <casa/Arrays/Vector.h>
#include <casa/Containers/RecordDesc.h>
#include <casa/Containers/RecordInterface.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAxis.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAsciiColumn.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>

#include <casa/BasicSL/String.h>


class FITSTableHDU :
    public HDUObject
{
    
    //
    //  Some of the FITSColumn functions are friend of ours....
    //
    friend casa::Bool FITSBinaryColumn::get( casa::uInt rownr, casa::RecordInterface& riref, 
                                        const casa::RecordFieldId& fieldidx ) const;
    friend casa::Bool FITSBinaryColumn::put( casa::uInt rownr, const casa::RecordInterface& riref, 
                                       const casa::RecordFieldId& fieldidx );

    friend casa::Bool FITSAsciiColumn::get( casa::uInt rownr, casa::RecordInterface& riref, 
                                        const casa::RecordFieldId& fieldidx ) const;
    friend casa::Bool FITSAsciiColumn::put( casa::uInt rownr, const casa::RecordInterface& riref, 
                                       const casa::RecordFieldId& fieldidx );


public:
    //
    //  Standard implementation is to return 0. HeaderDataUnits
    //  representing a table of some sort may want to implement this
    //  function. The object is supposed to return the current width
    //  of a row in the table (in bytes). Finds it's use in:
    //
    //  a) when a column is added, the column asks it's parent what
    //  the current rowwidth is, so the column object can determine
    //  it's own offset within the current row, and
    //
    //  b) when a row needs to be read/written it can be used to
    //  determine the amount of bytes to allocate to hold any number
    //  of rows.
    //
    casa::uLong                   getCurrentRowWidthInBytes( void ) const;

    //
    //  What's the current size of the table?
    //
    casa::uLong                   getCurrentDataVolumeInBytes( void ) const;

    //
    //  How many columns are present in this Table?
    //
    casa::uInt                    numberColumns( void ) const;

    //
    //  Rows:
    //

    //
    //  Use this functon to alter the number of rows in the table. The
    //  return value will tell you if the 'resize' was succesfull or
    //  not.
    //
    casa::Bool                    setNumberRows( casa::uInt nrow );
    casa::uInt                    getNumberRows( void ) const;

    //
    //  Check if columns of this type exist. Return the number of
    //  columns whose type is equal to the parameter 'type'
    //
    casa::uInt                    columnExists( const casa::String& type ) const;


    //
    //  Get/put a row in the table. The user's record will be
    //  restructured as needed to describe the row structure. In cae
    //  of a put, the provided record will be checked if it is
    //  compatible with the rowdescription of the current table.
    //
    virtual casa::Bool            getrow( casa::uInt rownr, casa::RecordInterface& riref ) const;
    
    virtual casa::Bool            putrow( casa::uInt rownr, const casa::RecordInterface& riref );

    //
    //  Return the description of a row in the table
    //
    casa::RecordDesc              getRowDescription( void ) const;
    
    //
    //  Derived classes should implement this! (either 'BINTABLE', or
    //  'TABLE')
    //
    virtual casa::String          xtension( void ) const = 0;
    
    //
    //  Read only access to table properties, use FITS nomenclature.
    //
    casa::String                  extname( void ) const;
    casa::Int                     extver( void ) const;
    
    //
    //  Destruct the object
    //
    virtual ~FITSTableHDU();


protected:
    //
    // Create a table extension
    //
    FITSTableHDU( const casa::String& name, casa::uInt versionnr );
    
    //
    //  Create the table HDU from a keywordlist+stream -> implied use:
    //  the HDU comes from an existing fitsfile!
    //
    FITSTableHDU( casa::FitsKeywordList& kwlist, casa::ByteIO* ptr2stream );
    
    //
    //  Tables do have the possibility of adding columns. FITSTable
    //  objects should implement this function, in order for them to
    //  get the pointer to the parent right!
    //
    //  IMPORTANT NOTE:
    //
    //  The column object that is passed as argument is COPIED before
    //  it is added internally. So any changes one performs on the
    //  local variable are NOT reflected in the internal column
    //  object. The 'getColumn()' methods should be used to get a
    //  reference to the actual column object. Binding information (if
    //  the column was bound to something) is copied. So if you
    //  already did that, there's no need to redo that after the
    //  column object is added to the HDU.
    //
    //  The return value is the actual column number that was assigned
    //  to this column. If less than zero: an error occurred and the
    //  column could not be added!
    //
    casa::Int                     addColumn( FITSColumn* column );

    //
    //  Retrieve the 'occurrence'th occurrence of the column with
    //  itsType='type' (columns with same name may exist multiple
    //  times). It is zero-based, so a value of 0 would mean the first
    //  occurrence of the column, a value of 1 would mean the second
    //  occurrence.
    //
    //  Note: if the column is not found, a null-pointer is returned.
    //
    FITSColumn*             getColumn( const casa::String& type, casa::Int occurrence=0 ) const;
    
    //
    //  Access column by number. Bounds-checkin is performed. If the
    //  columnnumber is unadressable, an AipsError-exception is
    //  thrown.
    //
    FITSColumn*             getColumn( casa::Int columnnr ) const;

    //
    //  Translate separate classes of FITSkeywords into
    //  fitskeywordlists
    //
    virtual casa::FitsKeywordList getHeaderKeywords( void ) const;
    
    virtual casa::FitsKeywordList getDataDescriptionKeywords( void ) const;
    

private:
    //
    //  The private parts....
    //

    //
    //  A table has two axes, store them in a vector
    //
    casa::Vector<FITSAxis*>      itsAxisptrs;

    //
    //  A table consists of a vector of columns; we keep const
    //  pointers rather than pointers to const!
    //
    casa::Vector<FITSColumn*>    itsColumnptrs;
    
    //
    //  This RecordDec object holds the structure of the tablerow!
    //
    casa::RecordDesc             itsRowDescription;

    //
    //  The private methods
    //
    
    //
    //  The table name and versionnr
    //
    casa::String                  itsTableName;
    casa::uInt                    itsTableVersion;

    //
    //  prevent copying and assignment
    //
    FITSTableHDU();
    FITSTableHDU( const FITSTableHDU& );
    FITSTableHDU& operator=( const FITSTableHDU& );    
};


#endif
