//
//  Implementation of the FITSAsciiColumn class
//
//
//  Author:
// 
//        Harro Verkouter,  18-05-1998
//
//  $Id: FITSAsciiColumn.cc,v 1.10 2012/07/05 07:49:51 jive_cc Exp $
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAsciiColumn.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSAsciiTableHDU.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSTableHDU.h>
#include <fits/FITS/fits.h>
#include <jive/Utilities/jexcept.h>


using namespace casa;
using namespace std;

FITSAsciiColumn::FITSAsciiColumn( const String& type, const String& comment, 
                                  const String& form, const String& unit ) :
    FITSColumn( type, comment, form, unit ),
    itsTotalWidth( 0L ),
    itsFieldWidth( 0 ),
    itsPrecision( 0 ),
    itsExponentPrecision( 0 )
{
    //
    //  Do our own 'Check form and init!'
    //
    CheckFormatAndInit();
}

    
//
//  The creator from a FITSfile...
//
FITSAsciiColumn::FITSAsciiColumn( Int number, uLong offset, FitsKeywordList& kwsource, 
                                  FITSAsciiTableHDU* ptr2parent ) :
    FITSColumn( number, offset, kwsource, (FITSTableHDU*)ptr2parent ),
    itsTotalWidth( 0L ),
    itsFieldWidth( 0 ),
    itsPrecision( 0 ),
    itsExponentPrecision( 0 )
{
    FitsKeyword*    keywordptr;
    
    //
    //  If we find a TBCOL keyword, we overwrite the 'offset' member
    //  with this value!
    //
    if( (keywordptr=kwsource(FITS::TBCOL, number+1))!=0 )
    {
        this->setColumnOffset( (uLong)keywordptr->asInt() );
        kwsource.del();
    }

    CheckFormatAndInit();
}

FITSAsciiColumn::FITSAsciiColumn( Int number, uLong offset, 
                                  const FITSAsciiColumn& other, FITSAsciiTableHDU* ptr2parent ) :
    FITSColumn( number, offset, (FITSColumn&)other, (FITSTableHDU*)ptr2parent ),
    itsTotalWidth( other.itsTotalWidth ),
    itsFieldWidth( other.itsFieldWidth ),
    itsPrecision( other.itsPrecision ),
    itsExponentPrecision( other.itsExponentPrecision )
{
}

FitsKeywordList FITSAsciiColumn::asFITSKeys( void ) const
{
    Int              columnnr( this->columnnr() );
    FitsKeywordList  retval( FITSColumn::asFITSKeys() );

    //
    //  Give all the keywords for this column
    //
    retval.last();

    //
    //  Add the ascii-column specific keyword(s)
    //
    retval.mk( columnnr+1, FITS::TBCOL, (Int)this->columnoffset()  );
    return retval;
}


//
//  Put the value that is in field fieldidx of the record into the cell
//  'rownr' of this column
//
Bool FITSAsciiColumn::put( uInt rownr, const RecordInterface& recordref, const RecordFieldId& fieldidx )
{
    Bool         retval( False );

    //
    //  Check if we have a parent!
    //
    if( !this->hasParentHDU() )
    {
        return retval;
    }

    //
    //  Check if the type of the record passed matches that of the
    //  column!
    //
    

    //
    //  If our parent has no datasegment associated yet, he/she should
    //  make one!
    //
    FITSAsciiTableHDU*  ptr2parent( (FITSAsciiTableHDU*)(&(this->getParentHDU())) );
    

    if( !(ptr2parent->hasDataSegment()) )
    {
        //
        //  Hell! Parent HDU has no data yet!
        //
        ptr2parent->makeDataSegment();
        
        if( !ptr2parent->hasDataSegment() )
        {
            return retval;
        }
    }
    
    //
    //  Ok. The parent now has a datasegment available!
    //

    //
    //  Find out if the datatypes of the indicated recordfield and the
    //  current column datatype match!
    //
    DataType     fieldtype( recordref.dataType(fieldidx) );
    RecordDesc   fielddesc;
    RecordDesc   columndesc( this->getColumnDescription() );

    //
    //  Create a recorddesc for the recordfield
    //
    if( isArray(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype,
                            recordref.shape(fieldidx) );
    }
    else if( isScalar(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype );
    }
    else
    {
        //
        //  Field is a table or another ecord or something we don't
        //  want tp bother with!
        //
        return retval;
    }
    
    
    if( fielddesc!=columndesc )
    {
        cout << "Rats. No put on column!" << endl;
        return retval;
    }

    //
    //  Ok. The field the fieldidx points at is compatible with what
    //  this column contains. So let's go on!
    //


    //
    //  That's it boys and girls: the checking's over: now do the
    //  putting!
    //
    //
    //  Create a pointer to the actual data-location!
    //
    uLong         totaloffset( 0L );

    totaloffset=((uLong)rownr*(uLong)ptr2parent->getCurrentRowWidthInBytes())+((uLong)this->columnoffset());
    
    //
    //  And make the pointer!
    //
    void*   dataptr( (ptr2parent->getDataSegment())[totaloffset] );

    if( !dataptr )
    {
        return retval;
    }
    

    //
    //  From now on, we'll assume the default return value is True,
    //  only in exceptional cases, the value might evaluate to False.
    //
    retval=True;
    
    //
    //  Let's allocate a buffer, big enough to hold the cell's value!
    //
    char*     bufptr( new char[(itsFieldWidth+1)*2] );
    
    
    switch( this->getColumnDataType() )
    {
        case TpShort:
            sprintf( bufptr, "%*d", itsFieldWidth, recordref.asShort(fieldidx) );
            break;
            
        case TpString:
            sprintf( bufptr, "%-*s", itsFieldWidth, (recordref.asString(fieldidx)).chars() );
            break;
            
        case TpFloat:
            {
                Float   value( recordref.asFloat(fieldidx) );
                
                //
                //  Depending on whether or not we have an exponent, we do
                //  something!
                //
                if( itsExponentPrecision )
                {
                    sprintf( bufptr, "%*.*E", itsFieldWidth, itsPrecision, (double)value );
                    //
                    //  Change the E in a D
                    //
                    char*   eptr( strchr(bufptr,'E') );
                    
                    if( eptr )
                    {
                        *eptr='D';
                    }
                }
                else
                {
                    sprintf( bufptr, "%*.*f", itsFieldWidth, itsPrecision, value );
                }
            }
            break;
            
        case TpDouble:
            {
                Double   value( recordref.asDouble(fieldidx) );
                
                //
                //  Depending on whether or not we have an exponent, we do
                //  something!
                //
                if( itsExponentPrecision )
                {
                    sprintf( bufptr, "%*.*E", itsFieldWidth, itsPrecision, (double)value );
                }
                else
                {
                    sprintf( bufptr, "%*.*lf", itsFieldWidth, itsPrecision, value );
                }
            }

            break;
            
        default:
            {
                THROW_JEXCEPT("Unsupported column type `"
                        << this->getColumnDataType() 
                        << "' in 'put' on ASCII column!");
            }
            break;
    }
    
    //
    //  Ok. Copy the value into the table!
    //
    memcpy( dataptr, bufptr, itsFieldWidth );

    //
    //  We don't need the buffer no more!
    //
    delete [] bufptr;
    
    return retval;
}

Bool FITSAsciiColumn::get( uInt rownr, RecordInterface& recordref, const RecordFieldId& fieldidx ) const
{
    //
    //  Now we know that we have a parent!
    //
    Bool                 except( False );
    Bool                 retval( False );

    //
    //  Check if the indicated field indeed does exist
    //
    try
    {
        recordref.idToNumber(fieldidx);
    }
    catch( const std::exception& x )
    {
        cerr << x.what() << endl;
        except = True;
    }
    
    if( except )
    {
        cout << "indicated field does not exist!" << endl;
        return retval;
    }
    
    //
    //  Check if the column-object has a parent HDU
    //
    if( !this->hasParentHDU() )
    {
        return retval;
    }
    
    //
    //  Create a pointer to the parent object
    //
    FITSAsciiTableHDU*  ptr2parent( (FITSAsciiTableHDU*)(&(this->getParentHDU())) );
    

    if( !(ptr2parent->hasDataSegment()) )
    {
        //
        //  Hell! Parent HDU has no data yet!
        //
        return retval;
    }
    
    //
    //  Very well: parent has data associated, now let's get the
    //  requested info!
    //

    //
    //  Make a recorddesc from the field the index points at. Compare
    //  it to the desc that describes this column. Ifg match OK,
    //  otherwise, get out!
    //
    DataType     fieldtype( recordref.dataType(fieldidx) );
    RecordDesc   fielddesc;
    RecordDesc   columndesc( this->getColumnDescription() );

    //
    //  Create a recorddesc for the recordfield
    //
    if( isArray(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype,
                            recordref.shape(fieldidx) );
    }
    else if( isScalar(fieldtype) )
    {
        fielddesc.addField( recordref.name(fieldidx),
                            fieldtype );
    }
    else
    {
        //
        //  Field is a table or another ecord or something we don't
        //  want tp bother with!
        //
        return retval;
    }


    if( fielddesc!=columndesc )
    {
        cout << "Descriptions don't match. No get!" << endl;
        return retval;
    }
    

    //
    //  Now do get the datapointer: calculate the total offset of this
    //  datum into the block of fitsdata (i.e. the FITSDataSegment).
    //
    uLong         totaloffset( 0L );

    totaloffset=((uLong)rownr*(uLong)ptr2parent->getCurrentRowWidthInBytes())+(uLong)(this->columnoffset());
    
    //cout << "Getting value for row " << rownr << " (" << this->ttype() << ") col. offset=" 
    //     << this->columnoffset() << " total: " << totaloffset << endl;

    //
    //  And make the pointer!
    //
    const void*   dataptr( (ptr2parent->getDataSegment())[totaloffset] );

    if( !dataptr )
    {
        return retval;
    }
    
    //
    //  Now then, depending on what the column is, fill the record!
    //

    //
    //  From now on, the default return val;ue changes to True, except
    //  when the field is not handled; the return value will be false
    //  then.
    //
    retval=True;
    

    //cout << "AsciiColumn: about to get ..." << this->getColumnDataType() << " ..." << flush;
    switch( this->getColumnDataType() )
    {
        case TpShort:
            {
                Int   tmp;
                
                sscanf( (char*)dataptr, "%d", &tmp );
                
                recordref.define( fieldidx, Short(tmp) );
            }
            break;
            
        case TpString:
            {
                char* bufptr( new char[itsFieldWidth+1] );
                
                memcpy( bufptr, dataptr, itsFieldWidth );
                
                //
                //  Make sure string is null terminated
                //
                bufptr[itsFieldWidth]='\0';
                
                recordref.define( fieldidx, String(bufptr) );

                delete bufptr;
            }
            break;
            
        case TpFloat:
            {
                Float   value;
                
                sscanf( (char*)dataptr,"%f", &value );
                
                recordref.define( fieldidx, value );
            }
            break;
            
        case TpDouble:
            {
                Double   value;
                
                sscanf( (char*)dataptr,"%lf", &value );
                
                recordref.define( fieldidx, value );
            }

            break;
            
        default:
            {
                THROW_JEXCEPT("Unsupported column type `"
                        << this->getColumnDataType() 
                        << "' in 'get' on ASCII column!");
            }
            break;
    }
    
    return retval;
}



uLong FITSAsciiColumn::getWidthInBytes( void ) const {
    return itsTotalWidth;
}


FITSAsciiColumn::~FITSAsciiColumn()
{}

//  Check the string 'itsForm' and try to deduce what's in
//  there. According to that, fill in the affected private
//  datamembers: columnDataType and itsTotalWidth!
void FITSAsciiColumn::CheckFormatAndInit( void ) {
    typedef enum tag_format_t {
            invalid, intchar, floating, floatingexp 
    } format_t;
   
    bool        interpreted;
    Char        fitstype;
    String      theformat;
    format_t    format( invalid );
    DataType    columnDataType;
    RecordDesc  columnDescription;
    const Regex intorchar( "[IA][0-9]+[ \t]*" );
    const Regex floats( "[F][0-9]+.[0-9]+[ \t]*" );
    const Regex floatswithexp( "[ED][0-9]+.[0-9]+E[0-9]+[ \t]*" );
    
    //  Attempt to math the format to any of the allowed forms!
    theformat=this->tform();
    
    if( theformat.matches(intorchar) )
        format=intchar;
    else if( theformat.matches(floats) )
        format=floating;
    else if( theformat.matches(floatswithexp) )
        format=floatingexp;
    
    //  If the form does not conform to an allowed form, we throw an
    //  exception!
    if( format==invalid )
        THROW_JEXCEPT("FITSAsciiColumn: the form specifier `"
                << this->tform()
                << "' is invalid. No valid column could be created");
    
    //  Now break apart....
    interpreted = false;
    switch( format ) {
        case intchar:
            //  Format is either Iw or Aw
            interpreted = 
                ::sscanf(theformat.chars(), "%c%d", &fitstype, &itsFieldWidth) == 2;
            break;
            
        case floating:
            interpreted = 
                ::sscanf(theformat.chars(), "%c%d.%d", &fitstype, &itsFieldWidth,
                        &itsPrecision) == 3;
            break;
            
        case floatingexp:
            interpreted = ::sscanf(theformat.chars(), "%c%d.%dE%d", &fitstype,
                    &itsFieldWidth, &itsPrecision, &itsExponentPrecision) ==4;
            break;
            
        default:
            break;
    }
    if( !interpreted )
        THROW_JEXCEPT("FITSAsciiColumn: could not interpret the format `"
            << theformat << "'");
    
    //  Ok. Determine the type of this column.....
    switch( fitstype ) {
        case 'I':
            //  A 16 bit integer: TpShort!
            columnDataType=TpShort;
            itsTotalWidth=itsFieldWidth;
            break;
            
        case 'A':
            //  Array of characters: commonly known as String!
            columnDataType=TpString;
            itsTotalWidth=itsFieldWidth;
            break;
            
        case 'F':
            //  Single precision floating point
            columnDataType=TpFloat;
            itsTotalWidth=itsFieldWidth;
            break;

        case 'E':
            //  Single precision floating point exponent
            columnDataType=TpFloat;

            //  Total width is width of number (itsFieldWidth) +
            //  number of digits in exponent (itsExponentPrecision) +
            //  1 (the 'E' character!)
            itsTotalWidth=itsFieldWidth+itsExponentPrecision+1;
            break;
            
        case 'D':
            //  Double precision floating point with exponent
            columnDataType=TpDouble;
            
            //  Total width is width of number (itsFieldWidth) +
            //  number of digits in exponent (itsExponentPrecision) +
            //  1 (the 'E' character!)
            itsTotalWidth=itsFieldWidth+itsExponentPrecision+1;
            break;

        default:
            //  AAARRGGGHHH - ?
            //  We didn't recognize/support the datatype?!
            THROW_JEXCEPT("FITSAsciiColumn: Unsupported column datatype `"
                    << this->tform() << "'");
            break;
    }

    //  Ok. Everything is filled in by now: we have the datatype and
    //  the column width right? Only thing left is to make the column
    //  description!

    //  Lets construct a unique name for this column:
    columnDescription.addField( this->ttype(), columnDataType );

    // Set the correct values in the base class
    setDataType( columnDataType );
    setColumnDescription( columnDescription );
}

