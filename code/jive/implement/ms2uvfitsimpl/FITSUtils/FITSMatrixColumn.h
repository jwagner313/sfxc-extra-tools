//
//  Specialization of a binary column-> the matrix column. This (NOT
//  STANDARD) fits stuff is needed to make data available to classic
//  AIPS in the FITS-IDI format.
//
//  This format makes use of matrices in binary table columns -> THEY
//  COME WITH AN AXIS DESCRIPTION, as if it were a PrimaryArray, only
//  the keywords are somewhat malformed...
//
//  Author: Harro Verkouter, 13-09-1999
//
//
//  $Id: FITSMatrixColumn.h,v 1.6 2007/01/17 08:22:01 jive_cc Exp $
//
//  $Log: FITSMatrixColumn.h,v $
//  Revision 1.6  2007/01/17 08:22:01  jive_cc
//  HV: Casa changed Fits.h into FITS.h
//
//  Revision 1.5  2006/03/02 14:21:41  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.4  2006/01/13 11:35:44  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.3  2004/01/05 15:24:27  verkout
//  HV: Template trouble. Included template code to be able to to let the compiler automatically create template-instantations.
//
//  Revision 1.2  2001/05/30 11:50:41  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.1.1.1  2000/03/20 15:16:48  verkout
//  HV: Imported aips++ implement-stuff
//
//
#ifndef FITSMATRIXCOLUMN_H
#define FITSMATRIXCOLUMN_H


//
//  We're some sort of a binary columns... thus....
//
#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryColumn.h>
#include <casa/Arrays/IPosition.h>
#include <fits/FITS.h>
#include <vector>

//
//  Forward declaration
//
class FITSMatrixAxis;



class FITSMatrixColumn :
    public FITSBinaryColumn
{
public:
    //
    //  Create a matrix column
    //

    //
    //  One must at least specify the datatype of this column -> the
    //  shape will be determined from the axes....  and the type
    //  (=name of the column) and unit. Comment is optional
    //
    FITSMatrixColumn( const casa::String& type, casa::DataType tp,
                      const casa::String& unit="",
		              const casa::String& comment="" );

    
    //
    //  These c'tors should NOT be used by the public. We could have
    //  made them protected, but then we need to make the
    //
    FITSMatrixColumn( casa::Int number, casa::uLong offset,
                      casa::FitsKeywordList& kwsource,
		              FITSBinaryTableHDU* ptr2parent );
    
    FITSMatrixColumn( casa::Int number, casa::uLong offset,
                      const FITSMatrixColumn& other,
		              FITSBinaryTableHDU* ptr2parent );


    //
    //  Get RO-access to some of the properties
    //
    casa::uInt               nrAxes( void ) const;
    const FITSMatrixAxis*    getAxis( casa::uInt axnr ) const;

    //
    //  Give usr. possibility to add an axis (if we're not bound to a
    //  table, that is!)
    //
    casa::Bool               addAxis( const FITSMatrixAxis& axis );

    //
    //  Return the shape of the column as an IPosition object
    //
    casa::IPosition          shape( void ) const;

    //
    //  Translate ourselves into FITSKeywords
    //
    virtual casa::FitsKeywordList  asFITSKeys( void ) const;

    //
    //  De-allocate resources..
    //
    virtual ~FITSMatrixColumn();
    

private:
    //
    //  Our private parts
    //
    
    //
    //  What data type are we???
    //
    //  NOTE: we keep the scalar form -> the column description will
    //  make it into an array.
    //
    casa::DataType                    myDataType;

    //
    //  We keep a list of matrix axes, describing the data in this
    //  matrix...
    //
    typedef std::vector<FITSMatrixAxis*> axislist_t;
    axislist_t                        myAxes;
    
    
    //
    //  These are undefined and hence we make'm inaccessible
    //
    FITSMatrixColumn();
    FITSMatrixColumn( const FITSMatrixColumn& );
    const FITSMatrixColumn& operator=( const FITSMatrixColumn& );
};


#endif
