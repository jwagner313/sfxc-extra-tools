//
//  Class: 
//
//       FITSBinaryColumn
//
//  Purpose:
//
//       Object describes a column in a FITSBinaryTable Object. Does
//       correct formatting on get/put optins.
//
//
//
//  Author:
//
//       Harrro Verkoutrer,    18-05-1998
//
//
//  $Id: FITSBinaryColumn.h,v 1.6 2007/04/11 12:01:47 jive_cc Exp $
//
#ifndef FITSBINARYCOLUMN_H_INC
#define FITSBINARYCOLUMN_H_INC

#include <casa/aips.h>
#include <casa/Arrays/IPosition.h>
#include <fits/FITS.h>
#include <jive/ms2uvfitsimpl/FITSUtils/FITSColumn.h>
//#include <jive/ms2uvfitsimpl/FITSUtils/FITSBinaryTableHDU.h>

class FITSBinaryTableHDU;


class FITSBinaryColumn : 
    public FITSColumn
{
public:
    //
    // The public interface of the FITSBinaryColumn. For explanation
    // of the three different creators, see FITSColumn.h
    //
    FITSBinaryColumn( const casa::String& type,
		      const casa::String& comment,
		      const casa::String& form,
		      const casa::String& unit );
    
    //
    //  Construct a binary column from some aips++ internal
    //  types. NOTE: it is important that if the usr wants to store an
    //  Array in this column, be sure to have the form an
    //  TpArray<desired type>. The shape tells the system what the
    //  shape of the array is. If all's well that ends well, the shape
    //  should be reconstructed when reading the column back.
    //
    FITSBinaryColumn( const casa::String& type, casa::DataType form, 
                      const casa::IPosition& shape=casa::IPosition(1,1),
                      const casa::String& unit="",
		      const casa::String& comment="" );

    //
    //  These c'tors should NOT be used by the public. We could have
    //  made them protected, but then we need to make the
    //  FITSBinaryTable class (or member functions thereof) friends of
    //  this clas, which means that they have access to ALL members
    //  and methods of this class, which I REALLY DONT LIKE!!! Better
    //  to let them have controlled acces with an object the doesn't
    //  do anything rather than giving them access to our private
    //  parts where they can mess up things whilst we don't know
    //  anything about it!
    //
    FITSBinaryColumn( casa::Int number, casa::uLong offset,
		      casa::FitsKeywordList& kwsource,
		      FITSBinaryTableHDU* ptr2parent );
    
    FITSBinaryColumn( casa::Int number, casa::uLong offset,
					  const FITSBinaryColumn& other,
		              FITSBinaryTableHDU* ptr2parent );


    // 
    // Because of gcc3.4 (and higher) ISO C++ standard compliance,
    // this copy c'tor must exists (bummer).
    // "temporaries, even those passed by const-reference *MUST*
    //  be copy constructable" <-- b*stards! ;)
    // And this construct we use in the 'addColumn()' method...
    // (passing a temporary by const ref)
    //
    FITSBinaryColumn( const FITSBinaryColumn& other );
    
    //
    //  Translate this Column to Fitskeywords
    //
    virtual casa::FitsKeywordList asFITSKeys( void ) const;


    //
    //  Via recordfieldid! Default to the first field in the record
    //
    virtual casa::Bool   get( casa::uInt rownr,
						      casa::RecordInterface& recordref, 
                              const casa::RecordFieldId& fieldidx=0 ) const;
    virtual casa::Bool   put( casa::uInt rownr,
							  const casa::RecordInterface& recordref, 
                              const casa::RecordFieldId& fieldidx=0 );

    //
    //  Width of this column
    //
    virtual casa::uLong           getWidthInBytes( void ) const;


    //
    // Are we really a binary column or are we a Matrix column?
    //
    casa::Bool                    isMatrixColumn( void ) const;

    //
    //  Destruct the column object!
    //
    ~FITSBinaryColumn();

protected:
    //
    // Any protected functions go here.
    //

    //
    //  Yuck! Do I not like this.... derived classes (and friends for
    //  that matter) can use this method to tell this BinaryColumn
    //  Object that is really is a MatrixColumn... but.. since we must
    //  make decisions for keywords in another object (the
    //  BinaryTable) we must be able to find out if there are Matrix
    //  columns present and if so, we must even know which one it is!
    //
    //  Wait... you haven't heard the best part yet... it's not even
    //  part of the standard, these so-called matrix columns! They're
    //  an invention of the the Classic AIPS team... and since we
    //  *really* need classic AIPS we'll need to find a way to get our
    //  data through into classic AIPS...
    // 
    void        setMatrixFlag( void );

    

    //
    //  This function checks the 'form' string and from there tries
    //  to determine the datatype of the column and the total column
    //  width. If a nonsupported datatype or invalid form-description
    //  was entered, an exception will be thrown!
    //
    void           CheckFormatAndInit( void );

private:
    //
    //  The BinaryColumn's specific data
    //

    //
    //  We keep track of the width of this column ourselves
    //
    casa::uLong                 itsTotalWidth;

    //
    //  And of the number of elements in the column cell!
    //
    casa::Int                   itsNumberElements;

    //
    //  To be able to doscriminate between character Bools/Bitarrays
    //  we need this variable. It tells us whether the array of
    //  booleans is to be stored as characters or as a bitarray!
    //
    casa::Bool                  boolIsBitArray;

    casa::Bool                  myMatrixFlag;

    //
    //  BinaryColumn private methods
    //


    //
    //  No default binary columns possible, nor is
    //  duplication/assignment allowed!
    //
    FITSBinaryColumn( );
    FITSBinaryColumn& operator=( const FITSBinaryColumn& );
};



#endif
