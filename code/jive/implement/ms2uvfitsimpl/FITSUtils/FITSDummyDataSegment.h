//
//  Class:
//
//         FITSDummyDataSegment
//
//  Purpose:
//
//         The dummy datasegment can be used when a hdu describes
//         data, but the user has not put anything in it. The dummy
//         dasegment is initalized to the 'zero' of the correct
//         datatype of the HDU. The whole dataarea the HDU is
//         represented is filled, only in reality, just one block is
//         used, thereby causing minimal overhead/memory allocation!
//
//
//
//  Author:
//
//         Harro Verkouter,    08-06-1998
//
//
//        $Id: FITSDummyDataSegment.h,v 1.3 2006/01/13 11:35:43 verkout Exp $
//
#ifndef FITSDUMMYDATASEGMENT_H_INC
#define FITSDUMMYDATASEGMENT_H_INC

#include <jive/ms2uvfitsimpl/FITSUtils/FITSDataSegment.h>
#include <casa/aips.h>
#include <casa/Utilities/DataType.h>
#include <casa/IO/ByteIO.h>


//
//  Forward declarations
//




class FITSDummyDataSegment :
    public FITSDataSegment
{

public:
    //
    //  Create the object
    //

    //
    //  This will create a DummyDataSegment of 'size' bytes long; a memory
    //  object. Pass the basic fitstype (either: TpChar, TpShort,
    //  TpInt, TpFloat or TpDouble), this parameter is used to pad the
    //  block with the zero representation of the correct type.
    //  
    FITSDummyDataSegment( casa::uLong size, casa::DataType basicfitstype );

    //
    //  The size this segment represents
    //
    virtual casa::uLong       getSegmentSize( void ) const;

    //
    //  Allow the user access to the data......
    //
    //  NOTE: don't deleet this pointer our from under us!
    //
    
    //
    //  Return the pointer to the dummy datasegment
    //
    virtual void*        operator[]( casa::uLong offset );
    virtual const void*  operator[]( casa::uLong offset ) const;

    //
    // Where did we originate from?
    //
    virtual FITSDataSegment::source_t dataSegmentOrigin( void ) const;

    //
    //  Destruct it
    //
    virtual ~FITSDummyDataSegment();

protected:
    //
    //  These functions are the interesting ones!
    //
    virtual casa::uLong        nrFitsBlocks( void ) const;

private:
    //
    //  The dummy data segment's private parts
    //
    //
    //  The size of the segment in fitsblocks
    //
    casa::uLong          itsNumberOfBlocks;

    //
    //  The pointer to the block!
    //
    casa::uChar*         itsDataptr;
    
    //
    //  Private methods
    //

    //
    //  No defaults, copying or assignment!
    //
    FITSDummyDataSegment();
    FITSDummyDataSegment( const FITSDummyDataSegment& );
    FITSDummyDataSegment& operator=( const FITSDummyDataSegment& );
};


#endif
