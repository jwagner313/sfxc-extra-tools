//
//
//   Here follow defines for Albert's datatype code ......
//
//

#ifndef EVNFRAFLAGS_H
#define EVNFRAFLAGS_H


#define  ACFLAG       0x01
#define  CMPLXFLAG    0x02
#define  ONEBITFLAG   0x04

#define  SPECTRUMFLAG 0x80

#endif
