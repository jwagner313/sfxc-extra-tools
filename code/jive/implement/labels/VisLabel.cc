//
//  Implementation of the VisLabel-class
//
//
#include <jive/labels/VisLabel.h>
#include <jive/labels/CorrelationCode.h>
#include <jive/Utilities/streamutil.h>

#include <iomanip>

using namespace std;

ostream& operator<<( ostream& os, const VisLabel& vlref )
{
    os << j2ms::printeffer("%2d: SA=%2d SB=%3d BL=", vlref.jobnr,
                            vlref.subarraynr, vlref.subbandnr)
       << vlref.baseline;
    return os;
}

ostream& operator<<( ostream& os, const ExtVisLabel& visref )
{
    os << visref.correlations.size() << " correlation products on ";
    os << (const VisLabel&)visref << endl;
    os << "Correlations: " << as_corrcode(visref.correlations) << endl;
    return os;
}


