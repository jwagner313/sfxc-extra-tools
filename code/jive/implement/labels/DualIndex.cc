//
//  What's left from DualIndex.h that needs to be implemented (still)
//
//
#include <jive/labels/DualIndex.h>

using std::ostream;

ostream& operator<<( ostream& os, const DualIndex& diref )
{
    os << "[" << diref[0] << "," << diref[1] << "]";
    return os;
}
