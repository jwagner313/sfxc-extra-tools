// j2ms specific polarization enums
//
// Author: H Verkouter
// 17/08/2006
//
// Allows us to map to and from external polarization-enums, characters, codes
// but remain internally consistent because the input-stuff will map their native
// codes to these and the output may map these codes to what's expected in the
// output
#ifndef JIVE_DTDLUT_POLARIZATION_H
#define JIVE_DTDLUT_POLARIZATION_H

#include <map>
#include <set>
#include <string>
#include <iostream>
#include <exception>

// stick the enum in the j2ms namespace such that
// it will not clash with others
namespace j2ms {

    enum polarization {
        unknown = -1,
        x = 19,   // linear, X
        y = 57,   //   ..  , Y
        r = 386,  // circular, R
        l = 902   //   ..    , L
    };
    // implement a global 'operator<()' for polarizations 
    // such that it can be used in Sorted STL containers
    // (or as key in such a container)
    //
    // NOTE: Read below for the importance of this method!!!!
    //
    // VERY NICE TO KNOW AS WELL:
    // This operator may throw an exception, yes boys and
    // girls, it's true... strange but there's a reason for
    // that.
    // 
    // This enumerated type (and the CorrelationCode (see .h))
    // will be used (amongst others) as keys into ordered
    // STL containers like std::set and std::map.
    // 
    // These containers require the type to implement a
    // Strict Weak Ordering, which they use to determine the
    // place of an element in the container. So far no reason
    // for throwing exceptions around.
    //
    // However, in the docs [http://www.sgi.com/tech/stl/StrictWeakOrdering.html]
    // it sais that equivalence of two elements is implied
    // when, for two elements x, y and a given strict weak ordering, swo,
    // the following is true:
    //  
    //  (swo(x,y) == false) && (swo(y,x)==false)
    //
    // In other words: if neither y nor x precedes the other.
    // [swo(x,y)==true means that x precedes y].
    //
    // So much for theory. Fastforward to practice.
    // We have enums/objects which may have an 'undefined' value.
    // (eg j2ms::unknown for the polarization enum).
    // Now what should the strict-weak-ordering function return
    // if any of the comparands has this value?
    // eg:
    //  what is the outcome of the following expression?
    //     j2ms::l < j2ms::undefined  ?
    //  or
    //     j2ms::undefined < j2ms::y ?
    //
    // If we decide to let the swo (in this case 'operator<()') return
    // false if any of the comparands is 'j2ms::undefined' that would
    // basically mean that this would imply equivalence for j2ms::undefined
    // to ANY of the other values!
    //
    // eg:  let    x === j2ms::undefined , y === j2ms::x
    // then: (without throwing exceptions)
    //      x < y == false  AND  y < x == false
    // hence 'the system' (or STL for that matter) would
    // conclude:
    //               x == y
    // Which is obviously not true...
    //
    // So I decided to let the operator< throw an exception
    // if any of the comparands is NOT known so as to inform
    // you something fishy may be going on.
    //
    // Same line of reasoning holds for the operator< for
    // CorrelationCode's.
    //
    // Note: it may not be an acute problem, as I'm not quite
    // sure where in the code equivalence of elements is determined
    // or used but it is a clear and strict requirement on ANY 
    // type which (wants to) serve as a key in a sorted STL container.
    //
    // Another thing is that normally it would not make a lot of
    // sense in comparing polarizations/CorrelationCodes. Just
    // be aware that they can (safely) be used as Keys in
    // mappings and sets.
    bool operator<(polarization lhs, polarization rhs);

    // this type will build an ordered set of polarizations,
    // based on the previous implemented strict weak ordering.
    // This will lead to a (albeit implicit) defined polarization
    // mapping.. if >1 recorded polarizationg (eg) then R will
    // always precede L (or X will precede Y for that matter)
    //
    // NOTE: A LOT OF CODE DEPENDS ON THE FACT THAT THE 
    // polarizations_t TYPE IS A SORTED CONTAINER IN WHICH
    // R COMES BEFORE L (AND X BEFORE Y FOR THAT MATTER).
    // IF YOU CHANGE THIS EXPECT TO BREAK A LOT OF STUFF...
    typedef std::set<polarization> polarizations_t;

    // and show in HRF
    std::ostream& operator<<(std::ostream& os, const polarizations_t& p);

    enum sense {
        unknown_sense  = -1,
        linear         = 42,
        circular       = 966
    };
    std::ostream& operator<<( std::ostream& os, sense s );

    // return the sense of a polarization
    // may return unknown
    sense sense_of( polarization p );

    // sometimes, you must be able to map a logical polarization label
    // (eg '0' or '1' as found in Albert's DataTransferDescriptor)
    // to a physical polarization. 
    // I think we will restrict ourselves to values of '0' and '1'
    // other inputs will be punished by exception throwing. We REALLY do not
    // want to map/unmap other values than '0' and '1'
    struct log2polmap {
        public:
            // val_type is pair<int,polarization>
            //
            // Create these like:
            // make_pair(0,j2ms::x)
            // make_pair(1,j2ms::l)
            // etc
            typedef std::map<int,polarization>  maptype;
            typedef maptype::value_type         val_type;
            typedef maptype::key_type           key_type;
            typedef maptype::const_iterator     const_iterator;

            // This key represents the 'invalid' key value.
            // Actually, there are 2^(wordsize)-3 invalid
            // keys. Just chose this one to be the one ;)
            static const key_type               invalid_key = key_type( -1 );

            // empty map. not quite sure wether to allow that...
            // on the other hand... when trying to unmap '0' or '1'
            // you get 'j2ms::unknown' and trying to do something
            // with those quite regularly would meet with exception
            // throwing or core-dumping (abort()ing the process...)
            log2polmap();
            
            // just a single mapping
            // define v0.first to represent v0.second
            //
            // requirements:
            //   first == 0|1 anything else gets an exception thrown
            //   second may not be 'unknown', only non-unknown polarizations
            //   may be mapped
            // These are assertions.
            // If for some reason, insertion in the map fails,
            // an exception derived from std::exception will be thrown
            log2polmap( val_type v0 );

            // map two logical labels to physical pol
            // Same requirements as with single arg c'tor
            // Also it checks for duplicates... If you
            // attempt to insert the same entry twice
            // you'd better hide 'cuz you'll be gettin'
            // sum exceptions thrown at ya!
            // Duplicate at the moment (21/08/2006) means
            // 'duplicate key'. It is possible to
            // create the following map:
            // '0 -> j2ms::x, 1 -> j2ms::x'
            // but not
            // '0 -> j2ms::x, 0 -> j2ms::y'
            log2polmap( val_type v0, val_type v1 );

            // attempt to insert this entry.
            // Will throw up if this fails/stuff not coherent
            void              insert( val_type v );

            // and try to locate a specific label
            // Note: it will be asserted if the argument
            // == 0|1!
            // May return 'unknown' if requested label
            // not defined in this mapping
            polarization      find( maptype::key_type label ) const;

            // inverse mapping. I know. Can be non-unique. In theory.
            // For all practical purposes of THIS class, we may assume
            // the mapping of polarization -> index is 1:1 in *both*
            // directions. This method returns the key of the given
            // polarization (if it's in the map). Otherwise
            // log2polmap::invalid_key will be returned
            maptype::key_type key_of( polarization p ) const;

            // Get the numerical index of a key/polarization.
            // The entries in the map itself define a mapping of a logical value
            // to a physical polarization. 
            // Sometimes, we have to write them into an array and hence we
            // must be able to get the array-index/indices of requested properties
            // will return <0 if not found.
            // This method is necessitated for the following reason:
            // 
            // log2polmap  l2p( make_pair(1, j2ms::l) );
            //
            // This creates a log2polmap with one entry, which defines a mapping
            // of the logical label '1' to polarization j2ms::l.
            // Now if we want to write this as an array 0..n [eg in
            // a MS Polarization table...] 
            // we must re-label this to be array-index 0.
            // This in itself does not necessitate these methods.
            // We must also write the polarization products as pairs of array-indices,
            // so, given an array of polarization products [eg CorrelatorSetup::getCorrelations()]
            // (which is an array of combinations of physical polarizations), we must
            // translate that to an array of array-indices:
            //
            // MSFeed::POLARIZATION_TYPE = vector<string>[0..nreceptor]
            //                             listing the physical polarizations for each receptor
            // MSPolarization::CORR_PRODUCT = matrix<int,int>[2, 0..npolproducs]
            //                             listing the polarization products as pairs of
            //                             indices into the MSFeed::POLARIZATION_TYPE vector
            //                             such they tell the physical polarization product

            // These methods return -1 if the given key/polarization does not occur
            // in the map
            int               index_of( polarization p ) const;
            int               index_of( key_type k ) const;

            // How many polarizationmappings are defined?
            // Typically returns 0, 1 or 2 ...
            size_t          size( void ) const;

            // allow outsiders to iterate over the mapping
            inline const_iterator begin( void ) const {
                return __m.begin();
            }
            inline const_iterator end( void ) const {
                return __m.end();
            }

            bool operator==( const log2polmap& other ) const;
            bool operator!=( const log2polmap& other ) const;

        private:
            typedef std::pair<maptype::iterator,bool> mapinsres;

            maptype    __m;

            struct failed_insert:
                public std::exception
            {
                failed_insert( val_type v, const std::string& action );
                virtual const char* what( void ) const throw();

                virtual ~failed_insert() throw () {}

                std::string  msg;
            };
    };

    // define output function for single polarization
    std::ostream& operator<<( std::ostream& os, polarization p );
    
    // define output function for a polarizationmap
    std::ostream& operator<<( std::ostream& os, const log2polmap& l2p );

    // warp a character into polcode
    // returns polcode 'unknown' if char not found
    // note: does case-insensitive search.
    // no surprises here by the way.
    polarization   char2polcode( char c );
}

// some defines which can be used to assert
// valid userinputs
#define ACCEPTABLE_INDEX(a) \
    (a==0 || a==1)
#define ACCEPTABLE_POLARIZATION(a) \
    (a!=j2ms::unknown)
#define ACCEPTABLE_SENSE(a) \
    (a!=j2ms::unknown_sense)


#endif
