//
// BaseLineIdx, class derived from DualIndex, holding two
// antennanumbers
//
//
#ifndef BASELINEIDX_H
#define BASELINEIDX_H


//  We *are* a DualIndex
#include <jive/labels/DualIndex.h>

//#include <jive_CD3.h>


class BaseLineIdx :
    public DualIndex
{
public:
    // 'magic' value to compare against
    // to decide if the BaseLineIdx is
    // a default (invalid) or filled in
    // thingy
    static const BaseLineIdx  invalidBaseLineIdx;

    //  zero-BaseLineIdx with maximum nr of stations
    explicit BaseLineIdx( unsigned int nrstations=32 );
    
    //  full BaseLineIdx
    BaseLineIdx( unsigned int st1, unsigned int st2, unsigned int nrstations=32 );
    
    //  Fill the baselineidx in from Alberts INTFR struct thingy
//    const BaseLineIdx& operator=( const ::INTFR_DATADESCR_TYPE& ifref );

    // Allow assignment from DualIndex, in order to
    // reset the contents?
//    const BaseLineIdx& operator=( const DualIndex& di );
    const BaseLineIdx& operator=( const BaseLineIdx& bli );

    // compare for equality
    bool operator==( const BaseLineIdx& other ) const;

    // tells us if the baselineidx has swapped coordinates
    // (that is: the 'X' index (of X-Y) > the 'Y' index.
    // "normalized" baseline index should have X<=Y
    bool        is_swapped( void ) const;

    // return a new BaseLineIdx but with
    // swapped indices
    BaseLineIdx swap( void ) const;
    
    //  Empty:
    ~BaseLineIdx()
    {}


private:
    unsigned int nrStations;
};



#endif
