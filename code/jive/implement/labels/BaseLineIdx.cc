//
// implementation
//
// $Id: BaseLineIdx.cc,v 1.5 2011/10/12 12:45:33 jive_cc Exp $
//
// $Log: BaseLineIdx.cc,v $
// Revision 1.5  2011/10/12 12:45:33  jive_cc
// HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//       changed into 'labels' for more genericity
//     * can now be built either as part of AIPS++/CASA [using makefile]
//       or can be built as standalone set of libs + binaries and linking
//       against casacore [using Makefile]
//       check 'code/
//
// Revision 1.4  2007-02-26 16:02:42  jive_cc
// HV: - cosmetic changes
//     - added swap() and is_swapped() methods
//       to properly deal with swapped baseline-indices
//
// Revision 1.3  2006/03/02 14:21:37  verkout
// HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
// Revision 1.2  2006/02/17 12:41:25  verkout
// HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//
// Revision 1.1  2004/08/24 13:22:38  verkout
// HV: Moved implementation to .cc files (this stuff was mostly in .h files... YUK!)
//
//
#include <jive/labels/BaseLineIdx.h>

const BaseLineIdx BaseLineIdx::invalidBaseLineIdx = BaseLineIdx(0);

BaseLineIdx::BaseLineIdx( unsigned int nrstations ) :
	DualIndex( nrstations ),
    nrStations( nrstations )
{}

BaseLineIdx::BaseLineIdx( unsigned int st1, unsigned int st2,
                          unsigned int nrstations ) :
	DualIndex( nrstations ),
    nrStations( nrstations )
{
	this->idx1() = st1;
	this->idx2() = st2;
}

/*
const BaseLineIdx& BaseLineIdx::operator=( const ::INTFR_DATADESCR_TYPE& ifref ) {
    this->DualIndex::operator=(DualIndex(nrStations));

	this->idx1() = ifref.XANTNR;
	this->idx2() = ifref.YANTNR;
	return *this;
}

const BaseLineIdx& BaseLineIdx::operator=( const DualIndex& di )
{
    this->DualIndex::operator=(di);
    return *this;
}
*/
const BaseLineIdx& BaseLineIdx::operator=( const BaseLineIdx& bli ) {
    if( this!=&bli ) {
        this->DualIndex::operator=((DualIndex&)bli);
        nrStations = bli.nrStations;
    }
    return *this;
}

bool BaseLineIdx::operator==( const BaseLineIdx& other ) const {
    return (this->nrStations==other.nrStations &&
            this->DualIndex::operator==(other));
}

bool BaseLineIdx::is_swapped( void ) const {
    return this->idx1()>this->idx2();
}

BaseLineIdx BaseLineIdx::swap( void ) const {
    return BaseLineIdx(this->idx2(), this->idx1(), this->nrStations);
}
