// implementation
//
#include <jive/labels/polarization.h>
#include <jive/Utilities/jexcept.h>

#include <map>
#include <sstream>

// For toupper/tolower
#include <ctype.h>

using namespace std;

namespace j2ms {

    // the strict weak ordering of polarizations:
    // R comes before L, X before Y and both R,L
    // come before any of X,Y
    bool operator<(polarization lhs, polarization rhs) {
        static const j2ms::polarization  __order[] = {j2ms::r, j2ms::l, j2ms::x, j2ms::y};
        static const unsigned int        __N_pol( sizeof(__order)/sizeof(__order[0]) );

        unsigned int   lptr, rptr;

        for(lptr=0; lptr<__N_pol; lptr++)
            if( __order[lptr]==lhs )
                break;
        for(rptr=0; rptr<__N_pol; rptr++)
            if( __order[rptr]==rhs )
                break;
        // really should NOT compare unknown stuff
        // in the header file is a comment about why
        if(lptr==__N_pol || rptr==__N_pol)
            THROW_JEXCEPT("comparing at least one unknown polarization to something else");

        return lptr<rptr;
    }

    // show a set of polarizations
    ostream& operator<<(ostream& os, const polarizations_t& p) {
        polarizations_t::const_iterator curpol;
        os << "[";
        for( curpol=p.begin(); curpol!=p.end(); curpol++ )
            os << *curpol;
        os << "]";
        return os;
    }
    // return the sense of a polarization
    sense sense_of( polarization p ) {
        sense  rv( unknown_sense );

        switch( p ) {
            case x:
            case y:
                rv = linear;
                break;

            case r:
            case l:
                rv = circular;
                break;
            default:
                break;
        }
        return rv;
    }

    ostream& operator<<(ostream& os, sense s ) {
        switch( s ) {
            case circular:
                os << "circular";
                break;
            case  linear:
                os << "linear";
                break;
            case unknown_sense:
                os << "<unknown>";
                break;
            default:
                os << "<INVALID!>";
                break;
        }
        return os;
    }
   
    // before we actually start defining the outputfunctions
    // or the char2pol function, let us define a global
    // character -> polarization mapping.
    // This will guarantee consistency between the two
    // methods as both use the same mapping albeit in
    // opposite directions..
    typedef std::map<char,polarization>  charmap_type;

    // function which returns the built-in mapping
    // Note: we leave it up to the char2pol map function
    // and the output function to decide which case to
    // support... [in the mapping currently only lowercase
    // in order to keep the mapping unique in both directions]
    charmap_type make_builtin_char2polmap( void ) {
        charmap_type   rv;

        rv.insert( make_pair('X', x) );
        rv.insert( make_pair('Y', y) );
        rv.insert( make_pair('R', r) );
        rv.insert( make_pair('L', l) );
        
        return rv;
    }

    // And we have a static const mapping which we will
    // init once.
    static const charmap_type  _c2pmap = make_builtin_char2polmap();
    
    // output a character indicating the polarization 
    // Attempt to locate the polarization in the 
    // builtin mapping
    ostream& operator<<( ostream& os, polarization p ) {
        char                         c( '?' );
        charmap_type::const_iterator ptr;
        
        for( ptr=_c2pmap.begin(); ptr!=_c2pmap.end(); ptr++ ) {
            if( ptr->second==p ) {
                c = ptr->first;
                break;
            }
        }
        return os << c;
    }


    // warp character into polcode
    // Note: we may not make any assumptions about
    // the case of character in the mapping
    // so we just try both up- and lower- case
    // of the character.
    polarization  char2polcode( char c ) {
        // now look up
        bool                         islc;
        polarization                 rv( unknown );
        charmap_type::const_iterator ucptr;
        charmap_type::const_iterator lcptr;

        ucptr = _c2pmap.find( ::toupper(c) );
        lcptr = _c2pmap.find( ::tolower(c) );

        // require it to be unique
        if( (ucptr==_c2pmap.end() && lcptr!=_c2pmap.end() && (islc=true)==true) ||
            (ucptr!=_c2pmap.end() && lcptr==_c2pmap.end() && (islc=false)==false) ) {
            if( islc ) 
                rv = lcptr->second;
            else
                rv = ucptr->second;
        }
        return rv;
    }


    // the label2polarizationmap thingy
    log2polmap::log2polmap()
    {}

    log2polmap::log2polmap( val_type v0 ) {
        mapinsres    ires;
        
        // assert that v0.first is either '0' or '1'
        if( !ACCEPTABLE_INDEX(v0.first) ) 
            THROW_JEXCEPT("log2polmap(v0): Unacceptable polarization index "
                       << v0.first << "; it is not 0|1");
        if( !ACCEPTABLE_POLARIZATION(v0.second) )
            THROW_JEXCEPT("log2polmap(v0): Unacceptable polarization " << v0.second);
        ires = __m.insert( v0 );
        if( !ires.second )
            throw failed_insert( v0, "Failed " );
    }

    log2polmap::log2polmap( val_type v0, val_type v1 ) {
        sense        v0sense( sense_of(v0.second) );
        sense        v1sense( sense_of(v1.second) );
        mapinsres    ires;
       
        // it only makes sense to map polarizations of the same sense
        if( !ACCEPTABLE_SENSE(v0sense) || !ACCEPTABLE_SENSE(v1sense) ||
            v0sense!=v1sense ) {
            THROW_JEXCEPT("log2polmap(v0,v1): Attempt to map incompatible "
                       "senses of polarization:\n" 
                       << " sense#0: " << v0sense << "(" << v0.second << ")\n"
                       << " sense#1: " << v1sense << "(" << v1.second << ")");
        }
        
        // assert that v0.first is either '0' or '1'
        if( !ACCEPTABLE_INDEX(v0.first) ) 
            THROW_JEXCEPT("log2polmap(v0, v1): Unacceptable polarization index '"
                       << v0.first << "' for v0; it is not 0|1");
        if( !ACCEPTABLE_POLARIZATION(v0.second) )
            THROW_JEXCEPT("log2polmap(v0, v1): Unacceptable polarization "
                       << v0.second << " for v0");
        ires = __m.insert( v0 );
        if( !ires.second )
            throw failed_insert( v0, "Failed " );

        // assert that v1.first is either '0' or '1'
        if( !ACCEPTABLE_INDEX(v1.first) ) 
            THROW_JEXCEPT("log2polmap(v0, v1): Unacceptable polarization index '"
                       << v1.first << "' for v1; it is not 0|1");
        if( !ACCEPTABLE_POLARIZATION(v1.second) )
            THROW_JEXCEPT("log2polmap(v0, v1): Unacceptable polarization "
                       << v1.second << " for v1");
        ires = __m.insert( v1 );
        if( !ires.second )
            throw failed_insert( v1, "Duplicate " );
    }

    // Attempt to insert. Upon any fishyness - throw up!
    void log2polmap::insert( val_type v ) {
        sense                   vsense( sense_of(v.second) );
        mapinsres               ires;
        maptype::const_iterator ptr;
      
        // sense of the polarization must make sense
        if( !ACCEPTABLE_SENSE(vsense) ) 
            THROW_JEXCEPT("Attempt to insert entry with unacceptable sense - " << vsense << " (" << v << ")");
        
        // Check existing entry/entries and verify that the sense is compatible (ie: equal)
        for(ptr=__m.begin(); ptr!=__m.end(); ptr++)
            if( sense_of(ptr->second)!=vsense )
                THROW_JEXCEPT("Attempt to insert entry with different sense. New entry: " << vsense
                              << " (from " << v << "), existing sense " << sense_of(ptr->second));
        // assert that v.first is either '0' or '1'
        if( !ACCEPTABLE_INDEX(v.first) ) 
            THROW_JEXCEPT("Unacceptable polarization index '" << v.first << "'; it is not 0|1");
        // Now try to insert the blighter
        ires = __m.insert( v );
        if( !ires.second )
            throw failed_insert( v, "Failed " );
    }

    polarization  log2polmap::find( int label ) const {
        polarization            rv( j2ms::unknown );
        maptype::const_iterator pptr;
        
        if( !ACCEPTABLE_INDEX(label) ) 
            THROW_JEXCEPT("log2polmap::find(label=" << label << ") - label is not 0|1");
        if( (pptr=__m.find(label))!=__m.end() )
            rv = pptr->second;
        return rv;
    }

    log2polmap::key_type log2polmap::key_of( j2ms::polarization p ) const {
        key_type       rv( invalid_key );
        const_iterator curentry;

        for( curentry=this->begin(); curentry!=this->end(); curentry++ ) {
            if( curentry->second==p ) {
                rv = curentry->first;
                break;
            }
        }
        return rv;
    }

    // retrieve indices
    int log2polmap::index_of( polarization p ) const {
        int            rv( -1 );
        int            idx;
        const_iterator curentry;

        for( idx=0, curentry=this->begin(); curentry!=this->end() && rv==-1; idx++, curentry++ )
            if( curentry->second==p ) 
                rv=idx;
        return rv;
    }
    // Lookup of logical label 'k'
    // it could be that "index_of(1) == 0" !!
    int log2polmap::index_of( key_type k ) const {
        int            rv( -1 );
        int            idx;
        const_iterator curentry;

        for( idx=0, curentry=this->begin(); curentry!=this->end() && rv==-1; idx++, curentry++ )
            if( curentry->first==k ) 
                rv=idx;
        return rv;
    }

    size_t log2polmap::size( void ) const {
        return __m.size();
    }

    bool log2polmap::operator==( const log2polmap& other ) const {
        return this->__m==other.__m;
    }
    bool log2polmap::operator!=( const log2polmap& other ) const {
        return !(this->operator==(other));
    }

    // the exception
    log2polmap::failed_insert::failed_insert( val_type v, const string& action ) {
        ostringstream  s;

        s << action << " insert of (label,pol) = (" << v.first << ", " << v.second 
          << ")";
        msg = s.str();
    }
    const char* log2polmap::failed_insert::what( void ) const throw() {
        return msg.c_str();
    }

    // show the contents of the log2pol map in HRF
    ostream& operator<<( ostream& os, const log2polmap& l2p ) {
        log2polmap::const_iterator curentry;

        os << '[';
        for( curentry=l2p.begin(); curentry!=l2p.end(); curentry++ ) {
            os << '<' << curentry->first << ":" << curentry->second << '>';
        }
        os << ']';
        return os;
    }
}
