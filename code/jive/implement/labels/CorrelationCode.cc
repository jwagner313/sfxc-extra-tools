//  implementation of the CorrelationCode class
//
#include <jive/labels/CorrelationCode.h>
#include <jive/Utilities/jexcept.h>

#include <algorithm>

// hmmm... don't really want
// to include <cassert> and <cstdlib>
// 'cuz I don't want this stuff to be in
// the std:: namespace, I like 'm were they
// are - in the :: namespace
#include <stdlib.h>

using std::ostream;


CorrelationCode::CorrelationCode() {
    __polarizations[0] = j2ms::unknown;
    __polarizations[1] = j2ms::unknown;
}

CorrelationCode::CorrelationCode( j2ms::polarization p0,
                                  j2ms::polarization p1 ) {

    j2ms::sense   s0( j2ms::sense_of(p0) );
    j2ms::sense   s1( j2ms::sense_of(p1) );

    if( !ACCEPTABLE_POLARIZATION(p0) || !ACCEPTABLE_POLARIZATION(p1) )
        THROW_JEXCEPT("Attempt to construct CorrelationCode from " 
                    << p0 << "," << p1 << " is unacceptable");
    if( !ACCEPTABLE_SENSE(s0) || !ACCEPTABLE_SENSE(s1) || (s0!=s1) )
        THROW_JEXCEPT("Attempt to construct CorrelationCode from "
                   << p0 << "," << p1 << " fails because of "
                   << "incompatible senses of polarization:\n"
                   << "  sense#0: " << s0 << "\n"
                   << "  sense#1: " << s1);
    // ok... got that straightened up ;)
    __polarizations[0] = p0;
    __polarizations[1] = p1;
}
/*
CorrelationCode::CorrelationCode( const ::INTFR_DATADESCR_TYPE& ifref,
                                  const j2ms::log2polmap& l2p ) {
    __polarizations[0] = l2p.find( ifref.XPOLNR );
    __polarizations[1] = l2p.find( ifref.YPOLNR );
}
*/
void CorrelationCode::swap( void ) {
    std::swap(__polarizations[0], __polarizations[1]);
    return;
}

// This will automatically return false for default CorrelationCodes
// since the default c'tor makes sure __polarizations[0]==__polarizations[1]
bool CorrelationCode::is_crosspol( void ) const {
    return __polarizations[0]!=__polarizations[1];
}

j2ms::polarization CorrelationCode::operator[]( unsigned int idx ) const {
    if( !ACCEPTABLE_INDEX(idx) )
        THROW_JEXCEPT("operator[" << idx << "]: index not 0|1");
    return __polarizations[idx];
}

// (in)equality operators
// as argued in the .h file, we can just do an element-by-element
// comparison.
bool CorrelationCode::operator==( const CorrelationCode& other ) const {
    return ( this->__polarizations[0]==other.__polarizations[0] &&
             this->__polarizations[1]==other.__polarizations[1] );
}

bool CorrelationCode::operator!=( const CorrelationCode& other ) const {
    return !this->operator==(other);
}

// insert into std::ostream
ostream& operator<<(ostream& os, const CorrelationCode& c ) {
    return os << c[0] << c[1];
}


#define MAKE_CODE(a,b) \
    CorrelationCode(a,b)


// implement strict weak ordering
// for correlation codes.
// 
// * parallel hands of polarization come before
//   cross
// * r comes before l
// * x comes before y
// * any r,l combi comes before any x,y combi
// * comparision with an undefined combi
//   (at least one of the constituent polarizations
//    is unknown or the code is [xy][lr] or [rl][xy])
//   will result in an assertion failure...
// No really, you only want to compare sensible
// correlation codes. Otherwise you may be comparing
// completely unrelated things which I think you'll
// agree may lead to rubbish results
bool operator<(const CorrelationCode& l, const CorrelationCode& r ) {
    static const CorrelationCode  __order[] = {
        CorrelationCode( j2ms::r, j2ms::r ),
        CorrelationCode( j2ms::l, j2ms::l ),
        CorrelationCode( j2ms::r, j2ms::l ),
        CorrelationCode( j2ms::l, j2ms::r ),
        CorrelationCode( j2ms::x, j2ms::x ),
        CorrelationCode( j2ms::y, j2ms::y ),
        CorrelationCode( j2ms::x, j2ms::y ),
        CorrelationCode( j2ms::y, j2ms::x )
    };
    static const unsigned int    __N_combi( sizeof(__order)/sizeof(__order[0]) );

    // look up the left- and righthandsides
    unsigned int   lptr, rptr;

    for(lptr=0; lptr<__N_combi; lptr++)
        if( __order[lptr]==l )
            break;
    for(rptr=0; rptr<__N_combi; rptr++)
        if( __order[rptr]==r )
            break;
    if( lptr==__N_combi || rptr==__N_combi )
        THROW_JEXCEPT("comparing at least one insensible polarizationcombination to another: "
                      << l << "<" << r);
    return lptr<rptr;
}

