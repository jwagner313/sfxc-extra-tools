/*********************************************************************************

  Class:
           VisLabel

  Purpose:
           To faciliate/unify indexing/labelling visibilities.

           One can set jobindex, subarrayindex,
           framenr(integrationnr), polarization, interferometer,
           baseline...

 *********************************************************************************/

#ifndef VISLABEL_H
#define VISLABEL_H

#include <jive/labels/BaseLineIdx.h>
#include <jive/labels/CorrelationCode.h>

#include <iostream>


struct VisLabel
{
    //  Default ctor -> zero index
    VisLabel( unsigned int job=0, unsigned int sa=0,
              unsigned int sb=0, const BaseLineIdx& bl=BaseLineIdx() ):
        jobnr( job ),
        subarraynr( sa ),
        subbandnr( sb ),
        baseline( bl )
    {}

    //  The index of this job in the array of jobs
    unsigned int   jobnr;

    //  The current subarraynr
    unsigned int   subarraynr;

    //  Which subband?
    //  (IF in classic AIPS)
    unsigned int   subbandnr;

    //  What is the baseline?
    BaseLineIdx    baseline;

    //  The destructor
    virtual ~VisLabel()
    {}
};

//  Send formatted output to the stream
std::ostream& operator<<( std::ostream& os, const VisLabel& vlref );

//
//  Oops... I forgot to include in VisLabel the fact that a visibility
//  does not necessarily has one correlation.... Therefore we need to
//  implement an extended VisLabel that holds an array of up to four
//  correlationnumbers!
//
//  Also, for us at JIVE, it might be interesting to keep track of the
//  SU where the data originally came from..
//
struct ExtVisLabel :
    public VisLabel
{
    

public:
    // ...
    ExtVisLabel():
        VisLabel()
    {}
   
    // Return the correlations (ie polarization products)
    // present in this visibility.
    // Order is important. It is assumed that row[i]
    // in the datamatrix holds data for the correlation
    // product correlations[i].
    correlations_t                       correlations;

    //  De-allocate resources...
    virtual ~ExtVisLabel()
    {}
};
// show this'un on sum stream
std::ostream& operator<<( std::ostream& os, const ExtVisLabel& visref );


//  The JIVE-visibility labels features stuff like station units and BOCFs...
struct JIVEVisLabel :
    public ExtVisLabel
{
    //  Default c'tor
    JIVEVisLabel():
        ExtVisLabel(),
        startBOCF( (unsigned int)-1 ),
        endBOCF( (unsigned int)-1 ),
        xStationUnit( (unsigned int)-1 ),
        yStationUnit( (unsigned int)-1 )
    {}
    
    //  The start/end bocf framenrs
    //  set to (unsigned int)-1 is invalid/not known
    unsigned int               startBOCF;
    unsigned int               endBOCF;

    //  And SU's (also: (unsigned int)-1 indicates invalid/unknown)
    unsigned int               xStationUnit;
    unsigned int               yStationUnit;

    //  Delete allocated resources
    virtual ~JIVEVisLabel()
    {}
    
};


#endif
