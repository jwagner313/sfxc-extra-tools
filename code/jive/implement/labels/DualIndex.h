//
//  A dual index keeps two indices together (like a baseline or a
//  correlationindex).
//
//  This is a base-class for specific indices that are a composite of
//  two numbers. An added feature is that we can map it to a linear
//  index.
//
//  The idea behind it is the following:
//
//  Suppose index1 ramges from 0-(n-1) and index2 ranges from 0-(m-1),
//  then we have at maximum n*m elements that we need to be able to
//  discriminate. If we map the index (x,y) to one number, say l (for
//  linear), we can jump into an array of length (n*m) to position l
//  to find the desired element directly. I think this may speed up
//  indexing quite a bit.
//
//
//
#ifndef DUALINDEX_H
#define DUALINDEX_H

#include <iostream>
#include <jive/tmplsrc/restrictedvalue.h>

//
// note: the maximum for idx[1] is just there
// for logic reasons, not for speed or mapping
// to a linear index...
//
template <typename T>
class dual_index_t
{
	public:

        // is_sane() on an object like this should
        // return false...
        dual_index_t() {}

		// create a 'symmetrical' dual index
		// max. for idx0 == max. for idx1 == max
		explicit dual_index_t( const T& max )
		{
			idx[0] = restrictedvalue_t<T>(0, max);
			idx[1] = restrictedvalue_t<T>(0, max);
		}

        // create a dual_index
        // with max. value max0 for idx[0] and max1 for idx[1]
        // and uninitialized idx0, idx1
		dual_index_t( const T& max0, const T& max1 )
		{
			idx[0] = restrictedvalue_t<T>(0, max0);
			idx[1] = restrictedvalue_t<T>(0, max1);
		}

        // create a 'symmetrical' dual_index
        // with max idx[0] == max idx[1] == max
        // and filled in idx[0] and idx[1]
        dual_index_t( const T& max, const T& i0, const T& i1 )
        {
            idx[0] = restrictedvalue_t<T>(0, max);
            idx[1] = restrictedvalue_t<T>(0, max);

            idx[0] = i0;
            idx[1] = i1;
        }
        // create a dual_index
        // with max idx[0] == max0, max idx[1] == max1
        // and filled in idx[0] and idx[1]
        dual_index_t( const T& max0, const T& max1, const T& i0, const T& i1 )
        {
            idx[0] = restrictedvalue_t<T>(0, max0);
            idx[1] = restrictedvalue_t<T>(0, max1);

            idx[0] = i0;
            idx[1] = i1;
        }

		//
		// copy c'tor
		//
		dual_index_t( const dual_index_t<T>& other )
		{
			// copy over the restricted values
			for( unsigned int i=0; i<2; ++i )
			{
				idx[i] = other.idx[i];
			}
		}

		//
		// assignment
		//
		const dual_index_t<T>& operator=( const dual_index_t<T>& other )
		{
			if( this!=&other )
			{
				for( unsigned int i=0; i<2; ++i )
				{
					idx[i] = other.idx[i];
				}
			}
			return *this;
		}
		
		// to linear -> the whole goal of this 
		T  to_linear( void ) const
		{
			return (idx[0]*idx[0].upper_bound())+idx[1];
		}

		// accessors for speed...
		inline restrictedvalue_t<T>& operator[]( unsigned int i )
		{
			if( i<2 )
			{
				return idx[i];
			}
			throw( "dual_index_t::operator[]/index out of range!" );
		}

		inline const restrictedvalue_t<T>& operator[]( unsigned int i ) const
		{
			if( i<2 )
			{
				return idx[i];
			}
			throw( "dual_index_t::operator[]/index out of range!" );
		}
        //
        // Old fashioned interface
        //
        inline restrictedvalue_t<T>&  idx1( void )
        {
            return idx[0];
        }
        inline restrictedvalue_t<T>&  idx2( void )
        {
            return idx[1];
        }
        inline const restrictedvalue_t<T>&  idx1( void ) const
        {
            return idx[0];
        }
        inline const restrictedvalue_t<T>&  idx2( void ) const
        {
            return idx[1];
        }

        // comparison operators
        bool operator==( const dual_index_t<T>& r ) const
        {
        	return (this->idx[0]==r.idx[0] && this->idx[1]==r.idx[1]);
        }
        bool operator!=( const dual_index_t<T>& r ) const
        {
        	return !(this->operator==(r));
        }

        //
        // sane()?
        // Iff you want to test for an uninitialized value
        // it's best to assing default dual_index_t()
        // since sane() will return 'sanity' of both
        // the restricted_values's
        //
        bool is_sane( void ) const
        {
            return (idx[0].is_sane() && idx[1].is_sane());
        }

		~dual_index_t()
		{
		}
	private:
		//
		// Private parts of the dual index
		//
		restrictedvalue_t<T>    idx[2];
};


typedef dual_index_t<unsigned int> DualIndex;

//
// Let's make the DualIndex class suitable for STL-indexing
// We'll need to make the thingy comparable
//
typedef struct _lt_dualindex_t
{
	// return true iff 'l' less-than 'r'
	bool operator()( const DualIndex& l, const DualIndex& r ) const
	{
		return ( l.to_linear()<r.to_linear() );
	}
} lt_dualindex_t;


//
// Dump the index nicely formatted on a stream
//
std::ostream& operator<<( std::ostream& os, const DualIndex& diref );

#endif
