// correlation code is a polarization-combination ie it is a two-tuple of j2ms::polarization
// indicating exactly which correlation/polarization product we're talking about
//
// As of 17/08/2006 - HV: decided that the handling of polarization in j2ms
//                    (and JCCS/data_handler) basically is a mess.
//                    Pol. info quite important, also for MODEL en SamplerStats
//                    it had to be un-messed. Quite significant change.
//
//                    CorrelationCode used to be just a pair of integers,
//                    either having only allowed values 0|1. No link
//                    whatsoever to physical polarization.
//
//                    As a result, different parts of the code sort of
//                    had to twiddle about what the 0 and 1 meant -
//                    sometimes it could be index, sometimes it would
//                    just be a logical label.
//
//                    Decided to make it an explicit thing: it is
//                    now a 2-element tuple of physical polarizations
//                    no 'logical' stuff, just what it is.
#ifndef JIVE_DTDLUT_CORRELATIONCODE_H
#define JIVE_DTDLUT_CORRELATIONCODE_H

#include <jive/labels/polarization.h>

// need this, it's from Albert's stuff
// and contains the struct definitions
//#include <jive_CD3.h>

#include <map>
#include <vector>
#include <iostream>

// for ostream_iterator and copy()
#include <iterator>
#include <algorithm>


// The actual correlation code class,
// really nothing but a pair of j2ms::polarization
// The thing MUST have exactly two polarizations.
// No check for unknown or bizarre combinations
// of XL, RY etc...
class CorrelationCode 
{
public:
    //  a default correlation code -
    //  both polarization get set to 'unknown'
    CorrelationCode( );
    
    //  Create a defined correlationcode
    //  Asserts that p0 and p1 are from the same sense
    //  and that none of them are unknown
    CorrelationCode( j2ms::polarization p0, j2ms::polarization p1 );

    //  Create it from an interferometer.
    //  In this case you MUST supply a map
    //  of logical label to physical polarization.
    //  Note: the assertion taht the log2polmap
    //  makes somewhat physical sense (eg the polarization
    //  types mapped are from the same sense) is
    //  enforced by the log2polmap itself.
    //  We just take for granted what the unmapping via
    //  the log2polmap giving us. No assertions
    //  whatsoever
//    CorrelationCode( const ::INTFR_DATADESCR_TYPE& ifref,
//                     const j2ms::log2polmap& l2p );

    // handy shortcuts for operations on polarization
    // combi's

    // ...
    // No assertions made.
    void      swap( void );

    // ...
    bool      is_crosspol( void ) const;

    // Use operator[] to access the polarizations
    // this two-tuple consists of.
    // Note: indexing with anything other than 0|1
    // will result in an assertion error. It's up to you
    j2ms::polarization  operator[]( unsigned int idx ) const;

    // compare CorrelationCodes.
    // as it is impossible to create a correlationcode
    // with one unknown (either both are unknown [default
    // c'tor] or both are known [asserted by c'tor from
    // two polarizations]) we can just get by
    // by doing an element by element comparison.
    // Note: even the c'tor taking an INTFR_DATADESCR_TYPE
    // enforces sesible values, albeit that in this
    // case it is the 'logical-to-polarization map'
    // which enforces this and not the c'tor.
    bool operator==( const CorrelationCode& other ) const;
    bool operator!=( const CorrelationCode& other ) const;

    // don't need no d'tor
private:
    j2ms::polarization   __polarizations[2];
};

// show the correlation code in HumanReadableFormat,
// just as XY, RL etc
std::ostream& operator<<( std::ostream& os, const CorrelationCode& c );

// implement a 'strick weak ordering' for
// CorrelationCodes. Necessary if CorrelationCodes
// are to be used in STL Ordered Container thingies
// (like map, set)
// Make it a global operator< so the user of
// CorrelationCode does not have to supply a 
// KeyCompare in his/her map/set template
//
// NOTE: this operator may throw an exception if
// you are comparing to (at least one) polarization
// product which is not completely defined (ie one
// of the constituent polarizations is 'j2ms::undefined'
// or even a ludicrous value, if you messed up the system).
//
// See 'polarization.h' file, comment near
// "bool operator<(polariation lhs, polarization rhs)"
// for the reason why.
bool operator<(const CorrelationCode& l, const CorrelationCode& r );


// A typedef for a list/set of correlations
// eg which products were computed by the
// correlator
// 
// As order (may) be important we cannot
// use containers which implement the
// Sorted* model, which leaves us with a
// choice of list, vector
// For historical reasons, operator[] has to
// be available so that limits us to vector...
typedef std::vector<CorrelationCode>  correlations_t;

// define an ordered set of correlations. CorrelationCodes
// inserted into this container will be ordered in a
// pre-defined order; you cannot influence it.
// If you want to influence the order in which
// the CorrelationCodes are in the container, use the
// "correlations_t" [which is an un-ordered container]
typedef std::set<CorrelationCode>     correlationset_t;


// this will assume L is a container with
// CorrelationCode's in it. Anything which
// supports .begin( and .end() should
// suffice...
template <typename L>
struct accimplstruct {
        friend std::ostream& operator<<(std::ostream& os, const accimplstruct<L>& ac) {
            os << "[";
            std::copy( ac.__container.begin(), ac.__container.end(),
                       std::ostream_iterator<CorrelationCode>(os, ",") );
            os << "]";
            return os;
        }
        accimplstruct( const L& l ):
            __container( l )
        {}
        accimplstruct( const accimplstruct<L>& other):
            __container( other.__container )
        {}
        
    private:
        const L&  __container;
        
        // no default/assignment
        accimplstruct();
        const accimplstruct<L>& operator=( const accimplstruct<L>& );
        
        
};

// by making as_corrcode a template function we can use
// it for any containertype which holds correlation codes!
// *yet* another big hoorah up c++'s ass!
template <typename L>
accimplstruct<L> as_corrcode( const L& l ) {
    return accimplstruct<L>(l);
}

#endif
