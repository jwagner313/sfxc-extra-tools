// handy for if assertions fail...
#ifndef JIVE_ASSERTFAIL_H
#define JIVE_ASSERTFAIL_H

#include <string>
#include <iostream>

// for unlimiting coredump
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include <errno.h>
#include <string.h>
#include <stdlib.h>

// you can pass stream formatted stuff
// into this, it will try to unlimit
// coredumpsize and then fail the
// assertion, such that it will
// dump core.
//
// Example:
//
// if( should_not_happen_condition(varA, constB) )
//    ASSERTFAIL("WOT? " << varA << " should NEVER be <fill-in> " << constB);
// 

#ifdef __GNUC__
#define __HFUNC__ __PRETTY_FUNCTION__
#else
#define __HFUNC__ ""
#endif

#define ASSERTFAIL(a) \
    do {\
        int                lNuM( __LINE__ );\
        struct rlimit      cdmp_SIze = {RLIM_INFINITY, RLIM_INFINITY};\
        std::cerr << __FILE__ << ":" << lNuM << " " << __HFUNC__ << "\n" << a << "\n";\
        if( ::setrlimit(RLIMIT_CORE, &cdmp_SIze)!=0 ) {\
            std::cerr << "Failed to set coredumpsize to unlimited - "\
                 << ::strerror(errno) << std::endl;\
        }\
        ::abort();\
    } while( 0 )

#endif
