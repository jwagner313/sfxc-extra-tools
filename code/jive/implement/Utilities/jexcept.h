// defines macro to simplify throwing
//
// $Id: jexcept.h,v 1.2 2007/03/15 16:36:38 jive_cc Exp $
//
// $Log: jexcept.h,v $
// Revision 1.2  2007/03/15 16:36:38  jive_cc
// HV: Changed formatting so that the exception text is
//     (hopefully) somewhat better readable
//
// Revision 1.1  2006/02/14 13:25:07  verkout
// HV: Defines a 'jive-exception-object', or jexcept for short. Also defines macros for easy throwing. The jexcept thingy is derived from std::exception so catching a const ref to std::exception catches these as well
//
//
#ifndef UTILITIES_JEXCEPT_H
#define UTILITIES_JEXCEPT_H

#include <string>
#include <sstream>
#include <exception>

// the jexcept exception, derived from std::exception
// so you can just catch std::exception& and catch these
// buggers as well...
class jexcept:
    public std::exception
{
    public:
        jexcept( const std::string& wot ):
            std::exception(), _wot( "jexcept:\n"+wot ) 
        {}
        virtual const char* what( void ) const throw() {
            return _wot.c_str();
        }

        virtual ~jexcept() throw() {}
    private:
        std::string  _wot;
};

//  Define the __HERE__ macro, using GNU-C extensions if available
#ifdef __GNUC__
#define __JEXCEPTHERE__ __FILE__ << ':' << _LcL_LiNeNo << "\nin method: " << __PRETTY_FUNCTION__
#else
#define __JEXCEPTHERE__ __FILE__ << ':' << _LcL_LiNeNo
#endif


// Throw an exception of type <excp>; use <strm> for the message. 
// Use this macro to insure that the  __HERE__ macro expands properly.
#define THROW_JEXCEPT(strm) \
{ \
  int                _LcL_LiNeNo( __LINE__ );\
  std::ostringstream _LcL_oss_; \
  _LcL_oss_ << __JEXCEPTHERE__ << "\n" << strm; \
  throw jexcept(_LcL_oss_.str()); \
}


#endif
