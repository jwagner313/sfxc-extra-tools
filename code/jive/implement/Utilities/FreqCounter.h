//
//  FreqCounter - a class that allows one to keep track of the number
//  of occurences of numbers
//
//  Author:   Harro Verkouter,  23-03-1999
//
//  $Id: FreqCounter.h,v 1.4 2006/03/02 14:21:38 verkout Exp $
//
#ifndef FREQCOUNTER_H
#define FREQCOUNTER_H


#include <map>
#include <iostream>
#include <algorithm>


namespace j2ms {
    
    struct printer
    {
        public:
            // create with ref to stream and
            // optional delimiter (printed
            // after each printout of an element)
            printer( std::ostream& s,
                     const char* d=0 );
            
            template <typename T>
            void operator()( const T& t )
            {
                _s << t;
                if( _d ) _s << _d;
            }
            
        private:
            std::ostream&  _s;
            const char*    _d;

            // prohibit these
            printer();
            const printer& operator=( const printer& );
    };

    // FreqCounter is now just a map from int -> unsigned int
    // where 'key' is the value being counted and
    // the value represents the number of times that value
    // was counted (ie: operator++ was used)
    typedef std::map<int, unsigned int> FreqCounter;

    // tell the system how to display a key/value pair
    std::ostream& operator<<( std::ostream& os,
                              const FreqCounter::value_type& v );

    // and how to display a whole FreqCounter
    std::ostream& operator<<( std::ostream& os, 
                              const FreqCounter& f );
}

#endif
