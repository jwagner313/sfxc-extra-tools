//# $Id: streamutil.cc,v 1.4 2006/11/23 13:24:49 jive_cc Exp $
//# $Log: streamutil.cc,v $
//# Revision 1.4  2006/11/23 13:24:49  jive_cc
//# HV: Beautified streamutil stuff
//#
//# Revision 1.3  2006/11/08 10:07:51  jive_cc
//#
//#
//# [small@jive.nl]
//# StandardPlots C++ changes:
//# * Fixed a bug in averaging for ANPChanPlotter.cc
//# * Adjusted prototypes to make warning-free compile (as needed for -WError)
//#
//# Revision 1.2  2006/02/17 12:41:26  verkout
//# HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//#
//# Revision 1.1  2003/09/12 07:23:49  verkout
//# HV: Nifty streamutils for eg pretty printing of numbers, indenting.
//#
//
#include <jive/Utilities/streamutil.h>
#include <iomanip>
#include <numeric>

// Needed for vsnprintf() and va_{arg,list} etc
#include <cstdio>
#include <cstdarg>

using std::ostream;
using std::string;
using std::setw;

namespace j2ms {
    
    // define the static thing in indent
    std::deque<unsigned int>  indent::_indents;

    // the pprint thingy
    pprint::pprint( int n, const string& str, const string& mult ) :
    	isint( true ),
    	mystr( str ),
    	mymult( mult )
    {
    	mynr.inr = n;
    }

    pprint::pprint( unsigned int n, const string& str, const string& mult ) :
    	isint( false ),
    	mystr( str ),
    	mymult( mult )
    {
    	mynr.uinr = n;
    }

    ostream& operator<<( ostream& os, const pprint& p )
    {
    	if( p.isint ) {
    		int   n = p.mynr.inr;

    		os << n << " " << p.mystr << ((n==1)?(""):(p.mymult.c_str()));
    	} else {
    		unsigned int   n = p.mynr.uinr;

    		os << n << " " << p.mystr << ((n==1)?(""):(p.mymult.c_str()));
    	}
    	return os;
    }

    // first up, the unindent thingy
    unindent::unindent() {}

    // it only does something if it's inserted into a stream...
    ostream& operator<<( ostream& os, const unindent& ) {
    	indent::_indents.pop_back();
    	return os;
    }

    indent::indent() :
    	dopush( false ),
    	nr( 0 )
    {}

    indent::indent( unsigned int howmuch ) :
    	dopush( true ),
    	nr( howmuch )
    {}

    indent::indent( const indent& other ) :
    	dopush( other.dopush ),
    	nr( other.nr )
    {}

    const indent& indent::operator=( const indent& other ) {
    	if( this!=&other ) {
    		dopush = other.dopush;
    		nr     = other.nr;
    	}
    	return *this;
    }

    ostream& operator<<( ostream& os, const indent& i ) {
    	// decide what to do:
    	// if dopush == true -> we just push the value on the deque
    	//    (somebody inserts an indent object in a stream which
    	//     has come from a c'tor with an argument)
    	// if dopush == false -> we insert sum[deque.begin(), deque.end()]
    	//     thingys into the stream
    	if( i.dopush ) {
    		indent::_indents.push_back( i.nr );
    	} else {
    		unsigned int total;

            total = std::accumulate( indent::_indents.begin(),
                                     indent::_indents.end(),
                                     0 );
    		os << setw(total) << "";
    	}
    	return os;
    }


    // the printeffer
    _printeffer::_printeffer() {}

    _printeffer::_printeffer( const char* fmt, ... )
    {
    	char    tmp[2048];
    	va_list a;

    	va_start(a, fmt);
    	::vsnprintf(tmp, sizeof(tmp)-1, fmt, a);
    	va_end(a);
    	buf = std::string(tmp);
    }

    _printeffer::_printeffer( const _printeffer& other ) :
    	buf( other.buf )
    {}
    
    const _printeffer& _printeffer::operator=( const _printeffer& other ) {
    	if( this!=&other )
    		buf = other.buf;
    	return *this;
    }

    _printeffer::~_printeffer()
    {}

    ostream& operator<<( ostream& os, const _printeffer& p ) {
    	return os << p.buf;
    }
}
