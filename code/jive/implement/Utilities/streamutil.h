// some utilities for pretty printing on streams and other stuff
//
//# $Id: streamutil.h,v 1.4 2006/11/23 13:24:49 jive_cc Exp $
//
//# $Log: streamutil.h,v $
//# Revision 1.4  2006/11/23 13:24:49  jive_cc
//# HV: Beautified streamutil stuff
//#
//# Revision 1.3  2006/03/02 14:21:38  verkout
//# HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//#
//# Revision 1.2  2006/02/17 12:41:26  verkout
//# HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//#
//# Revision 1.1  2003/09/12 07:23:51  verkout
//# HV: Nifty streamutils for eg pretty printing of numbers, indenting.
//#
//
#ifndef STREAMUTIL_H
#define STREAMUTIL_H

#include <deque>
#include <string>
#include <iostream>

// grrrrrrr this also was taken from the PCInt codebase
// so if you decide to link both packages together, then you
// might be in trouble... well... let me tell you, we were!
// quick fix: put this thang in its own namespace so this
// version and the one found in PCInt are 'different'
namespace j2ms {

    //
    // pretty print struct: takes a number and a text, <n> and <str>.
    // when output on a stream it formats the stuff as follows:
    //    <n><space><str>[s]   where the character 's' will be added if
    // <n> != 1
    //
    // usefull for (pretty) printing number of items stuff...
    //
    struct pprint
    {
        public:
        	friend std::ostream& operator<<( std::ostream& os, const pprint& p );

        	pprint( int n, const std::string& str, const std::string& mult="s" );
        	pprint( unsigned int n, const std::string& str, const std::string& mult="s" );
	
        private:
        	union _mynr
        	{
        		int          inr;
        		unsigned int uinr;
        	};
        	bool                isint;
        	std::string         mystr;
        	std::string         mymult;
        	union pprint::_mynr mynr;

        	// prohibit these
        	pprint();
    };
    std::ostream& operator<<( std::ostream& os, const pprint& p );


    //
    // Indenter.
    // Usage:
    //
    // cout << "Listing:" << endl
    //      << indent(2) 
    //      << indent() << "some text" << endl
    //      << indent(3)
    //      << indent() << "text 1" << endl
    //      << indent() << "text 2" << endl
    //      << unindent()
    //      << indent() << "some other text" << endl
    //      << unindent()
    //      << indent() << "back to where we started" << endl;
    //
    // would produce:
    // Listing:
    //   some text
    //      text 1
    //      text 2
    //   some other text
    // back to where we started
    //
    struct unindent
    {
        public:
        	unindent();

        private:
        	unindent( const unindent& );
        	const unindent& operator=( const unindent& );
    };

    struct indent
    {
        public:
        	indent();
        	indent( unsigned int howmuch );
        
        	indent( const indent& other );
        	const indent& operator=( const indent& other );

        private:
            bool            dopush;
        	unsigned int    nr;

            // these must be friends, they must be able to 
            // access the static _indents member
        	friend std::ostream& operator<<( std::ostream&, const indent& );
        	friend std::ostream& operator<<( std::ostream&, const unindent& );

        	// static stuff
        	static std::deque<unsigned int>    _indents;
    };

    std::ostream& operator<<( std::ostream& os, const indent& i );
    std::ostream& operator<<( std::ostream& os, const unindent& u );

    // The printffer - can be inserted into a stream.
    // Allows you to do printf-like formatting on-the-fly!
    //
    // example:
    //
    // char    c = 'a';
    // float   f = 3.14159;
    // cout << printeffer("0x%02x [%c]\n  %4.2f", c, c, f);
    //
    // would print:
    // 0x65 [a]
    // 3.14
    //
    typedef struct _printeffer
    {
    	public:
    		_printeffer();
    		_printeffer( const char* fmt, ...);
    		_printeffer( const _printeffer& other );

    		const _printeffer& operator=( const _printeffer& other );


    		~_printeffer();
    	private:
    		std::string  buf;
		
    		friend std::ostream& operator<<( std::ostream&, const _printeffer& );
    } printeffer;

    std::ostream& operator<<( std::ostream& os, const printeffer& p );
}


#endif
