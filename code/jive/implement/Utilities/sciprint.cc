//
// implementation of the sciprint struct
//
// $Id: sciprint.cc,v 1.2 2006/02/17 12:41:26 verkout Exp $
//
// $Log: sciprint.cc,v $
// Revision 1.2  2006/02/17 12:41:26  verkout
// HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//
// Revision 1.1  2002/08/06 06:23:42  verkout
// HV: Some ez utilities that make formatting output's life easier. _very_ lightweight but functional.
//
//
#include <jive/Utilities/sciprint.h>


using std::string;

namespace j2ms {
    //
    // The array of prefixes we know of.
    // The two empty entries are essential! So if you
    // decide to add new known prefixes, be sure to keep 
    // the first empty entry where it is (between milli and kilo)
    // and keep the second empty entry as the last entry in the list.
    //
    const string prfx::_prfx[] = {
    	"atto",
    	"femto",
    	"p",
    	"n",
    	"u",
    	"m",
    	"",
    	"k",
    	"M",
    	"G",
    	"T",
    	"Exa",
    	""
    };

    //
    // static method to find the index where to start from,
    // the one with no prefix (ie. the first empty value)
    //
    int prfx::_findStartIndex( void )
    {
    	int                retval;
    	const std::string* sptr = j2ms::prfx::_prfx;

    	for( retval=0 ; sptr->size()!=0; retval++, sptr++ );
    	return retval;
    }

    //
    // Find the last index, ie. the index that is one less than
    // the second empty string...
    //
    int prfx::_findLastIndex( void )
    {
    	int                retval( j2ms::prfx::_findStartIndex()+1 );
    	const std::string* sptr( &j2ms::prfx::_prfx[retval] );

		
    	for( ; sptr->size(); retval++, sptr++ );
    	return retval;
    }
}
