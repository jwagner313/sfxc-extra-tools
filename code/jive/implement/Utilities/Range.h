//
//  Range -> class that transforms a string into an array of inetegers; usr. is 
//           able to specifiy range(s) or single numbers
//
//
//  Author:   Harro Verkouter  19-10-1998
//
//
//   $Id: Range.h,v 1.5 2006/03/02 14:21:38 verkout Exp $
//
#ifndef RANGE_H
#define RANGE_H


//
//  We need the string-class
//
#include <casa/BasicSL/String.h>
#include <list>

typedef std::list<int>  Range;

Range rangeFromString(const casa::String& s );


#endif
