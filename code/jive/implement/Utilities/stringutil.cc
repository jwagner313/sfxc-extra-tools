#include <jive/Utilities/stringutil.h>

using std::vector;
using std::string;

vector<string>  split( const string& str, char c )
{
	vector<string>    retval;
	string::size_type substart;
	string::size_type subend;

	substart = 0;
	subend   = str.size();

	while( 1 )
	{
		string            substr = str.substr( substart, subend );
		string::size_type ssubend;

		if( (ssubend=substr.find(c))==string::npos )
		{
			retval.push_back(substr);
			break;
		}
		else
		{
			string   tmp = substr.substr(0, ssubend);

			retval.push_back( tmp );
		}

		substart += (ssubend+1);
		subend    = str.size();
	}
	return retval;
}


