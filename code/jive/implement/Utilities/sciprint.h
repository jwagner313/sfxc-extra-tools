//
// sciprint - manipulator that pretty prints 
//            values + units in scientific notation.
//            Give it a value and a unit and
//            it will determine the prefix for the
//            unit:
//
//            e.g:
//
//            cout << sciprint(1.6193654E9, "Hz") << endl
//                 << sciprint(3345.356E-6, "m") << endl;
//
//            would print:
//			  1.6193654 GHz
//            3.345 um
//
// $Id: sciprint.h,v 1.2 2006/02/17 12:41:26 verkout Exp $
//
// $Log: sciprint.h,v $
// Revision 1.2  2006/02/17 12:41:26  verkout
// HV: * Resolved coredump-on-exit due to same objects being present in j2ms-code-tree and pcint-code-tree. Fix by putting the j2ms-ones in their own namespace * Changed implementation of FreqCounter from totally homegrown to totally STL-based * Added printfuntor functor, can be used in STL algorithms to print stuff
//
// Revision 1.1  2002/08/06 06:23:44  verkout
// HV: Some ez utilities that make formatting output's life easier. _very_ lightweight but functional.
//
//
#ifndef SCIPRINT_H
#define SCIPRINT_H

#include <string>
#include <iostream>

// grrrrrr (own fault...)
// same thingy exists in the PCInt stuff
// Before we actually linked casa and PCInt,
// it was nice to transfer functionality
// by copying files.. but now that they're
// physically linked together, this leads to
// trouble. So we just put this'un in the j2ms
// namespace to make it different from the PCInt
// one
namespace j2ms {

    struct prfx
    {
    	//
    	// The prefixes..
    	//
    	static const std::string _prfx[];

    	static int               _findStartIndex( void );
    	static int               _findLastIndex( void );
    };


    template <class T>
    struct sciprint
    {
        public:
        	sciprint( T val, const std::string& unit ) :
        		data( val ),
        		u( unit )
        	{
        	}

        	T           data;
        	std::string u;

	
        private:
        	//
            // prohibit default c'tor 
            //
        	sciprint();
    };


    template <class T>
    std::ostream& operator<<( std::ostream& os, const sciprint<T>& sp )
    {
    	int             idx( j2ms::prfx::_findStartIndex() );
    	T               tmp( sp.data );
    	static const T  _one( T(1.0) );
    	static const T  _thousand( T(1000.0) );

    	while( tmp && (tmp<_one || tmp>_thousand) )
    	{
    		if( tmp<_one )
    		{
    			tmp *= _thousand;
    			idx--;
    		}
    		else
    		{
    			tmp /= _thousand;
    			idx++;
    		}
    	}
    	if( idx<0 || idx>j2ms::prfx::_findLastIndex() )
    	{
    		os << sp.data << " " << sp.u;
    	}
    	else
    	{
    		os << tmp << " " << j2ms::prfx::_prfx[idx] << sp.u;
    	}
    	return os;
    }

    typedef sciprint<float>   sciprintf;
    typedef sciprint<double>  sciprintd;
}
#endif
