// To make sure only one definition of certain functions appears
//
// $Id: FreqCounter.cc,v 1.5 2006/03/02 14:21:38 verkout Exp $
//
// $Log: FreqCounter.cc,v $
// Revision 1.5  2006/03/02 14:21:38  verkout
// HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//
#include <jive/Utilities/FreqCounter.h>

namespace j2ms {

    using std::ostream;

    
    printer::printer( ostream& s, const char* d ):
        _s( s ),
        _d( d )
    {}


    
    ostream& operator<<( ostream& os,
                         const FreqCounter::value_type& v )
    {
        return os << v.first << ": " << v.second;
    }

    ostream& operator<<( ostream& os, 
                         const FreqCounter& f )
    {
        std::for_each( f.begin(), f.end(),
                       j2ms::printer(os, ", ") );
        return os;
    }
}
