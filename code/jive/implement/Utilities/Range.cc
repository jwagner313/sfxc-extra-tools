//
//  Implementation of the range class
//
//  Author:   Harro Verkouter,   19-10-1998
//
//
#include <jive/Utilities/Range.h>
#include <jive/Utilities/stringutil.h>
#include <casa/Exceptions.h>
#include <casa/Utilities/Regex.h>

#include <cstdlib>
#include <iostream>

using namespace casa;
using namespace std;

Range rangeFromString( const String& str ) {
    static const Regex  rangetype1( "[+-]?[0-9]+" );
    static const Regex  rangetype2( "[+-]?[0-9]+:[+-]?[0-9]+" );

    Range                    retval;
    vector<string>           parts;
    vector<string>::iterator curpart;

    // split the source string at ','
    parts = split( string(str), ',' );

    for( curpart=parts.begin();
         curpart!=parts.end();
         curpart++ ) {
        String    tmp(*curpart);

        // see if the substring matches anything
        if( tmp.matches(rangetype1) )
            retval.push_back( ::strtol(curpart->c_str(), 0, 0) );
        else if( tmp.matches(rangetype2) ) {
            int               s, e;
            bool              reverse;
            string            ss,es;
            string::size_type colon;

            colon = curpart->find(':');
            ss    = curpart->substr(0, colon);
            es    = curpart->substr(colon+1);

            s = (int)::strtol(ss.c_str(), 0, 0);
            e = (int)::strtol(es.c_str(), 0, 0);

            reverse = (e<s);
            for (int i=s; ((reverse)?(i>e):(i<e)); ((reverse)?(i--):(i++)))
                retval.push_back(i);
        } else {
            cerr << "rangeFromString(" << str << ")/One of the components "
                 << "[" << *curpart << "] is NOT recognized as a valid "
                 << "range-specifier (should be <number> or <number>:<number>)"
                 << endl;
            throw std::exception();
        }
    }
    return retval;
}
