// defines macro to simplify throwing
//
// $Id: texcept.h,v 1.1 2006/11/22 13:45:05 jive_cc Exp $
#ifndef UTILITIES_TEXCEPT_H
#define UTILITIES_TEXCEPT_H

#include <string>
#include <sstream>
#include <exception>

// the texcept exception, derived from std::exception
// so you can just catch std::exception& and catch these
// buggers as well...
class texcept:
    public std::exception
{
    public:
        texcept( const std::string& wot ):
            std::exception(),
            _wot( "tConvert: "+wot ) {}

        virtual const char* what( void ) const throw()
        {
            return _wot.c_str();
        }

        virtual ~texcept() throw() {}
    private:
        std::string  _wot;
};

//
//  Define the __HERE__ macro, using GNU-C extensions if available
//
#ifdef __GNUC__
#define __TEXCEPTHERE__ __FILE__ << ':' << _LcL_LiNEno << '/' << __PRETTY_FUNCTION__
#else
#define __JEXCEPTHERE__ __FILE__ << ':' << _LcL_LiNEno
#endif


// Throw an exception of type <excp>; use <strm> for the message. 
// Use this macro to insure that the  __HERE__ macro expands properly.
#define THROW_TEXCEPT(strm) \
{ \
  int                _LcL_LiNEno( __LINE__ );\
  std::ostringstream _LcL_oss_; \
  _LcL_oss_ << __TEXCEPTHERE__ << ':' << strm; \
  throw texcept(_LcL_oss_.str()); \
}


#endif
