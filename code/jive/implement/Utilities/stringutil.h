#ifndef STRINGUTIL_H
#define STRINGUTIL_H

#include <vector>
#include <string>

//
// Split a string into substrings at char 'c'
// Note: the result may contain empty strings, eg if more than one separation
// characters follow each other.
//
// E.g.
//    res = ::split( "aa,bb,,dd,", ',' )
//
// would yield the following array:
//   res[0] = "aa"
//   res[1] = "bb"
//   res[2] = ""
//   res[3] = "dd"
//   res[4] = ""
//
//
std::vector<std::string> split( const std::string& str, char c );

#endif
