#include <casa/BasicMath/Math.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/Slicer.h>
#include <casa/BasicSL/Complex.h>
#include <casa/Utilities/Assert.h>
#include <casa/Utilities/Regex.h>
#include <measures/Measures/Stokes.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSMainColumns.h>
#include <ms/MeasurementSets/MSDataDescColumns.h>
#include <ms/MeasurementSets/MSPolColumns.h>
#include <ms/MeasurementSets/MSSpWindowColumns.h>
#include <scimath/Functionals/Chebyshev.h>
#include <scimath/Mathematics/FFTServer.h>
#include <tables/Tables/TableIter.h>
#include <tables/Tables/TableParse.h>

using namespace casa;
using namespace std;

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>

Double pi = 2*asin(1.0);

void fail(string s)
{
     cerr << s << endl;
     exit(1);
}

vector<string> split(string s){
     vector<string> res;
     string::size_type i=0;
     while (i!=s.size()) {
	  while(isblank(s[i]) && i!=s.size()) {
	       i++;
	  }
	  string::size_type j=i;
	  while((!isblank(s[j])) && j!=s.size()) {
	       j++;
	  }
	  if (i!=j) {
	       res.push_back(s.substr(i,j-i)); 
	  }
	  i = j;
     }
     return res;
}



typedef vector<Double> Polycoeffs;



struct Polyset {
     Double start;
     Double end;
     vector<Polycoeffs> delay_coeffs;
     vector<Polycoeffs> phase_coeffs;
};

typedef map<String, vector<Polyset> > antenna_poly_t;
typedef map<String, antenna_poly_t> full_poly_t;

bool checky = False;


vector<Double> linfit2(Double t1, Double t2, vector<Double> a)
{

// alternate way of fitting line to poly over an integration
//      PlyDly = a[1] + a[2]*(t-t0) + a[3]*(t-t0)^2 + .... + a[6]*(t-t0)^5
// where t0 = start of the poly-set,
//       t1 = start of the integration period (counting from t0... thus <120s)
//       t2 =  end   "  "       "         "        "     "    "      "  <=120s)
// lply = b[1] + b[2]*(t-t0)

     vector<Double> b;
     b.resize(2);
     Double tmid = (t1+t2)/2.0;
     // Double tint = t2-t1;

     //compute poly delay at begin,midpoint,end of integ

     Double tijd[3] = {t1, tmid, t2};
     Double pdly[3];
     for (Int j=0; j<3; j++) {
	  pdly[j] = a[5];
	  for (Int i=4; i>=0; i--) {
	       pdly[j] = tijd[j]*pdly[j]+a[i];
	  }
     }
     // cout << "Poly: " ;
     // for (int f=0; f<6; f++) {
     // cout << a[f] << " ";
     // }
     // cout << endl;
     // cout << "t: " << tmid  << endl;
     //compute two kinds of slopes: connecting end points; tangent at
     //mid-point m1 := (p2-p1)/tint;
     Double m2 = 5*a[5];
     for (Int i=4; i>=1; i--) {
	  m2 = tijd[2]*m2 + i*a[i];
     }

     b[0] = pdly[2] - tijd[2]*m2;
     b[1] = m2;

     Double diff[3];
     for (Int i=0; i<3; i++) {
	  diff[i] = (b[0] + b[1]*tijd[i]) - pdly[i];
     }
     b[0] -= 0.63/3.0*(diff[0]+diff[1]+diff[2]);  //numerical factor to ~centroid area

     return b;
}

Double fbscalc (vector<Double> b, Double tint, Double lag, Double t1) {
//  "integrate over" linear model over (t1,t2) modulo the integral lags to
//  compute the "effective" uncompensated FBS -- here for single BW for this int

     Double t2 = t1 + tint; 
     Double absb2 = abs(b[1]);
     Double delt = absb2*tint/lag;      //span of delay in [lags] over int 
     Double l1 = (b[0] + b[1]*t1)/lag;  //delay in [lags] at begin of int
     Double f1 = l1 - floor(l1+0.5);    //same, WRT to closest intgral lag
     Double sgnf = f1/abs(f1);
     Double fbs, step0, x0, l2;
     Double f2, area0, delt1, delt2, x1, area1, x2; 

     if (b[1] == 0) {
	  fbs = f1;
     } else {
	  if (b[1] > 0) {    //assign initial step size depending on sgn(b[2],f1)
	       if (f1 > 0) {                // step0 as configured here >=0
		    step0 = 0.5 - f1;         // hence delt above -> abs(delt)
	       } else {
		    step0 = abs(f1);
	       }
	  } else {  //neg slope
	       if (f1 >= 0) {
		    step0 = f1;
	       } else {
		    step0 = 0.5 + f1;
	       } 
	  }
	  x0 = step0*lag/absb2;   //step0 in [lag] * [s/lag], absb2 in [s/s] 
	  Double increm = 0.0;
	  if (step0 > delt) { //case integ. doesn't pass through lag/2 boundary
	       step0 = delt;
	       x0 = tint;
	       l2 = (b[0] + b[1]*t2)/lag;
	       f2 = l2 - floor(l2+0.5);      //like f1, but at end of integ
	       increm = min(abs(f1), abs(f2) );  //for the rectangluar part of the area
	  }
	  area0 = step0*x0 / 2.0;
	  delt1 = delt - step0;
	  delt2 = delt1 - floor(delt1);   //0 < delt2 < 1  
	  if (delt2 > 0.5) {
	       x1 = 0.5*lag/absb2;
	       area1 = area0 - 0.5*x1/2.0; 
	       delt2 -= 0.5;
	       x2 = delt2*lag/absb2;
	       area1 += delt2*x2/2.0;
	  } else {
	       x1 = delt2*lag/absb2;
	       area1 = area0 - delt2*x1/2.0;
	  }
	  area1 += increm*tint;
	  fbs = sgnf*area1/tint;     //area in [lag.s];  fbs in [lag]

     } //slope=0 IF
     return fbs;
}



void process_jobs(Table TBL, Vector<String> jobs, Vector<String> ants, 
		  full_poly_t fp, double toff, double lag, int ndd0); 


vector<Polyset> load_poly(string f_name)
{
     vector<Polyset> res;
     ifstream polyfile(f_name.c_str());
     // add exception handler
     string l;
     vector<Polyset>::iterator ps;
     while (getline(polyfile, l) )
     {
	  String l2(l); // sigh.
	  if (l2.find(Regex("-+Logical_Station\\s*"))==String::npos){
	       fail(f_name + " isn't really a poly file.");
	  }
	  enum {phase, delay} mode;
	  while (getline(polyfile, l)) {
	       l2 = String(l);
	       Regex re_rots("begin_rot.+Sysclks : [^ \t]+.*end_rot:.*Sysclks : [^ \t]+");
	       if (l2.find(re_rots) != String::npos) {
		    ps = res.insert(res.end(), Polyset());
		    Regex re_float("[+-]?[0-9]+\\.[0-9]+e[+-][0-9]+");
		    vector<Double> rots;
		    vector<string> vs = split(l);
		    for (vector<string>::iterator i=vs.begin(); i!=vs.end(); i++) {
			 if (String(*i).find(re_float) != String::npos) {
			      Double d(atof(i->c_str()));
			      // cout << "** Text: " << *i << ", val: " << d << endl;
				   
			      rots.push_back(d);
			 }
		    }
		    ps->start = rots[0];
		    ps->end = rots[1];
	       } else if (l2.find("Delay polynomials")!=String::npos) {
		    mode = delay;
	       } else if (l2.find("Phase polynomials")!=String::npos) {
		    mode = phase;
	       } else if (l2.find(Regex("([+-]?[0-9]+\\.[0-9]+e[+-][0-9]+\\s*){6}"))!=String::npos) {
		    vector<string> vs = split(l);
		    Polycoeffs nums;
		    for (vector<string>::iterator i=vs.begin(); i!=vs.end(); i++) {
			 nums.push_back(Double(atof(i->c_str())));
		    }
		    if (mode==delay) {
			 ps->delay_coeffs.push_back(nums);
		    }
		    else {
			 ps->phase_coeffs.push_back(nums);
		    }
	       }
	  }
     }
     return res;
}

void check_poly_set(vector<Polyset> vp)
{
     for (vector<Polyset>::iterator i=vp.begin(); i!=vp.end(); i++) {
	  cout << "Begin: " << i->start << ", end: " << i->end << endl;
  	  for (vector<Polycoeffs>::iterator j=i->delay_coeffs.begin(); j!=i->delay_coeffs.end(); j++) {
  	       for (Polycoeffs::iterator k=j->begin(); k!=j->end(); k++) {
  		    cout << *k << ", ";
  	       }
  	       cout << endl;
  	  }
     }
}

full_poly_t load_polys(String path, Vector<String> jobs, Vector<String> ants)
{
     full_poly_t fpoly;
     for (Vector<String>::iterator jp=jobs.begin(); jp!=jobs.end(); jp++) {
	  antenna_poly_t apoly;
	  for (Vector<String>::iterator ap=ants.begin(); ap!=ants.end(); ap++) {
	       cout << "Loading polynomials for " << *jp << "::" << *ap << "..." << endl;
	       vector<Polyset> vp = load_poly(path+"/"+*jp+"/polys/"+*ap+".polys");
	       if (vp.size()>0) {
		    apoly[*ap] = vp;
	       } else {
		    cout << "... Job " << *jp 
			 << " has no polys for antenna " << *ap 
			 << endl;
	       }
	       // check_poly_set(vp);
	  }
	  fpoly[*jp] = apoly;
     }
     return fpoly;
}

Double beginning_of_year(Double tmin)
{
     Double secday = 24*60*60.0;
     Double jd = tmin/secday;
     // need to convert from modified Julian date; see
     // <http://tycho.usno.navy.mil/mjd.html>
     Time ti((double)jd + 2400000.5);
     cout << "Year: " << ti.year() << endl;
     Time t_tmp = Time(ti.year(), 1, 1);
     Double toff = t_tmp.modifiedJulianDay()*secday;
     return toff;
}

void setup(string path, string MSName) 
{
     // fixes FBS by computing 'integrated' fractional-bit delay over each
     //   integration, and applying a corresponding phase-slope across the
     //   band (assumed to pivot about the central [(N_frq+1)/2] frq.pt).
     // Delay poly (6th-order) truncated down to linear over integration,
     //   using own algo instead of one in rjc_code.c
     // Change looping to write back to TBL in bigger chunks less frequently

     string MS = path+"/"+MSName;
  
     Table TBL(MS, Table::Update);
     // Read the job info from MS
     cout << "Getting jobs from PROCESSOR table\n";
     Table PRCTBL;
     try {
	  PRCTBL = Table(MS + "/PROCESSOR");
     } catch(runtime_error e) {
	  fail("**** Problem reading PROCESSOR table ***");
     }
     ROScalarColumn<Int> jobid(PRCTBL, "PASS_ID");
     //  PASS_ID = integer; leading 0 for years xxx0 not used in
     //  JobID, thus direct conversion to string for ease in making
     //  path-names to find polys possible
     uInt njobs = jobid.nrow();
     if (njobs==0) {
	  fail("No jobs in MS");
     }
     vector<String> jobs;
     for (uInt i=0; i<jobid.nrow(); i++) {
	  ostringstream os;
	  os << jobid(i);
	  jobs.push_back(os.str());
     }

     // Get list of stations
     cout << "Getting antennas from ANTENNA table\n";
     Table ANTTBL = Table(MS + "/ANTENNA");
     Vector<String> ants = ROScalarColumn<String>(ANTTBL, "NAME").getColumn();
     cout << ants.size() << " rows in antenna table" << endl;

     // check order of baselines always low->high
     cout << "Checking antenna table" << endl;
     Table AntErrTable = TBL(TBL.col("ANTENNA1") > TBL.col("ANTENNA2"));
     if (AntErrTable.nrow() > 0) {
	  fail("Some baseline(s in high-low order -- stopping\n");
     }     

     //// check T_int the same throughout
     cout << "Checking exposure table..." << endl;
     Double texp0 = ROScalarColumn<Double>(TBL, "EXPOSURE")(0);
     Table TintErrTable = TBL(TBL.col("EXPOSURE") != texp0);
     if (TintErrTable.nrow() > 0) {
	  fail("Some integration time(s) differ --stopping\n");
     }
     // Get bandwidths of subbands
     cout << "Getting SB bandwidths from SPECTRAL_WINDOW table\n";
     Table SWTBL(MS + "/SPECTRAL_WINDOW");

     cout << "Checking bandwidths..." << endl;

     Double bw =  ROScalarColumn<Double>(SWTBL, "TOTAL_BANDWIDTH")(0); //in [Hz] not [MHz]
     Table BWErrTable = SWTBL(SWTBL.col("TOTAL_BANDWIDTH") != bw);
     if (BWErrTable.nrow()>0) {
	  fail("Some subbands have different BW -- stopping\n");
     }

     Double lag = 1.0/(2.0*bw);   //s  not [us]
     Int nfrq0 = ROScalarColumn<Int>(SWTBL, "NUM_CHAN")(0);
     cout << "Checking subbands..." << endl;
     Table SBErrTable = SWTBL(SWTBL.col("NUM_CHAN")!=nfrq0);
     if (SBErrTable.nrow() > 0) {
	  fail("Some subbands have different N_frq -- stopping\n");
     } 
     Int ndd0 = SWTBL.nrow();
     Vector<Double> Tt = ROScalarColumn<Double>(TBL, "TIME").getColumn();
     Double tmin = min(Tt);
     Double toff = beginning_of_year(tmin);
     full_poly_t fp = load_polys(path, jobs, ants);
     process_jobs(TBL, jobs, ants, fp, toff, lag, ndd0);
}


void process_datum(Array<Complex>& Tdat, Int nfrq, Int npol, Int fp0, Double fbs)
{
     // this function at least means that we know what's used in the
     // actual calculation.

     Double phsslp = fbs*pi/nfrq;


     for (int ipol=0; ipol<npol; ipol++) {
	  for (int ifrq=0; ifrq<nfrq; ifrq++) {
	       IPosition p(2, ipol, ifrq);
	       Float phs0 = arg(Tdat(p));
	       Float amp = abs(Tdat(p));
	       Double delphs = (ifrq+1.0-fp0)*phsslp;
	       Float phs1 = phs0 - delphs;
	       Complex d1 = polar(amp, phs1);
	       Tdat(p) = d1;
	  }
     }
}

Int get_ply_ind(const vector<Polyset>& poly1, const vector<Polyset>& poly2, Double t) 
{
     Double end0 = -1;
     uInt k;
     for (k=0; k<poly1.size(); k++) {
	  Double begin = poly1[k].start/32.e6;
	  Double end = poly1[k].end/32.e6;
	  if (begin != poly2[k].start/32.e6 ||
	      end != poly2[k].end/32.e6) {
	       fail("start and end times don't match.");
	  }
	  Double realbegin=max(begin, end0);
	  if ((realbegin < t) && (t < end)) break;
	  end0 = end;
     }
     if (k==poly1.size()) {
	  fail("Out of range of polynomials");
     }
     return k;
}

void process_jobs(Table TBL, Vector<String> jobs, Vector<String> ants, 
		  full_poly_t fp, double toff, double lag,  int ndd0) 
{

     // all integration times are the same, so:
     Double t_int = ROScalarColumn<Double>(TBL, "EXPOSURE")(0);

     ROScalarColumn<Int> jid_c(TBL, "PROCESSOR_ID");
     ROScalarColumn<Int> ant1_c(TBL, "ANTENNA1");
     ROScalarColumn<Int> ant2_c(TBL, "ANTENNA2");
     ROScalarColumn<Double> time_c(TBL, "TIME");
     ArrayColumn<Complex> data_c(TBL, "DATA");

     Vector<Int> jids = jid_c.getColumn();
     Vector<Int> ant1s = ant1_c.getColumn();
     Vector<Int> ant2s = ant2_c.getColumn();
     Vector<Double> times = time_c.getColumn();
      
     IPosition shape = data_c.shape(0); // all rows the same
     Int npol = shape(0);
     Int nfrq = shape(1);
     Int fp0 = nfrq/2+1;
     
     for (uInt ind=0; ind<TBL.nrow(); ind++) { //TBL.nrow()
	  if ((ind % 10000)==0) {
	       cout << "Processing row " << ind << endl;
	  }	
	  Int jid = jids[ind];
	  const String& jobid = jobs[jid];
	  const Int i = ant1s[ind];
	  const Int j = ant2s[ind];
	  const String& ant1=ants[i];
	  const String& ant2=ants[j];
	  if (i==j) {
	       continue;
	  }
	  const Double t = times[ind]-toff;
	  const vector<Polyset>& poly1 = fp[jobid][ant1];
	  const vector<Polyset>& poly2 = fp[jobid][ant2];
	  if (poly1.size() != poly2.size()) {
	       fail("Polys for "+ants[i]+" and "+ants[j]
		    +" inconsistent size.");
	  }
	  Int k = get_ply_ind(poly1, poly2, t);
	  const Polycoeffs& pc1 = poly1[k].delay_coeffs[0];
	  const Polycoeffs& pc2 = poly2[k].delay_coeffs[0];
	  Polycoeffs bslnply;
  	  for (uInt l=0; l<pc1.size(); l++) {
  	       bslnply.push_back(pc1[l]-pc2[l]);
  	  }

	  Double begin = poly1[k].start/32.e6;
	  Double t_interp = t - begin;
	  Array<Complex> Tdat = data_c(ind);
	  
	  Double t1 = t_interp-0.5*t_int;
	  Double t2 = t1 + t_int;
	  vector<Double> b = linfit2(t1, t2, bslnply);
	  Double fbs = fbscalc(b, t_int, lag, t1);
	  process_datum(Tdat, nfrq, npol, fp0, fbs);
	  data_c.put(ind, Tdat);
     }
     TBL.flush();
}


pair<string, string> split_msname(string mspath)
{
     for (string::iterator it=mspath.end()-1;  it!=mspath.begin()-1; it--) {
	  if (*it=='/') {
	       return pair<string, string>(string(mspath.begin(), it),
					   string(it+1, mspath.end()));

	  }
     }
     throw runtime_error("No slash in function");
}



int main(int argc, char** argv)
{
     if (argc!=2) {
	  cerr << "Bad argument: " << argv[1] << endl;
	  exit(1);
     }
     string mspath = string(argv[1]);
     pair<string, string> p=split_msname(mspath);
     cout << "path: " << p.first << "\n" << "ms: " << p.second << endl;
     setup(p.first, p.second);

     return 0;
}

