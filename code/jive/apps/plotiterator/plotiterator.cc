#include <iostream>
#include <casa/aips.h>
#include <tables/Tables.h>
#include <tables/Tables/TableIter.h>
#include <casa/Exceptions.h>
#include <casa/Containers/Block.h>
#include <casa/Utilities/Copy.cc>
#include <casa/Arrays/ArrayUtil.h>
#include <casa/Arrays/ArrayLogical.h>

#include <tasking/Glish.h>
#include <casa/Containers/BlockIO.cc>
#include <casa/BasicSL/String.h>
#include <casa/Exceptions/Error.h>
#include <casa/OS/Memory.h>
#include <casa/Arrays/IPosition.h>
#include <ms/MeasurementSets/MeasurementSet.h>

#include <jive/Plotters/PlotterIF.h>


#include <sys/types.h>
#include <time.h>

#include <sstream>

#ifndef BUILDTIME
#define BUILDTIME "<no buildtime info available>"
#endif

// Dump record enables us to dump the contents of a record on a stream
#include <jive/ms2uvfitsimpl/DumpRecord.h>

using namespace std;
using namespace casa;

//
// drEn == dumb return EventName
//
String drEN( const GlishSysEvent& event )
{
    return event.type() + "_result";
}

//
//  Prototype
//
Bool makePlots( GlishSysEvent& , GlishSysEventSource& , String& );


int main( int argc, char**argv )
{
    String              message;
    GlishSysEvent       event;
    GlishSysEventSource eventStream(argc, argv);


    while( eventStream.connected() )
    {
		Bool ok = True;
		try
		{
			// block until next event
			event = eventStream.nextGlishEvent();
	    
			// There is an event.
			message = "";

			if( event.type()=="makeplots" )
			{
				ok = ::makePlots( event, eventStream, message );
			}
			else if( event.type()=="buildtime" )
			{
				ok = True;
				eventStream.postEvent( drEN(event), BUILDTIME );
			}
			else if( event.type()!="terminate" )
			{
				eventStream.unrecognized();
			}
		
		}
		catch( const AipsError& ae )
		{
			cout << "*** ARGH - " << ae.getMesg() << endl;
			message = ae.getMesg();
			ok = False;
		}
		if( !ok )
		{
			GlishRecord rec;
			rec.add ("error_message", event.type() + ": " + message);
			eventStream.postEvent( drEN(event), rec );
		}
	}
	return 0;
}




Bool makePlots( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
	ostringstream 			 dbgstrm;
	ostringstream            plotnamestrm;
	ostringstream            dsnamestrm;
	GlishArray::ElementType  gINT    = GlishArray::INT;
	GlishArray::ElementType  gBOOL   = GlishArray::BOOL;
	GlishArray::ElementType  gFLOAT  = GlishArray::FLOAT;
	GlishArray::ElementType  gSTRING = GlishArray::STRING;

	//
	//  Local automatic variables
	//
	uInt         i;
	uInt         nparam;
    Bool         retval( False );
  
	if( ev.val().type()!=GlishValue::RECORD )
	{
		dbgstrm.str( string() );
		dbgstrm << "makePlots: EINVAL! Expected a record!";
		msg = dbgstrm.str();
		return retval;
	}
 
	//
	//  Ok. A lengthy task may lay ahead of us.. producing the plots!
	//  Obviously, the first thing to check is the paramers that we have
	//  been passed from within glish....
	//
 	GlishRecord  params( ev.val() );
	
	evsrc.postEvent( "debug", "Start testing parameters...." );

	//
	//  We expect a record with a number of entries
	//
	//  1) a String - the tablename we're supposed to operate upon
	//  2) a String - the TaQL statement, describing the selection 
	//  3) a String - the name of the weight column OR a boolean - no weights
	//  4) a String - the name of the plotter
	//  5) an array of integers - the desired correlations (AIPS++ internal
	//                            enumeration values)
	//  6) an array of integers - the channel selection
	//  7) an array of booleans - the 'newplot' setting
	//  8) a String - the desired time averaging    -|
	//  9) a String - the desired channel averaging -|=> See PlotterIF.h for details
	// 10) a Float  - the weightthreshold: points with weights below this are not
	//     taken into account....
	//
	nparam = 10;
	if( params.nelements()!=nparam )
	{
		dbgstrm.str( string() );
		dbgstrm << "makePlots: EINVAL! Record does not contain " << nparam 
				<< " elements!";
		msg = dbgstrm.str();
		return retval;
	}

	//
	//  Verify types (and cache the resulting values!)
	//
	Float         weightthreshold;
	String        strarr[6];
	Vector<Int>   viarr[2];
	Vector<Bool>  vbool;

	for( i=0; i<nparam; i++ )
	{
		GlishValue  cur = params.get(i);

		dbgstrm.str( string() );
		dbgstrm << "Testing parameter " << i;
		evsrc.postEvent( "debug", dbgstrm.str() );	

		if( cur.type()!=GlishValue::ARRAY )
		{
			break;
		}
		
		Bool                    ok;
		GlishArray              gar( cur );
		uInt                    nel  = cur.nelements();
		uInt                    ndim = gar.shape().nelements();
		GlishArray::ElementType tp   = gar.elementType();

		ok = False;

		//
		//  Name the conditions under which we accept the value under consideration
		//
		switch( i )
		{
			case 0:
			case 1:
			case 3:
				//	
				// For these parameters we only accept single string-values
				//
				if( ok=(tp==gSTRING && nel==1 && ndim==1)==True )
				{
					gar.get( strarr[i] );
				}
				break;

			case 2:
				//
				//  This may be a boolean or a string...
				//
				if( (ok=((tp==gSTRING || tp==gBOOL) && nel==1 && ndim==1))==True && 
					tp==gSTRING )
				{
					gar.get( strarr[i] );
				}
				break;

			case 4:
			case 5:
				//
				//  For these parameters we accept arrays of integers...
				//  More strictly... vectors even!
				//
				if( (ok=(tp==gINT && nel>0 && ndim==1))==True )
				{
					gar.get( viarr[i-4] );
				}
				break;
			
			case 6:
				//
				//  Array of booleans corresponding to the axes in PlotterIF.h
				//  Used to determine the iterator axes and the plot/dataset
				//  labels
				//
				if( (ok=(tp==gBOOL && ndim==1 && nel==(uInt)PlotterIF::NUMAXES))==True )
				{
					gar.get( vbool );
				}
				break;

			case 7:
			case 8:
				//
				//  Time/channel averaging settings
				//
				//  Only accept single strings
				//
				//  stuff result into strarr[4] and strarr[5]
				//
				if( (ok=(tp==gSTRING && nel==1 && ndim==1))==True )
				{
					gar.get( strarr[i-3] );
				}
				break;

			case 9:
				//
				//  Weight threshold setting
				//
				if( (ok=(tp==gFLOAT && nel==1 && ndim==1))==True )
				{
					gar.get( weightthreshold );
				}
				break;
				
			default:
				break;
		}

		if( !ok )
		{
			dbgstrm.str( string() );
			dbgstrm << "Parameter " << i << " failed the test!";
			evsrc.postEvent( "debug", dbgstrm.str() );	
			break;
		}
	}
	
	if( i<nparam )
	{
		dbgstrm.str( string() );
		dbgstrm << "makePlots: EINVAL! Arguments were not according to expectations - " 
				<< "we expect:\n"
				<< "tableName <string>\n"
				<< "taqlString <string>\n"
				<< "weightcolName <string>\n"
				<< "plotterName <string>\n"
				<< "polSelection <vector of int>\n"
				<< "chanSelection <vector of int>\n"
				<< "newplot <vector of bool>\n"
				<< "timeAvg <string>\n"
				<< "chanAvg <string>\n"
				<< "weightThreshold <float>\n";
		msg = dbgstrm.str();
		return retval;
	}

	//
	//  Create 'aliases' for the parameters
	//
	const String&        tableName( strarr[0] );
	const String&        taqlString( strarr[1] );
	const String&        weightcolName( strarr[2] );
	const String&        plotterName( strarr[3] );
	const String&        timeAvgStr( strarr[4] );
	const String&        chanAvgStr( strarr[5] );
	const Vector<Int>&   polSelection( viarr[0] );
	const Vector<Int>&   chanSelection( viarr[1] );
	const Vector<Bool>&  newplot( vbool );	
	PlotterIF::AvgMethod timeAvg = PlotterIF::stringToAvg( timeAvgStr );
	PlotterIF::AvgMethod chanAvg = PlotterIF::stringToAvg( chanAvgStr );

	//
	//  Attempt to create the plotter
	//
	const PlotterIF*          thePlotter = PlotterIF::makePlotter( plotterName );

	if( !thePlotter )
	{
		dbgstrm.str( string() );
		dbgstrm << "makePlots: Failed to create plotter `" << plotterName << "'.\n"
				<< "Are you sure this type exists?";
		msg = dbgstrm.str();
		return retval;
	}

	Bool                   doSort;
	Vector<Bool>           iterCols( (uInt)PlotterIF::NUMAXES );
	Vector<Bool>           plotLabel( (uInt)PlotterIF::NUMAXES );
	Vector<Bool>           dataLabel( (uInt)PlotterIF::NUMAXES );
	Block<PlotterIF::Axis> bp;

	//
	//  Start off with a default label/iterator-setting
	//  1) By default, we iterate over all axes
	//  2) the indices mentioned in 'newplot' tell us when to
	//     start a new plot
	//  3) All other indices are dataset indices... 
	//
	iterCols  = True;
	plotLabel = newplot;
	dataLabel = !newplot;

	//
	//  Query the plot 
	//
	bp = thePlotter->bypassAxes();

	//
	//  Basically, if the plotter does not want to bypass
	//  one or more axes, they mean they do not care in what
	//  order the data come in.....
	//
	doSort = ( bp.nelements()!=0 );

	for( uInt i=0; i<bp.nelements(); i++ )
	{
		if( bp[i]==PlotterIF::NONE )
		{
			continue;
		}
		//
		//  Set this one to False in the iterator array...
		//
		iterCols( (uInt)bp[i] ) = False;
	}

	//
	//
	//  It's about time we started to open the table, do the selection
	//  read some de-mapping info,
	//  create the iterator 
	//
	//
	Table     maintable;
	Table     selection;

	try
	{
		maintable = Table( tableName, Table::Old );
	}
	catch( AipsError ae )
	{
		dbgstrm.str( string() );
		dbgstrm << "makePlots: Failed to open table `" << tableName << "'.\n"
				<< "Caught an exception: " << ae.getMesg();
		msg = dbgstrm.str();
		return retval;
	}

	//
	//  Attempt to do the selection...
	//
	try
	{
		selection = ::tableCommand( "SELECT FROM $1 WHERE "+taqlString, maintable );
	}
	catch( AipsError ae )
	{
		dbgstrm.str( string() );
		dbgstrm << "makePlots: Failed to do query!\n"
				<< "Caught an exception: " << ae.getMesg();
		msg = dbgstrm.str();
		return retval;
	}
	//
	// Determine the datacolumnname
	//
	const String         datacolname( ::dataColumnName(selection) );

	//
	//  Test if there's something to do anyway...
	//
	if( !selection.nrow() )
	{
		dbgstrm.str( string() );
		dbgstrm << "makePlots: Selection is empty!";
		msg = dbgstrm.str();
		return retval;
	}	

	//
	//  Create itername-block. Note: we are compiling the array of 
	//  column-names that the tableiterator needs to have in order such that
	//  it knows which columns to iterate over.
	//
	//  As such we skip the Polarization/Channel axes anyway since the
	//  the tableiterator does not iterate over those; we do that ourselves...
	//
	PlotterIF::Axis curax;
	PlotterIF::Axis sortOrder[PlotterIF::DATAAXES] = {
						PlotterIF::P, PlotterIF::CH, PlotterIF::BL,
						PlotterIF::SB, PlotterIF::FQ, PlotterIF::SRC,
						PlotterIF::TIME };
	Block<String>   iterNames;
	Block<String>   sortNames;

	for( uInt ax=0; ax<PlotterIF::NUMAXES; ax++ )
	{
		curax = sortOrder[ax];

		//
		//  Test if we need to skip this column for iteration
		//  Either it has been set to bypass by the Plotter or
		//  we do not iterate over it anyway.
		//
//		if( !iterCols((uInt)curax) || 
		
		if(	(curax==PlotterIF::P) ||
			(curax==PlotterIF::CH) || 
			(curax>=PlotterIF::DATAAXES) )
		{
			continue;
		}
		
		//
		//  Add the current axis to the block of iteration-columns
		//  or to the sort-colums....
		//  If the axis under consideration is flagged as False in the iterCols
		//  array this one gets added to the sortColumns.
		//
		//  Note: we determine that already at this point since we may need
		//        the correct block (see the processing of the PlotterIF::SB and
		//        the PlotterIF::FQ in the switch statement below)
		//
		//Block<String>&   bref( ((iterCols((uInt)curax)==True)?(iterNames):(sortNames)) );

		uInt     nr( 2 );
		//Bool     addToSort( True );
		Bool     addToSort( (iterCols((uInt)curax)==True) );
		Bool     addToIter( (iterCols((uInt)curax)==True) );
		String   colname[nr];
		
	
		switch( curax )
		{
			case PlotterIF::SB:
			case PlotterIF::FQ:
				{
					//
					//  These basically are equal; they translate into different
					//  DATA_DESC_ID... Check if we do not already have DAAT_DESC_ID...
					//
					uInt     chk;

					colname[0] = "DATA_DESC_ID";
					for( chk=0; addToIter && chk<iterNames.nelements(); chk++ )
					{
						if( iterNames[chk]==colname[0] )
						{
							// Already present: prevent addition...
							//colname[0] = "";
							addToIter = False;
							break;
						}
					}
					for( chk=0; addToSort && chk<sortNames.nelements(); chk++ )
					{
						if( sortNames[chk]==colname[0] )
						{
							// Already present: prevent addition...
							//colname[0] = "";
							addToSort = False;
							break;
						}
					}
				}		
				break;

			case PlotterIF::SRC:
				colname[0] = "FIELD_ID";
				break;

			case PlotterIF::BL:
				colname[0] = "ANTENNA1";
				colname[1] = "ANTENNA2";
				break;

			case PlotterIF::TIME:
				colname[0] = "TIME";
				break;

			default:
				break;
		}

		for( uInt tmp=0; tmp<nr; tmp++ )
		{
			if( colname[tmp]=="" )
			{
				continue;
			}
			uInt    ival;

			if( addToSort )
			{
				ival = sortNames.nelements();

				sortNames.resize( ival+1 );
				sortNames[ival] = colname[tmp];
			}
			if( addToIter )
			{
				ival = iterNames.nelements();

				iterNames.resize( ival+1 );
				iterNames[ival] = colname[tmp];
			}
		}
	}


	//for( uInt tmpi=0; tmpi<iterNames.nelements(); tmpi++ )
	//{
	//	cout << "Itername(" << tmpi << ")=" << iterNames[tmpi] << endl;
	//}
	//for( uInt tmpi=0; tmpi<sortNames.nelements(); tmpi++ )
	//{
	//	cout << "Sortname(" << tmpi << ")=" << sortNames[tmpi] << endl;
	//}

	//
	//  As of this point we know what axes to iterate over and hence we can 
	//  fully deduce (together with the settings from the 'newplot'-array):
	//
	//  1) Which labels (axis-loci) make up the plot-identification
	//  2) which labels make up the dataset-index
	//
	//  We can now finalize it by making sure that the iterator-axes
	//  are not used for plot-label generation
	//  
	for( Int cnt=0; cnt<PlotterIF::NUMAXES; cnt++ )
	{
		plotLabel(cnt) = plotLabel(cnt) && iterCols(cnt);
	}

	dataLabel = !plotLabel && !iterCols;

	//
	//  Step 1) We'll start by sorting the selection
	//          that is, if there are sortcolumns
	//
	Table     selectionaftersort = selection;

	//if( doSort && sortNames.nelements() )
	if( sortNames.nelements() )
	{
		//
		// Notify our glish client that we're about to start sorting
		// some stuff....
		//
		dbgstrm.str( string() );
		dbgstrm << "Start sorting your selection, which contains " 
				<< selection.nrow() << " rows.";
		evsrc.postEvent( "message", dbgstrm.str() );

		if( selection.nrow()>250000 )
		{
			dbgstrm.str( string() );
			dbgstrm << "This may take a while...";
			evsrc.postEvent( "message", dbgstrm.str() );
		}
		selectionaftersort = selection.sort( sortNames, Sort::Ascending );
	}

	//
	//  Notify the glish thingy that we're about to do something
	//
	dbgstrm.str( string() );
	dbgstrm << "Creating iterator (just hold on)...";
	evsrc.postEvent( "message", dbgstrm.str() );	
		
	//
	//  So now we can create the tableiterator...
	//
	Table           itertable;
	time_t          starttime,endtime;
	Record          rvrec;
	Record          pr;
	TableIterator   iter( selectionaftersort, iterNames,
						  TableIterator::DontCare, TableIterator::NoSort );

	//
	//  Notify the glish thingy that we're about to do something
	//
	dbgstrm.str( string() );
	dbgstrm << "Starting plots on table with " << selectionaftersort.nrow() << " rows.";
	evsrc.postEvent( "message", dbgstrm.str() );	
		

	//
	//  Arrays for storage
	//
	uLong                           nrSkipped;
	uLong                           nrTotal;
	uLong                           nrTodo;
	uLong                           percentage;
	uLong                           nrIter;
	Double                          avgStepSize;
	Double                          remain;
	//
	//  Prevent constructor calls of these objects; they
	//  appear in the inner loop when processing the results
	//  returned from the plotter.
	//
	RecordFieldPtr<String>          dstcomment;
	RecordFieldPtr<String>          srccomment;
	RecordFieldPtr<Array<Double> >  xptr;
	RecordFieldPtr<Array<Double> >  yptr;
	RecordFieldPtr<Array<Double> >  dsxptr;
	RecordFieldPtr<Array<Double> >  dsyptr;

	nrSkipped  = 0L;
	nrTotal    = 0L;
	nrIter     = 0L;
	nrTodo     = selectionaftersort.nrow();
	percentage = nrTodo/10L;


	//
	//  Save the start time for later use
	//
	starttime = ::time( (time_t*)0 );
	while( !iter.pastEnd() )
	{
		itertable = iter.table();

		nrTotal  += itertable.nrow();
		nrIter++;
	
		//if( nrIter>10 )
		//{
		//	break;
		//}
	
		avgStepSize = (Double)nrTotal/(Double)nrIter;

		//
		// 'smart' progress counter: try to print only one
		//   message per checkpoint. With rounding+unknown stepsize this
		//   is not trivial: since we're using the tableiterator we cannot
		//   be sure each iteration step has the same size.....
		//
		if( nrTodo>20000 )
		{
			remain = ::fmod( (Double)nrTotal, (Double)percentage );

			if( ::fabs(remain)<avgStepSize )
			{
				dbgstrm.str( string() );
				dbgstrm << "Done " << ((Double)nrTotal/(Double)nrTodo)*100.0 
						<< " percent...";
				evsrc.postEvent( "message", dbgstrm.str() );
			}
		}
		//
		//  Call plotroutine
		//
		pr = thePlotter->plotResult( itertable, weightthreshold, weightcolName, 
									 datacolname, newplot, 
									 polSelection, chanSelection, timeAvg, chanAvg );

		//
		//  Continue as soon as possible if nothing to do
		//
		if( pr.nfields()==0 )
		{
			nrSkipped += itertable.nrow();
			iter.next();
			continue;
		}

		//
		// Loop over all plots that have been created by the plotfunction
		//
		for( uInt fld=0; fld<pr.nfields(); fld++ )
		{
			Record&    res( pr.rwSubRecord(fld) );
			String     plotname( pr.name(fld) );
			
			//
			//  Check for existence (and if necessary create) PLOT-subrecord
			//
			//
			if( !rvrec.isDefined(plotname) )
			{
				rvrec.defineRecord( plotname, Record() );	
			}
			Record&    dstplot( rvrec.rwSubRecord(plotname) );
			
			//
			//  Now loop over all datasets
			//
			for( uInt ds=0; ds<res.nfields(); ds++ )
			{
				Record&  curds( res.rwSubRecord(ds) );
				String   dsname( res.name(ds) );

				if( !dstplot.isDefined(dsname) )
				{
					dstplot.defineRecord( dsname, Record(::makePlotResultLayout()) );
				}

				Record&                         dstds( dstplot.rwSubRecord(dsname) );

				//
				// Attach FieldPtrs to the corrct records
				//
				dstcomment.attachToRecord( dstds, "comment" );
				srccomment.attachToRecord( curds, "comment" );

				xptr.attachToRecord( dstds, "xval" );
				yptr.attachToRecord( dstds, "yval" );

				dsxptr.attachToRecord( curds, "xval" );
				dsyptr.attachToRecord( curds, "yval" );
				
				//
				//  Concatenate any existing data with new data
				//
				Array<Double>  tmp1(::concatenateArray(*xptr, *dsxptr));
				Array<Double>  tmp2(::concatenateArray(*yptr, *dsyptr));

				//
				//  Finally update contents of destrecord
				//
				(*xptr).reference( tmp1 );
				(*yptr).reference( tmp2 );

				if( (*dstcomment)!=(*srccomment) )
				{
					(*dstcomment) = (*dstcomment)+(*srccomment);
				}
			}
		}
		//
		//  Move on to next???
		//
		iter.next();
	}
	endtime = ::time( (time_t*)0 );	


	//
	// Plot the yvalues?
	//
	//for( uInt fld=0; fld<rvrec.nfields(); fld++ )
	//{
	//	Record&    res( rvrec.rwSubRecord(fld) );
	//	String     plotname( pr.name(fld) );
	//	cout << "* Found " << plotname << endl;	
		//
		//  Now loop over all datasets
		//
	//	for( uInt ds=0; ds<res.nfields(); ds++ )
	//	{
	//		Record&  curds( res.rwSubRecord(ds) );
	//		String   dsname( res.name(ds) );
	//		yptr.attachToRecord( curds, "yval" );
	//		cout << "  contains " << dsname << endl
	//			 << "  " << *yptr << endl;
	//	}
	//}

	//
	//  Post the return value?
	//
	GlishRecord    retrec;
	GlishRecord    statsrec;

	//
	//  Compile some statistics for those interested
	//
	statsrec.add("RowsPerMinute", ((Double)nrTotal/(Double)(endtime-starttime))*60.0 );
	statsrec.add("TotalNrRows", (Double)nrTotal );
	statsrec.add("NrSkippedRows", (Double)nrSkipped );
	statsrec.add("TotalRunTimeInSeconds", (Double)(endtime-starttime) );
	
	//
	//  Form the return value record...
	//
	retrec.fromRecord(rvrec);
	retrec.addAttribute( "Statistics", statsrec );

    evsrc.postEvent( drEN(ev), retrec );
    return True;    
}

