//  Translate a raw correlator dump into a MS
//
//
//  $Id: j2ms2.cc,v 1.15 2011/10/12 12:45:22 jive_cc Exp $
//
//  $Log: j2ms2.cc,v $
//  Revision 1.15  2011/10/12 12:45:22  jive_cc
//  HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
//        changed into 'labels' for more genericity
//      * can now be built either as part of AIPS++/CASA [using makefile]
//        or can be built as standalone set of libs + binaries and linking
//        against casacore [using Makefile]
//        check 'code/
//
//  Revision 1.14  2007-05-25 12:00:01  jive_cc
//  HV: - switched to using casa::Regex
//      - filter/ghostbuster is now on by default
//
//  Revision 1.13  2007/03/01 14:29:16  jive_cc
//  HV: mainly cosmetic changes, made sure stuff is better to read,
//      removed unused code
//
//  Revision 1.12  2006/03/02 14:21:36  verkout
//  HV: * Removed remaining instances of strstream, replaced with strstream * Got rid of (home grown) Container class!!!! YES! Finally replaced with STLs vector/list, whichever is appropriate * Added pure virtual method to hduGenerator - class writers MUST return an ID which (uniquely) identifies their specific Generator * mapping of generator-ID to actual Generator is now built automagically rather than by ms2uvfitsConverter, class writers should look in Registrar.h for details
//
//  Revision 1.11  2006/02/10 08:59:11  verkout
//  HV: * Removed dependency on libExceptionObject and libConnection * Removed Utilities/AIPSExceptionObject.h * Switched to throwing/catching of exceptions derived from std::exception * Removed strstream (maybe some still left) * Removed conditional includes (Scott Meyers Effective C++ trick) because it... well.. sucks * Start support for PCInt datafiles * Removed packags RAW and replaced it with JCCS
//
//  Revision 1.10  2006/01/13 12:59:55  verkout
//  HV: CASAfied version of the implement
//
//  Revision 1.9  2005/03/15 15:01:04  verkout
//  HV: Added scan-selection possibility for *.lis files. -v reads the first/last scan to process from the 4th and 5th column from the lis file. -V is compatibility mode: in this case the scanIDs from column 4/5 are not used; j2ms2 just translates everything
//
//  Revision 1.8  2003/02/14 15:48:43  verkout
//  HV:   std::string does not have automatic conversion to
//        (const char*). Fits-stuff does need (const char*)
//        so had to add the odd .c_str() here and there.
//
//  Revision 1.7  2002/11/15 08:46:10  verkout
//  HV: Added pointer to j2ms2 'homepage'.
//
//  Revision 1.6  2002/08/06 09:26:30  verkout
//  HV: * Added support for Huibs streamlining .lis file
//  * Sort of dropped support for Albert's format of data
//  * Added support for passing options to experiment and/or filler
//  * Make use of STL stuff, like vector/string rather than char**
//
//  Revision 1.5  2001/07/09 07:55:55  verkout
//  HV: Some mods
//  - Enabled passing of options from j2ms2 commandline to an experiment
//  - Enables passing of options from j2ms2 commandline to the JIVEMSFiller
//  - Needed more libs to link correctly
//
//  Revision 1.4  2001/05/30 11:55:58  verkout
//  HV: Made changes to accomodate MSv2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
//  Revision 1.3  2000/03/28 07:39:13  verkout
//  HV: By popular demand: added the cmd-line opetion -f to j2ms2 = read buffer specifications from a file.
//
#include <casa/aips.h>
#include <casa/Utilities/Regex.h>

#include <jive/Utilities/jexcept.h>
#include <jive/j2ms/Visibility.h>
#include <jive/labels/CorrelationCode.h>
#include <jive/j2ms/Scan.h>
#include <jive/j2ms/VEXperiment.h>
#include <jive/j2ms/JIVEMSFiller.h>
#include <jive/j2ms/subjob.h>

#include <iostream>
#include <sstream>
#include <fstream>

#include <signal.h>
#include <stdlib.h>
#include <libgen.h>
#include <ctype.h>


// STL-stuff
#include <string>
#include <vector>

using std::string;
using std::ostream;
using std::ifstream;
using std::istringstream;
using std::ostringstream;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;

using namespace casa;

#ifndef BUILDTIME
#define BUILDTIME "<no buildtime info present>"
#endif


// Stuff for processing Huib's .lis files...
typedef struct _job_t {
	string           experName;
	string           vexfileName;
	string           profileName;
	string           outputMSName;
	vector<subjob_t> jobList;
} job_t;



void strtolower( string& s ) {
	string::iterator   cur = s.begin();

	while( cur!=s.end() ) {
		*cur = ::tolower( *cur );
		cur++;
	}
	return;
}


// We want to have an exception thrown whenever signal's are
// generated...
void throwOnSignal( int signr ) {
    THROW_JEXCEPT("Signal #" << signr << " raised. Stopping!");
}


//  Read a list of buffers from file.
//  Assume:
//
//  1) one buffer/line
//  2) all characters following # (inclusive the #-sign itself) are discarded on the same line
//  3) empty lines are discarded
Bool  readListFromFile( const char* fname, vector<subjob_t>& buflist ) {
    char     linebuf[1024];
    char     outbuf[1024];
    Bool     retval;
    char*    ptr;
    char*    optr;
    ifstream strm( fname );
    
    //  Init return values to default
	buflist.clear();
    retval  = False;
    
    if( !strm || strm.bad() || strm.fail() || !strm.good() ) {
		cout << "readListFromFile: Drat! Failed to open file "
		   	 << ((fname==0)?("<NULL>"):(fname)) << "!" << endl;
		return retval;
    }
    
    //  Ok. Read the file!
    retval = True;
    while( !strm.eof() ) {
		strm.getline( linebuf, sizeof(linebuf) );

		//  Rub out comments
		if( (ptr=strchr(linebuf, '#'))!=0 ) 
			*ptr = '\0';
		
		//  Filter out whitespace....
		ptr  = linebuf;
		optr = outbuf;
		while( *ptr ) {
			if( !strchr(" \t\f\r", *ptr) ) {
				*optr = *ptr;
				optr++;
			}
			ptr++;
		}
		*optr = '\0';

		//  If there still are characters left, it must be a buffername!
		if( strlen(outbuf) ) 
			buflist.push_back( subjob_t(outbuf) );
    }
    strm.close();
    return retval;
}


// Read *.lis file as per HJvL definition.
//  in backwardcompatibility mode (compat==true)
//   the "first" and "last" scan info is discarded.
//   Otherwise only scans from first->last (inclusive) 
//   are written to the MS
bool readLisFile( const char* lisfile, job_t& job, bool compat ) {
    char     linebuf[2048];
    Bool     retval;
	bool     firstlineread;
    char*    ptr;
    char*    optr;
    ifstream strm( lisfile );
    
    //  Init return values to default
	job     = job_t();
    retval  = False;
    
    if( !strm || strm.bad() || strm.fail() || !strm.good() ) {
		cout << "readLisFile: Drat! Failed to open file "
		   	 << ((lisfile==0)?("<NULL>"):(lisfile)) << "!" << endl;
		return retval;
    }
    
    retval        = True;
	firstlineread = false;
    while( retval==True && !strm.eof() ) {
		strm.getline( linebuf, sizeof(linebuf)-1 );

		//  Rub out comments
		if( (ptr=strchr(linebuf, '#'))!=0 )
			*ptr = '\0';
		
		//  Skip leading whitespace....
		optr  = linebuf;
		while( *optr && strchr(" \t\f\r", *optr) ) 
			optr++;

		//  If no characters left, nothing to do but to read another line...
		if( !strlen(optr) )
			continue;

		// What can the line be?
		if( firstlineread==true ) {
			if( *optr=='+' ) {
				string        plus;
				string        jobname;
				string        dummy_pass;
				string        fscan;
				string        escan;
				istringstream sjstrm( optr );

				sjstrm >> plus >> jobname >> dummy_pass >> fscan >> escan;

				if( compat )
					job.jobList.push_back( subjob_t(jobname) );
				else
					job.jobList.push_back( subjob_t(jobname, fscan, escan) );
			}
		} else {
			istringstream flstrm( optr );

			flstrm >> job.experName >> job.vexfileName
				   >> job.profileName >> job.outputMSName;

			if( job.experName.size()==0 ) {
				retval = False;
				cout << "readLisFile: experiment name is empty?!" << endl;
			} else {
                // Provide defaults as per HJvL for these items if
                // they were not set in the lisfile
				if( job.vexfileName.size()==0 ) {
					job.vexfileName = job.experName+".vix";
					strtolower( job.vexfileName );
				}
				if( job.profileName.size()==0 )
					job.profileName = "Prod";
				if( job.outputMSName.size()==0 )
					job.outputMSName = job.experName+job.profileName+".ms";
			}
			firstlineread = true;
		}
    }
    strm.close();
    return retval;
}

void Usage( void ) {
    cout << "Usage: j2ms2 [options] [buffers]*\n"
         << "\n"
         << "Options:\n"
         << "  [-o <outputMS>]\n"
         << "  [-d <outputdomain>] {time|frequency}\n"
         << "  [-f <buflistfile>] {one buffer per line}\n"
         << "  [-(V|v) <lisfile>] {V=backward compat mode}\n"
         << "  [-h] {this message}\n"
         << "  [eo:<experiment option>]* {* indicates: may occur > once}\n"
         << "  [fo:<filler option>]* {see previous}\n";
    cout << "\n"
         << "Buffers typically are a directory; '<jobid>/<subjobid>'\n"
         << "\n"
         << "<filler option> can be used to switch on filter(s):\n"
         << "  fo:filter/source=<commaseparated list of sources to accept>\n"
         << "  fo:filter/ghostbuster throw out data for which a station has no\n"
         << "                        associated/scheduled scan (ghostdata)\n"
         << "                        THIS FILTER IS ON BY DEFAULT!\n"
         << "\n"
		 << "See http://www.jive.nl/~verkout/j2ms2.html for more detail.\n";
    return;
}

int main( int , char** argv ) {
	char*                 thisexe;

	if( (thisexe=::basename(argv[0]))==0 )
		thisexe = argv[0];
	cout << "\n"
		 << thisexe
#ifdef BUILDTIME
         << ": Version of " << BUILDTIME
#endif
		 << " begins"
         << "\n\n"
		 << flush;

    try
    {
		// define runmode to be able to find out what
        // mode we're actually running...
		typedef enum _runmode_t {
			unset, interactive, buflistfromfile, buflistfromcmdline, lisfile
		} runmode_t;
	
		runmode_t          runmode( unset );
			
		// Outputname -> used if non-empty
        // AND not running from lisfile, in which case it's overridden
		string           outname;
        
		// Buffers that need translation -> if this is empty -> interactively!
		vector<subjob_t> buffers2do;

		// thingy for lisfile runmode
        // backwardcompatibility mode (use -V to switch on)
		bool               compat(false); 
		job_t              job;
		
		// optional stuff that could be passed to the experiment
		// and/or the filler
		String             fo("filter/ghostbuster"); // (f)iller(o)ption
		String             eo; // (e)xperiment(o)ption
		const Regex        filleropt( "^fo:.+" );
		const Regex        experopt( "^eo:.+" );

		// In what domain do we want the data?
		Visibility::domain dstdomain( Visibility::undefined );

		// Let's count the errors...
		unsigned int       errcnt = 0;
		
		//  Before we do anything, we tell the system what to do when
		//  specific signals are raised...
		signal( SIGINT,  throwOnSignal );
		signal( SIGKILL, throwOnSignal );
		signal( SIGTERM, throwOnSignal );

		//  Ok. Do some commandline parsing. If there are entries on
		//  the cmd-line that are not options, we assume the
		//  usr. wants to run non-interactively. So what we do is to
		//  parse the cmdline and base our conclusions on the outcome
		//  of that!
		while( *(++argv) ) {
			// Do some pre-parsing... if the arg matches eo:<bla> or fo:<bla>
			// treat it somewhat different.
			if( String(*argv).matches(filleropt) ) {
				// Create a pointer that points to the first character
				// after the first ':'-sign
				char*   valptr = ::strchr(*argv, ':') + 1;

                // String multiple fo: optionsvalues together into
                // a ;-separated string
				if( fo.size() )
					fo += ';';
				fo += valptr;
				continue;
			} else if( String(*argv).matches(experopt) ) {
				// Create a pointer that points to the first character
				// after the first ':'-sign
				char*   valptr = ::strchr(*argv, ':') + 1;

                // Same as with the filler options
				if( eo.size() )
					eo += ';';
				eo += valptr;

				continue;
			}
            // Tried the fo: and eo: style argumenttypes
            // Go on and try '-[a-zA-Z]' style argumenttypes
			switch( *(*argv) ) {
				case '-':
					switch( *((*argv)+1) ) {
						case 'd':
							// Expect: -d <domain>
							if( !(*(argv+1)) || (**(argv+1)=='-') ) {
								cout << "Missing argument to -d option!"
                                     << endl;
								errcnt++;
								break;
							}
			    
							++argv;
							if( dstdomain!=Visibility::undefined ) {
								cout << "Duplicate -d option. Discarding "
                                     << "this value: " << *argv << endl;
								errcnt++;
								break;
							}
							if( !strcmp(*argv, "time") )
								dstdomain = Visibility::time;
							else if( !strcmp(*argv, "frequency") )
								dstdomain = Visibility::frequency;
							else {
								cout << "Unrecognized domain `"
                                     << *argv << "'" << endl;
								errcnt++;
							}
							break;
			    
						case 'o':
							// Expect: -o <outputfile>
							// Not acceptable when running in lis-file mode
							// (in that case, the output filename is determined
							// from the contents of the lis-file)
							if( runmode==lisfile ) {
								if( (*(argv+1)) && (**(argv+1)!='-') )
									argv++;
								cout << "Cannot use -o option in conjunction "
                                     << "with lis-file based operation!"
									 << endl;
								errcnt++;
								break;
							}

							if( !(*(argv+1)) || (**(argv+1)=='-') ) {
								cout << "Missing argument to -o option!"
                                     << endl;
								errcnt++;
								break;
							}
							++argv;

							if( outname.size()==0 )
								outname = *argv;
							else {
								cout << "Duplicate -o option! "
                                     << "Discarding this value: "
									 << *argv << endl;
								errcnt++;
							}
							break;

						case 'f':
							// Expect: -f <file with buffer-ids>
							// Only acceptable if runmode==unset...
							if( !(*(argv+1)) || (**(argv+1)=='-') ) {
								cout << "Missing argument to -f option!"
                                     << endl;
								errcnt++;
								break;
							}
							++argv;
							if( runmode!=unset ) {
								// if necessary, skip over nxt argument..
								cout << "Hum... it seems as if you're trying to blend "
									 << "different runmodes. The -f option is mutually "
									 << "exclusive to any other runmode";
								if( runmode==buflistfromfile ) {
									cout << ", even with itself; the option may only "
										 << "appear once on the commandline...";
								}
								cout << "." << endl
									 << "As a result, this one, " << *argv
									 << ", will be ignored" << endl;
								errcnt++;
								break;
							}

							if( readListFromFile(*argv, buffers2do) )
								runmode = buflistfromfile;
							else {
								cout << "Drat! Failed to read buf-list from file "
									 << *argv << endl;
								errcnt++;
							}
							break;
			  
						case 'V':
							compat = true;
						case 'v':
							// Expect: -v <lisfilename>
							// Only acceptable if runmode==unset...
							if( !(*(argv+1)) || (**(argv+1)=='-') ) {
								cout << "Missing argument to -v option!" << endl;
								errcnt++;
								break;
							}
							++argv;
							if( runmode!=unset ) {
								// if necessary, skip over nxt argument..
								cout << "You ok in there?"
								     << " It seems as if you're trying to blend "
									 << "different runmodes. The -[vV] option is mutually "
									 << "exclusive to any other runmode";
								if( runmode==lisfile ) {
									cout << ", even with itself; the option may only "
										 << "appear once on the commandline...";
								}
								cout << "." << endl
									 << "As a result, this one, " << *argv
									 << ", will be ignored" << endl;
								errcnt++;
								break;
							}

							if( readLisFile(*argv, job, compat) ) {
								runmode = lisfile;

								if( outname.size()!=0 ) {
									cout << endl
										 << "WARN: Apparently you wanted to set the "
										 << "output MSfilename via the commandline; "
										 << "this is not acceptable when using the "
										 << "lis-file runmode. Your setting of "
										 << outname << " will be overridden with the "
										 << "value found in the lis-file, "
										 << job.outputMSName
										 << endl;
									outname = "";
									errcnt++;
								}
							} else {
								cout << "Drat! Failed to read lis-file "
									 << *argv << endl;
								errcnt++;
							}
							break;

                        case 'h':
                            Usage();
                            return 0;
                            
						default:
							cout << "Unrecognized option '"
                                 << *argv << "'" << endl;
							errcnt++;
							break;
					}
					break;
		    
				default:
					static Bool  errorshown = False;
		    
					// If runmode not unset or buflistfromcmdline
					// we do not accept this!
					if( runmode!=unset && runmode!=buflistfromcmdline ) {
						if( !errorshown ) {
							cout << "You cannot mix styles of runmode,"
								 << " you\n"
							 	 << "have already chosen a different runmode; hence the\n"
								 << "buffers on the cmd-line will be ignored." << endl;
						    errorshown = True;
							errcnt++;
						}
						cout << "Discarding cmd-line buffer specification " << *argv 
							 << endl;
						break;
					}
		   
					//  Assume it is the name of a buffer....
				   	runmode = buflistfromcmdline;
					buffers2do.push_back( subjob_t(*argv) );					
					break;
			}
		}

		if( errcnt )
			THROW_JEXCEPT("There " << ((errcnt>1)?("were "):("was "))
			      << errcnt << " error" << ((errcnt>1)?("s"):(""))
                  << " detected. Giving up.");
		
		//  Now we can really start...
		String            scanid;
		Experiment*       theExperiment;
		const Regex       freqdomain( "^[ \t]*frequency[ \t]*$" );
		const Regex       timedomain( "^[ \t]*time[ \t]*$" );
		JIVEMSFiller      theFiller( fo );


		theExperiment = 0;
	
		try {
			String     vexfile;

			if( runmode==lisfile )
				vexfile = job.vexfileName;
			theExperiment = new VEXperiment( eo, vexfile );
		}
		catch( const std::exception& y ) {
			cerr << y.what() << endl;
			theExperiment = 0;
		}
	
/*
		if( !theExperiment )
		{
			try
			{
				theExperiment = new IniExperiment( eo );
				cout << "\n\n"
					 << "**** Detected ini-file experiment "
					 << "(assumes Alberts format datafiles)..."
					 << "\n\n" << flush;
			}
			catch( AIPSExceptionObject x )
			{
				x.getMesg();
				theExperiment = 0;
			}
			catch( const ExceptionObject& )
			{
				theExperiment = 0;
			}
		}
*/
		if( !theExperiment )
			THROW_JEXCEPT("Experiment is not COF.");

		//  Now decide if we are running interactively or not.  If
		//  usr. did not specify any filenames or lisfile or just a
        //  file with visbufids on the cmdline, we ask
		//  for it; i.e. we run interactively!
		if( runmode==unset )
			runmode = interactive;
	
		//  See if usr. specified some other options on the
		//  commandline!

        // If dstdomain was explicitly set:
		if( dstdomain!=Visibility::undefined )
			theExperiment->setDestinationDomain( dstdomain );

        // outputfilename overridden?
		if( outname.size() ) 
			theExperiment->setMSname( outname.c_str() );

		// If running in lisfile mode, copy some stuff across
        // Note: the -o option is discarded here
		if( runmode==lisfile ) {
            if( outname.size() ) {
                cerr << "\n"
                     << "WARN: Discarding -o option [" << outname << "]; "
                     << "because running from lisfile!!!!" << endl;
            }
			theExperiment->setMSname( job.outputMSName.c_str() );
			buffers2do = job.jobList;
		}

        //  Show exp. summary to usr.
		cout << *theExperiment << endl;

		//  Either run interactively or not!
		if( runmode==interactive ) {
			while( 1 ) {
				cin.tie( &cout );
				cout << "DataBuffer: ";
				cin >> scanid;
				cin.tie( 0 );

				if( scanid=="quit" ) 
					break;
		
				if( String(scanid.c_str()).matches(freqdomain) )
					theExperiment->setDestinationDomain( Visibility::frequency );
				else if( String(scanid.c_str()).matches(timedomain) )
					theExperiment->setDestinationDomain( Visibility::time );
				else if( !theFiller.fillDataToExperimentsMS(*theExperiment,scanid,true) )
					cerr << "Failed to write data to MS" << endl;
			}
		} else {
			//  Do not run interactively...
			vector<subjob_t>::iterator   cur = buffers2do.begin();

			while( cur!=buffers2do.end() ) {
				theFiller.fillDataToExperimentsMS( *theExperiment,
												   *cur,
												   false );
				cur++;
			}
		}
	
		delete theExperiment;
    }
    catch( const std::exception& x ) {
		cout << "Rats! " << x.what() << endl;
    }
    
    return 0;
}
