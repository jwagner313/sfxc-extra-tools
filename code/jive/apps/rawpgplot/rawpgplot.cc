//# rawpgplot.cc: A Glish client ra pgplot interfacing...
//# Copyright (C) 1994,1995,1996,1997,1998,1999
//# Associated Universities, Inc. Washington DC, USA.
//#
//# This program is free software; you can redistribute it and/or modify it
//# under the terms of the GNU General Public License as published by the Free
//# Software Foundation; either version 2 of the License, or (at your option)
//# any later version.
//#
//# This program is distributed in the hope that it will be useful, but WITHOUT
//# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//# more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with this program; if not, write to the Free Software Foundation, Inc.,
//# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//#
//# Correspondence concerning AIPS++ should be addressed as follows:
//#        Internet email: aips2-request@nrao.edu.
//#        Postal address: AIPS++ Project Office
//#                        National Radio Astronomy Observatory
//#                        520 Edgemont Road
//#                        Charlottesville, VA 22903-2475 USA
//#
//# $Id: rawpgplot.cc,v 1.5 2006/01/13 13:02:04 verkout Exp $
//
// $Log: rawpgplot.cc,v $
// Revision 1.5  2006/01/13 13:02:04  verkout
// HV: CASAfied version of the implement
//
// Revision 1.4  2001/05/30 11:59:27  verkout
// HV: Made changes to accomodate for MS v2.0 and moved all code from the 'vlbi' branch to the 'jive' branch.
//
// Revision 1.3  2000/05/23 11:35:17  verkout
// HV: Added more PGPlot commands...
//
// Revision 1.2  2000/04/21 14:45:18  verkout
// HV: Added some more pgplot commands...
//
//
//# <todo asof="1997/06/26>
//#  <li>
//# </todo>

#include <tasking/Glish.h>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/String.h>
#include <casa/Exceptions/Error.h>
#include <casa/OS/Memory.h>

#include <jive/../apps/rawpgplot/boring.h>

using namespace casa;


int main(int argc, char **argv)
{
    String              message;
    GlishSysEvent       event;
    GlishSysEventSource eventStream(argc, argv);


    while( eventStream.connected() )
    {
	Bool ok = True;
	try
	{
	    // block until next event
	    event = eventStream.nextGlishEvent();
	    
	    // There is an event.
	    message = "";
	    if( event.type()=="clos" )
	    {
		ok = doPGClos(event, eventStream, message);
	    }
	    else if( event.type()=="open" )
	    {
		ok = doPGOpen(event, eventStream, message);
	    }
	    else if( event.type()=="slct" )
	    {
		ok = doPGSelect(event, eventStream, message);
	    }
	    else if( event.type()=="eras" )
	    {
		ok = doPGErase(event, eventStream, message);
	    }
	    else if( event.type()=="subp" )
	    {
		ok = doPGSubp(event, eventStream, message);
	    }
	    else if( event.type()=="bbuf" )
	    {
		ok = doPGBbuf(event, eventStream, message);
	    }
	    else if( event.type()=="ebuf" )
	    {
		ok = doPGEbuf(event, eventStream, message);
	    }
	    else if( event.type()=="svp" )
	    {
		ok = doPGSvp(event, eventStream, message);
	    }
	    else if( event.type()=="env" )
	    {
		ok = doPGEnv(event, eventStream, message);
	    }
	    else if( event.type()=="sci" )
	    {
		ok = doPGSci(event, eventStream, message);
	    }
	    else if( event.type()=="slw" )
	    {
		ok = doPGSlw(event, eventStream, message);
	    }
	    else if( event.type()=="box" )
	    {
		ok = doPGBox(event, eventStream, message);
	    }
	    else if( event.type()=="tbox" )
	    {
		//
		//  Note: the doPGBox() function handles BOTH box/tbox
		//  events!
		//
		ok = doPGBox(event, eventStream, message);
	    }
	    else if( event.type()=="lab" )
	    {
		ok = doPGLab(event, eventStream, message);
	    }
	    else if( event.type()=="sls" )
	    {
		ok = doPGSls(event, eventStream, message);
	    }
	    else if( event.type()=="line" )
	    {
		ok = doPGLine(event, eventStream, message);
	    }    
	    else if( event.type()=="ask" )
	    {
		ok = doPGAsk(event, eventStream, message);
	    }
	    else if( event.type()=="mtxt" )
	    {
		ok = doPGMtxt(event, eventStream, message);
	    }
	    else if( event.type()=="sch" )
	    {
		ok = doPGSch(event, eventStream, message);
	    }	    
	    else if( event.type()=="qch" )
	    {
		ok = doPGQch(event, eventStream, message);
	    }	    
	    else if( event.type()=="swin" )
	    {
		ok = doPGSwin(event, eventStream, message);
	    }	    
	    else if( event.type()=="page" )
	    {
		ok = doPGPage(event, eventStream, message);
	    }
	    else if( event.type()=="pt" )
	    {
		ok = doPGPt(event, eventStream, message);
	    }
	    else if( event.type()=="qcs" )
	    {
		ok = doPGQcs(event, eventStream, message);
	    }
	    else if( event.type()=="ptxt" )
	    {
		ok = doPGPtxt(event, eventStream, message);
	    }
	    else if( event.type()=="vstd" )
	    {
		ok = doPGVstd(event, eventStream, message);
	    }
		else if( event.type()=="scf" )
		{
			ok = doPGScf( event, eventStream, message );
		}
		else if( event.type()=="qcf" )
		{
			ok = doPGQcf( event, eventStream, message );
		}
	    else if( event.type()!="terminate" )
	    {
			doPGTerminate();
			eventStream.unrecognized();		
	    }
	}
	catch( AipsError x )
	{
	    // We don't handle any exception; return them as an error
	    message = x.getMesg();
	    ok = False;
	} 
	if( !ok )
	{
	   GlishRecord rec;
	   rec.add ("error_message", event.type() + ": " + message);
	   eventStream.postEvent( drEN(event), rec );
	}
    }
    return 0;
}


// 	    if( event.type()=="open" )
// 	    {		
// 		ok = proxy.open (event, eventStream, message);
// 	    }
	    
// 	    } else if (event.type() == "open_update") {
// 		ok = proxy.openUpdate (event, eventStream, message);
// 	    } else if (event.type() == "create") {
// 		ok = proxy.create (event, eventStream, message);
// 	    } else if (event.type() == "command") {
// 		ok = proxy.command (event, eventStream, message);
// 	    } else if (event.type() == "select_rows") {
// 		ok = proxy.selectRows (event, eventStream, message);
// 	    } else if (event.type() == "flush") {
// 		ok = proxy.flush (event, eventStream, message);
// 	  } else if (event.type() == "close") {
// 	    ok = proxy.close (event, eventStream, message);
// 	  } else if (event.type() == "close_all") {
// 	    ok = proxy.closeAll (event, eventStream, message);
// 	  } else if (event.type() == "lock") {
// 	    ok = proxy.lock(event, eventStream, message);
// 	  } else if (event.type() == "unlock") {
// 	    ok = proxy.unlock (event, eventStream, message);
// 	  } else if (event.type() == "has_data_changed") {
// 	    ok = proxy.hasDataChanged (event, eventStream, message);
// 	  } else if (event.type() == "has_lock") {
// 	    ok = proxy.hasLock (event, eventStream, message);
// 	  } else if (event.type() == "lock_options") {
// 	    ok = proxy.lockOptions (event, eventStream, message);
// 	  } else if (event.type() == "is_multi_used") {
// 	    ok = proxy.isMultiUsed (event, eventStream, message);
// 	  } else if (event.type() == "copy") {
// 	    ok = proxy.copy (event, eventStream, message);
// 	  } else if (event.type() == "rename") {
// 	    ok = proxy.rename (event, eventStream, message);
// 	  } else if (event.type() == "delete") {
// 	    ok = proxy.deleteTable (event, eventStream, message);
// 	  } else if (event.type() == "read_ascii") {
// 	    ok = proxy.readAscii (event, eventStream, message);
// 	  } else if (event.type() == "get_open_tables") {
// 	    ok = proxy.getOpenTables (event, eventStream, message);
// 	  } else if (event.type() == "get_name") {
// 	    ok = proxy.getName (event, eventStream, message);
// 	  } else if (event.type() == "get_id") {
// 	    ok = proxy.getId (event, eventStream, message);
// 	  } else if (event.type() == "is_readable") {
// 	    ok = proxy.isReadable (event, eventStream, message);
// 	  } else if (event.type() == "is_writable") {
// 	    ok = proxy.isWritable (event, eventStream, message);
// 	  } else if (event.type() == "table_info") {
// 	    ok = proxy.tableInfo (event, eventStream, message);
// 	  } else if (event.type() == "put_table_info") {
// 	    ok = proxy.putTableInfo (event, eventStream, message);
// 	  } else if (event.type() == "add_readme_line") {
// 	    ok = proxy.addReadmeLine (event, eventStream, message);
// 	  } else if (event.type() == "addColumns") {
// 	    ok = proxy.addColumns (event, eventStream, message);
// 	  } else if (event.type() == "extend") {
// 	    ok = proxy.addRow (event, eventStream, message);
// 	  } else if (event.type() == "remove_row") {
// 	    ok = proxy.removeRow (event, eventStream, message);
// 	  } else if (event.type() == "set_max_cache_size") {
// 	    ok = proxy.setMaximumCacheSize (event, eventStream, message);
// 	  } else if (event.type() == "cell_contents_defined") {
// 	    ok = proxy.cellContentsDefined (event, eventStream, message);
// 	  } else if (event.type() == "get_cell") {
// 	    ok = proxy.getCell (event, eventStream, message);
// 	  } else if (event.type() == "get_cell_slice") {
// 	    ok = proxy.getCellSlice (event, eventStream, message);
// 	  } else if (event.type() == "put_cell") {
// 	    ok = proxy.putCell (event, eventStream, message);
// 	  } else if (event.type() == "put_cell_slice") {
// 	    ok = proxy.putCellSlice (event, eventStream, message);
// 	  } else if (event.type() == "get_column") {
// 	    ok = proxy.getColumn (event, eventStream, message);
// 	  } else if (event.type() == "get_column_slice") {
// 	    ok = proxy.getColumnSlice (event, eventStream, message);
// 	  } else if (event.type() == "put_column") {
// 	    ok = proxy.putColumn (event, eventStream, message);
// 	  } else if (event.type() == "put_column_slice") {
// 	    ok = proxy.putColumnSlice (event, eventStream, message);
// 	  } else if (event.type() == "get_column_shape_string") {
// 	    ok = proxy.getColumnShapeString (event, eventStream, message);
// 	  } else if (event.type() == "get_keyword") {
// 	    ok = proxy.getKeyword (event, eventStream, message);
// 	  } else if (event.type() == "get_keywordset") {
// 	    ok = proxy.getKeywordSet (event, eventStream, message);
// 	  } else if (event.type() == "put_keyword") {
// 	    ok = proxy.putKeyword (event, eventStream, message);
// 	  } else if (event.type() == "put_keywordset") {
// 	    ok = proxy.putKeywordSet (event, eventStream, message);
// 	  } else if (event.type() == "remove_keyword") {
// 	    ok = proxy.removeKeyword (event, eventStream, message);
// 	  } else if (event.type() == "get_fieldnames") {
// 	    ok = proxy.getFieldNames (event, eventStream, message);
// 	  } else if (event.type() == "get_table_desc") {
// 	    ok = proxy.getTableDescription (event, eventStream, message);
// 	  } else if (event.type() == "get_column_desc") {
// 	    ok = proxy.getColumnDescription (event, eventStream, message);
// 	  } else if (event.type() == "shape") {
// 	    ok = proxy.shape (event, eventStream, message);
// 	  } else if (event.type() == "row_numbers") {
// 	    ok = proxy.rowNumbers (event, eventStream, message);
// 	  } else if (event.type() == "column_names") {
// 	    ok = proxy.columnNames (event, eventStream, message);
// 	  } else if (event.type() == "is_scalar_column") {
// 	    ok = proxy.isScalarColumn (event, eventStream, message);
// 	  } else if (event.type() == "column_data_type") {
// 	    ok = proxy.columnDataType (event, eventStream, message);
// 	  } else if (event.type() == "column_array_type") {
// 	    ok = proxy.columnArrayType (event, eventStream, message);
// 	  } else if (event.type() == "make_iterator") {
// 	    ok = proxy.makeIterator (event, eventStream, message);
// 	  } else if (event.type() == "step_iterator") {
// 	    ok = proxy.stepIterator (event, eventStream, message);
// 	  } else if (event.type() == "close_iterator") {
// 	    ok = proxy.closeIterator (event, eventStream, message);
// 	  } else if (event.type() == "make_row") {
// 	    ok = proxy.makeRow (event, eventStream, message);
// 	  } else if (event.type() == "get_row") {
// 	    ok = proxy.getRow (event, eventStream, message);
// 	  } else if (event.type() == "put_row") {
// 	    ok = proxy.putRow (event, eventStream, message);
// 	  } else if (event.type() == "close_row") {
// 	    ok = proxy.closeRow (event, eventStream, message);
// 	  } else if (event.type() != "terminate") {
// 	    // We don't understand what this event is!
// 	    eventStream.unrecognized();
// 	  }
