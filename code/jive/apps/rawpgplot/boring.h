//
//  *yawn* boring... translate events -> pgplot calls?
//
#include <tasking/Glish.h>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/String.h>
#include <casa/Exceptions/Error.h>

#include <sstream>

using namespace casa;

String drEN( const casa::GlishSysEvent& event );

//
//  This is not a pg-plot function; it just closes down the lib!
//
Bool doPGTerminate();



Bool doPGClos( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGOpen( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGSelect( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGErase( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGSubp( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGBbuf( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGEbuf( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGSvp( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGEnv( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGSci( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGSlw( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGBox( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGLab( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGSls( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGLine( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGAsk( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );

Bool doPGMtxt( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGSch( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGQch( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );

Bool doPGSwin( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGPage( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGPt( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );

Bool doPGQcs( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );

Bool doPGPtxt( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );

Bool doPGVstd( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );


Bool doPGScf( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
Bool doPGQcf( casa::GlishSysEvent& ev, casa::GlishSysEventSource& evsrc, casa::String& msg );
