#include <jive/../apps/rawpgplot/boring.h>

#include <casa/Arrays.h>
#include <casa/Arrays/IPosition.h>

#include <iostream>

#include <cpgplot.h>

using namespace std;

//
//  Count the number of opendevices so we can easily check if 
//  certain operations make sense...
//
typedef struct _PGDevices
{
    friend ostream& operator<<( ostream& os, const _PGDevices& dvlist )
    {
	os << "PGDeviceList: listing " << dvlist.nrOpenPGDevices << endl;
	for( int i=0; i<dvlist.nrOpenPGDevices; i++ )
	{
	    os << "DEV[" << i << "]=" << dvlist.deviceIDs[i] << endl;
	}
	return os;
    }
    

    _PGDevices() :
	deviceIDs( 0 ),
	nrOpenPGDevices( 0 )
    {
    }

    int  nrOpenDevices( void ) const
    {
	return nrOpenPGDevices;
    }
    
    //
    //  Return deviceID of device # nr
    //
    //  -1 if invalid, >0 otherwise!
    //
    int operator[]( int nr ) const
    {
	int   retval = -1;
	
	if( nr>=0 && nr<nrOpenPGDevices )
	{
	    retval = deviceIDs[nr];
	}
	return retval;
    }
    

    bool addDevice( int deviceid )
    {
	int  i;
	bool retval = false;
	
	//
	//  Check if not already there...
	//
	for( i=0; i<nrOpenPGDevices; i++ )
	{
	    if( deviceIDs[i]==deviceid )
	    {
		break;
	    }
	}
	
	if( i==nrOpenPGDevices )
	{
	    nrOpenPGDevices++;
	    
	    if( (deviceIDs=(int *)(::realloc(deviceIDs, (nrOpenPGDevices)*sizeof(int))))==0 )
	    {
		nrOpenPGDevices = 0;
	    }
	    else
	    {
		deviceIDs[i] = deviceid;
		retval       = true;
	    }
	}
	return retval;
    }
    
    bool removeDevice( int deviceid )
    {
	int    i;
	bool   retval = false;
	
	for( i=0; i<nrOpenPGDevices; i++ )
	{
	    if( deviceIDs[i]==deviceid )
	    {
		break;
	    }
	}
	
	if( i==nrOpenPGDevices )
	{
	    //
	    //  No such device?!
	    //
	    return retval;
	}
	
	//
	//  Remove stuff...
	//
	nrOpenPGDevices--;
	
	if( !nrOpenPGDevices )
	{
	    free( deviceIDs );
	    deviceIDs = 0;
	    return true;
	}
	
	//
	//  If it's not the last device, we move everythin after it
	//  one place down!
	//
	//  NOTE: Since nrOpenPGDevices already has been decremented
	//  by one we do not need to do that again here!
	//
	if( i<nrOpenPGDevices )
	{
	    ::memmove( (void *)(&(deviceIDs[i])), (void *)(&(deviceIDs[i+1])),
		       (nrOpenPGDevices-i)*sizeof(int) );
	}
	return retval;
    }
    

    bool  haveDevice( int deviceid )
    {
	int   i;
	
	for( i=0; i<nrOpenPGDevices; i++ )
	{
	    if( deviceIDs[i]==deviceid )
	    {
		break;
	    }
	}
	//cout << "haveDevice(" << deviceid << "): i=" << i << " nrOpenPGDevices="
	//     << nrOpenPGDevices << endl;
	return (i<nrOpenPGDevices);
    }
    

    ~_PGDevices()
    {
	if( deviceIDs )
	{
	    ::free(deviceIDs);
	}

	deviceIDs       = 0;
	nrOpenPGDevices = 0;
    }
    
private:
    
    int*    deviceIDs;
    int     nrOpenPGDevices;
} PGDevices;


static PGDevices openPGDevices;



// drEn == dumb return EventName
String drEN( const GlishSysEvent& event )
{
    return event.type() + "_result";
}


Bool doPGTerminate( void )
{
    //if( openPGDevices.nrOpenDevices()>0 )
    //{
    ::cpgend();
    //}
    return True;
}




Bool doPGClos( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool  retval( True );
    
    if( openPGDevices.nrOpenDevices()>0 )
    {
	//
	//  Get id of current device...
	//
	int   device;

	::cpgqid( &device );

	//
	//  Close it...
	//
	::cpgclos();

	//
	//  And remove from the list
	//
	openPGDevices.removeDevice( device );
	
	//
	//  Auto-select if something is left??
	//
	//if( (device=openPGDevices.nrOpenDevices())>0 )
	//{
	//    cpgslct( openPGDevices[device-1] );
	//}
	

	//
	//  Send message back?
	//
	evsrc.postEvent( drEN(ev), "Closed a device..." );
    }
    else
    {
	msg = "HUH? There are no devices left to close!";
	retval = False;
    }
    return retval;
}


Bool doPGOpen( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    int     pgretval;
    Bool    retval( False );
    String  str;    
    
    if( ev.val().type()==GlishValue::RECORD )
    {
	msg = "doPGOpen: EINVAL! String expected got record!";
	return retval;
    }
    
    GlishArray   arg( ev.val() );
    
    if( arg.elementType()!=GlishArray::STRING || arg.nelements()!=1 )
    {
	msg = "doPGOpen: EINVAL! One string expected got array of other type?!";
	return retval;	
    }
    arg.get(str);

    pgretval = ::cpgopen( str.chars() );
    
    if( pgretval>0 )
    {
    	retval = openPGDevices.addDevice( pgretval );
    }
    
    GlishArray evretval( (Int)pgretval );

    evsrc.postEvent( drEN(ev), evretval );
    return True;
}


Bool doPGSelect( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );
    Int     device;

    if( ev.val().type()==GlishValue::RECORD )
    {
	msg = "doPGSelect: EINVAL! Int expected got record!";
	return retval;
    }

    GlishArray   arg( ev.val() );

    if( arg.elementType()!=GlishArray::INT || arg.nelements()!=1 )
    {
	msg = "doPGSelect: EINVAL! One INT expected got array of other type?!";
	return retval;	
    }

    arg.get(device);
    
    if( openPGDevices.haveDevice((int)device) )
    {
    	ostringstream   strm;
	
    	::cpgslct( (int)device );
    	retval = True;

    	strm << "doPGSelect: Selected device " << device;
    	evsrc.postEvent( drEN(ev), strm.str() );
    }
    else
    {
    	ostringstream  strm;
	
    	strm << "doPGSelect: No such device as " << device << "!";
    	msg = strm.str();
    }

    return retval;    
}


Bool doPGErase( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool   retval = False;
    
    if( openPGDevices.nrOpenDevices()>0 )
    {
	retval = True;
	::cpgeras( );
	evsrc.postEvent( drEN(ev), "doPGErase: page erased" );
    }
    else
    {
	msg = "doPGEras: No device to erase pages on...";
    }
    return retval;    
}

Bool doPGSubp( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	return True;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGSubp: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=2 )
    {
	msg = "doPGSubp: invalid number of parameters (!=2)";
	return retval;
    }

    Int           ival[2];
    uInt          i;
    
    for( i=0; i<2; i++ )
    {
	GlishValue    arg;

	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}
	GlishArray arr( arg );
	if( arr.elementType()!=GlishArray::INT || arr.nelements()!=1 )
	{
	    break;
	}
	arr.get( ival[i] );

	if( ival[i]==0 )
	{
	    break;
	}
    }

    if( i<2 )
    {
	msg = "doPGSubp: error in arguments. Not two ints but something else!";
	return retval;
    }
    
    ::cpgsubp( (int)ival[0], (int)ival[1] );
    evsrc.postEvent( drEN(ev), "doPGSubp: page subdivided" );
    return True;
}


Bool doPGBbuf( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool   retval = False;
    
    if( openPGDevices.nrOpenDevices()>0 )
    {
	retval = True;
	::cpgbbuf( );

	evsrc.postEvent( drEN(ev), "doPGBbuf: start buffering..." );
    }
    else
    {
	msg = "doPGBbuf: No device to start buffering on...";
    }
    return retval;        
}

Bool doPGEbuf( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool  retval = False;
    
    if( openPGDevices.nrOpenDevices()>0 )
    {
	retval = True;
	::cpgebuf( );

	evsrc.postEvent( drEN(ev), "doPGEbuf: ended buffering..." );
    }
    else
    {
	msg = "doPGEbuf: No device to end buffering on...";
    }
    return retval;        
}


Bool doPGSvp( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGSvp: No devices...";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGSvp: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=4 )
    {
	msg = "doPGSvp: invalid number of parameters (!=4)";
	return retval;
    }

    uInt          i;
    Double        dval[4];
    
    for( i=0; i<4; i++ )
    {
	GlishValue    arg;
	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}
	GlishArray arr( arg );
	if( arr.elementType()!=GlishArray::DOUBLE || arr.nelements()!=1 )
	{
	    break;
	}
	arr.get( dval[i] );
    }

    if( i<4 )
    {
	msg = "doPGSvp: error in arguments. Not four floats but something else!";
	return retval;
    }
    
    ::cpgsvp( (Float)dval[0], (Float)dval[1], (Float)dval[2], (Float)dval[3] );

    evsrc.postEvent( drEN(ev), "doPGSvp: Page subdivided..." );
    return True;
}

Bool doPGEnv( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGEnv: No device..";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGEnv: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=6 )
    {
	msg = "doPGSvp: invalid number of parameters (!=6)";
	return retval;
    }

    Int                     ival[2];
    uInt                    i;
    Double                  dval[4];
    GlishArray::ElementType desired[6] = { GlishArray::DOUBLE, GlishArray::DOUBLE, 
					       GlishArray::DOUBLE, GlishArray::DOUBLE,
					       GlishArray::INT, GlishArray::INT  
    };

    for( i=0; i<6; i++ )
    {
	GlishValue    arg;

	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}
	GlishArray arr( arg );
	if( arr.elementType()!=desired[i] || arr.nelements()!=1 )
	{
	    break;
	}
	if( i<4 )
	{
	    arr.get( dval[i] );
	}
	else
	{
	    arr.get( ival[i-4] );
	}
    }

    if( i<6 )
    {
	msg = "doPGSVP: error in arguments. Not four floats +2 ints but something else!";
	return retval;
    }
    
    ::cpgenv( (Float)dval[0], (Float)dval[1], (Float)dval[2], (Float)dval[3],
	    (int)ival[0], (int)ival[1] );

    evsrc.postEvent( drEN(ev), "doPGEnv: Environment set" );
    return True;    
}

Bool doPGSci( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGSci: No devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::ARRAY )
    {
	msg = "doPGSci: EINVAL! Expected a scalar!";
	return retval;
    }

    GlishArray   arr( ev.val() );

    if( arr.nelements()!=1 || arr.elementType()!=GlishArray::INT )
    {
	msg = "doPGSci: invalid number of parameters (!=1)";
	return retval;
    }

    Int           ival;

    arr.get( ival );
    
    ::cpgsci( (int)ival );
    
    evsrc.postEvent( drEN(ev), "doPGSci: Color index set" );
    return True;
}


Bool doPGSlw( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGSlw: No devices...";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::ARRAY )
    {
	msg = "doPGSlw: EINVAL! Expected a scalar!";
	return retval;
    }

    GlishArray   arr( ev.val() );

    if( arr.nelements()!=1 || arr.elementType()!=GlishArray::INT )
    {
	msg = "doPGSlw: invalid number of parameters (!=1)";
	return retval;
    }

    Int     ival;

    arr.get( ival );
    
    ::cpgslw( (int)ival );

    evsrc.postEvent( drEN(ev), "doPGSlw: Line width set" );
    return True;    
}

Bool doPGBox( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "goPG(T?)Box: No devices!";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGBox: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=6 )
    {
	msg = "doPGBox: invalid number of parameters (!=6)";
	return retval;
    }
    
    Int                     ival[2];
    uInt                    i;
    String                  sval[2];
    Double                  dval[2];
    GlishArray              gval[6];
    GlishArray::ElementType desired[6] = { GlishArray::STRING, GlishArray::DOUBLE, 
					       GlishArray::INT, GlishArray::STRING,
					       GlishArray::DOUBLE, GlishArray::INT  
    };

    for( i=0; i<6; i++ )
    {
	GlishValue    v;
	
	v = rec.get(i);
	
	if( v.type()!=GlishValue::ARRAY )
	{
	    break;
	}
	gval[i] = GlishArray(v);
	if( gval[i].elementType()!=desired[i] || gval[i].nelements()!=1 )
	{
	    break;
	}
    }

    if( i<6 )
    {
	msg = "doPGBox: error in arguments. Not 2 strings + 2 floats +2 ints but something else!";
	return retval;
    }

    gval[0].get( sval[0] );
    gval[3].get( sval[1] );
    
    gval[1].get( dval[0] );
    gval[4].get( dval[1] );
    
    gval[2].get( ival[0] );
    gval[5].get( ival[1] );
    
    if( ev.type()=="box" )
    {
	::cpgbox( sval[0].chars(), (float)dval[0], (int)ival[0],
		sval[1].chars(), (float)dval[1], (int)ival[1] );
    }
    else if( ev.type()=="tbox" )
    {
	::cpgtbox( sval[0].chars(), (float)dval[0], (int)ival[0],
		sval[1].chars(), (float)dval[1], (int)ival[1] );	
    }

    evsrc.postEvent( drEN(ev), "doPG(T?)Box: box is set..." );
    return True;    
}


Bool doPGLab( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGLab: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGLab: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=3 )
    {
	msg = "doPGLab: invalid number of parameters (!=3)";
	return retval;
    }

    uInt          i;
    String        sval[3];
    
    for( i=0; i<3; i++ )
    {
	GlishValue    arg;
	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}
	GlishArray arr( arg );
	if( arr.elementType()!=GlishArray::STRING || arr.nelements()!=1 )
	{
	    break;
	}
	arr.get( sval[i] );
    }

    if( i<3 )
    {
	msg = "doPGLab: error in arguments. Not three strings but something else!";
	return retval;
    }
    
    ::cpglab( sval[0].chars(), sval[1].chars(), sval[2].chars() );

    evsrc.postEvent( drEN(ev), "doPGLab: Label set" );
    return True;    
}

Bool doPGSls( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( True );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGSls: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::ARRAY )
    {
	msg = "doPGSls: EINVAL! Expected a scalar!";
	return retval;
    }

    GlishArray   arr( ev.val() );

    if( arr.nelements()!=1 || arr.elementType()!=GlishArray::INT )
    {
	msg = "doPGSls: invalid number of parameters (!=1)";
	return retval;
    }

    Int           ival;

    arr.get( ival );
    
    ::cpgsls( (int)ival );

    evsrc.postEvent( drEN(ev), "doPGSls: Line style set" );
    return True;    
}

Bool doPGLine( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGLine: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGLine: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=2 )
    {
	msg = "doPGLine: invalid number of parameters (!=2)";
	return retval;
    }

    uInt          i;
    uInt          nrelements;
    Array<Double> adval[2];
    
    for( i=0; i<2; i++ )
    {
	GlishValue    arg;
	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}
	GlishArray arr( arg );
	if( arr.elementType()!=GlishArray::DOUBLE || arr.shape().nelements()!=1 )
	{
	    break;
	}
	arr.get( adval[i] );
    }

    if( i<2 )
    {
	msg = "doPGLine: error in arguments. Not 2 vectors of floats but something else!";
	return retval;
    }

    if( (nrelements=adval[0].nelements())!=adval[1].nelements() )
    {
	msg = "doPGLine: Lengths of X/Y array differ!";
	return retval;	
    }
    
    Bool             deleteIt[2];
    float*          casted[2];
    const Double*    storage[2];

    for( uInt j=0; j<2; j++ )
    {
		storage[j] = adval[j].getStorage(deleteIt[j]);

		casted[j]  = new float[nrelements];
	
		for( uInt n=0; n<nrelements; n++ )
		{
			casted[j][n] = (float)storage[j][n];
		}
    }
    
    ::cpgline( (int)nrelements, casted[0], casted[1] );

    for( uInt j=0; j<2; j++ )
    {
	delete [] casted[j];
	
	adval[j].freeStorage( storage[j], deleteIt[j] );
    }

    evsrc.postEvent( drEN(ev), "doPGLine: Line plotted" );
    return True;
}

Bool doPGAsk( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGAsk: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::ARRAY )
    {
	msg = "doPGAsk: EINVAL! Expected a scalar!";
	return retval;
    }

    GlishArray   arr( ev.val() );

    if( arr.nelements()!=1 || arr.elementType()!=GlishArray::BOOL )
    {
	msg = "doPGAsk: invalid number of parameters (!=1 bool)";
	return retval;
    }

    int            i = 0;
    Bool           bval;

    arr.get( bval );

    if( bval )
    {
	i = 1;
    }
    
    ::cpgask( i );

    ostringstream   strm;
    
    strm << "doPGAsk: ASK set to " << ((bval==True)?("ON"):("OFF"));
    
    evsrc.postEvent( drEN(ev), strm.str() );
    return True;
    
}


Bool doPGMtxt( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool retval = False;
    
    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGMtxt: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGMtxt: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=5 )
    {
	msg = "doPGMtxt: invalid number of parameters (!=5)";
	return retval;
    }

    uInt                    i;
    Double                  dval[3];
    String                  sval[2];
    GlishArray              gval[5];
    GlishArray::ElementType desired[5] = { GlishArray::STRING, GlishArray::DOUBLE, 
					       GlishArray::DOUBLE, GlishArray::DOUBLE,
					       GlishArray::STRING  
    };

    for( i=0; i<5; i++ )
    {
	GlishValue    arg;
	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}

	gval[i] = GlishArray( arg );

	if( gval[i].elementType()!=desired[i] || gval[i].nelements()!=1 )
	{
	    break;
	}
    }

    if( i<5 )
    {
	msg = "doPGMtxt: error in arguments. Not 2 strings + 3 floats but something else!";
	return retval;
    }

    
    gval[0].get( sval[0] );
    gval[1].get( dval[0] );
    gval[2].get( dval[1] );
    gval[3].get( dval[2] );
    gval[4].get( sval[1] );

    
    ::cpgmtxt( sval[0].chars(), (float)dval[0], (float)dval[1], (float)dval[2], sval[1].chars() );
    evsrc.postEvent( drEN(ev), "doPGMtxt: Text emm'd :-)" );
    return True;
    
}


Bool doPGSch( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( True );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGSch: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::ARRAY )
    {
	msg = "doPGSch: EINVAL! Expected a scalar!";
	return retval;
    }

    GlishArray   arr( ev.val() );

    if( arr.nelements()!=1 || arr.elementType()!=GlishArray::DOUBLE )
    {
	msg = "doPGSch: invalid number of parameters (!=1)";
	return retval;
    }

    Double        dval;
    
    arr.get( dval );
    
    ::cpgsch( (float)dval );

    evsrc.postEvent( drEN(ev), "doPGSch: Character height set" );
    return True;    
    
}

Bool doPGQch( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );
    float   ch;
    
    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGQch: No open devices";
	return retval;
    }
    
    ::cpgqch( &ch );
    evsrc.postEvent( drEN(ev), GlishArray((Double)ch) );
    return True;    
    
}

Bool doPGSwin( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGSwin: No devices...";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGSwin: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=4 )
    {
	msg = "doPGSwin: invalid number of parameters (!=4)";
	return retval;
    }

    uInt          i;
    Double        dval[4];
    
    for( i=0; i<4; i++ )
    {
	GlishValue    arg;
	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}
	GlishArray arr( arg );
	if( arr.elementType()!=GlishArray::DOUBLE || arr.nelements()!=1 )
	{
	    break;
	}
	arr.get( dval[i] );
    }

    if( i<4 )
    {
	msg = "doPGSwin: error in arguments. Not four floats but something else!";
	return retval;
    }
    
    ::cpgswin( (Float)dval[0], (Float)dval[1], (Float)dval[2], (Float)dval[3] );

    evsrc.postEvent( drEN(ev), "doPGSwin: Window set..." );
    return True;
}



Bool doPGPage( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool   retval = False;
    
    if( openPGDevices.nrOpenDevices()>0 )
    {
	retval = True;
	::cpgpage();

	evsrc.postEvent( drEN(ev), "doPGPage: new page started..." );
    }
    else
    {
	msg = "doPGPage: No device to start new page on...";
    }
    return retval;        
}


Bool doPGPt( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGPt: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGPt: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=3 )
    {
	msg = "doPGPt: invalid number of parameters (!=3)";
	return retval;
    }


    Int                     ival;
    uInt                    i;
    uInt                    nrelements;
    Array<Double>           adval[2];
    GlishArray              gval[3];
    GlishArray::ElementType desired[3] = { GlishArray::DOUBLE, GlishArray::DOUBLE, GlishArray::INT };

    for( i=0; i<3; i++ )
    {
	GlishValue    arg;
	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}

	gval[i] = GlishArray( arg );

	if( gval[i].elementType()!=desired[i] || gval[i].shape().nelements()!=1 )
	{
	    break;
	}
    }

    if( i<3 )
    {
	msg = "doPGPt: error in arguments. Not 2 vectors of floats + an int but something else!";
	return retval;
    }

    //
    //  Retrieve the values
    //
    gval[0].get( adval[0] );
    gval[1].get( adval[1] );
    gval[2].get( ival );

    if( (nrelements=adval[0].nelements())!=adval[1].nelements() )
    {
	msg = "doPGPt: Lengths of X/Y array differ!";
	return retval;	
    }
    
    Bool            deleteIt[2];
    float*          casted[2];
    const Double*   storage[2];

    for( uInt j=0; j<2; j++ )
    {
		storage[j] = adval[j].getStorage(deleteIt[j]);

		casted[j]  = new float[nrelements];
		
		for( uInt n=0; n<nrelements; n++ )
		{
			casted[j][n] = (float)storage[j][n];
		}
    }
    
    ::cpgpt( (int)nrelements, casted[0], casted[1], ival );

    for( uInt j=0; j<2; j++ )
    {
	delete [] casted[j];
	
	adval[j].freeStorage( storage[j], deleteIt[j] );
    }

    evsrc.postEvent( drEN(ev), "doPGPt: points plotted" );
    return True;
}

Bool doPGQcs( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGQcs: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::ARRAY )
    {
	msg = "doPGQcs: EINVAL! Expected a scalar!";
	return retval;
    }

    GlishArray   arr( ev.val() );

    if( arr.nelements()!=1 || arr.elementType()!=GlishArray::INT )
    {
	msg = "doPGQcs: invalid number of parameters (!=1 INT)";
	return retval;
    }

    Int            ival;
    Float          flt[2];
    Vector<Double> vd( 2 );
    
    //
    //  Get the integer argument
    //
    arr.get( ival );
    
    //
    //  Cal cpgqcs
    //
    ::cpgqcs( (int)ival, &flt[0], &flt[1] );

    //
    //  Transform into Vector<Double>
    //
    vd(0) = (Double)flt[0];
    vd(1) = (Double)flt[1];
    
    evsrc.postEvent( drEN(ev), GlishArray(vd) );
    return True;
}

Bool doPGPtxt( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool retval = False;
    
    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
	msg = "doPGPtxt: No open devices";
	return retval;
    }
    

    if( ev.val().type()!=GlishValue::RECORD )
    {
	msg = "doPGPtxt: EINVAL! Expected a record!";
	return retval;
    }

    GlishRecord   rec( ev.val() );

    if( rec.nelements()!=5 )
    {
	msg = "doPGPtxt: invalid number of parameters (!=5)";
	return retval;
    }

    uInt                    i;
    Double                  dval[4];
    String                  sval;
    GlishArray              gval[5];
    GlishArray::ElementType desired[5] = { GlishArray::DOUBLE, GlishArray::DOUBLE, 
					       GlishArray::DOUBLE, GlishArray::DOUBLE,
					       GlishArray::STRING  
    };

    for( i=0; i<5; i++ )
    {
	GlishValue    arg;
	arg = rec.get(i);
	
	if( arg.type()!=GlishValue::ARRAY )
	{
	    break;
	}

	gval[i] = GlishArray( arg );

	if( gval[i].elementType()!=desired[i] || gval[i].nelements()!=1 )
	{
	    break;
	}
    }

    if( i<5 )
    {
	msg = "doPGPtxt: error in arguments. Not 4 floats + 1 string but something else!";
	return retval;
    }

    
    gval[0].get( dval[0] );
    gval[1].get( dval[1] );
    gval[2].get( dval[2] );
    gval[3].get( dval[3] );
    gval[4].get( sval );

    
    ::cpgptxt( (float)dval[0], (float)dval[1], (float)dval[2], (float)dval[3], sval.chars() );
    evsrc.postEvent( drEN(ev), "doPGPtxt: Text p'd :-)" );
    return True;
}

Bool doPGVstd( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool   retval = False;
    
    if( openPGDevices.nrOpenDevices()>0 )
    {
		retval = True;
		::cpgvstd();

		evsrc.postEvent( drEN(ev), "doPGVstd: standard viewport Ok..." );
    }
    else
    {
		msg = "doPGVstd: No device to set standard viewport on...";
    }
    return retval;        
}

Bool doPGScf( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
		msg = "doPGScf: No open devices";
		return retval;
    }

	//
	// The characterfont
	//
    Int     characterfont;

    if( ev.val().type()==GlishValue::RECORD )
    {
		msg = "doPGScf: EINVAL! Int expected got record!";
		return retval;
    }

    GlishArray   arg( ev.val() );

    if( arg.elementType()!=GlishArray::INT || arg.nelements()!=1 )
    {
		msg = "doPGScf: EINVAL! One INT expected got array of other type?!";
		return retval;	
    }

    arg.get(characterfont);

	if( characterfont<1 || characterfont>4 )
	{
		msg = "doPGScf: EINVAL! new font out of range (it is (<1 || >4))!!";
		return retval;
	}
    
	::cpgscf( (int)characterfont );

    evsrc.postEvent( drEN(ev), "doPGScf: Character font set" );

    return True;    
}


Bool doPGQcf( GlishSysEvent& ev, GlishSysEventSource& evsrc, String& msg )
{
    Bool    retval( False );

    //
    //  If no open devices: nothing to do!
    //
    //  Silently ignore this one....
    //
    if( openPGDevices.nrOpenDevices()==0 )
    {
		msg = "doPGQcf: No open devices";
		return retval;
    }
    

   	int    characterfont;
 
    //
    //  Cal cpgqcf
    //
    ::cpgqcf( &characterfont );

    //
    //  Transform into Int 
    //
	Int     aipsint( (Int)characterfont );   
 
    evsrc.postEvent( drEN(ev), GlishArray(aipsint) );
    return True;
}
