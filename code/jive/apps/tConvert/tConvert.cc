#include <iostream>

#include <casa/BasicSL/String.h>

#include <jive/ms2uvfitsimpl/Converters/FileFormatConverter.h>
#include <jive/ms2uvfitsimpl/Converters/ms2uvfitsConverter.h>


//  Splitfunctions
#include <jive/hvplugin/rowSplit.h>
#include <jive/hvplugin/sizeSplit.h>

#include <signal.h>
#include <libgen.h>

#include <sstream>

#ifndef BUILDTIME
#define BUILDTIME "<no buildtime info present>"
#endif

using namespace casa;


// define built-in defaults
static const char*      default_conversion = "m2i";
static const char*      default_split      = "size";

void Usage( const String& prgname )
{
    cout << "Usage: " << prgname << " [option] <source> <destination>" << endl;
    cout << "       where option can be:" << endl;
    cout << endl
         << "       Splitting options:" << endl
	 << "         -row  -> Split MS into a number of (roughly) equal" << endl
         << "                  pieces" << endl
         << "         -size -> Split MS into pieces of 2GB/piece" << endl
         << endl
         << "       Conversion options:" << endl;
    cout << "         -m2f  -> conversion from MS (source) to FITSfile" << endl
         << "                  (UVFITS for AIPS)" << endl;
    cout << "         -m2i  -> conversion from MS (source) to IDI FITSfile" << endl
         << "                  (VLBA format)" << endl;
    cout << "         -wm2f -> conversion from Westerbork MS (source)" << endl
         << "                  to FITSfile (destination)" << endl;
    cout << "         -fuzz SECONDS -> Extend scans for up to SECONDS at each end" << endl;
    cout << endl
         << "Defaults are:   '" << default_conversion << "' and '"
         << default_split << "' splitting" << endl;
    //cout << "         -v    -> show lots of info (if apropriate)" << endl;
    return;
}

void throwonsignal( int signalnr )
{
    ostringstream   buf;
    
    buf << endl << "**** Caught a signal #" << signalnr << "..." << endl;
    
    throw( AipsError(buf.str()) );
}



int main( int, char** argv )
{
    int                   retval( 1 );
    int                   errorcount( 0 );
    String                thisprogname;
    String                infile;
    String                outfile;
    String                method( default_conversion );
    String                splitter( default_split );
    fragmentfn            fptr = 0;
    FileFormatConverter*  ptr2converter( 0 );
    
#ifdef BUILDTIME
    char*                 thisexe;
    
    if( (thisexe=::basename(argv[0]))==0 )
    {
	 thisexe = argv[0];
    }
    cout << "\n"
	 << thisexe << ": Version of " << BUILDTIME << " begins..."
	 << "\n\n" 
	 << flush;
#endif
    
    //
    //  Set the handler for various signals!
    //
//    sigset( SIGSEGV, throwonsignal );
    sigset( SIGBUS, throwonsignal );
    sigset( SIGINT, throwonsignal );
    sigset( SIGKILL, throwonsignal );
    

    //
    //  Get the name of this program
    //
    thisprogname=*argv;
    
    if( !*(++argv) )
    {
        Usage( thisprogname );
        return retval;
    }

    while( *argv )
    {
        String    curarg( *argv );
        
        if( curarg[0]!='-' )
        {
            //
            //  Must be a filename?
            //
            if( !(infile.length()) )
            {
                infile=curarg;
            }
            else if( !(outfile.length()) )
            {
                outfile=curarg;
            }
            else
            {
                cout << "You supplied too much arguments. This one - " << curarg 
                     << " - will be discarded." << endl;
                errorcount++;
            }
        }
        else
        {
            //
            //  Check if valid option:
            //
            String     opt( curarg.after(0) );
            
            if( opt=="m2f" || opt=="wm2f" || opt=="m2i" || opt=="wm2i" ) {
                method = opt;
            }
            else if( opt=="row" || opt=="size" ) {
                  splitter = opt;
            }
	    else if( opt=="arg" )
	    {
	        MSConversionInfo::setFuzz(atof(*++argv));
	    }
            else
            {
                cout << "Unrecognized option: " << opt << ". Ignoring." << endl;
            }
        }

        //
        //  It might be an idea to increase the arg-counter....
        //
        argv++;
    }
    
    if( !infile.length() )
    {
        cout << "Hmm... you specified no inputfile!" << endl;
        errorcount++;
    }
    
    if( !outfile.length() )
    {
        cout << "What? No outputfile?!" << endl;
        errorcount++;
    }
    

    if( errorcount!=0 )
    {
        cout << endl << "------------ Summary ------------" << endl;
        cout << errorcount << " error" << ((errorcount==1)?(" was"):("s were")) 
             << " detected. Now showing what you should have done:" << endl;
        Usage( thisprogname );
        return errorcount;
    }

    // if specified, 
	if( splitter=="row" )
	{
	     fptr = rowSplit;
	}
	else if( splitter=="size" )
	{
	     fptr = sizeSplit;
	}
    
    // Depending on what the user entered, create the appropriate
    // transformation!

    
    
    if( method=="m2f" || method=="wm2f" )
    {
        MS2UVF::generatorlist_t   hdustodo;

        //  For both these transformations,
        //  the following HDUs need to be 
        //  written
        hdustodo.push_back( "maingroup" );//MS2UVF::mainprimarygroup
        hdustodo.push_back( "antab" );//MS2UVF::antennabinary
        hdustodo.push_back( "fqtab" );//MS2UVF::freqbinary
        hdustodo.push_back( "sutab" );//MS2UVF::sourcebinary

        if( method=="wm2f" )
        {
            // for westerbork, add the following
        	hdustodo.push_back( "tytab" ); //MS2UVF::tsysbinary
        	hdustodo.push_back( "gctab" );//MS2UVF::gaincurvebinary
        }

        // now create the actual converter
        ptr2converter = new ms2uvfitsConverter(hdustodo, fptr);
    }
    else if( method=="m2i" || method=="wm2i" )
    {
        // convert MS to IDI fits
        MS2UVF::generatorlist_t   hdustodo;

    	hdustodo.push_back( "idihdr" );//MS2UVF::idiheader
    	hdustodo.push_back( "arraygeometry" );//MS2UVF::arraygeometry
    	hdustodo.push_back( "idifrequency" );//MS2UVF::idifrequency
    	hdustodo.push_back( "idisource" );//MS2UVF::idisource
    	hdustodo.push_back( "idiantenna" );//MS2UVF::idiantenna

	hdustodo.push_back( "idiFlag" );
	hdustodo.push_back( "idiGainCurve" );
	hdustodo.push_back( "idiSysTemp" );

	if( method=="wm2i" )
	{
	     // for westerbork, add the following
	     hdustodo.push_back( "idity" ); 
	     hdustodo.push_back( "idigainc" ); 
	}

    	hdustodo.push_back( "uvtable" );//MS2UVF::uvtable

    	//  Set up the conversion
        ptr2converter = new ms2uvfitsConverter(hdustodo, fptr);
    }    
    else
    {
        cout << "AArrgghh - invalid conversion format ("
             << method << ") encountered." << endl;
        return 1;
    }
    

    //
    //  Do the transformation....
    //
    if( !(ptr2converter->convert(outfile, infile)) )
    {
        cout << "Rats. Tranformation not succeeded" << endl;
    }
    else
    {
        retval=0;
    }
    
    //
    //  Make sure the transformer is deleted correctly
    //
    delete ptr2converter;
    

    return retval;
}
