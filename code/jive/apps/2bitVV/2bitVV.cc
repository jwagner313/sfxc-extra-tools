// Compute 2-bit Van Vleck corrected autocorrelations and cross
// correlations from data correlated with Albert's normalization
// turned off.  In that case, the peak of the autocorrelation function
// is linear with the sum-of-highs sampler statistics.
//
// To avoid doing useless corrections on meaningless data or
// meaningless corrections on useless data, this program does two
// things:
//
// 1. We set a threshold on the weight.  Data with a weight below
// won't be trusted and will not be used to do any corrections.  The
// data, and the associated baselines, will be flagged.  This
// threshold should be (significantly) less than the normal
// weight-flagging threshold because of its follow-on effect on the
// baselines; we're really trying to cut out only the data that has
// truly messed up autocorrelations.  The default value for the
// threshold is 0.1.
//
// 2. We set a threshold on the fraction of highs.  The
// autocorrelation will essentially be divided by this fraction, thus
// any uncertainties in the Chebyshev fit will be magnified when
// dividing by a small number.  Again, data below the threshold will
// be flagged, together with the associated baselines.  The default
// value for the threshold is 0.01.
//
// Writen by Mark Kettenis <kettenis@jive.nl>, modelled after the
// Glish implementation written by Bob Campbell.
//

#define RCSID(string) \
  static char rcsid[] __attribute__ ((unused)) = string

RCSID("@(#) $Id: 2bitVV.cc,v 1.10 2007/07/02 09:17:40 jive_cc Exp $");

#include <casa/aips.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/Slicer.h>
#include <casa/BasicSL/Complex.h>
#include <casa/Utilities/Assert.h>
#include <measures/Measures/Stokes.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSMainColumns.h>
#include <ms/MeasurementSets/MSDataDescColumns.h>
#include <ms/MeasurementSets/MSPolColumns.h>
#include <ms/MeasurementSets/MSSpWindowColumns.h>
#include <scimath/Functionals/Chebyshev.h>
#include <scimath/Mathematics/FFTServer.h>
#include <tables/Tables/TableIter.h>

using namespace casa;

#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <sstream>

// Chebyshev coefficients from Sergei Pogrebenko's model for the JIVE
// MkIV correlator.
//
// According to this model, the corrected autocorrelation lags are
// given by:
//
// l' = \sum_{i=0}^8 T_{2i+1}(l) \sum_{j=0}^7 A_ji T_j(2 H - 1)
//
// where H is the fraction of highs from the sampler statistics.
//
// The corrected cross correlation frequencies are given by:
//
// \nu' = \nu \sum_{i=0}^7 T_i(2 H_1 - 1) \sum_{j=0}^7 X_ij T_j(2 H_2 - 1)
//
// where H_1 and H_2 represent the fraction of highs from the
// autocorrelations for the stations involved.

// Coefficients for the autocorrelations.
static const Float A[8][9] = 
{
  { 1.032665793815168, -0.01732908087630967, -0.003844421542703819,
    -0.008402949208145483, -0.003033994797041067, 0.0004313386023294604,
    0.0005568285050523292, -0.0005028791279229674, -0.0005406353704303605 },
  { 0.07832554226377696, -0.1076847848853732, 0.0144859628308445,
    0.01501426481820907, 0.003938149031587746, -0.002390146604787552,
    -0.002142562168059246, 9.035849102993823e-005, 0.0003632162227757399 },
  { 0.02209850805081083, -0.007961336312873999, -0.01182153827977801,
    -0.009251587604818154, 0.000676315458471611, 0.00450599552793709,
    0.002688075703829784, -0.000221546383240486, -0.0007128861603429883 },
  { 0.0001470177932475087, -0.005239272280793473, 0.007869740666935091,
    0.003097364409235589, -0.003113243958094692, -0.004024735778866074,
    -0.001299503409448733, 0.00130198254919081, 0.001260650008599806 },
  { 0.0004909699813484183, 0.0007394262819011311, -0.003440719273197225,
    -0.0007005899840750695, 0.002330244130882245, 0.001969090321012095,
    -0.0001177658149164948, -0.001145520072237565, -0.0001251355707212321 },
  { -0.0001256963483423657, -0.0006614840231310576, 0.001660638293939432,
    0.0001218560230109953, -0.001151833553538604, -0.0006513170943591845,
    0.0004964412237052472, 0.0006748722472079076, -0.0003634767684892089 },
  { 7.322288747183457e-005, 9.607478479592871e-005, -0.000601735457245428,
    3.634151839393394e-005, 0.000559254991538658, 0.0002443456901365588,
    -0.0002931540111729214, -0.0003132744981161893, 0.0001989240941952 },
  { -5.239271897257909e-005, -1.675382777209609e-005, 0.0002102118094693243,
    2.270797776407764e-005, -0.0002190149951733952, -9.858814176599213e-005,
    0.000106245366235056, 0.0001281691459664865, -8.058461574984117e-005 }
};

// Coefficients for the cross correlattions.
static const Float X[8][8] =
{
  { 1.470052048634628, -1.060891235180328, 0.6217752143639374,
    -0.3483588667285216, 0.1816734392299436, -0.1032619667858136,
    0.04122932437016719, -0.02087207155990624 },
  { -1.060891235180328, 1.124063859939623, -0.73397816327933,
    0.4621602767930924, -0.2603611928741569, 0.1541383620300146,
    -0.06430759303370136, 0.0320379686034864 },
  { 0.6217752143639374, -0.73397816327933, 0.488842916529214,
    -0.3239523347570117, 0.1817033897529823, -0.1132489469463761,
    0.04634992277805496, -0.02525814362290116 },
  { -0.3483588667285216, 0.4621602767930924, -0.3239523347570117,
    0.218974338972309, -0.1306044649233638, 0.07961551148757223,
    -0.03458768856217363, 0.01718394543983015 },
  { 0.1816734392299436, -0.2603611928741569, 0.1817033897529823,
    -0.1306044649233638, 0.0750865777010622, -0.04969899346132201,
    0.02041106583074713, -0.01204501778439738 },
  { -0.1032619667858136, 0.1541383620300146, -0.1132489469463761,
    0.07961551148757223, -0.04969899346132201, 0.03073128839579505,
    -0.01352424620356517, 0.006779404691905133 },
  { 0.04122932437016719, -0.06430759303370136, 0.04634992277805496,
    -0.03458768856217363, 0.02041106583074713, -0.01352424620356517,
    0.005693543947469367, -0.003256033748110735 },
  { -0.02087207155990624, 0.0320379686034864, -0.02525814362290116,
    0.01718394543983015, -0.01204501778439738, 0.006779404691905133,
    -0.003256033748110735, 0.001376081987123136 }
};

// Albert's magic number.
static const Float magic = 0.364;


// Iterator for sets of Stokes types.
typedef std::set<Stokes::StokesTypes>::const_iterator stokesIterator;

// Map for data description IDs.
typedef std::map<Int, Int> dataDescMap;

// Map types for receptor-related information.
//

// Key type.
struct receptorKey
{
  Int antennaId;
  Int dataDescId;
  Stokes::StokesTypes stokesType;
};

// Key comparision functor.
struct receptorKeyCompare
{
  // Return true if K is less than L.
  //
  bool operator()(receptorKey k, receptorKey l) const
    {
      return ((k.antennaId == l.antennaId)
	      ? ((k.dataDescId == l.dataDescId)
		 ? k.stokesType < l.stokesType
		 : k.dataDescId < l.dataDescId)
	      : k.antennaId < l.antennaId);
    }
};

typedef std::map<receptorKey, Float, receptorKeyCompare> receptorMap;
typedef std::map<receptorKey, Vector<Float>, receptorKeyCompare> receptorVMap;

// Map type for correlators.
//

// Key type.
struct corrKey
{
  Int dataDescId;
  Stokes::StokesTypes stokesType;
};

// Key comparision functor.
struct corrKeyCompare
{
  // Return true if K is less than L.
  //
  bool operator()(corrKey k, corrKey l) const
    {
      return ((k.dataDescId == l.dataDescId)
	      ? k.stokesType < l.stokesType
	      : k.dataDescId < l.dataDescId);
    }
};

typedef std::map<corrKey, Int, corrKeyCompare> corrMap;


// Implement VanVleck correction algorithm.

class VanVleckCorrector
{
public:
  VanVleckCorrector(MeasurementSet& ms,
		    Float minWeight = 0.1, Float minHighFrac = 0.01);

  uInt correct(bool verbose = false);

private:
  MeasurementSet& ms;
  Float minWeight;
  Float minHighFrac;

  // Correct autocorrelations.
  static vector<Chebyshev<Float> > chebA;
  static void initChebA();
  static Vector<Float> autoCoeffs(Float highFrac);
  static void autoCorrect (Float highFrac, Vector<Complex>& lags);
  static Vector<Complex> frequencyToLag(Vector<Complex>& freqs,
					Int netSideband);
  static Vector<Complex> lagToFrequency(Vector<Complex>& lags,
					Int netSideband);

  // Correct cross correlations.
  static vector<Chebyshev<Float> > chebX;
  static void initChebX();
  static Vector<Float> crossCoeffs(Float highFrac);
  static void crossCorrect (Float highFrac, Vector<Float> innerSum,
			    Vector<Complex>& freqs);

  static std::set<Stokes::StokesTypes> parStokes;
  static std::set<Stokes::StokesTypes> crossStokes;
  static std::set<Stokes::StokesTypes> allStokes;
  static void initStokes();

  mutable dataDescMap nsbCache;
  Int getNetSideband(Int dataDescId) const;

  mutable corrMap corrCache;
  Int getCorrelatorId(Int dataDescId, Stokes::StokesTypes stokesType) const;
};


// Constructor: build a Van Vleck corrector for a given
// MeasurementSet, setting the thresholds for the minimum weight and
// minimum fraction of highs.
//
VanVleckCorrector::VanVleckCorrector(MeasurementSet& ms,
				     Float minWeight, Float minHighFrac)
  : ms(ms), minWeight(minWeight), minHighFrac(minHighFrac)
{
  // Initialize Chebyshev polynomials.
  initChebA();
  initChebX();

  //Initialize Stokes types.
  initStokes();
}


// Return the correlator identifier (used as an index for data matrix,
// weight, etc.) corresponding to a given data description identifier
// and Stokes type.  Return -1 if a correlator for the given Stokes
// type (or data description identifier) doesn't exist.
//
Int
VanVleckCorrector::getCorrelatorId(Int dataDescId,
				   Stokes::StokesTypes stokesType) const
{
  struct corrKey key = { dataDescId, stokesType };

  if (corrCache.count(key) > 0)
    return corrCache[key];

  corrCache[key] = -1;

  ROMSDataDescColumns ddc(ms.dataDescription());
  ROMSPolarizationColumns pc(ms.polarization());

  Int polarizationId = ddc.polarizationId()(dataDescId);
  Vector<Int> corrType = pc.corrType()(polarizationId);

  for (uInt id = 0; id < corrType.nelements(); id++)
    if (corrType(id) == stokesType)
      {
	corrCache[key] = id;
	break;
      }

  return corrCache[key];
}

// Return the sideband corresponding to a given data description
// identifier.
//
Int
VanVleckCorrector::getNetSideband(Int dataDescId) const
{
  if (nsbCache.count(dataDescId) > 0)
    return nsbCache[dataDescId];

  ROMSDataDescColumns ddc(ms.dataDescription());
  ROMSSpWindowColumns spwc(ms.spectralWindow());

  Int spWindowId = ddc.spectralWindowId()(dataDescId);
  nsbCache[dataDescId] = spwc.netSideband()(spWindowId);

  return nsbCache[dataDescId];
}


// Return the sign of a permutation.
//
inline Float
sign (uInt permutation)
{
  return ((permutation % 2) ? 1.0 : -1.0);
}

// Return the lags corresponding to the given frequencies and sideband
// as a vector.  Should only be used for autocorrelations.
//
Vector<Complex>
VanVleckCorrector::frequencyToLag(Vector<Complex>& freqs, Int netSideband)
{
  uInt numFrequencies = freqs.nelements();
  uInt numLags = numFrequencies * 2;
  Vector<Complex> lags(numLags);
  FFTServer<Float, Complex> ffts;

  if (netSideband == 1)
    {
      // USB.
      for (uInt i = 1; i < numFrequencies; i++)
	lags[i] = freqs[numFrequencies - i];
      for (uInt i = numFrequencies; i < numLags; i++)
	lags[i] = freqs[i - numFrequencies];
    }
  else
    {
      // LSB.
      for (uInt i = 1; i < (numFrequencies + 1); i++)
	lags[i] = freqs[i - 1];
      for (uInt i = (numFrequencies + 1); i < numLags; i++)
	lags[i] = freqs[numLags - i - 1];
    }

  // Manipulate the first frequency such that the resulting first lag
  // (after Fourier transform) will be zero.
  lags[0] = -lags[numFrequencies];
  for (uInt i = 1; i < numFrequencies; i++)
    lags[0] += 2 * sign(i) * lags[i];

  // Use a complex FFT since that allows to do the transform in-place.
  // A real FFT probably wouldn't be faster; that was indeed the case
  // in the Glish version of this code.
  ffts.fft(lags, False);

  // Verofy that thefirst lag is indeed zero.  Don't test for real
  // equality since there will be some numerical noise.
  DebugAssertExit(nearAbs (lags[0], 0));
  return lags;
}

// Return the frequencies corresponding to the given lags and sideband
// as a vector.  Should only be used for autocorrelations.
//
Vector<Complex>
VanVleckCorrector::lagToFrequency(Vector<Complex>& lags, Int netSideband)
{
  uInt numLags = lags.nelements();
  uInt numFrequencies = numLags / 2;
  Vector<Complex> freqs(numFrequencies);
  FFTServer<Float, Complex> ffts;

  ffts.fft(lags, True);

  if (netSideband == 1)
    {
      // USB.
      for (uInt i = 0; i < numFrequencies; i++)
	freqs[i] = lags[numFrequencies + i];
    }
  else
    {
      // LSB.
      for (uInt i = 0; i < numFrequencies; i++)
	freqs[i] = lags[i + 1];
    }

  return freqs;
}

// Initialize Stokes types.
//
std::set<Stokes::StokesTypes> VanVleckCorrector::parStokes;
std::set<Stokes::StokesTypes> VanVleckCorrector::crossStokes;
std::set<Stokes::StokesTypes> VanVleckCorrector::allStokes;

void
VanVleckCorrector::initStokes()
{
  if (allStokes.size() == 0)
    {
      // Parallel polarizations.
      parStokes.insert(Stokes::LL);
      parStokes.insert(Stokes::RR);

      // Cross polarizations.
      crossStokes.insert(Stokes::RL);
      crossStokes.insert(Stokes::LR);

      // All polarizations.
      allStokes.insert(parStokes.begin(), parStokes.end());
      allStokes.insert(crossStokes.begin(), crossStokes.end());
    }
}


// Initialize Chebyshev coefficients for correcting the autocorrelations.
//
vector<Chebyshev<Float> > VanVleckCorrector::chebA;

void
VanVleckCorrector::initChebA()
{
  if (chebA.size() == 0)
    {
      uInt numRows = sizeof A / sizeof A[0];
      uInt numColumns = sizeof A[0] / sizeof A[0][0];

      chebA.resize(numColumns);
      for (uInt i = 0; i < numColumns; i++)
	{
	  Vector<Float> coeffs(numRows);

	  for (uInt j = 0; j < numRows; j++)
	    coeffs[j] = A[j][i];

	  chebA[i].setCoefficients(coeffs);
	}
    }
}

// Return the Chebyshev coefficients (the inner sum) for correcting
// the autocorrelations at the given fraction of highs.
//
Vector<Float>
VanVleckCorrector::autoCoeffs(Float highFrac)
{
  static uInt numTerms = chebA.size();
  Vector<Float> coeffs(2 * numTerms, 0.0);

  for (uInt i = 0; i < numTerms; i++)
    coeffs[2 * i + 1] = chebA[i](2 * highFrac - 1);

  return coeffs;
}

// Correct the autocorrelation lags for the given fraction of highs.
//
void
VanVleckCorrector::autoCorrect (Float highFrac, Vector<Complex>& lags)
{
  Chebyshev<Float> cheb;
  cheb.setCoefficients(autoCoeffs(highFrac));

  // Correct all individual lags.  Note that we throw away the
  // imaginary part if each lag.  The lags should be real, and the
  // imaginary part should only contain some numerical noise from the
  // FFT.  Ignoring it should be safe.
  for (uInt i = 0; i < lags.nelements(); i++)
    lags[i] = cheb(real(lags[i]));
}


// Initialize Chebyshev coefficients for correcting the cross
// correlations.
//
vector<Chebyshev<Float> > VanVleckCorrector::chebX;

void
VanVleckCorrector::initChebX()
{
  if (chebX.size() == 0)
    {
      uInt numTerms = sizeof X / sizeof X[0];

      DebugAssertExit(numTerms == sizeof X[0] / sizeof X[0][0]);

      chebX.resize(numTerms);
      for (uInt i = 0; i < numTerms; i++)
	{
	  Vector<Float> coeffs(numTerms);

	  for (uInt j = 0; j < numTerms; j++)
	    coeffs[j] = X[i][j];

	  chebX[i].setCoefficients(coeffs);
	}
    }
}

// Return the Chebyshev coefficients (the inner sum) for correcting
// the cross correlations at the given fraction of highs.
//
Vector<Float>
VanVleckCorrector::crossCoeffs(Float highFrac)
{
  static uInt numTerms = chebX.size();
  Vector<Float> coeffs(numTerms);

  for (uInt i = 0; i < numTerms; i++)
    coeffs[i] = chebX[i](2 * highFrac - 1);

  return coeffs;
}

// Correct the cross correlation frequenciess for the given fraction
// of highs corresponding to the first station and the pre-calculated
// inner sum corresponding to the second station.
//
void
VanVleckCorrector::crossCorrect (Float highFrac, Vector<Float> innerSum,
				 Vector<Complex>& freqs)
{
  Chebyshev<Float> cheb;
  cheb.setCoefficients(innerSum);

  freqs *= Complex(cheb(2 * highFrac - 1));
}


// Flag the specified correlator.
//
static void
flagCorrelator(MSMainColumns& msc, uInt row, Int correlatorId)
{
  // We flag the data by making its weight negative.  That way the
  // (raw) values survive, but the data is clearly invalid.  This
  // isn't quite the right thing to do.  We should properly flag the
  // data in the FLAG_CATEGORY column, and propagate it into the FLAG
  // column from there.
  //
  Vector<Float> weight = msc.weight()(row);
  weight[correlatorId] = -abs(weight[correlatorId]);
  msc.weight().put(row, weight);
}


// Perform a Van Vleck correction, performing progress reporting if
// asked to be verbose.  Return the total number of MeasurementSet
// rows processed.
//
uInt
VanVleckCorrector::correct (bool verbose)
{
  ROMSDataDescColumns ddc(ms.dataDescription());
  ROMSPolarizationColumns pc(ms.polarization());
  uInt totalRows = ms.nrow();
  stokesIterator stokes;

  // Check for unknown polarizations.
  for (uInt row = 0; row < pc.nrow(); row++)
    {
      Vector<Int> corrType = pc.corrType()(row);

      for (uInt id = 0; id < corrType.nelements(); id++)
	{
	  if (allStokes.count (Stokes::StokesTypes(corrType(id))) > 0)
	    continue;

	  std::stringstream msg;
	  msg << "Cannot handle Stokes type " << corrType(id);
	  throw(AipsError(msg.str()));
	}
    }

  // Iterate over data blocks corresponding to the same time.
  Block<String> iterKeys(1);
  iterKeys[0] = "TIME";
  TableIterator iter(ms, iterKeys, TableIterator::DontCare,
		     TableIterator::NoSort);
  uInt numIterations = 0;
  uInt numRows = 0;

  while (!iter.pastEnd())
    {
      MeasurementSet ms = iter.table();
      MSMainColumns msc(ms);

      // We calculate these values when looping over the
      // autocorrelations.
      receptorMap highFrac;
      receptorVMap innerSum;

      // Loop over all autocorrelations.  Simply iterating over all
      // rows and checking whether ANTENNA1 and ANTENNA2 are equal
      // seems to be faster than explicitly selecting the
      // autocorrelations using TableExprNode.
      for (uInt row = 0; row < msc.nrow(); row++)
	if (msc.antenna1()(row) == msc.antenna2()(row))
	  {
	    Int antennaId = msc.antenna1()(row);
	    Int dataDescId = msc.dataDescId()(row);
	    Int sideband = getNetSideband(dataDescId);
	    const Vector<Float>& weight = msc.weight()(row);

	    // Loop over parallel polarizations.
	    for (stokes = parStokes.begin();
		 stokes != parStokes.end(); stokes++)
	      {
		Int correlatorId = getCorrelatorId(dataDescId, *stokes);

		// Skip this polarization if there's no
		// corresponding data in the MeasurementSet.
		//
		if (correlatorId == -1)
		  continue;

		// We shouldn't see the same correlator twice.
		// ??? Shouldn't we handle this more gracefully?
		//
		receptorKey key = { antennaId, dataDescId, *stokes };
		AlwaysAssertExit (highFrac.count(key) == 0);

		// Don't bother if the weight's too low; if we
		// don't trust this data point, we shouldn't use
		// it to correct other datapoints.  Note that this
		// will cause all associated baselines to be
		// flagged.
		//
		if (weight[correlatorId] < minWeight)
		  {
		    flagCorrelator(msc, row, correlatorId);
		    highFrac[key] = floatNaN();
		    continue;
		  }

		// Fetch the data for this correlator as a vector.
		IPosition start(2, correlatorId, 0);
		IPosition length(2, 1, Slicer::MimicSource);
		Slicer slice(start, length);
		Vector<Complex> freqs =
		  msc.data().getSlice(row, slice).nonDegenerate();

		// Convert from fequency domain to lag domain.
		Vector<Complex> lags = frequencyToLag(freqs, sideband);

		// The autocorrelation peak always corresponds to
		// the center lag.  Use it to normalize the lags.
		Float maxLag = real(lags[lags.nelements() / 2]);
		lags /= Complex(maxLag);

		// The fraction of highs according to a linear
		// model.  Remember for correcting the cross
		// correlations.  If the fraction of highs is too
		// low, we shouldn't trust this data point and
		// shouldn't use it to correct other data points.
		// Again, this will cause all associated baselines
		// to be flagged.
		highFrac[key] = maxLag * magic;
		if (highFrac[key] < minHighFrac || highFrac[key] > 1.0)
		  {
		    flagCorrelator(msc, row, correlatorId);
		    highFrac[key] = floatNaN();
		    continue;
		  }

		// Correct the lags.
		autoCorrect(highFrac[key], lags);

		// Convert back from lag domain to frequency domain.
		freqs = lagToFrequency(lags, sideband);

		// Write the data back into the MeasurementSet.
		IPosition shape(2, 1, freqs.nelements());
		msc.data().putSlice(row, slice, freqs.reform(shape));

		// Pre-calculate the inner sum for the cross
		// correlations.
		innerSum[key] = crossCoeffs(highFrac[key]);
	      }
	  }

      // Loop over all correlations, leaving out the true
      // autocorrelations.
      for (uInt row = 0; row < msc.nrow(); row++)
	{
	  Int antenna1 = msc.antenna1()(row);
	  Int antenna2 = msc.antenna2()(row);
	  Int dataDescId = msc.dataDescId()(row);
	  stokesIterator begin, end;

	  if (msc.antenna1()(row) == msc.antenna2()(row))
	    {
	      // Loop over cross polarizations.  The parallel
	      // polarizations represent autocorrelations which
	      // have been treated above.
	      //
	      begin = crossStokes.begin();
	      end = crossStokes.end();
	    }
	  else
	    {
	      // Loop over all polarizations.
	      //
	      begin = allStokes.begin();
	      end = allStokes.end();
	    }

	  for (stokes = begin; stokes != end; stokes++)
	    {
	      Int correlatorId = getCorrelatorId(dataDescId, *stokes);

	      // Skip this polarization if there's no
	      // corresponding data in the MeasurementSet.
	      if (correlatorId == -1)
		continue;

	      // Fetch the data for this correlator as a vector.
	      IPosition start(2, correlatorId, 0);
	      IPosition length(2, 1, Slicer::MimicSource);
	      Slicer slice(start, length);
	      Vector<Complex> freqs =
		msc.data().getSlice(row, slice).nonDegenerate();

	      // Reduce Stokes type to the appropraite parallel
	      // polarizations for the individual stations.
	      //
	      Stokes::StokesTypes stokes1, stokes2;
	      switch (*stokes)
		{
		case Stokes::RR:
		case Stokes::LL:
		  stokes1 = stokes2 = *stokes;
		  break;
		case Stokes::RL:
		  stokes1 = Stokes::RR;
		  stokes2 = Stokes::LL;
		  break;
		case Stokes::LR:
		  stokes1 = Stokes::LL;
		  stokes2 = Stokes::RR;
		  break;
		default:
		  stokes1 = stokes2 = Stokes::Undefined;
		  AlwaysAssertExit(!"Unknown Stokes type");
		  break;
		}

	      // We should have seen the autocorrelation for both
	      // receptors that are part of this correlator.
	      // ??? Shouldn't we handle this more gracefully?
	      //
	      receptorKey key1 = { antenna1, dataDescId, stokes1 };
	      receptorKey key2 = { antenna2, dataDescId, stokes2 };
	      AlwaysAssertExit(highFrac.count(key1) > 0);
	      AlwaysAssertExit(highFrac.count(key2) > 0);

	      if (isNaN(highFrac[key1]) || isNaN(highFrac[key2]))
		{
		  flagCorrelator(msc, row, correlatorId);
		  continue;
		}

	      // Do the correction.  Much simpler than the
	      // autocorrelations since we don't need to do a
	      // Fourier transform.
	      //
	      crossCorrect(highFrac[key1], innerSum[key2], freqs);

	      // Write the data back into the MeasurementSet.
	      IPosition shape(2, 1, freqs.nelements());
	      msc.data().putSlice(row, slice, freqs.reform(shape));
	    }
	}

      // Progress reporting.
      numRows += ms.nrow();
      numIterations++;
      if (verbose && (numIterations % 16) == 0)
	std::cout << "\r" << "Processing row "
		  << numRows << "/" << totalRows << flush;

      iter.next();
    }

  if (verbose)
    std::cout << "\r" << "Processed "
	      << numRows << "/" << totalRows << " rows." << endl;

  return numRows;
}


int
main(int argc, char **argv)
{
  bool fail = false;
  bool force = false;
  bool verbose = true;
  bool usage = false;
  double minWeight = 0.1;
  double minHighFrac = 0.01;
  int c;

  while ((c = ::getopt(argc, argv, "fH:sW:")) != EOF)
    {
      switch (c)
	{
	case 'f':
	  force = true;
	  break;
	case 'H':
	  minHighFrac = ::atof(optarg);
	  break;
	case 's':
	  verbose = false;
	  break;
	case 'W':
	  minWeight = ::atof(optarg);
	  break;
	case '?':
	  usage = true;
	  break;
	}
    }

  if (usage || optind != (argc - 1))
    {
      std::cerr << "usage: " << argv[0] << " "
		<< "[-f|-s] [-H minhst] [-W minwgt] ms" << endl;
      return EXIT_FAILURE;
    }

  // Check the validity of the arguments.

  if (minWeight < 0.05 || minWeight > 0.4)
    {
      std::cerr << "Warning: minwgt is " << minWeight << ". " << endl
		<< "If this is really what you want, please use"
		<< " the -f option." << endl;
      fail = !force;
    }

  if (minHighFrac < 0.01 || minHighFrac > 0.5)
    {
      std::cerr << "Warning: minhst is " << minHighFrac << ". " << endl
		<< "If this is really what you want, please use"
		<< " the -f option." << endl;
      fail = !force;
    }

  if (fail)
    return EXIT_FAILURE;

  try
    {
      MeasurementSet ms(argv[optind], Table::Update);
      VanVleckCorrector vvc(ms, minWeight, minHighFrac);

      // Perform Van Vleck correction.
      vvc.correct(verbose);

      if (verbose)
	std::cout << "Flushing... " << flush;
      
      // ??? Without this explicit flush, the changes will not be
      // written out into the MeasurementSet.  Why?
      ms.flush();

      if (verbose)
	std::cout << "done." << endl;
    }

  catch (AipsError error)
    {
      std::cerr << error.getMesg() << endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
