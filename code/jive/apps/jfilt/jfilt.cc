//
// jfilt: Program to filter out duplicate baselines when doing 16+ station correlation
// with the JIVE correlator. In which case the whole stuff must be correlated
// in three passes.
//
//
// The main idea is that we want to keep one datastream per duplicate baseline.
// One datastream = one correlation pass (or job, in JOps terminology)
//
// So, at minimum, we must be able to discriminate between visibilities from
//   different passes (jobs). We can. The processorId column in main now points
//   to a row in the processor table, the indexed row contains the actual passId
//   (in our case, the jobid). However, suffice it to say that we can use
//   processorId as unique 'visibility-source-identifier'....
//
// But... it's a necessary but not sufficient condition: we must be able to 'pair'
//   two or more datastreams representing the same correlated data. (remember, *all*
//   correlation jobs are unique but that's not what we're interested in right now).
//   We must know which jobids out of all the unique jobids represent the same
//   slice in time. We cannot directly relate that to individual visibility times
//   since there 1) may be an offset in time between the different datastreams and
//   2) there may be missing visibilities around the start/end of the 'pass'
//   So the solution may be to do stuff on a Scan basis! j2ms2 (tm) now also
//   writes the 'scannumber' and hence we can gather our info on a baseline/scan/pass
//   basis and hence, we can select, per scan, the best dataset! 
//
//
// Author: Harro Verkouter, 25-06-2001
//
// $Id: jfilt.cc,v 1.8 2009/01/21 15:01:26 jive_cc Exp $
//
// $Log: jfilt.cc,v $
// Revision 1.8  2009/01/21 15:01:26  jive_cc
// HV: Did some layouting changes, should be minor if nothing at all.
//
// Revision 1.6  2007/05/25 11:57:04  jive_cc
// HV: Fix0red a signed/unsigned mismatch (with a cast .. so it is not *really* fixed)
//
// Revision 1.5  2006/01/13 13:01:04  verkout
// HV: CASAfied version of the implement
//
// Revision 1.4  2004/08/24 12:11:11  verkout
// HV: Hmmmm.. not the real PASSID was used for data-indexing. Fixed that
//
// Revision 1.3  2003/12/17 13:55:09  verkout
// HV: Rightie-ho! The brand new jfilt! Smaller, better and a LOT faster! (up to 100-1000 times...)
//
// Revision 1.2  2003/09/12 07:44:22  verkout
// HV: Code had to be made gcc3.* compliant.
//
// Revision 1.1  2002/08/06 09:28:05  verkout
// HV: The first iteration of jfilt, a filtering program which filters the best (based on integrated weight) baseline out of any number of duplicates.
//
//

// include aips++ stuff
#include <casa/aips.h>
#include <tables/Tables.h>
#include <tables/Tables/TableCopy.h>
#include <tables/Tables/TableRow.h>
#include <tables/Tables/TableIter.h>
#include <casa/Quanta/MVTime.h>
#include <ms/MeasurementSets.h>
#include <casa/Exceptions.h>
#include <casa/Utilities/Sort.h>
#include <casa/Arrays/ArrayMath.h>
#include <casa/Arrays/ArrayUtil.h>
#include <casa/Arrays/Vector.h>
#include <casa/Arrays/Vector.cc>
#include <casa/Arrays/Vector2.cc>
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Containers/Block.h>
#include <casa/Utilities/Copy.cc>
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/ArrayIO.cc>
#include <casa/Utilities/GenSort.h>
#include <casa/Utilities/Regex.h>

// C++ stuff
#include <iostream>
#include <iomanip>

// STL-stuff
#include <map>
#include <string>
#include <vector>

// POC-stuff (Plain-Old-C)
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <libgen.h>
#include <string.h>

//
// If BUILDTIME not set by makefile, think of one of our own...
//
#ifndef BUILDTIME
#define BUILDTIME "<no buildtime info present>"
#endif

using namespace std;
using namespace casa;


//
// Keep statistics for a specific pass. This enables us to
// select for a duplicate baseline the best pass
// 
// HV: 22-10-2003 - also record a vector<uInt>:
//                  the rownrs of the data from
//                  this specific pass.
//
typedef struct _passStats {
	Double       startTime;
	Double       endTime;
	Double       integratedWeight;
	vector<uInt> rowNrs;

	// Make sure stuff is properly initialized...
	_passStats() :
		startTime( -1.0 ),
		endTime( 0.0 ),
		integratedWeight( 0.0 )
	{}
} passStats;

// Record multiple pass-stats for a scan:
typedef struct _scanStats {
	// Map 'processor id' to a struct of type 'passStat' so we
	// can record the stats for all passes that contain data 
	// for this scan so as we can decide later on which one to keep
	map<int,passStats>   pStat;

	_scanStats() {}
} scanStats;

// For a duplicate baseline, keep a list of all 
// scans this baseline appears in.... (and thus a list of passes)
typedef struct _baselineStats {
	// Map scannumber to scanStat-struct so we can
	// gather information for that specific scan
	map<int,scanStats>   sStat;
} baselineStats;


// Check (non)existance of <infilename>, <outfilename>:
//
// <infilename> *must* exists
// <outfilename> *must NOT* exist
bool testInputFiles( const string& prefix,
					 const string& infilename,
					 const string& outfilename ) {
	int         statval;
	bool   	    retval = true;
	struct stat sb;


	if( !Table::isReadable(infilename.c_str()) ) {
		retval = false;
		cout << prefix << ": input MS  - " << infilename << " - does not exist?" << endl;
	}

	// Now attempt to stat the outfilename: it shouldn't be able to
	// stat it! (since the thing may not exist in what kind of form...
	// (e.g. symlink, block-special, regular file etc)
	//
	// Stay in while-loop until we get a returnvalue that was from
	// a non-interrupted systemcall...
	while( (statval=::stat(outfilename.c_str(), &sb))==-1 && errno==EINTR );
	
	// We'll consider this test an error condition if:
	//
	// 1) The named 'thingy', <outfilename> is stat-able (i.e. something
	//    exists by the name of <outfilename>) => the stat() systemcall
	//    return 0...
	//
	// 2) The systemcall fails. However, if it fails because of 'ENOENT'
	//    ("The named file does not exist or is the null pathname") this is
	//    not an error, this is basically what we want to test!
	//    All other failures indicate that either the path can't be reached
	//    etc.: most likely that we cant even create the new MS....
	//
	// We can summarize these conditions into:
	//  !(statval==-1 && errno==ENOENT)
	//
	// Since if the condition inside the ()'s is true, we don't want an
	// error; in all other cases we detect an error...
	if( !(statval==-1 && errno==ENOENT)) {
		retval = false;

		// Only print out presumed presence if we're quite sure something
		// was present....
		//
		if( statval==0 ) {
			cout << prefix << ": output MS - " << outfilename << " - already exists?"
				 << endl;
		} else {
			// something else wrong. print it out...
			cout << prefix << ": error trying to figure out if " << outfilename << "\n"
				 << prefix << ": exists - " << ::strerror(errno) << endl;
		}
	}
	return retval;
}

// Duplicate the table 'templatems': get the tabledesc from 
// templatems and create a new MS from that...
MeasurementSet* makeOutputMs( const string& msname,
                              const Table& templatems ) {
	Table               t;
	MeasurementSet*     retval;

	t = TableCopy::makeEmptyTable( String(msname.c_str()),
								   templatems.dataManagerInfo(),
		   						   templatems,
								   Table::NewNoReplace,
								   Table::LocalEndian,
								   true, true );

	TableCopy::copySubTables( t, templatems );
	retval = new MeasurementSet( t );

	retval->initRefs();
	
	return retval;	
}



void doDuplicates( vector<uInt>& rowstokeep,
                   const map<int,baselineStats>& duplicates,
				   const string& proggie ) {
	map<int,scanStats>::const_iterator     curscan;
	map<int,passStats>::const_iterator     curpass;
	map<int,passStats>::const_iterator     bestpass;
	map<int,baselineStats>::const_iterator curduplicate;


	for( curduplicate=duplicates.begin();
		 curduplicate!=duplicates.end();
		 curduplicate++ ) {
		int     ant1, ant2;
		MVTime  t1, t2;
		
		ant1 = curduplicate->first/1000;
		ant2 = curduplicate->first%1000;

		const map<int,scanStats>&   smapref( curduplicate->second.sStat );

		cout << proggie << ": BL=" << ant1 << "-" << ant2 << endl;

		for( curscan=smapref.begin(); curscan!=smapref.end(); curscan++ ) {
			double                    maxiwt( -9999999999.99 );
			const map<int,passStats>& pmapref( curscan->second.pStat );

			cout << proggie << ":    Found in Scan #" << curscan->first << endl;

			bestpass = pmapref.end();
			
			for( curpass=pmapref.begin(); curpass!=pmapref.end(); curpass++ ) {
				cout << proggie 
					 << ":       Data from pass #" << curpass->first
					 << " IWT=" << curpass->second.integratedWeight << "s " << endl
					 << "             ";

				t1 = MVTime( Quantity(curpass->second.startTime, "s") );
				t2 = MVTime( Quantity(curpass->second.endTime, "s") );

				cout << MVTime::Format(MVTime::YMD) << t1 << " - "
					 << MVTime::Format(MVTime::YMD) << t2 << endl
					 << "             duration=" 
					 << curpass->second.endTime-curpass->second.startTime << "s"
					 << endl;

				if( curpass->second.integratedWeight>maxiwt ) {
					bestpass = curpass;
					maxiwt   = curpass->second.integratedWeight;
				}
			}

			// If bestpass still pmapref.end(), we're in trouble!
			if( bestpass==pmapref.end() ) {
				cout << proggie << ":  YIKES! No 'bestpass' found?! This means\n"
					 << proggie << ":  that *no* IWT value > " << maxiwt << "?!\n";
				continue;
			}
			cout << proggie << ":    Selecting data from pass " << bestpass->first
				 << endl
				 << proggie << ":    Copying " << bestpass->second.rowNrs.size()
				 << " rows..." << endl;

			// Select data for the current best pass and move that to
			// the output MS... that is: copy the rownrs
			// from bestpass to rowstokeep...
			rowstokeep.insert( rowstokeep.end(), bestpass->second.rowNrs.begin(),
							   bestpass->second.rowNrs.end() );
		}
	}
	return;
}


int main( int, char** argv ) {
	string      proggie( ::basename(argv[0]) );
	string      infilename;
	string      outfilename;
    // leave the '=' in the Regex if you're modifying it; code
    // below trusts that *if* the regex matches an '=' is present
    // [it does not test the returnvalue of find('=') but blindly
    // uses it]
	const Regex infile( "^if=.+$" );
	const Regex outfile( "^of=.+$" );

	// Show usr which binary he/she's running
	cout << "\n"
		 << proggie << ": Version of " << BUILDTIME << " begins..."
		 << "\n\n" 
		 << flush;

	try {
		// Process the commandline args
		while( (*(++argv))!=0 ) {
			// process the argument...
			if( String(*argv).matches(infile) ) {
				if( infilename.size()>0 ) {
					cout << proggie << " - Duplicate if=... option. Ignoring this one:\n"
						 << proggie << "   " << *argv << endl;
                } else {
                    // looking for '=' is always succesfull
					string              arg( *argv );
					string::size_type   equal = arg.find( '=' );
	
					infilename = arg.substr( equal+1, arg.size() );

					if( infilename.size()==0 )
						cout << proggie << " - Missing value to if=... option!" << endl;
				}
			} else if( String(*argv).matches(outfile) ) {
				if( outfilename.size()>0 ) {
					cout << proggie << " - Duplicate of=... option. Ignoring this one:\n"
						 << proggie << "   " << *argv << endl;
				} else {
                    // looking for '=' is always succesfull
					string              arg( *argv );
					string::size_type   equal = arg.find( '=' );
	
					outfilename = arg.substr( equal+1, arg.size() );

					if( outfilename.size()==0 )
						cout << proggie << " - Missing value to of=... option!" << endl;
				}
			} else {
				cout << proggie << " - Unknown option " << *argv << "...." << endl;
			}
		}

		// Check usr sanity (i.e. if he/she mentioned in- and optional, outfilename)
		if( infilename.size()==0 ) {
			cout << endl
				 << "Usage: " << proggie << " if=<infile> [of=<outfile>]"
				 << endl << endl
				 << "Program to filter duplicate baselines out of multi-pass "
				 << "correlation MeasurementSet, keeping only the best datastream."
				 << endl
				 << "Options:" << endl
				 << "  if=<infilename>\n"
				 << "      (required) name of the input MS containing duplicates\n"
				 << "  of=<outfilename>\n"
				 << "      (optional) name of the MS where filtered data will be\n"
				 << "      stored. If none given, the system creates the name:\n"
				 << "        <infilename>.jfilt\n"
				 << "      Note: if the MS <outfilename> already exists, the \n"
				 << "      program will not run!\n";

			return 1;
		}

		// If outfilename not given, come up with one ourselves...
		if( outfilename.size()==0 )
			outfilename = infilename+".jfilt";

		// Test the (non)presence of the files...
		if( !::testInputFiles(proggie, infilename, outfilename) ) {
			cout << proggie << ": Unrecoverable problems detected.\n"
				 << "Giving up."
				 << endl;
			return 2;
		}


		// Now then... open the infile
		MeasurementSet     msin( infilename.c_str(), Table::Old );
		Table              tab2use = msin;

		// It's about time to sort the input table in the following order:
		//
		// ANTENNA1, ANTENNA2, SCAN_NUMBER, TIME, DATA_DESC_ID
		//
		// (we do not explicitly need to *sort* on DATA_DESC_ID but we need to
		// iterate over DATA_DESC_ID in order to filter out the duplicates. 
		// Therefore, we might as well sort on DDID in order to get any duplicates
		// together anyway...)
		Block<String>      sortnames( 5 );
		Block<String>      iternames( 4 );

		sortnames[0] = "ANTENNA1";
		sortnames[1] = "ANTENNA2";
		sortnames[2] = "SCAN_NUMBER";
		sortnames[3] = "DATA_DESC_ID";
		sortnames[4] = "TIME";
	
		iternames[0] = "ANTENNA1";
		iternames[1] = "ANTENNA2";
		iternames[2] = "SCAN_NUMBER";
		iternames[3] = "DATA_DESC_ID";

		// Main loop. Iterate over BL/SCAN/DATA_DESC_ID combi's.
		bool                     majorprob;
		String                   ant1name( MS::columnName(MS::ANTENNA1) );
		String                   ant2name( MS::columnName(MS::ANTENNA2) );
		String                   scanname( MS::columnName(MS::SCAN_NUMBER) );
		String                   ddidname( MS::columnName(MS::DATA_DESC_ID) );
		String                   procname( MS::columnName(MS::PROCESSOR_ID) );
		String                   timename( MS::columnName(MS::TIME) );
		String                   exponame( MS::columnName(MS::EXPOSURE) );
		String                   wtname( MS::columnName(MS::WEIGHT) );
        String                   passidname( MSProcessor::columnName(MSProcessor::PASS_ID) );
		IPosition                fltshp;
		IPosition                wtshape;
		IPosition                index;
        TableRecord              tabrec;
		Array<Float>             fltar;

		const IPosition          nosize( IPosition(0,0) );

		int                      maxcol( 8 );
		int                      curcol( 0 );
		Int                      shape0;
		Int                      shapetmp;
		Vector<Int>              ant1v;
		Vector<Int>              ant2v;
		Vector<Int>              scannov;
		Vector<Int>              ddidv;
		Vector<Int>              procidv;
                Vector<Int>              passidv;
		Vector<uInt>             orgrownr;
		vector<uInt>             rowstokeep; // std::vector rather than Vector!
		Vector<Double>           timev;
		Vector<Double>           exposurev;
		Array<Float>			 wtcolv;
        ROScalarColumn<Int>      ant1;
        ROScalarColumn<Int>      ant2;
        ROScalarColumn<Int>      ddid;
        ROScalarColumn<Int>      scanno;
        ROScalarColumn<Int>      procid;
        ROScalarColumn<Int>      passid;
        ROArrayColumn<Float>     wtcol;
        ROScalarColumn<Double>   timecol;
        ROScalarColumn<Double>   exposure;
		
		ant1.attach( tab2use, ant1name );
		ant2.attach( tab2use, ant2name );
		scanno.attach( tab2use, scanname );
		ddid.attach( tab2use, ddidname );
		procid.attach( tab2use, procname );
		timecol.attach( tab2use, timename );
		exposure.attach( tab2use, exponame );
		wtcol.attach( tab2use, wtname );

		shape0 = tab2use.nrow();
		orgrownr.resize( shape0 );
		indgen( orgrownr );

		cout << "Start reading columns....\n";

		cout << ++curcol << "/" << maxcol << ": " << ant1name << "..." << flush;
		ant1v = ant1.getColumn();
		cout << "ok\n";
		ant1v.shape( shapetmp );
		if( shapetmp!=shape0 )
			throw( AipsError("ANT1 shape != rownrs.shape()") );

		cout << ++curcol << "/" << maxcol << ": " << ant2name << "..." << flush;
		ant2v = ant2.getColumn();
		cout << "ok\n";
		ant2v.shape( shapetmp );
		if( shapetmp!=shape0 )
			throw( AipsError("ANT2 shape != rownrs.shape()") );

		cout << ++curcol << "/" << maxcol << ": " << scanname << "..." << flush;
		scannov = scanno.getColumn();
		cout << "ok\n";
		scannov.shape( shapetmp );
		if( shapetmp!=shape0 )
			throw( AipsError("SCANNO shape != rownrs.shape()") );

		cout << ++curcol << "/" << maxcol << ": " << ddidname << "..." << flush;
		ddidv = ddid.getColumn();
		cout << "ok\n";
		ddidv.shape( shapetmp );
		if( shapetmp!=shape0 )
			throw( AipsError("DATA_DESC_ID shape != rownrs.shape()") );

		cout << ++curcol << "/" << maxcol << ": " << procname << "..." << flush;
		procidv = procid.getColumn();
		cout << "ok\n";
		procidv.shape( shapetmp );
		if( shapetmp!=shape0 )
			throw( AipsError("PROCID shape != rownrs.shape()") );
		
		cout << ++curcol << "/" << maxcol << ": " << timename << "..." << flush;
		timev = timecol.getColumn();
		cout << "ok\n";
		timev.shape( shapetmp );
		if( shapetmp!=shape0 )
			throw( AipsError("TIME shape != rownrs.shape()") );
		
		cout << ++curcol << "/" << maxcol << ": " << exponame << "..." << flush;
		exposurev = exposure.getColumn();
		cout << "ok\n";
		exposurev.shape( shapetmp );
		if( shapetmp!=shape0 )
			throw( AipsError("EXPOSURE shape != rownrs.shape()") );
		
		cout << ++curcol << "/" << maxcol << ": " << wtname << "..." << flush;
		wtcolv = wtcol.getColumn();
		cout << "ok\n";
		wtshape = wtcolv.shape();
		if( wtshape[wtshape.nelements()-1]!=shape0 )
			throw( AipsError("WEIGHT shape != rownrs.shape()") );
	
        // Data-shape fine apparently
        //
        // Now read the passId's from the PROCESSOR subtable
        tabrec = tab2use.keywordSet();
        const MSProcessor&  proct( tabrec.asTable("PROCESSOR") );
        passid.attach( proct, passidname );
        passidv = passid.getColumn();

		// Now start sorting
		uInt         nritems;
		Sort         sorter;
		Vector<uInt> idx;

		sorter.sortKey(ant1v.data(), TpInt);
		sorter.sortKey(ant2v.data(), TpInt);
		sorter.sortKey(scannov.data(), TpInt);
		sorter.sortKey(ddidv.data(), TpInt);
		sorter.sortKey(timev.data(), TpDouble);

		cout << "Start sorting...." << flush;
		nritems = sorter.sort(idx, shape0);
		cout << "ok\n" << endl;

		// Loop vars
		uInt     currow;
		
		// This is where we record our duplicates!
		// We map the 'baselinenumber' to a struct 'baselineStats' for
		// easy lookup.... 'baselinenumber' == 1000*ANT1+ANT2
		map<int,baselineStats>           duplicates;
		map<int,scanStats>::iterator     curscan;
		map<int,passStats>::iterator     curpass;
		map<int,baselineStats>::iterator curduplicate;
	
		majorprob = false;
		
		currow = 0;
		while( !majorprob && currow<nritems ) {
			uInt                 startrow,stoprow;
			Int                  a1,a2,sn,dd;

			if( (currow%1000)<5 ) {
				cout << "Done: " << ((double)currow/(double)shape0)*100.0
					 << "%           \r" << flush;
			}
			// find all the rows for which ant1, ant2, scan_no and datadescription
			// are equal. only variance then is in pass/time...
			startrow = currow;

			a1 = ant1v[idx[startrow]];
			a2 = ant2v[idx[startrow]];
			sn = scannov[idx[startrow]];
			dd = ddidv[idx[startrow]];

			stoprow = startrow;
			while( stoprow<(uInt)shape0 &&
					ant1v[idx[stoprow]]==a1 && ant2v[idx[stoprow]]==a2 &&
					scannov[idx[stoprow]]==sn && ddidv[idx[stoprow]]==dd ) {
				stoprow++;
			}


			uInt      curint;

			curint = startrow;
			while( curint<stoprow ) {
				uInt      sametime;	
				Double    timelimit = timev[idx[curint]] + (exposurev[idx[curint]]/2.0);

				// find all rows with a time <= (time[startrow]+exposure[startrow]/2)
				//
				// we do that by only incrementating stoprow if row(stoprow)
				// satisfies the condition mentioned just before.
				// After the algorithm terminates, we may rest assured that
				// the last row in the subtable for which the condition holds
				// is row# 'stoprow'
				sametime = curint;
				while( (sametime+1)<stoprow &&
						timev[idx[sametime+1]]<timelimit ) {
					sametime++;
				}

				// By the time we end up here,
				// 'all' rows from curint -> sametime 
				// are within the time-limit ie they describe
				// the same visibility..
				//
				// Iff sametime==curint, this means that there
				// are no multiple integrations
				// for this visibility.
				if( curint==sametime ) {
					// no duplicates. write  current row to output ms
					rowstokeep.push_back( orgrownr[idx[curint]] );
					curint++;
					continue;
				}

				// Dang. Duplicate. Now record stuff for later use
				int     blcode = (1000*(int)(a1))+(int)(a2);
				int     scan   = (int)sn;
				
				if( (curduplicate=duplicates.find(blcode))==duplicates.end() ) {
					// no entry yet for 'blcode' -> define it!
					baselineStats     newblstat;

					duplicates[blcode] = newblstat;
					curduplicate = duplicates.find( blcode );
				}

				for( uInt r=curint; r<=sametime; r++ ) {
					uInt                ridx = idx[r];
					map<int,scanStats>& smapref( curduplicate->second.sStat );

					// locate current scan
					if( (curscan=smapref.find(scan))==smapref.end() ) {
						// scan not yet defined... make it so!
						scanStats  newscanstats;

						smapref[scan] = newscanstats;
						curscan       = smapref.find( scan );
					}

					// Locate 'processor_id' (==pass (within jive context))
					// within scan
					//
                    // 09/02/2004 - *rats* RMC bugged me 'bout probs
                    //              in jfilt... He plotted some
                    //              post-jfilted stuff agains PROCESSOR_ID
                    //              and I told'm that was wrong since
                    //              PROCESSOR_ID is not unique across
                    //              MSs.. sooh when I actually went in
                    //              to check what was going on, I found
                    //              out I was using the 'raw' PROCESSOR_ID
                    //              values.. b****rd! Need to unmap the
                    //              PROCESSOR_ID to something unique...
                    //              Need to get the passId from the PROCESSOR
                    //              subtable
                    //
					int                 proc/* = (int)procidv[ridx]*/;
					map<int,passStats>& pmapref( curscan->second.pStat );
                                        
                    proc = passidv[ (int)procidv[ridx] ];
					if( (curpass=pmapref.find(proc))==pmapref.end() ) {
						// not yet defined. Define it...
						passStats     newpassstats;
	
						pmapref[proc] = newpassstats;
						curpass       = pmapref.find( proc );
					}

					// update the record...
					passStats& ref( curpass->second );

					if( ref.startTime<0.0 )
						ref.startTime = timev[ridx];

					ref.endTime = timev[ridx];

					IPosition   start(2, 0, ridx), end(2, wtshape[0]-1, ridx),
								stride( 2, 1, 1 );
					Slicer      slic(start, end, stride, Slicer::endIsLast);

					fltar.resize( nosize );
					fltar  = wtcolv( slic.start(), slic.end(), slic.stride() );
					fltshp = fltar.shape();

					// we *must* test for nonzero array-length, otherwise
					// the 'mean' function will throw an exception....
					// Why it would do that? Go ask the AIPS++ design team...
					if( fltshp.product() )
						ref.integratedWeight += (sum(fltar)*exposurev[ridx]);

					// save the rownr..
					ref.rowNrs.push_back( orgrownr[ridx] );
				}
		
				// done processing this integration. Move on to the next...
				curint = sametime+1;
			}
			currow = stoprow;
		}
		// Now process the duplicates
		if( duplicates.size() ) {
			cout << proggie << ": - Found " << duplicates.size() << " duplicate baselines."
				<< endl;

			::doDuplicates( rowstokeep, duplicates, proggie );
		}
		Vector<uInt>    rws( rowstokeep );

		cout << proggie << ": Sorting rownrs to original order..." << flush;
		GenSort<uInt>::sort( rws );
		cout << "ok" << endl;

		cout << proggie << ": Start creating MS with " << rowstokeep.size() << " rows..."
			<< flush;

		MeasurementSet  msout( msin(rws) );		

		msout.rename( outfilename, Table::NewNoReplace );
		msout.flush();
		cout << "ok" << endl;
	}
	catch( AipsError& y ) {
		cout << proggie << ": - Darn! Caught exception!\n"
			<< proggie << ":   error=" << y.getMesg() << endl;
	}

	return 0;
}
