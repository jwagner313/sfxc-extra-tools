#
# Make sure we include all the standard locations...
#
#include $(ROOT)/system/locations.mak

#
# Ok, make these so they can be used in the commands
#
#export INCDIRS    = $(addprefix -I, $(foreach XLIB, $(XLIBLIST), $($(XLIB)INCD)))
#export LIBDIRS    = $(addprefix -L, $(foreach XLIB, $(XLIBLIST), $($(XLIB)LIBD)))
#export THELIBS    = $(addprefix -l, $(foreach XLIB, $(XLIBLIST), $($(XLIB)LIBS)))
#export THEDEPLIBS = $(addprefix -l, $(foreach DEP, $(DEPLIBS), $(DEP)))

#
# How to exactly tell the linker to store the current lib-search path in
# the shared object (apparently...) depends on which system we are.
#
# On Linux we need:     -rpath <path>          (this is ld.so)
# On Solaris we find:   -R <path>              (this is plain ld?)
# On HP-UX we find:     +b :              
#
ifeq ($(OSNAME),SunOS)
	export RUNTDIRS = $(addprefix -Xlinker -R -Xlinker ,\
				$(foreach XLIB, $(XLIBLIST), $($(XLIB)LIBD)))
endif

ifeq ($(OSNAME),HP-UX)
	export RUNTDIRS = -Xlinker +b -Xlinker :
endif

ifeq ($(OSNAME),Linux)
	export RUNTDIRS = $(addprefix -Xlinker -rpath -Xlinker ,\
			$(foreach XLIB, $(XLIBLIST), $($(XLIB)LIBD)))
endif

#
# Now follow the rules how to generate stuff!
#

# create object file from plain old C-source file...
$(REPOSDIR)/%.o: $(REPOSDIR)/%.d
	@if [ ! -d $(dir $@) ]; then\
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(CC) $(CFLAGS) $(THEDEFS) $(INCDIRS) -c -o $@ $*.c ;
	@echo

# create object file from (assumed) C++ source file
#$(REPOSDIR)/%.o: %.cc
$(REPOSDIR)/%.o: $(REPOSDIR)/%.dd
	@if [ ! -d $(dir $@) ]; then \
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(CXX) $(CXXFLAGS) $(THEDEFS) $(INCDIRS) -c -o $@ $*.cc ;
	@echo

# create object file from (assumed) C++ source file
$(REPOSDIR)/%.o: $(REPOSDIR)/%.D
	@if [ ! -d $(dir $@) ]; then\
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(CXX) $(CXXFLAGS) $(THEDEFS) $(INCDIRS) -c -o $@ $*.C ;
	@echo

#
# Dependency analysis...
#

# insert command how to generate the dependency file 
# for a plain old C source file
$(REPOSDIR)/%.d: %.c
	@if [ ! -d $(dir $@) ]; then\
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	@echo "Remaking C-dependency file for $<" 
	@$(MKcDEP) $(CFLAGS) $(THEDEFS) $(INCDIRS) $< | sed s=$(subst $(dir $*),,$*)\\.o=$@= > $@ ;
	@echo

$(REPOSDIR)/%.dd: %.cc
	@if [ ! -d $(dir $@) ]; then\
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	@echo "Remaking C++-dependency file for $<"
	@$(MKccDEP) $(CXXFLAGS) $(THEDEFS) $(INCDIRS) $< | sed s=$(subst $(dir $*),,$*)\\.o=$@= > $@ ;
	@echo

$(REPOSDIR)/%.D: %.c
	@if [ ! -d $(dir $@) ]; then\
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	@echo "Remaking C++-dependency file for $<"
	@$(MKCDEP) $(CXXFLAGS) $(THEDEFS) $(INCDIRS) $< | sed s=$(subst $(dir $*),,$*)\\.o=$@= > $@ ;
	@echo

#
# How to build a module!
#
define make_shared_module
	@if [ ! -d $(dir $@) ]; then \
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(LDMOD) -o $@ $(LIBDIRS) $(RUNTDIRS) $(filter-out %.a,$^) $(THEDEPLIBS) $(THELIBS);
	@echo;echo "Created module $@ (containing $(filter-out %.a,$(^F)))"; echo
endef

#
# How to make a lib...
#
define make_shared_lib
	@if [ ! -d $(dir $@) ]; then \
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(LDSHLIB) -o $@ $(LIBDIRS) $(RUNTDIRS) $(filter-out %.a,$^) $(THEDEPLIBS) $(THELIBS);
	@echo;echo "Created shared lib $@ (from $(filter-out %.a,$(^F)))"; echo
endef

#
# How to create a static lib
#
define make_static_lib
	@if [ ! -d $(dir $@) ]; then \
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";echo;\
	fi;
	$(LDSTATIC) $@ $(filter-out %.a,$^) 
	@echo;echo "Created static lib $@ (from $(filter-out %.a, $(^F)))"; echo
endef

#
# How to bake a binary
#
#	$(LDBIN) -o $@ $(LIBDIRS) $(RUNTDIRS) $(filter-out %.a,$^) $(THEDEPLIBS) $(THELIBS);
define make_binary
	@if [ ! -d $(dir $@) ]; then \
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(LDEXE) $(SHAREDIMGFLAGS) -o $@ $(LIBDIRS) $(RUNTDIRS) $(filter-out %.a,$^) $(THEDEPLIBS) $(THELIBS);
	@echo;echo "Created program $@"; echo
endef

#
# How to bake a static binary
#
#	$(LDBIN) -o $@ $(LIBDIRS) $(RUNTDIRS) $(filter-out %.a,$^) $(THEDEPLIBS) $(THELIBS);
define make_static_binary
	@if [ ! -d $(dir $@) ]; then \
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(LDEXE) $(STATICIMGFLAGS) -o $@ $(LIBDIRS) $(RUNTDIRS) $(filter-out %.a,$^) $(THEDEPLIBS) $(THELIBS);
	@echo;echo "Created program $@"; echo
endef

#
# How to create a linux kernel module
#
define make_kernel_module
	@if [ ! -d $(dir $@) ]; then \
		mkdir -p $(dir $@);\
		echo "Created dir $(dir $@)";\
	fi;
	$(LD) $(KERNELMODULEFLAGS) -o $@ $(LIBDIRS) $(filter-out %.a,$^) \
	$(THEDEPLIBS) $(THELIBS);
	@echo;\
	echo "Created kernelmodule: $@";\
	echo "                from: $(filter-out %.a, $(^F))";
endef


#.PHONY: help
#help:
#	@echo "This provides some help..."
