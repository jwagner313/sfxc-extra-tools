#
# $Id: lib.mak,v 1.8 2011/10/12 12:45:37 jive_cc Exp $
#
# $Log: lib.mak,v $
# Revision 1.8  2011/10/12 12:45:37  jive_cc
# HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
#       changed into 'labels' for more genericity
#     * can now be built either as part of AIPS++/CASA [using makefile]
#       or can be built as standalone set of libs + binaries and linking
#       against casacore [using Makefile]
#       check 'code/
#
# Revision 1.7  2006-04-19 09:50:35  verkout
# HV: support for simultaneous 32/64 builds on bi-arch systems without messing up
#
# Revision 1.6  2005/09/16 14:57:47  verkout
# HV: Removed erroneous TAB
#
# Revision 1.5  2004/06/03 14:26:58  verkout
# HV: Some more output added to the 'info' target
#
# Revision 1.4  2003/01/17 10:38:34  verkout
# HV: Minor mods to make compilation just fine, includes detecting
#     erroneous LINKAGE specifier.
#
# Revision 1.3  2003/01/15 16:08:28  verkout
# HV: Added support for creating both shared/static libs
#
#
# This will  be a makefile wich will create an library  in
# the directory $(ROOT)/lib by the name of 'lib(basename $PWD).so'
#
include $(ROOT)/system/compiler.mak
include $(ROOT)/system/locations.mak

#
#  Try to work out where we are (and form the targetname out of that)
#  Note: REPOSDIR should be defined since rules in 'rules.mak'
#        might depend on that var...
#
export CURDIR     = $(shell pwd)
export REPOSDIR   = $(CURDIR)/Repos/$(MACHINE)-$(B2B)

SHLIBEXT = so
ifeq ($(OSNAME),HP-UX)
	SHLIBEXT = sl
endif

export BASETARGET  = lib$(shell basename $(CURDIR))

# default is shared objects
export LINKAGE     ?= shared
export MYTARGET     = invalid.invalid
export STATICTARGET = 
export SHAREDTARGET =

ifeq ($(LINKAGE),static)
	export STATICTARGET := $(BASETARGET).a
endif
ifeq ($(LINKAGE),shared)
	export SHAREDTARGET := $(BASETARGET).$(SHLIBEXT)
endif
ifeq ($(LINKAGE),both)
	export STATICTARGET := $(BASETARGET).a
	export SHAREDTARGET := $(BASETARGET).$(SHLIBEXT)
endif

export MYTARGET := $(STATICTARGET) $(SHAREDTARGET)
ifeq ($(strip $(MYTARGET)),)
	export MYTARGET = invalid
endif
export TPREFIX  := $(ROOT)/lib/$(MACHINE)-$(B2B)/


# MYTARGET = logical name(s) of the target, THETARGET
# defines the full target name; may include OS/compiler/Architecture dependant
# parts
export THETARGET  = $(addprefix $(TPREFIX), $(foreach TARG, $(MYTARGET), $(TARG)))

#
# Make a shortcut for all objs...
#
export THEOBJS = $(addprefix $(REPOSDIR)/, $(foreach OBJ, $(ALLOBJS), $(OBJ)))

#
# The dependency files...
#
export THEDEPS = $(addprefix $(REPOSDIR)/, $(foreach DEP, $(ALLDEPFILES), $(DEP)))

REALDEPLIBS := $(foreach DEP, $(DEPLIBS), $(shell if test -f $(OWNLIBD)/lib$(DEP).a;\
			then echo $(OWNLIBD)/lib$(DEP).a;fi))

.PHONY: all
all: $(THETARGET) 
	@echo "-->  $(MYTARGET) is up to date";

.PHONY: clean
clean: 
	-@rm -f $(THEOBJS) $(THETARGET);\
	echo "Cleaned out: $(THEOBJS) $(THETARGET)";
	@echo

.PHONY: clobber
clobber:
	-@rm -f $(THEOBJS) $(THEDEPS) $(THETARGET);\
	rmdir $(REPOSDIR);\
	echo "...ok.. Library clobbered. Happy now?!";\
	echo

.PHONY: info
info:
	@if [ -z "$(ROOT)" ]; then \
		echo "ROOT environmentvariable NOT SET!!!";\
	else\
		echo "ROOT=$(ROOT)";\
	fi;\
	echo "LINKAGE=$(LINKAGE), MYTARGET=$(MYTARGET)";\
	echo "THETARGET=$(THETARGET)";\
	echo "XLIBLIST=$(XLIBLIST)";\
	echo "LIBDIRS: $(foreach XLIB,$(XLIBLIST),$($(XLIB)LIBD))";



ifneq ($(strip $(STATICTARGET)),)
$(TPREFIX)$(STATICTARGET) : $(THEOBJS)
	$(make_static_lib)
endif

ifneq ($(strip $(SHAREDTARGET)),)
$(TPREFIX)$(SHAREDTARGET): $(THEOBJS) $(REALDEPLIBS)
	$(make_shared_lib)
endif

%invalid:
	@echo "Invalid LINKAGE specifier ($(LINKAGE)), not one of";\
	echo "shared, static, both";\
	exit;


#
# Now that all's defined, include the rulez!
#
include $(ROOT)/system/rules.mak
include $(THEDEPS)
