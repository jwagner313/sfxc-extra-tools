#
# Define 3rd party support package support a la aips++
#
# For each package define the following variables (<PKG> is the identifier of 
# the package by which would like it to be referenced, examples follow)
#
#  <PKG>ROOT  = <rootdirectory of the package (optional)> 
#  <PKG>INCD  = <directory/ies containing the include files>
#  <PKG>LIBD  = <directory/ies containing the libraries>
#  <PKG>LIBS  = <list of libraries from this packages that need to 
#                be linked with>
#  <PKG>DEFS  = <list of defines, to be added to the compilation command,
#                the -D will be added by the system automagically>
#
# Suppose we want PGPlot to be made available to applications. Pick a name for
# it; PGPLOT might not be a bad choice :). Now define:
#
#  PGPLOTROOT  = /local/
#  PGPLOTINCD  = $(PGPLOTROOT)/include
#  PGPLOTLIBD  = $(PGPLOTROOT)/lib
#  PGPLOTLIBS  = cpgplot pgplot
#
# If programs/modules in the system wish to make use of PGPLOT, then they
# only need to define a string like this in their own makefile:
#  XLIBLIST  += PGPLOT
# and all stuff should work out ok!
#

#
# Define the project wide stuff. 
# We automatically add the $(ROOT)/src/libs directory to the include path 
# of all compile commands and the $(ROOT)/lib directory to the library
# search path for all link commands....
#
# (i.e. for all of module, lib and binary building).
# This means that you can easily include library header files as:
#     <mylib/file.h>
# or  <communication/unixSocket.h> 
#   (assuming that you have $(ROOT)/src/libs/mylib and 
#     $(ROOT)/src/libs/communication
#
#  In the per application/library/module specific makefile you
#  can define the libs (from this project)
#        that you really would like to link against as
#   for third party libs, the method is different, see above
#
#  DEPLIBS     += communication mylib
#  
#
# $Id: locations.mak,v 1.24 2011/10/12 12:45:37 jive_cc Exp $
#
# $Log: locations.mak,v $
# Revision 1.24  2011/10/12 12:45:37  jive_cc
# HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
#       changed into 'labels' for more genericity
#     * can now be built either as part of AIPS++/CASA [using makefile]
#       or can be built as standalone set of libs + binaries and linking
#       against casacore [using Makefile]
#       check 'code/
#
# Revision 1.23  2006-05-22 12:28:00  verkout
# HV: Prevent incorrect/missing ABROOT variable messing things up in the makesystem
#
# Revision 1.22  2006/04/20 06:05:43  verkout
# HV: added libz (for compress/uncompress) to MYSQL libraries so it will always be resolved ok
#
# Revision 1.21  2006/04/19 14:34:44  verkout
# HV: thirdparty libs now also honour the 32/64 bit in their bin/lib path. Paths in this file must be changed accordingly
#
# Revision 1.20  2006/04/19 09:50:35  verkout
# HV: support for simultaneous 32/64 builds on bi-arch systems without messing up
#
# Revision 1.19  2006/02/14 07:42:54  verkout
# HV: Changed the definition of the AB (albert bos) package; it is now a true library
#
# Revision 1.18  2006/01/18 13:47:11  verkout
# HV: Consolidated Albert's stuff across JCCS, PCInt and CASA. Do not take ab's stuff directly from /users/evn but from 'standard' location
#
# Revision 1.17  2005/03/22 12:43:08  verkout
# ${logmsg}
#
# Revision 1.16  2004/11/25 15:47:44  verkout
# HV: Added package definitions for
#     - MYSQL (...)
#     - AB ; Albert Bos; Albert's stuff that's
#       found in the JIVE Correlator Control Software,
#       JCCS for short
#
# Revision 1.15  2004/11/02 21:00:08  verkout
# HV: The thirdparty directory now is officially a package.
#     Has $(MACHINE) in its path too
#
# Revision 1.14  2004/06/03 14:51:48  verkout
# HV: XERCESC is now in thirdparty DIR
#
# Revision 1.13  2003/08/29 12:20:44  verkout
# HV: Added package GNU Readline, which by default is in ROOT/thirdparty
#
# Revision 1.12  2003/03/27 12:34:38  verkout
# HV: Made xercesc (the XML parser) part of PCInt
#
# Revision 1.11  2003/02/27 09:06:13  verkout
# HV: * Added support for building kernelmodules
# 	  (=linuxdrivers). Place code under $ROOT/src/modules
# 	  and in the makefile, say: LINKAGE=kernel. Default
# 	  is LINKAGE=shared, which produces a module which
# 	  is runtime-loadable into apps.
# 	* Added package MODULE, libs/apps that wish to be able
# 	  to deal with modules (load them, include header files)
# 	  can add MODULE to their XLIBLIST. The module-source-dir
# 	  gets added to the include path and a commandline define
# 	  MODULEDIR gets added. MODULEDIR is the directory where
# 	  compiled modules for your current system will end up.
# 	  Removed the MODULEDIR-define from package OWN, where it
# 	  was before.
# 	* In the module-makefile, package MODULE automatically
# 	  gets added to the XLIBLIST
# 	* Added a rule to build a kernel module. Note: at the
# 	  moment I think it's reasonably linux-specific
#
# Revision 1.10  2003/02/12 08:08:11  verkout
# HV: Silly. Got the definition of MODULEDIR wrong..
#
# Revision 1.9  2003/02/12 07:46:32  verkout
# HV: Added <PKG>DEFS, can now add package specific defines. Standard define added: MODULEDIR.
#
#

# Define the (relative) locations of the project's own
# source/include/lib files, standard defines and what not!
export OWNROOT   = $(ROOT)
export OWNINCD   = $(OWNROOT) 
export OWNLIBD   = $(ROOT)/lib/$(MACHINE)-$(B2B)
export OWNLIBS   = 
export OWNDEFS = 

# when building "loadable modules", these are used
export MODULEROOT = $(ROOT)/src
export MODULEINCD = $(MODULEROOT)/modules
export MODULEDEFS = MODULEDIR="\"$(ROOT)/modules/$(MACHINE)-$(B2B)\""

# Add ownlibd to resolv dependencies on libs from the project itself
vpath %.a $(OWNLIBD)

# DL = Dynamic Loading/Linking
export DLROOT    =
export DLINCD    =
export DLLIBD    =

ifeq ($(OSNAME),HP-UX)
	export DLLIBS    = dld
else
	export DLLIBS    = dl
endif

# THRD = multithreaded applications/libs
#export THRDROOT  = /home/marcel/linux
#export THRDROOT  = /usr/local
#export THRDINCD  = $(THRDROOT)/include
#export THRDLIBD  = $(THRDROOT)/lib
export THRDLIBS  = pthread

# X11
ifeq ($(OSNAME),SunOS)
	export X11ROOT   = /usr/openwin
endif

ifeq ($(OSNAME),Linux)
	export X11ROOT   = /usr/X11R6
endif

export X11LIBD   = $(X11ROOT)/lib
export X11INCD   = $(X11ROOT)/include
export X11LIBS   = X11

# SOCKET = system dependent libs for socket-stuff
ifeq ($(OSNAME),SunOS)
	export SOCKETLIBS = socket nsl
endif


# RT = RealTime stuff, eg. for clock_gettime()...
export RTLIBS    = rt

# C-library: we only need this if we want to do profiling of the C library
export CLIBS     = c


# GNU-Readline stuff
#
# HV: 20/07/2004 - thirdparty stuff now also
#                  has a $(MACHINE) in their
#                  path...
RLROOT             = $(JWROOT)
RLINCD             = $(RLROOT)/include
RLLIBD             = $(RLROOT)/lib/$(MACHINE)-$(B2B)
RLLIBS             = history readline ncurses

# MySQL stuff
#
# HV: 20/07/2004 - thirdparty stuff now also
#                  has a $(MACHINE) in their
#                  path...
MYSQLROOT          = $(ROOT)/thirdparty
MYSQLINCD          = $(MYSQLROOT)/include
MYSQLLIBD          = $(MYSQLROOT)/lib/$(MACHINE)-$(B2B)/mysql
MYSQLLIBS          = mysqlclient z

# JCCS becomes also a kind of
# thirdparty thing I guess.. err
# basically only the albert thingy
# We should take care not to make
# PCInt dependant on JCCS AND
# making JCCS dependant on PCInt
# *at the same time* -> that would be 
# a recipe for disaster. For now,
# just take the Albert module from JCCS
# (as far as JCCS is concerned, the Albert
# module is regarded as a 'thirdparty' package,
# so that's good).
#
# Note: you can check out the latest Albert
# module via CVS as in:
#     cvs -d :pserver:<user>@juw30.nfra.nl:/export/home/jive_cvs/JCCS/CVS_Repository co albert
#
# Point ABROOT at the 'albert' directory after cvs extraction.
# For now we only need the include files 
#
# HV: 13/2/2006 - Albert's stuff is now a "package",
# 				  we stuff the object file in a real
# 				  library such that we can link with
# 				  it
ABROOT  = /pcint2/src/albert
ABSYS  := $(shell sh -c "if [ -f $(ABROOT)/system_type.sh ]; then . $(ABROOT)/system_type.sh; fi")
ABINCD  = $(ABROOT)
ABLIBD  = $(ABROOT)/$(ABSYS)
ABLIBS  = albert

JCCSROOT = /pcint2/src/jccs
JCCSINCD = $(JCCSROOT)/include
JCCSSYS := $(shell sh -c "if [ -f $(JCCSROOT)/system_make/system_type.sh ]; then . $(JCCSROOT)/system_make/system_type.sh; fi")
JCCSLIBD = $(JCCSROOT)/lib/$(JCCSSYS)
JCCSLIBS = COF message_base vexplus herbert job_descr jive_msgs jccs_util stats

# JCCS stuff needs VEX
VEXROOT    := $(HOME)/code/sfxc-extra-tools/myvex
VEXINCD    := $(VEXROOT)
VEXLIBD    := $(VEXROOT)
VEXLIBS    := vex

# aips++/casa is now a thirdparty
# CASAROOT = $(HOME)/code/casacore-1.5.0  # location of source code
CASAROOT = $(HOME)/install/               # location of compiled casa after 'make install'
CASAINCD = $(CASAROOT)/include/casacore
CASALIBD = $(CASAROOT)/lib
CASALIBS = casa_measures casa_msfits casa_tables casa_coordinates casa_fits casa_images casa_lattices casa_mirlib casa_ms casa_scimath casa_scimath_f casa_components casa_casa fftw3 fftw3f 
#CASALIBS = casa_measures casa_measures_f casa_msfits casa_tables casa_coordinates casa_fits casa_images casa_lattices casa_mirlib casa_msvis casa_ms casa_scimath casa_scimath_f casa_components casa_casa

CFITSIOROOT = $(HOME)/install/
CFITSIOINCD = $(CFITSIOROOT)/include
CFITSIOLIBD = $(CFITSIOROOT)/lib
CFITSIOLIBS = cfitsio

# FLEX
FLEXLIBS    = fl

# On newer linuxen we need "-lbsd" for strlcpy()
BSDLIBS     = bsd
BSDDEFS     = LIBBSD

# Add to XLIBLIST the packages you'd like to link/compile with...
export XLIBLIST  += DL OWN GCC BSD

# Ok, make these so they can be used in the commands
#
# the RLIBs are "raw" libs, those will not be prefixed with -l,
# they will be passed verbatim to the linker
export INCDIRS    = $(addprefix -I, $(foreach XLIB, $(XLIBLIST), $($(XLIB)INCD)))
export LIBDIRS    = $(addprefix -L, $(foreach XLIB, $(XLIBLIST), $($(XLIB)LIBD)))
export THELIBS    = $(addprefix -l, $(foreach XLIB, $(XLIBLIST), $($(XLIB)LIBS))) $(foreach XLIB, $(XLIBLIST), $($(XLIB)RLIB))
export THEDEPLIBS = $(addprefix -l, $(foreach DEP, $(DEPLIBS), $(DEP)))
export THEDEFS    = $(addprefix -D, $(foreach XLIB, $(XLIBLIST), $($(XLIB)DEFS)))


#
# If PROF is set to "all", the user also wants profiling info of the system
# libraries. These libraries should have "_p" appended to their names.
# (e.g. libc_p.a is the profiling libc library).
#
ifeq ("all","$(PROF)")
	export XLIBLIST += C
	export THELIBS  = $(addprefix -l, $(addsuffix _p, \
                            $(foreach XLIB, $(XLIBLIST), $($(XLIB)LIBS))))
endif
