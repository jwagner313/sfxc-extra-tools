#
# This will  be a makefile wich will create an executable image in
# the directory $(ROOT)/bin by the name of '(basename $PWD)'
#
# $Id: app.mak,v 1.7 2011/10/12 12:45:36 jive_cc Exp $
#
# $Log: app.mak,v $
# Revision 1.7  2011/10/12 12:45:36  jive_cc
# HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
#       changed into 'labels' for more genericity
#     * can now be built either as part of AIPS++/CASA [using makefile]
#       or can be built as standalone set of libs + binaries and linking
#       against casacore [using Makefile]
#       check 'code/
#
# Revision 1.6  2006-04-19 09:50:35  verkout
# HV: support for simultaneous 32/64 builds on bi-arch systems without messing up
#
# Revision 1.5  2003/01/24 12:20:04  verkout
# HV: Hmmmm... LDFLAGS was explicitly set to something that wouldn't work on Solaris. Changed it to using the XLIBLIST variable, which takes care of saving the path to the shared lib in the executable image.
#
# Revision 1.4  2003/01/24 11:16:52  verkout
# HV: Added 'appinfo' target. Displays some info..
#
# Revision 1.3  2003/01/17 10:33:48  verkout
# HV: Can now do (proper) static OR shared linkage. Can run twice, but not
#     two targets in one go.
# 
#
include $(ROOT)/system/compiler.mak
include $(ROOT)/system/locations.mak

#
#  Try to work out where we are (and form the targetname out of that)
#
export CURDIR       = $(shell pwd)
export REPOSDIR     = $(CURDIR)/Repos/$(MACHINE)-$(B2B)
export BASETARGET   = $(shell basename $(CURDIR))

# MYTARGET = logical name, THETARGET is full target: may include OS/Architecture
# dependant parts
export LINKAGE          ?= shared
export STATICTARGET      =
export SHAREDTARGET      =
export THETARGET         = invalid.invalid

SHEXT = so
ifeq ($(OSNAME),HP-UX)
	SHEXT = sl
endif

ifeq ($(LINKAGE),shared)
	export SHAREDTARGET = $(BASETARGET)

endif

ifeq ($(LINKAGE),static)
	XLIBLIST := $(subst DL,,$(XLIBLIST))
	export STATICTARGET  = $(BASETARGET)_s
endif


# only add those deplibs for which there exists ONLY the static version,
# welll.. that is... if the target is shared/both
REALDEPLIBS := $(foreach DEP, $(DEPLIBS), $(shell if test -f $(OWNLIBD)/lib$(DEP).a \
			-a ! -f $(OWNLIBD)/lib$(DEP).$(SHEXT);\
			then echo $(OWNLIBD)/lib$(DEP).a;fi))

export MYTARGET  := $(STATICTARGET) $(SHAREDTARGET)	
ifeq ($(strip $(MYTARGET)),)
	export MYTARGET = invalid
endif
export TPREFIX   := $(ROOT)/bin/$(MACHINE)-$(B2B)/
export THETARGET  = $(addprefix $(TPREFIX), $(foreach TARG, $(MYTARGET), $(TARG)))

#
# Add the current dir to the include path (it would be nice if an app can 
# find its own include files, isn't it...). The library path will already
# be included by the system
#
export TARGETINCD  = $(CURDIR)
export XLIBLIST   += TARGET

#
# We depend on all our object files, who are stored in
# $(CURDIR)/Repos
#
export THEOBJS = $(addprefix $(REPOSDIR)/, $(foreach OBJ, $(ALLOBJS), $(OBJ)))

#
# The dependency files...
#
export THEDEPS = $(addprefix $(REPOSDIR)/, $(foreach DEP, $(ALLDEPFILES), $(DEP)))



.PHONY: all
all: $(THETARGET)
	@echo "-->  App $(THETARGET) is up to date";
	
.PHONY: clean
clean: 
	-@rm -f $(THEOBJS) $(THETARGET);\
	echo "Cleaned out: $(THEOBJS) $(THETARGET)";
	@echo

.PHONY: clobber
clobber:
	-@rm -f $(THEOBJS) $(THEDEPS) $(THETARGET);\
	rmdir $(REPOSDIR);\
	echo "Application stuff clobbered. Go try again!";\
	echo

.PHONY: info
info:
	@if [ -z "$(ROOT)" ]; then \
		echo "ROOT environmentvariable NOT SET!!!";\
	else \
		echo "ROOT=$(ROOT)";\
	fi;\
	echo "LINKAGE=$(LINKAGE), MYTARGET=$(MYTARGET)";\
	echo "THETARGET  =$(THETARGET)";\
	echo "REALDEPLIBS=$(REALDEPLIBS)";\
	echo "XLIBLIST   =$(XLIBLIST)";

# the main target of this makefile...
#$(THETARGET): $(THEOBJS) $(REALDEPLIBS)
#	$(make_binary);

ifneq ($(strip $(STATICTARGET)),)
$(TPREFIX)$(STATICTARGET): $(THEOBJS)
	$(make_static_binary)
endif

ifneq ($(strip $(SHAREDTARGET)),)
$(TPREFIX)$(SHAREDTARGET): $(THEOBJS) $(REALDEPLIBS) 
	$(make_binary)
endif

%invalid:
	@echo "Invalid LINKAGE specifier ($(LINKAGE)), not one of";\
	echo "shared, static";\
	exit;

.PHONY: appinfo
appinfo:
	@echo "OSNAME=$(OSNAME) OSVERSION=$(OSVERSION)"; \
	 echo "MACHINE  = $(MACHINE)"; \
	 echo "RUNTDIRS = $(RUNTDIRS)"; \
	 echo "LDFLAGS  = $(LDFLAGS)";

#
# Now that we've defined a lot of stuff, it might be useful to include 
# the rules!
#
include $(ROOT)/system/rules.mak
include $(THEDEPS)
