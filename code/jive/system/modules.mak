#
# This will  be a makefile wich will create a shared object in
# the directory $(ROOT)/modules by the name of '(basename $PWD).so'
#
include $(ROOT)/system/compiler.mak
include $(ROOT)/system/locations.mak

#
#  Try to work out where we are (and form the targetname out of that)
#
# Note: we work from the assumption that *all* module code
#       resides as subdir under a directory named 'modules'.
#       The full module-name (i.e. could be including a path)
#       will be made from the relative name
#       of the current directory.
#
#
#  e.g.:
#     Directory                                  produces module 
#
#     <root>/modules/test/<srcfiles>             test.so
#     <root>/modules/input/file/<srcfiles>       input/file.so
#     <root>/modules/input/socket/<srcfiles>     input/socket.so
#
export CURDIR     = $(shell pwd)
export REPOSDIR   = $(CURDIR)/Repos/$(MACHINE)-$(B2B)
export XLIBLIST  += MODULE

#
# LINKAGE may be set to 'shared' or 'kernel',
#
# 'shared' produces a .so file which can 
#          be loaded into apps
# 'kernel' produces a .o file, which can be
#          insmod'ed into the linuxkernel	
#
#  default = shared
#
export LINKAGE           ?= shared
export MYTARGET           = invalid

ifeq ($(LINKAGE),shared)
	export MYTARGET   = $(shell echo $(CURDIR) | \
			sed 's=^\(.*\)modules\(.*\)=\2=' | \
			sed 's=^/==').so
endif

ifeq ($(LINKAGE),kernel)
	export KERNELROOT = /usr/src/linux-2.4.19
	export KERNELINCD = $(KERNELROOT)/include
	export KERNELDEFS = __KERNEL__ MODULE
	XLIBLIST         += KERNEL

	# remove dynamic linking from XLIBLIST
	XLIBLIST         := $(patsubst DL,,$(XLIBLIST))

	# remove -fPIC from the flags
	export CFLAGS   := $(patsubst -fPIC,,$(CFLAGS))
	export CXXFLAGS := $(patsubst -fPIC,,$(CXXFLAGS))

	export MYTARGET   = $(shell echo $(CURDIR) | \
			sed 's=^\(.*\)modules\(.*\)=\2=' | \
			sed 's=^/==').o
endif

# MYTARGET = logical name of target, THETARGET will be full name 
# of target, which may include any OS/Machine/Compiler specific parts
export TARGETROOT = $(ROOT)/modules/$(MACHINE)-$(B2B)
export THETARGET  = $(ROOT)/modules/$(MACHINE)-$(B2B)/$(MYTARGET)

#
# We depend on all our object files, who are stored in
# $(CURDIR)/Repos
#
export THEOBJS = $(addprefix $(REPOSDIR)/, $(foreach OBJ, $(ALLOBJS), $(OBJ)))

#
# The dependency files...
#
export THEDEPS = $(addprefix $(REPOSDIR)/, $(foreach DEP, $(ALLDEPFILES), $(DEP)))

REALDEPLIBS := $(foreach DEP, $(DEPLIBS), $(shell if test -f $(OWNLIBD)/lib$(DEP).a;\
			then echo $(OWNLIBD)/lib$(DEP).a;fi))

.PHONY: all
all: $(THETARGET)
	@echo "-->  $(MYTARGET) is up to date"; echo;
	
.PHONY: clean
clean: 
	-@rm -f $(THEOBJS) $(THETARGET);\
	echo "Cleaned out: $(THEOBJS) $(THETARGET)";
	echo

.PHONY: clobber
clobber:
	-@rm -f $(THEOBJS) $(THEDEPS) $(THETARGET);\
	rmdir $(REPOSDIR);\
	echo "Everything's deleted. You can now go home.... or try again";

.PHONY: invalid
invalid:
	@echo "Invalid LINKAGE specifier [$(LINKAGE)]."; \
	echo "Choose either shared or kernel."

output:
	echo $(MYTARGET)
#
# Main target. Defined to depend on all its objectfiles....
#
#$(TARGETROOT)/%.so : $(THEOBJS) $(REALDEPLIBS)
#	$(make_shared_module)

#$(TARGETROOT)/%.o : $(THEOBJS) $(REALDEPLIBS)
#	@cp -f $(REPOSDIR)/$(MYTARGET) $(TARGETROOT)/$(MYTARGET)

ifeq ($(LINKAGE),shared)
$(THETARGET): $(THEOBJS) $(REALDEPLIBS)
	$(make_shared_module)
endif

ifeq ($(LINKAGE),kernel)
$(THETARGET): $(THEOBJS) $(REALDEPLIBS)
	$(make_kernel_module)
endif
	
#$(THETARGET): $(THEOBJS) $(REALDEPLIBS)
#	$(make_shared_module);

#
# Include all the other rules!
#
include $(ROOT)/system/rules.mak
include $(THEDEPS)
