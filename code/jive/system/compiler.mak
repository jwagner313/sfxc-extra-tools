#
# Lets find out which system we're on and create shortcuts to the parts of the
# system identification
#
# $Id: compiler.mak,v 1.22 2011/10/12 12:45:36 jive_cc Exp $
#
# $Log: compiler.mak,v $
# Revision 1.22  2011/10/12 12:45:36  jive_cc
# HV: * DtdLUT (DataTransferDescriptor Lookup Table) removed and
#       changed into 'labels' for more genericity
#     * can now be built either as part of AIPS++/CASA [using makefile]
#       or can be built as standalone set of libs + binaries and linking
#       against casacore [using Makefile]
#       check 'code/
#
# Revision 1.21  2005-09-16 14:58:30  verkout
# HV: Explicit 64bit lib path not necessary anymore (on the PCInt cluster)
#
# Revision 1.20  2005/03/22 12:41:22  verkout
# ${logmsg}
#
# Revision 1.19  2004/11/02 20:59:10  verkout
# HV: - Added support for building on the PCInt cluster
#     - Added support for building 64bit (B2B=64)
#
# Revision 1.18  2004/06/03 14:50:33  verkout
# HV: Needed extra template depth for Boost::Spirit ... 25 just wasn't enough...
#
# Revision 1.17  2003/02/27 09:06:13  verkout
# HV: * Added support for building kernelmodules
# 	  (=linuxdrivers). Place code under $ROOT/src/modules
# 	  and in the makefile, say: LINKAGE=kernel. Default
# 	  is LINKAGE=shared, which produces a module which
# 	  is runtime-loadable into apps.
# 	* Added package MODULE, libs/apps that wish to be able
# 	  to deal with modules (load them, include header files)
# 	  can add MODULE to their XLIBLIST. The module-source-dir
# 	  gets added to the include path and a commandline define
# 	  MODULEDIR gets added. MODULEDIR is the directory where
# 	  compiled modules for your current system will end up.
# 	  Removed the MODULEDIR-define from package OWN, where it
# 	  was before.
# 	* In the module-makefile, package MODULE automatically
# 	  gets added to the XLIBLIST
# 	* Added a rule to build a kernel module. Note: at the
# 	  moment I think it's reasonably linux-specific
#
# Revision 1.16  2003/01/24 12:20:04  verkout
# HV: Hmmmm... LDFLAGS was explicitly set to something that wouldn't work on Solaris. Changed it to using the XLIBLIST variable, which takes care of saving the path to the shared lib in the executable image.
#
# Revision 1.15  2003/01/17 10:37:00  verkout
# HV: Can now do (proper) static OR shared linkage. Can run twice, but not
#     two targets in one go. SHAREDIMGFLAGS had to be nulled; if -fPIC -shared
#     were present, apps would link but segfault upon startup...
#
#
export SYSTEM    := $(shell uname -mrs | tr / - )
export OSNAME    := $(strip $(word 1, $(SYSTEM)))
export OSVERSION := $(strip $(word 2, $(SYSTEM)))
export MACHINE   := $(strip $(word 3, $(SYSTEM)))

#
# For all builds, make this one available:
#
export HVDATETIME=$(shell date '+%A %d %B %Y/%T')

#
# Definitions for the compiler
#
# CXX is used for C++ code
# CC  is used for C code
#
ifeq ($(OSNAME),SunOS)
	ifeq ($(OSVERSION),5.7)
		export GCCROOT = /local/gcc-3.2-sol-5.7
	else
		export GCCROOT = /local/gcc-3.2
	endif
else 
	export GCCROOT = /usr
endif

#
# EXTRACXXFLGS could be used by the pgmr
# to add some extra flags..
# 
export B2B           = 64

export CXX           = $(GCCROOT)/bin/g++
export CXXFLAGS      = -fPIC -g -Wall -W -Werror -fexceptions -ftemplate-depth-35 \
	-D_REENTRANT -DBUILDTIME="\"$(HVDATETIME)\"" \
	-D_POSIX_PTHREAD_SEMANTICS -I$(HOME)/install/include/casacore/
#	-O2

export CC            = $(GCCROOT)/bin/gcc
export CFLAGS        = -fPIC -g -Wall -fexceptions \
	-D_POSIX_PTHREAD_SEMANTICS \
	-D_REENTRANT -DBUILDTIME="\"$(HVDATETIME)\"" -I$(HOME)/install/include/casacore/
#	-O2

# build 64bit or 32bit code
ifeq ($(B2B),64)
	export CXXFLAGS += -m64	
	export CFLAGS   += -m64	
else
	ifeq ($(B2B),32)
		export CXXFLAGS += -m32
		export CFLAGS   += -m32	
	else
		echo "Error: Unsupported value for B2B (Bits2Build)"
	endif
endif


#export LDFLAGS       = -Xlinker -rpath $(GCCROOT)/lib
ifeq ($(B2B),64)
	export LDFLAGS = -m64 -L$(HOME)/install/lib
else
	ifeq ($(B2B),32)
		export LDFLAGS = -m32 -L$(HOME)/install/lib
	else
		echo "Error: Unsupported value for B2B (Bits2Build)"
	endif
endif

GCCLIBD   = $(GCCROOT)/lib

#
# for 64-bit code...
#
#ifeq ($(B2B),64)
#	OGCLIBD    := $(GCCLIBD)
#	GCCLIBD    := /apps/usr/lib64 /lib64 /usr/lib64 $(OGCLIBD)
#endif

XLIBLIST += GCC

#
# support for profiling
#
ifdef PROF
	CXXFLAGS += -pg -DUSE_PROFILING
	CFLAGS   += -pg -DUSE_PROFILING
	LDFLAGS  += -pg
#	STATICBUILD = 1
	LINKAGE   = static
endif

#
# definitions for AWK
#
export AWK           = /bin/awk

ifeq ($(OSNAME),SunOS)
	export AWK       = /local/bin/awk
endif

#
# How to generate dependencies
#
# MKcDEP  => generate dependency from a .c file
# MKccDEP => generate dependency from a .cc file
# MKCDEP  => generate dependency from a .C file
#
export MKcDEP        = $(CC) -M
export MKccDEP       = $(CXX) -M
export MKCDEP        = $(CXX) -M

#
# The various link programs.
#
# LDMOD       to produce a module from a number of objectfiles
# LDSHLIB     to produce a shared lib from a number of objectfiles
# LDSTATIC    to procude a static lib from a number of objectfiles
# LDBIN       to produce a dynamically linked binary executable image from some stuff
# LDBINSTATIC to produce a statically linked binary executable image from some stuff
# LDEXE       to produce an executable image

#LDMOD
ifeq ($(OSNAME),Linux)
	export LDMOD      = $(CXX) $(LDFLAGS) -rdynamic -shared -fPIC
else
	export LDMOD      = $(CXX) $(LDFLAGS) -shared -fPIC
endif


#LDSHLIB
ifeq ($(OSNAME),Linux)
	export LDSHLIB    = $(CXX) $(LDFLAGS) -rdynamic -shared -fPIC
else
	export LDSHLIB    = $(CXX) $(LDFLAGS) -shared -fPIC
endif


#LDSTATIC
export LDSTATIC       = ar cru

#LDBIN
#ifdef STATICBUILD
#	export LDBIN      = $(CXX) $(LDFLAGS) -static
#else
#	ifeq ($(OSNAME),Linux)
#		export LDBIN      = $(CXX) $(LDFLAGS) -rdynamic
#	else
#		export LDBIN      = $(CXX) $(LDFLAGS) 
#	endif
#endif

#LDEXE
export LDEXE           = $(CXX) $(LDFLAGS)


# LDEXE can have static/shared flags
export STATICIMGFLAGS    = -static
export SHAREDIMGFLAGS    = 
export KERNELMODULEFLAGS = -r

# Find all participating sources,
# if the user din't tell us what they are
ifeq ($(strip $(SOURCES)),)
	export SOURCES := $(shell /usr/bin/find . -name \*.c -o -name \*.cc -o -name \*.C)
endif
export cSRC	:= $(filter %.c,$(SOURCES))
export ccSRC	:= $(filter %.cc,$(SOURCES))
export CSRC	:= $(filter %.C,$(SOURCES))
#export cSRC          := $(shell ${ROOT}/system/findcsrc )
#export ccSRC         := $(shell ${ROOT}/system/findccsrc )
#export CSRC          := $(shell ${ROOT}/system/findCsrc )
export MKFIL         := $(shell ls *akefile)

#
# Create list of objectfiles.....
#
export cOBJ       := $(cSRC:.c=.o)
export ccOBJ      := $(ccSRC:.cc=.o)
export COBJ       := $(CSRC:.C=.o)

export ALLOBJS    := $(cOBJ) $(ccOBJ) $(COBJ)

#
# Create a list of dependency files....
#
export cDEPFILES       = $(cSRC:.c=.d)
export ccDEPFILES      = $(ccSRC:.cc=.dd)
export CDEPFILES       = $(CSRC:.C=.D)

export ALLDEPFILES = $(cDEPFILES) $(ccDEPFILES) $(CDEPFILES) 

