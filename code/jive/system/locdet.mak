#
# Makefile which is used by the $ROOT/scripts/runpciproc.sh
# script to find out some directories that may have to be added
# to the LD_LIBRARY_PATH.
# Using the make system with the same compiler/locations.mak files
# enables to almost certainly get the right values for the right
# system....
#
include compiler.mak
include locations.mak
#include $(ROOT)/system/compiler.mak
#include $(ROOT)/system/locations.mak

.PHONY: xercescinfo
xercescinfo:
	@echo "$(XERCESCROOT)"
