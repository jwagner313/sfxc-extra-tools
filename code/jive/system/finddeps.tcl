# test for $ROOT set
if { ![info exists env(ROOT)] } {
	puts "ROOT environment variable not set!";
	exit 1;
}
set root $env(ROOT);
# gather all makefiles/Makefiles and skip the one in the current dir.
set files [exec find . -name Makefile -o -name makefile | sed s=^./== | grep -v "^Ma" | grep -v "^ma"];
variable libs;
variable buildorder;
# loop over all the files found
for {set i 0} {$i<[llength $files]} {incr i} {
	set curf [lindex $files $i];
	regexp {^([^/]*)/.*$} $curf {} curl;
	set deps [exec sed -f $root/system/dependencies.sed $curf];
#	puts "$curl: $deps";
	set libs($curl) $deps
}
# now then... loop over all libs and try to resolve deps
set libidx 1;
set oldsize [array size libs];
while {[array size libs]>0} {
	foreach curlib [array names libs] {
		set curdeps $libs($curlib);
#		puts "Processing: $curlib \[$curdeps\]";
		if {[llength $curdeps] == 0} {
			# lib has no deps, can add it immediately!
#			puts "Library($curlib) has no deps. Buildorder=$libidx";
			set buildorder($libidx) $curlib;
			unset libs($curlib);
			incr libidx;
			continue;
		}
		# ok. now the tough work starts. Check if the curlib
		# depends only on libs found in 'buildorder' 
		set cnt 0;
		foreach bld [array names buildorder] {
			set dep2look4 $buildorder($bld);
			if { [lsearch -exact $curdeps $dep2look4] != -1} {
				incr cnt;
			}
		}
		if { $cnt == [llength $curdeps] } {
#			puts "Resolved all dependencies for $curlib. Buildorder=$libidx";
			set buildorder($libidx) $curlib;
			unset libs($curlib);
			incr libidx;
		}
	}
	#
	# we have gone through all the libs
	# once now. check if [array size libs]
	# decreased. If it didn't, no deps
	# were resolved and we're probably
	# stuffed!
	#
	# Note: in principle newsize can never
	# be > oldsize, however, testing
	# on >= catches *everything* that might
	# go wrong...
	#
	set newsize [array size libs];
	if {$newsize >= $oldsize} {
		puts stderr "Arg?! Cannot resolve dependencies?";
		puts stderr "Check DEPLIBS statements in makefiles!";
		puts stderr " and make sure that the dependencies";
		puts stderr " are correct.";
		puts stderr "";
		puts stderr "Failing library(ies):";
		puts -nonewline stderr "  ";
		foreach name [array names libs] {
			puts -nonewline stderr "$name ";
		}
		puts stderr "";
		puts stderr "";
		exit 1;
	}
	set oldsize $newsize;
}
for {set idx 1} {$idx <= [array size buildorder]} {incr idx} {
	puts -nonewline "$buildorder($idx) ";
}
puts "";
exit 0;
