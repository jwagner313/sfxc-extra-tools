#!/bin/sh 

#
# $Id: fixincldir.sh,v 1.6 2006/02/14 13:14:44 verkout Exp $
#
# $Log: fixincldir.sh,v $
# Revision 1.6  2006/02/14 13:14:44  verkout
# HV: Script bails out if <Path> cannot be determined.
#
# Revision 1.5  2006/02/14 13:08:45  verkout
# HV: The script now also creates the output directories where casa/aips++ stores its output (binaries,libs,aux-info). Also changed string comparison on rc (return code) into numerical comparisons
#
# Revision 1.4  2006/02/14 12:56:16  verkout
# HV: Workaround for sh to retrieve the nth (in this case: 2nd) word of an environmentvar (simulate arrays in sh)
#
# Revision 1.3  2006/02/14 12:48:34  verkout
# HV: Grunt... operator == is not recognized by *real* sh
#
# Revision 1.2  2006/02/14 12:25:30  verkout
# HV: * forget the ! in #/bin/sh (bugger) * now tells what's been done if succes * fixed bug in detecting wether or not the target existed or not
#
# Revision 1.1  2006/02/14 10:19:59  verkout
# HV: Script to set up the 'include' symlink for jive-aips++ code correct. Should be executed after checkout (hopefully it's done automagically)
#
#
# This script should be run after being checked out;
# it tries to figure out where we are and then
# creates a symlink for the include path;
# otherwise the jive-aips++ stuff won't be able
# to find its own headers.
#
# NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE
#
#    This script should be called from somewhere in
#    the hierarchy, including being called from
#    <Path> (see below). Otherwise it will not
#    be able to tell where it is nor where to
#    do it's thang
#
# ===================================================
#
# Once the code is checked out the directory hierarchy looks
# like this:
#
# <Path>/code/jive/
#                  implement/
#                            ms2uvfits/{sourcecode-files}
#                            j2ms/{sourcecode-files}
#                            ...
#                  apps/
#                       j2ms2/{code for j2ms2 application}
#                       2bitVV/{code for 2bitVV application}
#                       tConvert/{...}
#                       ...
#
# Where <Path> is the location where you entered the
# 'cvs checkout' command.
#
# This script is in
# <Path>/code/jive/
#
# For the stuff under 'implement' and 'apps' to find
# its own headers, a link must be present:
#
# <Path>/code/include/jive  {this symlink should point to:
#                            <Path>/code/jive/implement}
#
# Also, for the system to work "nicely"
# we should create locations for aips++ (casa)
# to put its results (libs and binaries).
#
# The casa makesystem (wants to) put stuff under
#
# <Path>/<aips-arch>/(bin|lib|bindbg|libdb|aux)
# 
# and if these do not exist, it will put the .a and .o
# files and binaries in the current dir.
#
# Therefore: we create these mothers. However, we need
# the <aips-arch> for that, which is the second word
# in the AIPSPATH environmentvariable (if aips++/casa
# init file sourced).
#

# we really *need* aipsenv.var set
if [ -z "${AIPSPATH}" ]; then
    printf "\n\nHey! In order for this script to function\n"
    printf "you'll need to source an aips++/casa init script;\n"
    printf "we need some info from aips++/casa.\n\n"
    printf "Please visit us again after you have done this.\n"
    printf "Thanks for your attention.\n"
    exit 3
fi

aipsys=`/bin/echo "${AIPSPATH}" | awk '{ print $2; }'`

# see how we are called (we don't care
# what the script's called, as long
# as we can tell where we are...
us=`basename $0`

# never hurts to see where we were called from
curdir=`/bin/pwd`

#
# Step one:
#  Try to find out where we are;
#  we try to find the correct
#  value for <Path>
#  Set it to empty at first
#
Path=

# we *know* that this script should
# be adressable as: <Path>/code/jive/${us}
# So if we find /code/jive/ we can work our
# way from that
case "$0" in
    \.\./* | \./* )
        # being called from relative path
        # prepend current working dir
        Path=${curdir}/${0}
        ;;

    code/* | jive/* )
        Path=${curdir}/$0
        ;;

    *code/jive/* )
        Path=$0
        ;;
       
    *)
        # ??? current dir?
        Path=${curdir}
        ;;
esac

# verify we have 'code/jive' somewhere in there...
case "${Path}" in
    *code/jive* )
        # fine
        ;;
    * )
        # bugger!
        { printf "\nERROR ERROR ERROR\n\n";
          printf "The script is NOT located somewhere\n";
          printf "under the code/jive directory hierarchy!\n";
          printf "Did you do a correct checkout? Or has the\n";
          printf "thing moved/changed? In this case you'll\n";
          printf "have to update this script:\n";
          printf "    $0\n";
         exit 1; }
        ;;
esac

# strip trailing stuff
Path=`echo "$Path" | sed -e 's=code/jive.\{0,\}$=='`

# if Path is now empty, we were called
# as 'code/jive/...' so <Path> really is
# the current dir!
if [ -z "${Path}" ]; then
    Path=${curdir}
fi
# strip trailing slash(es), if any
Path=`echo "${Path}" | sed -e 's=/\{0,\}$=='`

# don't do anything if
# Path is empty
rc=1

if [ -z "${Path}" ]; then
    printf "\n\nRats! The script was unable to figure out\n"
    printf "what your '<Path>' (see this script) was.\n"
    printf "\n\n"
    printf "Can't do anything since don't know *where* to do it.\n"
    printf "Open up this script and try to debug....\n"
    printf "Or: mail/call verkouter@jive.nl to complain\n"
    exit 4
fi

#
# Now we start doing stuff
#
mkdir -p "${Path}/code/include"
rc=$?
if [ ${rc} = 0 ]; then
    # remove any existing link
    ltarget=${Path}/code/include/jive
    if [ -L "${ltarget}" ]; then
        rm -f "${ltarget}"
         rc=$?
    else
        if [ -e "${ltarget}" ]; then
            printf "\nWot?!?!\n${ltarget}\n"
            printf "exists BUT IT IS NOT A SYMLINK????\n"
            printf "\nThis script will now exit without\n"
            printf "doing anything...\n"
            rc=2
        fi
    fi
        
    # attempt to link (only if still success
    if [ ${rc} = 0 ]; then
        ln -s "${Path}/code/jive/implement" "${ltarget}"
        rc=$?
    fi
    if [ ${rc} = 0 ]; then
	    printf "\n${us}: Succesfully linked the include\n"
		printf "directory to the implement directory:\n\n"
		printf "   ${ltarget}\n"
		printf "        =>\n"
		printf "   ${Path}/code/jive/implement\n\n"
		printf "This is what the OS thinks:\n\n"
		ls -ald "${ltarget}"
		printf "\n"
	fi
fi
# if still success, make all the directories we need
basedir=${Path}/${aipsys}
for d in bin bindbg lib libdbg aux; do
    target=${basedir}/${d}
    printf "Creating ${target}: "
    mkdir -p "${target}"
    rc=$?
    if [ ${rc} != 0 ]; then
        printf "Failed?!\n"
        break
    fi
    printf "Ok\n"
done

exit ${rc}
